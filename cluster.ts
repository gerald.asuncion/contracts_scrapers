import {isMaster, fork} from "cluster"
const cpus   = require( 'os' ).cpus().length;
if(isMaster){
    for( let i = 0; i < cpus; i++ ) {
        fork();
    }
}else {
    require("./index")
}
