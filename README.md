# Contracts Scrapers

Repository dei Servizi di scraping, questi servizi partono su processi separati.

1. **API** Rest Api permette di chiamare gli scraper in modo sincrono. Quindi si attende la loro risposta

2. **Task Runner** Consumer della tabella queue per gestire gli scraper in modo asincrono, progettati per rispondere mediante webhook a chi ha inserito il task in coda

## Sviluppo

Per la fase di sviluppo si hanno due comandi per i due servizi in questo repo:

1. Api `yarn serve:api`

2. Task Consumer  `yarn serve:taskrunner`

## Unit test

I test automatici nel progetto vengono effettuati tramite la libreria [jest](https://jestjs.io/).
I vari test si trovano sotto la cartella `<rootDir>/test`, per eseguirli basta lanciare il comando `yarn test`.

Se invece si vuole eseguire i test presenti in un particolare file basta lanciare il comando `yarn test -- <nome modulo>`.
Ad esempio per il file ./test/utils/createFilenameForScreenshot.test.ts, il comando diventa:

```bash
yarn test -- createFilenameForScreenshot
```

Vi è anche la cartella `<rootDir>/tests` coi test fatti alla vecchia maniera, se si vogliono mantenere vanno passati alla modalità presente nella cartella `<rootDir>/test`.

## Deploy

Le operazioni di deploy sono semplicemente:

1. collegarsi alla macchina di build in base all'ambiente come utente **node** in ssh `ssh node@<ip>`
2. spostarsi nella cartella contenente il progetto `cd /var/www/<ambiente>/scrapers.supermoney.lan`
3. e poi lanciare il comando `yarn deploy:<ambiente>`

Al posto di ambiente quindi avremo `test` o `prod`.

### PROD

1. Collegarsi alla macchina 172.17.26.183 come utente **node** in ssh.

   ```bash
     ssh node@172.17.26.183
   ```

2. Una volta entrati spostarsi nella cartella contenente il progetto:

   ```bash
     cd /var/www/prod/scrapers.supermoney.lan
   ```

3. A questo punto si può lanciare lo script che si occupa di aggioranre il progetto, syncare le altre macchine e riavviarne i processi:

   ```bash
     yarn deploy:prod
   ```

### TEST

1. Collegarsi alla macchina 172.17.26.231 come utente **node** in ssh.

   ```bash
     ssh node@172.17.26.231
   ```

2. Una volta entrati spostarsi nella cartella contenente il progetto:

   ```bash
     cd /var/www/test/scrapers.supermoney.lan
   ```

3. A questo punto si può lanciare lo script che si occupa di aggioranre il progetto e riavviare i processi:

   ```bash
     yarn deploy:test
   ```

## Logs

I file di log per i vari ambienti si trovano:

### LOCALE
  path logs: `{project folder}/log`

### PROD
  path logs: `/var/log/web/scrapers.supermoney.lan/`

### TEST
  path logs: `/var/log/web/scrapers.supermoney.lan/`

Nelle cartelle di log si trovano i seguenti i file:
- http.log => gli accessi
- scrapers.log => i log dell'applicazione
- pm2.log => i log su console catturati da pm2

## Disaster Recovery

Per collegarsi alla macchina in disaster recovery, aprire un terminale e collegarsi a scraper00 (172.17.26.183) e poi usare il comando:

```bash
ssh 10.17.33.230
```

## Swagger

http://localhost:5000/api/docs
