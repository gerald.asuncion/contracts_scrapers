import { S3Client } from '@aws-sdk/client-s3';
import { AwsConfig, AwsS3Config } from '../config/types';
import { Logger } from '../logger';
import extractErrorMessage from '../utils/extractErrorMessage';

function createS3Client({ userAccessKeyId, userSecretAccessKey, region }: AwsS3Config): S3Client {
  const client = new S3Client({
    credentials: {
      accessKeyId: userAccessKeyId,
      secretAccessKey: userSecretAccessKey
    },
    region
  });
  return client;
}

export type S3ClientRun = (client: S3Client, config: AwsS3Config, logger: Logger) => Promise<void>;

export type S3ClientFactoryResult = () => (cb: S3ClientRun, logger: Logger) => Promise<void>;

export type S3ClientFactory = {
  (awsConfig: AwsConfig): S3ClientFactoryResult;
};

export default function s3ClientFactory(awsConfig: { aws: AwsConfig }): S3ClientFactoryResult {
  return () => {
    const client = createS3Client(awsConfig.aws.s3);
    return async (cb: S3ClientRun, logger: Logger) => {
      try {
        await cb(client, awsConfig.aws.s3, logger);
      } catch(ex) {
        logger.error(`Non sono riuscito a salvare lo screenshot su aws: ${extractErrorMessage(ex)}`);
      } finally {
        client.destroy();
      }
    };
  };
}
