import nodemailer from 'nodemailer';
import { Logger } from './logger';
import createChildLogger from './utils/createChildLogger';
import BrowserManager from './browser/BrowserManager';

export class Mailer implements MailerInterface {
  private transporter: any;

  private subjectPrefix: string;

  protected childLogger: Logger;

  constructor({ mailConfig, logger }: any) {
    this.transporter = nodemailer.createTransport({
      service: mailConfig.service,
      auth: {
        user: mailConfig.user,
        pass: mailConfig.pass
      }
    });
    this.subjectPrefix = mailConfig.subjectPrefix || '';
    this.childLogger = createChildLogger(logger, 'Mailer');
  }

  async send(to: string, subject: string, html: string, text: string): Promise<any> {
    // send mail with defined transport object
    const info = await this.transporter.sendMail({
      from: '"scraper" <no-reply@supermoney.eu>', // sender address
      to, // 'bar@example.com, baz@example.com', // list of receivers
      subject: this.subjectPrefix + subject,
      text, // 'Hello world?', // plain text body
      html, // '<b>Hello world?</b>' // html body
    });
    this.childLogger.info(`Message sent: ${info.messageId}`);
  }
}

export interface MailerInterface {
  send(to: string, subject: string, html: string, text: string): Promise<any[]>,
}

export interface MailerArgs {
  mailer: MailerInterface
}

export interface ScraperMailerArgs {
  browser: BrowserManager,
  mailer: MailerInterface,
  toItTeam: string
}
