export default function gracefulStartup(): void {
  if (process.send) {
    process.send('ready');
  } else {
    // eslint-disable-next-line no-console
    console.error('non posso inviare il segnale `ready` a pm2. la funzione process::send non esiste');
  }
}
