export default class GracefulShutdownController {
  private toTerminate: boolean;

  private locked: boolean;

  constructor() {
    this.toTerminate = false;
    this.locked = false;
  }

  isTerminable(): boolean { return this.toTerminate; }

  terminate(): void {
    this.toTerminate = true;
  }

  isUnlocked(): boolean { return this.locked === false; }

  lock(): void {
    this.locked = true;
  }

  free(): void {
    this.locked = false;
  }

  isStoppable(): boolean {
    return this.isTerminable() && this.isUnlocked();
  }
}
