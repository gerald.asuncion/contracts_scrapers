import { asClass, asFunction, asValue, createContainer } from 'awilix';
import config from 'config';
import emptyValidator from './emptyValidator';
import { MysqlPool } from './mysql';
import { Mailer } from './mailer';
import { logger } from './logger';
import TaskRepo from './repo/TaskRepo';
import ErrorMailerSelector from './task/ErrorMailerSelector';
import TaskDispatcherErrorMailer from './task/TaskDispatcherErrorMailer';
import createTempDirectory from './utils/createTempDirectory';
import { MailConfig, MysqlConfig } from './config/types';
import BloccoScraperErrorsRepo from './repo/BloccoScraperErrorsRepo';
import GracefulShutdownController from './graceful/GracefulShutdownController';
import BrowserManager from './browser/BrowserManager';
import s3ClientFactory from './aws/s3ClientFactory';
import registerScrapersToContainer from './register/registerScrapersToContainer';

createTempDirectory();

const container = createContainer();

container.register({
  puppeteerOptions: asValue(config.get('puppeteer.browser.options')),
  mysqlConfig: asValue<MysqlConfig>(config.get('mysqlConfig')),
  mailConfig: asValue<MailConfig>(config.get('mailConfig')),
  loggerConfig: asValue(config.get('loggerConfig')),
  toItTeam: asValue<string>(config.get('toItTeam')),
  baseDownloadPath: asValue<string>(config.get('download.path')),
  credenzialiServiceBaseUrl: asValue<string>(config.get('services.credenziali.baseUrl')),
  certificatiClientBasePath: asValue<string>(config.get('certificati.client.basePath')),
  browser: asClass(BrowserManager).singleton(),
  emptyValidator: asValue(emptyValidator),
  taskRepo: asClass(TaskRepo).singleton(),
  mysqlPool: asClass(MysqlPool).singleton(),
  mailer: asClass(Mailer).singleton(),
  logger: asValue(logger),
  errorMailerSelector: asClass(ErrorMailerSelector).singleton(),
  taskDispatcherErrorMailer: asClass(TaskDispatcherErrorMailer).singleton(),
  bloccoScraperErrorsRepo: asClass(BloccoScraperErrorsRepo).singleton(),
  gracefulShutdownController: asClass(GracefulShutdownController).singleton(),
  aws: asValue(config.get('aws')),
  s3ClientFactory: asFunction(s3ClientFactory).singleton(),
});

registerScrapersToContainer(container, config);

export default container;
