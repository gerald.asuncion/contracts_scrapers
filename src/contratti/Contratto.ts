export type Contratto = {
  nome: string;
  cognome: string;
  codiceFiscale: string;
};
