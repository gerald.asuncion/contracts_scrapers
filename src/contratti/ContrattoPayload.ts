type ContrattoPayload = {
  idDatiContratto: number;
  webhook?: string;
};

export default ContrattoPayload;
