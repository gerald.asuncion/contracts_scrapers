import { asClass, asFunction, asValue, createContainer, Lifetime } from 'awilix';
import { Router } from 'express';
import config from 'config';
import scraperBuilder from './scraper-builder';
import emptyValidator from './emptyValidator';
import { MailConfig, MysqlConfig } from './config/types';
import { MysqlPool } from './mysql';
import { Mailer } from './mailer';
import { logger, loggerHttp } from './logger';
import ScraperConfig from './config/ScraperConfig';
import healthCheck from './healthcheck';
import irenRemiCsvImporter from './importer/iren-remi-csv-importer';
import atenaCsvImporter from './importer/atena-csv-importer';
import TaskRepo from './repo/TaskRepo';
import TaskQueue from './task/TaskQueue';
import agsmGasUploadCsv from './scraperv2/agsm/gas/agsmGasUploadCsv';
import createTempDirectory from './utils/createTempDirectory';
import scraperList from './config/scraperList';
import irenRemiXlsImporter from './importer/iren-remi-xls-importer';
import AgsmGasImporterRepo from './repo/AgsmGasImporterRepo';
import agsmGasUploadXls from './scraperv2/agsm/gas/agsmGasUploadXls';
import GracefulShutdownController from './graceful/GracefulShutdownController';
import BrowserManager from './browser/BrowserManager';
import s3ClientFactory from './aws/s3ClientFactory';
import EdisonBlacklistRepo from './repo/EdisonBlacklistRepo';
import registerScrapersToContainer from './register/registerScrapersToContainer';

createTempDirectory();

const container = createContainer();

container.loadModules([
  'build/api/src/mock/**/*.js'
], {
  formatName: 'camelCase',
  resolverOptions: {
    register: asClass,
    lifetime: Lifetime.SCOPED,
  },
});

container.register({
  router: asValue<Router>(Router()),
  browser: asClass(BrowserManager).singleton(),
  scraperBuilder: asFunction(scraperBuilder).singleton(),
  emptyValidator: asValue(emptyValidator),
  taskRepo: asClass(TaskRepo).singleton(),
  irenRemiCsvImporter: asFunction(irenRemiCsvImporter).scoped(),
  atenaCsvImporter: asFunction(atenaCsvImporter).scoped(),
  mysqlPool: asClass(MysqlPool).singleton(),
  mailer: asClass(Mailer).singleton(),
  logger: asValue(logger),
  loggerHttp: asValue(loggerHttp),
  healthCheck: asFunction(healthCheck).singleton(),
  agsmGasUploadCsv: asFunction(agsmGasUploadCsv).singleton(),
  irenRemiXlsImporter: asFunction(irenRemiXlsImporter).singleton(),
  puppeteerOptions: asValue(config.get('puppeteer.browser.options')),
  port: asValue(config.get('server.port')),
  scraperList: asValue<ScraperConfig[]>(scraperList),
  mysqlConfig: asValue<MysqlConfig>(config.get('mysqlConfig')),
  mailConfig: asValue<MailConfig>(config.get('mailConfig')),
  loggerConfig: asValue(config.get('loggerConfig')),
  healthCheckFile: asValue<string>(config.get('healthCheck.path')),
  baseDownloadPath: asValue<string>(config.get('download.path')),
  toItTeam: asValue<string>(config.get('toItTeam')),
  nfsPath: asValue<string>(config.get('nfsConfig.path')),
  credenzialiServiceBaseUrl: asValue<string>(config.get('services.credenziali.baseUrl')),
  certificatiClientBasePath: asValue<string>(config.get('certificati.client.basePath')),
  taskQueue: asClass(TaskQueue).singleton(),
  agsmGasImporterRepo: asClass(AgsmGasImporterRepo).singleton(),
  agsmGasUploadXls: asFunction(agsmGasUploadXls).scoped(),
  gracefulShutdownController: asClass(GracefulShutdownController).singleton(),
  aws: asValue(config.get('aws')),
  s3ClientFactory: asFunction(s3ClientFactory).singleton(),
  edisonBlacklistRepo: asClass(EdisonBlacklistRepo).singleton(),
});

registerScrapersToContainer(container, config);

scraperList.filter((s) => s.enabled).forEach(({ options, name }: ScraperConfig): void => {
  Object.keys(options).forEach((key) => {
    const label = key + name.split('-').map((n: string) => n.charAt(0).toUpperCase() + n.substring(1)).join('');
    const value = options[key];
    container.register(label, asValue(value));
  });
});

export default container;
