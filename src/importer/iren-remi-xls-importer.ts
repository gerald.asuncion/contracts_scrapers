import path from 'path';
import fs from 'fs';
import { Router, Request, Response } from 'express';
import multer from 'multer';
import { Logger } from '../logger';
import { MysqlPool } from '../mysql';
import createChildLogger from '../utils/createChildLogger';
import getCurrentDayFormatted from '../utils/getCurrentDayFormatted';
import extractErrorMessage from '../utils/extractErrorMessage';
import FailureResponse from '../response/FailureResponse';
import parseXlsxToCsv from '../scraperv2/areti/parseXlsxToCsv';
import SuccessResponse from '../response/SuccessResponse';
import emptyDirectory from '../utils/emptyDirectory';

type DataImporterOptions = {
  logger: Logger;
  router: Router;
  mysqlPool: MysqlPool;
  baseDownloadPath: string;
};

export default function irenRemiXlsImporter({
  logger,
  router,
  mysqlPool,
  baseDownloadPath
}: DataImporterOptions): void {
  const myLogger = createChildLogger(logger, 'IrenRemiXlsImporter');
  const upload = multer({ dest: path.resolve(baseDownloadPath) });

  const sql = `LOAD DATA LOCAL INFILE ?
    INTO TABLE ??
    FIELDS TERMINATED BY ';'
    LINES TERMINATED BY '\n'
    IGNORE 1 LINES
    (AREABACKOFFICE, REGIONE, PROVINCIA, COMUNE, CAP, REMI, VECCHIA_REMI, DISTRIBUTORE, SUBENTRO);`;

  const tableName = 'IREN_GAS_REMI';

  myLogger.info('registro irenRemiXlsImporter alla rotta `/import/iren-remi-xls`');
  router.post('/import/iren-remi-xls', upload.single('xls'), async (req: Request, res: Response) => {
    try {
      const xlsFilePath = req.file?.path as string;
      myLogger.info(`Ricevuto file ${xlsFilePath}`);

      const downloadDir = path.resolve(baseDownloadPath, getCurrentDayFormatted(), 'iren-remi');
      if (!fs.existsSync(downloadDir)) {
        fs.mkdirSync(downloadDir);
      }

      myLogger.info('lo converto in csv');
      const parsedCsvPath = path.join(downloadDir, 'iren-remi-parsed.csv');
      const { outputFileName } = await parseXlsxToCsv(xlsFilePath, parsedCsvPath);
      myLogger.info(`creato file csv ${outputFileName}`);

      myLogger.info('elimino i dati attuali');
      await mysqlPool.query('DELETE FROM ??;', [tableName]);

      myLogger.info('resetto l\'id');
      await mysqlPool.query('ALTER TABLE ?? AUTO_INCREMENT = 1;', [tableName]);

      myLogger.info('invio al database il csv');
      await mysqlPool.query(sql, [outputFileName, tableName]);

      // Commit della transaction
      myLogger.info('commit');
      await mysqlPool.query('COMMIT');

      myLogger.info('svuoto la cartella di download');
      emptyDirectory(downloadDir);

      res.json(new SuccessResponse());
    } catch (ex) {
      const errMsg = extractErrorMessage(ex);
      myLogger.error(errMsg);
      myLogger.info('rollback delle modifiche');
      await mysqlPool.query('ROLLBACK');

      res.json(new FailureResponse(errMsg));
    }
  });
}
