import path from 'path';
import { Router } from 'express';
import { unlinkSync } from 'fs';
import _ from 'lodash';
import multer from 'multer';
import csv from 'csvtojson';
import { findBestMatch } from 'string-similarity';
import { Comune, ComuniRepo } from '../repo/ComuniRepo';
import { AtenaRepo } from '../repo/AtenaRepo';
import { AtenaCsvRow } from '../repo/types';

type AtenaCsvImporterOptions = {
  router: Router;
  comuniRepo: ComuniRepo;
  atenaRepo: AtenaRepo;
  baseDownloadPath: string;
};

export default function atenaCsvImporter({
  router,
  comuniRepo,
  atenaRepo,
  baseDownloadPath
}: AtenaCsvImporterOptions): void {
  const upload = multer({ dest: path.resolve(baseDownloadPath) });

  router.post('/import/atena-csv', upload.single('csv'), (req, res) => {
    console.log(req.file?.path);
    const report: any[] = [];
    csv().fromFile(req.file?.path as string).then((data: AtenaCsvRow[]) => {
      const cleanData = _.flatMap(data, (row: AtenaCsvRow): AtenaCsvRow[] => {
        const allComuni = comuniRepo.allComuni();
        const comunePerProvincia: Comune[] = allComuni.filter((comune) => comune.sigla.toLowerCase() === row.Provincia.toLowerCase());

        const match = findBestMatch(row.Comune.toLowerCase(), comunePerProvincia.map((c) => c.nome.toLowerCase()));

        const comune = comunePerProvincia.find((c) => c.nome.toLowerCase() == match.bestMatch.target);

        if (!comune) {
          report.push({
            row,
            message: 'Impossibile trovare corrispondenza',
            comunePerProvincia: comunePerProvincia.map((c) => c.nome)
          });
          return [];
        }
        return [{
          ...row,
          Comune: comune.nome || row.Comune,
        }];
      });

      return atenaRepo.import(cleanData);
    }, console.error).then(() => {
      unlinkSync(req.file?.path as string);
      res.json({
        status: 'FINISHED',
        errors: report
      });
    });
  });
}
