import path from 'path';
import { Router } from 'express';
import { unlinkSync } from 'fs';
import flatMap from 'lodash/flatMap';
import multer from 'multer';
import csv from 'csvtojson';
import { findBestMatch } from 'string-similarity';
import { Comune, ComuniRepo } from '../repo/ComuniRepo';
import IrenRemiRepo from '../repo/IrenRemiRepo';
// import { cleanString } from '../utils';
import { IrenRemiRow } from '../repo/types';
import getCurrentDayFormatted from '../utils/getCurrentDayFormatted';
import { Logger } from '../logger';
import createChildLogger from '../utils/createChildLogger';
import extractErrorMessage from '../utils/extractErrorMessage';
import FailureResponse from '../response/FailureResponse';

type IrenRemiCsvImporterOptions = {
  router: Router;
  comuniRepo: ComuniRepo;
  irenRemiRepo: IrenRemiRepo;
  baseDownloadPath: string;
  logger: Logger;
};

export default function irenRemiCsvImporter({
  router,
  comuniRepo,
  irenRemiRepo,
  baseDownloadPath,
  logger
}: IrenRemiCsvImporterOptions): void {
  const childLogger = createChildLogger(logger, 'IrenRemiImportCsv');
  const downloadDir = path.resolve(baseDownloadPath, getCurrentDayFormatted(), 'import', 'iren-remi');
  const upload = multer({ dest: downloadDir });

  router.post('/import/iren-remi-csv', upload.single('csv'), async (req, res) => {
    const report: any[] = [];
    let errMsg: string = '';
    try {
      childLogger.info(`Arrivato file ${req.file?.path}`);
      const data: IrenRemiRow[] = await csv({ delimiter: ';' }).fromFile(req.file?.path as string);

      childLogger.info('elaboro i dati prima di mandarli al database');
      const cleanData = flatMap<IrenRemiRow, IrenRemiRow>(data, (row: IrenRemiRow): IrenRemiRow[] => {
        const allComuni = comuniRepo.allComuni();
        const comunePerProvincia: Comune[] = allComuni.filter((comune) => comune.sigla.toLowerCase() === row.PROVINCIA.toLowerCase());

        const comuniPerCap = allComuni.filter((comune) => comune.cap.includes(row.CAP.padStart(5, '0')));

        // const matchConNomeComune = (comune: Comune): boolean => comune.nome.toLowerCase().includes(cleanString(row.COMUNE.toLowerCase()));

        const comuniPerMatch = [...comuniPerCap, ...comunePerProvincia];

        const match = findBestMatch(row.COMUNE.toLowerCase(), comuniPerMatch.map((c) => c.nome.toLowerCase()));

        const comune = comuniPerMatch.find((c) => c.nome.toLowerCase() == match.bestMatch.target);

        if (!comune) {
          report.push({
            row, message: 'Impossibile trovare corrispondenza', comuniPerCap, comunePerProvincia: comunePerProvincia.map((c) => c.nome)
          });
          return [];
        }
        return [{
          ...row,
          COMUNE: comune.nome || row.COMUNE,
          CAP: comune.cap[0] || row.CAP
        }];
      });

      childLogger.info('salvo i dati su database');

      await irenRemiRepo.importV2(cleanData);
    } catch (ex) {
      errMsg = extractErrorMessage(ex);
    } finally {
      childLogger.info('elimino il file non più necessario');
      unlinkSync(req.file?.path as string);
    }

    if (errMsg) {
      childLogger.error(errMsg);
      res.json(new FailureResponse(errMsg));
    } else {
      if (report.length) {
        childLogger.error(`finito con ${report.length} errori`);
      } else {
        childLogger.info('finito');
      }

      res.json({
        status: 'FINISHED',
        errors: report
      });
    }
  });
}
