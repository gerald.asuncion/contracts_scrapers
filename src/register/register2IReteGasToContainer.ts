import { asClass, AwilixContainer } from "awilix";
import { IConfig } from "config";
import DueIReteGas from "../scraperv2/due-i-retegas/DueIReteGas";
import DueIReteGasV3 from "../v3/due-i-rete-gas/DueIReteGas.scraper";

export default function register2IReteGasToContainer(container: AwilixContainer, config: IConfig) {
  container.register({
    dueIReteGas: asClass(DueIReteGas).scoped(),
    dueIReteGasV3: asClass(DueIReteGasV3).scoped(),
  });
}
