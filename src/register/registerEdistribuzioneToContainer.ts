import { asClass, asFunction, AwilixContainer } from "awilix";
import { IConfig } from "config";
import EDistribuzionePodChecker from "../scraperv2/eDistribuzione/EDistribuzionePodChecker";
import eDistribuzionePodCheckerValidator from "../validator/eDistribuzionePodCheckerValidator";
import EDistribuzione from "../scraperv2/e-distribuzione";
import EdistribuzioneCheckPod from "../v3/e-distribuzione/EdistribuzioneCheckPod.scraper";

export default function registerEdistribuzioneToContainer(container: AwilixContainer, config: IConfig) {
  container.register({
    eDistribuzionePodChecker: asClass(EDistribuzionePodChecker).scoped(),
    eDistribuzionePodCheckerValidator: asFunction(eDistribuzionePodCheckerValidator).singleton(),
    eDistribuzione: asClass(EDistribuzione).scoped(),
    eDistribuzioneCheckPod: asClass(EdistribuzioneCheckPod).scoped(),
  });
}
