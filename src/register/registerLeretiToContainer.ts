import { asClass, asFunction, AwilixContainer } from "awilix";
import { IConfig } from "config";
import LeretiCheckPdr from "../scraperv2/lereti/LeretiCheckPdr";
import LeretiCheckPdrV3 from "../v3/lereti/LeretiCheckPdr.scraper";
import leretiCheckPdrValidator from "../validator/leretiCheckPdrValidator";

export default function registerLeretiToContainer(container: AwilixContainer, config: IConfig) {
  container.register({
    leretiCheckPdr: asClass(LeretiCheckPdr).scoped(),
    leretiCheckPdrValidator: asFunction(leretiCheckPdrValidator).singleton(),
    leretiCheckPdrV3: asClass(LeretiCheckPdrV3).scoped(),
  });
}
