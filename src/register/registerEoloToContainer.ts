import { asClass, asFunction, asValue, AwilixContainer } from "awilix";
import { IConfig } from "config";
import EoloErrorMailer from '../task/EoloErrorMailer';
import Eolo from '../scraperv2/eolo/Eolo';
import eoloValidator from '../validator/eoloValidator';
// import EoloCheck from "../scraperv2/eolo/EoloCheck";
// import eoloCheckValidator from "../validator/eoloCheckValidator";
import EoloCheckFirmeV2 from "../scraperv2/eolo/EoloCheckFirmeV2";
import eoloCheckFirmeV2Validator from "../validator/eoloCheckFirmeV2Validator";
import EoloVerificaCopertura from "../scraperv2/eolo/EoloVerificaCopertura";
import eoloVerificaCoperturaValidator from "../validator/eoloVerificaCoperturaValidator";

export default function registerEoloToContainer(container: AwilixContainer, config: IConfig) {
  container.register({
    eoloMailingList: asValue<string>(config.get('eoloMailingList')),
    eolo: asClass(Eolo).scoped(),
    eoloValidator: asFunction(eoloValidator).singleton(),
    eoloErrorMailer: asClass(EoloErrorMailer).singleton(),
    eoloCheckFirme: asClass(EoloCheckFirmeV2).scoped(),
    eoloCheckFirmeValidator: asFunction(eoloCheckFirmeV2Validator).singleton(),
    eoloVerificaCopertura: asClass(EoloVerificaCopertura).scoped(),
    eoloVerificaCoperturaValidator: asFunction(eoloVerificaCoperturaValidator).singleton(),
  });
}
