import { asClass, AwilixContainer } from "awilix";
import { IConfig } from "config";
import Erogasmet from "../scraperv2/terranova/erogasmet";
import ErogasmetV3 from "../v3/erogasmet/Erogasmet.scraper";

export default function registerErogasmetToContainer(container: AwilixContainer, config: IConfig) {
  container.register({
    erogasmet: asClass(Erogasmet).scoped(),
    erogasmetV3: asClass(ErogasmetV3).scoped(),
  });
}
