import { asClass, asFunction, AwilixContainer } from "awilix";
import { IConfig } from "config";
import { ComuniRepo } from "../repo/ComuniRepo";
import pdrValidator from "../scrapers/pdr-validator";
import podValidator from "../scrapers/pod-validator";

export default function registerCommonsToContainer(container: AwilixContainer, config: IConfig) {
  container.register({
    pdrValidator: asFunction(pdrValidator).singleton(),
    podValidator: asFunction(podValidator).singleton(),
    comuniRepo: asClass(ComuniRepo).singleton(),
  });
}
