import { asClass, asFunction, asValue, AwilixContainer } from "awilix";
import { IConfig } from "config";
import IrenErrorMailer, { IrenLinkemErrorMailer } from '../task/IrenErrorMailer';
import IrenLinkem from '../scraperv2/iren/IrenLinkem';
import irenLinkemValidator from '../validator/irenLinkemValidator';
import IrenScraper from "../scraperv2/iren";
import irenValidator from "../validator/irenValidator";
// import IrenPreCheck from "../scraperv2/iren/IrenPreCheck";
// import irenPreCheckValidator from "../validator/IrenPreCheckValidator";
import IrenPreCheckV2 from "../scraperv2/iren/IrenPreCheckV2";
import irenPreCheckV2Validator from "../validator/irenPreCheckV2Validator";
import IrenSubentroGas from "../scraperv2/iren/IrenSubentroGas";
import IrenSwitchGas from "../scraperv2/iren/IrenSwitchGas";
import irenFattibilitaValidator from "../validator/irenFattibilitaValidator";
import IrenRemiRepo from "../repo/IrenRemiRepo";
import IrenPrecheckV3 from "../v3/iren/IrenPrecheck.scraper";
import irenPrecheckV3Validator from "../validator/irenPrecheckV3Validator";

export default function registerIrenToContainer(container: AwilixContainer, config: IConfig) {
  container.register({
    irenRemiRepo: asClass(IrenRemiRepo).singleton(),
    irenMailingList: asValue<Array<string>>(config.get('irenMailingList')),
    irenErrorMailer: asClass(IrenErrorMailer).singleton(),
    irenLinkemErrorMailer: asClass(IrenLinkemErrorMailer).singleton(),
    irenLinkem: asClass(IrenLinkem).scoped(),
    irenLinkemValidator: asFunction(irenLinkemValidator).singleton(),
    iren: asClass(IrenScraper).scoped(),
    irenValidator: asFunction(irenValidator).singleton(),
    irenPreCheck: asClass(IrenPreCheckV2).scoped(),
    irenPreCheckValidator: asFunction(irenPreCheckV2Validator).singleton(),
    irenSubentroGas: asClass(IrenSubentroGas).scoped(),
    irenSwitchGas: asClass(IrenSwitchGas).scoped(),
    // usato sia da irenSubentroGas che da irenSwitchGas
    irenFattibilitaValidator: asFunction(irenFattibilitaValidator).singleton(),
    irenPrecheckV3: asClass(IrenPrecheckV3).scoped(),
    irenPrecheckV3Validator: asFunction(irenPrecheckV3Validator).singleton(),
  });
}
