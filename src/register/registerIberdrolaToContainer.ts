import { asClass, asFunction, asValue, AwilixContainer } from "awilix";
import { IConfig } from "config";
import IberdrolaErrorMailer, { IberdrolaV2ErrorMailer } from '../task/IberdrolaErrorMailer';
// import Iberdrola from '../scraperv2/iberdrola/Iberdrola';
// import iberdrolaValidator from '../validator/iberdrolaValidator';
import IberdrolaV2 from '../scraperv2/iberdrolaV2/IberdrolaInserimentoV2';
import iberdrolaV2Validator from '../validator/iberdrolaV2Validator';
import IberdrolaCheckFirmeV2 from "../scraperv2/iberdrolaV2/IberdrolaCheckFirmeV2";
import iberdrolaCheckFirmeV2Validator from "../validator/iberdrolaCheckFirmeV2Validator";
import IberdrolaCreditCheckV2 from "../scraperv2/iberdrolaV2/IberdrolaCreditCheckV2";
import iberdrolaCreditCheckV2Validator from "../validator/iberdrolaCreditCheckV2Validator";
import IberdrolaDistributoriGasV2 from "../scraperv2/iberdrolaV2/IberdrolaDistributoriGasV2";
import IberdrolaDistributoriLuceV2 from "../scraperv2/iberdrolaV2/IberdrolaDistributoriLuceV2";
import IberdrolaFornitoriGasV2 from "../scraperv2/iberdrolaV2/IberdrolaFornitoriGasV2";
import IberdrolaFornitoriLuceV2 from "../scraperv2/iberdrolaV2/IberdrolaFornitoriLuceV2";

export default function registerIberdrolaToContainer(container: AwilixContainer, config: IConfig) {
  container.register({
    iberdrolaMailingList: asValue<string>(config.get('iberdrolaMailingList')),
    iberdrolaErrorMailer: asClass(IberdrolaErrorMailer).singleton(),
    iberdrolaV2: asClass(IberdrolaV2).scoped(),
    iberdrolaV2Validator: asFunction(iberdrolaV2Validator).singleton(),
    iberdrolaV2ErrorMailer: asClass(IberdrolaV2ErrorMailer).singleton(),
    iberdrolaCheckFirme: asClass(IberdrolaCheckFirmeV2).scoped(),
    iberdrolaCheckFirmeValidator: asFunction(iberdrolaCheckFirmeV2Validator).singleton(),
    iberdrolaCreditCheck: asClass(IberdrolaCreditCheckV2).scoped(),
    iberdrolaCreditCheckValidator: asFunction(iberdrolaCreditCheckV2Validator).singleton(),
    iberdrolaDistributoriGas: asClass(IberdrolaDistributoriGasV2).scoped(),
    iberdrolaDistributoriLuce: asClass(IberdrolaDistributoriLuceV2).scoped(),
    iberdrolaFornitoriGas: asClass(IberdrolaFornitoriGasV2).scoped(),
    iberdrolaFornitoriLuce: asClass(IberdrolaFornitoriLuceV2).scoped(),
  });
}
