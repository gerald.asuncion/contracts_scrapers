import { asClass, asFunction, AwilixContainer } from "awilix";
import { IConfig } from "config";
import AgsmGasCopertura from "../scraperv2/agsm/gas/AgsmGasCopertura";
import AgsmPrecheck from "../scraperv2/agsm/precheck/AgsmPrecheck";
import agsmGasCoperturaValidator from "../validator/agsmGasCoperturaValidator";
import agsmPrecheckValidator from "../validator/agsmPrecheckValidator";

export default function registerAgsmToContainer(container: AwilixContainer, config: IConfig) {
  container.register({
    agsmGasCopertura: asClass(AgsmGasCopertura).scoped(),
    agsmGasCoperturaValidator: asFunction(agsmGasCoperturaValidator).singleton(),
    agsmPrecheck: asClass(AgsmPrecheck).scoped(),
    agsmPrecheckValidator: asFunction(agsmPrecheckValidator).singleton(),
  });
}
