import { asClass, asFunction, AwilixContainer } from "awilix";
import { IConfig } from "config";
import WindCheckCopertura from "../scraperv2/wind/WindCheckCopertura";
import windCheckCoperturaValidator from "../validator/windCheckCoperturaValidator";
import WindVerificaMorosita from "../scraperv2/wind/WindVerificaMorosita";
import windVerificaMorositaValidator from "../validator/windVerificaMorositaValidator";
import WindVerificaMorositaV3 from "../v3/wind/WindVerificaMorosita.scraper";
import windVerificaMorositaV3Validator from "../validator/windVerificaMorositaV3Validator";

export default function registerWindToContainer(container: AwilixContainer, config: IConfig) {
  container.register({
    windCheckCopertura: asClass(WindCheckCopertura).scoped(),
    windCheckCoperturaValidator: asFunction(windCheckCoperturaValidator).singleton(),
    windVerificaMorosita: asClass(WindVerificaMorosita).scoped(),
    windVerificaMorositaValidator: asFunction(windVerificaMorositaValidator).singleton(),
    windVerificaMorositaV3: asClass(WindVerificaMorositaV3).scoped(),
    windVerificaMorositaV3Validator: asFunction(windVerificaMorositaV3Validator).singleton(),
  });
}
