import { asClass, asFunction, asValue, AwilixContainer } from "awilix";
import { IConfig } from "config";
import Eni from '../scraperv2/eni/Eni';
import EniEstrattoreAteco from '../scraperv2/eni/EniEstrattoreAteco';
import EniResidenzialeInserimento from '../scraperv2/eni/EniResidenzialeInserimento';
import eniValidator from '../validator/eniValidator';
import eniResidenzialeInserimentoValidator from '../validator/eniResidenzialeInserimentoValidator';
import EniResidenzialeInserimentoErrorMailer from "../task/EniResidenzialeInserimentoErrorMailer";
import EniErrorMailer from '../task/EniErrorMailer';
import EniCodiciAtecoRepo from '../repo/EniCodiciAtecoRepo';
import EniCheck from "../scraperv2/eni/EniCheck";
import EniCheckFattiblita from "../scraperv2/eni/EniCheckFattibilita";
import eniCheckFattiblitaValidator from "../validator/eniCheckFattiblitaValidator";
import EniCodiciAteco from "../scraperv2/eni/EniCodiciAteco";
import EniFormeGiuridiche from "../scraperv2/eni/EniFormeGiuridiche";
import EniFornitoriGas from "../scraperv2/eni/EniFornitoriGas";
import EniFornitoriLuce from "../scraperv2/eni/EniFornitoriLuce";
import EniSettoriMerceologici from "../scraperv2/eni/EniSettoriMerceologici";
import EniAttivitaMerceologiche from "../scraperv2/eni/EniAttivitaMerceologiche";

export default function registerEniToContainer(container: AwilixContainer, config: IConfig) {
  container.register({
    eniMailingList: asValue<string>(config.get('eniMailingList')),
    eni: asClass(Eni).scoped(),
    eniValidator: asFunction(eniValidator).singleton(),
    eniErrorMailer: asClass(EniErrorMailer).singleton(),
    eniCodiciAtecoRepo: asClass(EniCodiciAtecoRepo).singleton(),
    eniEstrattoreAteco: asClass(EniEstrattoreAteco).scoped(),
    eniResidenziale: asClass(EniResidenzialeInserimento).scoped(),
    eniResidenzialeValidator: asFunction(eniResidenzialeInserimentoValidator).singleton(),
    eniResidenzialeErrorMailer: asClass(EniResidenzialeInserimentoErrorMailer).singleton(),
    eniCheckFirme: asClass(EniCheck).scoped(),
    eniCheckFattibilita: asClass(EniCheckFattiblita).scoped(),
    eniCheckFattibilitaValidator: asFunction(eniCheckFattiblitaValidator).singleton(),
    eniCodiciAteco: asClass(EniCodiciAteco).scoped(),
    eniFormeGiuridiche: asClass(EniFormeGiuridiche).scoped(),
    eniFornitoriGas: asClass(EniFornitoriGas).scoped(),
    eniFornitoriLuce: asClass(EniFornitoriLuce).scoped(),
    eniSettoriMerceologici: asClass(EniSettoriMerceologici).scoped(),
    eniAttivitaMerceologiche: asClass(EniAttivitaMerceologiche).scoped(),
  });
}
