import { asClass, asFunction, asValue, AwilixContainer } from "awilix";
import { IConfig } from "config";
import FastwebErrorMailer from '../task/FastwebErrorMailer';
import Fastweb from '../scraperv2/fastweb/Fastweb';
import fastwebValidator from '../validator/fastwebValidator';
import fastwebPostFirmaValidator from '../validator/fastwebPostFirmaValidator';
import FastwebPostFirmaErrorMailer from '../task/FastwebPostFirmaErrorMailer';
import FastwebPostFirma from '../scraperv2/fastweb/FastwebPostFirma';
import FastwebCheck from "../scraperv2/fastweb/FastwebCheck";
import fastwebCheckValidator from "../validator/fastwebCheckValidator";
import FastwebCheckFattibilita from "../scraperv2/fastweb/FastwebCheckFattibilita";
import fastwebCheckFattibilitaValidator from "../validator/fastwebCheckFattibilitaValidator";
import FastwebFornitoriDati from "../scraperv2/fastweb/FastwebFornitoriDati";
import FastwebFornitoriInternet from "../scraperv2/fastweb/FastwebFornitoriInternet";
import FastwebFornitoriMobile from "../scraperv2/fastweb/FastwebFornitoriMobile";
import FastwebVerificaCopertura from "../scraperv2/fastweb/FastwebVerificaCopertura";
import fastwebVerificaCoperturaValidator from "../validator/fastwebVerificaCoperturaValidator";
import FastwebVerificaDocumento from "../scraperv2/fastweb/FastwebVerificaDocumento";
import fastwebVerificaDocumentoValidator from "../validator/fastwebVerificaDocumentoValidator";
import FastwebVerificaMorosita from "../scraperv2/fastweb/FastwebVerificaMorosita";
import fastwebVerificaMorositaValidator from "../validator/fastwebVerificaMorositaValidator";

export default function registerFastwebToContainer(container: AwilixContainer, config: IConfig) {
  container.register({
    fastwebMailingList: asValue<Array<string>>(config.get('fastwebMailingList')),
    fastweb: asClass(Fastweb).scoped(),
    fastwebValidator: asFunction(fastwebValidator).singleton(),
    fastwebErrorMailer: asClass(FastwebErrorMailer).singleton(),
    fastwebPostFirma: asClass(FastwebPostFirma).scoped(),
    fastwebPostFirmaValidator: asFunction(fastwebPostFirmaValidator).singleton(),
    fastwebPostFirmaErrorMailer: asClass(FastwebPostFirmaErrorMailer).singleton(),
    fastwebCheckFirme: asClass(FastwebCheck).scoped(),
    fastwebCheckFirmeValidator: asFunction(fastwebCheckValidator).singleton(),
    fastwebCheckFattibilita: asClass(FastwebCheckFattibilita).scoped(),
    fastwebCheckFattibilitaValidator: asFunction(fastwebCheckFattibilitaValidator).singleton(),
    fastwebFornitoriDati: asClass(FastwebFornitoriDati).scoped(),
    fastwebFornitoriInternet: asClass(FastwebFornitoriInternet).scoped(),
    fastwebFornitoriMobile: asClass(FastwebFornitoriMobile).scoped(),
    fastwebVerificaCopertura: asClass(FastwebVerificaCopertura).scoped(),
    fastwebVerificaCoperturaValidator: asFunction(fastwebVerificaCoperturaValidator).singleton(),
    fastwebVerificaDocumento: asClass(FastwebVerificaDocumento).scoped(),
    fastwebVerificaDocumentoValidator: asFunction(fastwebVerificaDocumentoValidator).singleton(),
    fastwebVerificaMorosita: asClass(FastwebVerificaMorosita).scoped(),
    fastwebVerificaMorositaValidator: asFunction(fastwebVerificaMorositaValidator).singleton(),
  });
}
