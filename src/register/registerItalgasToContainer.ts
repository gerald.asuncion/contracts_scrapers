import { asClass, AwilixContainer } from "awilix";
import { IConfig } from "config";
import Italgas from "../scraperv2/italgas/italgas";
import ItalgasAttivazione from "../scraperv2/italgas/italgas-attivazione";
import ItalgasSantangeloLodigiano from '../scraperv2/italgas/italgas-santangelo-lodigiano';
import ItalgasToscana from "../scraperv2/italgas/italgas-toscana";
import ItalgasUmbriaDistribuzione from "../scraperv2/italgas/italgas-umbria-distribuzione";
import ItalgasContendibilitaGas from "../scraperv2/italgas/italgasContendibilitaGas";
import ItalgasV3 from "../v3/italgas/italgas.scraper";
import ItalgasAttivazioneV3 from "../v3/italgas/italgasAttivazione.scraper";
import ItalgasContendibilitaGasV3 from "../v3/italgas/italgasContendibilitaGas.scraper";
import ItalgasToscanaV3 from "../v3/italgas/italgasToscana.scraper";

export default function registerItalgasToContainer(container: AwilixContainer, config: IConfig) {
  container.register({
    italgas: asClass(Italgas).scoped(),
    italgasAttivazione: asClass(ItalgasAttivazione).scoped(),
    italgasSantangeloLodigiano: asClass(ItalgasSantangeloLodigiano).scoped(),
    italgasToscana: asClass(ItalgasToscana).scoped(),
    italgasUmbriaDistribuzione: asClass(ItalgasUmbriaDistribuzione).scoped(),
    italgasContendibilitaGas: asClass(ItalgasContendibilitaGas).scoped(),
    italgasV3: asClass(ItalgasV3).scoped(),
    italgasAttivazioneV3: asClass(ItalgasAttivazioneV3).scoped(),
    italgasToscanaV3: asClass(ItalgasToscanaV3).scoped(),
    italgasContendibilitaGasV3: asClass(ItalgasContendibilitaGasV3).scoped(),
  });
}
