import { AwilixContainer } from "awilix";
import { IConfig } from "config";
import register2IReteGasToContainer from "./register2IReteGasToContainer";
import registerAgsmToContainer from "./registerAgsmToContainer";
import registerAreraToContainer from "./registerAreraToContainer";
import registerAretiToContainer from "./registerAretiToContainer";
import registerAtenaToContainer from "./registerAtenaToContainer";
import registerCentriaToContainer from "./registerCentriaToContainer";
import registerCommonsToContainer from "./registerCommonsToContainer";
import registerCovercareToContainer from "./registerCovercareToContainer";
import registerEdisonToContainer from "./registerEdisonToContainer";
import registerEdistribuzioneToContainer from "./registerEdistribuzioneToContainer";
import registerEniToContainer from "./registerEniToContainer";
import registerEoloToContainer from "./registerEoloToContainer";
import registerErogasmetToContainer from "./registerErogasmetToContainer";
import registerFastwebToContainer from "./registerFastwebToContainer";
import registerIberdrolaToContainer from "./registerIberdrolaToContainer";
import registerIrenToContainer from "./registerIrenToContainer";
import registerIretiToContainer from "./registerIretiToContainer";
import registerItalgasToContainer from "./registerItalgasToContainer";
import registerLeretiToContainer from "./registerLeretiToContainer";
import registerUnaretiToContainer from "./registerUnaretiToContainer";
import registerWindToContainer from "./registerWindToContainer";

const functions = [
  registerCommonsToContainer,
  registerEniToContainer,
  registerEoloToContainer,
  registerIberdrolaToContainer,
  registerFastwebToContainer,
  registerAretiToContainer,
  registerEdisonToContainer,
  registerCentriaToContainer,
  registerCovercareToContainer,
  register2IReteGasToContainer,
  registerEdistribuzioneToContainer,
  registerIrenToContainer,
  registerIretiToContainer,
  registerItalgasToContainer,
  registerLeretiToContainer,
  registerErogasmetToContainer,
  registerUnaretiToContainer,
  registerAreraToContainer,
  registerWindToContainer,
  registerAtenaToContainer,
  registerAgsmToContainer,
];

const registerScrapersToContainer = (container: AwilixContainer, config: IConfig) => functions.forEach(fn => fn(container, config))

export default registerScrapersToContainer;
