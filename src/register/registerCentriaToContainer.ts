import { asClass, AwilixContainer } from "awilix";
import { IConfig } from "config";
import CentriaPdrCheck from "../scraperv2/centria/CentriaPdrCheck";
import CentriaPdrCheckV3 from "../v3/centria/CentriaPdrCheck.scraper";

export default function registerCentriaToContainer(container: AwilixContainer, config: IConfig) {
  container.register({
    centriaPdrCheck: asClass(CentriaPdrCheck).scoped(),
    centriaPdrCheckV3: asClass(CentriaPdrCheckV3).scoped(),
  });
}
