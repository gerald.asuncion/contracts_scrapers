import { asClass, AwilixContainer } from "awilix";
import { IConfig } from "config";
import AtenaCoperturaGas from "../scraperv2/atena/AtenaCoperturaGas";
import AtenaCoperturaGasSwitch from "../scraperv2/atena/AtenaCoperturaGasSwitch";
import AtenaCoperturaLuce from "../scraperv2/atena/AtenaCoperturaLuce";
import { AtenaRepo } from "../repo/AtenaRepo";

export default function registerAtenaToContainer(container: AwilixContainer, config: IConfig) {
  container.register({
    atenaRepo: asClass(AtenaRepo).singleton(),
    atenaCoperturaGas: asClass(AtenaCoperturaGas).scoped(),
    atenaCoperturaGasSwitch: asClass(AtenaCoperturaGasSwitch).scoped(),
    atenaCoperturaLuce: asClass(AtenaCoperturaLuce).scoped(),
  });
}
