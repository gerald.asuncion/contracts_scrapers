import { asClass, asFunction, AwilixContainer } from "awilix";
import { IConfig } from "config";
import IretiCheckPdr from "../scraperv2/ireti/IretiCheckPdr";
import iretiCheckPdrValidator from "../validator/iretiCheckPdrValidator";
import IretiCheckPod from "../scraperv2/ireti/IretiCheckPod";
import iretiCheckPodValidator from "../validator/iretiCheckPodValidator";
import IretiCheckPodV3 from "../v3/ireti/IretiCheckPod.scraper";
import iretiCheckPodV3Validator from "../validator/iretiCheckPodV3Validator";
import IretiCheckPdrV3 from "../v3/ireti/IretiCheckPdr.scraper";
import iretiCheckPdrV3Validator from "../validator/iretiCheckPdrV3Validator";

export default function registerIretiToContainer(container: AwilixContainer, config: IConfig) {
  container.register({
    iretiCheckPdr: asClass(IretiCheckPdr).scoped(),
    iretiCheckPdrValidator: asFunction(iretiCheckPdrValidator).singleton(),
    iretiCheckPod: asClass(IretiCheckPod).scoped(),
    iretiCheckPodValidator: asFunction(iretiCheckPodValidator).singleton(),
    iretiCheckPodV3: asClass(IretiCheckPodV3).scoped(),
    iretiCheckPodV3Validator: asFunction(iretiCheckPodV3Validator).singleton(),
    iretiCheckPdrV3: asClass(IretiCheckPdrV3).scoped(),
    iretiCheckPdrV3Validator: asFunction(iretiCheckPdrV3Validator).singleton(),
  });
}
