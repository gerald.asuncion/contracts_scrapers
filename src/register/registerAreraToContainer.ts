import { asClass, asFunction, AwilixContainer } from "awilix";
import { IConfig } from "config";
import AreraRepo from "../repo/AreraRepo";
import AreraV3 from "../v3/arera/Arera.scraper";
import AreraCheckDistributoriGas from "../scraperv2/arera/AreraCheckDistributoriGas";
import areraCheckDistributoriGasValidator from "../validator/areraCheckDistributoriGasValidator";
import areraValidator from "../validator/areraValidator";

export default function registerAreraToContainer(container: AwilixContainer, config: IConfig) {
  container.register({
    areraRepo: asClass(AreraRepo).singleton(),
    areraCheckDistributoriGas: asClass(AreraCheckDistributoriGas).scoped(),
    areraCheckDistributoriGasValidator: asFunction(areraCheckDistributoriGasValidator).singleton(),
    arera: asClass(AreraV3).scoped(),
    areraValidator: asFunction(areraValidator).singleton(),
  });
}
