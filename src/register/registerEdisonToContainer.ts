import { asClass, asFunction, asValue, AwilixContainer } from "awilix";
import { IConfig } from "config";
import EdisonErrorMailer from '../task/EdisonErrorMailer';
import edisonValidator from '../validator/edisonV2Validator';
import EdisonInserimentoV2 from '../scraperv2/edisonV2/EdisonInserimentoV2';
import EdisonCheckFirme from "../scraperv2/edisonV2/EdisonCheckFirmeV2";
import EdisonFornitoriGasV2 from "../scraperv2/edisonV2/EdisonFornitoriGasV2";
import EdisonFornitoriLuceV2 from "../scraperv2/edisonV2/EdisonFornitoriLuceV2";
import EdisonFattibilita from "../scraperv2/edison/EdisonFattibilita";

export default function registerEdisonToContainer(container: AwilixContainer, config: IConfig) {
  container.register({
    edisonMailingList: asValue<string>(config.get('edisonMailingList')),
    edison: asClass(EdisonInserimentoV2).scoped(),
    edisonValidator: asFunction(edisonValidator).singleton(),
    edisonErrorMailer: asClass(EdisonErrorMailer).singleton(),
    edisonCheckFirme: asClass(EdisonCheckFirme).scoped(),
    edisonFornitoriGas: asClass(EdisonFornitoriGasV2).scoped(),
    edisonFornitoriLuce: asClass(EdisonFornitoriLuceV2).scoped(),
    edisonFattibilita: asClass(EdisonFattibilita).scoped(),
  });
}
