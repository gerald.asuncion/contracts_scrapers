import { asClass, asFunction, AwilixContainer } from "awilix";
import { IConfig } from "config";
import AretiContendibilitaChecker from '../scraperv2/areti/AretiContendibilitaChecker';
import { AretiRepo } from '../repo/AretiRepo';
import AretiPodChecker from "../scraperv2/areti/AretiPodChecker";
import aretiPodCheckerValidator from "../validator/aretiPodCheckerValidator";

export default function registerAretiToContainer(container: AwilixContainer, config: IConfig) {
  container.register({
    aretiRepo: asClass(AretiRepo).singleton(),
    areti: asClass(AretiContendibilitaChecker).scoped(),
    aretiCheckPod: asClass(AretiPodChecker).scoped(),
    aretiCheckPodValidator: asFunction(aretiPodCheckerValidator).singleton(),
  });
}
