import { asClass, asFunction, AwilixContainer } from "awilix";
import { IConfig } from "config";
// import UnaretiGas from "../scraperv2/unareti/unareti-gas";
// import UnaretiLuceNuovaAttivazione from "../scraperv2/unareti/unareti-luce-nuova-attivazione";
// import UnaretiLuce from "../scraperv2/unareti/unareti-luce";
import UnaretiRepo from "../repo/UnaretiRepo";
import UnaretiContendibilitaElettricitaChecker from "../scraperv2/unareti/UnaretiContendibilitaElettricitaChecker";
import UnaretiContendibilitaGasChecker from "../scraperv2/unareti/UnaretiContendibilitaGasChecker";
import UnaretiPdrChecker from "../scraperv2/unareti/UnaretiPdrChecker";
import unaretiPdrCheckerValidator from "../validator/unaretiPdrCheckerValidator";
import UnaretiPodChecker  from "../scraperv2/unareti/UnaretiPodChecker";
import unaretiPodCheckerValidator from "../validator/unaretiPodCheckerValidator";
import UnaretiCheckPdrV3 from "../v3/unareti/UnaretiCheckPdrV3";
import pdrValidator from "../scrapers/pdr-validator";
import UnaretiCheckPodV3 from "../v3/unareti/UnaretiCheckPodV3";
import podValidator from "../scrapers/pod-validator";

export default function registerUnaretiToContainer(container: AwilixContainer, config: IConfig) {
  container.register({
    unaretiRepo: asClass(UnaretiRepo).singleton(),
    unaretiContendibilitaElettricitaChecker: asClass(UnaretiContendibilitaElettricitaChecker).scoped(),
    unaretiContendibilitaGasChecker: asClass(UnaretiContendibilitaGasChecker).scoped(),
    unaretiPdrChecker: asClass(UnaretiPdrChecker).scoped(),
    unaretiPdrCheckerValidator: asFunction(unaretiPdrCheckerValidator).singleton(),
    unaretiPodChecker: asClass(UnaretiPodChecker).scoped(),
    unaretiPodCheckerValidator: asFunction(unaretiPodCheckerValidator).singleton(),
    unaretiCheckPdrV3: asClass(UnaretiCheckPdrV3).scoped(),
    unaretiCheckPdrV3Validator: asFunction(pdrValidator).scoped(),
    unaretiCheckPodV3: asClass(UnaretiCheckPodV3).scoped(),
    unaretiCheckPodV3Validator: asFunction(podValidator).scoped(),
  });
}
