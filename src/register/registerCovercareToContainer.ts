import { asClass, asFunction, AwilixContainer } from "awilix";
import { IConfig } from "config";
import CovercareZoneDisagiateChecker from "../scraperv2/covercare/covercareZoneDisagiateChecker";
import covercareZoneDisagiateValidator from "../validator/covercareZoneDisagiateValidator";

export default function registerCovercareToContainer(container: AwilixContainer, config: IConfig) {
  container.register({
    covercareZoneDisagiateChecker: asClass(CovercareZoneDisagiateChecker).scoped(),
    covercareZoneDisagiateCheckerValidator: asFunction(covercareZoneDisagiateValidator).singleton(),
  });
}
