import { padStartDate } from './utils/pad';

export default function formatTimestamp(): string {
  const date = new Date();
  const first = [
    date.getFullYear(),
    padStartDate(date.getMonth() + 1),
    padStartDate(date.getDate())
  ].join('-');

  const second = [
    padStartDate(date.getHours()),
    padStartDate(date.getMinutes()),
    padStartDate(date.getSeconds())
  ].join(':');

  // YYYY-MM-dd HH:mm:ss
  return `${first} ${second}`;
}
