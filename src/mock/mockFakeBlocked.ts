import BlockedScraperResponse from '../response/BlockedScraperResponse';
import DbScraper from '../scraperv2/DbScraper';
import { ScraperResponse } from '../scraperv2/scraper';
import { SCRAPER_TIPOLOGIA } from '../scraperv2/ScraperOptions';

export default class MockFakeBlocked extends DbScraper {
  async scrape(): Promise<ScraperResponse> {
    return new BlockedScraperResponse('Timeout');
  }

  getScraperCodice() {
    return 'mock-fakeblocked';
  }

  getScraperTipologia() {
    return SCRAPER_TIPOLOGIA.FATTIBILITA;
  }
}
