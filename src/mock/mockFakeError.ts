import ErrorResponse from '../response/ErrorResponse';
import DbScraper from '../scraperv2/DbScraper';
import { ScraperResponse } from '../scraperv2/scraper';
import { SCRAPER_TIPOLOGIA } from '../scraperv2/ScraperOptions';

export default class MockFakeError extends DbScraper {
  async scrape(): Promise<ScraperResponse> {
    return new ErrorResponse('Un raggio cosmico ha colpito il server');
  }

  getScraperCodice() {
    return 'mock-fakerror';
  }

  getScraperTipologia() {
    return SCRAPER_TIPOLOGIA.FATTIBILITA;
  }
}
