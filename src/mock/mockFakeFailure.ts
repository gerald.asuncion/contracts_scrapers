import FailureResponse from '../response/FailureResponse';
import DbScraper from '../scraperv2/DbScraper';
import { ScraperResponse } from '../scraperv2/scraper';
import { SCRAPER_TIPOLOGIA } from '../scraperv2/ScraperOptions';

export default class MockFakeFailure extends DbScraper {
  async scrape(): Promise<ScraperResponse> {
    return new FailureResponse('POD/PDR - non contendibile');
  }

  getScraperCodice() {
    return 'mock-fakefailure';
  }

  getScraperTipologia() {
    return SCRAPER_TIPOLOGIA.FATTIBILITA;
  }
}
