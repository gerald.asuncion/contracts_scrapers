import { AwilixContainer } from 'awilix';
import {
  NextFunction, Request, Response, RequestHandler
} from 'express';
import RequestWithContainer from './RequestWithContainer';

export default function scopePerRequest(container: AwilixContainer): RequestHandler {
  return function scopePerRequestMiddleware(req: Request, res: Response, next: NextFunction) {
    (req as RequestWithContainer).container = container.createScope();
    return next();
  };
}
