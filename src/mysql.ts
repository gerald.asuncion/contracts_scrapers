import mysql, { Pool } from 'mysql';
import _ from 'lodash';

type MysqlPoolConfig = {
  mysqlConfig: mysql.PoolConfig & {
    username: string
  }
};

export class MysqlPool implements MysqlPoolInterface {
  pool: Pool;

  constructor({ mysqlConfig }: MysqlPoolConfig) {
    this.pool = mysql.createPool({
      host: mysqlConfig.host,
      port: mysqlConfig.port,
      user: mysqlConfig.username,
      password: mysqlConfig.password,
      database: mysqlConfig.database,
      waitForConnections: true,
      connectionLimit: 10,
      queueLimit: 0
    });
  }

  async query<TReturn = unknown>(sql: string, args: unknown[] = []): Promise<TReturn[]> {
    return this.queryAll<TReturn>(sql, args);
  }

  async queryAll<TReturn = unknown>(sql: string, args: unknown[] = []): Promise<TReturn[]> {
    return new Promise<TReturn[]>((resolve, reject) => {
      this.pool.query(sql, args, (error, results) => { // fields
        if (error) {
          reject(error);
        } else {
          resolve(JSON.parse(JSON.stringify(results)));
        }
      });
    });
  }

  async queryOne<TReturn = unknown>(sql: string, args: unknown[] = []): Promise<TReturn> {
    const results = await this.query<TReturn>(sql, args);
    if (results.length > 0) {
      return results[0];
    }
    throw 'no result';
  }

  async queryOneV2<TReturn = unknown>(sql: string, args: unknown[] = []): Promise<TReturn | null> {
    const results = await this.query<TReturn>(sql, args);

    if (results.length) {
      return results[0];
    }

    return null;
  }

  async truncateTable<TReturn = unknown>(table: string): Promise<TReturn[]> {
    return this.query<TReturn>('DELETE FROM ?? WHERE 1', [table]);
  }

  async insertIntoChunk<TReturn = unknown>(table: string, allData: string[][], chunk: number): Promise<TReturn[][]> {
    const columns = allData[0].map(() => '?').join(',');
    const placeholder = `(${columns})`;
    const chunks = _.chunk(allData, chunk);

    return await Promise.all(chunks.map((rows) => {
      const placeholders = rows.map(() => placeholder).join(',');
      const values = rows.reduce((res, row) => [...res, ...row], [table]);
      const sql = `INSERT INTO ?? VALUES ${placeholders}`;

      return this.queryAll<TReturn>(sql, values);
    }));
  }
}

export interface MysqlPoolInterface {
  queryAll<TReturn = unknown>(sql: string, args: unknown): Promise<TReturn[]>,
  query<TReturn = unknown>(sql: string, args: unknown): Promise<TReturn[]>,
  queryOne<TReturn = unknown>(sql: string, args: unknown): Promise<TReturn>,
  truncateTable<TReturn = unknown>(table: string): Promise<TReturn[]>,
  insertIntoChunk<TReturn = unknown>(table: string, allData: string[][], chunk: number): Promise<TReturn[][]>,
}

export interface MysqlArgs {
  mysqlPool: MysqlPoolInterface
}
