import { Logger } from '../logger';

export default function createChildLogger(parentLogger: Logger, componentName: string): Logger {
  return parentLogger.child({ componentName });
}
