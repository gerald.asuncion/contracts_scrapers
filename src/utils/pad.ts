export type PadOptions = {
  maxLength?: number;
  fillString?: string;
};

export function padStart(str: string | number, options?: PadOptions): string {
  return String(str).padStart(options?.maxLength || 2, options?.fillString || ' ');
}

export function padEnd(str: string | number, { maxLength = 2, fillString = ' ' }: PadOptions): string {
  return String(str).padEnd(maxLength, fillString);
}

export const padStartDate = (str: string | number): string => padStart(str, { fillString: '0' });
