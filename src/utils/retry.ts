import { logger, Logger } from '../logger';
import delay from "./delay";

// const { logger } = container.cradle;
// const logger = container.resolve<Logger>('logger')
// const logger = console; // (...args: any[]) => console.log(...args);

const {
  DEFAULT_RETRIES_TIMES = '5',
  DEFAULT_RETRIES_SETS = '5',
  DEFAULT_RETRIES_SETS_DELAY = '3000' // ms
} = process.env;

export async function retry<T>(
  cback: () => Promise<T>, {
    infoMessage = '',
    retryTimes = parseInt(DEFAULT_RETRIES_TIMES)
  }: {
    infoMessage?: string;
    retryTimes?: number
  } = {},
  innerLogger: Logger = logger
): Promise<T> {
  let result: T | undefined = undefined;
  const startingRetryTimes = retryTimes;

  while (retryTimes-- > 0) {
    try {
      result = await cback();
      break;
    } catch (err) {
      // TODO: logger; retry ${retries} err
      innerLogger.error(`[Utils::Retry] Failed n° ${startingRetryTimes - retryTimes + 1} retry: ${infoMessage}`);
      innerLogger.error(err);
    }
  }

  if (retryTimes < 0) {
    const errorMessage = `Failed after ${startingRetryTimes} retry: ${infoMessage}`;

    innerLogger.error(`[Utils::Retry] ${errorMessage}`);
    throw Error(errorMessage);
  }

  return result as T;
}


export default async function delayedRetry<T>(
  cback: () => Promise<T>, {
    infoMessage = '',
    retryTimes = parseInt(DEFAULT_RETRIES_TIMES),
    retrySetDelay = parseInt(DEFAULT_RETRIES_SETS_DELAY),
    retrySetTimes = parseInt(DEFAULT_RETRIES_SETS)
  }: {
    infoMessage?: string;
    retryTimes?: number,
    retrySetDelay?: number,
    retrySetTimes?: number
  } = {},
  innerLogger: Logger = logger
): Promise<T> {
  let result: T | undefined = undefined;
  const startingRetrySetTimes = retrySetTimes;

  const retryPayload = {
    infoMessage,
    retryTimes
  };

  while (retrySetTimes-- > 0) {
    try {
      result = await retry<T>(cback, retryPayload, innerLogger);
      break;
    } catch (err) {
      innerLogger.warn(`[Utils::DelayedRetry] Restarting delay of ${retrySetDelay}s for the ${startingRetrySetTimes - retrySetTimes}th time: ${infoMessage}`);

      await delay(retrySetDelay);
    }
  }

  if (retrySetTimes < 0) {
    const errorMessage = `Failed after ${startingRetrySetTimes} delayed retries: ${infoMessage}`;

    innerLogger.error(`[Utils::DelayedRetry] ${errorMessage}`);
    throw Error(errorMessage);
  }

  return result as T;
}
