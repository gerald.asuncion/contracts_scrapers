import fs from 'fs';
import removeDirectory from './removeDirectory';

/**
 * Svuota una cartella da tutti i file e/o cartelle contenuti al suo interno
 * @param path percorso della cartella da svuotare
 */
export default function emptyDirectory(path: string): void {
  if (fs.existsSync(path)) {
    const files = fs.readdirSync(path);

    if (files.length > 0) {
      files.forEach((filename) => {
        if (fs.statSync(`${path}/${filename}`).isDirectory()) {
          removeDirectory(`${path}/${filename}`);
        } else {
          fs.unlinkSync(`${path}/${filename}`);
        }
      });
    }
  }
}
