export default function timeoutAsync(fn: () => any, delay: number = 10): Promise<any> {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      const promise = fn();
      if((promise as any).then) {
        promise.then(resolve).catch(reject);
      } else {
        resolve(promise);
      }
    }, delay);
  });
}
