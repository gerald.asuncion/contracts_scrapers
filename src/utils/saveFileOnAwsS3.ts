import path from 'path';
import fs from 'fs';
import { S3Client, PutObjectCommand } from '@aws-sdk/client-s3';
import { S3ClientFactoryResult } from '../aws/s3ClientFactory';
import { Logger } from '../logger';
import { AwsS3Config } from '../config/types';
import createFileKeyForAwsS3 from './createFileKeyForAwsS3';

export default async function saveFileOnAwsS3(s3ClientFactory: S3ClientFactoryResult, awsS3Folder: string, filePath: string, filenameToSave: string, logger: Logger) {
  const runner = s3ClientFactory();
  await runner(async (client: S3Client, config: AwsS3Config) => {
    const fileKey = createFileKeyForAwsS3(awsS3Folder, filenameToSave);
    logger.info(`salvo il file ${fileKey} su aws s3`);

    const readStream = fs.createReadStream(filePath);

    const params = { Bucket: config.bucket, Key: fileKey, Body: readStream };
    const command = new PutObjectCommand(params);
    await client.send(command);
  }, logger);
}
