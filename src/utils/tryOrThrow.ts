import TimeoutError from "../errors/TimeoutError";

export class TryOrThrowError extends Error {
  constructor(msg: string) {
    super(msg);

    this.name = this.constructor.name;

    Error.captureStackTrace(this, this.constructor);
  }

  source?: Error;

  hasSource(...errClasses: (new (...args: any[]) => TryOrThrowError)[]): boolean {
    if (errClasses.some((errClass) => this instanceof errClass)) {
      return true;
    }
    if (this.source && this.source instanceof TryOrThrowError) {
      return this.source.hasSource(...errClasses);
    }
    return false;
  }
}

function canExtendMessage(ex: any): ex is Error {
  return ex instanceof TryOrThrowError || ex instanceof TimeoutError || typeof ex.message === 'string' && !/Node is detached from document/i.test(ex.message);
}

export default async function tryOrThrow<T>(fn: () => Promise<T>, msg: string, ErrClazz = TryOrThrowError): Promise<T> {
  try {
    const res = await fn();
    return res;
  } catch (ex) {
    const errorMessage = `${msg} ${canExtendMessage(ex) ? ex.message : ''}`.trim();
    const error = new ErrClazz(errorMessage);

    // if (Object.prototype.isPrototypeOf.bind(ErrClazz)(TryOrThrowError)) {
    if (error instanceof TryOrThrowError) {
      error.source = ex as Error;
    }

    throw error;
  }
}
