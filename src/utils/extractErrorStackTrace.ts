export default function extractErrorStackTrace(error: any): string {
  return error.stack ? error.stack : '';
}
