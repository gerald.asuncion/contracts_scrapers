import formatTimestamp from '../logTimestampFormatter';

function formatDate() {
  return formatTimestamp().replace(/-|:| /g, '');
}

export default function createFilenameForScreenshot(name: string, idDatiContratto?: string | number): string {
  return [
    idDatiContratto || null,
    formatDate(),
    name
  ].filter(Boolean).join('.');
}
