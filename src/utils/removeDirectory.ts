import fs from 'fs';

export default function removeDirectory(path: string): void {
  if (fs.existsSync(path)) {
    const files = fs.readdirSync(path);

    if (files.length > 0) {
      files.forEach((filename) => {
        if (fs.statSync(`${path}/${filename}`).isDirectory()) {
          removeDirectory(`${path}/${filename}`);
        } else {
          fs.unlinkSync(`${path}/${filename}`);
        }
      });
      fs.rmdirSync(path);
    } else {
      fs.rmdirSync(path);
    }
  }
}
