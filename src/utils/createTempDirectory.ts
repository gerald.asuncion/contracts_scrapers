import config from 'config';
import path from 'path';
import fs from 'fs';

export default function createTempDirectory(): void {
  const downloadDir = path.resolve(config.get('download.path'));
  if (!fs.existsSync(downloadDir)) {
    fs.mkdirSync(downloadDir);
  }
}
