export default function isStraniero(codiceFiscale: string): boolean {
  return codiceFiscale[11].toUpperCase() === 'Z';
}
