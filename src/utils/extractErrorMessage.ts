export default function extractErrorMessage(error: any): string {
  return typeof error === "string" ? error : error.message;
}
