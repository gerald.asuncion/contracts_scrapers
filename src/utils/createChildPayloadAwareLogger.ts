import { Logger } from '../logger';
import ContrattoPayload from '../contratti/ContrattoPayload';

export default function createChildPayloadAwareLogger(parentLogger: Logger, { idDatiContratto }: Partial<ContrattoPayload>): Logger {
  if (idDatiContratto) {
    return parentLogger.child({ idDatiContratto });
  }
  return parentLogger;
}
