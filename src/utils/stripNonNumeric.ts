const stripNonNumeric = (text: string): string => text.replace(/\D/g, '');
export default stripNonNumeric;
