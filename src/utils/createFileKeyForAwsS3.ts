import todayDate from './todayDate';

export default function createFileKeyForAwsS3(folder: string, filenameWithExtension: string) {
  const date = todayDate().split('-');

  return encodeURI(`scraper/${folder}/${date[0]}/${date[1]}/${date[2]}/${filenameWithExtension}`);
}
