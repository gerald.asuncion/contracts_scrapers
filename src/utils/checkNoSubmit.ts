import { NoSubmitPayload } from "../payloads/types";
import { TryOrThrowError } from "./tryOrThrow";

export default function checkNoSubmit<T>(payload: T & NoSubmitPayload): void {
  if (payload.noSubmit) {
    throw new TryOrThrowError('no submit');
  }
}
