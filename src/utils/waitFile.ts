import fs from 'fs';
import delay from './delay';

export default async function waitFile(filePath: string) {
  if (!fs.existsSync(filePath)) {
    await delay(500);
    await waitFile(filePath);
  }
}
