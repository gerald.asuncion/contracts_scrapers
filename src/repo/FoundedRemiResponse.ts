/* eslint-disable no-underscore-dangle */
import { RemiResponse } from './types';

export default class FoundedRemiResponse<T> implements RemiResponse<T> {
  private _data: T[];

  constructor(data: T[]) {
    this._data = data;
  }

  data(): Promise<T[]> {
    return Promise.resolve(this._data);
  }
}
