import { MysqlPool } from '../mysql';
import RepoOptions from './RepoOptions';
import { Logger } from '../logger';
import createChildLogger from '../utils/createChildLogger';

export default abstract class AbstractRepo {
  protected mysqlPool: MysqlPool;

  protected logger: Logger;

  constructor({ mysqlPool, logger }: RepoOptions) {
    this.mysqlPool = mysqlPool;
    this.logger = logger;
    this.logger = createChildLogger(logger, this.constructor.name);
  }

  async startTransaction(): Promise<void> {
    // await this.mysqlPool.query('SET AUTOCOMMIT=false;');
    await this.mysqlPool.query('START TRANSACTION;');
  }

  async commit(): Promise<void> {
    await this.mysqlPool.query('COMMIT;');
  }

  async rollback(): Promise<void> {
    await this.mysqlPool.query('ROLLBACK;');
  }

  async resetPrimaryKeyIndex(tableName: string): Promise<void> {
    await this.mysqlPool.query('ALTER TABLE ?? AUTO_INCREMENT = 1;', [tableName]);
  }

  async truncate(tableName: string): Promise<void> {
    await this.mysqlPool.query('DELETE FROM ??;', [tableName]);
  }
}
