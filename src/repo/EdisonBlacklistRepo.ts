import AbstractRepo from './AbstractRepo';

type EdisonBlacklistCheckPayload = {
  comune: string;
  cap: string;
};

const COMUNECONCAPPRESENTE_SQL = 'SELECT * FROM edison_blacklist WHERE comune = ? AND cap = ?;';

export default class EdisonBlacklistRepo extends AbstractRepo {
  async comuneConCapPresente({ comune, cap }: EdisonBlacklistCheckPayload): Promise<boolean> {
    const result = await this.mysqlPool.query(COMUNECONCAPPRESENTE_SQL, [comune, cap]);
    return !!result.length;
  }
}
