export const getComuniAggiuntivi = () => [
  {"nome":"Barberino Val d'Elsa","codice":"048003","zona":{"codice":"3","nome":"Centro"},"regione":{"codice":"09","nome":"Toscana"},"provincia":{"codice":"048","nome":"Firenze"},"sigla":"FI","codiceCatastale":"A633","cap":["50021"],"popolazione":4403},
  {"nome":"Casasco d'Intelvi","codice":"013254","zona":{"codice":"1","nome":"Nord-ovest"},"regione":{"codice":"03","nome":"Lombardia"},"provincia":{"codice":"013","nome":"Como"},"sigla":"CO","codiceCatastale":"M394","cap":["22023", "22022"],"popolazione":3227},
  {"nome":"Ortonovo","codice":"011020","zona":{"codice":"1","nome":"Nord-ovest"},"regione":{"codice":"07","nome":"Liguria"},"provincia":{"codice":"011","nome":"La Spezia"},"sigla":"SP","codiceCatastale":"G143","cap":["19034"],"popolazione":8405}
];
