import orderBy from 'lodash/orderBy';
import { levenshtein } from 'string-comparison';
import { ComuniRepo } from './ComuniRepo';
import { IrenRemiRow, IrenRemiResponse } from './types';
import FoundedRemiResponse from './FoundedRemiResponse';
import { IrenFattibilitaPayload } from '../scraperv2/iren/IrenFattibilitaPayload';
import AbstractRepo from './AbstractRepo';
import RepoOptions from './RepoOptions';

const setComuni: Record<string, string> = {
  forlì: 'Forli`',
  "forli'": 'Forli`',
  "cantu'": 'CANTU`',
  cantù: 'CANTU`',
  "sant'angelo le fratte": 'SANT`ANGELO LE FRATTE',
  "san dona' di piave": 'SAN DONA` DI PIAVE',
  'san donà di piave': 'SAN DONA` DI PIAVE',
  "roccaforte mondovi'": 'ROCCAFORTE MONDOVI`',
  'roccaforte mondovì': 'ROCCAFORTE MONDOVI`',
  "cava de' tirreni": 'CAVA DE` TIRRENI',
  'castelfranco piandiscò': 'Castelfranco PiandiscÃƒÂ²'
};

function mapComuni(comune: string): string {
  return setComuni[(comune || '').toLowerCase()] || comune;
}

const SET_REGIONI: Record<string, string> = {
  'emilia-romagna': 'emilia romagna'
};

function mapRegione(regione: string): string {
  return SET_REGIONI[regione.toLowerCase()] || regione;
}

type IremRemiRepoOptions = RepoOptions & {
  comuniRepo: ComuniRepo;
};

export default class IrenRemiRepo extends AbstractRepo {
  static readonly TABLE_NAME = 'IREN_GAS_REMI';
  static readonly SQL_EQ = 'SELECT * FROM IREN_GAS_REMI WHERE COMUNE = ?';

  static readonly SQL_STARTS_WITH = 'SELECT * FROM IREN_GAS_REMI WHERE COMUNE LIKE ?';

  static readonly SQL_GET_CITIES_BY_REGION_AND_PROVINCE = 'SELECT * FROM IREN_GAS_REMI WHERE REGIONE = ? AND PROVINCIA = ?';

  static readonly SQL_IMPORT = 'INSERT INTO ?? (AREABACKOFFICE, REGIONE, PROVINCIA, COMUNE, CAP, REMI, VECCHIA_REMI, DISTRIBUTORE, SUBENTRO) VALUES ?';

  static readonly SIMILARITY_THRESHOLD = 0.66;

  private comuniRepo: ComuniRepo;

  constructor(options: IremRemiRepoOptions) {
    super(options);
    this.comuniRepo = options.comuniRepo;
  }

  async isValidByCap(cap: string): Promise<IrenRemiResponse> {
    const result = await this.comuniRepo.findComuneByCap(cap);
    const data = await this.mysqlPool.query<IrenRemiRow>(IrenRemiRepo.SQL_EQ, [mapComuni(result.nome)]);
    return new FoundedRemiResponse(data);
  }

  private async isValidByCity(comune: string): Promise<IrenRemiRow[]> {
    const data = await this.mysqlPool.query<IrenRemiRow>(IrenRemiRepo.SQL_EQ, [comune]);
    return data;
  }

  private async isValidBySimilarity(comune: string, regione: string, provincia: string): Promise<IrenRemiRow[]> {
    const list = await this.mysqlPool.query<IrenRemiRow>(IrenRemiRepo.SQL_GET_CITIES_BY_REGION_AND_PROVINCE, [regione, provincia]);

    const result: IrenRemiRow[] = [];
    if (list.length) {
      const evaluated = list.map((item) => ({
        value: item,
        similarity: levenshtein.similarity(comune, item.COMUNE)
      }));

      const [bestMatch] = orderBy(evaluated, ['similarity'], ['desc']);

      if (bestMatch.similarity >= IrenRemiRepo.SIMILARITY_THRESHOLD) {
        result.push(bestMatch.value);
      }
    }

    return result;
  }

  async isValidByComuneV2({ comune, provincia, regione }: IrenFattibilitaPayload): Promise<IrenRemiResponse> {
    let data = await this.isValidByCity(comune);
    if (!data.length) {
      data = await this.isValidBySimilarity(comune, mapRegione(regione), provincia);
    }
    return new FoundedRemiResponse(data);
  }

  // vecchio metodo che non riesce a coprire tutti i casi e per cui è nato il mapping
  // lasciato solo per eventuali controlli, ma NON USARLO
  async isValidByComune(comune: string, cap: string): Promise<IrenRemiResponse> {
    const matchingComune = await this.comuniRepo.findBestMatchingComune(comune, cap);
    const mapped = mapComuni(matchingComune.nome);
    const sql = mapped.includes('ÃƒÂ²') ? IrenRemiRepo.SQL_STARTS_WITH : IrenRemiRepo.SQL_EQ;
    const value = mapped.includes('ÃƒÂ²') ? mapped.replace('ÃƒÂ²', '%') : mapped;
    const data = await this.mysqlPool.query<IrenRemiRow>(sql, [value]);
    return new FoundedRemiResponse(data);
  }

  async import(rows: IrenRemiRow[]): Promise<void> {
    await this.mysqlPool.truncateTable('IREN_GAS_REMI');

    await this.mysqlPool.insertIntoChunk(
      'IREN_GAS_REMI',
      rows.map((r) => [r.AREABACKOFFICE, r.REGIONE, r.PROVINCIA, r.COMUNE, r.CAP, r.REMI, r.VECCHIA_REMI, r.DISTRIBUTORE, r.SUBENTRO]),
      1000
    );
  }

  async importV2(rows: IrenRemiRow[]): Promise<void> {
    try {
      await this.startTransaction();
      await this.truncate(IrenRemiRepo.TABLE_NAME);
      await this.resetPrimaryKeyIndex(IrenRemiRepo.TABLE_NAME);
      await this.mysqlPool.query(IrenRemiRepo.SQL_IMPORT, [IrenRemiRepo.TABLE_NAME, rows.map((r) => [r.AREABACKOFFICE, r.REGIONE, r.PROVINCIA, r.COMUNE, r.CAP, r.REMI, r.VECCHIA_REMI, r.DISTRIBUTORE, r.SUBENTRO])]);
      await this.commit();
    } catch (ex) {
      await this.rollback();
      throw ex;
    }
  }
}
