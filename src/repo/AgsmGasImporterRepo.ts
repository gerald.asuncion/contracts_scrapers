import AbstractRepo from './AbstractRepo';

const sql = `LOAD DATA LOCAL INFILE ?
  INTO TABLE ??
  FIELDS TERMINATED BY ';'
  LINES TERMINATED BY '\n'
  IGNORE 1 LINES
  (@dummy, @dummy, comune, @dummy, @dummy, provincia, @dummy, @dummy, @dummy, subentro, switch);`;

export default class AgsmGasImporterRepo extends AbstractRepo {
  static readonly TABLE_NAME: string = 'agsm_gas_copertura';

  async loadFileIntoDatabase(file: string): Promise<void> {
    try {
      await this.startTransaction();

      await this.truncate(AgsmGasImporterRepo.TABLE_NAME);

      await this.resetPrimaryKeyIndex(AgsmGasImporterRepo.TABLE_NAME);

      await this.mysqlPool.query(sql, [file, AgsmGasImporterRepo.TABLE_NAME]);

      await this.commit();
    } catch (ex) {
      await this.rollback();
      throw ex;
    }
  }
}
