import { MysqlPool } from '../mysql';
import { UnaretiContendibilitaLuceRecord, UnaretiContendibilitaGasRecord } from './types';

export default class UnaretiRepo {
  private mysqlPool: MysqlPool;

  constructor({ mysqlPool }: { mysqlPool: MysqlPool }) {
    this.mysqlPool = mysqlPool;
  }

  async loadDataInfile(filePath: string, todayDate: string, tableName: string): Promise<void> {
    let sql = '';

    if (tableName === 'unareti_contendibilita_luce') {
      sql = `LOAD DATA LOCAL INFILE ?
        INTO TABLE ??
        FIELDS TERMINATED BY ';'
        LINES TERMINATED BY '\n'
        IGNORE 1 LINES
        (pod, toponimo, nome_strada, civico, estensione_civico, cap, comune, provincia,
          codice_istat, piano, interno, scala, matricola_contatore, tensione, potenza_contrattuale, stato_del_punto, potenza_predisposta_sul_pod, valore_di_tensione)
        SET data_creazione = ?, data_aggiornamento = ?;`;
    } else {
      sql = `LOAD DATA LOCAL INFILE ?
        INTO TABLE ??
        FIELDS TERMINATED BY ';'
        LINES TERMINATED BY '\n'
        IGNORE 1 LINES
        (pdr, toponimo, nome_strada, civico, estensione_civico, cap, comune, provincia,
          codice_istat, piano, interno, scala, matricola_contatore, calibro_contatore, stato_del_punto, portata, codice_remi)
        SET data_creazione = ?, data_aggiornamento = ?;`;
    }

    await this.mysqlPool.query(sql, [filePath, tableName, todayDate, todayDate]);
  }

  async truncate(tableName: string): Promise<void> {
    const sql = 'DELETE FROM ??;';
    await this.mysqlPool.query(sql, [tableName]);
  }

  async resetPrimaryKeyIndex(tableName: string): Promise<void> {
    const sql = 'ALTER TABLE ?? AUTO_INCREMENT = 1;';
    await this.mysqlPool.query(sql, [tableName]);
  }

  async pdrCheck(pdr: string): Promise<UnaretiContendibilitaGasRecord | null> {
    const sql = 'SELECT * FROM unareti_contendibilita_gas WHERE pdr = ?;';
    try {
      const record = await this.mysqlPool.queryOne<UnaretiContendibilitaGasRecord>(sql, [pdr]);
      return record;
    } catch (e) {
      return null;
    }
  }

  async podCheck(pod: string): Promise<UnaretiContendibilitaLuceRecord | null> {
    const sql = 'SELECT * FROM unareti_contendibilita_luce WHERE pod = ?;';
    try {
      const record = await this.mysqlPool.queryOne<UnaretiContendibilitaLuceRecord>(sql, [pod]);
      return record;
    } catch (e) {
      return null;
    }
  }
}
