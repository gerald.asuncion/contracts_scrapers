/* eslint-disable @typescript-eslint/naming-convention */
import AbstractRepo from './AbstractRepo';

type AretiRepoRow = {
  stato_contatore: string;
  flag_pod_contendibile: string;
  indirizzo: string;
  interno: string;
  scala: string;
  potenza_disponibile: number;
  tensione_rete: string;
};

export class AretiRepo extends AbstractRepo {
  static readonly TABLE_NAME: string = 'areti_contendibilita';

  async loadDataInfile(filePath: string, todayDate: string, {
    podIndex,
    statoIndex,
    flagIndex,
    indirizzoIndex,
    internoIndex,
    scalaIndex,
    potenzaIndex,
    tensioneIndex,
    numberOfColumns
  }: AretiCsvInfo): Promise<void> {
    const header = (new Array(numberOfColumns)).fill('@dummy');
    header[podIndex] = 'pod';
    header[statoIndex] = 'stato_contatore';
    header[flagIndex] = 'flag_pod_contendibile';
    header[indirizzoIndex] = 'indirizzo';
    header[internoIndex] = 'interno';
    header[scalaIndex] = 'scala';
    header[potenzaIndex] = 'potenza_disponibile';
    header[tensioneIndex] = 'tensione_rete';

    // const sql = `LOAD DATA LOCAL INFILE '${filePath}'
    const sql = `LOAD DATA LOCAL INFILE ?
      INTO TABLE areti_contendibilita
      FIELDS TERMINATED BY ';'
      LINES TERMINATED BY '\n'
      IGNORE 1 LINES
      (${header.join(', ')})
      SET data_creazione = ?, data_aggiornamento = ?;`;
    //  SET data_creazione = '${todayDate}', data_aggiornamento = '${todayDate}';`;

    await this.mysqlPool.query(sql, [filePath, todayDate, todayDate]);
  }

  async resetPrimaryKeyIndex(): Promise<void> {
    const sql = 'ALTER TABLE areti_contendibilita AUTO_INCREMENT = 1;';
    await this.mysqlPool.query(sql);
  }

  async podCheck(pod: string, isSubentro: boolean): Promise<null | string> {
    const sql = 'SELECT * FROM areti_contendibilita WHERE pod = ?;';

    try {
      const result = await this.mysqlPool.queryOne<AretiRepoRow>(sql, [pod]);

      return this.getSupermoneyStatusFromStatoContatoreAndFlagPodContendibile(result, isSubentro);
    } catch (e) {
      return null;
    }
  }

  private getSupermoneyStatusFromStatoContatoreAndFlagPodContendibile(row: AretiRepoRow, isSubentro: boolean) {
    const {
      stato_contatore,
      flag_pod_contendibile,
      indirizzo,
      interno,
      scala,
      potenza_disponibile,
      tensione_rete
    } = row;
    // eslint-disable-next-line max-len
    const okMessageFactory = () => `OK - ${indirizzo} - Interno: ${interno || '#'}, Scala ${scala || '#'}; Potenza disponibile: ${potenza_disponibile || '#'}, Tensione rete: ${tensione_rete || '#'}`;

    if (isSubentro) {
      switch (stato_contatore) {
        case 'Senza attribuz.':
          return 'KO - Procedere con prima attivazione';
        case 'Montato':
          if (flag_pod_contendibile.toUpperCase() === 'SI') {
            return okMessageFactory();
          }
          return null;
        default:
          return null;
      }
    }

    if (flag_pod_contendibile.toUpperCase() === 'NO') {
      return null;
    }

    switch (stato_contatore) {
      case 'Senza attribuz.':
        return okMessageFactory();
      case 'Montato':
        return 'KO - Necessario procedere con un Subentro';
      default:
        return null;
    }
  }

  async loadDataIntoDatabase(
    firstFilename: string,
    firstFileInfo: AretiCsvInfo,
    secondFilename: string,
    secondFileInfo: AretiCsvInfo,
    date: string
  ): Promise<void> {
    try {
      await this.startTransaction();

      await this.truncate(AretiRepo.TABLE_NAME);

      await this.resetPrimaryKeyIndex();

      await this.loadDataInfile(firstFilename, date, firstFileInfo);

      await this.loadDataInfile(secondFilename, date, secondFileInfo);

      await this.commit();
    } catch (exc) {
      await this.rollback();

      throw exc;
    }
  }

  async loadDataArrayIntoDatabase(filesNameAndInfo: [string, AretiCsvInfo][], todayDate: string) {
    try {
      await this.startTransaction();

      await this.truncate(AretiRepo.TABLE_NAME);

      await this.resetPrimaryKeyIndex();

      for (const data of filesNameAndInfo) {
        await this.loadDataInfile(data[0], todayDate, data[1]);
      }

      await this.commit();
    } catch (exc) {
      await this.rollback();

      throw exc;
    }
  }
}

export interface AretiCsvInfo {
  podIndex: number;
  statoIndex: number;
  flagIndex: number;
  indirizzoIndex: number;
  internoIndex: number;
  scalaIndex: number;
  potenzaIndex: number;
  tensioneIndex: number;
  numberOfColumns: number;
}
