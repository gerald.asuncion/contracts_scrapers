import { MysqlPool } from '../mysql';
import { AreraDistributore } from '../scraperv2/arera/types';

export default class AreraRepo {
  private mysqlPool: MysqlPool;

  constructor({ mysqlPool }: { mysqlPool: MysqlPool }) {
    this.mysqlPool = mysqlPool;
  }

  async estraiFornitoreDaCodiceIstatComune(codiceIstatComune: string): Promise<AreraDistributore[]> {
    const sql = 'SELECT unix_timestamp(data_aggiornamento) as data_aggiornamento, id, codice_istat_comune, comune, distributore '
    + 'FROM arera '
    + 'WHERE codice_istat_comune = ?;';

    try {
      const results = await this.mysqlPool.query<AreraDistributore>(sql, [codiceIstatComune]);
      return results;
    } catch (e) {
      return [];
    }
  }

  async aggiungi(codiceIstatComune: string, comune: string, distributore: string): Promise<void> {
    const sql = 'INSERT INTO arera (codice_istat_comune, comune, distributore, data_creazione, data_aggiornamento) VALUES (?,?,?,?,?);';

    await this.mysqlPool.query(sql, [codiceIstatComune, comune, distributore, this.todayDate(), this.todayDate()]);
  }

  async aggiornaDistributore(idDistributore: string, distributore: string): Promise<void> {
    const sql = 'UPDATE arera SET distributore = ?, data_aggiornamento = ? WHERE id = ?;';

    await this.mysqlPool.query(sql, [distributore, this.todayDate(), idDistributore]);
  }

  private todayDate() {
    const today = new Date();
    const dd = String(today.getDate()).padStart(2, '0');
    const mm = String(today.getMonth() + 1).padStart(2, '0'); // January is 0!
    const yyyy = today.getFullYear();
    const hh = today.getHours();
    const ii = today.getMinutes();
    const ss = today.getSeconds();

    return `${yyyy}-${mm}-${dd} ${hh}:${ii}:${ss}`;
  }
}
