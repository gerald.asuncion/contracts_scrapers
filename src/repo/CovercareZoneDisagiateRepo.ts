import { MysqlArgs, MysqlPoolInterface } from '../mysql';

export interface CovercareZoneDisagiateRepo {
  //  * @param comune Comune da controllare
  /**
   * Ritorna true se il comune passato è disagiato
   * @param cap    Codice di Avviamento Postale da controllare
   */
  isUncomfortable(/* comune: string, */ cap: string): Promise<boolean>;

  //  * @param comune Comune da controllare
  /**
   * Ritorna true se il comune passato NON è disagiato
   * @param cap    Codice di Avviamento Postale da controllare
   */
  isNotUncomfortable(/* comune: string, */ cap: string): Promise<boolean>;
}

// const IS_UNCOMFORTABLE_SQL =  "SELECT * FROM cover_care_zone_disagiate WHERE comune = ? AND cap = ?;";
const IS_UNCOMFORTABLE_SQL = 'SELECT * FROM cover_care_zone_disagiate WHERE cap = ?;';

export default class CovercareZoneDisagiate implements CovercareZoneDisagiateRepo {
  private mysqlPool: MysqlPoolInterface;

  constructor(options: MysqlArgs) {
    this.mysqlPool = options.mysqlPool;
  }

  async isUncomfortable(/* comune: string, */ cap: string): Promise<boolean> {
    const result = await this.mysqlPool.query(IS_UNCOMFORTABLE_SQL, [cap]);
    const uncomfortable = !!result.length;
    return Promise.resolve(uncomfortable);
  }

  async isNotUncomfortable(/* comune: string, */ cap: string): Promise<boolean> {
    const result = await this.isUncomfortable(/* comune, */ cap);
    return Promise.resolve(!result);
  }
}

export function covercareZoneDisagiateRepo(options: MysqlArgs): CovercareZoneDisagiate {
  return new CovercareZoneDisagiate(options);
}
