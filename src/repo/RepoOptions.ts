import { MysqlPool } from '../mysql';
import { Logger } from '../logger';

export default interface RepoOptions {
  mysqlPool: MysqlPool;
  logger: Logger;
}
