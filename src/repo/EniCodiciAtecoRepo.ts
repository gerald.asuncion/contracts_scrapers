import EniCodiciAtecoResult from '../scraperv2/eni/ateco/EniCodiciAtecoResult';
import extractErrorMessage from '../utils/extractErrorMessage';
import AbstractRepo from './AbstractRepo';

export default class EniCodiciAtecoRepo extends AbstractRepo {
  static readonly TABLE_NAME: string = 'eni_ateco';

  async save(lista: EniCodiciAtecoResult): Promise<void> {
    try {
      await this.startTransaction();

      await this.truncate(EniCodiciAtecoRepo.TABLE_NAME);

      await this.resetPrimaryKeyIndex(EniCodiciAtecoRepo.TABLE_NAME);

      const sql = `INSERT INTO ??
        (forma_giuridica, settore_merceologico, attivita_merceologica, codice_ateco_label, codice_ateco_value)
        VALUES ?`;

      await this.mysqlPool.query(sql, [EniCodiciAtecoRepo.TABLE_NAME, lista]);

      await this.commit();
    } catch (ex) {
      this.logger.error(extractErrorMessage(ex));
      await this.rollback();
    }
  }

  async caricaFormeGiuridiche(): Promise<Array<string>> {
    const records = await this.mysqlPool.query<{
      forma_giuridica: string
    }>('SELECT DISTINCT forma_giuridica FROM ??;', [EniCodiciAtecoRepo.TABLE_NAME]);

    return records.map(({ forma_giuridica }) => forma_giuridica);
  }

  async caricaSettoriMerceologici(formaGiuridica: string): Promise<Array<string>> {
    const records = await this.mysqlPool.query<{
      settore_merceologico: string
    }>(
      'SELECT DISTINCT settore_merceologico FROM ?? WHERE forma_giuridica = ?;',
      [EniCodiciAtecoRepo.TABLE_NAME, formaGiuridica]
    );

    return records.map(({ settore_merceologico }) => settore_merceologico);
  }

  async caricaAttivitaMerceologiche(settoreMerceologico: string): Promise<Array<string>> {
    const records = await this.mysqlPool.query<{
      attivita_merceologica: string
    }>(
      'SELECT DISTINCT attivita_merceologica FROM ?? WHERE settore_merceologico = ?;',
      [EniCodiciAtecoRepo.TABLE_NAME, settoreMerceologico]
    );

    return records.map(({ attivita_merceologica }) => attivita_merceologica);
  }

  async caricaCodiciAteco(
    attivitaMerceologica: string
  ): Promise<Array<{ label: string; ateco: string; }>> {
    const records = await this.mysqlPool.query<{
      codice_ateco_label: string;
      codice_ateco_value: string;
    }>(
      'SELECT DISTINCT codice_ateco_label, codice_ateco_value FROM ?? WHERE attivita_merceologica = ?;',
      [EniCodiciAtecoRepo.TABLE_NAME, attivitaMerceologica]
    );

    return records.map(({ codice_ateco_label, codice_ateco_value }) => ({
      label: codice_ateco_label,
      ateco: codice_ateco_value
    }));
  }
}
