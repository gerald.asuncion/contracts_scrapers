// import * as path from 'path';
// import * as fs from 'fs';
import { findBestMatch } from 'string-similarity';
import { getComuni } from './comuni';
import { getComuniAggiuntivi } from './comuni-aggiuntivi';

export class ComuniRepo implements ComuniRepo {
  private comuni: Comune[];

  constructor() {
    this.comuni = getComuni().concat(getComuniAggiuntivi());
    // this.comuni = [];
    // const comuniJsonPath = path.resolve(process.cwd(), 'build', 'api', 'src', 'repo', 'comuni.json');

    // console.log(`leggo il file comuni.json al percorso ${comuniJsonPath}`);
    // fs.readFile(comuniJsonPath, (err, raw) => {
    //   if (err) {
    //     console.log(`non sono riuscito ad aprire il file ${comuniJsonPath}`, err);
    //     return;
    //   }
    //   this.comuni = [
    //     ...this.comuni,
    //     ...JSON.parse(raw.toString())
    //   ];
    // });
    // const comuniAggiuntiviJsonPath = path.resolve(process.cwd(), 'build', 'api', 'src', 'repo', 'comuni-aggiuntivi.json');

    // console.log(`leggo il file comuni-aggiuntivi.json al percorso ${comuniAggiuntiviJsonPath}`);
    // fs.readFile(comuniAggiuntiviJsonPath, (err, raw) => {
    //   if (err) {
    //     console.log(`non sono riuscito ad aprire il file ${comuniAggiuntiviJsonPath}`, err);
    //     return;
    //   }
    //   this.comuni = [
    //     ...this.comuni,
    //     ...JSON.parse(raw.toString())
    //   ];
    // });
  }

  allComuni(): Comune[] {
    return this.comuni;
  }

  findComune(comune: string): Promise<Comune> {
    return new Promise<Comune>(
      (resolve, reject) => {
        const foundedComune = this.comuni.find((c) => c.nome.toLowerCase() === comune.toLowerCase());
        if (foundedComune) {
          resolve(foundedComune);
        } else {
          reject();
        }
      }
    );
  }

  findBestMatch(comune: string): Promise<Comune> {
    return new Promise<Comune>(
      (resolve, reject) => {
        const foundedComune = this.allComuni().find(
          (c) => c.nome.toLowerCase()
            .normalize('NFD').replace(/[\u0300-\u036f]/g, '') === comune.toLowerCase().normalize('NFD').replace(/[\u0300-\u036f]/g, '')
        );
        if (foundedComune) {
          resolve(foundedComune);
        } else {
          reject();
        }
      }
    );
  }

  findComuneByCap(cap: string): Promise<Comune> {
    return new Promise<Comune>(
      (resolve, reject) => {
        const foundedComune = this.comuni.find((c) => c.cap.includes(cap));
        if (foundedComune) {
          resolve(foundedComune);
        } else {
          reject();
        }
      }
    );
  }

  findBestMatchingComuneWithThreshold(comune: string, cap: string, threshold: number): Promise<Comune> {
    return new Promise<Comune>(((resolve, reject) => {
      const comuniByCap = this.allComuni().filter((c) => c.cap.includes(cap));
      const match = findBestMatch(comune.toLowerCase(), comuniByCap.map((c) => c.nome.toLowerCase()));
      if (match.bestMatch.rating < threshold) {
        reject();
      }
      const foundedComune = comuniByCap.find((c) => c.nome.toLowerCase() === match.bestMatch.target.toLowerCase());
      if (foundedComune) {
        resolve(foundedComune);
      } else {
        reject();
      }
    }));
  }

  findBestMatchingComune(comune: string, cap: string): Promise<Comune> {
    return this.findBestMatchingComuneWithThreshold(comune, cap, 0.5);
  }
}

export interface ComuniRepo {
  findComune(comune: string): Promise<Comune>

  findComuneByCap(cap: string): Promise<Comune>

  findBestMatchingComune(comune:string, cap: string): Promise<Comune>

  findBestMatch(comune: string): Promise<Comune>

  findBestMatchingComuneWithThreshold(comune: string, cap: string, threshold: number): Promise<Comune>
}

export interface Comune {
  nome: string,
  codice: string,
  zona: {
    codice: string,
    nome: string
  },
  regione: {
    codice: string,
    nome: string
  },
  provincia: {
    codice: string,
    nome: string
  },
  sigla: string,
  codiceCatastale: string
  cap: string[],
  popolazione: number
}
