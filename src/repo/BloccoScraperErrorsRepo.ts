import AbstractRepo from './AbstractRepo';

const SQL = 'SELECT bloccante FROM blocco_scraper_errors WHERE testo = ?;';

export default class BloccoScraperErrorsRepo extends AbstractRepo {
  static readonly TABLE_NAME: string = 'blocco_scraper_errors';

  async isBloccante(msg: string): Promise<boolean> {
    const [record] = await this.mysqlPool.query<{ bloccante: number }>(SQL, [msg]);
    return record && record.bloccante === 1;
  }
}
