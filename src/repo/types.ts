export type UnaretiContendibilitaRecordCommonData = {
  id: string;
  stato_del_punto: string;
  data_creazione: string;
  data_aggiornamento: string;
  toponimo: string;
  nome_strada: string;
  civico: string;
  estensione_civico: string;
  cap: string;
  comune: string;
  codice_istat: string;
  piano: string;
  provincia: string;
  interno: string;
  scala: string;
  matricola_contatore: string;
};

export type UnaretiContendibilitaLuceRecord = UnaretiContendibilitaRecordCommonData & {
  pod: string;
  potenza_contrattuale: string;
  tensione: string;
  potenza_predisposta_sul_pod: string;
  valore_di_tensione: string;
};

export type UnaretiContendibilitaGasRecord = UnaretiContendibilitaRecordCommonData & {
  pdr: string;
  calibro_contatore: string;
  portata: number;
  codice_remi: string;
};

export interface RemiResponse<T> {
  data: () => Promise<T[]>;
}

export type IrenRemiRow = {
  AREABACKOFFICE: string,
  REGIONE: string,
  PROVINCIA: string,
  COMUNE: string,
  CAP: string,
  REMI: string,
  VECCHIA_REMI: string,
  DISTRIBUTORE: string,
  SUBENTRO: string
};

export type AtenaCsvRow = {
  Comune: string;
  Provincia: string;
  Regione: string;
};

export type IrenRemiResponse = RemiResponse<IrenRemiRow>;

export type AtenaRepoResponse = RemiResponse<AtenaCsvRow>;
