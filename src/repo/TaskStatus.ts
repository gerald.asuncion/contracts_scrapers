export const TASK_STATUS = {
  READY: 'READY',
  PENDING: 'PENDING',
  COMPLETED: 'COMPLETED',
  ERROR: 'ERROR'
} as const;

export type TaskStatus = typeof TASK_STATUS[keyof typeof TASK_STATUS];
