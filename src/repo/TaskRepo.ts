import NoResultError from '../errors/NoResultError';
import { Logger } from '../logger';
import { MysqlPool } from '../mysql';
import { TaskStatus, TASK_STATUS } from './TaskStatus';

export default class TaskRepo {
  private mysqlPool: MysqlPool;

  private table = 'queue';

  //private logger: Logger;

  constructor({ mysqlPool }: { mysqlPool: MysqlPool }) {
    this.mysqlPool = mysqlPool;

    // this.logger = createChildLogger(container.resolve<Logger>('logger'), 'TaskRepo');
  }

  async add(task: Task): Promise<void> {
    const sql = `INSERT INTO ?? (scraper, status, retry, max_retry, payload, webhook, created_at, updated_at)
    VALUES (?, ?, ?, ?, ?, ?, NOW(), NOW())`;
    await this.mysqlPool.query(
      sql, [
        this.table,
        task.scraper, task.status, task.retry, task.maxRetry, JSON.stringify(task.payload)/* .replace(/'/g, "\\'") */, task.webhook
      ]
    );
  }

  async getByStatus(status: TaskStatus): Promise<Task[]> {
    const sql = 'SELECT * FROM ?? WHERE status = ?';
    const results = await this.mysqlPool.query(sql, [this.table, status]);

    return results.map((r) => Object.assign({} as Task, r));
  }

  async getOneByStatus(status: TaskStatus, logger: Logger): Promise<Task | undefined> {
    let logTaskId = '-';

    let closingSqlCommand = 'COMMIT';

    try {
      // Start della transaction
      const transactionSql = 'START TRANSACTION';
      await this.mysqlPool.query(transactionSql);

      // Estrazione primo task valido
      // const sql = `SELECT * FROM ${this.table} WHERE status = '${status}' ORDER BY id ASC LIMIT 1 FOR UPDATE`;
      // const sql = 'SELECT * FROM ?? WHERE status = ? ORDER BY id ASC LIMIT 1 FOR UPDATE SKIP LOCKED';
      const sql = 'SELECT * FROM ?? WHERE status = ? ORDER BY RAND() ASC LIMIT 1 FOR UPDATE SKIP LOCKED';
      // const sql = 'select * from queue where id=(select id from queue where status="ready" order by id asc limit 1) for update skip locked;';

      // const res = await this.mysqlPool.queryOne<Task>(sql, [this.table, status]);
      const res = await this.mysqlPool.queryOneV2<Task>(sql, [this.table, status]);

      if (!res) {
        logger.info(`TASKREPO::getOneByStatus - skipped task after update!`);
        return;
      }

      logTaskId = (res.id || 'NONE').toString();

      // Aggiorno lo status del task in PENDING
      res.status = TASK_STATUS.PENDING;
      await this.updateStrict(res, status);

      return {
        ...res,
        maxRetry: (res as any).max_retry
      };
    } catch (e) {
      logger.error(`TASKREPO::getOneByStatus - TASKID = '${logTaskId}'`);
      logger.error(e);

      closingSqlCommand = 'ROLLBACK';
    } finally {
      // Commit o Rollback della transaction
      await this.mysqlPool.query(closingSqlCommand);
    }
  }

  async update(task: Task): Promise<void> {
    const sql = `UPDATE ?? SET
    scraper = ?,
    status = ?,
    retry = ?,
    response = ?,
    updated_at = NOW()
    WHERE id = ?`;
    await this.mysqlPool.query(sql, [this.table, task.scraper, task.status, task.retry, task.response || 'NULL', task.id]);
  }

  async updateStrict(task: Task, currentStatus: TaskStatus): Promise<void> {
    const sql = `UPDATE ?? SET
    scraper = ?,
    status = ?,
    retry = ?,
    response = ?,
    updated_at = NOW()
    WHERE id = ? AND status = ?`;
    const {
      changedRows,
      message
    } = (await this.mysqlPool.query(sql, [this.table, task.scraper, task.status, task.retry, task.response || 'NULL', task.id, currentStatus])) as any as { affectedRows: number; changedRows: number; message: string; };

    if (changedRows != 1) {
      throw new Error(`TaskRepo::updateStrict changedRow=${changedRows} message=${message}!`);
    }
  }

  async updatePayloadTrasformato<T>(task: Task, payloadTrasformato: T): Promise<void> {
    const sql = `UPDATE ?? SET
    scraper = ?,
    payloadTrasformato = ?,
    updated_at = NOW()
    WHERE id = ?`;
    await this.mysqlPool.query(sql, [this.table, task.scraper, payloadTrasformato ? JSON.stringify(payloadTrasformato) : 'NULL', task.id]);
  }
}

export interface Task {
  id?: number,
  status: TaskStatus,
  scraper: string,
  retry: number,
  maxRetry: number,
  payload: any,
  webhook: string,
  response?: string
}
