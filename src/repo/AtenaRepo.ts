/* eslint-disable no-underscore-dangle */
/* eslint-disable max-classes-per-file */
import { MysqlPool } from '../mysql';
import { ComuniRepo } from './ComuniRepo';
import { AtenaRepoResponse, AtenaCsvRow } from './types';
import FoundedRemiResponse from './FoundedRemiResponse';

export class AtenaRepo implements AtenaRepo {
  private mysqlPool: MysqlPool;

  private comuniRepo: ComuniRepo;

  constructor({ mysqlPool, comuniRepo }: { mysqlPool: MysqlPool, comuniRepo: ComuniRepo }) {
    this.mysqlPool = mysqlPool;
    this.comuniRepo = comuniRepo;
  }

  async isValidByCap(cap: string): Promise<AtenaRepoResponse> {
    const result = await this.comuniRepo.findComuneByCap(cap);
    const data = await this.mysqlPool.query<AtenaCsvRow>('SELECT * FROM ATENA_COPERTURA_GAS WHERE Comune = ?', [result.nome]);
    return new FoundedRemiResponse(data);
  }

  async isValidByComune(comune: string, cap: string): Promise<AtenaRepoResponse> {
    const matchingComune = await this.comuniRepo.findBestMatchingComune(comune, cap);
    const data = await this.mysqlPool.query<AtenaCsvRow>('SELECT * FROM ATENA_COPERTURA_GAS WHERE Comune = ?', [matchingComune.nome]);
    return new FoundedRemiResponse(data);
  }

  async import(rows: AtenaCsvRow[]): Promise<void> {
    await this.mysqlPool.truncateTable('ATENA_COPERTURA_GAS');

    await this.mysqlPool.insertIntoChunk('ATENA_COPERTURA_GAS', rows.map((r) => [r.Comune, r.Provincia, r.Regione]), 1000);
  }
}

export interface AtenaRepo {
  isValidByCap(cap: string): Promise<AtenaRepoResponse>

  isValidByComune(comune: string, cap: string): Promise<AtenaRepoResponse>

  import(rows: AtenaCsvRow[]): Promise<void>
}
