import { ScraperResponse } from "../scraperv2/scraper";

export default class TaskError extends Error {
  errCode: string;
  scraperResponse: ScraperResponse;

  constructor(msg: string, errCode: string, scraperResponse: ScraperResponse) {
    super(msg);

    this.name = this.constructor.name;

    this.errCode = errCode;

    this.scraperResponse = scraperResponse;

    Error.captureStackTrace(this, this.constructor);
  }
}
