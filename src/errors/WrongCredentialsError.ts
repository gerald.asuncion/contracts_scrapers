import WrongCredentialsResponse from '../response/WrongCredentialsResponse';

export default class WrongCredentialsError extends Error {
  constructor(public url: string, public username: string) {
    super(`Wrong credentials ${username} for ${url}`);

    this.name = this.constructor.name;

    Error.captureStackTrace(this, this.constructor);
  }

  toResponse(): WrongCredentialsResponse {
    return new WrongCredentialsResponse(this.url, this.username);
  }
}
