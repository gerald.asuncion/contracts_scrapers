export default class NoResultError extends Error {
  constructor() {
    super('no result');

    this.name = this.constructor.name;

    Error.captureStackTrace(this, this.constructor);
  }
}
