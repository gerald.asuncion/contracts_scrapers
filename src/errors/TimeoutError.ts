import TimeOutResponse from '../response/TimeOutResponse';

export default class TimeoutError extends Error {
  constructor(message: string) {
    super(message);

    this.name = this.constructor.name;

    Error.captureStackTrace(this, this.constructor);
  }

  toResponse(): TimeOutResponse {
    return new TimeOutResponse(this.message);
  }
}
