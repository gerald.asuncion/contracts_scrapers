import BlockedScraperResponse from '../response/BlockedScraperResponse';

export default class BlockedScraperError extends Error {
  constructor(msg: string) {
    super(msg);

    this.name = this.constructor.name;

    Error.captureStackTrace(this, this.constructor);
  }

  toResponse(): BlockedScraperResponse {
    return new BlockedScraperResponse(this.message);
  }
}
