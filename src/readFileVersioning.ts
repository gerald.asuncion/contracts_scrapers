import path from 'path';
import fs from 'fs';

export default function readFileVersioning(filename: string): string {
  const buffer = fs.readFileSync(path.join(process.cwd(), filename), { encoding: 'utf-8' });
  return buffer.toString();
}
