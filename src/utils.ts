export const trace: any = (data: any) => {
  console.log(data);
  return data;
};

export const cleanString = (s: string): string => s.replace(/[^a-zA-Z ]/g, "");

export const formatDate = (date: Date): string => [
  date.getDay().toString().padStart(2, '0'),
  date.getUTCMonth().toString().padStart(2, '0'),
  date.getFullYear()
].join("/")

export const formatDateString = (date:string) : string => date.split("-").reverse().join('/');
