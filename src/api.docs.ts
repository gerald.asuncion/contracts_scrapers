import path from "path";
import swaggerJSDoc from "swagger-jsdoc";

const options: swaggerJSDoc.Options = {
  definition: {
    openapi: '3.0.0',
    info: {
      title: 'Scrapers API',
      version: '2.0.0'
    },
    // host: `http://${process.env.IP}:${process.env.PORT}`,
    // basePath: '/'
    tags: [
      { name: 'v2'},
      { name: 'v3' },
      { name: 'queue' }
    ]
  },
  apis: [
    path.resolve(__dirname, '**', '*.apidoc.js')
  ]
};

const apiDocs = swaggerJSDoc(options);

export default apiDocs;
