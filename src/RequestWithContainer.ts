import { AwilixContainer } from 'awilix';
import { Request } from 'express';

export default interface RequestWithContainer extends Request {
  container: AwilixContainer;
}
