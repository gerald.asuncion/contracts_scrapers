import axios from 'axios';
import config from 'config';
import { logger, Logger } from '../logger';
import delayedRetry from "../utils/retry";

const {
  SERVICES_MAPPING_BASEURL = config.get('services.mapping.baseUrl'),
} = process.env;

const reqConfig = {
  headers: {
    'Content-Type': 'application/json'
  }
};

export async function getMappingOfScraper<T>(scraperCodice: string, _logger: Logger = logger): Promise<T> {
  const url = `${SERVICES_MAPPING_BASEURL}/v1/scrapers/${scraperCodice}/mapping`;

  return delayedRetry(() => axios.get<T>(url, reqConfig), {
    infoMessage: `mapping service at "${SERVICES_MAPPING_BASEURL}" with scraper "${scraperCodice}".`
  }, _logger)
  .then(resp => resp.data);
}
