import FastwebInserimentoPayload from "../../scraperv2/fastweb/payload/FastwebInserimentoPayload";
import { ScraperMappingMapper, ScraperMappingSet } from "./types";

export default (function (
  mapping: ScraperMappingSet<FastwebInserimentoPayload>, {
    comuneNascitaEstero,
    recapitoComune,
    recapitoProvincia,
    attivazioneComune,
    attivazioneProvincia,
    residenzaComune,
    residenzaProvincia,
    comuneNascita,
    provinciaNascita
  }: FastwebInserimentoPayload, mappedPayload: FastwebInserimentoPayload) {
  return {
    ...mappedPayload,
    // comuneNascita: comuneNascitaEstero || provinciaNascita === 'EE' ?
    //   comuneNascita :
    //   fixComuneFastweb(mergeNomiCitta(...fixNomiCittaFastweb(fixComuneFastweb(comuneNascita), provinciaNascita))),
    // residenzaComune: fixComuneResidenza(mergeNomiCitta(...fixNomiCittaFastweb(fixComuneFastweb(residenzaComune), residenzaProvincia))),
    // attivazioneComune: attivazioneComune ? fixComuneAttivazione(mergeNomiCitta(...fixNomiCittaFastweb(fixComuneFastweb(attivazioneComune), attivazioneProvincia))) : attivazioneComune,
    // recapitoComune: fixComuneSpedizione(mergeNomiCitta(...fixNomiCittaFastweb(fixComuneFastweb(recapitoComune), recapitoProvincia)), mapping),

    comuneNascita: mapComuneNascita(comuneNascita, provinciaNascita, mapping, comuneNascitaEstero),
    residenzaComune: mapComuneResidenza(residenzaComune, residenzaProvincia, mapping),
    attivazioneComune: mapComuneAttivazione(attivazioneComune, attivazioneProvincia, mapping),
    recapitoComune: mapComuneSpedizione(recapitoComune, recapitoProvincia, mapping)
  };
} as ScraperMappingMapper<FastwebInserimentoPayload>);

function parseComune(comune: string) {
  return comune.toLowerCase();
}
function parseProvincia(provincia: string) {
  return provincia.toLowerCase();
}

function mapComune(campo: string) {
  const campoEE = `${campo} (EE)`;

  return function(_comune: string, _provincia: string, mapping: ScraperMappingSet<any>, forzaEstero = false) {
    const comune = parseComune(_comune);

    if (forzaEstero || _provincia === 'EE') {
      return mapping[campoEE][comune] || mapping[campoEE][_comune] || _comune;
    }

    const provincia = parseProvincia(_provincia);
    const campoFull = mergeNomiCitta(comune, provincia);

    return mapping[campo][campoFull] || mapping[campo][comune] || mapping[campo][_comune] || campoFull;
  }
}

const mergeNomiCitta = (comune: string, provincia: string): string => `${comune} (${provincia})`;

const mapComuneNascita = mapComune('comuneNascita');
const mapComuneResidenza = mapComune('residenzaComune');
const mapComuneAttivazione = mapComune('attivazioneComune');
const mapComuneSpedizione = mapComune('recapitoComune');
