export type ScraperMappingPayload = {}; // Record<string, string|number|boolean|Date>;

export type ScraperMappingSet<T extends ScraperMappingPayload> = {
  [K in keyof Partial<T>]: Record<string, T[K]>
};

export type ScraperMappingMapper<T extends ScraperMappingPayload = any> = (mapping: ScraperMappingSet<T>, payload: T, mappedPayload: T) => {};
