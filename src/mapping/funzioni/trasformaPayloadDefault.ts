import EoloPayload from "../../scraperv2/eolo/EoloPayload";
import { ScraperMappingMapper, ScraperMappingSet } from "./types";

const parseValue = (value: string) => value.toLowerCase()
  .replace(/[a|A]'/g, 'à')
  .replace(/[e|E]'/g, 'è')
  .replace(/[i|I]'/g, 'ì')
  .replace(/[o|O]'/g, 'ò')
  .replace(/[u|U]'/g, 'ù')
  .replace(/ dè /g, ' de\' ');

export default (function (mapping: ScraperMappingSet<EoloPayload>, payload: EoloPayload, mappedPayload: EoloPayload) {
  Object.keys(mapping)
  .filter(campo => campo in payload)
  .forEach(campo => {
    // console.log('controllo il campo: ', campo);
    const campoSet = mapping[campo as keyof EoloPayload];

    if (campoSet) {
      // console.log(`trovato set (${campo}): `, campoSet);
      const value = payload[campo as keyof EoloPayload] as string;

      if (value) {
        const lValue = value.toLowerCase();
        const parsedValue = parseValue(value);
        // console.log(`${lValue} ??? ${parsedValue} (${value})`);

        const setValue = parsedValue in campoSet && parsedValue || lValue in campoSet && lValue || value in campoSet && value;

        if (setValue) {
          // console.log('set value: ', campoSet[setValue]);
          (mappedPayload as any)[campo] = campoSet[setValue];
        }
      }
    }
  });

  return mappedPayload;
} as ScraperMappingMapper<EoloPayload>);
