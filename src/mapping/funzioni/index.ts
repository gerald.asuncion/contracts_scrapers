import { ScraperMappingMapper } from "./types";
import trasformaPayloadFastweb from "./trasformaPayloadFastweb";

export default {
  'fastweb': trasformaPayloadFastweb
} as Record<string, ScraperMappingMapper<any>>;
