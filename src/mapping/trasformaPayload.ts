import { isEmpty } from 'lodash';
import mappers from './funzioni';
import trasformaPayloadDefault from './funzioni/trasformaPayloadDefault';
import { ScraperMappingSet } from './funzioni/types';
import { getMappingOfScraper } from "./getMappingOfScraper";

export async function trasformaPayload<T>(scraperCodice: string, payload: T): Promise<T> {
  const mappedPayload: T = {
    ...payload
  };
  const mapping = await getMappingOfScraper<ScraperMappingSet<T>>(scraperCodice);
  // console.log(`${scraperCodice} mapping:\n${JSON.stringify(mapping, null, 2)}`);

  if (!isEmpty(mapping)) {
    // console.log('effettuo il mapping')
    const mapper = mappers[scraperCodice] || trasformaPayloadDefault;

    return mapper(mapping, payload, mappedPayload) as T;
  }

  return mappedPayload;
}
