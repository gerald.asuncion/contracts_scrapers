import joi from 'joi';

export const caseInsensitive = (schema: joi.StringSchema) => schema.insensitive();

export const allowNullAndEmpty = (schema: joi.AnySchema) => schema.allow('', null);

export const isRequired = (schema: joi.AnySchema) => schema.required();

export const yesOrNo = () => joi.string().valid('si', 'no');

export const gasLuceOrDual = () => joi.string().valid('gas', 'luce', 'dual');

export const documentType = () => joi.string().valid("carta d'identità", 'patente', 'passaporto');

export const needDataPower = (ref: string | joi.Reference, schema: joi.AnySchema) => joi.when(ref, {
  is: joi.string().valid('luce', 'dual'),
  then: isRequired(schema),
  otherwise: allowNullAndEmpty(schema)
});

export const needDataGas = (ref: string | joi.Reference, schema: joi.AnySchema) => joi.when(ref, {
  is: joi.string().valid('gas', 'dual'),
  then: isRequired(schema),
  otherwise: allowNullAndEmpty(schema)
});

export const needIfYes = (ref: string | joi.Reference, schema: joi.AnySchema) => joi.when(ref, {
  is: 'si',
  then: isRequired(schema),
  otherwise: allowNullAndEmpty(schema)
});

export const needIfNo = (ref: string | joi.Reference, schema: joi.AnySchema) => joi.when(ref, {
  is: 'no',
  then: isRequired(schema),
  otherwise: allowNullAndEmpty(schema)
});

export const needIf = (ref: string | joi.Reference, is: joi.SchemaLike, schema: joi.AnySchema) => joi.when(ref, {
  is: is,
  then: isRequired(schema),
  otherwise: allowNullAndEmpty(schema)
});
