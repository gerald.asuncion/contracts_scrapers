import * as fs from 'fs';
import { Request, Response, Router } from 'express';
import * as path from 'path';
import GracefulShutdownController from './graceful/GracefulShutdownController';

type HealthCheckOptions = {
  healthCheckFile: string;
  router: Router;
  gracefulShutdownController: GracefulShutdownController
};

export default function healthCheck({ healthCheckFile, router, gracefulShutdownController }: HealthCheckOptions): void {
  router.get('/healthcheck', (req: Request, res: Response) => {
    const { name, version } = JSON.parse(fs.readFileSync(path.resolve(process.cwd(), 'package.json')).toString());

    if (gracefulShutdownController.isTerminable() || !fs.existsSync(healthCheckFile)) {
      res.status(404).json({ name, version });
    } else {
      res.json({ name, version });
    }
  });
}
