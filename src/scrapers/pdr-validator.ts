import joi, { ObjectSchema } from 'joi';

export default (): ObjectSchema => joi.object({
  pdr: joi.string().required()
});
