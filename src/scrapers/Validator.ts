import { Schema } from 'joi';

type Validator = Schema & { unknown: (flag: boolean) => Validator; validateAsync: (val: any) => Promise<any>; };

export default Validator;
