import { scraperArera } from './scraper-gas';

export const scraperLuce = ({ browser }: any) => scraperArera(browser, 'CKE_5', []);

export default scraperLuce;
