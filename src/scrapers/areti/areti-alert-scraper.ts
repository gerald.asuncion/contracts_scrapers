import { Page } from 'puppeteer';
import cheerio from 'cheerio';
import { ScraperInterface, ScraperResult } from '../ScraperInterface';
import { ScraperMailerArgs } from '../../mailer';

const aretiAlertScraper = ({ browser, mailer, toItTeam }: ScraperMailerArgs): ScraperInterface => {
  const credentials = {
    username: 'iride1',
    password: 'dp0013'
  };

  const lastIdFile = '3767886';

  return {
    scrape: async (): Promise<ScraperResult> => {
      const bc = await browser.createIncognitoBrowserContext();
      const m = await mailer;
      const page: Page = await bc.newPage();
      await page.goto('https://pad.areti.it/Documents/Documents.aspx');
      try {
        await page.type('#Email.form-control', credentials.username);
        await page.type('#Password.form-control', credentials.password);
        await page.click('input[type="submit"]');
        await page.goto('https://pad.areti.it/Documents/Documents.aspx');
      } catch (e) {
        // nulla da fare ???
      }
      await page.waitFor(300);
      await page.waitForSelector('#dynTable');
      await page.waitForXPath('//a[contains(text(), "Comunicazioni Pod Contendibili")]');
      const links = await page.$x('//a[contains(text(), "Comunicazioni Pod Contendibili")]');
      if (links.length > 0) {
        await links[0].click();
      } else {
        throw new Error('Link not found');
      }
      await page.waitFor(300);
      await page.waitForXPath('//a[@class="edt"]');

      const tds = await page.$x('//a[@class="edt"]');
      if (tds.length > 0) {
        await tds[tds.length - 1].click();
      } else {
        throw new Error('td not found');
      }

      await page.waitForXPath('//a[@class="edt"]');
      const tbody = await page.$eval('tbody', (e) => e.innerHTML);
      const $ = cheerio.load(tbody);
      const files:any = [];
      // @ts-ignore
      $('td > span').each((i: number, e: CheerioElement) => {
        files[i] = $(e).attr('id');
      });
      const last:string = files.sort().reverse()[0];
      const updated = last == lastIdFile;
      if (!updated) {
        await m.send(toItTeam, 'Aggiornamento POD Areti', '', 'Occorre aggiornare il DB `scrapers` di Areti, caricato un nuovo file nella sezione "Comunicazioni Pod Contendibili"');
      }
      await bc.close();
      return {
        success: updated
      };
    },
    getScraperCodice: () => 'areti-alert'
  };
};

export default aretiAlertScraper;
