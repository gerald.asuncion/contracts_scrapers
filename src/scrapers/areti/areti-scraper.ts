import { ScraperInterface, ScraperResult } from "../ScraperInterface";
import { MysqlArgs } from "../../mysql";

const aretiScraper = ({mysqlPool}: MysqlArgs): ScraperInterface => {
  return {
    scrape: async ({pod}: any): Promise<ScraperResult> => {
      const sql = 'SELECT * FROM areti WHERE `pod` = ? AND `contendibile` = ?';
      const args = [pod, 1];
      try {
        await mysqlPool.queryOne(sql, args);
      } catch (e) {
        return {success: false}
      }
      return {success: true}
    },
    getScraperCodice: () => 'areti'
  }
};

export default aretiScraper;
