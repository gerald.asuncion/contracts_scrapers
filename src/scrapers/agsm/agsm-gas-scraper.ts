import { ScraperArgs, ScraperInterface, ScraperResult } from '../ScraperInterface';

const agsmGasScraper = ({ browser }: ScraperArgs): ScraperInterface => {
  const credential = {
    username: 'IREN',
    password: 'PASSW_IREN_01'
  };

  return {
    scrape: async ({ pdr }: any): Promise<ScraperResult> => {
      const bc = await browser.createIncognitoBrowserContext();
      const page = await bc.newPage();

      await page.goto('http://snc.agsmdistribuzione.it/Portal/Index.aspx?Idn=001E&area=B2B');
      try {
        await page.type('#twsTemplate_Header1_Contenuto_twsModule_txtUser', credential.username);
        await page.type('#twsTemplate_Header1_Contenuto_twsModule_txtPSW', credential.password);
        await page.click('#twsTemplate_Header1_Contenuto_twsModule_btnLogin');
        await page.waitForSelector('#twsTemplate_Header1_Contenuto_twsModule_rptServizi_imgBtn_1');
        await page.click('#twsTemplate_Header1_Contenuto_twsModule_rptServizi_imgBtn_1');
      } catch (e) {
        await page.setCookie({ name: 'KS', value: 'G', domain: 'snc.agsmdistribuzione.it' });
      }
      await page.goto('http://snc.agsmdistribuzione.it/Portal/Index.aspx?m=370G&area=B2B');
      await page.type('#twsTemplate_Header1_Contenuto_twsModule_txtPDR', pdr);
      await page.click('#twsTemplate_Header1_Contenuto_twsModule_btnShow');
      await page.waitForSelector('#twsTemplate_Header1_Contenuto_twsModule_btnShow');
      const content = await page.content();
      await bc.close();
      if (content.includes('twsTemplate_Header1_Contenuto_twsModule_lbl_Err')) {
        return {
          success: false
        };
      }

      return { success: true };
    },
    getScraperCodice: () => 'agsmgas'
  };
};

export default agsmGasScraper;
