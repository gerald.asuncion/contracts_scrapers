import { Comune } from '../repo/ComuniRepo';
import BrowserManager from '../browser/BrowserManager';

export const scraperArera = (browser: BrowserManager, checkboxId: string, cachedResults: CachedResult[] = []): ScraperDistributori => ({
  scrape: async ({ comune }: ScraperDistributoriArgs): Promise<ScraperDistributoriResponse> => {
    const cachedResult = cachedResults.find((cache) => cache.comune.codice == comune.codice);

    if (cachedResult !== undefined) {
      return {
        success: true,
        results: cachedResult.results
      };
    }

    const bc = await browser.createIncognitoBrowserContext();
    const page = await bc.newPage();

    await page.goto('https://www.arera.it/ModuliDinamiciPortale/elencooperatori/elencoOperatoriHome');
    await page.click(`#${checkboxId}`);
    await page.select('#cercaPerComuneComboRegione', comune.regione.codice);
    await page.waitForFunction(`document.getElementById("cercaPerComuneComboProvincia").innerHTML.includes('${comune.provincia.codice}')`);
    await page.select('#cercaPerComuneComboProvincia', comune.provincia.codice);
    await page.waitForFunction(`document.getElementById("cercaPerComuneComboComune").innerHTML.includes('${comune.codice}')`);
    await page.select('#cercaPerComuneComboComune', comune.codice);
    await page.click('#butSub');
    await page.waitForSelector('#item');
    // const content = await page.content();
    await bc.close();
    return { success: true, results: [] };

    /*const root = parse(content);
    const results = root.querySelectorAll('#item tbody tr')
        .map((tr: HTMLElement) => tr.childNodes.find((e: { nodeType: number; }) => e.nodeType != 3))
        // @ts-ignore
        .map((td: HTMLElement) => {
            return td.firstChild.text.replace('\t', '').replace('\n', '').trim();
        });
    cachedResults.push({comune, results});
    return {success:true, results}*/
  }
});

export interface ScraperDistributoriArgs {
  comune: Comune
}

export interface ScraperDistributori {
  scrape(args: ScraperDistributoriArgs): Promise<ScraperDistributoriResponse>
}

export interface ScraperDistributoriResponse {
  results: string[]
  success: boolean
}

export interface CachedResult {
  comune: Comune,
  results: string[]
}

const scraperGas = ({ browser }: any) => scraperArera(browser, 'CKG_2', []);

export default scraperGas;
