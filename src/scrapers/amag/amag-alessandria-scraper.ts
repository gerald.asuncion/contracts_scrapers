import { ScraperInterface, ScraperResult, ScraperArgs } from '../ScraperInterface';

const amagAlessandriaScraper = ({ browser }: ScraperArgs): ScraperInterface => {
  const credential = {
    username: 'FD7300',
    password: '8LA7RULA'
  };

  return {
    scrape: async ({ pdr }: any): Promise<ScraperResult> => {
      const bc = await browser.createIncognitoBrowserContext();
      const page = await bc.newPage();
      await page.goto('http://gruppoamag.terranovasoftware.eu/Portal/Index.aspx?m=831G&area=B2B');

      try {
        await page.type('#twsTemplate_Content2_twsModule_txtUser', credential.username);
        await page.type('#twsTemplate_Content2_twsModule_txtPSW', credential.password);
        await page.click('#twsTemplate_Content2_twsModule_btnLogin');
        await page.goto('http://gruppoamag.terranovasoftware.eu/Portal/Index.aspx?m=831G&area=B2B');
      } catch (e) {
        // nulla da fare ???
      }
      // await page.waitForSelector('twsTemplate_Content2_twsModule_txtPdr', pdr);
      await page.type('#twsTemplate_Content2_twsModule_txtPdr', pdr);
      await page.click('#twsTemplate_Content2_twsModule_btnSearchPDP');
      await page.waitForSelector('#twsTemplate_Content2_twsModule_lblMsg');
      const content = await page.content();
      const isInvalid = content.includes('PDR trovati: 0') || (content.includes('PDR trovati: 1') && !content.includes('posato e chiuso'));
      await bc.close();

      return {
        success: !isInvalid
      };
    },
    getScraperCodice: () => 'amag'
  };
};

export default amagAlessandriaScraper;
