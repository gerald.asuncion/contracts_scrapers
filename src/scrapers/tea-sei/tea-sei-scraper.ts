import { Page } from 'puppeteer';
import { ScraperArgs, ScraperInterface, ScraperResult } from '../ScraperInterface';

const teaSeiScraper = ({ browser }: ScraperArgs): ScraperInterface => {
  const credentials = {
    username: 'FRUSSO',
    password: 'TEA00142'
  };

  return {
    scrape: async ({ pdr }: any): Promise<ScraperResult> => {
      const bc = await browser.createIncognitoBrowserContext();
      const page: Page = await bc.newPage();
      await page.goto('https://teaswitch.teaspa.it/RetiGas/Index.aspx?idn=0009&area=B2B');
      try {
        await page.type('#twsTemplate_Content2_twsModule_txtUser', credentials.username);
        await page.type('#twsTemplate_Content2_twsModule_txtPSW', credentials.password);
        await page.click('#twsTemplate_Content2_twsModule_btnLogin');
        await page.goto('https://teaswitch.teaspa.it/RetiGas/Index.aspx?m=835G&area=B2B');
      } catch (e) {
        // nulla da fare ???
      }
      await page.goto('https://teaswitch.teaspa.it/RetiGas/Index.aspx?m=835G&area=B2B');
      await page.select('#twsTemplate_Content2_twsModule_SceltaTracciato1_ddlCodServizio', 'A01');

      await page.waitForSelector('#twsTemplate_Content2_twsModule_cTracciati_cCercaPDR_btnOpenCercaPDP');
      await page.click('#twsTemplate_Content2_twsModule_cTracciati_cCercaPDR_btnOpenCercaPDP');
      await page.waitForResponse('https://teaswitch.teaspa.it/RetiGas/Index.aspx?m=835G&area=B2B');
      await page.waitFor(300);
      await page.type('#twsTemplate_Content2_twsModule_cTracciati_cCercaPDR_txtCodicePDP', pdr);
      await page.click('#twsTemplate_Content2_twsModule_cTracciati_cCercaPDR_btn_Cerca');
      await page.waitForResponse('https://teaswitch.teaspa.it/RetiGas/Index.aspx?m=835G&area=B2B');
      await page.waitFor(300);
      let isInvalid;
      try {
        await page.waitForSelector('.messageError');
        const content = await page.content();
        isInvalid = content.includes('PDR non trovato');
      } catch (e) {
        isInvalid = false;
      }
      await bc.close();
      return {
        success: !isInvalid
      };
    },
    getScraperCodice: () => 'teasei'
  };
};

export default teaSeiScraper;
