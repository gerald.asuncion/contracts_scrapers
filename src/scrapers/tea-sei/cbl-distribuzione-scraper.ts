import { Page } from 'puppeteer';
import { ScraperArgs, ScraperInterface, ScraperResult } from '../ScraperInterface';

export default ({ browser }: ScraperArgs): ScraperInterface => {
  const credentials = {
    username: 'F.MUTTI',
    password: 'TTURQ4D2'
  };

  const baseUrl = 'http://95.110.171.12/Portal/Index.aspx';

  return {
    scrape: async ({ pdr }: any): Promise<ScraperResult> => {
      const bc = await browser.createIncognitoBrowserContext();
      const page: Page = await bc.newPage();
      await page.goto(`${baseUrl}?Idn=0004&area=B2B`);
      try {
        await page.type('#twsTemplate_Content1_twsModule_txtUser', credentials.username);
        await page.type('#twsTemplate_Content1_twsModule_txtPSW', credentials.password);
        await page.click('#twsTemplate_Content1_twsModule_btnLogin');
        await page.goto(`${baseUrl}?m=835G&area=B2B`);
      } catch (e) {
        // nulla da fare
      }
      await page.goto(`${baseUrl}?m=835G&area=B2B`);
      await page.select('#twsTemplate_Content2_twsModule_SceltaTracciato1_ddlCodServizio', 'A01');

      await page.waitForSelector('#twsTemplate_Content2_twsModule_cTracciati_cCercaPDR_btnOpenCercaPDP');
      await page.click('#twsTemplate_Content2_twsModule_cTracciati_cCercaPDR_btnOpenCercaPDP');
      await page.waitForResponse(`${baseUrl}?m=835G&area=B2B`);
      await page.waitFor(300);
      await page.type('#twsTemplate_Content2_twsModule_cTracciati_cCercaPDR_txtCodicePDP', pdr);
      await page.click('#twsTemplate_Content2_twsModule_cTracciati_cCercaPDR_btn_Cerca');
      await page.waitForResponse(`${baseUrl}?m=835G&area=B2B`);
      await page.waitFor(300);
      let isInvalid;
      try {
        await page.waitForSelector('.messageError');
        const content = await page.content();
        isInvalid = content.includes('PDR non trovato');
      } catch (e) {
        isInvalid = false;
      }
      await bc.close();
      return {
        success: !isInvalid
      };
    },
    getScraperCodice: () => 'cbl'
  };
};
