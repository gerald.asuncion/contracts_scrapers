import { Page } from 'puppeteer';
import fs from 'fs';
import path from 'path';
import unzip from 'unzipper';
import csvParser from 'csv-parse';
import { MysqlPoolInterface } from '../../mysql';
import { MailerInterface } from '../../mailer';
import { ScraperInterface, ScraperResult } from '../ScraperInterface';
import { Logger } from '../../logger';
import BrowserManager from '../../browser/BrowserManager';
import waitForSelectorAndClick from '../../browser/page/waitForSelectorAndClick';
import waitForNavigation from '../../browser/page/waitForNavigation';
import waitCheckCredentials from '../../browser/page/waitCheckCredentials';

export default ({
  browser, mailer, mysqlPool, logger, baseDownloadPath, toItTeam
}: UnaretiArgs): ScraperInterface => {
  const credentials = {
    username: 'F_MUTTI',
    password: 'Mercato5'
  };

  const getCsvData = async (filename: string): Promise<string[][]> => new Promise((resolve, reject) => {
    const input = fs.readFileSync(filename, { encoding: 'utf8' });
    csvParser(input, {
      delimiter: ';', skip_empty_lines: true, skip_lines_with_error: true, from_line: 2
    }, (err, records, info) => {
      if (err) {
        reject(err);
      } else {
        logger.info(`- Trovate ${records.length} righe da inserire`);
        resolve(records);
      }
    });
  });

  const unzipCsv = async (downloadPath: string): Promise<string> => new Promise((resolve, reject) => {
    const filesZip = fs.readdirSync(downloadPath);
    const zip = filesZip.find((f) => path.extname(f).toLowerCase() === '.zip');
    if (zip) {
      fs.createReadStream(`${downloadPath}/${zip}`).pipe(unzip.Extract({ path: downloadPath })).on('finish', resolve);
      logger.info(`- estraggo il file ${zip}`);
    }
  });

  return {
    scrape: async (): Promise<ScraperResult> => {
      const bc = await browser.createIncognitoBrowserContext();
      const m = await mailer;
      const page: Page = await bc.newPage();

      try {
        logger.info('### Aggiornamento della tabella scrapers.unareti');
        const loginUrl = 'https://netgategas.unareti.it/Netgate/login.do?adfgenDispatchAction=confirm';
        const response: any = await page.goto(loginUrl);
        const chain = response.request().redirectChain();
        if (chain.length > 0) {
          await page.type('#j_username', credentials.username);
          await page.type('#j_password', credentials.password);
          await page.waitFor(1000);
          await Promise.all([
            waitForSelectorAndClick(page, 'img[alt="ENTRA"]'),
            waitForNavigation(page)
          ]);
          await waitCheckCredentials(page, '#message', loginUrl, credentials.username, logger);
        }
        await page.waitForSelector('#manageLoginForm');
        const client = await page.target().createCDPSession();
        const datetime = new Date();
        const downloadPath = `${baseDownloadPath}/unareti/${datetime.getFullYear()}-${datetime.getMonth() + 1}-${datetime.getDate()}`;
        if (!fs.existsSync(downloadPath)) {
          fs.mkdirSync(downloadPath, { recursive: true });
          await page.waitFor(3000);
        }
        if (!fs.existsSync(downloadPath)) {
          await m.send(toItTeam, 'Alert Unareti Gas Updater', '', `non è riuscito a creare la cartella ${downloadPath}`);
          return { success: false };
        }
        await client.send('Page.setDownloadBehavior', {
          behavior: 'allow', downloadPath
        });
        try {
          logger.info('- Download del file CSV zippato');
          await page.goto('https://netgategas.unareti.it/Netgate/manageSitiContendibili.do?adfgenDispatchAction=download', { timeout: 30000 });
        } catch (e) {
          logger.info('  -> ha bombato, ma niente panico lo fa sempre');
          await page.waitFor(3000);
        }
        logger.info('- Unzip del file');
        await page.close();
        await unzipCsv(downloadPath);
        const files: string[] = fs.readdirSync(downloadPath) || [];
        const file: string | undefined = files.find((f) => path.extname(f).toLowerCase() == '.csv');
        if (file == undefined) {
          logger.error('Errore Unareti Updater');
          await m.send(toItTeam, 'Alert Unareti Gas Updater', '', 'non è riuscito a trovare il file csv');
          return { success: false };
        }
        logger.info('- Truncate della tabella scrapers.unareti');
        await mysqlPool.truncateTable('unareti');
        const records = await getCsvData(`${downloadPath}/${file}`);
        logger.info('- Inserimento in scrapers.unareti dei dati presenti nel CSV');
        await mysqlPool.insertIntoChunk('unareti', records, 100);
        logger.info('### Fine Aggiornamento');

        fs.readdirSync(downloadPath).map((f) => fs.unlinkSync(path.join(downloadPath, f)));
        fs.rmdirSync(downloadPath);
      } catch (e: any) {
        const isClose = await page.isClosed();
        if (!isClose) {
          await page.close();
        }
        await m.send(toItTeam, 'Alert Unareti GAS Updater', '', `${e.message}\n${e.stack}`);
        return { success: false };
      }

      await bc.close();

      return { success: true };
    },
    getScraperCodice: () => 'unareti'
  };
};

export interface UnaretiArgs {
  browser: BrowserManager,
  mailer: MailerInterface,
  mysqlPool: MysqlPoolInterface,
  logger: Logger,
  baseDownloadPath: string,
  toItTeam: string
}
