import {ScraperInterface, ScraperResult} from "../ScraperInterface";
import {MysqlArgs} from "../../mysql";

export default ({mysqlPool}: MysqlArgs): ScraperInterface => {
  return {
    scrape: async ({pod}: any): Promise<ScraperResult> => {
      const sql = 'SELECT * FROM unareti_luce WHERE `Codice POD` = ? AND `Stato del punto` = ?';
      const args = [pod, 'DISATTIVATO'];
      try {
        await mysqlPool.queryOne(sql, args);
      } catch (e: any) {
        return {success: false, results: e.toString()}
      }
      return {success: true}
    },
    getScraperCodice: () => 'unareti-luce'
  }
};
