import {ScraperInterface, ScraperResult} from "../ScraperInterface";
import {MysqlArgs} from "../../mysql";

export default ({mysqlPool}: MysqlArgs): ScraperInterface => {
  return {
    scrape: async ({pdr}: any): Promise<ScraperResult> => {
      const sql = 'SELECT * FROM unareti WHERE `Codice PdR` = ? AND `Stato del punto` = ?';
      const args = [pdr, 'DISATTIVATO'];
      try {
        await mysqlPool.queryOne(sql, args);
      } catch (e: any) {
        return {success: false, results: e.toString()};
      }
      return {success: true};
    },
    getScraperCodice: () => 'unareti'
  }
};
