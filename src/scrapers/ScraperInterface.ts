import BrowserManager from '../browser/BrowserManager';

export interface ScraperInterface {
  scrape: (args: any, params?: any) => Promise<ScraperResult>;
  getScraperCodice: () => string;
}

export interface ScraperResult {
  success: boolean,
  results?: any[]
}

export interface ScraperArgs {
  browser: BrowserManager
}
