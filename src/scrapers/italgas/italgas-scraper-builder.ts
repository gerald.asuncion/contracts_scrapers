import { Page, Protocol } from 'puppeteer';
import { ScraperResult } from '../ScraperInterface';
import BrowserManager from '../../browser/BrowserManager';

export default ({ browser, dominio }:ItalgasConfig) => {
  const credentials = {
    username: 'RIE13ZS',
    password: 'CHIWAY!1'
  };

  let cookies: Protocol.Network.CookieParam[] = [];

  return {
    scrape: async ({ pdr }: any): Promise<ScraperResult> => {
      const bc = await browser.createIncognitoBrowserContext();
      const page: Page = await bc.newPage();
      await page.goto('https://servizi-online.italgas.it/gasonlineWeb/initRicercaSIQ001.action?clearForm=true');
      await page.setCookie(...cookies);
      await page.goto('https://servizi-online.italgas.it/gasonlineWeb/initRicercaSIQ001.action?clearForm=true');
      try {
        await page.type('#username', credentials.username);
        await page.type('#password', credentials.password);
        await page.select('select[name=dominio]', dominio);
        await page.click('.ladda-button');
        await page.goto('https://servizi-online.italgas.it/gasonlineWeb/initRicercaSIQ001.action?clearForm=true');
        cookies = await page.cookies();
      } catch (e) {
        // nulla da fare ???
      }

      await page.type('#pdr', pdr);
      await page.click('#report');
      await page.reload();
      const content = await page.content();
      const isInvalid = content.includes('Errore');

      await page.close();
      await bc.close();

      return {
        success: !isInvalid
      };
    }
  };
};

export interface ItalgasScraperRequest {
  pdr: string
}

export interface ItalgasScraperArgs {
  browser: BrowserManager
}

export interface ItalgasConfig {
  dominio : string,
  browser: BrowserManager
}
