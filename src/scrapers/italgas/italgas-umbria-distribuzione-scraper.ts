import builder, { ItalgasScraperArgs } from './italgas-scraper-builder';

export default ({ browser }:ItalgasScraperArgs) => builder({ browser, dominio: '01356930550' });
