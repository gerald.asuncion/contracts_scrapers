import builder from "../terranova/terranova-scraper-builder";
import { ScraperArgs } from "../ScraperInterface";

export default ({ browser }: ScraperArgs) => builder(browser, {
  credentials: {
    username: 'C467B3',
    password: 'CRC63QXX'
  },
  loginUrl: 'http://54.229.250.70/Portal/Index.aspx?idn=008F&area=B2B',
  searchUrl: 'http://54.229.250.70/Portal/Index.aspx?m=831G&area=B2B',
  fields: {
    username: '#twsTemplate_Content2_twsModule_txtUser',
    password: '#twsTemplate_Content2_twsModule_txtPSW',
    loginBtn: '#twsTemplate_Content2_twsModule_btnLogin',
    pdr: '#twsTemplate_Content2_twsModule_txtPdr',
    search: '#twsTemplate_Content2_twsModule_btnSearchPDP',
    message: '#twsTemplate_Content2_twsModule_lblMsg',
  }
});
