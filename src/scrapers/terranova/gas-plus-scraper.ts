import builder from "../terranova/terranova-scraper-builder";
import { ScraperArgs } from "../ScraperInterface";

export default ({browser}: ScraperArgs) => builder(browser, {
  credentials: {
    username: 'IRENMERCATO1',
    password: 'IREN9877'
  },
  loginUrl: 'http://95.110.171.5/Portal/Index.aspx?idn=001A&mode=GPRETI&area=B2B',
  searchUrl: 'http://95.110.171.5/Portal/Index.aspx?m=831G&area=B2B',
  fields: {
    username: '#twsTemplate_Content2_twsModule_txtUser',
    password: '#twsTemplate_Content2_twsModule_txtPSW',
    loginBtn: '#twsTemplate_Content2_twsModule_btnLogin',
    pdr: '#twsTemplate_Content2_twsModule_txtPdr',
    search: '#twsTemplate_Content2_twsModule_btnSearchPDP',
    message: '#twsTemplate_Content2_twsModule_lblMsg',
  }
});
