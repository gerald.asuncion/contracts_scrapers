import builder from "../terranova/terranova-scraper-builder";
import { ScraperArgs } from "../ScraperInterface";

export default ({browser}: ScraperArgs) => builder(browser, {
  credentials: {
    username: '06033C',
    password: 'K85JDJWK'
  },
  loginUrl: 'http://www.assisigestioniservizi.it/Portal/index.aspx?idn=0012&area=b2b',
  searchUrl: 'http://www.assisigestioniservizi.it/Portal/index.aspx?m=831G&area=B2B',
  fields: {
    username: '#twsTemplate_Content2_twsModule_txtUser',
    password: '#twsTemplate_Content2_twsModule_txtPSW',
    loginBtn: '#twsTemplate_Content2_twsModule_btnLogin',
    pdr: '#twsTemplate_Content2_twsModule_txtPdr',
    search: '#twsTemplate_Content2_twsModule_btnSearchPDP',
    message: '#twsTemplate_Content2_twsModule_lblMsg',
  },
  cookies: [{
    name: 'KS',
    value: 'G'
  }]
});
