import builder from './terranova-scraper-builder';
import { ScraperArgs, ScraperInterface } from '../ScraperInterface';

export default ({ browser }: ScraperArgs): ScraperInterface => builder(browser, {
  credentials: {
    username: '1DABEA',
    password: 'FRANCI25'
  },
  loginUrl: 'http://tn.retipiu.it/Distribuzione/Index.aspx?idn=0025&area=B2B',
  searchUrl: 'http://tn.retipiu.it/Distribuzione/Index.aspx?m=831G&area=B2B',
  fields: {
    username: '#twsTemplate_Content2_twsModule_txtUser',
    password: '#twsTemplate_Content2_twsModule_txtPSW',
    loginBtn: '#twsTemplate_Content2_twsModule_btnLogin',
    pdr: '#twsTemplate_Content2_twsModule_txtPdr',
    search: '#twsTemplate_Content2_twsModule_btnSearchPDP',
    message: '#twsTemplate_Content2_twsModule_lblMsg',
  },
  cookies: [{
    name: 'KS',
    value: 'G'
  }]
});
