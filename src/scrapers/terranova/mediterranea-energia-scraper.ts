import builder from "../terranova/terranova-scraper-builder";
import { ScraperArgs } from "../ScraperInterface";

export default ({browser}: ScraperArgs) => builder(browser, {
  credentials: {
    username: 'FA6245',
    password: 'YKP7AG7M'
  },
  loginUrl: 'http://portale.mediterraneaenergia.net/Portal/Index.aspx?idn=000A&area=B2B',
  searchUrl: 'http://portale.mediterraneaenergia.net/Portal/Index.aspx?m=831G&area=B2B',
  fields: {
    username: '#twsTemplate_Header1_Contenuto_twsModule_txtUser',
    password: '#twsTemplate_Header1_Contenuto_twsModule_txtPSW',
    loginBtn: '#twsTemplate_Header1_Contenuto_twsModule_btnLogin',
    pdr: '#twsTemplate_Header1_Contenuto_twsModule_txtPdr',
    search: '#twsTemplate_Header1_Contenuto_twsModule_btnSearchPDP',
    message: '#twsTemplate_Header1_Contenuto_twsModule_lblMsg',
  }
});
