import { Page, Protocol } from 'puppeteer';
import { ScraperInterface, ScraperResult } from '../ScraperInterface';
import BrowserManager from '../../browser/BrowserManager';

export default (browser: BrowserManager, {
  credentials, searchUrl, loginUrl, fields, cookies = []
}: TerranovaBuilderLuceArgs): ScraperInterface => {
  const login = loginUrl || searchUrl;

  return {
    scrape: async ({ pod }: any): Promise<ScraperResult> => {
      const bc = await browser.createIncognitoBrowserContext();
      const page: Page = await bc.newPage();
      const response: any = await page.goto(searchUrl);
      const chain = response.request().redirectChain();
      await page.setCookie(...cookies);
      try {
        await page.type(fields.username, credentials.username);
        await page.type(fields.password, credentials.password);
        await page.click(fields.loginBtn);
        await page.goto(searchUrl);
      } catch (e) {
        if (chain.length > 0) {
          await page.goto(login);
          await page.type(fields.username, credentials.username);
          await page.type(fields.password, credentials.password);
          await page.click(fields.loginBtn);
          await page.goto(searchUrl);
        }
      }

      await page.waitForSelector(fields.pod);

      await page.type(fields.pod, pod);
      await page.click(fields.search);
      await page.waitForSelector(fields.message);
      const content = await page.content();
      const isInvalid = content.includes('POD trovati: 0') || (content.includes('POD trovati: 1') && !content.includes('posato e chiuso'));
      await bc.close();

      return {
        success: !isInvalid
      };
    },
    getScraperCodice: () => 'terranova-luce'
  };
};

export interface TerranovaBuilderLuceArgs {
  credentials: {
    username: string,
    password: string,
  },
  loginUrl?: string,
  searchUrl: string,
  cookies?: Protocol.Network.CookieParam[],
  fields: {
    username: string,
    password: string,
    loginBtn: string,
    pod: string,
    search: string,
    message: string
  }
}
