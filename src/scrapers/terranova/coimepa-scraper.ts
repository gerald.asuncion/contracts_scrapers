import builder from "../terranova/terranova-scraper-builder";
import { ScraperArgs } from "../ScraperInterface";

export default ({browser}: ScraperArgs) => builder(browser, {
  credentials: {
    username: 'IRENMERCATO',
    password: 'TKGMJ00G'
  },
  loginUrl: 'https://www.asretigas.it/Portal/Index.aspx?idn=0091&area=B2B&mode=PRODCO',
  searchUrl: 'https://www.asretigas.it/Portal/Index.aspx?m=831G&area=B2B',
  fields: {
    username: '#twsTemplate_Header1_Contenuto_twsModule_txtUser',
    password: '#twsTemplate_Header1_Contenuto_twsModule_txtPSW',
    loginBtn: '#twsTemplate_Header1_Contenuto_twsModule_btnLogin',
    pdr: '#twsTemplate_Header1_Contenuto_twsModule_txtPdr',
    search: '#twsTemplate_Header1_Contenuto_twsModule_btnSearchPDP',
    message: '#twsTemplate_Header1_Contenuto_twsModule_lblMsg',
  },
  cookies: [{
    name: 'KS',
    value: 'G'
  }]
});
