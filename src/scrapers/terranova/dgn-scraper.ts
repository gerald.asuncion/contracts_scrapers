import builder from "../terranova/terranova-scraper-builder";
import { ScraperArgs } from "../ScraperInterface";

export default ({browser}: ScraperArgs) => builder(browser, {
  credentials: {
    username: 'V00018',
    password: 'V00018.13'
  },
  loginUrl: 'http://terranovaws.dgn-net.it/Portal/Index.aspx?idn=003A&area=B2B',
  searchUrl: 'http://terranovaws.dgn-net.it/Portal/Index.aspx?m=831G&area=B2B',
  fields: {
    username: '#twsTemplate_Header1_Contenuto_twsModule_txtUser',
    password: '#twsTemplate_Header1_Contenuto_twsModule_txtPSW',
    loginBtn: '#twsTemplate_Header1_Contenuto_twsModule_btnLogin',
    pdr: '#twsTemplate_Header1_Contenuto_twsModule_txtPdr',
    search: '#twsTemplate_Header1_Contenuto_twsModule_btnSearchPDP',
    message: '#twsTemplate_Header1_Contenuto_twsModule_lblMsg',
  },
  cookies: [{
    name: 'KS',
    value: 'G'
  }]
});
