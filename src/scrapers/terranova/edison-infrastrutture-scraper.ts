import builder from "../terranova/terranova-scraper-builder";
import { ScraperArgs } from "../ScraperInterface";

export default ({browser}: ScraperArgs) => builder(browser, {
  credentials: {
    username: '7CBDA0',
    password: 'YSWHW1Y6'
  },
  loginUrl: 'https://portale.infrastrutturedg.it/Portal/Index.aspx?idn=0099&area=B2B',
  searchUrl: 'https://portale.infrastrutturedg.it/Portal/Index.aspx?m=831G&area=B2B',
  fields: {
    username: '#twsTemplate_Content1_twsModule_txtUser',
    password: '#twsTemplate_Content1_twsModule_txtPSW',
    loginBtn: '#twsTemplate_Content1_twsModule_btnLogin',
    pdr: '#twsTemplate_Content2_twsModule_txtPdr',
    search: '#twsTemplate_Content2_twsModule_btnSearchPDP',
    message: '#twsTemplate_Content2_twsModule_lblMsg',
  },
  cookies: [{
    name: 'KS',
    value: 'G'
  }]
});
