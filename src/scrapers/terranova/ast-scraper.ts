import builder from "../terranova/terranova-scraper-builder";
import { ScraperArgs } from "../ScraperInterface";

export default ({browser}: ScraperArgs) => builder(browser, {
  credentials: {
    username: 'IREN',
    password: 'IRENME789'
  },
  loginUrl: 'http://distribuzione.zeccaonline.it/Portal/Index.aspx?idn=00B9&area=B2B',
  searchUrl: 'http://distribuzione.zeccaonline.it/Portal/Index.aspx?m=831G&area=B2B',
  fields: {
    username: '#twsTemplate_Content1_twsModule_txtUser',
    password: '#twsTemplate_Content1_twsModule_txtPSW',
    loginBtn: '#twsTemplate_Content1_twsModule_btnLogin',
    pdr: '#twsTemplate_Content2_twsModule_txtPdr',
    search: '#twsTemplate_Content2_twsModule_btnSearchPDP',
    message: '#twsTemplate_Content2_twsModule_lblMsg',
  },
  cookies: [{
    name: 'KS',
    value: 'G'
  }]
});
