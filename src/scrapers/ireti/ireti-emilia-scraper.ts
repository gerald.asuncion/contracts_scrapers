import builder from "../terranova/terranova-scraper-builder";
import { ScraperArgs } from "../ScraperInterface";

export default ({ browser }: ScraperArgs) => builder(browser, {
  credentials: {
    username: 'IRENMERCATO',
    password: 'LAVS18AU'
  },
  searchUrl: 'https://snc.eniaspa.it/Portal/Index.aspx?m=831G&area=B2B',
  fields: {
    username: '#twsTemplate_Content2_twsModule_txtUser',
    password: '#twsTemplate_Content2_twsModule_txtPSW',
    loginBtn: '#twsTemplate_Content2_twsModule_btnLogin',
    pdr: '#twsTemplate_Content2_twsModule_txtPdr',
    search: '#twsTemplate_Content2_twsModule_btnSearchPDP',
    message: '#twsTemplate_Content2_twsModule_lblMsg',
  }
});
