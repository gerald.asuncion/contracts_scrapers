import { Page } from 'puppeteer';
import { ScraperInterface, ScraperResult } from '../ScraperInterface';
import BrowserManager from '../../browser/BrowserManager';

const iretiScraper = ({ browser }: IretiScraperArgs): ScraperInterface => {
  const credential = {
    username: 'MAGTUT',
    password: 'IrenMercato21'
  };

  return {
    scrape: async ({ pod }: any): Promise<ScraperResult> => {
      const bc = await browser.createIncognitoBrowserContext();
      const page: Page = await bc.newPage();
      await page.goto('http://ee.ireti.it/CNRGWebDistributore/richiestanuovaoperazione.do?todo=interrogaPod');
      let content = await page.content();
      if (content.includes('Effettuare nuovamente il login')) {
        await page.goto('http://ee.ireti.it/CNRGWebDistributore/login.do?distributore=aemd');
        await page.type('input[name=user]', credential.username);
        await page.type('input[name=password]', credential.password);
        await page.click('input[type=submit]');
        await page.goto('http://ee.ireti.it/CNRGWebDistributore/richiestanuovaoperazione.do?todo=interrogaPod');
      }
      await page.click('input[name=pod]');
      await page.keyboard.down('Control');
      await page.keyboard.press('KeyA');
      await page.keyboard.up('Control');
      await page.keyboard.press('Backspace');
      await page.type('input[name=pod]', pod);
      await page.click('input[name=bottone]');
      await page.waitForSelector('.tab_dati');
      content = await page.content();
      const isInvalid = content.includes('Error: 00');
      await bc.close();
      return {
        success: !isInvalid
      };
    },
    getScraperCodice: () => 'ireti'
  };
};

interface IretiScraperArgs {
  browser: BrowserManager
}

export default iretiScraper;
