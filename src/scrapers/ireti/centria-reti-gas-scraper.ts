import builder from "../terranova/terranova-scraper-builder";
import { ScraperArgs } from "../ScraperInterface";

export default ({ browser }: ScraperArgs) => builder(browser, {
  credentials: {
    username: 'RUSSO',
    password: 'TDWWFT32'
  },
  searchUrl: 'http://retigas.consiag.it/Portal/Index.aspx?m=831G&area=B2B',
  fields: {
    username: '#twsTemplate_Content2_twsModule_txtUser',
    password: '#twsTemplate_Content2_twsModule_txtPSW',
    loginBtn: '#twsTemplate_Content2_twsModule_btnLogin',
    pdr: '#twsTemplate_Content1_twsModule_txtPdr',
    search: '#twsTemplate_Content1_twsModule_btnSearchPDP',
    message: '#twsTemplate_Content1_twsModule_lblMsg',
  }
});
