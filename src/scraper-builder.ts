import { NextFunction, Request, Response, Router } from 'express';
import { ScraperInterface } from './scrapers/ScraperInterface';
import ScraperConfig from './config/ScraperConfig';
import Validator from './scrapers/Validator';
import { Logger } from './logger';
import extractErrorMessage from './utils/extractErrorMessage';
import extractErrorStackTrace from './utils/extractErrorStackTrace';
import ScraperBuilder from './ScraperBuilder';
import ErrorResponse from './response/ErrorResponse';
import RequestWithContainer from './RequestWithContainer';
import FailureResponse from './response/FailureResponse';
import GracefulShutdownController from './graceful/GracefulShutdownController';
import { trasformaPayload } from './mapping/trasformaPayload';

interface ScraperBuilderArgs {
  router: Router,
  scraperList: ScraperConfig[],
  logger: Logger,
  healthCheck: any
  irenRemiCsvImporter: any,
  atenaCsvImporter: any
  agsmGasUploadCsv: unknown,
  irenRemiXlsImporter: unknown,
  agsmGasUploadXls: unknown
}

export default ({
  router,
  scraperList,
  logger,

  // sono qui solo per fare in modo che awilix li istanzi
  // si potrebbe fare diversamente
  healthCheck,
  irenRemiCsvImporter,
  atenaCsvImporter,
  agsmGasUploadCsv,
  irenRemiXlsImporter,
  agsmGasUploadXls
}: ScraperBuilderArgs): ScraperBuilder => {
  const scraperEnabled = scraperList.filter((s) => s.enabled);

  const buildValidator = (validatorName: string) => async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const validator: Validator = (req as RequestWithContainer).container.resolve(validatorName);
      await validator.unknown(true).validateAsync(req.body);
      next();
    } catch (e) {
      res.status(400).json(new FailureResponse(extractErrorMessage(e)));
    }
  };

  const buildController = (scraperName: string) => async (req: Request, res: Response): Promise<void> => {
    try {
      const containerScoped = (req as RequestWithContainer).container;
      const gracefulShutdownController = containerScoped.resolve<GracefulShutdownController>('gracefulShutdownController');

      if (gracefulShutdownController.isTerminable()) {
        res.status(503).json(new ErrorResponse('sistema in aggiornamento'));
      } else {
        const realScraper: ScraperInterface = containerScoped.resolve(scraperName);

        const scraperCodice = realScraper.getScraperCodice();
        const scraperResult = await realScraper.scrape(await trasformaPayload(scraperCodice, req.body), req.params);

        res.json(scraperResult);
      }
    } catch (e) {
      const errMsg = extractErrorMessage(e);
      const stackTrace = extractErrorStackTrace(e);
      logger.error(`${scraperName} scraper failed - ${errMsg} ${stackTrace}`);
      res.json(new ErrorResponse(errMsg)).status(500);
    }
  };

  const buildScrapers = () => {
    scraperEnabled.forEach(({ scraper, validator = 'emptyValidator', path }: ScraperConfig) => {
      const realValidator = buildValidator(validator);
      const controller = buildController(scraper);
      router.post(path, realValidator, controller);
    });
  };

  return {
    build: () => {
      buildScrapers();
    },
  };
};
