import axios from 'axios';
import { TryOrThrowError } from '../utils/tryOrThrow';
import {Logger} from "../logger";

type RecordBE = {
  id: number;
  dataCreazione: string; // Date
  dataAggiornamento: string; // Date
  dataEliminazione: string | null; // Date
};

type CallCenterBE = RecordBE & {
  nome: string;
  note: string | null;
};

type CredenzialiScraperBE = RecordBE & {
  username: string;
  password: string;
  dataUltimaModificaPassword: string; // Date
  durataPassword: number;
  reminder: boolean;
  email: string;
  note: string | null;
};

type RelCredenzialiScraperBE = RecordBE & {
  credenzialiId: number;
  callCenterId: number | null;
  scraperId: number;
  callCenter: CallCenterBE;
  credenziali: CredenzialiScraperBE;
};

type ScraperBE = RecordBE & {
  id: number;
  codice: string;
  label: string;
  partner: string;
  tipologia: string;
  relCredenziali: RelCredenzialiScraperBE[];
};

export type Credenziali = {
  username: string;
  password: string;
};

export type BasicPayload = {
  callCenter?: string;
};

export type BasicCredenzialiPayload = Partial<Credenziali> & BasicPayload;

export default async function getCredenzialiDalServizio<T extends BasicPayload>(logger: Logger, baseUrl: string, scraperCodice: string, payload?: T): Promise<Credenziali> {
  try {
    const response = await axios.get(`${baseUrl}/v1/relcredenziali/credenziali-round-robin?codice=${scraperCodice}`);
    // logger.info(JSON.stringify(response.data));
    logger.info(`[Credenziali::getCredenzialiDalServizio:round-robin][codice=${scraperCodice}] ${JSON.stringify(response.data)}`);

    return response.data;
  } catch (e: any) {
    logger.error("Errore nel recupero delle credenziali round robin");
    logger.error(e.toString());
  }


  const scraper = await axios.get<ScraperBE[]>(`${baseUrl}/v1/scrapers/full?filter[codice]=${scraperCodice}`).then(resp => resp.data[0]);
  const relazioni = scraper.relCredenziali;

  let credenziali: Credenziali | undefined;
  const callCenter = payload && payload.callCenter;
  if (callCenter) {
    credenziali = relazioni.find((rel) => rel.callCenter.nome.toLowerCase() === callCenter.toLowerCase())?.credenziali;
  } else {
    credenziali = relazioni[0].credenziali;
  }

  if (!credenziali) {
    throw new TryOrThrowError('credenziali non trovate');
  }

  logger.info(`[Credenziali::getCredenzialiDalServizio:flat][codice=${scraperCodice}] ${JSON.stringify(credenziali)}`);

  return credenziali;
}
