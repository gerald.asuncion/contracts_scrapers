import axios, { AxiosRequestConfig } from "axios";
import { readFile } from "fs/promises";
import https from "https";
import { Page, PageEventObject } from "puppeteer";
import { Logger } from "../logger";

const urlsImageDataSplit = ';base64,';
const urlsWithImageData = [
  'data:image/png;base64,',
  'data:image/svg+xml;base64,'
];

// template literals require ts 4.1
// export type CertificateOptions<T extends 'p12' | 'pfx' | 'cert' | 'cer' | 'key' = any, TName extends string = string> = {
//   path: `${TName}.${T}`
// } & (T extends 'p12' | 'pfx' ? {
//   passphrase: string;
// } : {});

export type CertificateOptions<T extends 'p12' | 'pfx' | 'cert' | 'cer' | 'key' = any> = {
  manual?: true;
  additionalHeaders?: Record<'cookie' | string, string>;
  path: string;
} & (T extends 'p12' | 'pfx' ? {
  passphrase: string;
} : {});


export const onRequestProxyHandlerFactory = async (
  page: Page,
  logger: Logger,
  pathCertificato: string,
  passphrase: string,
  additionalHeaders?: CertificateOptions['additionalHeaders']
) => {
  logger.info(`[HttpInterceptor] Enabling interceptors (at page level) and reading certificate file at "${pathCertificato}"!`);

  await page.setRequestInterception(true);

  // TODO: manage failure
  const pfx = await readFile(pathCertificato);

  const httpsAgent = new https.Agent({
    rejectUnauthorized: false,
    pfx,
    passphrase
  });

  let onRequest = async (interceptedRequest: PageEventObject['request']) => {
    const url = interceptedRequest.url();
    const resxType = interceptedRequest.resourceType();

    if (resxType === 'image' && urlsWithImageData.some(urlWithImageData => url.indexOf(urlWithImageData) === 0)) {
      logger.info(`[HttpInterceptor] resxType = '${resxType}'`);

      const base64Data = url.substring(url.indexOf(urlsImageDataSplit) + urlsImageDataSplit.length);
      const body = Buffer.from(base64Data, 'base64');

      interceptedRequest.respond({
        status: 200,
        contentType: 'image/png',
        body
      });

      return;
    }

    // const isDocument = interceptedRequest.resourceType() === 'document';
    // const isRedirect = interceptedRequest.isNavigationRequest() || !!interceptedRequest.redirectChain().length;

    const method = interceptedRequest.method();
    const headers = interceptedRequest.headers();
    const data = interceptedRequest.postData();

    // logger.info(`[HttpInterceptor] resxType-method(isRedirect)::url = '${resxType}'-'${method}'(${interceptedRequest.isNavigationRequest() || !!interceptedRequest.redirectChain().length})::'${url}'`);
    // logger.info(`[HttpInterceptor] headers = '${JSON.stringify(interceptedRequest.headers() || {})}'`);

    axios({
      httpsAgent,
      url,
      method,
      headers: {
        ...additionalHeaders,
        ...headers
      },
      withCredentials: true,
      maxRedirects: 20,
      data
    } as AxiosRequestConfig)
    .then(async (response) => interceptedRequest.respond({
      status: response.status,
      contentType: response.headers['content-type'],
      headers: response.headers,
      body: response.data
    }))
    .catch(error => {
      logger.error(`[HttpInterceptor] Unable to call ${url}`);
      logger.error(error);

      interceptedRequest.abort('connectionrefused');
    });
  };

  page.on('request', onRequest);

  logger.info(`[HttpInterceptor] Request handler added!`);

  return async () => {
    page.off('request', onRequest);
    await page.setRequestInterception(false);

    logger.info(`[HttpInterceptor] Request handler removed!`);
  }
}
