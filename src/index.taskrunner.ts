import taskConsumer from './task/taskConsumer';
import GracefulShutdownController from './graceful/GracefulShutdownController';
import gracefulStartup from './graceful/gracefulStartup';
import container from './taskContainer';
import { Logger } from './logger';
import extractErrorMessage from './utils/extractErrorMessage';
import createChildLogger from './utils/createChildLogger';
import readFileVersioning from './readFileVersioning';

const version = readFileVersioning('taskrunner.version.txt');

const logger = createChildLogger(container.resolve<Logger>('logger'), 'TaskRunner');
const gracefulShutdownController = container.resolve<GracefulShutdownController>('gracefulShutdownController');

// eslint-disable-next-line no-console
const promise = taskConsumer().catch(console.error);

async function gracefulShutdown(signal: string) {
  logger.info(`QUEUE[pid:${process.pid}] - graceful shutdown by ${signal}`);

  logger.info('calling gracefulShutdownController::terminate');
  gracefulShutdownController.terminate();

  logger.info('waiting consumer to finish');
  try {
    await promise;
    logger.info('consumer stopped, now i can exit');
    process.exit(0);
  } catch (ex) {
    logger.error(`An error occours: ${extractErrorMessage(ex)}`);
    process.exit(1);
  }
}

process.on('SIGTERM', gracefulShutdown);
process.on('SIGINT', gracefulShutdown);

logger.info(`QUEUE[pid:${process.pid}][v:${version.replace(/\n/g, '').trim()}] graceful startup - invio ready a pm2`);
gracefulStartup();
