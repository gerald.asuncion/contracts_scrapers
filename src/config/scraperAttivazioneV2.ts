import capitalize from 'lodash/capitalize';
import ScraperConfig from './ScraperConfig';

const scrapersAttivazioneV2: ScraperConfig[] = ([
  {
    name: 'unareti-luce-nuova-attivazione',
    fornitura: 'luce',
    enabled: true,
    distributori: ['UNARETI SPA'],
  },
].map(({ name, fornitura, ...rest }) => {
  const path = `/scraper/v2/${name}`;
  const contratto = 'attivazione';
  const validator = fornitura === 'luce' ? 'podValidator' : 'pdrValidator';
  // const options = rest.options || {};
  const options = {};

  const scraper = name.split('-').reduce((s, l) => s + capitalize(l));
  const info: ScraperConfig = {
    path,
    validator,
    options,
    scraper,
    contratto,
    name,
    ...rest
  };

  if (fornitura) {
    info.fornitura = fornitura === 'luce' ? 'luce' : 'gas';
  }

  return info;
}));

export default scrapersAttivazioneV2;
