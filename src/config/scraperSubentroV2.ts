import capitalize from 'lodash/capitalize';
import ScraperConfig from './ScraperConfig';

const distributori: ScraperConfig[] = ([
  {
    name: 'e-distribuzione',
    options: {
      username: 'gzzbnc91h59d969u@e-distribuzione.pt',
      password: 'Websales30!'
    },
    enabled: true,
    distributori: ['E-DISTRIBUZIONE S.P.A.'],
    fornitura: 'luce'
  },
  {
    name: 'aeg',
    enabled: true,
    distributori: ['RETI DISTRIBUZIONE S.R.L.'],
    fornitura: 'gas'
  },
  {
    name: 'agsm-gas',
    enabled: true,
    distributori: ['MEGARETI SPA', 'AGSM ENERGIA SPA', 'AGSM VERONA S.P.A.'],
    fornitura: 'gas'
  },
  {
    name: 'agsm-luce',
    enabled: true,
    distributori: ['MEGARETI SPA', 'AGSM ENERGIA SPA', 'AGSM VERONA S.P.A.'],
    fornitura: 'luce'
  },
  {
    name: 'iren',
    validator: 'irenValidator',
    options: {
      username: 'sm_op',
      password: '!Supermoney2023!'
    },
    enabled: true,
    distributori: [],
    fornitura: 'luce'
  },
  {
    name: 'amag',
    scraper: 'amag',
    fornitura: 'gas',
    enabled: true,
    distributori: ['AMAG AMBIENTE', 'AMAG RETI GAS S.P.A.']
  },
  {
    name: 'aspm-soresina-servizi-luce',
    fornitura: 'luce',
    enabled: true,
    distributori: ['ASPM SORESINA SERVIZI SRL']
  },
  {
    name: 'ireti-emilia',
    fornitura: 'gas',
    enabled: true,
    distributori: ['IRETI S.P.A.'],
  },
  {
    name: 'reti-metano-territorio',
    fornitura: 'gas',
    enabled: true,
    distributori: ['RETI METANO TERRITORIO S.R.L.'],
  },
  {
    name: 'cbl-distribuzione',
    fornitura: 'gas',
    enabled: true,
    distributori: ['CBL DISTRIBUZIONE SRL', 'CBL SPA'],
  },
  {
    name: 'tea-sei',
    fornitura: 'gas',
    enabled: true,
    distributori: ['SEI SERVIZI ENERGETICI INTEGRATI S.R.L.'],
  },
  {
    name: 'acqui-rete-gas',
    fornitura: 'gas',
    enabled: true,
    distributori: ['ACQUI RETE GAS S.R.L.'],
  },
  {
    name: 'acsm-agam',
    fornitura: 'gas',
    enabled: true,
    distributori: ['ACSM - AGAM SPA', 'ACSM-AGAM RETI GAS-ACQUA S.P.A.'],
  },
  {
    name: 'aemme-linea',
    fornitura: 'gas',
    enabled: true,
    distributori: ['AEMME LINEA DISTRIBUZIONE SRL', 'AEMME LINEA AMBIENTE SRL'],
  },
  {
    name: 'aim-servizi-a-rete-gas',
    fornitura: 'gas',
    enabled: true,
    distributori: ['SERVIZI A RETE SRL', 'AIM VICENZA SPA'],
  },
  {
    name: 'aim-servizi-a-rete-luce',
    fornitura: 'luce',
    enabled: true,
    distributori: ['SERVIZI A RETE SRL'],
  },
  {
    name: 'as-reti-gas',
    fornitura: 'gas',
    enabled: true,
    distributori: ['AS RETIGAS SRL'],
  },
  {
    name: 'assisi-gestioni-servizi-gas',
    fornitura: 'gas',
    enabled: true,
    distributori: ['ASSISI GESTIONI SERVIZI SRL'],
  },
  {
    name: 'ast-gas',
    fornitura: 'gas',
    enabled: true,
    distributori: ['AZIENDA SERVIZI TERRITORIALI SPA'],
  },
  {
    name: 'ast-luce',
    fornitura: 'luce',
    enabled: true,
    distributori: ['AZIENDA SERVIZI TERRITORIALI SPA'],
  },
  {
    name: 'atr-gas',
    fornitura: 'gas',
    enabled: true,
    distributori: ['A T.R. S.R.L.'],
  },
  {
    name: 'coimepa-gas',
    fornitura: 'gas',
    enabled: true,
    distributori: ['COIMEPA SERVIZI S.R.L.'],
  },
  {
    name: 'coimepa-luce',
    fornitura: 'luce',
    enabled: true,
    distributori: ['COIMEPA SERVIZI S.R.L.'],
  },
  {
    name: 'cosvim',
    fornitura: 'gas',
    enabled: true,
    distributori: ['COSVIM SOC. COOP.'],
  },
  {
    name: 'dgn',
    fornitura: 'gas',
    enabled: true,
    distributori: ['DISTRIBUZIONE GAS NATURALE S.R.L.'],
  },
  {
    name: 'edison-infrastrutture',
    fornitura: 'gas',
    enabled: true,
    distributori: ['EDISON SPA'],
  },
  {
    name: 'erogasmet',
    fornitura: 'gas',
    enabled: true,
    distributori: ['EROGASMET S.P.A.'],
  },
  {
    name: 'gas-plus',
    fornitura: 'gas',
    enabled: true,
    distributori: ['GAS PLUS S.P.A.'],
  },
  {
    name: 'gigas',
    fornitura: 'gas',
    enabled: true,
    distributori: ['GIGAS RETE SRL'],
  },
  {
    name: 'mediterranea-energia',
    fornitura: 'gas',
    enabled: true,
    distributori: ['MEDITERRANEA ENERGIA'],
  },
  {
    name: 'pescara-distribuzione-gas',
    fornitura: 'gas',
    enabled: true,
    distributori: ['PESCARA DISTRIBUZIONE GAS S.R.L.'],
  },
  {
    name: 'pescara-distribuzione-luce',
    fornitura: 'luce',
    enabled: true,
    distributori: ['PESCARA DISTRIBUZIONE GAS S.R.L.'],
  },
  {
    name: 'prealpi',
    fornitura: 'gas',
    enabled: true,
    distributori: ['PREALPI GAS S.R.L.'],
  },
  {
    name: 'reti-piu-gas',
    fornitura: 'gas',
    enabled: true,
    distributori: ['RETIPIÙ SRL'],
  },
  {
    name: 'reti-piu-luce',
    fornitura: 'luce',
    enabled: true,
    distributori: ['RETIPIÙ SRL'],
  },
  {
    name: 'serenissima-gas',
    fornitura: 'gas',
    enabled: true,
    distributori: ['SERENISSIMA GAS S.P.A.'],
  },
  {
    name: 'sime',
    fornitura: 'gas',
    enabled: true,
    distributori: ['SOCIETÀ IMPIANTI METANO S.R.L.'],
  },
  {
    name: 'unigas',
    fornitura: 'gas',
    enabled: true,
    distributori: ['UNIGAS S.R.L.'],
  },
  {
    name: 'acm',
    fornitura: 'gas',
    enabled: true,
    distributori: ['AZIENDA MULTISERVIZI CASALESE SPA'],
  },
  {
    name: 'bresciana',
    fornitura: 'gas',
    enabled: true,
    distributori: ['EROGASMET S.P.A.'],
  },
  {
    name: 'caronno-pertusella',
    fornitura: 'gas',
    enabled: true,
    distributori: ['CARONNO PERTUSELLA SERVIZI SRL UNIPERSONALE'],
  },
  {
    name: 'tecniconsul',
    fornitura: 'gas',
    enabled: true,
    distributori: ['TECNICONSUL SRL - COSTRUZIONI E GESTIONI'],
  },
  {
    name: 'valenza',
    fornitura: 'gas',
    enabled: true,
    distributori: ['VALENZA RETE GAS S.P.A.'],
  },
  {
    name: 'pasubio',
    fornitura: 'gas',
    enabled: true,
    distributori: ['ASCOPIAVE S.P.A.', 'ASCOPIAVE ENERGIE S.P.A.'],
  },
  {
    name: 'unareti-gas',
    fornitura: 'gas',
    enabled: true,
    distributori: ['UNARETI SPA'],
  },
  {
    name: 'unareti-luce',
    fornitura: 'luce',
    enabled: true,
    distributori: ['UNARETI SPA'],
  },
  {
    name: 'italgas-santangelo-lodigiano',
    fornitura: 'gas',
    enabled: true,
    distributori: ['METANO SANT\'ANGELO LODIGIANO S.P.A.'],
  },
  {
    name: 'italgas-umbria-distribuzione',
    fornitura: 'gas',
    enabled: true,
    distributori: ['UMBRIA DISTRIBUZIONE GAS S.P.A.'],
  },
  {
    name: 'ireti',
    fornitura: 'luce',
    enabled: true,
    distributori: ['IRETI S.P.A.'],
  },
  {
    name: 'areti',
    fornitura: 'luce',
    enabled: true,
    distributori: ['ARETI SPA'],
  }
].map(({ name, fornitura, ...rest }) => {
  const path = `/scraper/v2/${name}`;
  const contratto = 'subentro';
  const validator = fornitura === 'luce' ? 'podValidator' : 'pdrValidator';
  const options = rest.options || {};

  const scraper = name.split('-').reduce((s, l) => s + capitalize(l));
  const info: ScraperConfig = {
    path,
    validator,
    options,
    scraper,
    contratto,
    name,
    ...rest
  };

  if (fornitura) {
    info.fornitura = fornitura === 'luce' ? 'luce' : 'gas';
  }

  return info;
}));

const copertura: ScraperConfig = {
  name: 'iren-copertura-gas-subentro',
  enabled: true,
  path: '/scraper/v2/iren/copertura-gas-subentro',
  fornitura: 'gas',
  scraper: 'irenSubentroGas',
  validator: 'irenFattibilitaValidator',
  distributori: [],
  options: {}
};

const coperturaAtena: ScraperConfig = {
  name: 'atena-copertura-gas',
  enabled: true,
  path: '/scraper/v2/atena/copertura-gas',
  fornitura: 'gas',
  scraper: 'atenaCoperturaGas',
  distributori: [],
  options: {}
};

const coperturaLuceAtena: ScraperConfig = {
  name: 'atena-copertura-luce',
  enabled: true,
  path: '/scraper/v2/atena/copertura-luce',
  fornitura: 'luce',
  scraper: 'atenaCoperturaLuce',
  distributori: [],
  options: {}
};

const scrapersSubentroV2 = [
  copertura,
  coperturaAtena,
  coperturaLuceAtena,
  ...distributori
];

export default scrapersSubentroV2;
