import ScraperConfig from './ScraperConfig';

const scrapersv1: ScraperConfig[] = [
  {
    name: 'aretiAlert',
    path: '/scraper/areti/alert',
    scraper: 'aretiAlertScraper',
    options: {},
    enabled: true,
    distributori: []
  },
  {
    name: 'unaretiUpdate',
    path: '/scraper/unareti-update',
    scraper: 'unaretiUpdateScraper',
    options: {},
    enabled: true,
    distributori: []
  },
  {
    name: 'unareti-luce-update',
    path: '/scraper/unareti-luce-update',
    scraper: 'unaretiLuceUpdateScraper',
    options: {},
    enabled: true,
    distributori: [],
  }
];

export default scrapersv1;
