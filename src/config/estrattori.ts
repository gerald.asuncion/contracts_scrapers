import ScraperConfig from './ScraperConfig';

const rotteEstrattori: ScraperConfig[] = ([
  {
    name: 'edisonFornitoriLuce',
    enabled: true,
    path: '/scraper/v2/edison/fornitori/luce',
    scraper: 'edisonFornitoriLuce',
    distributori: [],
    options: {}
  },
  {
    name: 'edisonFornitoriGas',
    enabled: true,
    path: '/scraper/v2/edison/fornitori/gas',
    scraper: 'edisonFornitoriGas',
    distributori: [],
    options: {}
  }
]);

export default rotteEstrattori;
