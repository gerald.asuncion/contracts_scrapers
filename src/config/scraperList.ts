import rotteCheck from './check';
import rotteEstrattori from './estrattori';
import rotteFattibilita from './fattibilita';
import rotteInserimento from './inserimento';
import scrapersv1 from './scraperV1';
import scrapersSubentroV2 from './scraperSubentroV2';
import scrapersAttivazioneV2 from './scraperAttivazioneV2';
import scrapersSwitchV2 from './scraperSwitchV2';
import scrapersGenericiV2 from './scrapersGenericiV2';
import ScraperConfig from './ScraperConfig';

const scrapers: ScraperConfig[] = [
  ...rotteCheck,
  ...rotteEstrattori,
  ...rotteFattibilita,
  ...rotteInserimento,
  ...scrapersSubentroV2,
  ...scrapersAttivazioneV2,
  ...scrapersv1,
  ...scrapersSwitchV2,
  ...scrapersGenericiV2
];

export default scrapers;
