import ScraperConfig from './ScraperConfig';

const rotteCheck: ScraperConfig[] = ([
  {
    name: 'edisonCheckFirme',
    enabled: true,
    path: '/scraper/v2/edison/check/firme',
    scraper: 'edisonCheckFirme',
    distributori: [],
    options: {}
  },
  {
    name: 'windCheckCopertura',
    enabled: true,
    path: '/scraper/v2/wind/check/copertura',
    scraper: 'windCheckCopertura',
    validator: 'windCheckCoperturaValidator',
    distributori: [],
    options: {}
  },
  {
    name:"dueIReteGasChecker",
    enabled: true,
    path: "/scraper/v2/due-i-rete-gas/checker",
    scraper: 'dueIReteGas',
    distributori:[],
    options:{}
  },
  {
    name: 'iretiCheckPod',
    enabled: true,
    path: '/scraper/ireti/check/pod',
    fornitura: 'luce',
    scraper: 'iretiCheckPod',
    validator: 'iretiCheckPodValidator',
    distributori: [],
    options: {}
  },
  {
    name: 'iretiCheckPdr',
    enabled: true,
    path: '/scraper/ireti/check/pdr',
    fornitura: 'gas',
    scraper: 'iretiCheckPdr',
    validator: 'iretiCheckPdrValidator',
    distributori: [],
    options: {}
  },
  {
    name: 'leretiCheckPdr',
    enabled: true,
    path: '/scraper/lereti/check/pdr',
    fornitura: 'gas',
    scraper: 'leretiCheckPdr',
    validator: 'leretiCheckPdrValidator',
    distributori: [],
    options: {}
  },
  {
    name:"dueIReteGasV3",
    enabled: true,
    path: "/scraper/v3/due-i-rete-gas/check",
    scraper: 'dueIReteGasV3',
    distributori:[],
    options:{}
  },
  {
    name: 'eDistribuzionePodChecker',
    enabled: true,
    path: '/scraper/v2/eDistribuzione/pod/checker',
    fornitura: 'luce',
    scraper: 'eDistribuzionePodChecker',
    validator: 'eDistribuzionePodCheckerValidator',
    distributori: [],
    options: {}
  },
  {
    name: 'eDistribuzioneCheckPod',
    enabled: true,
    path: '/scraper/v3/edistribuzione/check/pod',
    fornitura: 'luce',
    scraper: 'eDistribuzioneCheckPod',
    validator: 'podValidator',
    distributori: [],
    options: {}
  },
  {
    name: 'iretiCheckPodV3',
    enabled: true,
    path: '/scraper/v3/ireti/check/pod',
    fornitura: 'luce',
    scraper: 'iretiCheckPodV3',
    validator: 'iretiCheckPodV3Validator',
    distributori: [],
    options: {}
  },
  {
    name: 'iretiCheckPdrV3',
    enabled: true,
    path: '/scraper/v3/ireti/check/pdr',
    fornitura: 'gas',
    scraper: 'iretiCheckPdrV3',
    validator: 'iretiCheckPdrV3Validator',
    distributori: [],
    options: {}
  },
  {
    name: 'leretiCheckPdrV3',
    enabled: true,
    path: '/scraper/v3/lereti/check/pdr',
    fornitura: 'gas',
    scraper: 'leretiCheckPdrV3',
    validator: 'pdrValidator',
    distributori: [],
    options: {}
  },
]);

export default rotteCheck;
