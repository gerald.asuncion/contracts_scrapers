import ScraperConfig from './ScraperConfig';

const rotteCheck: ScraperConfig[] = ([
  {
    name: 'irenLinkem',
    enabled: true,
    path: '/scraper/v2/irenLinkem/inserimento',
    scraper: 'irenLinkem',
    validator: 'irenLinkemValidator',
    distributori: [],
    options: {}
  },
  {
    name: 'fastwebPostFirma',
    enabled: true,
    path: '/scraper/v2/fastwebPostFirma/inserimento',
    scraper: 'fastwebPostFirma',
    validator: 'fastwebPostFirmaValidator',
    distributori: [],
    options: {}
  },
  {
    name: 'eniResidenziale',
    enabled: true,
    path: '/scraper/v2/eniResidenziale/inserimento',
    scraper: 'eniResidenziale',
    validator: 'eniResidenzialeValidator',
    distributori: [],
    options: {}
  },
]);

export default rotteCheck;
