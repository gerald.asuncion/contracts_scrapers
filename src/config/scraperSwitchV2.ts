import ScraperConfig from './ScraperConfig';

const scrapersSwitchV2: ScraperConfig[] = ([
  {
    name: 'iren-copertura-gas-switch',
    enabled: true,
    path: '/scraper/v2/iren/copertura-gas-switch',
    fornitura: 'gas',
    scraper: 'irenSwitchGas',
    validator: 'irenFattibilitaValidator',
    distributori: [],
    options: {}
  },
  {
    name: 'iren-pre-check-switch',
    enabled: true,
    path: '/scraper/v2/iren/iren-pre-check-switch',
    fornitura: 'gas',
    scraper: 'irenPreCheck',
    distributori: [],
    options: {},
    validator: 'irenPreCheckValidator'
  },
  {
    name: 'atena-copertura-gas-switch',
    enabled: true,
    path: '/scraper/v2/atena/copertura-gas-switch',
    fornitura: 'gas',
    scraper: 'atenaCoperturaGasSwitch',
    distributori: [],
    options: {}
  },
  {
    name: 'iberdrolaStatoContrattoChecker',
    enabled: true,
    path: '/scraper/v2/iberdrola/checker',
    fornitura: 'gas',
    scraper: 'iberdrolaStatoContrattoChecker',
    distributori: [],
    options: {}
  },
  {
    name: 'iberdrolaCreditChecker',
    enabled: true,
    path: '/scraper/v2/iberdrola/checker/credit',
    fornitura: 'gas',
    scraper: 'iberdrolaCreditChecker',
    distributori: [],
    options: {}
  },
  {
    name: 'UnaretiContendibilitaGasChecker',
    enabled: true,
    path: '/scraper/v2/unareti/contendibilita/gas/download',
    fornitura: 'gas',
    scraper: 'unaretiContendibilitaGasChecker',
    distributori: [],
    options: {}
  },
  {
    name: 'UnaretiContendibilitaElettricitaChecker',
    enabled: true,
    path: '/scraper/v2/unareti/contendibilita/elettricita/download',
    fornitura: 'luce',
    scraper: 'unaretiContendibilitaElettricitaChecker',
    distributori: [],
    options: {}
  },
  {
    name: 'AretiContendibilitaChecker',
    enabled: true,
    path: '/scraper/v2/areti/contendibilita/download',
    fornitura: 'gas',
    scraper: 'areti',
    distributori: [],
    options: {}
  },
  {
    name: 'AretiPodChecker',
    enabled: true,
    path: '/scraper/v2/areti/pod/check',
    fornitura: 'gas',
    scraper: 'aretiCheckPod',
    validator: 'aretiCheckPodValidator',
    distributori: [],
    options: {}
  },
  {
    name: 'iberdrolaFornitoriLuce',
    enabled: true,
    path: '/scraper/v2/iberdrola/fornitori/luce',
    fornitura: 'luce',
    scraper: 'iberdrolaFornitoriLuce',
    distributori: [],
    options: {}
  },
  {
    name: 'iberdrolaFornitoriGas',
    enabled: true,
    path: '/scraper/v2/iberdrola/fornitori/gas',
    fornitura: 'gas',
    scraper: 'iberdrolaFornitoriGas',
    distributori: [],
    options: {}
  },
  {
    name: 'iberdrolaDistributoriLuce',
    enabled: true,
    path: '/scraper/v2/iberdrola/distributori/luce',
    fornitura: 'luce',
    scraper: 'iberdrolaDistributoriLuce',
    distributori: [],
    options: {}
  },
  {
    name: 'iberdrolaDistributoriGas',
    enabled: true,
    path: '/scraper/v2/iberdrola/distributori/gas',
    fornitura: 'gas',
    scraper: 'iberdrolaDistributoriGas',
    distributori: [],
    options: {}
  },
  {
    name: 'coverCareZoneDisagiate',
    enabled: true,
    path: '/scraper/v2/covercare/zone-disagiate/check',
    fornitura: 'gas',
    scraper: 'covercareZoneDisagiateChecker',
    distributori: [],
    validator: 'covercareZoneDisagiateCheckerValidator',
    options: {}
  },
  {
    name: 'agsmGasCopertura',
    enabled: true,
    path: '/scraper/v2/agsm/gas/copertura',
    fornitura: 'gas',
    scraper: 'agsmGasCopertura',
    validator: 'agsmGasCoperturaValidator',
    distributori: [],
    options: {}
  },
  {
    name: 'taskQueue',
    enabled: true,
    path: '/scraper/queue/:scraper',
    fornitura: 'luce',
    scraper: 'taskQueue',
    distributori: [],
    options: {}
  },
  {
    name: 'eoloCheck',
    enabled: true,
    path: '/scraper/v2/eolo/check',
    scraper: 'eoloCheck',
    distributori: [],
    validator: 'eoloCheckValidator',
    options: {}
  },
  {
    name: 'eoloCheckFirmeV2',
    enabled: true,
    path: '/scraper/v2/eolo/check/firme',
    scraper: 'eoloCheckFirme',
    distributori: [],
    validator: 'eoloCheckFirmeValidator',
    options: {}
  },
  {
    name: 'eoloVerificaCopertura',
    enabled: true,
    path: '/scraper/v2/eolo/verifica/copertura',
    scraper: 'eoloVerificaCopertura',
    distributori: [],
    validator: 'eoloVerificaCoperturaValidator',
    options: {}
  },
  {
    name: 'eniCheck',
    enabled: true,
    path: '/scraper/v2/eni/check',
    fornitura: 'gas',
    scraper: 'eniCheckFirme',
    distributori: [],
    options: {}
  },
  {
    name: 'eniCheckFattibilita',
    enabled: true,
    path: '/scraper/v2/eni/fattibilita',
    fornitura: 'gas',
    scraper: 'eniCheckFattibilita',
    validator: 'eniCheckFattibilitaValidator',
    distributori: [],
    options: {}
  },
  {
    name: 'eniFornitoriGas',
    enabled: true,
    path: '/scraper/v2/eni/fornitori/gas',
    fornitura: 'gas',
    scraper: 'eniFornitoriGas',
    distributori: [],
    options: {}
  },
  {
    name: 'eniFornitoriLuce',
    enabled: true,
    path: '/scraper/v2/eni/fornitori/luce',
    fornitura: 'luce',
    scraper: 'eniFornitoriLuce',
    distributori: [],
    options: {}
  },
  {
    name: 'eniFormeGiuridiche',
    enabled: true,
    path: '/scraper/v2/eni/forma-giuridica',
    fornitura: 'luce',
    scraper: 'eniFormeGiuridiche',
    distributori: [],
    options: {}
  },
  {
    name: 'eniSettoriMerceologici',
    enabled: true,
    path: '/scraper/v2/eni/settore-merceologico/:formaGiuridica',
    fornitura: 'luce',
    scraper: 'eniSettoriMerceologici',
    distributori: [],
    options: {}
  },
  {
    name: 'eniAttivitaMerceologiche',
    enabled: true,
    path: '/scraper/v2/eni/attivita-merceologica/:settoreMerceologico',
    fornitura: 'luce',
    scraper: 'eniAttivitaMerceologiche',
    distributori: [],
    options: {}
  },
  {
    name: 'eniCodiciAteco',
    enabled: true,
    path: '/scraper/v2/eni/codice-ateco/:attivitaMerceologica',
    fornitura: 'luce',
    scraper: 'eniCodiciAteco',
    distributori: [],
    options: {}
  },
  {
    name: 'fastwebCheckFattibilita',
    enabled: true,
    path: '/scraper/v2/fastweb/fattibilita',
    scraper: 'fastwebCheckFattibilita',
    validator: 'fastwebCheckFattibilitaValidator',
    distributori: [],
    options: {},
  },
  {
    name: 'fastwebCheck',
    enabled: true,
    path: '/scraper/v2/fastweb/check',
    scraper: 'fastwebCheckFirme',
    validator: 'fastwebCheckFirmeValidator',
    distributori: [],
    options: {}
  },
  {
    name: 'fastwebFornitoriMobile',
    enabled: true,
    path: '/scraper/v2/fastweb/fornitori/mobile',
    scraper: 'fastwebFornitoriMobile',
    distributori: [],
    options: {}
  },
  {
    name: 'fastwebFornitoriInternet',
    enabled: true,
    path: '/scraper/v2/fastweb/fornitori/internet',
    scraper: 'fastwebFornitoriInternet',
    distributori: [],
    options: {}
  },
  {
    name: 'fastwebFornitoriDati',
    enabled: true,
    path: '/scraper/v2/fastweb/fornitori/dati',
    scraper: 'fastwebFornitoriDati',
    distributori: [],
    options: {}
  },
  {
    name: 'fastwebVerificaMorosita',
    enabled: true,
    path: '/scraper/v2/fastweb/fattibilita/morosita',
    fornitura: 'gas',
    scraper: 'fastwebVerificaMorosita',
    validator: 'fastwebVerificaMorositaValidator',
    distributori: [],
    options: {},
  },
  {
    name: 'fastwebVerificaCopertura',
    enabled: true,
    path: '/scraper/v2/fastweb/fattibilita/copertura',
    fornitura: 'gas',
    scraper: 'fastwebVerificaCopertura',
    validator: 'fastwebVerificaCoperturaValidator',
    distributori: [],
    options: {},
  },
  {
    name: 'fastwebVerificaDocumento',
    enabled: true,
    path: '/scraper/v2/fastweb/fattibilita/documento',
    fornitura: 'gas',
    scraper: 'fastwebVerificaDocumento',
    validator: 'fastwebVerificaDocumentoValidator',
    distributori: [],
    options: {},
  },
  {
    name: 'agsmPrecheck',
    enabled: true,
    path: '/scraper/v2/agsm/precheck',
    // fornitura: 'gas',
    scraper: 'agsmPrecheck',
    validator: 'agsmPrecheckValidator',
    distributori: [],
    options: {},
  },
  {
    name: 'mockFakeFailure',
    enabled: true,
    path: '/scraper/mock/failure',
    scraper: 'mockFakeFailure',
    distributori: [],
    options: {}
  },
  {
    name: 'mockFakeError',
    enabled: true,
    path: '/scraper/mock/error',
    scraper: 'mockFakeError',
    distributori: [],
    options: {}
  },
  {
    name: 'mockFakeBlocked',
    enabled: true,
    path: '/scraper/mock/blocked',
    scraper: 'mockFakeBlocked',
    distributori: [],
    options: {}
  },
  {
    name: 'iberdrolaCreditCheckV2',
    enabled: true,
    path: '/scraper/v2/iberdrolaV2/check/credit',
    scraper: 'iberdrolaCreditCheck',
    validator: 'iberdrolaCreditCheckValidator',
    distributori: [],
    options: {}
  },
  {
    name: 'iberdrolaCheckFirmeV2',
    enabled: true,
    path: '/scraper/v2/iberdrolaV2/check/firme',
    fornitura: 'gas',
    scraper: 'iberdrolaCheckFirme',
    validator: 'iberdrolaCheckFirmeValidator',
    distributori: [],
    options: {}
  },
  {
    name: 'iberdrolaFornitoriLuceV2',
    enabled: true,
    path: '/scraper/v2/iberdrolaV2/fornitori/luce',
    fornitura: 'luce',
    scraper: 'iberdrolaFornitoriLuce',
    distributori: [],
    options: {}
  },
  {
    name: 'iberdrolaFornitoriGasV2',
    enabled: true,
    path: '/scraper/v2/iberdrolaV2/fornitori/gas',
    fornitura: 'luce',
    scraper: 'iberdrolaFornitoriGas',
    distributori: [],
    options: {}
  },
  {
    name: 'iberdrolaDistributoriLuceV2',
    enabled: true,
    path: '/scraper/v2/iberdrolaV2/distributori/luce',
    fornitura: 'luce',
    scraper: 'iberdrolaDistributoriLuce',
    distributori: [],
    options: {}
  },
  {
    name: 'iberdrolaDistributoriGasV2',
    enabled: true,
    path: '/scraper/v2/iberdrolaV2/distributori/gas',
    fornitura: 'luce',
    scraper: 'iberdrolaDistributoriGas',
    distributori: [],
    options: {}
  },
  {
    name: 'iberdrola',
    enabled: true,
    path: '/scraper/v2/iberdrola/inserimento',
    scraper: 'iberdrola',
    validator: 'iberdrolaValidator',
    distributori: [],
    options: {}
  },
  {
    name: 'iberdrolaInserimentoV2',
    enabled: true,
    path: '/scraper/v2/iberdrolaV2/inserimento',
    fornitura: 'luce',
    scraper: 'iberdrolaV2',
    validator: 'iberdrolaV2Validator',
    distributori: [],
    options: {}
  },
  {
    name: 'fastweb',
    enabled: true,
    path: '/scraper/v2/fastweb/inserimento',
    scraper: 'fastweb',
    validator: 'fastwebValidator',
    distributori: [],
    options: {}
  },
  {
    name: 'eolo',
    enabled: true,
    path: '/scraper/v2/eolo/inserimento',
    scraper: 'eolo',
    validator: 'eoloValidator',
    distributori: [],
    options: {}
  },
  {
    name: 'eni',
    enabled: true,
    path: '/scraper/v2/eni/inserimento',
    scraper: 'eni',
    validator: 'eniValidator',
    distributori: [],
    options: {}
  },
  {
    name: 'fastweb',
    enabled: true,
    path: '/scraper/v2/fastweb/inserimento',
    scraper: 'fastweb',
    validator: 'fastwebValidator',
    distributori: [],
    options: {}
  },
  {
    name: 'edison',
    enabled: true,
    path: '/scraper/v2/edison/inserimento',
    scraper: 'edison',
    validator: 'edisonValidator',
    distributori: [],
    options: {},
  }
]);

export default scrapersSwitchV2;
