export default interface ScraperConfig {
  name: string;
  path: string;
  validator?: string;
  scraper: string;
  options: any;
  enabled: boolean;
  distributori: string[];
  fornitura?: 'gas' | 'luce';
  contratto?: string;
}
