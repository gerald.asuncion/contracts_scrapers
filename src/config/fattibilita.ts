import ScraperConfig from './ScraperConfig';

const rotteFattibilita: ScraperConfig[] = ([
  {
    name: 'edisonFattibilita',
    enabled: true,
    path: '/scraper/v2/edison/fattibilita',
    scraper: 'edisonFattibilita',
    distributori: [],
    options: {}
  },
  {
    name: 'wind',
    enabled: true,
    path: '/scraper/v2/wind/fattibilita/morosita',
    scraper: 'windVerificaMorosita',
    validator: 'windVerificaMorositaValidator',
    distributori: [],
    options: {},
  },
  {
    name: 'windVerificaMorositaV3',
    enabled: true,
    path: '/scraper/v3/wind/fattibilita/morosita',
    scraper: 'windVerificaMorositaV3',
    validator: 'windVerificaMorositaV3Validator',
    distributori: [],
    options: {},
  },
  {
    name: 'AreraCheckDistributoriGas',
    enabled: true,
    path: '/scraper/v2/arera/gas/distributori/check/',
    fornitura: 'gas',
    scraper: 'areraCheckDistributoriGas',
    validator: 'areraCheckDistributoriGasValidator',
    distributori: [],
    options: {}
  },
  {
    name: 'Arera',
    enabled: true,
    path: '/scraper/v3/arera/check',
    fornitura: 'gas',
    scraper: 'arera',
    validator: 'areraValidator',
    distributori: [],
    options: {}
  },
  {
    name: 'italgas',
    enabled: true,
    path: '/scraper/v2/italgas',
    fornitura: 'gas',
    scraper: 'italgas',
    validator: 'pdrValidator',
    distributori: ['ITALGAS RETI S.P.A.'],
    options: {}
  },
  {
    name: 'italgas-toscana',
    enabled: true,
    path: '/scraper/v2/italgas-toscana',
    fornitura: 'gas',
    scraper: 'italgasToscana',
    validator: 'pdrValidator',
    distributori: ['TOSCANA ENERGIA S.P.A.'],
    options: {}
  },
  {
    name: 'italgas-attivazione',
    enabled: true,
    fornitura: 'gas',
    path: '/scraper/v2/italgas-attivazione',
    scraper: 'italgasAttivazione',
    validator: 'pdrValidator',
    distributori: ['ITALGAS RETI S.P.A.'],
    options: {}
  },
  {
    name: 'italgasContendibilitaGas',
    enabled: true,
    path: '/scraper/v2/italgas/contendibilita/gas',
    fornitura: 'gas',
    scraper: 'italgasContendibilitaGas',
    distributori: [],
    options: {},
  },
  {
    name: 'italgasV3',
    enabled: true,
    fornitura: 'gas',
    path: '/scraper/v3/italgas/check/gas',
    scraper: 'italgasV3',
    validator: 'pdrValidator',
    distributori: ['ITALGAS RETI S.P.A.'],
    options: {}
  },
  {
    name: 'italgasAttivazioneV3',
    enabled: true,
    fornitura: 'gas',
    path: '/scraper/v3/italgas/attivazione/check/gas',
    scraper: 'italgasAttivazioneV3',
    validator: 'pdrValidator',
    distributori: ['ITALGAS RETI S.P.A.'],
    options: {}
  },
  {
    name: 'italgasContendibilitaGasV3',
    enabled: true,
    path: '/scraper/v3/italgas/contendibilita/gas',
    fornitura: 'gas',
    scraper: 'italgasContendibilitaGasV3',
    validator: 'pdrValidator',
    distributori: [],
    options: {},
  },
  {
    name: 'italgasToscanaV3',
    enabled: true,
    path: '/scraper/v3/italgas/toscana/check/gas',
    fornitura: 'gas',
    scraper: 'italgasToscanaV3',
    validator: 'pdrValidator',
    distributori: [],
    options: {},
  },
  {
    name: 'UnaretiPdrChecker',
    enabled: true,
    path: '/scraper/v2/unareti/pdr/check',
    fornitura: 'gas',
    scraper: 'unaretiPdrChecker',
    validator: 'unaretiPdrCheckerValidator',
    distributori: [],
    options: {}
  },
  {
    name: 'UnaretiPodChecker',
    enabled: true,
    path: '/scraper/v2/unareti/pod/check',
    fornitura: 'luce',
    scraper: 'unaretiPodChecker',
    validator: 'unaretiPodCheckerValidator',
    distributori: [],
    options: {}
  },
  {
    name: 'unaretiCheckPdrV3',
    enabled: true,
    path: '/scraper/v3/unareti/check/pdr',
    fornitura: 'gas',
    scraper: 'unaretiCheckPdrV3',
    validator: 'pdrValidator',
    distributori: [],
    options: {}
  },
  {
    name: 'unaretiCheckPodV3',
    enabled: true,
    path: '/scraper/v3/unareti/check/pod',
    fornitura: 'luce',
    scraper: 'unaretiCheckPodV3',
    validator: 'podValidator',
    distributori: [],
    options: {}
  },
  {
    name: 'centriaPdrCheckV3',
    enabled: true,
    path: '/scraper/v3/centria/check/pdr',
    fornitura: 'luce',
    scraper: 'centriaPdrCheckV3',
    validator: 'pdrValidator',
    distributori: [],
    options: {}
  },
  {
    name: 'irenPrecheckV3',
    enabled: true,
    path: '/scraper/v3/iren/precheck',
    scraper: 'irenPrecheckV3',
    validator: 'irenPrecheckV3Validator',
    distributori: [],
    options: {}
  },
  {
    name: 'erogasmetV3',
    enabled: true,
    path: '/scraper/v3/erogasmet/check/pdr',
    scraper: 'erogasmetV3',
    validator: 'pdrValidator',
    distributori: [],
    options: {}
  },
]);

export default rotteFattibilita;
