export interface MysqlConfig {
  host: string,
  port: string,
  username: string,
  password: string,
  database: string,
}

export interface MailConfig {
  service: string,
  user: string,
  pass: string,
  subjectPrefix?: string
}

export type AwsConfig = {
  s3: {
    bucket: string;
    userAccessId: string;
    userAccessKeyId: string;
    userSecretAccessKey: string;
    region: string;
  };
};

export type AwsS3Config = AwsConfig['s3'];
