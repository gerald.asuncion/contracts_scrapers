const map: Record<string, string> = {
  // 'RETIPIÙ SRL': 'areraRetiPiuGas',
  'ITALGAS RETI S.P.A.': 'italgasContendibilitaGasV3',
  'UNARETI SPA': 'unaretiCheckPdrV3',
  'TOSCANA ENERGIA S.P.A.': 'italgasToscanaV3',
  CENTRIA: 'centriaPdrCheckV3',
  'EROGASMET S.P.A.': 'erogasmetV3',
  '2I RETE GAS S.P.A.': 'dueIReteGasV3',
  'IRETI S.P.A.': 'iretiCheckPdrV3',
  'LERETI S.P.A.': 'leretiCheckPdrV3'
};

export default function getNomeScraperDaNomeDistributorePerOneForm(distributore: string): string | undefined {
  return map[distributore];
}
