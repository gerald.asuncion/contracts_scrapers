import AreraRepo from "../../repo/AreraRepo";
import { ScraperResponse, WebScraper } from "../../scraperv2/scraper";
import { ScraperTipologia } from "../../scraperv2/ScraperOptions";
import AreraCheckDistributoriGas from "../../scraperv2/arera/AreraCheckDistributoriGas";
import { getUnifiedResponseFromMap } from "../../scraperv2/arera/helpers/creaMultipleResponse";
import recuperaListaDistributori from "../../scraperv2/arera/helpers/recuperaListaDistributori";
import { recuperaEsitiDistributori } from "../../scraperv2/arera/helpers/recuperaEsitiDistributori";
import { AreraOptions, AreraPayload } from "../../scraperv2/arera/types";
import SuccessResponse from "../../response/SuccessResponse";
import { AwilixContainer } from "awilix";
import getNomeScraperDaNomeDistributorePerOneForm from "./utils/getNomeScraperDaNomeDistributorePerOneForm";
import { QueueTask } from "../../task/QueueTask";

@QueueTask({ scraperName: 'arera' })
export default class AreraV3 extends WebScraper<AreraPayload> {
  private areraRepo: AreraRepo;
  private cradle: AwilixContainer['cradle'];

  constructor(options: AreraOptions) {
    super(options);
    this.areraRepo = options.areraRepo;
    this.cradle = options;
  }

  login(): Promise<void> {
    return Promise.resolve();
  }

  async scrapeWebsite(payload: AreraPayload): Promise<ScraperResponse> {
    const logger = this.childLogger;

    logger.info(`eseguo il controllo per il payload ${JSON.stringify(payload)}`);

    let [distributori, err] = await recuperaListaDistributori({
      getPage: () => this.p(),
      logger,
      areraRepo: this.areraRepo,
      payload
    });

    if (err) {
      return err;
    }

    distributori = distributori || [];

    logger.info(`chiamo gli scrapers per i distributori ${distributori.join(', ')}`);

    const esiti = await recuperaEsitiDistributori({
      logger,
      payload,
      distributori: distributori.slice(0),
      cradle: this.cradle,
      getNomeScraperDaNomeDistributore: getNomeScraperDaNomeDistributorePerOneForm
    });

    const result = getUnifiedResponseFromMap(esiti, distributori, true);

    if (result instanceof SuccessResponse) {
      logger.info(`Success ${JSON.stringify(result)}`);
    } else {
      logger.error(`Ritorno l'errore ${JSON.stringify(result)}`);
    }

    return result;
  }

  getScraperCodice(): string {
    return AreraCheckDistributoriGas.prototype.getScraperCodice();
  }

  getScraperTipologia(): ScraperTipologia {
    return AreraCheckDistributoriGas.prototype.getScraperTipologia();
  }
}
