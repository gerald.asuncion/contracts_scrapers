/**
 * @openapi
 *
 * /scraper/v3/arera/check:
 *  post:
 *    tags:
 *      - v3
 *    description: Va sul portale di arera per trovare i distributori nella città passata. Dopodiché chiama tutti gli scraper disponibili per quei distributori trovati.
 *    consume:
 *      - application/json
 *    produces:
 *      - application/json
 *    responses:
 *      200:
 *        description: SuccessResponse
 *    parameters:
 *      - in: body
 *        description:
 *          pdr e indirizzo da controllare.
 *          L'indirizzo serve per trovare i distributori di quella zona, poi viene passato tutto il payload ai relativi scraper per il controllo.
 *        schema:
 *          type: object
 *          required:
 *            - pdr
 *            - regione
 *            - provincia
 *            - comune
 *          properties:
 *            pdr:
 *              type: string
 *            regione:
 *              type: string
 *            provincia:
 *              type: string
 *            comune:
 *              type: string
 *
 * /scraper/queue/arera:
 *  post:
 *    tags:
 *      - v3
 *      - queue
 *    description:
 *      Inserisce in queue il task per effettuare il check tramite lo scraper arera.
 *      Il payload è lo stesso della rotta diretta `/scraper/v3/arera/check`,
 *      in più va aggiunto il webhook che lo scraper poi andrà a richiamare per notificare l'esito.
 *    consume:
 *      - application/json
 *    produces:
 *      - application/json
 *    responses:
 *      200:
 *        description: SuccessResponse
 */
