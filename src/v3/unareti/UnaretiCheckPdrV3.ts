import { ScraperResponse } from "../../scraperv2/scraper";
import UnaretiChecker from "../../scraperv2/unareti/UnaretiChecker";
import { QueueTask } from "../../task/QueueTask";
import generaEsito from "./gas/generaEsito";

type UnaretiCheckPdrV3Payload = {
  pdr: string;
};

@QueueTask()
export default class UnaretiCheckPdrV3 extends UnaretiChecker<UnaretiCheckPdrV3Payload> {
  /**
   * @override
   * @param payload Request payload
   */
   async scrapeWebsite({ pdr }: UnaretiCheckPdrV3Payload): Promise<ScraperResponse> {
    const logger = this.childLogger;

    logger.info(`start check pdr ${pdr}`);
    const record = await this.unaretiRepo.pdrCheck(pdr);

    const esito = generaEsito(record);
    logger.info(`per il pdr ${pdr} trovato record ${JSON.stringify(record)} e generato esito ${JSON.stringify(esito)}`);

    return esito;
  }

  /**
   * @override
   */
  getScraperCodice() {
    return 'unareti-checkpdr';
  }
}
