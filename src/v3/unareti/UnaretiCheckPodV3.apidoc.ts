/**
 * @openapi
 *
 * /scraper/v3/unareti/check/pod:
 *  post:
 *    tags:
 *      - v3
 *    description: Va sul portale di Unareti e controlla se il pod è valido.
 *    consume:
 *      - application/json
 *    produces:
 *      - application/json
 *    responses:
 *      200:
 *        description: SuccessResponse
 *    parameters:
 *      - in: body
 *        description:
 *          pod da controllare.
 *        schema:
 *          type: object
 *          required:
 *            - pod
 *          properties:
 *            pod:
 *              type: string
 *
 * /scraper/queue/unaretiCheckPodV3:
 *  post:
 *    tags:
 *      - v3
 *      - queue
 *    description:
 *      Inserisce in queue il task per effettuare il check tramite lo scraper UnaretiCheckPodV3.
 *      Il payload è lo stesso della rotta diretta `/scraper/v3/unareti/check/pod`,
 *      in più va aggiunto il webhook che lo scraper poi andrà a richiamare per notificare l'esito.
 *    consume:
 *      - application/json
 *    produces:
 *      - application/json
 *    responses:
 *      200:
 *        description: SuccessResponse
 */
