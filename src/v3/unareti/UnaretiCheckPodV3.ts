import { ScraperResponse } from "../../scraperv2/scraper";
import UnaretiChecker from "../../scraperv2/unareti/UnaretiChecker";
import { QueueTask } from "../../task/QueueTask";
import generaEsito from "./luce/generaEsito";

type UnaretiCheckPodV3Payload = {
  pod: string;
};

@QueueTask()
export default class UnaretiCheckPodV3 extends UnaretiChecker<UnaretiCheckPodV3Payload> {
  /**
   * @override
   * @param payload Request payload
   */
   async scrapeWebsite({ pod }: UnaretiCheckPodV3Payload): Promise<ScraperResponse> {
    const logger = this.childLogger;

    logger.info(`start check pod ${pod}`);
    const record = await this.unaretiRepo.podCheck(pod);

    const esito = generaEsito(record);
    logger.info(`per il pod ${pod} trovato record ${JSON.stringify(record)} e generato esito ${JSON.stringify(esito)}`);

    return esito;
  }

  /**
   * @override
   */
  getScraperCodice() {
    return 'unareti-checkpdr';
  }
}
