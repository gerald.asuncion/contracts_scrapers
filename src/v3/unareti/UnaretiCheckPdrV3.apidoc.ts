/**
 * @openapi
 *
 * /scraper/v3/unareti/check/pdr:
 *  post:
 *    tags:
 *      - v3
 *    description: Va sul portale di Unareti e controlla se il pdr è valido.
 *    consume:
 *      - application/json
 *    produces:
 *      - application/json
 *    responses:
 *      200:
 *        description: SuccessResponse
 *    parameters:
 *      - in: body
 *        description:
 *          pdr da controllare.
 *        schema:
 *          type: object
 *          required:
 *            - pdr
 *          properties:
 *            pdr:
 *              type: string
 *
 * /scraper/queue/unaretiCheckPdrV3:
 *  post:
 *    tags:
 *      - v3
 *      - queue
 *    description:
 *      Inserisce in queue il task per effettuare il check tramite lo scraper UnaretiCheckPdrV3.
 *      Il payload è lo stesso della rotta diretta `/scraper/v3/unareti/check/pdr`,
 *      in più va aggiunto il webhook che lo scraper poi andrà a richiamare per notificare l'esito.
 *    consume:
 *      - application/json
 *    produces:
 *      - application/json
 *    responses:
 *      200:
 *        description: SuccessResponse
 */
