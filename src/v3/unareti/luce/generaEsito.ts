import { UnaretiContendibilitaLuceRecord } from "../../../repo/types";
import CasoNonGestitoResponse from "../../../response/CasoNonGestitoResponse";
import FailureResponse from "../../../response/FailureResponse";
import SuccessResponse from "../../../response/SuccessResponse";

function getSuccessInfo({
  toponimo,
  nome_strada,
  civico,
  estensione_civico,
  cap,
  comune,
  provincia,
  interno,
  scala,
  codice_istat,
  piano,
  matricola_contatore,
  stato_del_punto,
  tensione,
  potenza_predisposta_sul_pod
}: UnaretiContendibilitaLuceRecord) {
  return {
    toponimo,
    nomeStrada: nome_strada,
    civico,
    estensioneCivico: estensione_civico,
    cap,
    comune,
    provincia,
    interno,
    scala,
    codiceIstat: codice_istat,
    piano,
    matricolaContatore: matricola_contatore,
    statoDelPunto: stato_del_punto,
    tensione,
    potenzaPredispostaSulPod: potenza_predisposta_sul_pod,
  };
}

export default function generaEsito(record: UnaretiContendibilitaLuceRecord | null) {
  if (!record) {
    // pdr nel payload non trovato nella tabella
    return new FailureResponse('ko-pod non contendibile');
  }

  const statoDelPunto = record.stato_del_punto.toLowerCase();


  if (!['disattivato', 'mai attivato'].includes(statoDelPunto)) {
    return new FailureResponse('ko-pod non contendibile');
  }

  if (statoDelPunto === 'disattivato') {
    return new SuccessResponse('ok-subentro', getSuccessInfo(record));
  }

  if (statoDelPunto === 'mai attivato') {
    return new SuccessResponse('ok-prima attivazione', getSuccessInfo(record));
  }

  return new CasoNonGestitoResponse();
}
