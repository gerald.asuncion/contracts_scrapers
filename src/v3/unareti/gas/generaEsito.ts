import { UnaretiContendibilitaGasRecord } from "../../../repo/types";
import CasoNonGestitoResponse from "../../../response/CasoNonGestitoResponse";
import FailureResponse from "../../../response/FailureResponse";
import SuccessResponse from "../../../response/SuccessResponse";

function getSuccessInfo({
  toponimo,
  nome_strada,
  civico,
  estensione_civico,
  cap,
  comune,
  provincia,
  interno,
  scala,
  codice_istat,
  piano,
  matricola_contatore,
  calibro_contatore,
  stato_del_punto,
  portata,
  codice_remi,
}: UnaretiContendibilitaGasRecord) {
  return {
    toponimo,
    nomeStrada: nome_strada,
    civico,
    estensioneCivico: estensione_civico,
    cap,
    comune,
    provincia,
    interno,
    scala,
    codiceIstat: codice_istat,
    piano,
    matricolaContatore: matricola_contatore,
    calibroContatore: calibro_contatore,
    statoDelPunto: stato_del_punto,
    portata,
    codiceRemi: codice_remi,
  };
}

export default function generaEsito(record: UnaretiContendibilitaGasRecord | null) {
  if (!record) {
    // pdr nel payload non trovato nella tabella
    return new FailureResponse('ko-pdr non contendibile');
  }

  const statoDelPunto = record.stato_del_punto.toLowerCase();


  if (!['disattivato', 'mai attivato'].includes(statoDelPunto)) {
    return new FailureResponse('ko-pdr non contendibile');
  }

  if (statoDelPunto === 'disattivato') {
    return new SuccessResponse('ok-subentro', getSuccessInfo(record));
  }

  if (statoDelPunto === 'mai attivato') {
    return new SuccessResponse('ok-prima attivazione', getSuccessInfo(record));
  }

  return new CasoNonGestitoResponse();
}
