import CasoNonGestitoResponse from "../../../response/CasoNonGestitoResponse";
import SuccessResponse from "../../../response/SuccessResponse";
import { LeretiCheckPdrDatiPerEsito } from "../../../scraperv2/lereti/recuperaDatiPerEsito";

function createExtraInfo(datiPerEsito: LeretiCheckPdrDatiPerEsito) {
  return {
    ...datiPerEsito,
    noteMisuratore: datiPerEsito.noteMisuratore.join(' ')
  }
}

export default function creaEsito(datiPerEsito: LeretiCheckPdrDatiPerEsito) {
  const noteMisuratore = datiPerEsito.noteMisuratore;

  if (noteMisuratore.includes('chiuso') && (noteMisuratore.includes('posato') || noteMisuratore.includes('rimosso'))) {
    return new SuccessResponse("ok-subentro", createExtraInfo(datiPerEsito));
  }

  if (!noteMisuratore || (!noteMisuratore.includes('chiuso') && noteMisuratore.includes('posato'))) {
    return new SuccessResponse("ok-prima attivazione", createExtraInfo(datiPerEsito));
  }

  return new CasoNonGestitoResponse();
}
