/**
 * @openapi
 *
 * /scraper/lereti/v3/check/pdr:
 *  post:
 *    tags:
 *      - v2
 *    description: Va sul portale di Lereti e controlla se il pdr è valido.
 *    consume:
 *      - application/json
 *    produces:
 *      - application/json
 *    responses:
 *      200:
 *        description: SuccessResponse
 *    parameters:
 *      - in: body
 *        description:
 *          pdr da controllare.
 *        schema:
 *          type: object
 *          required:
 *            - pdr
 *          properties:
 *            pdr:
 *              type: string
 *
 * /scraper/queue/leretiCheckPdr:
 *  post:
 *    tags:
 *      - v2
 *      - queue
 *    description:
 *      Va sul portale di Lereti e controlla se il pdr è valido.
 *      Il payload è lo stesso della rotta diretta `/scraper/v3/lereti/check/pdr`,
 *      in più va aggiunto il webhook che lo scraper poi andrà a richiamare per notificare l'esito.
 *    consume:
 *      - application/json
 *    produces:
 *      - application/json
 *    responses:
 *      200:
 *        description: SuccessResponse
 */
