import inserisciPdr from "../../scraperv2/lereti/inserisciPdr";
import LeretiCheckPdr from "../../scraperv2/lereti/LeretiCheckPdr";
import login from "../../scraperv2/lereti/login";
import recuperaDatiPerEsito from "../../scraperv2/lereti/recuperaDatiPerEsito";
import { ScraperResponse, WebScraper } from "../../scraperv2/scraper";
import { ScraperTipologia } from "../../scraperv2/ScraperOptions";
import { QueueTask } from "../../task/QueueTask";
import tryOrThrow from "../../utils/tryOrThrow";
import { LeretiCheckPdrV3Payload } from "./types";
import creaEsito from "./utils/creaEsito";

@QueueTask()
export default class LeretiCheckPdrV3 extends WebScraper<LeretiCheckPdrV3Payload> {
  async login(payload: LeretiCheckPdrV3Payload): Promise<void> {
    const credenziali = await this.getCredenziali(payload);
    const page = await this.p();

    await login(page, LeretiCheckPdr.LOGIN_URL, credenziali);
  }

  async scrapeWebsite(payload: LeretiCheckPdrV3Payload): Promise<ScraperResponse> {
    const logger = this.childLogger;
    const page = await this.p();

    await inserisciPdr(page, payload.pdr);

    const [datiPerEsito, err] = await tryOrThrow(() => recuperaDatiPerEsito(page), "non sono riuscito a recuperare i dati per l'esito:");

    if (err) {
      return err;
    }

    const esito = creaEsito(datiPerEsito!);
    logger.info(`payload: ${JSON.stringify(payload)} | dati per esito: ${JSON.stringify(datiPerEsito)} | esito: ${JSON.stringify(esito)}`);
    return esito;
  }

  getScraperCodice(): string {
    return LeretiCheckPdr.prototype.getScraperCodice();
  }

  getScraperTipologia(): ScraperTipologia {
    return LeretiCheckPdr.prototype.getScraperTipologia();
  }
}
