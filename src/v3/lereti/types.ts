import { BasicPayload } from "../../credenziali/getCredenzialiDalServizio";

export type LeretiCheckPdrV3Payload = BasicPayload & {
  pdr: string;
};
