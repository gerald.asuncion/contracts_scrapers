import { DatiEsito } from "../../../scraperv2/iren/check/recuperaDatiPerEsito";
import { ScraperResponse } from "../../../scraperv2/scraper";
import { IrenPrecheckV3Payload } from "../types";
import { EsitoCallback } from "./types";

export default function executorEsitoCallbacksFactory(callbacks: EsitoCallback[]) {
  const execute = (dati: DatiEsito, payload: IrenPrecheckV3Payload): ScraperResponse | undefined => {
    if (!callbacks.length) return;

    let cb = callbacks.shift() as EsitoCallback;

    let esito = cb(dati, payload);

    if (esito) {
      return esito;
    }

    return execute(dati, payload);
  }

  return execute;
}
