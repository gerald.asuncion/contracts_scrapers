import ErrorResponse from "../../../response/ErrorResponse";
import { DatiEsito } from "../../../scraperv2/iren/check/recuperaDatiPerEsito";
import { ScraperResponse } from "../../../scraperv2/scraper";
import { IrenPrecheckV3Payload } from "../types";
import generaEsitoElettricita from "./generaEsitoElettricita";
import generaEsitoGas from "./generaEsitoGas";
import gestioneEsitoComune from "./gestioneEsitoComune";

const CB_MAP: Record<string, (dati: DatiEsito, payload: IrenPrecheckV3Payload) => ScraperResponse | undefined> = {
  'gas': generaEsitoGas,
  'elettricita': generaEsitoElettricita,
  'Gas': generaEsitoGas,
  'Elettricità': generaEsitoElettricita
};

export default function generaEsito(dati: DatiEsito, payload: IrenPrecheckV3Payload) {
  const cb = CB_MAP[payload.fornitura];

  if (!cb) {
    throw new Error(`funzione per generare l'esito per il tipo contratto ${payload.fornitura} non trovata`);
  }

  return cb(dati, payload) || gestioneEsitoComune(dati, payload) || new ErrorResponse(dati.dettaglioEsitoPrecheck);
}
