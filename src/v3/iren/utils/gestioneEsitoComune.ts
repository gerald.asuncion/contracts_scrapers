import ErrorResponse from "../../../response/ErrorResponse";
import FailureResponse from "../../../response/FailureResponse";
import SuccessResponse from "../../../response/SuccessResponse";
import { DatiEsito } from "../../../scraperv2/iren/check/recuperaDatiPerEsito";
import { IrenPrecheckV3Payload } from "../types";
import executorEsitoCallbacksFactory from "./executorEsitoCallbacksFactory";
import { EsitoCallback } from "./types";

// ATTENZIONE: l'ordine è importante
const getEsitoCallbacksForCommons: () => EsitoCallback[] = () => [
  ({ esito, esitoCompatibilita, venditoreUscente }: DatiEsito, payload: IrenPrecheckV3Payload) => {
    if (esito === 'OK' && esitoCompatibilita === 'KO') {
      return new SuccessResponse('Su questo punto di fornitura è già in attivazione un contratto Iren', { venditoreUscente });
    }
  },
  ({ esito, esitoCompatibilita, venditoreUscente }: DatiEsito, payload: IrenPrecheckV3Payload) => {
    if (esito === 'OK' && esitoCompatibilita === 'OK') {
      return new SuccessResponse('Ok', { venditoreUscente });
    }
  },
  ({ esito, dettaglioEsitoPrecheck, esitoCompatibilita }: DatiEsito, payload: IrenPrecheckV3Payload) => {
    if (esito === 'KO' && dettaglioEsitoPrecheck === 'Punto di prelievo non attivo' && esitoCompatibilita === 'KO') {
      return new FailureResponse('Punto di prelievo non attivo-Su questo punto di fornitura è già in attivazione un contratto Iren');
    }
  },
  ({ esito, dettaglioEsitoPrecheck, esitoCompatibilita }: DatiEsito, payload: IrenPrecheckV3Payload) => {
    if (esito === 'KO' && dettaglioEsitoPrecheck === 'Punto di prelievo non attivo' && esitoCompatibilita === 'OK') {
      return new FailureResponse(dettaglioEsitoPrecheck);
    }
  },
  ({ dettaglioEsitoPrecheck }: DatiEsito, payload: IrenPrecheckV3Payload) => {
    if ([
      'sql exception',
      'errore tecnico',
      "errore nell'applicazione adibita alla chiamata verso la pdc per il pkg1",
      'nessuna risposta dal sistema',
    ].includes(dettaglioEsitoPrecheck)) {
      return new ErrorResponse(dettaglioEsitoPrecheck);
    }
  },
  ({ esito, dettaglioEsitoPrecheck, esitoCompatibilita }: DatiEsito, payload: IrenPrecheckV3Payload) => {
    if (esito === 'KO' && dettaglioEsitoPrecheck === 'Presenza di dati identificativi del cliente finale non corretti per più di 2 caratteri alfanumerici' && esitoCompatibilita === 'OK') {
      return new FailureResponse(dettaglioEsitoPrecheck);
    }
  },
  ({ esito, dettaglioEsitoPrecheck, esitoCompatibilita }: DatiEsito, payload: IrenPrecheckV3Payload) => {
    if (esito === 'KO' && dettaglioEsitoPrecheck === 'Presenza di dati identificativi del cliente finale non corretti per più di 2 caratteri alfanumerici' && esitoCompatibilita === 'KO') {
      return new FailureResponse('Presenza di dati identificativi del cliente finale non corretti per più di 2 caratteri alfanumerici-su questo punto di fornitura è già in attivazione un contratto iren');
    }
  },
  ({ esito, dettaglioEsitoPrecheck, esitoCompatibilita }: DatiEsito, payload: IrenPrecheckV3Payload) => {
    if (esito === 'KO' && dettaglioEsitoPrecheck === 'Presenza di dati identificativi del cliente finale non corretti per meno di 2 caratteri alfanumerici' && esitoCompatibilita === 'OK') {
      return new FailureResponse(dettaglioEsitoPrecheck);
    }
  },
  ({ esito, dettaglioEsitoPrecheck, esitoCompatibilita }: DatiEsito, payload: IrenPrecheckV3Payload) => {
    if (esito === 'KO' && dettaglioEsitoPrecheck === 'Presenza di dati identificativi del cliente finale non corretti per meno di 2 caratteri alfanumerici' && esitoCompatibilita === 'KO') {
      return new FailureResponse('Presenza di dati identificativi del cliente finale non corretti per meno di 2 caratteri alfanumerici--su questo punto di fornitura è già in attivazione un contratto iren');
    }
  },
  ({ esito, dettaglioEsitoPrecheck, esitoCompatibilita }: DatiEsito, payload: IrenPrecheckV3Payload) => {
    if (esito === 'KO' && dettaglioEsitoPrecheck === 'Cliente inesistente' && esitoCompatibilita === 'OK') {
      return new FailureResponse(dettaglioEsitoPrecheck);
    }
  },
  ({ esito, dettaglioEsitoPrecheck, esitoCompatibilita }: DatiEsito, payload: IrenPrecheckV3Payload) => {
    if (esito === 'KO' && dettaglioEsitoPrecheck === 'Cliente inesistente' && esitoCompatibilita === 'KO') {
      return new FailureResponse('Cliente inesistente-Su questo punto di fornitura è già in attivazione un contratto Iren');
    }
  },
];

export default function gestioneEsitoComune(dati: DatiEsito, payload: IrenPrecheckV3Payload) {
  const cb = executorEsitoCallbacksFactory(getEsitoCallbacksForCommons());
  return cb(dati, payload);
}
