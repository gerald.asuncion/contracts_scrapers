import FailureResponse from "../../../response/FailureResponse";
import { DatiEsito } from "../../../scraperv2/iren/check/recuperaDatiPerEsito";
import { IrenPrecheckV3Payload } from "../types";
import executorEsitoCallbacksFactory from "./executorEsitoCallbacksFactory";
import { EsitoCallback } from "./types";

// ATTENZIONE: l'ordine è importante
const getEsitoCallbacksForElettricita: () => EsitoCallback[] = () => [
  ({ esito, dettaglioEsitoPrecheck }: DatiEsito, payload: IrenPrecheckV3Payload) => {
    if (esito === 'KO' && dettaglioEsitoPrecheck === 'POD inesistente') {
      return new FailureResponse('POD inesistente');
    }
  },
];

export default function generaEsitoElettricita(dati: DatiEsito, payload: IrenPrecheckV3Payload) {
  const cb = executorEsitoCallbacksFactory(getEsitoCallbacksForElettricita());
  return cb(dati, payload);
}
