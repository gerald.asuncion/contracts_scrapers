import { DatiEsito } from "../../../scraperv2/iren/check/recuperaDatiPerEsito";
import { ScraperResponse } from "../../../scraperv2/scraper";
import { IrenPrecheckV3Payload } from "../types";

export type EsitoCallback = (dati: DatiEsito, payload: IrenPrecheckV3Payload) => ScraperResponse | undefined;
