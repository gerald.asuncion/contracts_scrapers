import ErrorResponse from "../../../response/ErrorResponse";
import FailureResponse from "../../../response/FailureResponse";
import { DatiEsito } from "../../../scraperv2/iren/check/recuperaDatiPerEsito";
import { IrenPrecheckV3Payload } from "../types";
import executorEsitoCallbacksFactory from "./executorEsitoCallbacksFactory";
import { EsitoCallback } from "./types";

// ATTENZIONE: l'ordine è importante
const getEsitoCallbacksForGas: () => EsitoCallback[] = () => [
  ({ esito, dettaglioEsitoPrecheck }: DatiEsito, payload: IrenPrecheckV3Payload) => {
    if (esito === 'KO' && dettaglioEsitoPrecheck === "PDR già attivo con l'utente richiedente") {
      return new ErrorResponse('Non è possibile effettuare il controllo per il pod/pdr inserito. Utilizzare un altro portale per fare la verifica', { venditoreUscente: 'Iren Mercato SpA' });
    }
  },
  ({ esito, dettaglioEsitoPrecheck }: DatiEsito, payload: IrenPrecheckV3Payload) => {
    if (esito === 'KO' && dettaglioEsitoPrecheck === 'PDR inesistente') {
      return new FailureResponse('PDR inesistente');
    }
  },
];

export default function generaEsitoGas(dati: DatiEsito, payload: IrenPrecheckV3Payload) {
  const cb = executorEsitoCallbacksFactory(getEsitoCallbacksForGas());
  return cb(dati, payload);
}
