import { FornitorePayload, IrenPreCheckPayload } from "../../scraperv2/iren/payload/IrenPreCheckPayload";

export type IrenPrecheckV3Payload = IrenPreCheckPayload & FornitorePayload & {webhook: string};
