import waitCheckCredentials from "../../browser/page/waitCheckCredentials";
import waitForSelectorAndClick from "../../browser/page/waitForSelectorAndClick";
import waitForSelectorAndType from "../../browser/page/waitForSelectorAndType";
import waitForVisible from "../../browser/page/waitForVisible";
import FailureResponse from "../../response/FailureResponse";
import inserisciDatiCliente from "../../scraperv2/iren/check/inserisciDatiCliente";
import recuperaDatiPerEsito from "../../scraperv2/iren/check/recuperaDatiPerEsito";
import waitForError from "../../scraperv2/iren/check/waitForError";
import { ScraperResponse, WebScraper } from "../../scraperv2/scraper";
import { SCRAPER_TIPOLOGIA } from "../../scraperv2/ScraperOptions";
import { QueueTask } from "../../task/QueueTask";
import extractErrorMessage from "../../utils/extractErrorMessage";
import tryOrThrow from "../../utils/tryOrThrow";
import { IrenPrecheckV3Payload } from "./types";
import generaEsito from "./utils/generaEsito";

const PAGE_AVAILABLE_SELECTOR = '[name="selectClasse"]';

@QueueTask({scraperName: 'irenPrecheckV3'})
export default class IrenPrecheckV3 extends WebScraper<IrenPrecheckV3Payload> {
  static readonly LOGIN_URL = 'https://iren.force.com/s/utilitycustomercheck';

  async login(): Promise<void> {
    const { username, password } = await this.getCredenziali();
    this.checkPageClosed();

    const page = await this.p();
    const logger = this.childLogger;

    // se ha mantenuto la sessione e sono già loggato ritorno subito
    await page.goto(IrenPrecheckV3.LOGIN_URL, { waitUntil: 'networkidle2' });
    const sel = await page.$(PAGE_AVAILABLE_SELECTOR);
    if (sel !== null) {
      logger.info('login > utente già loggato');
      return;
    }
    await waitForVisible(page, '#sfdc_username_container input');

    await tryOrThrow(
      () => waitForSelectorAndType(page, '#sfdc_username_container input', username),
      'non sono riuscito ad inserire lo username:'
    );

    await tryOrThrow(
      () => waitForSelectorAndType(page, '#sfdc_password_container input', password),
      'non sono riuscito ad inserire la password:'
    );

    await tryOrThrow(
      () => waitForSelectorAndClick(page, '.loginButton'),
      'non sono riuscito a cliccare sul pulsante per la login:'
    );

    await Promise.race([
      waitCheckCredentials(page, '.uiOutputRichText', IrenPrecheckV3.LOGIN_URL, username, logger),
      waitForVisible(page, PAGE_AVAILABLE_SELECTOR)
    ]);
  }

  async scrapeWebsite(payload: IrenPrecheckV3Payload): Promise<ScraperResponse> {
    const logger = this.childLogger;
    const page = await this.p();

    try {
      await inserisciDatiCliente(page, payload);
    } catch (ex) {
      const exMsg = extractErrorMessage(ex);

      if (exMsg.includes("cannot read property 'touppercase' of")) {
        return new FailureResponse("Inserire i dati dell'intestatario uscente");
      }
    }

    let err = await waitForError(page);
    if (err) {
      logger.error(err.details);
      return err;
    }

    const dati = await recuperaDatiPerEsito(page);

    const esito = generaEsito(dati, payload);
    logger.info(`payload: ${JSON.stringify(payload)} | dati per esito: ${JSON.stringify(dati)} | esito: ${JSON.stringify(esito)}`);

    return esito;
  }

  getScraperCodice() {
    return 'iren-precheck';
  }

  getScraperTipologia() {
    return SCRAPER_TIPOLOGIA.FATTIBILITA;
  }
}
