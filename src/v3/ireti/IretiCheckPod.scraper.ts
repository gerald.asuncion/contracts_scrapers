import stepCompileForm from "../../scraperv2/ireti/helpers/stepCompileForm";
import stepLogin from "../../scraperv2/ireti/helpers/stepLogin";
import stepNavigateToForm from "../../scraperv2/ireti/helpers/stepNavigateToForm";
import stepWaitIframesLoading from "../../scraperv2/ireti/helpers/stepWaitIframesLoading";
import webHelpers from "../../scraperv2/ireti/helpers/webHelpers";
import IretiCheckPod from "../../scraperv2/ireti/IretiCheckPod";
import { ScraperResponse, WebScraper } from "../../scraperv2/scraper";
import { ScraperTipologia, SCRAPER_TIPOLOGIA } from "../../scraperv2/ScraperOptions";
import { QueueTask } from "../../task/QueueTask";
import { IretiCheckPodV3Payload } from "./types";
import creaEsito from "./utils/creaEsito";
import recuperaDatiPerEsito from "./utils/recuperaDatiPerEsito";

@QueueTask()
export default class IretiCheckPodV3 extends WebScraper<IretiCheckPodV3Payload> {
  private static LOGIN_URL = IretiCheckPod.LOGIN_URL;

  async login(payload: IretiCheckPodV3Payload | undefined): Promise<void> {
    const logger = this.childLogger;
    const { username, password } = await this.getCredenziali();
    // const { username = 'IELP0013QZRS', password = 'LupitaLupita22!' } = {};

    logger.info('step > login');

    this.checkPageClosed();

    return stepLogin(await this.p(), logger, IretiCheckPodV3.LOGIN_URL, username, password, payload)
  }

  async scrapeWebsite(payload: IretiCheckPodV3Payload): Promise<ScraperResponse> {
    const logger = this.childLogger;

    const page = await this.p();

    let stepError = await stepWaitIframesLoading(page);
    if (stepError) {
      return stepError;
    }

    const {
      pod,
      codicePratica = `SUPERMONEY${(Math.random() * 46656) | 0}`
    } = payload;

    const {
      selectIframe,
      selectElementByXPath,
      typeElement,
      waitForDefined,
      waitForDelay,
      logger_info
    } = await webHelpers(page, logger, `[codicePratica=${codicePratica}]`);

    stepError = await stepNavigateToForm(page, 'SC1 – Verifica sito contendibile');
    if (stepError) {
      return stepError;
    }

    stepError = await stepCompileForm(page, 'Pod/Matricola Misuratore', codicePratica, pod);
    if (stepError) {
      return stepError;
    }

    const datiPerEsito = await recuperaDatiPerEsito(page, "pod", [
      "Via Pod",
      "Numero Pod",
      "Località Pod",
      "Cap Pod",
      "Provincia Pod",
      "Tensione",
      "Potenza Disponibile",
      "Potenza in Franchigia",
      "Matricola Misuratore",
      "ISTAT Pod",
    ]);

    const esito = creaEsito(datiPerEsito, 'pod');
    logger.info(`per il payload ${JSON.stringify(payload)} recuperate info ${JSON.stringify(datiPerEsito)} e generato esito ${JSON.stringify(esito)}`);

    return esito;
  }

  /**
   * @override
   */
  getScraperCodice(): string {
    return 'ireti-checkpod';
  }

  /**
   * @override
   */
  getScraperTipologia(): ScraperTipologia {
    return SCRAPER_TIPOLOGIA.FATTIBILITA;
  }
}
