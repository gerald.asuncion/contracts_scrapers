import IretiCheckPdrPayload from "../../scraperv2/ireti/check/IretiCheckPdrPayload";
import IretiCheckPodPayload from "../../scraperv2/ireti/check/IretiCheckPodPayload";

export type IretiCheckPodV3Payload = Omit<IretiCheckPodPayload, 'tipoContratto'>;

export type IretiCheckPdrV3Payload = Omit<IretiCheckPdrPayload, 'tipoContratto'>;
