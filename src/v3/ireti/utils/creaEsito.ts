import camelCase from "lodash/camelCase";
import CasoNonGestitoResponse from "../../../response/CasoNonGestitoResponse";
import FailureResponse from "../../../response/FailureResponse";
import SuccessResponse from "../../../response/SuccessResponse";
import { DatiPerEsito } from "./recuperaDatiPerEsito";

function normilzeString(str: string) {
  return str.normalize("NFD").replace(/[\u0300-\u036f]/g, "");
}

export default function creaEsito({ esito, ...rest }: DatiPerEsito, type: 'pdr' | 'pod') {
  if (esito === 'rifiutata') {
    return new FailureResponse(type === 'pod' ? 'ko-pod non contendibile' : 'ko-pdr non contendibile');
  }

  if (esito === 'positiva') {
    const extra: Record<string, string> = {};

    for (const key in rest) {
      extra[camelCase(normilzeString(key))] = rest[key];
    }

    return new SuccessResponse('Ok', extra);
  }

  return new CasoNonGestitoResponse();
}
