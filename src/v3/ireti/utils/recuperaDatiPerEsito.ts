import { Page } from "puppeteer";
import webHelpers from "../../../scraperv2/ireti/helpers/webHelpers";
import tryOrThrow from "../../../utils/tryOrThrow";

export type DatiPerEsito = {
  esito: string;
  [key: string]: string;
};

export default async function recuperaDatiPerEsito(page: Page, type: 'pdr' | 'pod', fieldLabels: string[]): Promise<DatiPerEsito> {
  // here only for local declarations
  const {
    selectIframe,
    selectElementByXPath,
    typeElement,
    waitForDefined,
    waitForDelay,
    logger_info
  } = await webHelpers(page, undefined as any);

  async function retrieveFields(page: Page) {
    const selectIframe: any = null;
    const selectElementByXPath: any = null;

    return tryOrThrow(() => page.evaluate((_fieldLabels: string[]) => {
      logger_info("recupero l'esito");

      const iframe = selectIframe('iframe#contentAreaFrame', 'iframe#isolatedWorkArea');
      const getInputValueByLabel = (label: string) => {
        logger_info(`dati immobile: '${label}'`);
        return selectElementByXPath(`//span[text() = "${label}"]//ancestor::tr[1]//input`, iframe).value;
      };

      const result: Record<string, string> = {};
      for (const fieldLabel of _fieldLabels) {
        result[fieldLabel] = getInputValueByLabel(fieldLabel);
      }

      return result;
    }, fieldLabels), "non sono riuscito a recuperare i dati da restituire:");
  }

  const esito = await tryOrThrow(() => page.evaluate(() => {
    const iframe = selectIframe('iframe#contentAreaFrame', 'iframe#isolatedWorkArea');

    const elBtnInoltra = selectElementByXPath('//span[text()="Inoltra"]', iframe)!;
    elBtnInoltra.click();

    return waitForDefined(() => selectElementByXPath('//span[text() = "Doc. rif. distr."]/ancestor::span[contains(@class, "lsFlowLayoutItem--wrapping")]//span[contains(@class, "lsTextView--paragraph")]', iframe)!.innerText)
    .catch(() => {
      logger_info("'Doc. rif. distr.': assente");
    })
    .then((docRifDistr) => {
      logger_info(`'Doc. rif. distr.': '${docRifDistr}'`);

      const elTextEsito = selectElementByXPath('//span[text() = "Stato Richiesta"]/ancestor::span[contains(@class, "lsFlowLayoutItem--wrapping")]//span[contains(@class, "lsTextView--paragraph")]', iframe)!;
      logger_info(elTextEsito ? `testo esito: ${elTextEsito.innerText}` : 'testo esito non trovato');

      return elTextEsito.innerText.toLowerCase();
    });
  }), "non sono riuscito a ricavare l'esito:");

  if (esito === 'positiva') {
    return {
      esito,
      ...(await retrieveFields(page))
    };
  }
  return { esito };
}
