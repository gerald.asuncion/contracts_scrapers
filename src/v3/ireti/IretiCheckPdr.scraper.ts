import stepCompileForm from "../../scraperv2/ireti/helpers/stepCompileForm";
import stepLogin from "../../scraperv2/ireti/helpers/stepLogin";
import stepNavigateToForm from "../../scraperv2/ireti/helpers/stepNavigateToForm";
import stepWaitIframesLoading from "../../scraperv2/ireti/helpers/stepWaitIframesLoading";
import webHelpers from "../../scraperv2/ireti/helpers/webHelpers";
import IretiCheckPdr from "../../scraperv2/ireti/IretiCheckPdr";
import { ScraperResponse, WebScraper } from "../../scraperv2/scraper";
import { SCRAPER_TIPOLOGIA } from "../../scraperv2/ScraperOptions";
import { QueueTask } from "../../task/QueueTask";
import { IretiCheckPdrV3Payload } from "./types";
import creaEsito from "./utils/creaEsito";
import recuperaDatiPerEsito from './utils/recuperaDatiPerEsito';

@QueueTask()
export default class IretiCheckPdrV3 extends WebScraper<IretiCheckPdrV3Payload> {
  static readonly LOGIN_URL = IretiCheckPdr.LOGIN_URL;

  async login(payload: IretiCheckPdrV3Payload): Promise<void> {
    const logger = this.childLogger;
    const { username, password } = await this.getCredenziali();

    return stepLogin(await this.p(), logger, IretiCheckPdrV3.LOGIN_URL, username, password, payload);
  }

  async scrapeWebsite(payload: IretiCheckPdrV3Payload): Promise<ScraperResponse> {
    const logger = this.childLogger;

    const page = await this.p();

    let stepError = await stepWaitIframesLoading(page);
    if (stepError) {
      return stepError;
    }

    const {
      pdr,
      codicePratica = `SUPERMONEY${(Math.random() * 46656) | 0}`
    } = payload;

    const {
      selectIframe,
      selectElementByXPath,
      typeElement,
      waitForDefined,
      waitForDelay,
      logger_info
    } = await webHelpers(page, logger, `[codicePratica=${codicePratica}]`);

    stepError = await stepNavigateToForm(page, 'SC1 - Siti Contendibili');
    if (stepError) {
      return stepError;
    }

    stepError = await stepCompileForm(page, 'PdR', codicePratica, pdr);
    if (stepError) {
      return stepError;
    }

    const datiPerEsito = await recuperaDatiPerEsito(page, "pdr", [
      "Provincia Immobile",
      "Località Immobile",
      "ISTAT Immobile",
      "Via Immobile",
      "Numero Civico Immobile",
      "Matricola Misuratore",
      "Potenzialità minima",
      "Potenzialità massima",
      "Calibro",
      "REMI",
    ]);

    const esito = creaEsito(datiPerEsito, 'pdr');
    logger.info(`per il payload ${JSON.stringify(payload)} recuperate info ${JSON.stringify(datiPerEsito)} e generato esito ${JSON.stringify(esito)}`);

    return esito;
  }

  /**
   * @override
   */
  getScraperCodice() {
    return 'ireti-checkpdr';
  }

  /**
   * @override
   */
  getScraperTipologia() {
    return SCRAPER_TIPOLOGIA.FATTIBILITA;
  }
}
