/**
 * @openapi
 *
 * /scraper/v3/edistribuzione/check/pod:
 *  post:
 *    tags:
 *      - v3
 *    description: Effettua il controllo del pod sul sito di E-distribuzione.
 *    consume:
 *      - application/json
 *    produces:
 *      - application/json
 *    responses:
 *      200:
 *        description: SuccessResponse
 *    parameters:
 *      - in: body
 *        schema:
 *          type: object
 *          required:
 *            - pod
 *          properties:
 *            pod:
 *              type: string
 *
 * /scraper/queue/eDistribuzioneCheckPod:
 *  post:
 *    tags:
 *      - v3
 *      - queue
 *    description:
 *      Effettua il controllo del pod sul sito di E-distribuzione.
 *      Il payload è lo stesso della rotta diretta `/scraper/v3/edistribuzione/check/pod`,
 *      in più va aggiunto il webhook che lo scraper poi andrà a richiamare per notificare l'esito.
 *    consume:
 *      - application/json
 *    produces:
 *      - application/json
 *    responses:
 *      200:
 *        description: SuccessResponse
 */
