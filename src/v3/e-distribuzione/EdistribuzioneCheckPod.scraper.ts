import waitForNavigation from "../../browser/page/waitForNavigation";
import waitForSelectorAndClick from "../../browser/page/waitForSelectorAndClick";
import waitForSelectorAndType from "../../browser/page/waitForSelectorAndType";
import waitForVisible from "../../browser/page/waitForVisible";
import WrongCredentialsError from "../../errors/WrongCredentialsError";
import ErrorResponse from "../../response/ErrorResponse";
import delay from "../../utils/delay";
import extractErrorMessage from "../../utils/extractErrorMessage";
import { ScraperResponse, WebScraper } from "../../scraperv2/scraper";
import { SCRAPER_TIPOLOGIA } from "../../scraperv2/ScraperOptions";
import estraiInfoControllaStatiPerOneForm from "./check-pod/estraiInfoControllaStatiPerOneForm";
import { EdistribuzioneCheckPodPayload } from "../../scraperv2/eDistribuzione/types";
import vaiAllaPaginaDiControllo from "../../scraperv2/eDistribuzione/vaiAllaPaginaDiControllo";
import { QueueTask } from "../../task/QueueTask";

@QueueTask({ scraperName: 'eDistribuzioneCheckPod' })
export default class EdistribuzioneCheckPod extends WebScraper<EdistribuzioneCheckPodPayload> {
  static readonly LOGIN_URL = 'https://4pt.e-distribuzione.it/';

  /**
   * @override
   */
  getLoginTimeout(): number { return 90000; }

  async login(): Promise<void> {
    const { username, password } = await this.getCredenziali();

    this.checkPageClosed();

    const logger = this.childLogger;

    const page = await this.p();
    await page.goto(EdistribuzioneCheckPod.LOGIN_URL, { waitUntil: 'networkidle0' });

    if (await page.$('#header-overlay > div.cHeaderLine.slds-grid > div.cSearchPublisher') !== null) return;

    // attendo la input per l'inserimento della username
    await waitForVisible(page, 'input[type=text]');
    // attendo la input per l'inserimento della passsword
    await waitForVisible(page, 'input[type=password]');

    await waitForSelectorAndType(page, 'input[type=text]', username);
    // inserisco la password
    await waitForSelectorAndType(page, 'input[type=password]', password);

    await delay(500);

    try {
      await Promise.all([
        waitForNavigation(page),
        waitForSelectorAndClick(page, '#centerPanel > div > div.slds-col--padded.contentRegion.comm-layout-column > div > div:nth-child(2) > div > div:nth-child(4) > button'),
      ]);
    } catch (navEx) {
      const navErrMsg = extractErrorMessage(navEx);
      logger.warn(navErrMsg);
      logger.info('controllo se le credenziali sono ancora valide');
      try {
        await waitForVisible(page, '#centerPanel > div > div.slds-col--padded.contentRegion.comm-layout-column > div > div:nth-child(2) > div > span > div > div');
        logger.error('credenziali scadute');
        throw new WrongCredentialsError(EdistribuzioneCheckPod.LOGIN_URL, username);
      } catch (ex) {
        if (ex instanceof WrongCredentialsError) {
          throw ex;
        }
        // nulla da fare le credenziali sono corrette
        logger.info('credenziali valide');
        throw new Error(`login > ${navErrMsg}`);
      }
    }

    await waitForVisible(page, '.apexp');

    await page.$eval('#thePage\\:j_id2\\:i\\:f\\:pb\\:d\\:Accetta\\.input', (el) => { (el as HTMLInputElement).checked = true; });
    await page.evaluate(() => {
      const el = document.querySelector<HTMLInputElement>('.dataCol input[type=hidden]');
      if (el) {
        el.value = 'true';
      }
    });

    logger.info('clicco sul button della modale');
    await waitForSelectorAndClick(page, '.FlowFinishBtn');
  }

  async scrapeWebsite(payload: EdistribuzioneCheckPodPayload): Promise<ScraperResponse> {
    const logger = this.childLogger;

    const page = await this.p();

    try {
      logger.info('vado alla pagina per controllare lo stato');
      await vaiAllaPaginaDiControllo(page, logger, payload);

      const result = await estraiInfoControllaStatiPerOneForm(page, logger, payload, Date.now());

      return result;
      } catch (e) {
      const errMsg = extractErrorMessage(e);
      logger.error(errMsg);
      return new ErrorResponse(errMsg);
    }
  }

  /**
   * @override
   */
  getScraperCodice() {
    return 'edistribuzione';
  }

  /**
   * @override
   */
  getScraperTipologia() {
    return SCRAPER_TIPOLOGIA.FATTIBILITA;
  }
}
