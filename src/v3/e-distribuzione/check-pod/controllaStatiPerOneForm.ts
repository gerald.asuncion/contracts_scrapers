import { Page } from "puppeteer";
import { Logger } from "../../../logger";
import { Info } from "../../../scraperv2/eDistribuzione/estraiInfo";
import ErrorResponse from "../../../response/ErrorResponse";
import SuccessResponse from "../../../response/SuccessResponse";
import { estraiInfoDaRestituire } from "../../../scraperv2/eDistribuzione/estraiSuccessInfo";
import { ScraperResponse } from "../../../scraperv2/scraper";
// import CasoNonGestitoResponse from '../../response/CasoNonGestitoResponse';

export default async function controllaStatiPerOneForm(
  page: Page,
  logger: Logger,
  { stato, statoPod }: Info
): Promise<ScraperResponse | null> {
  logger.info(`Stato = ${stato}`);
  logger.info(`Stato POD = ${statoPod}`);

  if (stato === 'Inserita_' && (statoPod === 'Presa in carico' || statoPod === 'Inserita')) {
    return null;
  }

  if (stato === 'Evasa_' && (statoPod === 'Cessato' || statoPod === 'Predisposto')) {
    const {
      toponimo,
      via,
      civico,
      scala,
      interno,
      localita,
      comune,
      provincia,
      cap,
      potenzaContrattuale,
      tensione,
      codiceIstatComune,
      piano,
      codiceEneltel,
      misuratoreMatricola,
      misuratoreTipologia,
      livelloTensione
    } = await estraiInfoDaRestituire(page);

    const details = statoPod === 'Cessato' ? 'ok -subentro' : 'ok-prima attivazione';

    return new SuccessResponse(details, {
      toponimo,
      via,
      civico,
      scala,
      interno,
      localita,
      comune,
      provincia,
      cap,
      potenzaContrattuale: potenzaContrattuale || null,
      tensione: tensione || null,
      codiceIstatComune,
      piano,
      codiceEneltel,
      misuratoreMatricola,
      misuratoreTipologia,
      livelloTensione
    });
  }

  logger.error('caso non gestito');
  return new ErrorResponse('Esito non mappato, segnalarlo al back-office di supermoney e Verificare il punto di fornitura col back-office prima di proseguire');
}
