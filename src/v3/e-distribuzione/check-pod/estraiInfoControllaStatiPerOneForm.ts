import { Page } from "puppeteer";
import { Logger } from "../../../logger";
import { EdistribuzioneCheckPodPayload } from "../../../scraperv2/eDistribuzione/types";
import estraiInfo, { Info } from "../../../scraperv2/eDistribuzione/estraiInfo";
import ErrorResponse from "../../../response/ErrorResponse";
import { ScraperResponse } from "../../../scraperv2/scraper";
import controllaStatiPerOneForm from "./controllaStatiPerOneForm";

const ESITO_ERRORE = 'Il portale non sta restituendo esito, segnalarlo al back-office di supermoney e Verificare il punto di fornitura col back-office prima di proseguire';

export default async function estraiInfoControllaStatiPerOneForm(
  page: Page,
  logger: Logger,
  payload: EdistribuzioneCheckPodPayload,
  timestamp: number,
  count = 1
): Promise<ScraperResponse> {
  logger.info("estraggo l'id della richiesta");

  logger.info(`estraggo gli stati nel portale. tentativo ${count}`);
  const [info, err] = await estraiInfo(page, logger);

  if (err) {
    err.extra = { idRichiesta: info.idRichiesta };
    return err;
  }

  logger.info('controllo gli stati');
  const result = await controllaStatiPerOneForm(page, logger, info as Info);

  if (!result) {
    if (((Date.now() - timestamp)/1000) >= 60) {
      return new ErrorResponse(ESITO_ERRORE);
    }

    await page.reload({ waitUntil: 'networkidle2' });

    return estraiInfoControllaStatiPerOneForm(page, logger, payload, timestamp, ++count);
  }
  result.extra = { ...result.extra, idRichiesta: info.idRichiesta };

  return result;
}
