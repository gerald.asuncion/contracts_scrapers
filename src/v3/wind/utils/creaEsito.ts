import ErrorResponse from "../../../response/ErrorResponse";
import FailureResponse from "../../../response/FailureResponse";
import SuccessResponse from "../../../response/SuccessResponse";

export default function creaEsito(esitoPortale: string) {
  const message = esitoPortale.trim().toLowerCase().replace(/è/g, "e'").replace(/é/g, "e'");

  if (
    ~message.indexOf("anagrafica risulta bloccata".toLowerCase()) ||
    ~message.indexOf("non e' possibile procedere con l’inserimento".toLowerCase()) ||
    ~message.indexOf("procedere con il pagamento delle fatture sospese".toLowerCase()) ||
    ~message.indexOf("cliente non lavorabile".toLowerCase())
  ) {
    return new FailureResponse("Cliente non acquisibile.");
  }

  if (
    message.startsWith("non e' stato trovato nessun cliente con il criterio di ricerca utilizzato.".toLowerCase()) ||
    message.startsWith("e' possibile procedere con l'inserimento")
  ) {
    return new SuccessResponse();
  }

  if (message === "per errore tecnico non e' stato possibile verificare il cliente, riprovare oppure procedere eventualmente inserendo direttamente l'attivazione da pos".toLocaleLowerCase()) {
    return new FailureResponse('Errore Tecnico');
  }

  return new ErrorResponse(message);
}
