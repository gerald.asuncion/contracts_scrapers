import getElementText from "../../browser/page/getElementText";
import waitForNavigation from "../../browser/page/waitForNavigation";
import waitForSelectorAndType from "../../browser/page/waitForSelectorAndType";
import waitForVisible from "../../browser/page/waitForVisible";
import waitForXPathAndClick from "../../browser/page/waitForXPathAndClick";
import ErrorResponse from "../../response/ErrorResponse";
import { ScraperResponse } from "../../scraperv2/scraper";
import WindVerificaMorosita from "../../scraperv2/wind/WindVerificaMorosita";
import { QueueTask } from "../../task/QueueTask";
import delay from "../../utils/delay";
import extractErrorMessage from "../../utils/extractErrorMessage";
import tryOrThrow from "../../utils/tryOrThrow";
import { WindVerificaMorositaV3Payload } from "./types";
import creaEsito from "./utils/creaEsito";
import FailureResponse from "../../response/FailureResponse";

@QueueTask()
export default class WindVerificaMorositaV3 extends WindVerificaMorosita {
  async scrapeWebsite(payload: WindVerificaMorositaV3Payload): Promise<ScraperResponse> {
    if (this.hasPasswordToChange) {
      return new FailureResponse("Credentials Error: " + this.hasPasswordToChange);
    }
    if (this.hasCertificateErrorFlag) {
      return new FailureResponse("Certificate Error: " + this.hasCertificateErrorFlag);
    }

    const logger = this.childLogger;
    const page = await this.p();

    await delay(200);
    await tryOrThrow(
      () => waitForXPathAndClick(page, '//div[@id="attivita-logged"][contains(., "Attività")]'),
      'non sono riuscito a cliccare su `Attività`:'
    );

    await Promise.all([
      waitForNavigation(page),
      delay(200),
      waitForXPathAndClick(page, '//a[contains(., "Preverifica CF/P.IVA attivazione")]')
    ]);

    await tryOrThrow(
      () => waitForSelectorAndType(page, 'input[data-placeholder="Codice Fiscale, Partita IVA"]', payload.codiceFiscale),
      'non sono riuscito ad inserire il codice fiscale:'
    );

    await tryOrThrow(
      () => waitForXPathAndClick(page, '//button[contains(., "Cerca")]'),
      'non sono riuscito a cliccare su `Cerca`:'
    );

    let esitoPortale: string;
    try {
      await waitForVisible(page, 'posevo-notification');
      esitoPortale = await getElementText(page, 'posevo-notification div.content', 'textContent') as string;
    } catch (ex) {
      // modale di success non apparsa
      // recupero altro messaggio
      try {
        await waitForVisible(page, 'posevo-invoices-list');
        esitoPortale = await getElementText(page, 'posevo-invoices-list > div > div > div > div> div.col', 'textContent') as string;
      } catch (ex) {
        return new ErrorResponse(`non sono riuscito a recuperare l esito : ${extractErrorMessage(ex)}`);
      }
    }

    const esito = creaEsito(esitoPortale);
    logger.info(`per il payload ${JSON.stringify(payload)} recuperate info ${esitoPortale} e generato esito ${JSON.stringify(esito)}`);

    return esito;
  }
}
