/**
 * @openapi
 *
 * /scraper/v3/wind/fattibilita/morosita:
 *  post:
 *    tags:
 *      - v3
 *    description: Va sul portale di Wind ed effettua il controllo sulla morosità.
 *    consume:
 *      - application/json
 *    produces:
 *      - application/json
 *    responses:
 *      200:
 *        description: SuccessResponse
 *    parameters:
 *      - in: body
 *        description:
 *          pod da controllare.
 *        schema:
 *          type: object
 *          required:
 *            - codiceFiscale
 *          properties:
 *            codiceFiscale:
 *              type: string
 *
 * /scraper/queue/windVerificaMorositaV3:
 *  post:
 *    tags:
 *      - v3
 *      - queue
 *    description:
 *      Va sul portale di Wind ed effettua il controllo sulla morosità.
 *      Il payload è lo stesso della rotta diretta `/scraper/v3/wind/fattibilita/morosita`,
 *      in più va aggiunto il webhook che lo scraper poi andrà a richiamare per notificare l'esito.
 *    consume:
 *      - application/json
 *    produces:
 *      - application/json
 *    responses:
 *      200:
 *        description: SuccessResponse
 */
