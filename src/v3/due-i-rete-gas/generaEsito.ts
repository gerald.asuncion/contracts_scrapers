// import CasoNonGestitoResponse from "../../response/CasoNonGestitoResponse";
import FailureResponse from "../../response/FailureResponse";
import SuccessResponse from "../../response/SuccessResponse";
import { ScraperResponse } from "../../scraperv2/scraper";
import { DatiPerEsito } from "./types";

function checkValue(value: string, toCheck: string) {
  return value.toLocaleLowerCase() == toCheck.toLocaleLowerCase();
}

function isMotivazioneValid(motivazione: string) {
  return !motivazione || checkValue(motivazione, 'nessuna motivazione indicata') || checkValue(motivazione, 'Nessuna motivazione di sospensione/cessazione trovata');
}

function getSuccessData(datiPerEsito: DatiPerEsito) {
  return {
    indirizzo: datiPerEsito.datiIndirizzo,
    civico: datiPerEsito.datiCivico,
    cap: datiPerEsito.daticap,
    comune: datiPerEsito.datiComune,
    provincia: datiPerEsito.datiProvincia,
    modificato: datiPerEsito.datiModificato,
    potMaxRichiesta: datiPerEsito.datiPotMaxRichiesta,
    societaVendita: datiPerEsito.datiSocieta,
    statoFornitura: datiPerEsito.datiStato,
    inAttesaAttivazione: datiPerEsito.datiAttivazione,
    codiceRemiPool: datiPerEsito.codiceRemiPool,
    ragioneSocialeDistributore: datiPerEsito.ragioneSocialeDistributore,
    concessione: datiPerEsito.concessione,
    descrizioneConcessione: datiPerEsito.descrizioneConcessione,
    codiceComuneIstat: datiPerEsito.codiceComuneIstat,
    intestatario: datiPerEsito.intestatario,
    richiedente: datiPerEsito.richiedente,
    accessibilita: datiPerEsito.accessibilita,
    ubicazione: datiPerEsito.ubicazione,
    misuratore: datiPerEsito.misuratore,
    correttore: datiPerEsito.correttore,
    classeMisuratore: datiPerEsito.classeMisuratore,
    matricolaContatore: datiPerEsito.matricolaContatore,
    tipologiaPdr: datiPerEsito.tipologiaPdr,
    statoContatore: datiPerEsito.statoContatore,
    matricolaCorrettoriVolumi: datiPerEsito.matricolaCorrettoriVolumi,
    elettrovalvolaChiusa: datiPerEsito.elettrovalvolaChiusa,
    compLettCiclo: datiPerEsito.compLettCiclo,
    potenzaMassimaErogabile: datiPerEsito.potenzaMassimaErogabile,
    categoriaUso: datiPerEsito.categoriaUso,
    classePrelievo: datiPerEsito.classePrelievo,
    codiceProfiloPrelievo: datiPerEsito.codiceProfiloPrelievo,
    presenzaTelelettura: datiPerEsito.presenzaTelelettura,
    dataMessaInServizio: datiPerEsito.dataMessaInServizio,
    titolarita: datiPerEsito.titolarita,
    causaleIn: datiPerEsito.causaleIn,
    causaleOut: datiPerEsito.causaleOut,
    pdrContrattualizzato: datiPerEsito.pdrContrattualizzato,
    servizioEnergetico: datiPerEsito.servizioEnergetico,
    revoca: datiPerEsito.revoca,
    nomeClienteFinale: datiPerEsito.nomeClienteFinale,
    cognomeClienteFinale: datiPerEsito.cognomeClienteFinale,
    ragioneSocialeClienteFinale: datiPerEsito.ragioneSocialeClienteFinale,
    codiceFiscaleClienteFinale: datiPerEsito.codiceFiscaleClienteFinale,
    pivaClienteFinale: datiPerEsito.pivaClienteFinale,
    telefonoClienteFinale: datiPerEsito.telefonoClienteFinale,
    emailClienteFinale: datiPerEsito.emailClienteFinale,
    nomeBeneficiario: datiPerEsito.nomeBeneficiario,
    cognomeBeneficiario: datiPerEsito.cognomeBeneficiario,
    ragioneSocialeBeneficiario: datiPerEsito.ragioneSocialeBeneficiario,
    codiceFiscaleBeneficiario: datiPerEsito.codiceFiscaleBeneficiario,
    pivaBeneficiario: datiPerEsito.pivaBeneficiario,
    telefonoBeneficiario: datiPerEsito.telefonoBeneficiario,
    emailBeneficiario: datiPerEsito.emailBeneficiario,
    inizioAgevolazione: datiPerEsito.inizioAgevolazione,
    fineAgevolazione: datiPerEsito.fineAgevolazione,
  };
}

export default function generaEsito(dati: DatiPerEsito): ScraperResponse {
  const {
    datiStato,
    datiSocieta,
    datiAttivazione,
    datiMotivazione,
  } = dati;
  if (checkValue(datiStato, 'cessato') && !datiSocieta && checkValue(datiAttivazione, 'no') && isMotivazioneValid(datiMotivazione)) {
    return new SuccessResponse('ok-subentro', getSuccessData(dati));
  }

  if ((checkValue(datiStato, 'in attesa prima attivazione') || checkValue(datiStato, 'attesa prima attivazione')) && !datiSocieta && checkValue(datiAttivazione, 'no') && isMotivazioneValid(datiMotivazione)) {
    return new SuccessResponse('ok-prima attivazione', getSuccessData(dati));
  }

  return new FailureResponse('ko-pdr non contendibile');
  // return new CasoNonGestitoResponse();
}
