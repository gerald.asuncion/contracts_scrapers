import isVisible from "../../browser/page/isVisible";
import waitForNavigation from "../../browser/page/waitForNavigation";
import waitForSelectorAndClickEvaluated from "../../browser/page/waitForSelectorAndClickEvaluated";
import waitForSelectorAndType from "../../browser/page/waitForSelectorAndType";
import waitForXPathAndClickEvaluated from "../../browser/page/waitForXPathAndClickEvaluated";
import ErrorResponse from "../../response/ErrorResponse";
import FailureResponse from "../../response/FailureResponse";
import DueIReteGas from "../../scraperv2/due-i-retegas/DueIReteGas";
import { ScraperResponse } from "../../scraperv2/scraper";
import { QueueTask } from "../../task/QueueTask";
import delay from "../../utils/delay";
import generaEsito from "./generaEsito";
import { DueIReteGasV3Payload } from "./types";

@QueueTask()
export default class DueIReteGasV3 extends DueIReteGas {
  async scrapeWebsite(payload: DueIReteGasV3Payload): Promise<ScraperResponse> {
    const logger = this.childLogger;
    const page = await this.p();

    await this.setCookies(page);

    await page.goto(DueIReteGas.HOMEPAGE_URL, { waitUntil: 'networkidle2' });
    await this.setCookies(page);

    if (await isVisible(page, 'input[name="avvisoLogin"] + input')) {
      await waitForSelectorAndClickEvaluated(page, '#avvisoLoginModalPanelContainer input[name="avvisoLogin"] + input[type="submit"]');
      await this.setCookies(page);

      await delay(5000);
    }

    await Promise.all([
      waitForXPathAndClickEvaluated(page, '//span[text() = "PdR"]/parent::div'),
      waitForNavigation(page)
    ]);
    await this.setCookies(page);

    await waitForSelectorAndType(page, '#formRicercaPdr\\:numeroPdr', payload.pdr);

    await Promise.all([
      waitForSelectorAndClickEvaluated(page, '#formRicercaPdr\\:btn_ricerca'),
      waitForNavigation(page)
    ]);
    await this.setCookies(page);

    const infoMessage = await this.checkForInfoMessages(page);
    if (infoMessage) {
      if (infoMessage === 'Non è stato trovato alcun pdr rispondente ai parametri selezionati') {
        return new ErrorResponse(infoMessage);
      }

      return new FailureResponse(infoMessage);
    }

    const datiPerEsito = await this.recuperaDatiPerEsito(page);

    const esito = generaEsito(datiPerEsito);
    logger.info(`payload: ${JSON.stringify(payload)} | dati per esito: ${JSON.stringify(datiPerEsito)} | esito: ${JSON.stringify(esito)}`);

    return esito;
  }
}
