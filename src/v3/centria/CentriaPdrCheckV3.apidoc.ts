/**
 * @openapi
 *
 * /scraper/v3/centria/check/pdr:
 *  post:
 *    tags:
 *      - v3
 *    description: Controlla se per Centria il pdr passato è valido. Viene utilizzato da Arera.
 *    consume:
 *      - application/json
 *    produces:
 *      - application/json
 *    responses:
 *      200:
 *        description: SuccessResponse
 *    parameters:
 *      - in: body
 *        description:
 *          bisogna passare il pdr da controllare
 *        schema:
 *          type: object
 *          required:
 *            - pdr
 *          properties:
 *            pdr:
 *              type: string
 */
