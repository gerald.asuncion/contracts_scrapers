import { QueueTask } from '../../task/QueueTask';
import CentriaPdrCheck from '../../scraperv2/centria/CentriaPdrCheck';
import { ScraperResponse } from '../../scraperv2/scraper';
import CentriaPayload from '../../scraperv2/centria/payload/CentriaPayload';
import { recuperaDatiPerEsito } from './utils/recuperaDatiPerEsito';
import creaEsito from './utils/creaEsito';

@QueueTask()
export default class CentriaPdrCheckV3 extends CentriaPdrCheck {
  /**
   * @override
   */
  async scrapeWebsite(payload: CentriaPayload): Promise<ScraperResponse> {
    const logger = this.childLogger;
    const page = await this.p();

    await this.ricercaPdr(page, logger, payload);

    const datiPerEsito = await recuperaDatiPerEsito(page);

    const esito = creaEsito(datiPerEsito);
    logger.info(`per il payload ${JSON.stringify(payload)} recuperate info ${JSON.stringify(datiPerEsito)} e generato esito ${JSON.stringify(esito)}`);

    return esito;
  }
}
