import { Page } from "puppeteer";
import getElementText from "../../../browser/page/getElementText";

export type DatiPerEsito = {
  esito: 'ok' | 'ko',
  [key: string]: string | null
}

export async function recuperaDatiPerEsito(page: Page): Promise<DatiPerEsito> {
  const elResponse = await page.waitForSelector('#twsTemplate_Content1_twsModule_lblMsg');
  const elResponseText: string = await page.evaluate((el: HTMLSpanElement) => el.innerText || '', elResponse);
  const elResponseClassName: string = await page.evaluate((el) => el.className, elResponse);

  if (elResponseText.trim().toLowerCase().includes('pdr trovati: 0')) {
    return { esito: 'ko' };
  }

  if (elResponseClassName.toLowerCase().includes('error')) {
    return { esito: 'ko', error: elResponseText };
  }

  const selectorPre = '#twsTemplate_Content1_twsModule_TestataPDP > tbody > tr > td > div > table > tbody > tr.trRptItem >';

  const elNoteMisuratoreText: string = await getElementText(page, `${selectorPre} td:nth-child(8)`, 'innerText');

  const comune = await getElementText(page, `${selectorPre} td:nth-child(2)`, 'innerText');
  const indirizzo = await getElementText(page, `${selectorPre} td:nth-child(3)`, 'innerText');
  const potenzaMassima = await getElementText(page, `${selectorPre} td:nth-child(4)`, 'innerText');
  const matricolaMisuratore = await getElementText(page, `${selectorPre} td:nth-child(5)`, 'innerText');
  const classeMisuratore = await getElementText(page, `${selectorPre} td:nth-child(6)`, 'innerText');
  const stato = await getElementText(page, `${selectorPre} td:nth-child(7)`, 'innerText');

  return {
    esito: 'ok',
    noteMisuratore: elNoteMisuratoreText.toLowerCase(),
    comune,
    indirizzo,
    potenzaMassima,
    matricolaMisuratore,
    classeMisuratore,
    stato,
  };
}
