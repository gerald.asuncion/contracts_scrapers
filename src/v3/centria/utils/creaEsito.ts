import CasoNonGestitoResponse from "../../../response/CasoNonGestitoResponse";
import FailureResponse from "../../../response/FailureResponse";
import SuccessResponse from "../../../response/SuccessResponse";
import { DatiPerEsito } from "./recuperaDatiPerEsito";

export default function creaEsito({ esito, ...rest }: DatiPerEsito) {
  if (esito === 'ko') {
    return new FailureResponse('ko-pdr non contendibile');
  }

  if (rest.noteMisuratore?.includes('chiuso') && (rest.noteMisuratore.includes('rimosso') || rest.noteMisuratore.includes('posato'))) {
    return new SuccessResponse('Ok', rest);
  }

  return new CasoNonGestitoResponse();
}
