/**
 * @openapi
 *
 * /scraper/v3/italgas/toscana/check/gas:
 *  post:
 *    tags:
 *      - v3
 *    description: Va sul portale di Italgas e controlla se il pdr è valido.
 *    consume:
 *      - application/json
 *    produces:
 *      - application/json
 *    responses:
 *      200:
 *        description: SuccessResponse
 *    parameters:
 *      - in: body
 *        description:
 *          pdr da controllare.
 *        schema:
 *          type: object
 *          required:
 *            - pdr
 *          properties:
 *            pdr:
 *              type: string
 *
 * /scraper/queue/italgasToscanaV3:
 *  post:
 *    tags:
 *      - v3
 *      - queue
 *    description:
 *      Inserisce in queue il task per effettuare il check tramite lo scraper ItalgasToscanaV3.
 *      Il payload è lo stesso della rotta diretta `/scraper/v3/italgas/toscana/check/gas`,
 *      in più va aggiunto il webhook che lo scraper poi andrà a richiamare per notificare l'esito.
 *    consume:
 *      - application/json
 *    produces:
 *      - application/json
 *    responses:
 *      200:
 *        description: SuccessResponse
 */
