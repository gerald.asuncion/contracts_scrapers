import ScraperOptions from "../../scraperv2/ScraperOptions";
import ItalgasAbstractV3 from "./ItalgasAbstractV3";

export default class ItalgasV3 extends ItalgasAbstractV3 {
  constructor(options: ScraperOptions) {
    super(options);
    this.dominio = '00489490011';
  }
}
