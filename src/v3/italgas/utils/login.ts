import { Page } from "puppeteer";
import waitCheckCredentials from "../../../browser/page/waitCheckCredentials";
import waitForNavigation from "../../../browser/page/waitForNavigation";
import waitForSelectorAndClick from "../../../browser/page/waitForSelectorAndClick";
import waitForSelectorAndType from "../../../browser/page/waitForSelectorAndType";
import { Credenziali } from "../../../credenziali/getCredenzialiDalServizio";
import { Logger } from "../../../logger";

export type ItalgasV3LoginContext = {
  page: Page;
  logger: Logger;
  credenziali: Credenziali;
  loginUrl: string;
};

export default async function italgasLogin({
  page,
  logger,
  credenziali,
  loginUrl,
}: ItalgasV3LoginContext) {
  const {username, password } = credenziali;
  logger.info('login > inizio');
    await page.goto(loginUrl, { waitUntil: 'networkidle2' });

    logger.info('login > attendo caricamento pagina tipo utenza');
    await page.waitForSelector('#centerPanel', { visible: true });

    logger.info('login > clicco sul pulsante accedi');
    await Promise.all([
      // eslint-disable-next-line max-len
      waitForSelectorAndClick(page, '#centerPanel > div > div.slds-col--padded.contentRegion.comm-layout-column > div > div:nth-child(6) > div > ul > li > button'),
      page.waitForNavigation({ waitUntil: 'networkidle2' })
    ]);

    // controllo se l'utente è già loggato
    logger.info("login > controllo se l'utente è già loggato");
    try {
      await page.waitForSelector('#workArea', { timeout: 3000 });
    } catch (ex) {
      logger.info('login > utente già loggato procedo allo step di selezione servizio');
      return;
    }

    logger.info('login > attendo caricamento pagina col form');
    await page.waitForSelector('#content', { visible: true });

    await waitForSelectorAndType(page, '#userNameInput', username);
    logger.info('login > inserito username');

    await waitForSelectorAndType(page, '#passwordInput', password);
    logger.info('login > inserita password');

    await Promise.all([
      waitForSelectorAndClick(page, '#submitButton'),
      Promise.race([
        waitForNavigation(page),
        waitCheckCredentials(page, '#error', loginUrl, username, logger)
      ])
    ]);
}
