import { Page } from "puppeteer";
import getRowFieldText from "../../../scraperv2/italgas/utils/getRowFieldText";

export type ItalgasSuccessInfo = {
  siglaToponomastica: string;
  indirizzo: string;
  civico: string;
  comune: string;
  provincia: string;
  cap: string;
  scala: string;
  interno: string;
  portata: string;
  statoDelibera: string;
  matricolaContatore: string;
  tipoPdr: string;
  classeContatore: string;
  piano: string;
  accessibilita: string;
  codiceImpianto: string;
  nominativoClienteFinale: string;
  telefonoCliente: string;
  statoElettrovalvola: string;
  codiceIstat: string;
  pressione: string;
  statoPdr: string;
  tipoFornitura: string;
  dataImpegno: string;
  statoCommerciale: string;
  portataMinima: string;
  portataMassima: string;
  marcaContatore: string;
  numeroCifreContatore: string;
  tipologiaTecnica: string;
  annoFabbricazione: string;
  proprietaMisuratore: string;
  integrato: string;
  flagTeleletto: string;
};

export default async function recuperaSuccessInfo(page: Page): Promise<ItalgasSuccessInfo> {
  const siglaToponomastica = await getRowFieldText(page, 'Sigla Toponomastica');
  const indirizzo = await getRowFieldText(page, 'Indirizzo fornitura');
  const civico = await getRowFieldText(page, 'Civico');
  const comune = await getRowFieldText(page, 'Comune');
  const provincia = await getRowFieldText(page, 'Provincia');
  const cap = await getRowFieldText(page, 'CAP');
  const scala = await getRowFieldText(page, 'Scala');
  const interno = await getRowFieldText(page, 'Interno');
  const portata = await getRowFieldText(page, 'Potenzialità PDR');
  const statoDelibera = await getRowFieldText(page, 'Stato Att. delibera 40/14');
  const matricolaContatore = await getRowFieldText(page, 'Matricola contatore');

  const tipoPdr = await getRowFieldText(page, 'Tipo PDR');
  const classeContatore = await getRowFieldText(page, 'Classe contatore');
  const piano = await getRowFieldText(page, 'Piano');
  const accessibilita = await getRowFieldText(page, 'Accessibilità  229/01');
  const codiceImpianto = await getRowFieldText(page, 'Codice Impianto');
  const nominativoClienteFinale = await getRowFieldText(page, 'Nominativo Cliente Finale');
  const telefonoCliente = await getRowFieldText(page, 'Telefono Cliente');
  const statoElettrovalvola = await getRowFieldText(page, 'Stato Elettrovalvola');
  const codiceIstat = await getRowFieldText(page, 'Codice Istat');
  const pressione = await getRowFieldText(page, 'Pressione');
  const statoPdr = await getRowFieldText(page, 'Stato PDR');
  const tipoFornitura = await getRowFieldText(page, 'Tipo Fornitura');

  /**
   * ATTENZIONE
   *
   * nel caso aprono bug che l data impegno non viene passata,
   * tenete presente che quando il dato non viene mostrato nel portale
   * all'interno dell'elemento vicino alla label non c'è proprio nulla, è solo un div completamente vuoto
   */
  const dataImpegno = await getRowFieldText(page, 'Data impegno', false);

  const statoCommerciale = await getRowFieldText(page, 'Stato commerciale');
  const portataMinima = await getRowFieldText(page, 'Portata minima (mcs/h)');
  const portataMassima = await getRowFieldText(page, 'Portata massima (mcs/h)');
  const marcaContatore = await getRowFieldText(page, 'Marca contatore');
  const numeroCifreContatore = await getRowFieldText(page, 'Numero cifre contatore');
  const tipologiaTecnica = await getRowFieldText(page, 'Tipologia Tecnica');
  const annoFabbricazione = await getRowFieldText(page, 'Anno Fabbricazione');
  const proprietaMisuratore = await getRowFieldText(page, 'Proprietà Misuratore');
  const integrato = await getRowFieldText(page, 'Integrato');
  const flagTeleletto = await getRowFieldText(page, 'Flag Teleletto');

  return {
    siglaToponomastica,
    indirizzo,
    civico,
    comune,
    provincia,
    cap,
    scala,
    interno,
    portata,
    statoDelibera,
    matricolaContatore,
    tipoPdr,
    classeContatore,
    piano,
    accessibilita,
    codiceImpianto,
    nominativoClienteFinale,
    telefonoCliente,
    statoElettrovalvola,
    codiceIstat,
    pressione,
    statoPdr,
    tipoFornitura,
    dataImpegno,
    statoCommerciale,
    portataMinima,
    portataMassima,
    marcaContatore,
    numeroCifreContatore,
    tipologiaTecnica,
    annoFabbricazione,
    proprietaMisuratore,
    integrato,
    flagTeleletto,
  };
}
