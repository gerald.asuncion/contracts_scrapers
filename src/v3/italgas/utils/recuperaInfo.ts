import { Page } from "puppeteer";
import { recuperaErrore } from "../../../scraperv2/italgas/steps/stepVerificaEsito";
import getRowFieldText from "../../../scraperv2/italgas/utils/getRowFieldText";

export type ItalgasInfo = {
  erroreModale: string | null;
  societaDiVenditaAbbinata: string;
  statoContatore: string;
  statoTecnico: string;
};

export default async function recuperaInfo(page: Page): Promise<ItalgasInfo> {
  const errore = await recuperaErrore(page);

  let societaDiVenditaAbbinata = '';
  let statoContatore = '';
  let statoTecnico = '';

  try {
    societaDiVenditaAbbinata = (await getRowFieldText(page, 'Società di vendita abbinata')).toLowerCase();
  } catch (ex) {
    // info non mostrata dal portale
  }

  try {
    statoContatore = (await getRowFieldText(page, 'Stato contatore')).toLowerCase();
  } catch (ex) {
    // info non mostrata dal portale
  }

  try {
    statoTecnico = (await getRowFieldText(page, 'Stato Tecnico')).toLowerCase();
  } catch (ex) {
    // info non mostrata dal portale
  }

  return {
    erroreModale: errore ? errore.toLowerCase() : null,
    societaDiVenditaAbbinata,
    statoContatore,
    statoTecnico,
  };
}
