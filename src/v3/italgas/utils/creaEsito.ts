import { Page } from "puppeteer";
import ErrorResponse from "../../../response/ErrorResponse";
import FailureResponse from "../../../response/FailureResponse";
import SuccessResponse from "../../../response/SuccessResponse";
import { ItalgasInfo } from "./recuperaInfo";
import recuperaSuccessInfo from "./recuperaSuccessInfo";

export default async function creaEsito(page: Page, {
  erroreModale,
  societaDiVenditaAbbinata,
  statoContatore,
  statoTecnico,
}: ItalgasInfo) {
  if (erroreModale) {
    if (erroreModale.includes("pdr associato ad altra societa' di vendita")) {
      return new FailureResponse("KO - PDR non contendibile");
    }

    if (erroreModale.includes('pdr inesistente')) {
      return new FailureResponse('KO - PDR inesistente');
    }

    if (erroreModale.includes('pdr gestito ad altro distributore') || erroreModale.includes('pdr appartenente ad altro distributore')) {
      return new ErrorResponse('pdr gestito da altro distributore');
    }
  }

  if (!societaDiVenditaAbbinata.includes('nessuna società di vendita associata')) {
    return new FailureResponse('KO - PDR non contendibile');
  }

  if (statoContatore.includes("chiuso per morosita'")) {
    return new FailureResponse('KO - PDR non contendibile');
  }

  if (!societaDiVenditaAbbinata || societaDiVenditaAbbinata.includes('nessuna società di vendita associata')) {
    if (statoTecnico === 'in gas' || statoTecnico === 'chiuso') {
      if (statoContatore === 'chiuso') {
        return new SuccessResponse('ok-Subentro', { ...(await recuperaSuccessInfo(page)), statoTecnico, statoContatore });
      }

      if (!statoContatore) {
        return new SuccessResponse('ok-prima attivazione', { ...(await recuperaSuccessInfo(page)), statoTecnico, statoContatore });
      }
    }
  }

  if (societaDiVenditaAbbinata.includes('nessuna società di vendita associata')) {
    if (!statoContatore && statoTecnico === 'non in gas') {
      return new FailureResponse('KO - PDR rimosso/interrotto per morosità');
    }

    if (statoContatore === 'pdr non in gas, contatore aperto' && statoTecnico === 'non in gas') {
      return new FailureResponse('KO - PDR rimosso/interrotto per morosità');
    }
  }
}
