/**
 * @openapi
 *
 * /scraper/v3/italgas/check/gas:
 *  post:
 *    tags:
 *      - v3
 *    description: Va sul portale di Italgas e controlla se il pdr è valido.
 *    consume:
 *      - application/json
 *    produces:
 *      - application/json
 *    responses:
 *      200:
 *        description: SuccessResponse
 *    parameters:
 *      - in: body
 *        description:
 *          pdr da controllare.
 *        schema:
 *          type: object
 *          required:
 *            - pdr
 *          properties:
 *            pdr:
 *              type: string
 */
