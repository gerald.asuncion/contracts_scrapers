import ScraperOptions from "../../scraperv2/ScraperOptions";
import { QueueTask } from "../../task/QueueTask";
import ItalgasAbstractV3 from "./ItalgasAbstractV3";

@QueueTask()
export default class ItalgasContendibilitaGasV3 extends ItalgasAbstractV3 {
  constructor(options: ScraperOptions) {
    super(options);
    this.dominio = '00489490011';
  }
}
