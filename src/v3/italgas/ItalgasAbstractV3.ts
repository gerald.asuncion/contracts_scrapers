import CasoNonGestitoResponse from "../../response/CasoNonGestitoResponse";
import ErrorResponse from "../../response/ErrorResponse";
import FailureResponse from "../../response/FailureResponse";
import stepInserimentoPdr from "../../scraperv2/italgas/steps/stepInserimentoPdr";
import stepSelezioneServizio from "../../scraperv2/italgas/steps/stepSelezioneServizio";
import { ScraperResponse, WebScraper } from "../../scraperv2/scraper";
import ScraperOptions, { SCRAPER_TIPOLOGIA } from "../../scraperv2/ScraperOptions";
import extractErrorMessage from "../../utils/extractErrorMessage";
import { ItalgasV3Payload } from "./types";
import creaEsito from "./utils/creaEsito";
import italgasLogin from "./utils/login";
import recuperaInfo from "./utils/recuperaInfo";

export default class ItalgasAbstractV3 extends WebScraper<ItalgasV3Payload> {
  static readonly LOGIN_URL: string = 'https://www.gas2be.it/s/login/';

  protected dominio: string;

  constructor(options: ScraperOptions) {
    super(options);
    this.dominio = '00489490011';
  }

  /**
   * @override
   */
  getLoginTimeout(): number { return 60000; }

  async login(): Promise<void> {
    const credenziali = await this.getCredenziali();

    const page = await this.p();

    const logger = this.childLogger;

    return italgasLogin({ page, logger, credenziali, loginUrl: ItalgasAbstractV3.LOGIN_URL });
  }

  async scrapeWebsite(payload: ItalgasV3Payload): Promise<ScraperResponse> {
    const logger = this.childLogger;

    logger.info('login > terminata');

    try {
      const page = await this.p();

      await stepSelezioneServizio(page, logger);

      // await stepSelezioneRete(page, this.dominio, logger);

      await stepInserimentoPdr(page, payload.pdr, logger);

      logger.info("controllo l'esito");
      const info = await recuperaInfo(page);
      const esito = await creaEsito(page, info);

      if (esito) {
        const { details } = esito;
        if (esito instanceof FailureResponse || esito instanceof ErrorResponse) {
          logger.error(details);
        } else {
          logger.info(`per il payload ${JSON.stringify(payload)} recuperate info ${JSON.stringify(info)} e generato esito positivo ${details}`);
        }

        return esito;
      }

      logger.error(`caso non gestito per il payload ${JSON.stringify(payload)} recuperate info ${JSON.stringify(info)}`);
      return new CasoNonGestitoResponse();
    } catch (ex) {
      const errMsg = extractErrorMessage(ex);
      logger.error(errMsg);
      return new ErrorResponse(errMsg);
    }
  }

  /**
   * @override
   */
  getScraperCodice() {
    return 'italgas';
  }

  /**
   * @override
   */
  getScraperTipologia() {
    return SCRAPER_TIPOLOGIA.FATTIBILITA;
  }
}
