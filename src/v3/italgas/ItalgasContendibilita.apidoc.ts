/**
 * @openapi
 *
 * /scraper/v3/italgas/contendibilita/gas:
 *  post:
 *    tags:
 *      - v3
 *    description: Va sul portale di Italgas e controlla se il pdr è valido.
 *    consume:
 *      - application/json
 *    produces:
 *      - application/json
 *    responses:
 *      200:
 *        description: SuccessResponse
 *    parameters:
 *      - in: body
 *        description:
 *          pdr da controllare.
 *        schema:
 *          type: object
 *          required:
 *            - pdr
 *          properties:
 *            pdr:
 *              type: string
 *
 * /scraper/queue/italgasContendibilitaV3:
 *  post:
 *    tags:
 *      - v3
 *      - queue
 *    description:
 *      Inserisce in queue il task per effettuare il check tramite lo scraper ItalgasContendibilitaV3.
 *      Il payload è lo stesso della rotta diretta `/scraper/v3/italgas/contendibilita/gas`,
 *      in più va aggiunto il webhook che lo scraper poi andrà a richiamare per notificare l'esito.
 *    consume:
 *      - application/json
 *    produces:
 *      - application/json
 *    responses:
 *      200:
 *        description: SuccessResponse
 */
