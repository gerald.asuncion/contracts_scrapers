import { Page } from "puppeteer";
import getAttribute from "../../../browser/page/getAttribute";
import waitForNavigation from "../../../browser/page/waitForNavigation";
import waitForSelectorAndClick from "../../../browser/page/waitForSelectorAndClick";
import waitForSelectorAndType from "../../../browser/page/waitForSelectorAndType";
import waitForVisible from "../../../browser/page/waitForVisible";
import { ErogasmetPayload } from "../../../scraperv2/terranova/erogasmet";

export default async function inserisciPdr(page: Page, { pdr }: ErogasmetPayload) {
  await waitForVisible(page, '#form1');

  const url = await getAttribute<string>(page, '#twsTemplate_Mainmenu2_aspMainMenun79 > td > table > tbody > tr > td > a', 'href');

  await page.goto(url, { waitUntil: 'networkidle2' });

  // eslint-disable-next-line max-len
  await waitForVisible(page, '#form1 > div:nth-child(22) > table > tbody > tr:nth-child(4) > td > table > tbody > tr > td:nth-child(2) > table:nth-child(4)');

  await waitForSelectorAndType(page, '#twsTemplate_Content2_twsModule_txtPdr', pdr);

  await Promise.all([
    waitForNavigation(page),
    waitForSelectorAndClick(page, '#twsTemplate_Content2_twsModule_btnSearchPDP')
  ]);
}
