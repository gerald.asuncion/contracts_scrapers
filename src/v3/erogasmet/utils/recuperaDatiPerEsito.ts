import { Page } from "puppeteer";
import getElementText from "../../../browser/page/getElementText";

export type ErogasmetDatiPerEsito = {
  esito: string;
  noteMisuratore?: string;
  comune?: string;
  indirizzo?: string;
  potenzaMassima?: string;
  matricolaMisuratore?: string;
  classeMisuratore?: string;
  stato?: string;
};

async function getText(page: Page, selector: string): Promise<string> {
  return (await page.$eval(selector, (el) => el.textContent?.trim())) || '';
}

export default async function recuperaDatiPerEsito(page: Page): Promise<ErogasmetDatiPerEsito> {
  // const esito = await page.$eval<string>(
  //   // eslint-disable-next-line max-len
  //   '#form1 > div:nth-child(22) > table > tbody > tr:nth-child(4) > td > table > tbody > tr > td:nth-child(2) > table:nth-child(4) > tbody > tr:nth-child(2) > td > table:nth-child(2) > tbody > tr:nth-child(1) > td > div',
  //   (el) => el.textContent?.trim() as string
  // );
  const esito = await getElementText(
    page,
    '#form1 > div:nth-child(22) > table > tbody > tr:nth-child(4) > td > table > tbody > tr > td:nth-child(2) > table:nth-child(4) > tbody > tr:nth-child(2) > td > table:nth-child(2) > tbody > tr:nth-child(1) > td > div',
    'textContent'
  ) as string;

  if (esito.includes('0')) {
    return { esito };
  }

  const noteMisuratore = (await getText(
    page,
    '#twsTemplate_Content2_twsModule_TestataPDP > tbody > tr > td > div > table > tbody > tr.trRptItem > td:nth-child(8)'
  )).toLowerCase();

  const comune = await getText(
    page,
    '#twsTemplate_Content2_twsModule_TestataPDP > tbody > tr > td > div > table > tbody > tr.trRptItem > td:nth-child(2)'
  );

  const indirizzo = await getText(
    page,
    '#twsTemplate_Content2_twsModule_TestataPDP > tbody > tr > td > div > table > tbody > tr.trRptItem > td:nth-child(3)'
  );

  const potenzaMassima = await getText(
    page,
    '#twsTemplate_Content2_twsModule_TestataPDP > tbody > tr > td > div > table > tbody > tr.trRptItem > td:nth-child(4)'
  );

  const matricolaMisuratore = await getText(
    page,
    '#twsTemplate_Content2_twsModule_TestataPDP > tbody > tr > td > div > table > tbody > tr.trRptItem > td:nth-child(5)'
  );

  const classeMisuratore = await getText(
    page,
    '#twsTemplate_Content2_twsModule_TestataPDP > tbody > tr > td > div > table > tbody > tr.trRptItem > td:nth-child(6)'
  );

  const stato = await getText(
    page,
    '#twsTemplate_Content2_twsModule_TestataPDP > tbody > tr > td > div > table > tbody > tr.trRptItem > td:nth-child(7)'
  );

  // Matricola misuratore, Classe Misuratore, Stato

  return {
    esito,
    noteMisuratore,
    comune,
    indirizzo,
    potenzaMassima,
    matricolaMisuratore,
    classeMisuratore,
    stato,
  };
}
