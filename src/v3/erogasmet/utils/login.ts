import { Page } from "puppeteer";
import waitCheckCredentials from "../../../browser/page/waitCheckCredentials";
import waitForNavigation from "../../../browser/page/waitForNavigation";
import waitForSelectorAndClick from "../../../browser/page/waitForSelectorAndClick";
import waitForSelectorAndType from "../../../browser/page/waitForSelectorAndType";
import waitForVisible from "../../../browser/page/waitForVisible";
import { Credenziali } from "../../../credenziali/getCredenzialiDalServizio";
import { Logger } from "../../../logger";
import tryOrThrow from "../../../utils/tryOrThrow";

export default async function login(page: Page, logger: Logger, url: string, { username, password }: Credenziali) {
  await page.goto(url, { waitUntil: 'networkidle2' });

  // eslint-disable-next-line max-len
  const areaPrivataLinkEl = await page.waitForSelector('#Form1 > div:nth-child(3) > table > tbody > tr:nth-child(5) > td > table > tbody > tr > td:nth-child(3) > a');

  if (areaPrivataLinkEl) {
    await Promise.all([
      areaPrivataLinkEl.click(),
      waitForNavigation(page)
    ]);
  }

  // eslint-disable-next-line max-len
  await waitForVisible(page, '#form1 > div:nth-child(12) > table > tbody > tr:nth-child(3) > td:nth-child(2) > table > tbody > tr > td > table > tbody > tr:nth-child(2) > td > center > table');

  await tryOrThrow(
    () => waitForSelectorAndType(page, '#twsTemplate_Content2_twsModule_txtUser', username),
    'non sono riuscito ad inserire l\'username'
  );
  await tryOrThrow(
    () => waitForSelectorAndType(page, '#twsTemplate_Content2_twsModule_txtPSW', password),
    'non sono riuscito ad inserire la password'
  );

  await Promise.all([
    waitForSelectorAndClick(page, '#twsTemplate_Content2_twsModule_btnLogin'),
    waitForNavigation(page)
  ]);

  await waitCheckCredentials(page, '#twsTemplate_Content2_twsModule_lblMsg', url, username, logger, { timeout: 3000 });
}
