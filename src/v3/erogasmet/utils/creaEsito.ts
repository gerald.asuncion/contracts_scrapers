import CasoNonGestitoResponse from "../../../response/CasoNonGestitoResponse";
import FailureResponse from "../../../response/FailureResponse";
import SuccessResponse from "../../../response/SuccessResponse";
import { ErogasmetDatiPerEsito } from "./recuperaDatiPerEsito";

export default function creaEsito({ esito, ...rest }: ErogasmetDatiPerEsito) {
  if (esito.includes('0')) {
    return new FailureResponse('ko-pdr non contendibile');
  }

  const noteMisuratore = rest.noteMisuratore as string;

  if (noteMisuratore.includes('posato') || noteMisuratore.includes('chiuso')) {
    return new SuccessResponse('Ok', rest);
  }

  return new CasoNonGestitoResponse();
}
