/**
 * @openapi
 *
 * /scraper/v3/erogasmet/check/pdr:
 *  post:
 *    tags:
 *      - v3
 *    description: Effettua il controllo del pdr sul portale di Erogasmet.
 *      Questa è la rotta diretta allo scraper, quindi non usa il meccanismo del webhook.
 *    consume:
 *      - application/json
 *    produces:
 *      - application/json
 *    responses:
 *      200:
 *        description: SuccessResponse
 *    parameters:
 *      - in: body
 *        schema:
 *          type: object
 *          required:
 *            - pdr
 *          properties:
 *            pdr:
 *              type: string
 *
 * /scraper/queue/erogasmetV3:
 *  post:
 *    tags:
 *      - v3
 *      - queue
 *    description:
 *      Effettua il controllo del pdr sul portale di Erogasmet.
 *      Il payload è lo stesso della rotta diretta `/scraper/v3/erogasmet/check/pdr`,
 *      in più va aggiunto il webhook che lo scraper poi andrà a richiamare per notificare l'esito.
 *    consume:
 *      - application/json
 *    produces:
 *      - application/json
 *    responses:
 *      200:
 *        description: SuccessResponse
 */
