import { QueueTask } from '../../task/QueueTask';
import { ScraperResponse, WebScraper } from '../../scraperv2/scraper';
import login from './utils/login';
import Erogasmet, { ErogasmetPayload } from '../../scraperv2/terranova/erogasmet';
import inserisciPdr from './utils/inserisciPdr';
import recuperaDatiPerEsito from './utils/recuperaDatiPerEsito';
import creaEsito from './utils/creaEsito';

@QueueTask()
export default class ErogasmetV3 extends WebScraper<ErogasmetPayload> {
  async login(): Promise<void> {
    const logger = this.childLogger;
    const credenziali = await this.getCredenziali();
    const page = await this.p();

    return login(page, logger, Erogasmet.LOGIN_URL, credenziali);
  }

  async scrapeWebsite(payload: ErogasmetPayload): Promise<ScraperResponse> {
    const logger = this.childLogger;
    logger.info(`effettuo il check per il pdr ${payload.pdr}`);
    const page = await this.p();

    await inserisciPdr(page, payload);

    const datiPerEsito = await recuperaDatiPerEsito(page);

    const esito = creaEsito(datiPerEsito);
    logger.info(`per il payload ${JSON.stringify(payload)} recuperate info ${JSON.stringify(datiPerEsito)} e generato esito ${JSON.stringify(esito)}`);

    return esito;
  }

  /**
   * @override
   */
  getScraperCodice() {
    return Erogasmet.prototype.getScraperCodice();
  }

  /**
   * @override
   */
  getScraperTipologia() {
    return Erogasmet.prototype.getScraperTipologia();
  }
}
