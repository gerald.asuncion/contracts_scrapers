import joi, { ObjectSchema } from 'joi';
import { IrenPreCheckValidatorSchema } from './IrenPreCheckValidator';

export default (): ObjectSchema => joi.object({
  ...IrenPreCheckValidatorSchema,
});
