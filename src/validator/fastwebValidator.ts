import joi, { ObjectSchema } from 'joi';
import { isRequired } from '../validatorUtils';

const regexDateValidator = /^(?:(?:\D*?\d{2}){3})\d{2}\D*?$/;

export const fastwebPayloadSchema = {
  codiceComsy: joi.string().allow(null, ''),

  idDatiContratto: isRequired(joi.alternatives().try(joi.string(), joi.number())),

  username: joi.string().allow(null, ''), //.required(),
  password: joi.string().allow(null, ''), //.required(),

  tipoOfferta: joi.string().valid('Fisso+mobile', 'Mobile', 'Upsell mobile su cb Fisso', 'Fisso', 'Upsell fisso su CB mobile')
    .insensitive().required(),
  offertaFisso: joi.string().valid('Fastweb Nexxt Casa', 'Fastweb Nexxt Casa Light', 'Fastweb Casa Light', 'Fastweb NeXXt Casa Plus', 'Fastweb Casa Light FWA', 'Fastweb Casa FWA').insensitive().allow(null, ''),
  offertaMobile: joi.string().valid('Fastweb NeXXt Mobile', 'Fastweb NeXXt Mobile Maxi', 'Fastweb Mobile Light')
    .insensitive().allow(null, ''),
  opzioneMobile: joi.string().valid('nessuna opzione').insensitive().allow(null, ''),
  offertaMobile2: joi.string().valid('nessuna offerta mobile 2').insensitive().allow(null, ''),
  sconto1: joi.string().allow(null, ''),
  sconto2: joi.string().allow(null, ''),
  partnership: joi.string().required(),
  opzioneFisso1: joi.string().valid('nessuna opzione', 'Opzione Now (primi 3 mesi gratis)', 'Servizio potenziamento Wi Fi', 'Servizio di estensione del segnale Wi Fi').insensitive().allow(null, ''),
  opzioneFisso2: joi.string().valid('nessuna opzione', 'Opzione Fastweb&Dazn (primi 3 mesi gratis)', 'Servizio potenziamento Wi Fi', 'Servizio di estensione del segnale Wi Fi', 'UpPlus').insensitive().allow(null, ''),
  opzioneFisso3: joi.string().valid('Nessuna Opzione', 'Assicurazione Assistenza Casa Quixa').allow(null, ''),
  opzioneFisso8: joi.string().valid('Nessuna Opzione', 'Sconto Fisso Mobile').allow(null, ''),

  accountCliente: joi.string().allow(null, ''),

  nome: joi.string().required(),
  cognome: joi.string().required(),
  sesso: joi.string().valid('M', 'F').insensitive().required(),
  dataNascita: joi.string().regex(regexDateValidator).required(),
  comuneNascitaEstero: joi.boolean().required(),
  comuneNascita: joi.string().required(),
  provinciaNascita: joi.string().required(),
  codiceFiscale: joi.string().required(),

  cellulare: joi.string().required(),
  email: joi.string().required(),
  tipoInvio: joi.string().valid('EMAIL', 'SMS').insensitive().required(),

  documentoTipo: joi.string().required(),
  documentoNumero: joi.string().required(),
  documentoEnteRilascio: joi.string().required(),
  documentoDataRilascio: joi.string().regex(regexDateValidator).required(),
  documentoRilasciatoDa: joi.string().allow(null, ''),
  documentoProvinciaRilascio: joi.string().allow(null, ''),

  residenzaComune: joi.string().required(),
  residenzaIndirizzo: joi.string().required(),
  residenzaCivico: joi.string().required(),
  residenzaCap: joi.string().required(),
  residenzaProvincia: joi.string().required(),

  attivazioneComune: joi.string().required(),
  attivazioneProvincia: joi.string().required(),
  attivazioneIndirizzo: joi.string().required(),
  attivazioneCivico: joi.string().required(),
  attivazioneCap: joi.string().required(),
  attivazioneScala: joi.string().allow(null, ''),
  attivazionePiano: joi.string().allow(null, ''),
  attivazioneInterno: joi.string().allow(null, ''),

  prefissoNumeroTelefono: joi.string().allow(null, ''),
  numeroTelefono: joi.string().allow(null, ''),
  codiceMigrazione: joi.string().allow(null, ''),
  attualeFornitoreInternet: joi.string().allow(null, ''),

  sceltaModem: joi.string().valid('FASTWEB', 'PROPRIETA').insensitive().required(),
  isProdottoPortabilita: joi.boolean().required(),
  iccid: joi.string().allow(null, ''),
  cellulareAttuale: joi.string().allow(null, ''),
  attualeFornitoreMobile: joi.string().allow(null, ''),
  tipoContratto: joi.string().valid('ricaricabile').insensitive().required(),

  recapitoComune: joi.string().required(),
  recapitoProvincia: joi.string().required(),
  recapitoIndirizzo: joi.string().required(),
  recapitoCivico: joi.string().required(),
  recapitoCap: joi.string().required(),
  recapitoPresso: joi.string().allow(null, ''),

  consenso1: joi.boolean().required(),
  consenso2: joi.boolean().required(),
  consenso3: joi.boolean().required(),

  tipoPagamento: joi.string().valid('Addebito su Conto Corrente').allow(null, ''),
  iban: joi.string().allow(null, ''),
  intestazioneContoCorrente: joi.string().allow(null, ''),

  webhook: joi.string().required(),

  clientePossiedeUnaNumerazioneDaPortare: joi.string().valid('si', 'no').required(),
  clientePossiedeUnaLineaDati: joi.string().valid('si', 'no').required(),
  codicePortabilitaDati: joi.string().allow(null, ''),
  operatoreProvenienzaDati: joi.string().allow(null, '')
};

export default (): ObjectSchema => joi.object(fastwebPayloadSchema);
