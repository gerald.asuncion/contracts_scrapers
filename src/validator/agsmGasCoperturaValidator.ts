import joi, { ObjectSchema } from 'joi';

export default (): ObjectSchema => joi.object({
  comune: joi.string().required(),
  provincia: joi.string().required(),
  tipologia: joi.string().required()
});
