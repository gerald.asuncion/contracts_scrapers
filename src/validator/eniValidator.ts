import joi, { ObjectSchema } from 'joi';

export default (): ObjectSchema => joi.object({
  idDatiContratto: joi.alternatives().try(joi.string(), joi.number()).required(),
  tipoFornitura: joi.string().valid('luce', 'gas').required(),

  // verifica vendibilità
  ragioneSociale: joi.string().required(),
  partitaIva: joi.string().required(),
  codiceFiscale: joi.string().required(),
  consumoAnnuo: joi.number().required(),
  modalitaPagamento: joi.string().valid('SDD', 'Bollettino', 'sdd', 'bollettino').required(),
  pod: joi.string().allow(null),
  pdr: joi.string().allow(null),
  tipoIndirizzoAttivazione: joi.string().required(),
  indirizzoAttivazione: joi.string().required(),
  civicoIndirizzoAttivazione: joi.string().required(),
  provinciaIndirizzoAttivazione: joi.string().required(),
  comuneIndirizzoAttivazione: joi.string().required(),
  capIndirizzoAttivazione: joi.string().required(),

  // dati del cliente
  prodotto: joi.string().valid('Eni Plenitude PLACET fissa gas altri usi (Base)', 'Eni Plenitude PLACET variabile gas altri usi (Base)', 'Trend Business gas (Base)', 'Eni Plenitude PLACET fissa luce altri usi (Base)', 'Eni Plenitude PLACET variabile luce altri usi (Base)', 'Trend Business Luce (Base)').required(),
  formaGiuridica: joi.string().required(),
  settoreMerceologico: joi.string().required(),
  attivitaMerceologica: joi.string().required(),
  codiceAteco: joi.string().required(),
  tipoInterlocutore: joi.string().valid(
    'Cliente', 'Legale rappresentante', 'Referente', 'cliente', 'legale rappresentante', 'referente'
  ).required(),
  nomeLegaleRappresentante: joi.string().required(),
  cognomeLegaleRappresentante: joi.string().required(),
  provinciaNascitaLegaleRappresentante: joi.string().required(),
  comuneNascitaLegaleRappresentante: joi.string().required(),
  dataDiNascitaLegaleRappresentante: joi.string().required(),
  sessoLegaleRappresentante: joi.string().valid('m', 'f', 'M', 'F').required(),
  tipoDocumentoLegaleRappresentante: joi.string().valid('carta d\'identità', 'patente', 'passaporto').required(),
  numeroDocumentoLegaleRappresentante: joi.string().required(),
  dataRilascioDocumentoLegaleRappresentante: joi.string().required(),
  comuneRilascioDocumentoLegaleRappresentante: joi.string().required(),
  cellulare: joi.string().required(),
  email: joi.string().required(),
  tipoIndirizzoSedeLegale: joi.string().required(),
  indirizzoSedeLegale: joi.string().required(),
  civicoSedeLegale: joi.string().required(),
  // scalaSedeLegale: joi.string().required(),
  provinciaSedeLegale: joi.string().required(),
  comuneSedeLegale: joi.string().required(),
  capSedeLegale: joi.string().required(),

  // punti fornitura
  occupazione: joi.string().valid('proprietario', 'inquilino', 'comodatario', 'usufruttuario', 'usufruttuario_altro').insensitive().required(),
  attualeFornitore: joi.string().required(),
  mercatoProvenienza: joi.string().valid('Libero', 'Tutelato', 'Salvaguardia', 'libero', 'tutelato', 'salvaguardia').allow(null, ''),
  tensione: joi.string().allow(null),
  agevolazione: joi.boolean().required(),
  tipoIndirizzoRecapitoFattura: joi.string().allow(null),
  indirizzoRecapitoFattura: joi.string().allow(null),
  civicoRecapitoFattura: joi.string().allow(null),
  scalaRecapitoFattura: joi.string().allow(null),
  provinciaRecapitoFattura: joi.string().allow(null),
  comuneRecapitoFattura: joi.string().allow(null),
  capRecapitoFattura: joi.string().allow(null),
  potenza: joi.string().allow(null),

  // dati pagamento
  iban: joi.string().required(),
  tipoIntestatarioSepa: joi.string().valid('persona fisica', 'persona giuridica').required(),
  nomeSepa: joi.string().allow(null),
  cognomeSepa: joi.string().allow(null),
  codiceFiscaleSepa: joi.string().allow(null),
  partitaIvaSepa: joi.string().allow(null),
  ragioneSocialeSepa: joi.string().allow(null),
  codiceFiscaleAziendaSepa: joi.string().allow(null),
  codiceFiscaleLegaleRappresentante: joi.string().required(),
  consensoUno: joi.boolean().required(),
  consensoDue: joi.boolean().required(),
  consensoTre: joi.boolean().required()
});
