import joi from 'joi';

export default () => joi.object({
  pod: joi.string().required(),
  tipoContratto: joi.string().required(),
});
