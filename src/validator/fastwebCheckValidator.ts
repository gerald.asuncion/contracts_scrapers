import joi, { ObjectSchema } from 'joi';

export default (): ObjectSchema => joi.object({
  codiceContrattoArray: joi.array().required()
});
