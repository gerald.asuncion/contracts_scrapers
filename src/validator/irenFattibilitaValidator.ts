import joi, { ObjectSchema } from 'joi';

export default (): ObjectSchema => joi.object({
  comune: joi.string().required(),
  cap: joi.string(),
  provincia: joi.string().required(),
  regione: joi.string().required(),
  webhook: joi.string().allow(null, ""),
});
