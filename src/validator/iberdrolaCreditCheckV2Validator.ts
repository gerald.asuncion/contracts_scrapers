import joi, { ObjectSchema } from 'joi';
import { IberdrloaCreditCheckV2Payload } from '../scraperv2/iberdrolaV2/payload/IberdrloaCreditCheckV2Payload';

export default (): ObjectSchema => joi.object<Record<keyof IberdrloaCreditCheckV2Payload, ObjectSchema>>({
  tipoFornitura: joi.string().required(),
  prodotto: joi.string().valid('luce', 'gas', 'dual').insensitive().required(),
  cap: joi.string().required(),
  // partitaIva: joi.string().required(),
  potenzaDisponibile: joi.when('prodotto', { is: joi.string().valid('luce', 'dual'), then: joi.string().required(), otherwise: joi.allow('', null) }),
  codiceFiscale: joi.string().required()
});
