import joi, { ObjectSchema } from 'joi';

export const areraValidatorSchema = {
  pdr: joi.string().required(),
  comune: joi.string().required(),
  provincia: joi.string().required(),
  regione: joi.string().required()
};

export default (): ObjectSchema => joi.object(areraValidatorSchema);
