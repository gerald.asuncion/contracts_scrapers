import joi, { ObjectSchema } from 'joi';
import { fastwebPayloadSchema } from './fastwebValidator';

const { intestazioneContoCorrente, ...fastwebPostFirmaPayloadSchemaBase } = fastwebPayloadSchema;

export default (): ObjectSchema => joi.object({
  ...fastwebPostFirmaPayloadSchemaBase,
  idEsterno: joi.number().required(),

  nascitaStato: joi.string().allow(null, ''),
  metodoPagamentoNome: joi.string().allow(null, ''),
  metodoPagamentoCognome: joi.string().allow(null, ''),
  metodoPagamentoSesso: joi.string().valid('M', 'F').allow(null, ''),
  metodoPagamentoNascitaData: joi.string().allow(null, ''),
  metodoPagamentoNascitaEstero: joi.boolean().allow(null, ''),
  metodoPagamentoNascitaStato: joi.string().allow(null, ''),
  metodoPagamentoNascitaComune: joi.string().allow(null, ''),
  metodoPagamentoNascitaProvincia: joi.string().allow(null, ''),
  metodoPagamentoCodiceFiscale: joi.string().allow(null, '')
});
