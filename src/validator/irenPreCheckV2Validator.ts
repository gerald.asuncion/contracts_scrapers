import joi, { ObjectSchema } from 'joi';
import { IrenPreCheckValidatorSchema } from './IrenPreCheckValidator';

export default (): ObjectSchema => joi.object({
  ...IrenPreCheckValidatorSchema,
  fornitore: joi.string().required(),
  tipoContratto: joi.string().required().valid('switch', 'subentro', 'attivazione', 'voltura')
});
