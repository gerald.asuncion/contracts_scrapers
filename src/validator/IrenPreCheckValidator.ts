import joi, { ObjectSchema } from 'joi';

export const IrenPreCheckValidatorSchema = {
  fornitura: joi.string().required(),
  codice: joi.string().required(),
  nome: joi.string().allow(null, ""),
  cognome: joi.string().allow(null, ""),
  codiceFiscale: joi.string().allow(null, ""),
  codiceFiscaleSocieta: joi.string().allow(null, ""),
  partitaIva: joi.string().allow(null, ""),
  ragioneSociale: joi.string().allow(null, ""),
  webhook: joi.string().allow(null, ""),
};

export default (): ObjectSchema => joi.object(IrenPreCheckValidatorSchema);
