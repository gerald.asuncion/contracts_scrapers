import joi, { ObjectSchema, AnySchema } from 'joi';
import { EdisonPayload } from '../scraperv2/edison/payload/EdisonPayload';

const validationRecapito = joi.when('recapitoUgualeFornitura', { is: true, then: joi.optional().allow(null), otherwise: joi.string().required() });

const isOffertaGas = (then: AnySchema) => joi.when('tipoOfferta', { is: 'Gas', then, otherwise: joi.allow('', null) });
const isOffertaLuce = (then: AnySchema) => joi.when('tipoOfferta', { is: 'Elettricita', then, otherwise: joi.allow('', null) });

const offertaGas = isOffertaGas(joi.string().required());
const offertaLuce = isOffertaLuce(joi.string().required());

const isVasAcquistato = (then: AnySchema) => joi.when('vasAcquistato', { is: joi.exist().not('', null), then });
const isSwitchIn = (then: AnySchema) => joi.when('tipoAttivazione', { is: 'Switch_In', then, otherwise: joi.allow('', null) });

export default (): ObjectSchema => joi.object<Record<keyof EdisonPayload, ObjectSchema>>({
  tipoCliente: joi.string().required().allow('Residenziale'),
  tipoOfferta: joi.string().required().allow('Elettricita', 'Gas'),
  dataFirma: joi.string().required(),

  accordoQuadro: joi.string().allow(null),

  serviceType: joi.string().required().only().allow('Contestuale'),

  // anagrafica cliente
  nome: joi.string().required(),
  cognome: joi.string().required(),
  codiceFiscale: joi.string().required(),
  cellularePrincipale: joi.string().required(),
  email: joi.string().required(),

  vasAcquistato: joi.string().allow(null),

  tipoAttivazione: joi.string().required().only().allow('Switch_In', 'Subentro', 'Allaccio_Preposato'),

  ripensamento: joi.boolean().allow('', null),

  attivazioneComune: joi.string().required(),
  attivazioneIndirizzo: joi.string().required(),
  attivazioneCap: joi.string().required(),
  attivazioneCivico: joi.string().required(),
  attivazioneProvincia: joi.string().required(),
  attivazioneScala: joi.string().required(),
  titolaritaImmobile: joi.string().required().only().allow('Proprietario', 'Usufruttuario', 'Affittuario', 'Titolare_Altro_Diritto_Immobile'),

  flagResidente: joi.boolean().required(),
  recapitoUgualeFornitura: joi.boolean().required(),

  recapitoComune: validationRecapito,
  recapitoIndirizzo: validationRecapito,
  recapitoCap: validationRecapito,
  recapitoCivico: validationRecapito,
  recapitoProvincia: validationRecapito,
  recapitoScala: joi.string().allow(null),

  canaleDicontattoPreferito: joi.string().required().allow('Cellulare', 'Email', 'Telefono_Fisso'),
  fasciaOrarioDiRicontatto: joi.string().required().allow('Dalle_09_alle_12', 'Dalle_12_alle_16', 'Dalle_16_alle_18', 'Dalle_18_alle_21', 'SABATO'),

  canaleInvioComunicazionePreferito: joi.string().required().allow('Posta_Elettronica', 'Posta_Tradizionale'),
  modalitaConfermaAdesione: joi.string().required().allow('Registrazione Telefonica', 'Conferma scritta'),
  modalitaDiPagamento: joi.string().required().allow('Bollettino_Postale', 'RID', 'Carta_Di_Credito'),

  bollettaElettronica: joi.boolean(),
  sepaIban: joi.when('modalitaDiPagamento', { is: 'RID', then: joi.string().required() }),
  intestatarioContodiverso: joi.boolean().required(),
  nomeIntestatarioconto: joi.string().required(),
  cognomeIntestatarioConto: joi.string().required(),
  codicefiscaleIntestatarioConto: joi.string().required(),
  subagenzia: joi.string().required(),
  modalitaInvioLinkform: joi.string().required(), // sms

  tipoAbitazione: isVasAcquistato(joi.string().required().valid('Casa', 'Appartamento')),
  impiantoConforme: isVasAcquistato(joi.string().required().valid('1', 'SI', 'NO')),

  informativaSullaPrivacy: joi.boolean().required(),
  consenso1: joi.boolean().required(),
  consenso2: joi.boolean().required(),
  consenso3: joi.boolean().required(),

  // luce:
  codicePod: offertaLuce,
  offertaAcquistataLuce: offertaLuce,
  // .valid('a0U2o00000oSloDEAS', 'a0U5700000en9CeEAI', 'a0U5700000lje5bEAA', 'a0U5700000lje5eEAA', 'a0UD000000PkvX4MAJ'),
  consumoUltimi12mesi: joi.number().allow(null),
  spesaAnnoPrecedenteLuce: joi.number().allow(null),
  attualeFornitoreLuce: joi.string().allow(null),
  potImpegnata: joi.string().allow(null, 'P0_5', 'P1', 'P1_5', 'P2', 'P2_5', 'P3', 'P3_5', 'P4', 'P4_5', 'P5', 'P5_5', 'P6', 'P7', 'P8', 'P9', 'P10', 'P15', 'P20', 'P25', 'P30', 'Oltre_30'),
  potDisponibile: joi.alternatives(joi.string(), joi.number()).allow(null),
  tipoTensione: joi.string().allow(null).valid('Bassa_Tensione'),
  tensione: joi.string().allow(null).valid('T220', 'T380', 'T500', 'T1000', 'T6000', 'T10000', 'T15000', 'T20000', 'T23000', 'T35000', 'Oltre_35000'),
  usoEE: joi.string().allow(null).valid('Domestico'),
  valore: joi.string().allow(null).valid('MONORARIO'),
  mercatoProvenienzaLuce: isOffertaLuce(isSwitchIn(joi.string().required().valid('Libero', 'Maggior Tutela', 'Salvaguardia'))),

  // gas
  codicePdr: offertaGas,
  offertaAcquistataGas: offertaGas,
  // .valid('a0U2o00000oSloCEAS', 'a0U5700000en9h3EAA', 'a0U5700000lje5cEAA', 'a0U5700000lje5gEAA', 'a0UD000000PkvXPMAZ'),
  mercatoProvenienzaGas: isOffertaGas(isSwitchIn(joi.string().required().valid('Libero', 'Maggior Tutela', 'Salvaguardia'))),
  consumoAnnuo: joi.number().allow(null),
  categoriaUso: joi.string().allow(null).valid('C1', 'C2', 'C3'),
  classePrelievo: joi.string().allow(null).valid('5GG', '6GG', '7GG'),
  potenzialitaMassimaRichiesta: joi.string().allow(null),
  mercatoInteresse: joi.string().allow(null).valid('Libero'),
  attualeFornitoreGas: joi.string().allow(null)
});
