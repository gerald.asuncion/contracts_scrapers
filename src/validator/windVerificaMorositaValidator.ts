import joi, { ObjectSchema } from 'joi';
import { WindVerificaMorositaPayload } from '../scraperv2/wind/payload/WindVerificaMorositaPayload';

export default (): ObjectSchema => joi.object<Record<keyof WindVerificaMorositaPayload, ObjectSchema>>({
  comune: joi.string().allow("", null),
  provincia: joi.string().allow("", null),
  indirizzo: joi.string().allow("", null),
  civico: joi.string().allow("", null),
  codiceFiscale: joi.string().required()
});
