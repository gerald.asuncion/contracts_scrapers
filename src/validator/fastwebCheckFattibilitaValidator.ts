import joi, { ObjectSchema } from 'joi';
import { isRequired } from '../validatorUtils';

export default (): ObjectSchema => joi.object({
  idDatiContratto: isRequired(joi.alternatives().try(joi.string(), joi.number())),
  attivazioneComune: joi.string().required(),
  attivazioneIndirizzo: joi.string().required(),
  attivazioneCivico: joi.string().required(),
  attivazioneCap: joi.string().required(),
  attivazioneProvincia: joi.string().required(),
  nome: joi.string().required(),
  cognome: joi.string().required(),
  codiceFiscale: joi.string().required(),
  sesso: joi.string().valid('m', 'f', 'M', 'F').required(),
  dataNascita: joi.string().required(),
  nascitaProvinciaEstera: joi.string().valid('si', 'no').required(),
  nascitaStato: joi.string().required(),
  nascitaComune: joi.string().required(),
  nascitaProvincia: joi.string().required(),
  cellulare: joi.string().required(),
  email: joi.string().required(),
  documentoTipo: joi.string().required(),
  documentoNumero: joi.string().required(),
  documentoRilasciatoDa: joi.string().required(),
  documentoEnteRilascio: joi.string().valid('comune', 'prefettura', 'motorizzazione', 'COMUNE', 'PREFETTURA', 'MOTORIZZAZIONE').required(),
  documentoDataRilascio: joi.string().required(),
  documentoProvinciaRilascio: joi.string().required()
});
