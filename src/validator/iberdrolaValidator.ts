import joi, { ObjectSchema } from 'joi';
import IberdrolaPayload from '../scraperv2/iberdrola/IberdrolaPayload';

const onlyNumbersRegex = /^[+-]?\d*[.,]?\d+(?:[Ee][+-]?\d+)?$/;
const validatoreNumeri = (property: string) => joi
  .string().trim().regex(onlyNumbersRegex).message(`Il '${property}' deve essere una stringa numerica!`);
const whenLuce = (then: joi.SchemaLike, otherwise: joi.SchemaLike = joi.valid(null, '')) => joi.when('tipoEnergia', {
  is: joi.exist().not(null, '').equal('elettricita', 'dual'),
  then,
  otherwise
});
const whenGas = (then: joi.SchemaLike, otherwise: joi.SchemaLike = joi.valid(null, '')) => joi.when('tipoEnergia', {
  is: joi.exist().not(null, '').equal('gas', 'dual'),
  then,
  otherwise
});

export default (): ObjectSchema => joi.object<Record<keyof IberdrolaPayload, ObjectSchema>>({
  tipoIntestatario: joi.string().required().allow('residenziale', 'business'),
  tipoEnergia: joi.string().required().allow('elettricita', 'gas', 'dual'),
  tipoDocumento: joi.string().required().allow('cartaIdentita', 'passaporto', 'patente'),

  primoConsenso: joi.boolean().required(),
  secondoConsenso: joi.boolean().required(),
  terzoConsenso: joi.boolean().required(),

  pdr: joi.string().required().allow(null, ''),
  pod: joi.string().required().allow(null, ''),

  numeroDocumento: joi.string().required(),
  codiceFiscaleRappresentante: joi.string().required(),
  nomeRappresentante: joi.string().required(),
  cognomeRappresentante: joi.string().required(),
  dataDiNascitaRappresentante: joi.string().required(),
  telefonoRappresentante: joi.string().required(),
  emailRappresentante: joi.string().required(),
  ragioneSocialeRappresentante: joi.string().required().allow(null),
  PECRappresentante: joi.string().required().allow(null),

  cap: joi.string().required(),
  comune: joi.string().required(),
  indirizzo: joi.string().required(),
  tipoStrada: joi.string().required(),
  civico: joi.string().required(),

  indirizzoResidenza: joi.string().allow(null),
  capResidenza: joi.string().allow(null),
  comuneResidenza: joi.string().allow(null),
  tipoStradaResidenza: joi.string().allow(null),
  civicoResidenza: joi.string().allow(null),

  attualeFornitoreGas: joi.string().required().allow(null),
  distributoreGas: whenGas(joi.string().required(), joi.string().allow(null, '')),
  utilizzoGas: whenGas(joi.string().required(), joi.string().allow(null, '')),
  consumoGas: whenGas(validatoreNumeri('ConsumoGas').required(), joi.string().allow(null, '')),

  attualeFornitoreElettricita: joi.string().required().allow(null),
  distributoreElettricita: whenLuce(joi.string().required(), joi.string().allow(null, '')),
  // eslint-disable-next-line max-len
  potenzaImpegnata: whenLuce(joi.alternatives().try(validatoreNumeri('PotenzaImpegnata'), joi.number()).required(), joi.string().allow(null, '')),
  consumoLuce: whenLuce(validatoreNumeri('ConsumoLuce').required(), joi.string().allow(null, '')),
  tensione: whenLuce(validatoreNumeri('Tensione').required(), joi.string().allow(null, '')),

  titolaritaImmobile: joi.string().required(),

  modalitaPagamento: joi.string().required(),
  iban: joi.string().required().allow(null),

  codiceABarre: joi.string().required(),
  dataCompletato: joi.string().required(),

  prodottoEnergia: joi.string().required().allow(null),
  prodottoGas: joi.string().required().allow(null),

  modalitaInvioFattura: joi.string().required(),
});
