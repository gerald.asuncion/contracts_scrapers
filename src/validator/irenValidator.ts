import joi, { ObjectSchema } from 'joi';

export default (): ObjectSchema => joi.object({
  codiceFiscale: joi.string().required(),
  webhook: joi.string().allow(null, ""),
});
