import joi, { ObjectSchema } from 'joi';
import { WindVerificaMorositaV3Payload } from '../v3/wind/types';

export default (): ObjectSchema => joi.object<Record<keyof WindVerificaMorositaV3Payload, ObjectSchema>>({
  codiceFiscale: joi.string().required()
});
