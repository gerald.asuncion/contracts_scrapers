import joi, { ObjectSchema } from 'joi';
import {isRequired} from "../validatorUtils";

export default (): ObjectSchema => joi.object({
  idDatiContratto: isRequired(joi.alternatives().try(joi.string(), joi.number())),
  webhook: joi.string().required(),

  codiceOperatore: joi.string().required(),

  nome: joi.string().required(),
  cognome: joi.string().required(),
  codiceFiscale: joi.string().required(),
  dataDiNascita: joi.string().required(),
  luogoDiNascita: joi.string().required(),
  telefonoPrimario: joi.string().required(),
  email: joi.string().required(),
  indirizzo: joi.string().required(),
  civicoIndirizzo: joi.string().required(),
  cap: joi.string().required(),
  citta: joi.string().required(),
  provincia: joi.string().required(),

  indirizzoInstallazione: joi.string().required(),
  civicoInstallazione: joi.string().required(),
  capInstallazione: joi.string().required(),
  cittaInstallazione: joi.string().required(),
  provinciaInstallazione: joi.string().required(),

  metodoPagamento: joi.string().valid('Domiciliazione Bancaria', 'carta di credito').insensitive().required(),
  iban: joi.string().allow(null, ''),
  tipoPagamento: joi.string().valid('Unica Soluzione', 'Pagamento Rateizzato').insensitive().required(),
  durataRateizzazione: joi.string().allow(null, ''),
  orarioContatto: joi.array().valid('9-11', '11-13', '14-16', '16-18').required(),
  profiloServizio: joi.array().valid('30 mega', '100 mega') // TODO: ADD .required()
});
