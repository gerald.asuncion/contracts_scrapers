import joi, { ObjectSchema } from 'joi';
import { areraValidatorSchema } from './areraValidator';

export default (): ObjectSchema => joi.object({
  tipoContratto: joi.string().required(),
  ...areraValidatorSchema
});
