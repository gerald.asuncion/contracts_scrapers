import joi, { ObjectSchema } from 'joi';

export default (): ObjectSchema => joi.object({
  tipoFornitura: joi.string().valid('luce', 'gas').required(),

  // verifica vendibilità
  ragioneSociale: joi.string().required(),
  partitaIva: joi.string().required(),
  codiceFiscale: joi.string().required(),

  pod: joi.string().allow(null),
  pdr: joi.string().allow(null)
});
