import joi, { ObjectSchema } from 'joi';

export default (): ObjectSchema => joi.object({
  codiceCupsArray: joi.array().items(joi.string()).required()
});
