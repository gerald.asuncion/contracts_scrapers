import joi, { ObjectSchema } from 'joi';

export default (): ObjectSchema => joi.object({
  pod: joi.string().required(),
  tipoContratto: joi.string()
    .required()
    .allow('subentro', 'attivazione', 'switch', 'allaccio', 'voltura', 'attivazione-fibra', 'mandato', 'altro')
});
