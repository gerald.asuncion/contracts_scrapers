import joi, { ObjectSchema } from 'joi';
import {isRequired} from "../validatorUtils";

export default (): ObjectSchema => joi.object({
  lista: joi.array().items(joi.object({
    idDatiContratto: isRequired(joi.alternatives().try(joi.string(), joi.number())),
    // nome: joi.string().required(),
    // cognome: joi.string().required(),
    codiceFiscale: joi.string().required()
  })).required()
});
