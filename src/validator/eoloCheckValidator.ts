import joi, { ObjectSchema } from 'joi';

export default (): ObjectSchema => joi.object({
  codiceFastlaneArray: joi.array().required()
});
