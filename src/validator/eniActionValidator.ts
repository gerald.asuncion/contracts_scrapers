import joi, { ObjectSchema } from 'joi';

export default (): ObjectSchema => joi.object({
  action: joi.string().valid('caricaFormeGiuridiche', 'caricaSettoriMerceologici', 'caricaAttivitaMerceologiche', 'caricaCodiciAteco').required(),
  formaGiuridica: joi.string().allow(null),
  settoreMerceologico: joi.string().allow(null),
  attivitaMerceologica: joi.string().allow(null)
});
