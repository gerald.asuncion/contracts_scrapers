import joi, { ObjectSchema } from 'joi';
import { isRequired } from '../validatorUtils';

export default (): ObjectSchema => joi.object({
  idDatiContratto: isRequired(joi.alternatives().try(joi.string(), joi.number())),
  attivazioneComune: joi.string().required(),
  attivazioneIndirizzo: joi.string().required(),
  attivazioneCivico: joi.string().required(),
  attivazioneCap: joi.string().required(),
  attivazioneProvincia: joi.string().required()
});
