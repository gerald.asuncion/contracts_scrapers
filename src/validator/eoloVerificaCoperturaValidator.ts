import joi, { ObjectSchema } from 'joi';
import { EoloVerificaCoperturaPayload } from '../scraperv2/eolo/payload/EoloVerificaCoperturaPayload';

export default (): ObjectSchema => joi.object<EoloVerificaCoperturaPayload>({
  indirizzoAttivazione: joi.string().required(),
  tipoIndirizzoAttivazione: joi.string().required(),
  numeroCivicoAttivazione: joi.string().required(),
  comuneAttivazione: joi.string().required(),
  capAttivazione: joi.string().required(),
  tipoContratto: joi.string().required(),
  precedenteOperatore: joi.string().required(),
  nomeAltroOperatore: joi.string().allow(null)
});
