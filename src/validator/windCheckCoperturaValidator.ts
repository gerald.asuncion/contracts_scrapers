import joi, { ObjectSchema } from 'joi';

export default (): ObjectSchema => joi.object({
  comune: joi.string().required(),
  provincia: joi.string().required(),
  indirizzo: joi.string().required(),
  civico: joi.string().required(),
  numeroFissoPrefisso: joi.string().allow('', null),
  numeroFisso: joi.string().allow('', null),
  numeroMobile: joi.string().allow('', null)
});
