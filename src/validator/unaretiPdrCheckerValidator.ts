import joi, { ObjectSchema } from 'joi';

export default (): ObjectSchema => joi.object({
  tipoContratto: joi.string().required(),
  pdr: joi.string().required()
});
