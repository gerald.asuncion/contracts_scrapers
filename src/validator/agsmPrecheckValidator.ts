import joi, { NumberSchema, ObjectSchema } from 'joi';
import { AgsmPrecheckPayload } from '../scraperv2/agsm/precheck/payload/AgsmPrecheckPayload';

export default (): ObjectSchema => joi.object<Record<keyof AgsmPrecheckPayload, NumberSchema>>({
  prodotto: joi.string().required().allow('luce', 'gas'),
  pod: joi
    .string()
    .empty()
    .allow(null)
    .when('prodotto', {
      is: 'luce',
      then: joi.required()
    }),
  pdr: joi
    .string()
    .empty()
    .allow(null)
    .when('prodotto', {
      is: 'gas',
      then: joi.required()
    }),

  codiceFiscale: joi
    .string()
    .empty()
    .allow(null),
  pIva: joi
    .string()
    .empty()
    .allow(null),

  dataDecorrenza: joi
    .string()
    .empty()
    .allow(null)
    .when('prodotto', {
      is: 'luce',
      then: joi.required()
    })
});
// .xor('pod', 'pdr')
// .xor('codiceFiscale', 'pIva');
