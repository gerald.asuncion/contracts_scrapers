import joi, { ObjectSchema } from 'joi';
import { EdisonInserimentoV2Payload } from '../scraperv2/edisonV2/payload/EdisonInserimentoV2Payload';
import edisonValidator from './edisonValidator';

export default (): ObjectSchema => joi.object<Record<keyof EdisonInserimentoV2Payload, ObjectSchema>>({
  fase: joi.when('tipoOfferta', {
    is: 'Elettricita',
    then: joi.when('tipoAttivazione', {
      is: joi.not().equal('Switch_In'),
      then: joi.string().allow(null, 'Monofase', 'Trifase'),
      otherwise: joi.allow(null, '')
    }),
    otherwise: joi.allow(null, '')
  })
}).concat(edisonValidator());
