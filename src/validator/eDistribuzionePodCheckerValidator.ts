import joi, { ObjectSchema } from 'joi';

export default (): ObjectSchema => joi.object({
  pod: joi.string().required(),
  tipoContratto: joi.string().required()
});
