import joi from 'joi';

export default () => joi.object({
  pdr: joi.string().required(),
  codicePratica: joi.string().allow(null, ''),
  tipoContratto: joi.string().allow(null, ''),
});
