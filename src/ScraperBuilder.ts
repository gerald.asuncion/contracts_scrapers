export default interface ScraperBuilder {
  build(): void;
}
