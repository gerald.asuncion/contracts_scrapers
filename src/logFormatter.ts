import { format } from 'winston';
import ContrattoPayload from './contratti/ContrattoPayload';

const { printf } = format;

const LOG_SEPARATOR = ', ';

const {
  exec_mode, //: 'cluster_mode' | 'fork_mode'
  pm_id,
  name
} = process.env;

export default printf(({
  level,
  message,
  timestamp,
  ...rest
}) => {
  const {
    componentName,
    idDatiContratto
  } = rest as ContrattoPayload & {
    componentName: string;
  };

  return `${[
    `Time="${timestamp}"`,
    `Process="${name}-${pm_id}"`,
    `LogLevel="${level.toUpperCase()}"`,
    componentName ? `Service="${componentName}"` : null,
    idDatiContratto ? `Contratto="${idDatiContratto}"` : null,
    `Message="${message.replace(/"/g, '\'')}"`
  ]
    .filter(Boolean)
    .join(LOG_SEPARATOR)}`;
});
