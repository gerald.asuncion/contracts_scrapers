import { Credenziali, CredenzialiCallcenter, DefaultCallcenter, isCredenziali } from './domain';
import * as store from './store';

// TODO: logger

export default function getCredenzialiScraper(scraper: keyof typeof store, callcenter?: string): Credenziali {
  if (!(scraper in store)) {
    throw Error(`Scraper '${scraper}' non valido per la richiesta credenziali!`);
  }

  const scraperStore = store[scraper] as Credenziali | CredenzialiCallcenter;

  if (isCredenziali(scraperStore)) {
    return scraperStore;
  }

  const credenziali = scraperStore[callcenter || DefaultCallcenter] || scraperStore[DefaultCallcenter];

  if (isCredenziali(credenziali)) {
    return credenziali;
  }

  throw Error('Store delle credenziali malformato!');
}
