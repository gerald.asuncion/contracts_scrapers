export type Credenziali = {
  username: string;
  password: string;
};

export const DefaultCallcenter: unique symbol = Symbol('default');

export type CredenzialiCallcenter = {
  [DefaultCallcenter]: Credenziali;
} & Record<string, Credenziali>;

export function isCredenziali(obj: Record<string, unknown>): obj is Credenziali {
  return !!obj.username && !!obj.password;
}
