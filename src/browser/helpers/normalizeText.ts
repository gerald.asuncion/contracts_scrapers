export default function(text: string, replaceDiatricts = ''): string {
  return text.normalize('NFD').replace(/[\u0300-\u036f]/g, replaceDiatricts);
}
