import { BestMatch, findBestMatch } from 'string-similarity';
import { CollateBestMatchError } from '../errors/methods/collateBestMatchError';

export type CollateBestMatchOptions = {
  /**
   * if matching is case insensitive (true) or sensitive (false) respectively
   * @default true
   */
  caseless: boolean;
  /**
   * minimum threshold of acceptance for matching.
   * if matching is lower then this threshold then an TryOrThrowError exception is thrown
   * can be a positive float lower than 1 or false (no threshold at all)
   * @default 0.83
   */
  threshold: false | number;
};

export default function collateBestMatch(value: string, allowedValues: string[], {
  caseless = true,
  threshold = 0.83
}: Partial<CollateBestMatchOptions> = {}): [number, string, BestMatch] {
  // la libreria corrente non supporta casi abbastanza gravi in caso di collation: (ex. Si e No)
  const acceptedValueBestMatch = findBestMatch(
    caseless ? value.toLowerCase() : value,
    caseless ? allowedValues.map((text) => text.toLowerCase()) : allowedValues
  );

  if (threshold && acceptedValueBestMatch.bestMatch.rating < threshold) {
    throw new CollateBestMatchError(`Miglior match per il valore '${value}' sotto soglia per la lista: '${allowedValues.join("', '")}'`);
  }

  const { bestMatchIndex } = acceptedValueBestMatch;

  return [
    bestMatchIndex,
    allowedValues[bestMatchIndex],
    acceptedValueBestMatch
  ];
}

