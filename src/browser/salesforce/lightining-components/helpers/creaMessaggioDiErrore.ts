import { LabeledError } from './errors';

export default function creaMessaggioDiErrore(errors: LabeledError[]): string {
  return errors.map(({ message }) => {
    if (message.toLowerCase() === 'error kickbox') {
      return "L' indirizzo e-mail inserito non é valido";
    }

    return message;
  }).join('; ');
}
