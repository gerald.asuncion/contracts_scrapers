import { TryOrThrowError } from "../../../../utils/tryOrThrow";

const notEmptyMatch: unique symbol = Symbol('notEmptyMatch');

export default async function retryUntilValue<T extends string | number | boolean | symbol = string>(fn: () => Promise<T>, {
  maxRetry = 10,
  mustMatch = notEmptyMatch as any
}: {
  maxRetry?: number,
  mustMatch?: T
} = {}): Promise<T> {
  if (maxRetry > 0) {
    maxRetry--;

    const value = await fn();

    if (mustMatch === notEmptyMatch) {
      if (typeof value !== 'undefined' && value != null && value !== '') {
        return value;
      }
    } else if (value == mustMatch) {
      return value;
    }

    return retryUntilValue<T>(fn, {
      maxRetry,
      mustMatch
    });
  }

  throw new TryOrThrowError(`Nessun match per il valore '${mustMatch == null ? 'NULL' : mustMatch == notEmptyMatch ? 'NOT EMPTY' : mustMatch.toString()}' dopo tentativi multipli`);
}
