/* eslint-disable no-empty */
import { Page, WaitForSelectorOptions } from 'puppeteer';
import { Logger } from '../../../../logger';

const spinnerExpression = '//lightning-spinner';
const spinnerSelector = '.loadingSpinner';
const hiddenSpinnerSelector = `${spinnerSelector}.slds-hide`;

export default async function waitFormLoading(page: Page, logger: Logger, {
  xPathPre = '',
  xPathPost = ''
}: {
  xPathPre?: string
  xPathPost?: string
} = {}): Promise<void> {
  const waitInOptions: WaitForSelectorOptions = {
    timeout: 2500
  };

  const waitOutOptions: WaitForSelectorOptions = {
    hidden: true,
    timeout: 60000
  };

  try {
    await Promise.race([
      page.waitForXPath(`${xPathPre}${spinnerExpression}${xPathPost}`, waitInOptions),
      page.waitForSelector(spinnerSelector, waitInOptions)
    ]);
  } catch {}

  await Promise.all([
    page.waitForXPath(`${xPathPre}${spinnerExpression}${xPathPost}`, waitOutOptions),
    page.waitForSelector(hiddenSpinnerSelector, waitOutOptions)
  ]);
}
