import { Page } from 'puppeteer';
import tryOrThrow from '../../../../utils/tryOrThrow';
import waitForXPathAndClick from '../../../page/waitForXPathAndClick';
import { expressionEqualsText } from '../helpers/selectors';

export default async function checkCheckbox(page: Page, label: string): Promise<void> {
  return tryOrThrow(async () => {
    const labelSelector = `//lightning-input/label/span${expressionEqualsText(label)}`;

    await waitForXPathAndClick(page, labelSelector);
  }, `Errore durante il check del campo Checkbox "${label}".`);
}
