import { Page } from 'puppeteer';
import waitForXPathAndType from '../../../../browser/page/waitForXPathAndType';
import { Logger } from '../../../../logger';
import tryOrThrow from '../../../../utils/tryOrThrow';
import collateBestMatch from '../../../../browser/helpers/collateBestMatch';
import { expressionContainsClass } from '../helpers/selectors';

export default async function inputCField(page: Page, logger: Logger, $class: string, value: string, force = false): Promise<void> {
  return tryOrThrow(async () => {
    const containerSelector = `//c-address-field${expressionContainsClass($class)}`;
    const inputSelector = `${containerSelector}//input`;
    const datalistSelector = `${containerSelector}//datalist`;
    const optionsSelector = `${datalistSelector}/option`;

    let textValue = value;

    if (!force) {
      const optionsHandler = await page.$x(optionsSelector);

      if (optionsHandler.length > 0) {
        // seleziona da datalist
        const optionsValues = await Promise.all(optionsHandler.map(async (el) => (await el.getProperty('value')).jsonValue<string>()));

        [, textValue] = collateBestMatch(value, optionsValues, {
          threshold: false
        });
      }
    }

    await waitForXPathAndType(page, inputSelector, textValue);
  }, `Errore durante la compilazione del campo CField con selettore ".${$class}" con il valore "${value}".`);
}
