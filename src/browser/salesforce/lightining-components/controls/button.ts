import { Page } from 'puppeteer';
import waitForXPathAndClick from '../../../../browser/page/waitForXPathAndClick';
import { Logger } from '../../../../logger';
import tryOrThrow from '../../../../utils/tryOrThrow';

export default async function clickButton(page: Page, logger: Logger, label: string): Promise<void> {
  return tryOrThrow(async () => {
    const containerSelector = `//lightning-button//button[contains(.,"${label}")]`;

    await waitForXPathAndClick(page, containerSelector);
  }, `Errore durante il click del Bottone "${label}"`);
}
