/* eslint-disable @typescript-eslint/no-non-null-assertion */
import { Page } from 'puppeteer';
import collateBestMatch from '../../../../browser/helpers/collateBestMatch';
import waitForXPathAndClickEvaluated from '../../../../browser/page/waitForXPathAndClickEvaluated';
import { Logger } from '../../../../logger';
import tryOrThrow from '../../../../utils/tryOrThrow';
import retryingWaitFor from '../helpers/retryingWaitFor';
import { expressionContainsText, expressionEqualsText } from '../helpers/selectors';
import { isInputDisabled } from './input';
import waitForXPathAndClick from '../../../page/waitForXPathAndClick';
import waitForXPathVisible from '../../../page/waitForXPathVisible';
import isXPathDisabled from '../../../page/isXPathDisabled';

export function comboboxContainerExpression(label: string, equals: boolean = false): string {
  return `//lightning-combobox/label${equals ? expressionEqualsText(label) : expressionContainsText(label)}//ancestor::lightning-combobox`;
}

export default async function selectCombobox(
  page: Page,
  logger: Logger,
  label: string,
  value: string, {
    equals = false,
    valueProperty = 'title',
    xPathPre = ''
  }: {
    equals?: boolean,
    valueProperty?: string,
    xPathPre?: string
  } = {}
): Promise<void> {
  let allowedValues: string[] | undefined;
  const isValuePropertyAttribute = valueProperty && valueProperty.indexOf('data-') === 0;

  return tryOrThrow(async () => {
    const containerSelector = `${xPathPre}${comboboxContainerExpression(label, equals)}`;
    const inputSelector = `${containerSelector}//input["lightning-basecombobox_basecombobox"]`;
    const itemsSelector = `${containerSelector}//div[@data-dropdown-element]/lightning-base-combobox-item${isValuePropertyAttribute ? `[@${valueProperty}]` : '/span/span'}`;

    if (await isInputDisabled(page, inputSelector)) {
      return;
    }

    await retryingWaitFor(page, logger, itemsSelector, () => waitForXPathAndClickEvaluated(page, inputSelector));

    const itemsHandlers = await page.$x(itemsSelector);

    if (isValuePropertyAttribute) {
      allowedValues = (await Promise.all(
        itemsHandlers.map(async (itemHandler) => (await itemHandler.evaluate((el, _valueProperty) => el.getAttribute(_valueProperty) || '', valueProperty)))
      ));
    } else {
      allowedValues = (await Promise.all(
        itemsHandlers.map(async (itemHandler) => (await itemHandler.getProperty(valueProperty)).jsonValue<string>())
      ));
    }

    // https://stackoverflow.com/questions/47304665/how-to-pass-a-function-in-puppeteers-evaluate-method
    // await page.exposeFunction('findBestMatch', findBestMatch);

    const [acceptedIndex] = collateBestMatch(value, allowedValues, {
      threshold: false
    });

    await itemsHandlers[acceptedIndex].click();
  }, `Errore durante la compilazione del campo Combobox "${label}" con il valore "${value}".${
    allowedValues ? ` I valori ammessi sono: "${allowedValues.join('", "')}".` : ''
  }`);
}

// function shadowDomSelector(element: HTMLElement, ...selectors: string []) {

// }
// shadowDomSelector(null, [
//   `x|//lightning-combobox/label[contains(., "${label}")]//ancestor::lightning-combobox`,
//   '.slds-combobox_container',
//   '*|lightning-base-combobox-item',
//   'attr|title'
// ])

export async function selectComboboxV2(
  page: Page,
  logger: Logger,
  label: string,
  value: string, {
    equals = false,
    valueProperty = 'innerText',
    xPathPre = '',
    forceEvaluate
  }: {
    equals?: boolean,
    valueProperty?: string,
    xPathPre?: string,
    forceEvaluate?: boolean
  } = {}
) {
  const isValuePropertyAttribute = valueProperty && valueProperty.indexOf('data-') === 0;
  // const buttonSelector = `//label[contains(., "${label}")]/following-sibling::div//button`;
  const buttonSelector = `//label${equals ? expressionEqualsText(label) : expressionContainsText(label)}/following-sibling::div//button`;
  const itemSelector = `//label${equals ? expressionEqualsText(label) : expressionContainsText(label)}/following-sibling::div//div[@role="listbox"]//lightning-base-combobox-item`;

  let allowedValues: string [] | undefined;
  await tryOrThrow(
    async () => {
      if (await isXPathDisabled(page, buttonSelector)) {
        return;
      }

      await forceEvaluate ? waitForXPathAndClickEvaluated(page, buttonSelector) : waitForXPathAndClick(page, buttonSelector);
      await waitForXPathVisible(page, itemSelector);
      // await waitForXPathAndClick(page, itemSelector);

      const items = await page.$x(itemSelector);

      // allowedValues = await Promise.all(items.map(async (item) => (await item.getProperty('textContent')).jsonValue()));
      if (isValuePropertyAttribute) {
        allowedValues = (await Promise.all(
          items.map(async (item) => (await item.evaluate((el, _valueProperty) => el.getAttribute(_valueProperty) || '', valueProperty)))
        ));
      } else {
        allowedValues = (await Promise.all(
          items.map(async (item) => (await item.getProperty(valueProperty)).jsonValue())
        )) as string[];
      }

      const [acceptedIndex] = collateBestMatch(value, allowedValues, {
        threshold: false
      });

      await items[acceptedIndex].click();
    },
    `Errore durante la compilazione del campo Combobox "${label}" con il valore "${value}".${
      allowedValues ? ` I valori ammessi sono: "${allowedValues.join('", "')}".` : ''
    }`
  )
}
