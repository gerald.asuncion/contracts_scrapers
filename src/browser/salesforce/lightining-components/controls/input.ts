import { Page } from 'puppeteer';
import waitForXPathAndType, { InputMode } from '../../../../browser/page/waitForXPathAndType';
import { Logger } from '../../../../logger';
import tryOrThrow from '../../../../utils/tryOrThrow';
import { expressionContainsText, expressionDisabled, expressionEqualsText } from '../helpers/selectors';

export function inputTextContainerExpression(label: string, equals: boolean = false, xPathPre: string = ''): string {
  return `${xPathPre}//lightning-input//label${equals ? expressionEqualsText(label) : expressionContainsText(label)}/ancestor::lightning-input`;
}
export function inputTextElementExpression(label: string, equals: boolean = false, xPathPre: string = ''): string {
  return `${inputTextContainerExpression(label, equals, xPathPre)}//input["lightning-input_input"]`;
}
export function inputTextElementDisabledExpression(label: string, equals: boolean = false, xPathPre: string = ''): string {
  return `${inputTextElementExpression(label, equals, xPathPre)}${expressionDisabled()}`;
}

export async function isInputDisabled(page: Page, selector: string): Promise<boolean> {
  return !!(await page.$x(`${selector}${expressionDisabled()}`)).length;
}

export default async function inputText(page: Page, logger: Logger, label: string, value: string, {
  clearSelection = false,
  equals = false,
  xPathPre = ''
}: {
  clearSelection?: boolean,
  equals?: boolean,
  xPathPre?: string
} = {}): Promise<void> {
  return tryOrThrow(async () => {
    const inputSelector = inputTextElementExpression(label, equals, xPathPre);

    if (!(await isInputDisabled(page, inputSelector))) {
      await waitForXPathAndType(page, inputSelector, value, clearSelection ? InputMode.SelectFullLine : InputMode.default);
    }
  }, `Errore durante la compilazione del campo INPUT "${label}" con il valore "${value}".`);
}

export function getInputText(page: Page, label: string, {
  xPathPre = ''
}: {
  xPathPre?: string
} = {}): Promise<string> {
  return tryOrThrow(async () => {
    const inputSelector = inputTextElementExpression(label, false, xPathPre);
    const [xElement] = await page.$x(inputSelector);

    return page.evaluate((el) => el.value, xElement);
  }, `Errore durante il recuper del valore del campo INPUT "${label}".`);
}
