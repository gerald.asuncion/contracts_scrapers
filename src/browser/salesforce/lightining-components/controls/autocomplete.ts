/* eslint-disable no-empty */
import { ElementHandle, Page } from 'puppeteer';
import collateBestMatch from '../../../../browser/helpers/collateBestMatch';
import waitForXPathAndClick from '../../../../browser/page/waitForXPathAndClick';
import waitForXPathAndType, { InputMode } from '../../../../browser/page/waitForXPathAndType';
import { Logger } from '../../../../logger';
import delay from '../../../../utils/delay';
import tryOrThrow from '../../../../utils/tryOrThrow';
import { ControlHasNoAllowedValuesError } from '../../../errors/controls/controlHasNoAllowedValuesError';

const selectorsMappers = {
  iberdrola: (label: string, xPathPre: string = '') => {
    const containerSelector = `${xPathPre}//c-ibd-lookup//label[contains(., "${label}")]//ancestor::c-ibd-lookup`;

    return {
      containerSelector,
      inputSelector: `${containerSelector}//input`,
      itemsSelector: `${containerSelector}//ul/li/span[@data-recordid]`
    };
  },
  edison: (label: string, xPathPre: string = '') => {
    const containerSelector = `${xPathPre}//lightning-input-field//label[contains(., "${label}")]//ancestor::lightning-input-field`;

    return {
      containerSelector,
      inputSelector: `${containerSelector}//input`,
      itemsSelector: `${containerSelector}/parent::div/div[@role="listbox"]/ul/li[@data-value]`
    };
  }
}

async function waitForItems(page: Page, itemsSelector: string): Promise<[string[], ElementHandle<Element>[]]> {
  try {
    await page.waitForXPath(itemsSelector, {
      timeout: 10000
    });
  } catch { }

  const itemsHandler = await page.$x(itemsSelector);
  const itemsData = await Promise.all(
    itemsHandler
      .map(async (eh) => eh.evaluate((el) => (el as HTMLElement).innerText))
  );

  return [itemsData, itemsHandler];
}

export async function extractAutocompleteItems(page: Page, logger: Logger, label: string, {
  scraper
}: {
  scraper: keyof typeof selectorsMappers
}): Promise<string[]> {
  const {
    inputSelector,
    itemsSelector
  } = selectorsMappers[scraper](label);

  await waitForXPathAndClick(page, inputSelector);

  const [itemsLabel] = await waitForItems(page, itemsSelector);

  return itemsLabel;
}

export default async function selectAutocomplete(page: Page, logger: Logger, label: string, value: string, {
  scraper,
  xPathPre = '',
  delayItemsSelector = 0
}: {
  scraper: keyof typeof selectorsMappers,
  xPathPre?: string,
  delayItemsSelector?: number
}): Promise<void> {
  let allowedValues: string[]|undefined;

  return tryOrThrow(async () => {
    const {
      inputSelector,
      itemsSelector
    } = selectorsMappers[scraper](label, xPathPre);

    await waitForXPathAndType(page, inputSelector, value, InputMode.SelectFullLine);

    // forza un delay (anche se 0). nei casi in cui gli items sono virtuali o simili
    await delay(delayItemsSelector);

    try {
      await page.waitForXPath(itemsSelector, {
        timeout: 10000
      });
    } catch { }

    let itemIndex = 0;
    let itemsHandler: ElementHandle<Element>[];

    [allowedValues, itemsHandler] = await waitForItems(page, itemsSelector);

    if (allowedValues.length === 0) {
      throw new ControlHasNoAllowedValuesError(`Il campo a valori multipli '${label}' non possiede alcun match per il valore '${value}'!`);
    }

    [itemIndex] = collateBestMatch(value, allowedValues);

    await itemsHandler[itemIndex].click();
  }, `Errore durante la compilazione del campo Autocomplete "${label}" con il valore "${value}".${
    allowedValues ? ` I valori ammessi sono: "${allowedValues.join('", "')}".` : ''
  }`);
}
