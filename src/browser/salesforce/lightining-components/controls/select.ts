/* eslint-disable @typescript-eslint/no-non-null-assertion */
import { Page } from 'puppeteer';
import collateBestMatch from '../../../../browser/helpers/collateBestMatch';
import { Logger } from '../../../../logger';
import tryOrThrow, { TryOrThrowError } from '../../../../utils/tryOrThrow';
import { expressionContainsClass, expressionContainsText, expressionDisabled } from '../helpers/selectors';
import getOptionToSelect from '../../../../scraperv2/eni/inserimentocontratto/getOptionToSelect';
import waitForSelectorAndSelect from '../../../page/waitForSelectorAndSelect';

export function selectContainerExpression($class: string): string {
  return `//*${expressionContainsClass($class)}`;
}
function selectSelectorByClass($class: string): string {
  return `${selectContainerExpression($class)}//select`;
}

export async function isSelectDisabled(page: Page, logger: Logger, $class: string): Promise<boolean> {
  return !!(await page.$x(`${selectSelectorByClass($class)}${expressionDisabled()}`)).length;
}

/*
selettore XPath non più classe
*/
export default async function selectSelect(page: Page, logger: Logger, label: string, value: string, {
  valueNorText = true
}: {
  valueNorText?: boolean
} = {}): Promise<void> {
  return tryOrThrow(async () => {
    const selectExpression = `//div/label/span${expressionContainsText(label)}/parent::label/parent::div//select`;
    const optionsExpression = `${selectExpression}/option`;

    const selectHandler = await page.waitForXPath(selectExpression);
    const optionsHandler = await page.$x(optionsExpression);

    const optionsValues = await Promise.all(optionsHandler.map(async (el) => (await el.getProperty('value')).jsonValue<string>()));
    const optionsLabels = valueNorText ? null : await Promise.all(optionsHandler.map((el) => el.evaluate(el => el.textContent))) as string[];

    const [acceptedIndex, acceptedValue] = collateBestMatch(value, valueNorText ? optionsValues : optionsLabels!, {
      threshold: false
    });

    await selectHandler?.select(valueNorText ? acceptedValue : optionsValues[acceptedIndex]);
  }, `Errore nella selectSelect per il campo ${label}`)
};

export async function selectV2(page: Page, selector: string, label: string) {
  const trimmed = label.trim();
  const valoreOffertaDaSelezionare = await getOptionToSelect(page, selector, trimmed);
  if(valoreOffertaDaSelezionare) {
    await waitForSelectorAndSelect(page, selector, [valoreOffertaDaSelezionare]);
  } else {
    throw new TryOrThrowError(`non sono riuscito a selezionare l'offerta ${trimmed} (${valoreOffertaDaSelezionare})`);
  }
}
