import { Page } from 'puppeteer';
import tryOrThrow from '../../../../utils/tryOrThrow';
import waitForXPathAndClick from '../../../page/waitForXPathAndClick';
import { expressionEqualsText } from '../helpers/selectors';
import isXPathDisabled from '../../../page/isXPathDisabled';

export function radioGroupContainerExpression(label: string): string {
  return `//lightning-radio-group/fieldset/legend${expressionEqualsText(label)}/ancestor::fieldset`;
}

export function radioGropuInputSelector(containerSelector: string, value: string): string {
  return `${containerSelector}//label/input[@value = "${value}"]`;
}

export default async function checkRadioGroup(page: Page, label: string, value: string): Promise<void> {
  return tryOrThrow(async () => {
    const containerSelector = radioGroupContainerExpression(label);
    const labelSelector = `${containerSelector}//label/span${expressionEqualsText(value)}`;
    const inputSelector = radioGropuInputSelector(containerSelector, value);

    if (!(await isXPathDisabled(page, inputSelector))) {
      await waitForXPathAndClick(page, labelSelector);
    }
  }, `Errore durante il check del campo RadioGroup "${label}" con valore ${value}.`);
}
