import puppeteer, {
  Browser, LaunchOptions, BrowserContext, Page
} from 'puppeteer';
import { Logger } from '../logger';
import BrowserContextManager from './BrowserContextManager';
import newPage from './page/newPage';
import extractErrorMessage from '../utils/extractErrorMessage';
import createChildLogger from '../utils/createChildLogger';

type BrowserManagerOptions = {
  puppeteerOptions: LaunchOptions;
  logger: Logger;
};

export default class BrowserManager {
  static readonly RECONNECT_MAX_RETRY: number = 10;

  private puppeteerOptions: LaunchOptions;

  private browser: Browser | undefined;

  private logger: Logger;

  constructor({ puppeteerOptions, logger }: BrowserManagerOptions) {
    this.puppeteerOptions = puppeteerOptions;

    this.logger = createChildLogger(logger, this.constructor.name);

    this.startup = this.startup.bind(this);
  }

  async startup(): Promise<void> {
    const browser = await puppeteer.launch(this.puppeteerOptions);

    browser.on('disconnected', this.startup);

    this.logger.info(`startup browser with pid ${browser.process()?.pid}`);

    this.browser = browser;
  }

  private async exec<T>(fn: (browser: Browser) => Promise<T>, count = 1): Promise<T> {
    const { logger } = this;
    let result: T;
    try {
      if (!this.browser) {
        await this.startup();
      }

      result = await fn(this.browser as Browser);
    } catch (ex) {
      const err = extractErrorMessage(ex);
      if (err.toLowerCase().includes('websocket is not open')) {
        if (count <= BrowserManager.RECONNECT_MAX_RETRY) {
          logger.warn('riavvio la connessione al browser');
          this.browser = undefined;

          result = await this.exec<T>(fn, count + 1);
        } else {
          const msg = `non riesco a connettermi al browser, superato numero massimo (${BrowserManager.RECONNECT_MAX_RETRY}) di tentativi`;
          logger.error(msg);
          throw new Error(msg);
        }
      } else {
        throw ex;
      }
    }

    return result;
  }

  async createBrowserContext(incognito = true): Promise<BrowserContextManager> {
    return this.exec<BrowserContextManager>(async (browser: Browser) => {
      let context: BrowserContext;
      if (incognito) {
        context = await browser.createIncognitoBrowserContext();
      } else {
        context = await browser.defaultBrowserContext();
      }
      return new BrowserContextManager(context);
    });
  }

  async createIncognitoBrowserContext(): Promise<BrowserContextManager> {
    return this.exec<BrowserContextManager>(async (browser: Browser) => {
      const context = await browser.createIncognitoBrowserContext();
      return new BrowserContextManager(context);
    });
  }

  newPage(): Promise<Page> {
    return this.exec<Page>((browser: Browser): Promise<Page> => newPage(browser));
  }

  async close(): Promise<void> {
    if (this.browser) {
      await this.browser.close();
    }
  }
}
