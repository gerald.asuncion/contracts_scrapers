import { BrowserContext, Page } from 'puppeteer';
import newPage from './page/newPage';

export default class BrowserContextManager {
  constructor(private context: BrowserContext) {}

  async newPage(): Promise<Page> {
    return newPage(this.context);
  }

  close(): Promise<void> {
    return this.context.close();
  }
}
