import { TryOrThrowError } from "../../../utils/tryOrThrow";

export class ControlHasNoAllowedValuesError extends TryOrThrowError {}
