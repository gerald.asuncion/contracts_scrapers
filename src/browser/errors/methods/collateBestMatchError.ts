import { TryOrThrowError } from "../../../utils/tryOrThrow";

export class CollateBestMatchError extends TryOrThrowError {}
