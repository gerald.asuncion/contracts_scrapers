import { Page } from 'puppeteer';
import saveScreenshot from './saveScreenShot';
import saveHtml from './saveHtml';
import { Logger } from '../../logger';

export default async function saveScreenshotAndHtml(
  page: Page,
  filename: string,
  logger: Logger
): Promise<void> {
  await saveScreenshot(page, filename, logger);
  await saveHtml(page, filename, logger);
}
