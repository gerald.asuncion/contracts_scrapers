import { Page, WaitForOptions, HTTPResponse } from 'puppeteer';

export default function waitForNavigation(
  page: Page,
  options?: WaitForOptions
): Promise<HTTPResponse | null> {
  return page.waitForNavigation(options || { waitUntil: 'networkidle2' });
}
