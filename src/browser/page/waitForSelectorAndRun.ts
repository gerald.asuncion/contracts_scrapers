import { ElementHandle, Page, WaitForSelectorOptions } from 'puppeteer';

export default async function waitForSelectorAndRun<T>(
  page: Page,
  selector: string,
  cb: { (element: ElementHandle | null): Promise<T> },
  waitOptions?: WaitForSelectorOptions
): Promise<T> {
  const element = await page.waitForSelector(selector, waitOptions);
  return cb(element);
}
