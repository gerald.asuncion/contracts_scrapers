import { ElementHandle, Page, WaitForSelectorOptions } from 'puppeteer';
import waitForXPathAndRun from './waitForXPathAndRun';

export default async function waitForXPathAndSelect(
  page: Page,
  selector: string,
  values: Array<string>,
  waitOptions?: WaitForSelectorOptions
): Promise<Array<string>> {
  return waitForXPathAndRun<Array<string>>(
    page,
    selector,
    async (p: Page, element: ElementHandle | null) => element ? element.select(selector, ...values) : [],
    waitOptions
  );
}
