import { ElementHandle, Page, WaitForSelectorOptions } from 'puppeteer';

export default async function waitForXPathAndRun<T>(
  page: Page,
  selector: string,
  cb: { (page: Page, element: ElementHandle | null): Promise<T> },
  waitOptions?: WaitForSelectorOptions
): Promise<T> {
  const element = await page.waitForXPath(selector, waitOptions);
  return cb(page, element);
}
