import { Page } from 'puppeteer';

export default async function clearBrowserCookies(page: Page): Promise<void> {
  const client = await page.target().createCDPSession();
  await client.send('Network.clearBrowserCookies');
}
