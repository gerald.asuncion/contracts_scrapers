import { Page, WaitForSelectorOptions } from 'puppeteer';
import waitForSelectorAndRun from './waitForSelectorAndRun';

export default async function waitForSelectorAndType(
  page: Page,
  selector: string,
  text: string,
  waitOptions?: WaitForSelectorOptions,
  typeOptions?: { delay: number }
): Promise<void> {
  return waitForSelectorAndRun<void>(
    page,
    selector,
    () => page.type(selector, text, typeOptions),
    waitOptions
  );
}
