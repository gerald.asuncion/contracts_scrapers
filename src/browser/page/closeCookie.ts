import { Page } from 'puppeteer';
import waitForSelectorAndClick from './waitForSelectorAndClick';
import waitForVisibleStyled from './waitForVisibleStyled';

export default async function closeCookie(
  page: Page,
  bannerSelector: string,
  closeBtnSelector: string
): Promise<void> {
  try {
    await waitForVisibleStyled(page, bannerSelector);
    // se da problemi usare waitForSelectorAndClickEvaluated
    await waitForSelectorAndClick(page, closeBtnSelector);
  } catch (ex) {
    // il popup non appare, posso uscire e basta
  }
}
