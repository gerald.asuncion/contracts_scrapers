import { Page } from 'puppeteer';
import isDisabled from './isDisabled';

export default async function ifEnabledExecute(
  page: Page,
  selector: string,
  fn: () => void
): Promise<void> {
  if (!(await isDisabled(page, selector))) {
    await fn();
  }
}
