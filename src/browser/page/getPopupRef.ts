import { Page } from "puppeteer";

export type GetPopupRefOptions = {
  timeout?: number;
};

const TIMOUT_DEF = 120000;

export default async function getPopupRef(page: Page, options?: GetPopupRefOptions): Promise<Page> {
  const timeout = (options && options.timeout) || TIMOUT_DEF;

  return new Promise<Page>((resolve, reject) => {
    let isRejected = false;
    let timeoutId: NodeJS.Timeout;
    const browser = page.browser();
    try {
      browser.once('targetcreated', (target) => {
        if (timeoutId) {
          clearTimeout(timeoutId);
        }

        if (!isRejected) {
          resolve(target.page())
        }
      });

      timeoutId = setTimeout(() => {
        isRejected = true;
        reject(new Error('popup non apparso in tempo'));
      }, timeout);
    } catch (ex) {
      reject(ex);
    }
  });
}
