import { WaitForSelectorOptions, Page, ElementHandle } from 'puppeteer';

export default function waitForHidden(
  page: Page,
  selector: string,
  waitOptions?: WaitForSelectorOptions
): Promise<ElementHandle | null> {
  return page.waitForSelector(
    selector,
    waitOptions ? { ...waitOptions, visible: false } : { visible: false }
  );
}
