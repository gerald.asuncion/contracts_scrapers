export default interface OptionInfo {
  label: string;
  value: string;
}

export type OptionInfoList = Array<OptionInfo>;
