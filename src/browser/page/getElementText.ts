import { Page } from 'puppeteer';

export type ElementWithTextFields = {
  value: string;
  textContent: string | null;
  innerText: string;
  innerHTML: string;
};

export default async function getElementText<T extends Element & ElementWithTextFields & Record<string, any>, K extends keyof ElementWithTextFields = keyof ElementWithTextFields>(
  page: Page,
  selector: string,
  field: K
): Promise<T[K]> {
  const text = await page.evaluate((internalSelector, internalField) => {
    const el = document.querySelector<T>(internalSelector);

    if (el) {
      const value = el[internalField];

      if (value !== null && typeof value !== 'undefined') {
        return value.trim();
      }
    }
    return '';
  }, selector, field);

  return text as T[K];
}

// eslint-disable-next-line max-len
export const getInputText = (page: Page, selector: string): Promise<string> => getElementText<HTMLInputElement>(page, selector, 'value') as Promise<string>;

// eslint-disable-next-line max-len
export const getOptionText = (page: Page, selector: string): Promise<string> => getElementText<HTMLOptionElement>(page, selector, 'textContent') as Promise<string>;
