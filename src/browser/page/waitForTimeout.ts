import { Page } from 'puppeteer';

export default function waitForTimeout(page: Page, timeout: number): Promise<void> {
  return page.waitForTimeout(timeout);
}
