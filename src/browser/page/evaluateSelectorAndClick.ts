import { Page } from 'puppeteer';

export default async function evaluateSelectorAndClick(
  page: Page,
  selector: string
): Promise<void> {
  page.evaluate((cssSelector) => {
    const element = document.querySelector(cssSelector);

    (element as HTMLDivElement)?.click();
  }, selector);
}
