import { Page, Browser } from 'puppeteer';

export default async (page: Page): Promise<Page> => new Promise<Page>((mainRes, mainRej) => {
  const handler: Parameters<Browser['on']>[1] = async (target) => {
    try {
      if (target && target.type() === 'page') {
        page.browser().off('targetcreated', handler);

        const newPage = await target.page();
        const isPageLoaded = await newPage.evaluate(() => document.readyState);

        await (
          isPageLoaded.match('complete|interactive')
            ? newPage
            : new Promise<Page>((res) => newPage.once('domcontentloaded', () => res(newPage)))
        );

        mainRes(newPage);
      }
    } catch (ex) {
      mainRej(ex);
    }
  };

  page.browser().on('targetcreated', handler);
});
