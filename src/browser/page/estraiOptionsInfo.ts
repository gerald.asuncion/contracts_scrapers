import { Page } from 'puppeteer';
import { OptionInfoList } from './OptionInfo';

export default function estraiOptionsInfo(
  page: Page,
  parentSelector: string
): Promise<OptionInfoList> {
  return page.evaluate((selector) => {
    const options = document.querySelectorAll<HTMLOptionElement>(`${selector} option`);

    return Array.from(options).slice(1).map((el) => ({
      label: <string>el.textContent?.trim(),
      value: el.value
    })) as OptionInfoList;
  }, parentSelector);
}
