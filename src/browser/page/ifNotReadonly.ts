import { Page } from 'puppeteer';
import isReadonly from './isReadonly';

export default async function ifNotReadonly(page: Page, selector: string, fn: () => void): Promise<void> {
  if (!(await isReadonly(page, selector))) {
    await fn();
  }
}
