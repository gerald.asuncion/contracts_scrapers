import { Page } from 'puppeteer';

export default async function isDisabled(page: Page, selector: string): Promise<boolean> {
  return page.evaluate((sel) => {
    const el = document.querySelector<{ disabled: boolean; } & Element>(sel);
    return !!el?.disabled;
  }, selector);
}
