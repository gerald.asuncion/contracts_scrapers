import { Page, WaitForSelectorOptions } from 'puppeteer';
import waitForSelectorAndRun from './waitForSelectorAndRun';

export default async function waitForSelectorAndSelect(
  page: Page,
  selector: string,
  values: Array<string>,
  waitOptions?: WaitForSelectorOptions
): Promise<Array<string>> {
  return waitForSelectorAndRun<Array<string>>(
    page,
    selector,
    () => page.select(selector, ...values),
    waitOptions
  );
}
