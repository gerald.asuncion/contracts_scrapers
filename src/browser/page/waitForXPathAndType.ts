import { Page, WaitForSelectorOptions } from 'puppeteer';

export enum InputMode {
  default = 1,
  SelectFullLine = 3,
  SelectMultiLine = 4
}
export default async function waitForXPathAndType(
  page: Page,
  expression: string,
  text: string,
  inputMode: InputMode = InputMode.default,
  waitOptions?: WaitForSelectorOptions,
  typeOptions?: { delay: number; }
): Promise<void> {
  const inputHandler = await page.waitForXPath(expression, waitOptions);

  await inputHandler?.click({ clickCount: inputMode });
  await inputHandler?.type(text, typeOptions);
}
