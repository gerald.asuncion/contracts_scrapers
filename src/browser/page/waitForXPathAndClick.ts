import { ClickOptions, ElementHandle, Page, WaitForSelectorOptions } from 'puppeteer';
import waitForXPathAndRun from './waitForXPathAndRun';

export default async function waitForXPathAndClick(
  page: Page,
  selector: string,
  waitOptions: WaitForSelectorOptions = {
    visible: false
  },
  clickOptions?: ClickOptions
): Promise<void> {
  return waitForXPathAndRun<void>(
    page,
    selector,
    async (p: Page, element: ElementHandle | null) => {
      await element?.click(/* selector,  */clickOptions);
    },
    waitOptions
  );
}
