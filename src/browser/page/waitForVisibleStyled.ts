import { Page, JSHandle } from 'puppeteer';

export default async function waitForVisibleStyled(
  page: Page,
  selector: string,
  waitOptions?: {
    timeout?: number;
    polling?: string | number;
  }
): Promise<JSHandle> {
  const internalOptions = { polling: 100 };
  return page.waitForFunction((innerSelector: string) => {
    const el = document.querySelector(innerSelector);
    if (!el) {
      return false;
    }
    const computedStyle = window.getComputedStyle(el);
    return computedStyle.display === 'block';
  }, waitOptions ? { ...internalOptions, ...waitOptions } : internalOptions, selector);
}
