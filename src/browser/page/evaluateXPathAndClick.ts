import { Page } from 'puppeteer';

export default async function evaluateXPathAndClick(
  page: Page,
  xPathExpression: string
): Promise<void> {
  page.evaluate((expression) => {
    const element = document.evaluate(
      expression,
      document,
      null,
      XPathResult.ANY_TYPE,
      null
    ).iterateNext();

    (element as HTMLDivElement)?.click();
  }, xPathExpression);
}
