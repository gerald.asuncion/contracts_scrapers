import { Page } from 'puppeteer';

export default async function isReadonly(page: Page, selector: string): Promise<boolean> {
  return page.evaluate((sel) => {
    const el = document.querySelector(sel);

    return el.hasAttribute('readonly');
  }, selector);
}
