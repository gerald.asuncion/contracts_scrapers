import { Page } from 'puppeteer';

export default async function clearBrowserCache(page: Page): Promise<void> {
  const client = await page.target().createCDPSession();
  await client.send('Network.clearBrowserCache');
}
