import {ElementHandle} from "puppeteer";

export const waitForElementAndNoFails = async (callback: Promise<any>): Promise<ElementHandle | null> => {
  try {
    return await callback;
  } catch (noError) {}

  return null;
};
