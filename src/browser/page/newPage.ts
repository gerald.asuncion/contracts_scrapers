import { Page, BrowserContext, Browser } from 'puppeteer';

export default async function newPage(context: Browser | BrowserContext): Promise<Page> {
  const page = await context.newPage();
  page.setUserAgent('Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36');
  return page;
}
