import { Page, WaitForSelectorOptions } from 'puppeteer';

export default async function waitForXPathHidden(page: Page, selector: string, options?: WaitForSelectorOptions): Promise<void> {
  await page.waitForXPath(selector, {
    timeout: 60000,
    ...(options || {}),
    hidden: true
  });
}
