import { Page, WaitForSelectorOptions } from 'puppeteer';
import collateBestMatch from '../helpers/collateBestMatch';
import waitForSelectorAndRun from './waitForSelectorAndRun';

export default async function waitForSelectorAndTypeByMatch(
  page: Page,
  selector: string,
  text: string,
  // itemsProperty = 'value',
  waitOptions?: WaitForSelectorOptions,
  typeOptions?: { delay: number }
): Promise<void> {
  const itemsProperty = 'value';

  return waitForSelectorAndRun<void>(
    page,
    `${selector} > option:not([${itemsProperty}=""])`,
    async () => {
      const itemsHandlers = await page.$$(`${selector} > option`);
      const allowedValues: string[] = (await Promise.all(
        itemsHandlers.map(async (itemHandler) => (await itemHandler.getProperty(itemsProperty)).jsonValue())
      )) as string[];

      const [_, acceptedText] = collateBestMatch(text, allowedValues);

      await page.type(selector, acceptedText, typeOptions);
    },
    waitOptions
  );
}
