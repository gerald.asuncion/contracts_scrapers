import { Page } from 'puppeteer';

export default async function isXPathVisible(page: Page, expression: string, timeout = 500): Promise<boolean> {
  try {
    await page.waitForXPath(expression, {
      timeout,
      visible: true
    });
  } catch {
    return false;
  }

  return true;
}
