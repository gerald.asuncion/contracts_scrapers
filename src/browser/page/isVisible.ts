import { Page } from 'puppeteer';
import waitForVisible from './waitForVisible';

export default async function isVisible(page: Page, selector: string, timeout = 500): Promise<boolean> {
  try {
    await waitForVisible(page, selector, {
      timeout
    });
  } catch {
    return false;
  }
  return true;
}
