import { Page } from 'puppeteer';
import isDisabled from './isDisabled';

export default async function isEnabled(page: Page, selector: string): Promise<boolean> {
  const resp = await isDisabled(page, selector);
  return !resp;
}
