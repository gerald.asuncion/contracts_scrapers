import path from 'path';
import { Page } from 'puppeteer';
import fs from 'fs';
import config from 'config';
import { Logger } from '../../logger';
import getCurrentDayFormatted from '../../utils/getCurrentDayFormatted';

export default async function saveHtml(
  page: Page,
  filename: string,
  logger: Logger
): Promise<void> {
  try {
    const downloadDir = path.resolve(config.get('download.path'), getCurrentDayFormatted());
    if (!fs.existsSync(downloadDir)) {
      fs.mkdirSync(downloadDir);
    }
    const filePath = path.join(downloadDir, `${filename}.html`);
    const html = await page.content();
    fs.writeFileSync(filePath, html);
  } catch (ex: any) {
    logger.warn(`Non sono riuscito a salvare il contenuto (html) della pagina - ${ex.message}`);
  }
}
