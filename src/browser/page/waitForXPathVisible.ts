import { Page, WaitForSelectorOptions, Frame } from 'puppeteer';

export default async function waitForXPathVisible(page: Page | Frame, selector: string, options?: WaitForSelectorOptions): Promise<void> {
  await page.waitForXPath(selector, {
    timeout: 60000,
    ...(options || {}),
    visible: true
  });
}
