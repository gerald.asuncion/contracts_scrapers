import { ElementHandle, Page, WaitForSelectorOptions } from 'puppeteer';
import waitForXPathAndRun from './waitForXPathAndRun';

export default async function waitForXPathAndClickEvaluated(
  page: Page,
  selector: string,
  waitOptions: WaitForSelectorOptions = {
    visible: false
  }
): Promise<void> {
  return waitForXPathAndRun<void>(
    page,
    selector,
    (p: Page, element: ElementHandle | null) => page.evaluate((el) => el?.click(), element),
    waitOptions
  );
}
