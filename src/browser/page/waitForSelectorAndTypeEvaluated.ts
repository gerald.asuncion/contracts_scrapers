import { ElementHandle, Page, WaitForSelectorOptions } from 'puppeteer';
import waitForSelectorAndRun from './waitForSelectorAndRun';

export default async function waitForSelectorAndTypeEvaluated(
  page: Page,
  selector: string,
  text: string,
  waitOptions?: WaitForSelectorOptions
): Promise<void> {
  return waitForSelectorAndRun<void>(page, selector, async (element: ElementHandle | null) => {
    await element?.evaluate((el, txt) => { (el as HTMLInputElement).value = txt; }, text);
  }, waitOptions);
}
