import { Page, JSHandle } from 'puppeteer';

export default async function waitForHiddenStyled(
  page: Page,
  selector: string,
  waitOptions?: {
    timeout?: number;
    polling?: string | number;
  }
): Promise<JSHandle> {
  const internalOptions = { polling: 100 };
  return page.waitForFunction((internalSelector: string) => {
    const el = document.querySelector(internalSelector);
    if (!el) {
      return false;
    }
    const computedStyle = window.getComputedStyle(el);
    return computedStyle.display === 'none';
  }, waitOptions ? { ...internalOptions, ...waitOptions } : internalOptions, selector);
}
