import { Page } from 'puppeteer';

export default async <TValue>(page: Page, selector: string, attribute: string): Promise<TValue> => {
  const element = await page.waitForSelector(selector);
  const property = (await element?.getProperty(attribute));

  return await property?.jsonValue() as TValue;
};
