import { Page, WaitForSelectorOptions } from 'puppeteer';

export default async function waitForSelectorClearAndType(
  page: Page,
  selector: string,
  text: string,
  waitOptions?: WaitForSelectorOptions,
  typeOptions?: { delay: number }
): Promise<void> {
  await page.waitForSelector(selector, waitOptions);
  const input = await page.$(selector);
  await input?.click({ clickCount: 3 })
  await input?.type(text, typeOptions);
}
