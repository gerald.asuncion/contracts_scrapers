import path from 'path';
import { Page } from 'puppeteer';
import config from 'config';
import fs from 'fs';
import getCurrentDayFormatted from '../../utils/getCurrentDayFormatted';
import { Logger } from '../../logger';
import extractErrorMessage from '../../utils/extractErrorMessage';

export default async function saveScreenshot(page: Page, nomeImmagine: string, logger: Logger): Promise<void> {
  try {
    const downloadDir = path.resolve(config.get('download.path'), getCurrentDayFormatted());
    if (!fs.existsSync(downloadDir)) {
      fs.mkdirSync(downloadDir);
    }
    const filePath = path.join(downloadDir, `${nomeImmagine}.png`);
    await page.screenshot({ fullPage: true, path: filePath });
  } catch (ex) {
    // nulla da fare
    logger.warn(extractErrorMessage(ex));
  }
}
