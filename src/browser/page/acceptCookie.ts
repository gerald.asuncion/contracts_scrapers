import { Page } from 'puppeteer';
import waitForSelectorAndClick from './waitForSelectorAndClick';
import waitForVisibleStyled from './waitForVisibleStyled';
import waitForSelectorAndClickEvaluated from './waitForSelectorAndClickEvaluated';
import waitForVisible from './waitForVisible';
import waitForXPathAndClickEvaluated from "./waitForXPathAndClickEvaluated";

export default async function acceptCookie(
  page: Page,
  bannerSelector: string,
  acceptBtnSelector: string,
): Promise<void> {
  try {
    await waitForVisibleStyled(page, bannerSelector);
    await waitForSelectorAndClick(page, acceptBtnSelector);
  } catch (ex) {
    // il popup non appare, posso uscire e basta
  }
}

export async function acceptCookieEvaluated(
  page: Page,
  bannerSelector: string,
  acceptBtnSelector: string,
  isXPath: boolean = false,
): Promise<void> {
  try {
    await waitForVisible(page, bannerSelector, {timeout: 10000});
    if (isXPath) {
      await waitForXPathAndClickEvaluated(page, acceptBtnSelector);
    } else {
      await waitForSelectorAndClickEvaluated(page, acceptBtnSelector);
    }
  } catch (ex) {
    // il popup non appare, posso uscire e basta
  }
}
