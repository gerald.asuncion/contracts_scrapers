import { Page } from 'puppeteer';

type LookForVisibleWithTextElementsResult = Array<{
  tag: string;
  id: string;
  classes: string;
  text: string;
}>;

export default async function lookForVisibleWithTextElements(
  page: Page,
  selector: string
): Promise<LookForVisibleWithTextElementsResult> {
  return page.evaluate((selettore) => [...document.querySelectorAll(selettore)]
    .filter((el) => {
      const style = window.getComputedStyle(el);
      const rect = el.getBoundingClientRect();

      return style.visibility !== 'hidden' && !!(rect.bottom || rect.top || rect.height || rect.width);
    })
    .filter((el) => ((el as HTMLElement).innerText || '').trim())
    .map((el) => ({
      tag: el.tagName,
      id: el.id,
      classes: el.className,
      text: (el as HTMLElement).innerText.trim()
    })), selector);
}
