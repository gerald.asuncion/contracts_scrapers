import { Page, WaitForSelectorOptions } from 'puppeteer';
import { Logger } from '../../logger';
import WrongCredentialsError from '../../errors/WrongCredentialsError';
import waitForVisible from './waitForVisible';

const FALSE_POSITIVE_MESSAGES = ['Target closed', 'timeout'];

function isFalsePositive(ex: Error | string): boolean {
  const msg = typeof ex === 'string' ? ex : ex.message;
  for (const falsePositiveMessage of FALSE_POSITIVE_MESSAGES) {
    if (msg.includes(falsePositiveMessage)) {
      return true;
    }
  }
  return false;
}

export default async function waitCheckCredentials(
  page: Page,
  selector: string,
  loginUrl: string,
  username: string,
  logger: Logger,
  waitOptions?: WaitForSelectorOptions
): Promise<void> {
  try {
    await waitForVisible(page, selector, waitOptions);
    throw new WrongCredentialsError(loginUrl, username);
  } catch (ex: any) {
    if (isFalsePositive(ex)) {
      return Promise.resolve();
    }

    // faccio così per loggare solo gli errori non gestiti
    if (ex instanceof WrongCredentialsError) {
      throw ex;
    }

    logger.error('failed to check login credentials: ', ex);
    throw ex;
  }
}
