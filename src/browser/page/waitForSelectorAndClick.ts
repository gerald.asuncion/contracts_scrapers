import { ClickOptions, ElementHandle, Page, WaitForSelectorOptions } from 'puppeteer';
import waitForSelectorAndRun from './waitForSelectorAndRun';

export default function waitForSelectorAndClick(
  page: Page,
  selector: string,
  waitOptions?: WaitForSelectorOptions,
  clickOptions?: ClickOptions
): Promise<void> {
  return waitForSelectorAndRun(
    page,
    selector,
    async (element: ElementHandle | null) => {
      await element?.click(/* selector,  */clickOptions);
    },
    waitOptions
  );
}
