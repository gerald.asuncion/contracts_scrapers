import { ElementHandle, Page, WaitForSelectorOptions } from 'puppeteer';
import waitForSelectorAndRun from './waitForSelectorAndRun';

export default async function waitForSelectorAndClickEvaluated(
  page: Page,
  selector: string,
  waitOptions?: WaitForSelectorOptions
): Promise<void> {
  return waitForSelectorAndRun<void>(
    page,
    selector,
    async (element: ElementHandle | null) => {
      await element?.evaluate((el) => (el as HTMLElement).click());
    },
    waitOptions
  );
}
