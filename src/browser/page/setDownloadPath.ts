import { Page } from "puppeteer";

export default async function setDownloadPath(page: Page, downloadPath: string) {
  const client = await page.target().createCDPSession();

  await client.send('Page.setDownloadBehavior', {
    behavior: 'allow',
    downloadPath
  });
}
