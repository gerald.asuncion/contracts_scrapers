import path from 'path';
import fs from 'fs';
import { Page } from 'puppeteer';
import { S3Client, PutObjectCommand } from '@aws-sdk/client-s3';
import globalConfig from 'config';
import { Logger } from '../../logger';
import { S3ClientFactoryResult } from '../../aws/s3ClientFactory';
import { AwsS3Config } from '../../config/types';
import extractErrorMessage from '../../utils/extractErrorMessage';
import getCurrentDayFormatted from '../../utils/getCurrentDayFormatted';
import createFileKeyForAwsS3 from '../../utils/createFileKeyForAwsS3';
import mkdir from '../../utils/mkdir';

export default async function saveScreenshotOnAwsS3(
  s3ClientFactory: S3ClientFactoryResult,
  page: Page,
  nomeImmagine: string,
  logger: Logger
): Promise<void> {
  try {
    const runner = s3ClientFactory();
    await runner(async (client: S3Client, config: AwsS3Config) => {
      const fileKey = createFileKeyForAwsS3('screenshot', `${nomeImmagine}.png`);
      logger.info(`salvo lo screenshot ${fileKey} su aws s3`);

      const downloadDir = path.resolve(globalConfig.get('download.path'), getCurrentDayFormatted());
      mkdir(downloadDir);

      const filePath = path.join(downloadDir, `${nomeImmagine}.png`);
      await page.screenshot({ fullPage: true, path: filePath });

      const readStream = fs.createReadStream(filePath);

      const params = { Bucket: config.bucket, Key: fileKey, Body: readStream };
      const command = new PutObjectCommand(params);
      await client.send(command);

      logger.info('elimino il file salvato su aws s3 dalla cartella di download locale');
      fs.unlinkSync(filePath);
    }, logger);
  } catch (ex) {
    // loggo solo un warning per sapere cosa sia successo
    // ma non lancio eccezioni per non bloccare l'applicazione
    logger.warn(extractErrorMessage(ex));
  }
}
