import { Page } from 'puppeteer';
import timeoutPromise from '../../scraperv2/timeoutPromise';

export default function downloadFileByUrl(page: Page, url: string, fileName: string, { delay } = { delay: 300000 }) {
  const timeoutError = timeoutPromise('Download Timeout', delay);
  return timeoutError(page.evaluate((iurl, fname) => {
    return fetch(iurl, {
      method: 'GET',
      credentials: 'include'
    }).then(r => r.blob()).then(blob => {
      let anchor = document.createElement('a');
      anchor.href = URL.createObjectURL(blob);
      anchor.setAttribute('download', fname);
      anchor.click();
    });
  }, url, fileName));
}
