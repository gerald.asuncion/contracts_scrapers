import { Page } from "puppeteer";
import { ElementWithTextFields } from "./getElementText";
import waitForXPathVisible from "./waitForXPathVisible";

export default async function getElementTextByXPath<T extends Element & ElementWithTextFields & Record<string, any>, K extends keyof ElementWithTextFields = keyof ElementWithTextFields>(
  page: Page,
  selector: string,
  field: K
): Promise<T[K]> {
  await waitForXPathVisible(page, selector);
  const [el] = await page.$x(selector);
  // const el = await page.evaluateHandle((el) => el.nextElementSibling, label);

  const result = await (await el.getProperty(field)).jsonValue();

  return result as T[K];
}
