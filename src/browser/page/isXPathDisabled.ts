import { Page } from "puppeteer";

export default async function isXPathDisabled(page: Page, selector: string) {
  const [$el] = await page.$x(selector);
  return page.evaluate((el) => {
    return !!el?.disabled;
  }, $el);
}
