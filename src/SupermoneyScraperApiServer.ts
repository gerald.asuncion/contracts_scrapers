import http from 'http';
import express, {
  Router, Request, Response, NextFunction
} from 'express';
import bodyParser from 'body-parser';
import morgan from 'morgan';
import scopePerRequest from './scopePerRequest';
import container from './container';
import { loggerStream, loggerHttp } from './logger';
import ScraperBuilder from './ScraperBuilder';
import swaggerUi from "swagger-ui-express";
import apiDocs from './api.docs';

const PORT = container.resolve('port');
const router = container.resolve<Router>('router');
const scraperBuilder = container.resolve<ScraperBuilder>('scraperBuilder');

// const agsmGasUploadCsv = container.resolve('agsmGasUploadCsv');

const app = express();
app.use(bodyParser.json());

app.use(morgan('dev', { stream: loggerStream }));

app.use((req, res: express.Response, next) => {
  loggerHttp.info(`${req.method} ${res.statusCode} ${req.originalUrl} ${JSON.stringify(req.body)}`.replace(/\n/g, '').trim());
  next();
});

app.use(scopePerRequest(container));

app.use('/api', swaggerUi.serve, swaggerUi.setup(apiDocs));

scraperBuilder.build();

app.use(router);

// eslint-disable-next-line @typescript-eslint/no-unused-vars
app.use((err: unknown, req: Request, res: Response, next: NextFunction) => {
  loggerHttp.error(err);
});

export type SupermoneyScraperApiServer = {
  start: () => http.Server;
};

const supermoneyScraperApiServer: SupermoneyScraperApiServer = {
  // eslint-disable-next-line no-console
  start: (): http.Server => app.listen(PORT, () => console.log(`Server started on port ${PORT}`))
};

export default supermoneyScraperApiServer;
