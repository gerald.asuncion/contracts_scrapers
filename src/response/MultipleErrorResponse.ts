import SupermoneyResponse from './SupermoneyResponse';

export default class MultipleErrorResponse implements SupermoneyResponse<SupermoneyResponse<any>[]> {
  readonly code: string = '05';

  readonly message: string = 'ERROR';

  readonly details: SupermoneyResponse<any>[];

  extra: any;

  constructor(details: SupermoneyResponse<any>[] = [], extra?: any) {
    this.details = details;
    this.extra = extra;
  }
}
