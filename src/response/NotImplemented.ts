import SupermoneyResponse from './SupermoneyResponse';

export default class NotImplemented implements SupermoneyResponse<string> {
  readonly code: string = '04';

  readonly message: string = 'NOT_IMPLEMENTED';

  readonly details: string;

  extra: any;

  constructor(details: string) {
    this.details = details;
  }
}
