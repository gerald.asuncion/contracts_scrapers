import SupermoneyResponse from './SupermoneyResponse';

export default class GenericSuccessResponse<T> implements SupermoneyResponse<T> {
  readonly code: string = '01';

  readonly message: string = 'SUCCESS';

  readonly details: T;

  extra: any;

  constructor(details: T, extra?: any) {
    this.details = details;
    this.extra = extra;
  }
}
