import SupermoneyResponse from './SupermoneyResponse';

export default class TimeOutResponse implements SupermoneyResponse<string> {
  readonly code: string = '06';

  readonly message: string = 'TIMEOUT';

  readonly details: string;

  extra: any;

  constructor(details: string) {
    this.details = details;
  }
}
