import GenericSuccessResponse from './GenericSuccessResponse';

export default class SuccessResponse extends GenericSuccessResponse<string> {
  constructor(details = 'Ok', extra?: any) {
    super(details, extra);
  }
}
