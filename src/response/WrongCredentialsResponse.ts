import SupermoneyResponse from './SupermoneyResponse';

type Credentials = {
  url: string;
  username: string;
};

export default class WrongCredentialsResponse implements SupermoneyResponse<string> {
  readonly message: string = 'WRONG_CREDENTIALS';

  readonly code: string = '03';

  readonly credential: Credentials;

  readonly details: string;

  extra: any;

  constructor(url: string, username: string) {
    this.credential = { url, username };
    this.details = `Wrong credentials ${username} for ${url}`;
  }
}
