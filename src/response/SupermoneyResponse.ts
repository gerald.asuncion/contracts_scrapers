export default interface SupermoneyResponse<T> {
  code: string;
  message: string;
  details: T;
  extra: any;
}
