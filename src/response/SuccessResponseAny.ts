import GenericSuccessResponse from './GenericSuccessResponse';

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export default class SuccessResponseAny extends GenericSuccessResponse<any> {}
