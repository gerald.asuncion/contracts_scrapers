import ErrorResponse from './ErrorResponse';

export default class CasoNonGestitoResponse extends ErrorResponse {
  constructor() {
    super('caso non gestito');
  }
}
