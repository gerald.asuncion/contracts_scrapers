import SupermoneyResponse from './SupermoneyResponse';

export default class MultipleResponse implements SupermoneyResponse<SupermoneyResponse<any>[]> {
  readonly code: string = '02';

  readonly message: string = 'FAILURE';

  readonly details: SupermoneyResponse<any>[];

  extra: any;

  constructor(details: SupermoneyResponse<any>[] = [], extra?: any) {
    this.details = details;
    this.extra = extra;
  }
}
