import SupermoneyResponse from './SupermoneyResponse';

export default class BlockedScraperResponse implements SupermoneyResponse<string> {
  readonly code: string = '07';

  readonly message: string = 'BLOCKED';

  readonly details: string;

  extra: any;

  constructor(details: string, extra?: any) {
    this.details = details;
    this.extra = extra;
  }
}
