import SupermoneyResponse from './SupermoneyResponse';

export default class FailureResponse implements SupermoneyResponse<string> {
  readonly code: string = '02';

  readonly message: string = 'FAILURE';

  readonly details: string;

  extra: any;

  constructor(details: any, extra?: any) {
    this.details = details;
    this.extra = extra;
  }
}
