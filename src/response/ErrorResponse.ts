import SupermoneyResponse from './SupermoneyResponse';

export default class ErrorResponse implements SupermoneyResponse<string> {
  readonly code: string = '05';

  readonly message: string = 'ERROR';

  readonly details: string;

  extra: any;

  constructor(details: string, extra?: any) {
    this.details = details;
    this.extra = extra;
  }
}
