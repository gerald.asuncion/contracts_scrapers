import {ScraperResponse} from "../scraper";
import { Scraper } from "../types";
import FailureResponse from "../../response/FailureResponse";
import SuccessResponse from "../../response/SuccessResponse";
import {trace} from "../../utils";
import {ComuniRepo} from "../../repo/ComuniRepo";
import { SCRAPER_TIPOLOGIA } from "../ScraperOptions";

export default class AtenaCoperturaLuce implements Scraper {
    private comuniRepo: ComuniRepo;

    private provinceNonCoperte = [
      'TO',
      'GE',
      'PC',
      'RE',
      'PR'
    ];

    constructor({comuniRepo}: { comuniRepo: ComuniRepo }) {
      this.comuniRepo = comuniRepo;
    }

    async login(): Promise<void> {}

    scrape({comune, cap}: any): Promise<ScraperResponse> {
      console.log(comune, cap);


      return this.comuniRepo.findBestMatchingComune(comune, cap)
        .then(trace)
        .then(c => !this.provinceNonCoperte.includes(c.sigla) ? new SuccessResponse(): new FailureResponse(`${comune} non è coperto dalla rete luce Atena`))
    }

    getScraperCodice() {
      return 'atena-coperturaluce';
    }

    getScraperTipologia() {
      return SCRAPER_TIPOLOGIA.FATTIBILITA;
    }
}
