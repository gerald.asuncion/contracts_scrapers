import {ScraperResponse} from "../scraper";
import { Scraper } from "../types";
import FailureResponse from "../../response/FailureResponse";
import SuccessResponse from "../../response/SuccessResponse";
import {AtenaRepo} from "../../repo/AtenaRepo";
import {trace} from "../../utils";
import { SCRAPER_TIPOLOGIA } from "../ScraperOptions";

export default class AtenaCoperturaGas implements Scraper {
  private atenaRepo: AtenaRepo;

  constructor({atenaRepo}: { atenaRepo: AtenaRepo }) {
    this.atenaRepo = atenaRepo;
  }

  async login(): Promise<void> {}

  scrape({comune, cap}: any): Promise<ScraperResponse> {
    console.log(comune, cap);
    return this.atenaRepo.isValidByComune(comune, cap)
      .then(trace)
      .then(response => response.data())
      .then(trace)
      .then(d => d.length > 0 ? new SuccessResponse(): new FailureResponse(`${comune} non è coperto dalla rete gas Atena`));
  }

  getScraperCodice() {
    return 'atena-coperturagas';
  }

  getScraperTipologia() {
    return SCRAPER_TIPOLOGIA.FATTIBILITA;
  }
}
