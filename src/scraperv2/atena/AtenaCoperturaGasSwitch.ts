import {ScraperResponse} from "../scraper";
import { Scraper } from "../types";
import FailureResponse from "../../response/FailureResponse";
import SuccessResponse from "../../response/SuccessResponse";
import {trace} from "../../utils";
import {ComuniRepo} from "../../repo/ComuniRepo";
import { SCRAPER_TIPOLOGIA } from "../ScraperOptions";

export default class AtenaCoperturaGasSwitch implements Scraper {
    private comuniRepo: ComuniRepo;

    private provinceCoperte = [
      'BI', 'VC', 'VB', 'CN', 'VA', 'MI', 'AT', 'AL', 'PV', 'BG', 'VR', 'MB', 'BS', 'NO', 'CR',
      'VE', 'CO', 'PD', 'VI', 'TV', 'UD', 'MN', 'LC', 'RV', 'PN', 'TS', 'TN', 'LO'
    ];

    constructor({ comuniRepo }: { comuniRepo: ComuniRepo }) {
      this.comuniRepo = comuniRepo;
    }

    async login(): Promise<void> {
    }

    scrape({comune, cap}: any): Promise<ScraperResponse> {
      console.log(comune, cap);

      return this.comuniRepo.findBestMatchingComune(comune, cap)
          .then(trace)
          .then(c => this.provinceCoperte.includes(c.sigla) ? new SuccessResponse() : new FailureResponse(`${comune} non è coperto dalla rete gas Atena`));
    }

    getScraperCodice() {
      return 'atena-coperturagasswitch';
    }

    getScraperTipologia() {
      return SCRAPER_TIPOLOGIA.FATTIBILITA;
    }
}
