import EdisonFornitori from './EdisonFornitori';
import { ScraperResponse } from '../scraper';
import creaPayloadFinto from './fornitori/creaPayloadFinto';

export default class EdisonFornitoriGas extends EdisonFornitori {
  async scrapeWebsite(): Promise<ScraperResponse> {
    return super.scrapeWebsite(creaPayloadFinto('Gas') as any);
  }
}
