import todayDate from '../../../utils/todayDate';

export default function creaPayloadFinto(tipoOfferta: 'Elettricita' | 'Gas' = 'Elettricita'): Record<string, unknown> {
  return {
    idDatiContratto: 666,

    tipoCliente: 'Residenziale',
    tipoOfferta,
    dataFirma: todayDate().split('-').reverse().join('/'),

    nome: 'test',
    cognome: 'test',
    codiceFiscale: 'TSTTST80A01F205J',
    cellularePrincipale: '3333333331',
    email: 'test@test.com',

    tipoAttivazione: 'Switch_In',
    serviceType: "Contestuale",
  };
}
