import { ElementHandle, Page, WaitForSelectorOptions } from 'puppeteer';
import { Logger } from 'winston';
import collateBestMatch from '../../browser/helpers/collateBestMatch';
import waitForHiddenStyled from '../../browser/page/waitForHiddenStyled';
import waitForNavigation from '../../browser/page/waitForNavigation';
import waitForSelectorAndClick from '../../browser/page/waitForSelectorAndClick';
import waitForSelectorAndClickEvaluated from '../../browser/page/waitForSelectorAndClickEvaluated';
import waitForSelectorAndRun from '../../browser/page/waitForSelectorAndRun';
import waitForSelectorAndSelect from '../../browser/page/waitForSelectorAndSelect';
import waitForSelectorAndType from '../../browser/page/waitForSelectorAndType';
import waitForVisibleStyled from '../../browser/page/waitForVisibleStyled';
import waitForXPathAndClick from '../../browser/page/waitForXPathAndClick';
import WrongCredentialsError from '../../errors/WrongCredentialsError';
import ErrorResponse from '../../response/ErrorResponse';
import SuccessResponse from '../../response/SuccessResponse';
import createChildPayloadAwareLogger from '../../utils/createChildPayloadAwareLogger';
import delay from '../../utils/delay';
import extractErrorMessage from '../../utils/extractErrorMessage';
import tryOrThrow, { TryOrThrowError } from '../../utils/tryOrThrow';
import { ScraperResponse, WebScraper } from '../scraper';
import {
  EdisonPayload, EdisonPayloadBase, EdisonPayloadGas, EdisonPayloadLuce
} from './payload/EdisonPayload';
import waitForVisible from '../../browser/page/waitForVisible';
import waitForSelectorAndTypeEvaluated from '../../browser/page/waitForSelectorAndTypeEvaluated';
import waitForTimeout from '../../browser/page/waitForTimeout';
import isEnabled from '../../browser/page/isEnabled';
import saveScreenshotOnAwsS3 from '../../browser/page/saveScreenshotOnAwsS3';
import SupermoneyResponse from '../../response/SupermoneyResponse';
import { SCRAPER_TIPOLOGIA } from '../ScraperOptions';

declare const document: Document;

type FormSections = 'sezioneclienteofferta' | 'sezioneanagraficacliente' | 'sezionecomunicazioniresidenziale' |
'blockcontenitorVas' | 'blockcontenitorContract' | 'riepilogopage' | 'contenitoreRigheCommPage' | 'sezionedatifornitura' |
'sezionemetodopagamentoresidenziale' | 'sezionetestatacontrattoresidenziale' | 'sezioneinviocomunicazione' |
'sezioneindirizzofatturazioneresidenziale' | 'idSelezionaVASItem' | 'idFindVasItems';

function inputSelector(sezione: FormSections, name: string) {
  return `input[name*=":${sezione}:"][name$=":${name}"]`;
}
function selectSelector(sezione: FormSections, name: string) {
  return `select[name*=":${sezione}:"][name$=":${name}"]`;
}

function mapping(prop: keyof Pick<EdisonPayload, 'offertaAcquistataLuce' | 'offertaAcquistataGas' | 'vasAcquistato'>, valore: string): string {
  const mapper = {
    offertaAcquistataLuce: {
      'EDISON LUCE PREZZO FISSO12': 'EDISON LUCE PREZZO FISSO12',
      'EDISON SWEET LUCE': 'EDISON SWEET LUCE'
    },
    offertaAcquistataGas: {
      'EDISON LUCE PREZZO FISSO12': 'EDISON LUCE PREZZO FISSO12',
      'EDISON SWEET GAS': 'EDISON SWEET GAS'
    },
    vasAcquistato: {
      'casa relax luce': 'Edison Casa Relax Luce +',
      'casa relax gas': 'Edison Casa Relax Gas +'
    }
  } as Record<'offertaAcquistataLuce' | 'offertaAcquistataGas' | 'vasAcquistato', Record<string, string>>;

  return mapper[prop][valore] || valore;
}

function getSelectorArguments<T>(selectorCreator: typeof inputSelector, ...args: unknown[]): {
  selector: string;
  sezione: FormSections;
  name: string;
  value: T;
} | {
  selector: string;
  sezione: undefined;
  name: undefined;
  value: T;
} {
  // const [page, ...args] = _args;

  if (args.length === 3) {
    return {
      selector: selectorCreator(args[0] as FormSections, args[1] as string),
      sezione: args[0] as FormSections,
      name: args[1] as string,
      value: args[2] as T
    };
  }
  return {
    selector: args[0] as string,
    sezione: undefined,
    name: undefined,
    value: args[1] as T
  };
}

type DialogContextType = {
  message: string | undefined;
  resolver: ((value: unknown) => unknown) | undefined
};

export default class Edison extends WebScraper<EdisonPayload> {
  static readonly LOGIN_URL = 'https://login.salesforce.com/';

  protected nazionalitaDefault = 'ITALIA';

  static readonly config = {
    username: 'alessandro.monti@nice.it',
    password: 'Supermoney2029',

    navBar_nuovoContatto: 'Nuovo Contatto',
    steps: {
      fornituraEComm: 'Dati Fornitura e Comunicazioni',
      accettazioniEdison: 'Accettazioni Edison',
      accettazioniAc: 'Accettazioni AC',
      riepilogo: 'Riepilogo'
    }
  };

  async login(): Promise<void> {
    // const logger = createChildPayloadAwareLogger(this.childLogger, rest);
    const logger = this.childLogger;
    const page = await this.p();

    const { username, password } = await this.getCredenziali();

    await page.goto(Edison.LOGIN_URL, { waitUntil: 'networkidle2' });

    await waitForSelectorAndType(page, '#username', username);
    logger.info('login > inserito username');

    await waitForSelectorAndType(page, '#password', password);
    logger.info('login > inserita password');

    await Promise.all([
      waitForSelectorAndClick(page, '#Login'),
      waitForNavigation(page)
    ]);

    await delay(250);

    const wrongCredentialsHandler = await page.$('#error.loginError');
    if (wrongCredentialsHandler) {
      throw new WrongCredentialsError(Edison.LOGIN_URL, username);
    }

    logger.info('login > terminata');
  }

  async scrapeWebsite(args: EdisonPayload): Promise<ScraperResponse> {
    const logger = createChildPayloadAwareLogger(this.childLogger, args);
    const page = await this.p();

    logger.info("Navigazione verso la pagina per l'inserimento.");

    try {
      await Promise.all([
        // waitForSelectorAndClick(page, 'a[title="Scheda Contratti"]'),
        waitForSelectorAndClick(page, 'li#AllTab_Tab > a'),
        waitForNavigation(page)
      ]);
      await Promise.all([
        waitForSelectorAndClick(page, '.relatedListIcon[title="Contratti"]'),
        waitForNavigation(page)
      ]);
      await Promise.all([
        waitForSelectorAndClick(page, 'input[name="new"]'),
        waitForNavigation(page)
      ]);

      logger.info('Compilazione anagrafica e offerta.');

      await tryOrThrow(async () => {
        await this.sezioneClienteOfferta(page, logger, args);
        await this.checkValidation(page, logger);
      }, 'Errore durante lo sezione ClienteOfferta!');

      await tryOrThrow(async () => {
        await this.sezioneAnagraficaCliente(page, logger, args);
        await this.checkValidation(page, logger);
      }, 'Errore durante lo sezione AnagraficaCliente!');

      await tryOrThrow(async () => {
        await this.clickAndWaitLoading(page, 'input[name$=":ConfermaTestata2"]');
        await this.checkValidation(page, logger);

        try {
          await waitForVisible(page, 'div[role="dialog"]');

          const dialogMessage = await page.$eval('div[role="dialog"] .ui-dialog-content', (el) => el.textContent?.trim());

          throw new TryOrThrowError(dialogMessage as string);
        } catch (ex) {
          if (ex instanceof TryOrThrowError) {
            throw ex;
          }
          // dialog con errore non apparsa => nulla da fare
        }
        await this.goToNextStep(page, Edison.config.steps.fornituraEComm);
      }, 'Errore durante conferma Anagrafica e passaggio a Fornitura:');
      logger.info('Click su conferma anagrafica.');

      logger.info('Compilazione VAS.');

      let warningMessages: string[] = [];

      await tryOrThrow(async () => {
        logger.info('Compilazione sezione VasContestuale.');

        await this.sezioneVasContestuale(page, logger, args);
      }, 'Errore durante lo sezione VasContestuale!');

      await tryOrThrow(async () => {
        logger.info('Compilazione sezione Indirizzo e Dati Fornitura.');

        await this.sezioneIndirizzoFornitura(page, logger, args);
        await this.sezioneDatiFornitura(page, logger, args);
      }, 'Errore durante lo sezione DatiFornitura!');

      await tryOrThrow(async () => {
        const confermaRigaSelector = inputSelector('0' as any, 'confermaRiga');

        if (await page.$(`${confermaRigaSelector}:disabled`)) {
          const messages = await Promise.all([
            this.getWarningMessage(page, logger).then((submessages) => submessages.join('\n')),
            this.getErrorMessage(page, logger).then((submessages) => submessages.join('\n'))
          ]);

          if (messages.length) {
            warningMessages = warningMessages.concat(messages);
          }
        }

        await this.clickAndWaitLoading(page, confermaRigaSelector);

        if (await page.$(selectSelector('contenitoreRigheCommPage', 'Remi'))) {
          await this.selectOptionByIndex(page, 'contenitoreRigheCommPage', 'Remi', 1);

          await this.clickAndWaitLoading(page, '0' as any, 'confermaRiga');
        }

        const preCommodityErrors = await this.getOtherErrorMessage(page);
        if (preCommodityErrors.length) {
          // warningMessages = warningMessages.concat(preCommodityErrors);
          throw new TryOrThrowError(preCommodityErrors.join('\n'));
        }

        const commodityErrors = await Promise.all([
          this.getWarningMessage(page, logger).then((submessages) => submessages.join('\n')),
          this.getErrorMessage(page, logger).then((submessages) => submessages.join('\n'))
        ]);
        if (commodityErrors.length) {
          warningMessages = warningMessages.concat(commodityErrors);
        }

        // riclicco sullo stesso step
        await this.goToNextStep(page, Edison.config.steps.fornituraEComm);
      }, 'Errore durante lo conferma Commodity!');

      await tryOrThrow(async () => {
        logger.info('Compilazione sezione Comunicazioni.');

        try {
          await waitForVisible(page, '[name*="ComuneBC"]');
        } catch (ex) {
          throw new TryOrThrowError(warningMessages.length ? warningMessages.join('\n') : 'Sezione comunicazioni non disponibile. Qualche dato non era corretto per la sezione commodity');
        }

        await this.sezioneComunicazioni(page, logger, args);
        await this.checkValidation(page, logger);
      }, 'Errore durante lo sezione Comunicazioni!');

      await tryOrThrow(async () => {
        logger.info('Compilazione sezione Coordinate.');

        await this.sezioneCoordinate(page, logger, args);
        await this.checkValidation(page, logger);
      }, 'Errore durante lo sezione Coordinate!');

      await tryOrThrow(async () => {
        logger.info('Compilazione sezione InfoContratto.');

        await this.sezioneInfoContratto(page, logger, args);
        await this.checkValidation(page, logger);
      }, 'Errore durante lo sezione InfoContratto!');

      await tryOrThrow(async () => {
        logger.info('Compilazione sezione LinkForm.');

        await this.sezioneLinkForm(page, logger, args);

        const errorMessage = await this.getErrorMessage(page, logger);
        if (errorMessage && errorMessage.some((errMess) => errMess.toLowerCase().indexOf('attenzione: righe non validate.') >= 0)) {
          throw new TryOrThrowError((warningMessages && warningMessages.length ? warningMessages : errorMessage).join('; '));
        }
        await this.checkValidation(page, logger);
      }, 'Errore durante la sezione LinkForm!');

      await tryOrThrow(async () => {
        await this.sezioneAcquistoVas(page, logger, args);

        await this.checkValidation(page, logger);
      }, 'Errore durante lo sezione AcquistoVas!');

      // a volte sfasciola
      await delay(1000);
      if (!await this.goToNextStep(page, Edison.config.steps.accettazioniEdison)) {
        throw new TryOrThrowError((warningMessages).join('\n'));
      }

      await tryOrThrow(async () => {
        await this.sezioneAccettazioneEdison(page, logger, args);

        await this.clickAndWaitLoading(page, 'input[id*=":sezioneprivacy:bottom"].btn');
        await this.checkValidation(page, logger);
      }, 'Errore durante lo sezione Privacy Edison!');

      await this.goToNextStep(page, Edison.config.steps.accettazioniAc);

      await tryOrThrow(async () => {
        await this.sezioneAccettazioneAC(page, logger, args);

        await this.clickAndWaitLoading(page, 'input[id*=":sezioneprivacy:bottom"].btn');
        await this.checkValidation(page, logger);
      }, 'Errore durante lo sezione Privacy AC!');

      await this.goToNextStep(page, Edison.config.steps.riepilogo);

      await waitForNavigation(page);

      this.checkNoSubmit(args);

      await delay(1000);
      try {
        await waitForSelectorAndClick(page, 'input[name^="riepilogopage:"][name$=":SalvaAvanti"]', {
          timeout: 10000
        });
        await this.checkValidation(page, logger);
      } catch {
        // a volte il tasto è disabilitato e dopo un po esegue il salvataggio e redirect automaticamente
      }

      return await tryOrThrow(async () => {
        const dialogContext: DialogContextType = {
          message: undefined,
          resolver: undefined
        };

        page.on('dialog', async dialog => {
          dialogContext.message = dialog.message();
          await dialog.dismiss();

          dialogContext.resolver!(null);
        });

        return this.sezioneInvioContratto(page, logger, dialogContext);
      }, 'Errore durante il recupero codice contratto!');
    } catch (err) {
      const errMessage = extractErrorMessage(err);

      if (page) {
        await saveScreenshotOnAwsS3(this.s3ClientFactory, page, `${args.idDatiContratto}-edison-errore`, logger);
      } else {
        logger.warn('impossibile estrarre lo screenshot; pagina non trovata');
      }

      logger.error(errMessage);
      return new ErrorResponse(errMessage);
    }
  }

  private async sezioneInvioContratto(page: Page, logger: Logger, dialogContext: DialogContextType, retry = 3): Promise<SupermoneyResponse<string>> {
    const contractHandler = await page.waitForSelector('.bPageTitle > .ptBody h2.pageDescription');
    const contractProperty = await contractHandler?.getProperty('textContent');
    const contractValue = (await contractProperty?.jsonValue<string>()) as string;

    const dialogOrNavigation = new Promise(res => dialogContext.resolver = res);

    await Promise.all([
      waitForSelectorAndClick(page, 'input.btn[name="invio_contratto_al_web"]'),
      Promise.race([
        page.waitForNavigation(),
        dialogOrNavigation
      ])
    ]);

    dialogContext.resolver!(undefined);
    dialogContext.resolver = undefined;

    await delay(500);

    if (dialogContext.message) {
      const [tdHandler] = await page.$x(`//td[text()="Stato"]/following-sibling::td[normalize-space(text())='In attesa conferma Link Form']`);

      if (!tdHandler) {
        if (retry < 1) {
          logger.info(`sezioneInvioContratto: retry fallito - ${dialogContext.message}`);

          return new ErrorResponse(dialogContext.message);
        }

        return this.sezioneInvioContratto(page, logger, dialogContext, retry - 1);
      }
    }

    return new SuccessResponse(contractValue.trim());
  }

  async goToNextStep(page: Page, selector: string): Promise<boolean> {
    const [wizardElement] = (await page.$x(`//a[@class="hyper"][normalize-space(text())='${selector}']`));
    if (!wizardElement) {
      return false;
    }

    await tryOrThrow(
      () => Promise.all([
        waitForXPathAndClick(
          page,
          `//a[@class="hyper"][normalize-space(text())='${selector}']`
        ),
        page.waitForResponse(() => true)
      ]),
      `Errore durante il cambio di step: '${selector}'`
    );
    return true;
  }

  async waitLoading(page: Page, message: string, innerCall = false): Promise<void> {
    const selector = '[id$=":overlay.start"]';

    if (innerCall) {
      try {
        await waitForVisibleStyled(page, selector, {
          timeout: 4000
        });
      } catch {}
    }

    await waitForHiddenStyled(page, selector, {
      timeout: 60000
    });

    try {
      // attendi se il loading riparte
      await waitForVisibleStyled(page, selector, {
        timeout: 2000
      });
    } catch {
      return;
    }

    await this.waitLoading(page, message, true);
  }

  async selectFirstAndWaitLoading(page: Page, sezione: FormSections, name: string): Promise<void> {
    const selector = selectSelector(sezione, name);

    tryOrThrow(
      () => page.evaluate((cssSelector: string) => {
        const elSelect = document.querySelector(cssSelector)!;
        const elOption = elSelect.querySelector('option:nth-child(2)')!;

        (elOption as HTMLOptionElement).selected = true;

        const event = new Event('change');
        elSelect.dispatchEvent(event);
      }, selector),
      `Errore selectFirstAndWaitLoading - name '${name}' sezione '${sezione}'`
    );

    await this.waitLoading(page, `Errore selectFirstAndWaitLoading:waitLoading - name '${name}' sezione '${sezione}'`);
  }

  async clickAndWaitLoading(
    page: Page,
    sezione: string|FormSections,
    name?: string,
    clickOptions?: WaitForSelectorOptions
  ): Promise<ReturnType<typeof waitForSelectorAndClick>> {
    let selector: string = sezione;

    if (typeof name !== 'undefined') {
      // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
      selector = inputSelector(sezione as FormSections, name!);
    }

    const result = tryOrThrow(
      () => waitForSelectorAndClick(page, selector, clickOptions),
      name ? `Errore clickAndWaitLoading - selezione '${name}' nella sezione' ${sezione}'` : `Errore clickAndWaitLoading - selezione '${sezione}'`
    );

    await this.waitLoading(page, `Errore clickAndWaitLoading:waitLoading - selezione '${name}' nella sezione '${sezione}'`);

    return result;
  }

  async selectOption(page: Page, selector: string, value: string): Promise<ReturnType<typeof waitForSelectorAndSelect>>;
  async selectOption(page: Page, sezione: FormSections, name: string, value: string): Promise<ReturnType<typeof waitForSelectorAndSelect>>;
  async selectOption(page: Page, ...args: unknown[]): Promise<ReturnType<typeof waitForSelectorAndSelect>> {
    const {
      selector,
      sezione,
      name,
      value
    } = getSelectorArguments<string>(selectSelector, ...args);

    return tryOrThrow(
      () => waitForSelectorAndSelect(page, selector, [value]),
      `Errore selectOption - selezione '${name}' nella sezione '${sezione}'`
    );
  }

  async selectOptionByLabel(page: Page, selector: string, value: string): Promise<void>;
  async selectOptionByLabel(page: Page, sezione: FormSections, name: string, value: string): Promise<void>;
  async selectOptionByLabel(page: Page, ...args: unknown[]): Promise<void> {
    const {
      selector,
      sezione,
      name,
      value
    } = getSelectorArguments<string>(selectSelector, ...args);

    return tryOrThrow(
      async () => {
        const itemsHandlers = await page.$$(`${selector} > option`);
        const allowedValues = (await Promise.all(
          itemsHandlers.map(async (itemHandler) => (await itemHandler.getProperty('label')).jsonValue<string>())
        ));

        const [acceptedIndex] = collateBestMatch(value, allowedValues);

        await this.selectOptionByIndex(page, selector, acceptedIndex);
      }, `Errore selectOption - ${value ? `selezione '${name}' nella sezione '${sezione}'` : `selettore ${selector}`}`
    );
  }

  async selectOptionByIndex(page: Page, selector: string, idx: number): Promise<boolean>;
  async selectOptionByIndex(page: Page, sezione: FormSections, name: string, idx: number): Promise<boolean>;
  async selectOptionByIndex(page: Page, ...args: unknown[]): Promise<boolean> {
    const {
      selector,
      sezione,
      name,
      value
    } = getSelectorArguments<number>(selectSelector, ...args);

    return tryOrThrow(
      async () => {
        const fornituraHandler = await page.$(selector);
        const toSelectValue = await page.evaluate(
          (element, elIdx) => {
            if (element.selectedIndex !== elIdx) {
              return element.querySelectorAll('option')[elIdx].getAttribute('value');
            }
            return null;
          },
          fornituraHandler,
          value
        );

        if (toSelectValue === null) {
          return false;
        }

        await this.selectOption(page, selector, toSelectValue);
        return true;
      },
      `Errore selectOptionByIndex - ${name ? `selezione '${name}' nella sezione '${sezione}'` : `selettore ${selector}`}`
    );
  }

  async selectOptionAndWaitLoading(page: Page, sezione: FormSections, name: string, value: string): Promise<void> {
    await Promise.all([
      this.selectOption(page, sezione, name, value),
      this.waitLoading(page, `Errore selectOptionAndWaitLoading - waitLoading '${name}' nella sezione '${sezione}'`)
    ]);
  }

  async selectjQueryUiAutocomplete(page: Page, selector: string, value: string): Promise<void>;
  async selectjQueryUiAutocomplete(page: Page, sezione: FormSections, name: string, value: string): Promise<void>;
  async selectjQueryUiAutocomplete(page: Page, ...args: unknown[]): Promise<void> {
    const {
      selector,
      sezione,
      name,
      value
    } = getSelectorArguments<string>(inputSelector, ...args);

    return tryOrThrow(
      async () => {
        const itemsData: string[] = await page.evaluate((elementSelector) => {
          const autocompleteElement = $(elementSelector);
          return (<any> autocompleteElement)?.autocomplete('option', 'source');
        }, selector);

        if (itemsData && itemsData.length) {
          const [_, allowedValue] = collateBestMatch(value, itemsData);

          await page.evaluate((elementSelector, val) => {
            const autocompleteElement = $(elementSelector);
            return (<any> autocompleteElement)?.val(val);
          }, selector, allowedValue);
        } else if (sezione) {
          await this.typeInput(page, sezione, name!, value);
        } else {
          await this.typeInput(page, selector, value);
        }
      },
      `Errore selectjQueryUiAutocomplete - '${name}' nella sezione '${sezione}'`
    );
  }

  private async selectGeolabAutocompleteMatch(page: Page, sezione: FormSections, name: string, prop: string, subPrp: string, value: string): Promise<string> {
    let itemsData: string[] = await page.evaluate((property, subProperty) => (window[property] as any).Suggest[subProperty], prop, subPrp); // 'consigli');

    try {
      if (itemsData && itemsData.length) {
        // a volte ci sono degli item tipo '<b>Frazione</b>' che vanno rimossi senza cambiare gli indici
        itemsData = itemsData.map((data) => (data[0] === '<' ? '????????????????????' : data));

        const [_, allowedValue] = collateBestMatch(value, itemsData);

        return allowedValue;
      }
    } catch { }

    return value;
  }

  async selectGeolabAutocomplete(page: Page, sezione: FormSections, name: string, prop: string, subPrp: string, value: string, retry = 4): Promise<void> {
    let allowedValue = value;

    await tryOrThrow(
      async () => {
        await this.typeInput(page, sezione, name, value);

        await delay(250);

        // allowedValue = await this.selectGeolabAutocompleteMatch(page, sezione, name, prop, 'consigli', value);
        allowedValue = await this.selectGeolabAutocompleteMatch(page, sezione, name, prop, subPrp, value);

        await this.typeInput(page, sezione, name, allowedValue);

        await delay(250);

        await page.evaluate((property) => {
          (window[property] as any).Suggest.nascondi();
        }, prop);
        // non funziona sempre
        //   await page.evaluate((property, idx) => {
        //     (window[property] as any).Suggest.seleziona(idx);
        //   }, prop, allowedIndex);
        // }
      },
      `Errore selectGeolabAutocomplete - '${name}' nella sezione '${sezione}'`
    );

    if (retry > 0 && await this.controlIsEmptyOrNotValuedAs(page, inputSelector(sezione, name), allowedValue)) {
      await this.selectGeolabAutocomplete(page, sezione, name, prop, subPrp, value, retry - 1);
    }
  }

  async typeInput(page: Page, selector: string, value: string): Promise<ReturnType<typeof waitForSelectorAndType>>;
  async typeInput(page: Page, sezione: FormSections, name: string, value: string): Promise<ReturnType<typeof waitForSelectorAndType>>;
  async typeInput(page: Page, ...args: any[]): Promise<ReturnType<typeof waitForSelectorAndType>> {
    const {
      selector,
      sezione,
      name,
      value
    } = getSelectorArguments<string>(inputSelector, ...args);

    return tryOrThrow(
      async () => {
        // const inputHandler = await page.waitForSelector(selector);
        // await inputHandler.click({ clickCount: 3 });

        await waitForSelectorAndTypeEvaluated(page, selector, '');

        try {
          // se scoppia con NodeVisibility usiamo il evaluated
          await waitForSelectorAndType(page, selector, value);
        } catch {
          await waitForSelectorAndTypeEvaluated(page, selector, value);
        }
      },
      `Errore typeInput - selezione '${name}' nella sezione '${sezione}'`
    );
  }

  async controlIsEmptyOrNotValuedAs(page: Page, selector: string, value?: string): Promise<boolean>;
  async controlIsEmptyOrNotValuedAs(page: Page, handle: ElementHandle<Element>, value?: string): Promise<boolean>;
  async controlIsEmptyOrNotValuedAs(page: Page, selectorOrHandle: string|ElementHandle<Element>, value?: string): Promise<boolean> {
    let handle: ElementHandle<Element> | null;

    if (typeof selectorOrHandle === 'string') {
      handle = await page.waitForSelector(selectorOrHandle);
    } else {
      handle = selectorOrHandle;
    }

    const controlValue = await page.evaluate((x) => x?.value, handle);

    if (arguments.length > 2 && typeof value !== 'undefined') {
      return controlValue.toLowerCase() !== value.toLowerCase();
    }

    return !controlValue;
  }

  /**
   * non verifica/attende che l'elemento ci sia, ma solo che non ci sia disabilitato
   * @param page
   * @param selector
   * @returns
   */
  async controlEnabled(page: Page, selector: string): Promise<boolean> {
    return isEnabled(page, selector);
  }

  async controlEnabledEndNotValuedAs(page: Page, selector: string, value?: string): Promise<boolean> {
    const controlHandler = await page.waitForSelector(selector);
    const controlIsEnabled = await this.controlEnabled(page, selector);

    if (controlIsEnabled) {
      return this.controlIsEmptyOrNotValuedAs(page, controlHandler as ElementHandle, value);
    }

    return false;
  }

  private async getWarningMessage(page: Page, logger: Logger): Promise<string[]> {
    let message: string[] = [];

    try {
      message = await page.evaluate(() => Array
        .from(document.querySelectorAll('.message.warningM3 [id*=":pMmessaggiRiga:"].messageText'))
        .map((el) => (el as HTMLElement).innerText.replace(/\n\n/gm, ' ').trim()), {
        timeout: 250
      });
    } catch { }

    return message;
  }

  private async getErrorMessage(page: Page, logger: Logger): Promise<string[]> {
    let message: string[] = [];

    // TODO: get main alert error messages => .message.[.errorM6, .errorM4, .errorM3, .errorM2, .errorS1] .messageCell .messageText
    // TODO: get field's error messages '.errorMsg'

    const errorMessagesQuery = ['errorM6', 'errorM4', 'errorM3', 'errorM2', 'errorS1']
      .map((errClass) => `.message.${errClass} span[id*=":pMmessaggi:"] li, .message.${errClass} .messageText`)
      .join(', ');

    try {
      message = await page.evaluate(
        (_errorMessagesQuery) => Array
          .from(document.querySelectorAll(`${_errorMessagesQuery}, .errorMsg`))
          .map((el) => (el as HTMLElement).innerText.replace(/\n\n/gm, ' ').trim()),
        errorMessagesQuery
      );
    } catch { }

    const validationMessades = await page.$x('//*[contains(@id, ":pbRiga:pMmessaggiRiga:")]/ul/li');
    if (validationMessades.length) {
      message = [
        ...message,
        ...await Promise.all(validationMessades
          .map((element) => element.evaluate((el) => el.textContent as string)))
      ];
    }

    return message;
  }

  private async getOtherErrorMessage(page: Page): Promise<string[]> {
    let messages: string[] = [];
    try {
      messages = await page.evaluate(() => Array.from(document.querySelectorAll('.errorMsg')).map(el => el.textContent!.trim()).filter(Boolean));
    } catch (ex) {
      // nulla da fare
    }

    return messages;
  }

  async checkValidation(page: Page, logger: Logger): Promise<void> {
    logger.info('check validation');

    let message: string[] = await this.getErrorMessage(page, logger);

    try {
      message = [
        ...message,
        await waitForSelectorAndRun(page, '.messageClass', async (element) => {
          const res = await element?.evaluate((el) => el.textContent as string);
          return res as string;
        }, {
          timeout: 250
        })
      ];
    } catch { }

    if (message.length) {
      logger.info(`Closing with error message: "${message}"`);

      throw new TryOrThrowError(`Errori di validazione del portale:\n${message.join('\n')}`);
    }
  }

  isContrattoLuce(payload: EdisonPayload): payload is EdisonPayloadBase & EdisonPayloadLuce {
    return payload.tipoOfferta === 'Elettricita';
  }

  async sezioneClienteOfferta(page: Page, logger: Logger, payload: EdisonPayload): Promise<void> {
    logger.info('sezione Cliente Offerta');

    const {
      tipoCliente,
      tipoOfferta,
      dataFirma,
      serviceType
    } = payload;

    // tipo è sempre residenziale che è preselezionato, altrimenti dovrei mettere l'awaitloading dopo
    await this.selectOption(page, 'sezioneclienteofferta', 'TipoCliente', tipoCliente);

    await this.selectOptionAndWaitLoading(page, 'sezioneclienteofferta', 'TipoOfferta', tipoOfferta);

    await this.typeInput(page, 'input[name*=":sezioneclienteofferta:"][name$=":DataFirma"]', dataFirma);
    await Promise.all([
      page.click('label[for*=":sezioneclienteofferta:"][for$=":actionType"]'),
      this.waitLoading(page, '')
    ]);

    await Promise.all([
      this.selectOption(page, 'sezioneclienteofferta', 'serviceType', serviceType),
      this.waitLoading(page, '')
    ]);
  }

  async sezioneAnagraficaCliente(page: Page, logger: Logger, payload: EdisonPayload): Promise<void> {
    logger.info('sezione Anagrafica Cliente');

    const {
      nome,
      cognome,
      codiceFiscale,
      cellularePrincipale,
      email,
      accordoQuadro
    } = payload;

    await this.typeInput(page, 'sezioneanagraficacliente', 'Nome', nome);
    await this.typeInput(page, 'sezioneanagraficacliente', 'Cognome', cognome);
    await this.typeInput(page, 'sezioneanagraficacliente', 'CodiceFiscale', codiceFiscale);

    if (accordoQuadro) {
      await this.selectOption(page, 'sezioneclienteofferta', 'AQConv', accordoQuadro);
      await this.waitLoading(page, '');
    }

    if (cellularePrincipale) {
      await this.typeInput(page, 'sezioneanagraficacliente', 'Cell', cellularePrincipale);
    }

    if (email) {
      await waitForSelectorAndType(page, inputSelector('sezioneanagraficacliente', 'Email'), email);
      await waitForSelectorAndType(page, inputSelector('sezioneanagraficacliente', 'ConfermaEmail'), email);
    }
  }

  // async sezioneVasStandAlone(page: Page, logger: Logger, payload: EdisonPayload): Promise<void> {
  //   logger.info('sezione Vas');

  //   const {
  //     vasAcquistato,
  //     // vasFatturazione,
  //     // vasQuantity,
  //     // vasPagamento
  //   } = payload;

  //   await this.clickAndWaitLoading(page, 'input[name*=":blockcontenitorContract:"][name*=":blockcontenitorRowVas:"]');

  //   await this.clickAndWaitLoading(page, 'blockcontenitorVas', 'SelezionaVAS');

  //   await this.selectOptionAndWaitLoading(page, 'blockcontenitorVas', 'selVasItemOptions', vasAcquistato);
  //   // await this.selectOptionAndWaitLoading(page, 'blockcontenitorVas', 'selIstBuildingCodes', vasFatturazione);

  //   // se la prossima select viene eseguita troppo presto non scatena la generazione del resto della form
  //   // await delay(500);

  //   await this.selectFirstAndWaitLoading(page, 'blockcontenitorVas', 'sellIstBuildingImports');

  //   // solo per vasAcquistato == V-MNTZGP
  //   // if (vasPagamento) {
  //   //   await this.selectOption(page, 'blockcontenitorVas', 'selPaymentTypesSelectOptionList', vasPagamento);
  //   // }
  // }

  // async sezioneVasUpSelling(page: Page, logger: Logger, payload: EdisonPayload): Promise<void> {
  //   await this.selectFirstAndWaitLoading(page, 'blockcontenitorVas', 'selListActivesContracts');
  // }

  async sezioneVasContestuale(page: Page, logger: Logger, payload: EdisonPayload): Promise<void> {
    const {
      tipoAttivazione,
      attualeFornitoreLuce,
      attualeFornitoreGas,
      mercatoProvenienzaLuce,
      mercatoProvenienzaGas,
      mercatoInteresse,
      ripensamento
    } = payload;

    const [
      attualeFornitore,
      mercatoProvenienza
    ] = this.isContrattoLuce(payload) ? [
      attualeFornitoreLuce,
      mercatoProvenienzaLuce
    ] : [
      attualeFornitoreGas,
      mercatoProvenienzaGas
    ];

    await delay(500);
    await waitForSelectorAndClickEvaluated(page, 'input[name*=":blockcontenitorContract:"][name*=":blockcontenitorRowCommodity:"]');

    await Promise.all([
      this.selectOption(page, 'contenitoreRigheCommPage', 'tipoAttivazione', tipoAttivazione),
      this.waitLoading(page, 'Errore durante il loading di "tipoAttivazione"')
    ]);

    if (ripensamento) {
      await waitForSelectorAndClick(page, 'input[id$="flagRipensamento"]');
    }

    if (this.isContrattoLuce(payload)) {
      const {
        codicePod,
        spesaAnnoPrecedenteLuce,
        consumoUltimi12mesi,
        potDisponibile,
        potImpegnata,
        tipoTensione,
        usoEE,
        tensione
      } = payload;

      await this.typeInput(page, 'contenitoreRigheCommPage', 'POD', codicePod);

      await this.typeInput(page, 'sezionedatifornitura', 'SpesaAnnoPrecEE', spesaAnnoPrecedenteLuce.toString());

      await page.keyboard.press('Tab');
      await waitForTimeout(page, 500);

      this.waitLoading(page, 'Errore durante il loading di "spesa anno precendente"');

      await this.typeInput(page, 'sezionedatifornitura', 'ConsumoUltimi12mesi', consumoUltimi12mesi.toString());

      if (potImpegnata) {
        await this.selectOption(page, 'sezionedatifornitura', 'PotImpegnata', potImpegnata);
        const potenzaDisponibile = potDisponibile || (
          potImpegnata === 'P4_5' ? '5,00' : String((parseFloat(potImpegnata.substr(1).replace('_', ',')) * 1.1).toFixed(2)).replace('.', ',')
        );
        await this.typeInput(page, 'sezionedatifornitura', 'PotDisponibile', potenzaDisponibile.toString());
      }

      await this.selectOption(page, 'sezionedatifornitura', 'TipoTensione', tipoTensione);
      await this.selectOption(page, 'sezionedatifornitura', 'Tensione', tensione);
      await this.selectOption(page, 'sezionedatifornitura', 'UsoEE', usoEE);
    } else {
      const {
        codicePdr,
        consumoAnnuo,
        categoriaUso
      } = payload as EdisonPayloadGas;

      await this.typeInput(page, 'contenitoreRigheCommPage', 'PdR', codicePdr);
      await this.typeInput(page, 'sezionedatifornitura', 'ConsumoAnnuo', consumoAnnuo.toString());
      await this.selectOption(page, 'sezionedatifornitura', 'CategoriaUso', categoriaUso);

      if (mercatoInteresse) {
        await this.selectOption(page, 'contenitoreRigheCommPage', 'MercatoInteresse', mercatoInteresse);
      }
    }

    // Dati Fornitura
    if (attualeFornitore && await page.$(inputSelector('sezionedatifornitura', 'FornitoreAttuale'))) {
      await this.selectjQueryUiAutocomplete(page, 'sezionedatifornitura', 'FornitoreAttuale', attualeFornitore);
    }
    const mercatoProvenienzaSelector = selectSelector('sezionedatifornitura', 'MercatoProvenienzaIn');
    if (mercatoProvenienza && await waitForVisible(page, mercatoProvenienzaSelector)) {
      await waitForSelectorAndSelect(page, mercatoProvenienzaSelector, [mercatoProvenienza.split(' ').join('_')])
    }
  }

  async sezioneIndirizzoFornitura(page: Page, logger: Logger, payload: EdisonPayload): Promise<void> {
    const {
      titolaritaImmobile
    } = payload;

    await this.selectOption(page, 'sezionedatifornitura', 'infoAff', titolaritaImmobile);

    const {
      attivazioneComune,
      attivazioneIndirizzo,
      attivazioneCivico,
      attivazioneCap,
      attivazioneProvincia,
      attivazioneScala,
      flagResidente
    } = payload;

    await this.selectGeolabAutocomplete(page, 'sezionedatifornitura', 'ComuneRiga', 'srcBoxLoc', 'comune', attivazioneComune);
    await this.selectGeolabAutocomplete(page, 'sezionedatifornitura', 'IndirizzoRiga', 'srcBoxVia', 'address', attivazioneIndirizzo);
    await this.typeInput(page, 'sezionedatifornitura', 'CivicoRiga', attivazioneCivico);

    if (await this.controlEnabledEndNotValuedAs(page, inputSelector('sezionedatifornitura', 'CAPRiga'), attivazioneCap)) {
      await this.typeInput(page, 'sezionedatifornitura', 'CAPRiga', attivazioneCap);
    }

    if (await this.controlEnabledEndNotValuedAs(page, selectSelector('sezionedatifornitura', 'ProvinciaRiga'), attivazioneProvincia)) {
      await this.selectOption(page, 'sezionedatifornitura', 'ProvinciaRiga', attivazioneProvincia);
    }
    if (attivazioneScala) {
      await waitForSelectorAndClick(page, 'a[id*=":sezionedatifornitura:"][id$=":Informazioni"]');
      await this.typeInput(page, 'sezionedatifornitura', 'ScalaRiga', attivazioneScala);
    }

    if (flagResidente) {
      await waitForSelectorAndClick(page, inputSelector('sezionedatifornitura', 'Residente'));
    }
  }

  async sezioneDatiFornitura(page: Page, logger: Logger, payload: EdisonPayload): Promise<void> {
    await this.clickAndWaitLoading(page, 'input[type="submit"][name*=":sezionedatifornitura:sezioneOfferte:"]');

    const offertaAcquistata = this.isContrattoLuce(payload)
      ? mapping('offertaAcquistataLuce', payload.offertaAcquistataLuce)
      : mapping('offertaAcquistataGas', payload.offertaAcquistataGas!);

    if (offertaAcquistata) {
      await this.selectOptionByLabel(page, 'sezionedatifornitura', 'OffertaAcquistata', offertaAcquistata);
      await this.waitLoading(page, '');
    }

    if (this.isContrattoLuce(payload)) {
      const {
        valore
      } = payload;

      if (valore && await page.$(selectSelector('sezionedatifornitura', 'ValoreOpzione'))) {
        await this.selectOption(page, 'sezionedatifornitura', 'ValoreOpzione', valore);
      }
    } else {
      const {
        classePrelievo,
        potenzialitaMassimaRichiesta
      } = payload;

      if (classePrelievo && await page.$(selectSelector('contenitoreRigheCommPage', 'ClassePrelievo'))) {
        // prime attivazioni e subentri
        await this.selectOption(page, 'contenitoreRigheCommPage', 'ClassePrelievo', classePrelievo);
        await this.waitLoading(page, '');
      }

      if (potenzialitaMassimaRichiesta && await page.$(inputSelector('contenitoreRigheCommPage', 'PotMaxRich'))) {
        await this.typeInput(page, 'contenitoreRigheCommPage', 'PotMaxRich', potenzialitaMassimaRichiesta);
      }
    }
  }

  async sezioneComunicazioni(page: Page, logger: Logger, payload: EdisonPayload): Promise<void> {
    const {
      recapitoUgualeFornitura,
    } = payload;
    let {
      recapitoComune, recapitoIndirizzo, recapitoCivico, recapitoCap, recapitoProvincia, recapitoScala
    } = payload;

    if (recapitoUgualeFornitura) {
      // per qualche inspiegabile motivo dopo un 'po' la checkbox FlagFornUgualeBoll, se ceccata troppo presto, si uncecca da sola: l'ho visto coi miei occhi
      // await waitForSelectorAndClick(page, inputSelector('sezionecomunicazioniresidenziale', 'FlagFornUgualeBoll'));
      // return;

      recapitoComune = payload.attivazioneComune;
      recapitoIndirizzo = payload.attivazioneIndirizzo;
      recapitoCivico = payload.attivazioneCivico;
      recapitoCap = payload.attivazioneCap;
      recapitoProvincia = payload.attivazioneProvincia;
      recapitoScala = payload.attivazioneScala;
    }

    if (recapitoComune) {
      await this.selectGeolabAutocomplete(page, 'sezioneindirizzofatturazioneresidenziale', 'ComuneBC', 'srcBoxLoc', 'comune', recapitoComune);
    }
    if (recapitoIndirizzo) {
      await this.selectGeolabAutocomplete(page, 'sezioneindirizzofatturazioneresidenziale', 'IndirizzoBC', 'srcBoxVia', 'address', recapitoIndirizzo);
    }
    if (recapitoCivico) {
      await this.typeInput(page, 'sezioneindirizzofatturazioneresidenziale', 'CivicoBC', recapitoCivico);
    }

    if (recapitoCap && await this.controlEnabledEndNotValuedAs(page, inputSelector('sezioneindirizzofatturazioneresidenziale', 'CAPBC'), recapitoCap)) {
      await this.typeInput(page, 'sezioneindirizzofatturazioneresidenziale', 'CAPBC', recapitoCap);
    }

    // eslint-disable-next-line max-len
    if (recapitoProvincia && await this.controlEnabledEndNotValuedAs(page, selectSelector('sezioneindirizzofatturazioneresidenziale', 'ProvinciaBC'), recapitoProvincia.toUpperCase())) {
      await this.selectOption(page, 'sezioneindirizzofatturazioneresidenziale', 'ProvinciaBC', recapitoProvincia.toUpperCase());
    }

    if (recapitoScala) {
      await this.typeInput(page, 'sezioneindirizzofatturazioneresidenziale', 'ScalaBC', recapitoScala);
    }

    const {
      fasciaOrarioDiRicontatto,
      canaleDicontattoPreferito,
      canaleInvioComunicazionePreferito,
      modalitaConfermaAdesione
    } = payload;

    await this.selectOption(page, selectSelector('sezionecomunicazioniresidenziale', 'CanaleComunicazione'), canaleDicontattoPreferito);
    await this.selectOption(page, selectSelector('sezionecomunicazioniresidenziale', 'MetodoComunicazione'), canaleInvioComunicazionePreferito);
    await this.selectOption(page, selectSelector('sezionecomunicazioniresidenziale', 'FasciaOraria'), fasciaOrarioDiRicontatto);
    await this.selectOption(page, selectSelector('sezionecomunicazioniresidenziale', 'ModalitaConfermaAdesione'), modalitaConfermaAdesione);
  }

  async sezioneCoordinate(page: Page, logger: Logger, payload: EdisonPayload): Promise<void> {
    const {
      modalitaDiPagamento,
      sepaIban,
      intestatarioContodiverso,
      nomeIntestatarioconto,
      cognomeIntestatarioConto,
      codicefiscaleIntestatarioConto
    } = payload;

    await this.selectOption(page, selectSelector('sezionemetodopagamentoresidenziale', 'ModalitaPagamentoResIn'), modalitaDiPagamento);
    await this.waitLoading(page, '');

    if (modalitaDiPagamento === 'RID') {
      await this.typeInput(page, 'sezionemetodopagamentoresidenziale', 'IbanRes', sepaIban);
    }

    if (intestatarioContodiverso) {
      await waitForSelectorAndClick(page, 'input[id$="CheckIntestatario"]');

      await waitForVisible(page, 'div[id$="sezioneintestatariores"]');
      await waitForSelectorAndType(page, 'input[id$="NomeIntestatarioTestata"]', nomeIntestatarioconto);
      await waitForSelectorAndType(page, 'input[id$="CognomeIntestatarioTestata"]', cognomeIntestatarioConto);
      await waitForSelectorAndType(page, 'input[id$="CodiceFiscaleIntestatarioTestata"]', codicefiscaleIntestatarioConto);
    }
  }

  async sezioneInfoContratto(page: Page, logger: Logger, payload: EdisonPayload): Promise<void> {
    const {
      subagenzia
    } = payload;

    await this.typeInput(page, 'sezionetestatacontrattoresidenziale', 'Subagenzia', subagenzia);
  }

  async sezioneLinkForm(page: Page, logger: Logger, paylod: EdisonPayload): Promise<void> {
    await this.clickAndWaitLoading(page, inputSelector('sezioneinviocomunicazione', 'AttivaLinkForm'));

    const {
      modalitaInvioLinkform
    } = paylod;

    await waitForSelectorAndClick(
      page,
      inputSelector('sezioneinviocomunicazione', modalitaInvioLinkform.toLowerCase() === 'sms' ? 'checkSms' : 'checkEmail')
    );

    await this.clickAndWaitLoading(page, inputSelector('blockcontenitorContract' as any, 'ConfermaTestataResidenziale'));
  }

  async sezioneAcquistoVas(page: Page, logger: Logger, payload: EdisonPayload): Promise<void> {
    const { vasAcquistato } = payload;

    if (vasAcquistato) {
      const {
        tipoAbitazione,
        impiantoConforme
      } = payload;

      await this.clickAndWaitLoading(page, 'input[type="button"][name*=":blockcontenitorContract:"][name*=":blockcontenitorRowVas:"]');

      await this.selectOptionByIndex(page, 'blockcontenitorVas', 'setCommodity', 1);
      await this.waitLoading(page, '');

      await this.clickAndWaitLoading(page, 'idSelezionaVASItem', 'SelezionaVAS');

      await this.selectOptionByLabel(page, 'idFindVasItems', 'selVasItemOptions', mapping('vasAcquistato', vasAcquistato));
      await this.waitLoading(page, '');

      await this.selectOptionByIndex(page, 'blockcontenitorVas', 'selIstBuildingCodes', 1);
      await this.waitLoading(page, '');

      await this.selectOptionByIndex(page, 'blockcontenitorVas', 'sellIstBuildingImports', 1);
      await this.waitLoading(page, '');

      await this.selectOptionByIndex(page, 'blockcontenitorVas', 'selPaymentTypesSelectOptionList', 1);

      if (tipoAbitazione) {
        await this.selectOption(page, 'blockcontenitorVas', 'selTipoAbitazioneSel', tipoAbitazione);
      }
      if (impiantoConforme) {
        await this.selectOption(page, 'blockcontenitorVas', 'selImpConformeSel', impiantoConforme === '1' ? 'SI' : (impiantoConforme || 'NO'));
      }

      await this.clickAndWaitLoading(page, 'blockcontenitorVas', 'insConfermaVAS');
    }
  }

  private async sezioneAccettazione(page: Page, logger: Logger, payload: EdisonPayload): Promise<void> {
    const {
      informativaSullaPrivacy,
      consenso1,
      consenso2,
      consenso3
    } = payload;

    await tryOrThrow(() => waitForVisible(page, 'div[id*="sezioneprivacy"]'), 'il pannello dei consensi non è apparso');

    await tryOrThrow(async () => {
      if (await this.controlEnabled(page, `input[id$=":sezioneprivacy:privacyField:${informativaSullaPrivacy ? '0' : '1'}"]`)) {
        await waitForSelectorAndClick(page, `input[id$=":sezioneprivacy:privacyField:${informativaSullaPrivacy ? '0' : '1'}"]`);
      }
    }, 'non sono riuscito a selezionare il consenso sull\'informativa sulla privacy');

    await tryOrThrow(async () => {
      if (await this.controlEnabled(page, `input[id$=":sezioneprivacy:marketingField:${consenso1 ? '0' : '1'}"]`)) {
        await waitForSelectorAndClick(page, `input[id$=":sezioneprivacy:marketingField:${consenso1 ? '0' : '1'}"]`);
      }
    }, 'non sono riuscito a selezionare il consenso marketing');

    await tryOrThrow(async () => {
      if (await this.controlEnabled(page, `input[id$=":sezioneprivacy:marketingTerziField:${consenso2 ? '0' : '1'}"]`)) {
        await waitForSelectorAndClick(page, `input[id$=":sezioneprivacy:marketingTerziField:${consenso2 ? '0' : '1'}"]`);
      }
    }, 'non sono riuscito a selezionare il consenso marketing terze parti');

    await tryOrThrow(async () => {
      if (await this.controlEnabled(page, `input[id$=":sezioneprivacy:profilazioneField:${consenso3 ? '0' : '1'}"]`)) {
        await waitForSelectorAndClick(page, `input[id$=":sezioneprivacy:profilazioneField:${consenso3 ? '0' : '1'}"]`);
      }
    }, 'non sono riuscito a selezionare il consenso sulla profilazione');
  }

  async sezioneAccettazioneEdison(page: Page, logger: Logger, payload: EdisonPayload): Promise<void> {
    return tryOrThrow(
      () => this.sezioneAccettazione(page, logger, payload),
      'Errore durante la selezione delle privacy Edison'
    );
  }

  async sezioneAccettazioneAC(page: Page, logger: Logger, payload: EdisonPayload): Promise<void> {
    return tryOrThrow(
      () => this.sezioneAccettazione(page, logger, payload),
      'Errore durante la selezione delle privacy AC'
    );
  }

  getScraperCodice() {
    return 'edison';
  }

  getScraperTipologia() {
    return SCRAPER_TIPOLOGIA.INSERIMENTO;
  }
}
