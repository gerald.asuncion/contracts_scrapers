import DbScraper, { DbScraperOptions } from '../DbScraper';
import { ScraperResponse } from '../scraper';
import EdisonBlacklistRepo from '../../repo/EdisonBlacklistRepo';
import FailureResponse from '../../response/FailureResponse';
import SuccessResponse from '../../response/SuccessResponse';
import { EdisonBlacklistFattibilitaPayload } from './payload/EdisonBlacklistFattibilitaPayload';
import { SCRAPER_TIPOLOGIA } from '../ScraperOptions';

export default class EdisonFattibilita extends DbScraper {
  private repo: EdisonBlacklistRepo;

  constructor(options: DbScraperOptions & { edisonBlacklistRepo: EdisonBlacklistRepo; }) {
    super(options);
    this.repo = options.edisonBlacklistRepo;
  }

  async scrape(args: EdisonBlacklistFattibilitaPayload): Promise<ScraperResponse> {
    const { logger, repo } = this;

    const comuneInBlacklist = await repo.comuneConCapPresente(args);

    if (comuneInBlacklist) {
      logger.info(`comune ${args.comune} (${args.cap}) è in blacklist per edison`);
      return new FailureResponse(
        'Il comune di attivazione risulta per Edison in blacklist, non è possibile quindi procedere alla sottoscrizione del contratto'
      );
    }

    logger.info(`comune ${args.comune} (${args.cap}) è ok per edison`);
    return new SuccessResponse();
  }

  getScraperCodice() {
    return 'edison-fattibilita';
  }

  getScraperTipologia() {
    return SCRAPER_TIPOLOGIA.FATTIBILITA;
  }
}
