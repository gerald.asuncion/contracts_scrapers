import EdisonFornitori from './EdisonFornitori';
import { ScraperResponse } from '../scraper';
import creaPayloadFinto from './fornitori/creaPayloadFinto';

export default class EdisonFornitoriLuce extends EdisonFornitori {
  async scrapeWebsite(): Promise<ScraperResponse> {
    return super.scrapeWebsite(creaPayloadFinto() as any);
  }
}
