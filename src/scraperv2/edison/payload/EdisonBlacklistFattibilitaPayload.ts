export type EdisonBlacklistFattibilitaPayload = {
  cap: string;
  comune: string;
};
