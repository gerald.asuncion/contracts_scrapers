import ContrattoPayload from '../../../contratti/ContrattoPayload';

type MercatoProvenienza = 'Libero' | 'Maggior Tutela' | 'Salvaguardia';

export type EdisonPayloadLuce = {
  codicePod: string;
  consumoUltimi12mesi: number;
  spesaAnnoPrecedenteLuce: number;
  /**
   * solo per switch
   */
  attualeFornitoreLuce?: string;
  potImpegnata?: 'P0_5'|'P1'|'P1_5'|'P2'|'P2_5'|'P3'|'P3_5'|'P4'|'P4_5'|'P5'|'P5_5'|'P6'|
  'P7'|'P8'|'P9'|'P10'|'P15'|'P20'|'P25'|'P30'|'Oltre_30';
  /**
   * in kw
   */
  potDisponibile?: string|number;
  tipoTensione: 'Bassa_Tensione';
  tensione: 'T220'|'T380'|'T500'|'T1000'|'T6000'|'T10000'|'T15000'|'T20000'|'T23000'|'T35000'|'Oltre_35000'; // V
  usoEE: 'Domestico';
  /**
   * "EDISON LUCE PREZZO FISSO12'|'EDISON WORLD LUCE'|'EDISON PLACET VARIABILE LUCE - CASA'| 'EDISON PLACET FISSA LUCE - CASA'|'EDISON WEB LUCE"
   * 'a0U2o00000oSloDEAS'|'a0U5700000en9CeEAI'|'a0U5700000lje5bEAA'|'a0U5700000lje5eEAA'|'a0UD000000PkvX4MAJ'
   */
  offertaAcquistataLuce: string;
  /**
   * "MULTIORARIO", "MONORARIO"
   */
  valore: 'MONORARIO';
  /**
   * solo per switch
   */
  mercatoProvenienzaLuce?: MercatoProvenienza;
};

export type EdisonPayloadGas = {
  codicePdr: string;
  /**
   * solo per switch
   */
  mercatoProvenienzaGas?: MercatoProvenienza;
  consumoAnnuo: number;
  /**
   * "C1. Riscaldamento", "C2. Cottura cibi e/o acqua calda", "C3. Riscaldamento + cottura cibi e/o acqua calda"
   */
  categoriaUso: 'C1' | 'C2' | 'C3';
  /**
   * "EDISON GAS PREZZO FISSO12", "EDISON WORLD GAS", "EDISON PLACET VARIABILE GAS - CASA", "EDISON PLACET FISSA GAS - CASA", "EDISON WEB GAS"
   * 'a0U2o00000oSloCEAS' | 'a0U5700000en9h3EAA' | 'a0U5700000lje5cEAA' | 'a0U5700000lje5gEAA' | 'a0UD000000PkvXPMAZ';
   */
  offertaAcquistataGas: string;
  /**
   * da inviare sempre '7 giorni', è solo per prime attivazioni e subentri
   */
  classePrelievo?: '5GG' | '6GG' | '7GG';
  /**
   * è solo per prime attivazioni e subentri
   */
  potenzialitaMassimaRichiesta?: string;
  mercatoInteresse: 'Libero';
  /**
   * solo per switch
   */
  attualeFornitoreGas?: string;
};

export type EdisonPayloadBase = ContrattoPayload & {
  // tipologia contratto
  tipoCliente: 'Residenziale';
  tipoOfferta: 'Elettricita' | 'Gas';
  dataFirma: string;

  /**
   * 'Sconto 25 Gas' = 'a092o00003FdyW2AAJ' | 'Sconto 50 Gas' = 'a09570000326KMvAAM'
   * 'Sconto 25 Luce' = 'a092o00003FdyW3AAJ' | 'Sconto 50 Luce' = 'a09570000326KMuAAM'
   */
  accordoQuadro:
    'a092o00003FdyW3AAJ' | 'a09570000326KMuAAM' | // Elettricità
    'a092o00003FdyW2AAJ' | 'a09570000326KMvAAM'; // Gas

  serviceType: 'Contestuale';

  // anagrafica cliente
  nome: string;
  cognome: string;
  codiceFiscale: string;
  cellularePrincipale: string;
  email: string;

  /**
   * vas
   * options: {
      "V-EMOBFBET": "Edison PLUG&GO BICI ELETTRICA - Top Lady",
      "V-EMOBFBETK": "Edison PLUG&GO BICI ELETT-Top Lady + Kit",
      "V-EMOBFMS": "Edison PLUG&GO Monopattino - Smart",
      "V-EMOBFMSK": "Edison PLUG&GO Monopattino-Smart + Kit",
      "V-EMOBFMT": "Edison PLUG&GO Monopattino - Top",
      "V-EMOBFMTK": "Edison PLUG&GO Monopattino-Top+Kit",
      "V-EMOBKIDS": "Edison PLUG&GO Monopattino - Kids",
      "V-EMOBKIDSINEDS": "Edison PLUG&GO Monopattino-Kids-Inedison",
      "V-EDSRISOLVE": "EdisonRisolve",
      "V-MNTZGP": "Manutenzione Caldaia+ Promo"
    }

    if (option.startsWith('V-EMOB'))
    {
      vasQuantity: number;
      vasFatturazione: 'One_Shot';
    }
    :else:
    {
      vasFatturazione: 'Fee';
    }
   */
  vasAcquistato: 'V-EMOBFBET' | 'V-EMOBFBETK' | 'V-EMOBFMS' | 'V-EMOBFMSK' | 'V-EMOBFMT' | 'V-EMOBFMTK' |
  'V-EMOBKIDS' | 'V-EMOBKIDSINEDS' | 'V-EDSRISOLVE' | 'V-MNTZGP' |
  'V-CSRLPL2' | 'V-CSRLXL+' |
  'V-CSRLXG+' | 'V-CSRLPG2' | 'V-MNTZGP' |
  string;
  // vasQuantity?: number;
  // vasFatturazione: 'One_Shot' | 'Fee';
  // vasPagamento: 'SDD';

  // serviceType = Contestuale >>>
  // idTipologiaContratto: 'Switch' | 'Subentro';
  tipoAttivazione: 'Switch_In' | 'Subentro' | 'Allaccio_Preposato';

  attivazioneComune: string;
  attivazioneIndirizzo: string;
  attivazioneCap: string;
  attivazioneCivico: string;
  attivazioneProvincia: string;
  attivazioneScala: string;
  titolaritaImmobile: 'Proprietario'|'Usufruttuario'|'Affittuario'|'Titolare_Altro_Diritto_Immobile';

  /**
   * Se indirizzo residenza == NULL allora è TRUE
   */
  flagResidente: boolean;
  recapitoUgualeFornitura: boolean;
  /**
   * se recapitoUgualeSpedizione == true -> questo valore diventa il valore del campo “spedizione”
   */
  recapitoComune?: string;
  /**
   * se recapitoUgualeSpedizione == true -> questo valore diventa il valore del campo “spedizione”
   */
  recapitoIndirizzo?: string;
  /**
   * se recapitoUgualeSpedizione == true -> questo valore diventa il valore del campo “spedizione”
   */
  recapitoCap?: string;
  /**
   * se recapitoUgualeSpedizione == true -> questo valore diventa il valore del campo “spedizione”
   */
  recapitoCivico?: string;
  /**
   * se recapitoUgualeSpedizione == true -> questo valore diventa il valore del campo “spedizione”
   */
  recapitoProvincia?: string;
  recapitoScala?: string;

  /**
   * Accetta i valori: 'Cellulare' | 'Email' | 'Telefono_Fisso', ma per noi è fisso a 'Email'.
   */
  canaleDicontattoPreferito: 'Cellulare' | 'Email' | 'Telefono_Fisso'; // Email
  /**
   * Accetta i valori: 'Dalle_09_alle_12' | 'Dalle_12_alle_16' | 'Dalle_16_alle_18' | 'Dalle_18_alle_21' | 'SABATO', ma per noi è fisso a 'Dalle_18_alle_21'.
   */
  fasciaOrarioDiRicontatto: 'Dalle_09_alle_12' | 'Dalle_12_alle_16' | 'Dalle_16_alle_18' | 'Dalle_18_alle_21' | 'SABATO';
  /**
   * Accetta i valori: 'Posta_Elettronica' | 'Posta_Tradizionale', ma per noi è fisso a 'Posta_Elettronica'.
   */
  canaleInvioComunicazionePreferito: 'Posta_Elettronica' | 'Posta_Tradizionale';
  /**
   * Accetta i valori: 'Registrazione Telefonica' | 'Conferma scritta', ma per noi è fisso a 'Registrazione Telefonica'.
   */
  modalitaConfermaAdesione: 'Registrazione Telefonica' | 'Conferma scritta';
  modalitaDiPagamento: 'Bollettino_Postale' | 'RID' | 'Carta_Di_Credito';

  /**
   * invio 1 se modalità_spedizione= email, altrimenti invio 0
   */
  bollettaElettronica?: boolean;
  sepaIban: string;
  intestatarioContodiverso: boolean;
  nomeIntestatarioconto: string;
  cognomeIntestatarioConto:string;
  codicefiscaleIntestatarioConto:string;
  subagenzia: string;
  modalitaInvioLinkform: 'sms' | 'email';

  tipoAbitazione?: 'Casa' | 'Appartamento';
  impiantoConforme?: '1' | 'SI' | 'NO';

  informativaSullaPrivacy: boolean;
  consenso1: boolean;
  consenso2: boolean;
  consenso3: boolean;

  ripensamento: boolean;
};

export type EdisonPayload = EdisonPayloadBase & Partial<EdisonPayloadLuce> & Partial<EdisonPayloadGas>;
