import { Page } from 'puppeteer';
import Edison from './Edison';
import { EdisonPayload } from './payload/EdisonPayload';
import { ScraperResponse } from '../scraper';
import createChildPayloadAwareLogger from '../../utils/createChildPayloadAwareLogger';
import { Logger } from '../../logger';
import waitForSelectorAndClick from '../../browser/page/waitForSelectorAndClick';
import waitForNavigation from '../../browser/page/waitForNavigation';
import GenericSuccessResponse from '../../response/GenericSuccessResponse';
import tryOrThrow from '../../utils/tryOrThrow';
import waitForSelectorAndSelect from '../../browser/page/waitForSelectorAndSelect';

export default class EdisonFornitori extends Edison {
  async vaiAllaPaginaFornitura(page: Page, logger: Logger, payload: EdisonPayload): Promise<void> {
    logger.info("Navigazione verso la pagina per l'inserimento.");

    await tryOrThrow(
      () => page.goto('https://c.eu25.visual.force.com/apex/AnagraficaCliente', { waitUntil: 'networkidle2' }),
      'Non sono riuscito ad aprire la pagina di inserimento contratto'
    );

    logger.info('Compilazione anagrafica e offerta.');

    await tryOrThrow(async () => {
      await this.sezioneClienteOfferta(page, logger, payload);
      await this.checkValidation(page, logger);
    }, "non sono riuscito ad inserire i dati dell'offerta");

    await tryOrThrow(async () => {
      await this.sezioneAnagraficaCliente(page, logger, payload);
      await this.checkValidation(page, logger);
    }, 'non sono riuscito ad inserire i dati del cliente');

    logger.info('Click su conferma anagrafica.');
    await tryOrThrow(async () => {
      await this.clickAndWaitLoading(page, 'input[name$=":ConfermaTestata2"]');
      await this.checkValidation(page, logger);
      await this.goToNextStep(page, Edison.config.steps.fornituraEComm);
    }, 'non sono riuscito ad accedere alla pagina `dati fornitura e comunicazioni`');
  }

  private async estraiFornitori(page: Page): Promise<string[]> {
    const list: string[] = await page.evaluate(() => {
      const autocompleteElement = $('input[id$="FornitoreAttuale"]');
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      const source = (<any> autocompleteElement)?.autocomplete('option', 'source');
      return source;
    });
    return list;
  }

  async scrapeWebsite(args: EdisonPayload): Promise<ScraperResponse> {
    const logger = createChildPayloadAwareLogger(this.childLogger, args);
    const page = await this.p();

    await tryOrThrow(() => this.vaiAllaPaginaFornitura(page, logger, args), 'step navigazione');

    await tryOrThrow(() => Promise.all([
      waitForSelectorAndClick(page, 'input[value="Aggiungi COMMODITY"]'),
      waitForNavigation(page)
    ]), 'non sono riuscito a selezionare la commodity');

    await tryOrThrow(
      () => waitForSelectorAndSelect(page, 'select[name$="tipoAttivazione"]', [args.tipoAttivazione]),
      'non sono riuscito a selezionare il tipo di attivazione'
    );

    logger.info('estraggo i fornitori');
    const fornitori = await tryOrThrow(() => this.estraiFornitori(page), 'non sono riuscito ad estrarre i fornitori');

    logger.info(`estratti ${fornitori.length} fornitori ${args.tipoOfferta}`);

    return new GenericSuccessResponse(fornitori);
  }
}
