import { Page } from 'puppeteer';
import Edison from './Edison';
import { EdisonCheckFirmePayload } from './payload/EdisonCheckFirmePayload';
import { EdisonPayload } from './payload/EdisonPayload';
import { ScraperResponse } from '../scraper';
import createChildPayloadAwareLogger from '../../utils/createChildPayloadAwareLogger';
import waitForVisible from '../../browser/page/waitForVisible';
import tryOrThrow from '../../utils/tryOrThrow';
import { STATO_CONTRATTO } from '../../contratti/StatoContratto';
import waitForSelectorAndTypeEvaluated from '../../browser/page/waitForSelectorAndTypeEvaluated';
import waitForSelectorAndClick from '../../browser/page/waitForSelectorAndClick';
import waitForNavigation from '../../browser/page/waitForNavigation';
import GenericSuccessResponse from '../../response/GenericSuccessResponse';
import { Logger } from '../../logger';

const SEARCH_INPUT_SELECTOR = '#phSearchInput';

async function checkFirmaContratto(page: Page, codiceContratto: string, logger: Logger): Promise<[string, string]> {
  await waitForSelectorAndTypeEvaluated(page, SEARCH_INPUT_SELECTOR, '');
  await waitForSelectorAndTypeEvaluated(page, SEARCH_INPUT_SELECTOR, codiceContratto);
  await Promise.all([
    waitForSelectorAndClick(page, '#phSearchButton'),
    waitForNavigation(page)
  ]);

  let text;
  try {
    text = await page.$eval(
      '.bRelatedList > .listRelatedObject > .bPageBlock > .pbBody > .list > tbody > tr.dataRow.even.last.first > td:nth-child(9)',
      (el) => el.textContent?.trim()
    );
  } catch (error) {
    // tabella non apparsa
    // faccio in modo che venga impostato lo stato 'DA FIRMARE' visto che non ho trovato il record
    text = '';
  }
  const statoContratto = text?.toLowerCase() === 'inviato a crm' ? STATO_CONTRATTO.FIRMATO : STATO_CONTRATTO.DA_FIRMARE;

  logger.info(`Per il contratto ${codiceContratto} recuperato lo stato (edison) '${text}', convertito in '${statoContratto}'`);

  return [codiceContratto, statoContratto];
}

async function checkFirmaContratti(page: Page, codiciContratto: string[], logger: Logger): Promise<[string, string][]> {
  let result = [];
  const codiceContratto = codiciContratto.pop();
  if (codiceContratto) {
    result.push(await checkFirmaContratto(page, codiceContratto, logger));
  }

  if (codiciContratto.length) {
    result = result.concat(await checkFirmaContratti(page, codiciContratto, logger));
  }

  return result;
}

export default class EdisonCheckFirme extends Edison {
  /**
   * @override
   * @param payload Payload per il check stato firme
   */
  async scrapeWebsite(payload: EdisonCheckFirmePayload & EdisonPayload): Promise<ScraperResponse> {
    const logger = createChildPayloadAwareLogger(this.childLogger, payload);
    const page = await this.p();

    await tryOrThrow(() => waitForVisible(page, SEARCH_INPUT_SELECTOR), 'la pagina per controllare lo stato delle firme non si è caricata in tempo');

    const result = await checkFirmaContratti(page, payload.codici, logger);

    const mapped = result.reduce((acc, [codice, stato]) => {
      acc[codice] = stato;
      return acc;
    }, Object.create(null));

    return new GenericSuccessResponse<Record<string, string>>(mapped);
  }
}
