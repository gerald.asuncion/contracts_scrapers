import { join } from "path";
import { Page } from 'puppeteer';
import { S3ClientFactoryResult } from '../aws/s3ClientFactory';
import BrowserContextManager from '../browser/BrowserContextManager';
import BrowserManager from '../browser/BrowserManager';
import saveScreenshotOnAwsS3 from '../browser/page/saveScreenshotOnAwsS3';
import getCredenzialiDalServizio, { BasicCredenzialiPayload, Credenziali } from '../credenziali/getCredenzialiDalServizio';
import { CertificateOptions, onRequestProxyHandlerFactory } from '../credenziali/getScraperCertificato';
import { Logger } from '../logger';
import { NoSubmitPayload } from '../payloads/types';
import SupermoneyResponse from '../response/SupermoneyResponse';
import checkNoSubmit from '../utils/checkNoSubmit';
import createChildLogger from '../utils/createChildLogger';
import createFilenameForScreenshot from '../utils/createFilenameForScreenshot';
import extractErrorMessage from '../utils/extractErrorMessage';
import delayedRetry from '../utils/retry';
import parseScraperError from './parseScraperError';
import ScraperOptions, { ScraperTipologia } from './ScraperOptions';
import timeoutPromise from './timeoutPromise';
import { Scraper } from './types';

export abstract class WebScraper<T extends BasicCredenzialiPayload & Record<string, any> = {}> implements Scraper {
  protected browser: BrowserManager;

  protected logger: Logger;

  protected page: Page | undefined;

  protected childLogger: Logger;

  private browserContext: BrowserContextManager | null;

  protected s3ClientFactory: S3ClientFactoryResult;

  protected credenzialiServiceBaseUrl: string;

  protected baseDownloadPath: string;

  protected certificatiClientBasePath: string;

  protected certificatoClientConfig: CertificateOptions | undefined;
  protected disableInterceptors = () => Promise.resolve();

  constructor({ browser, logger, s3ClientFactory, credenzialiServiceBaseUrl, certificatiClientBasePath, baseDownloadPath }: ScraperOptions) {
    this.browser = browser;
    this.logger = logger;
    this.childLogger = createChildLogger(logger, this.constructor.name);
    this.browserContext = null;
    this.s3ClientFactory = s3ClientFactory;
    this.credenzialiServiceBaseUrl = credenzialiServiceBaseUrl;
    this.baseDownloadPath = baseDownloadPath;
    this.certificatiClientBasePath = certificatiClientBasePath;
  }

  async getBrowserContext(): Promise<BrowserContextManager> {
    if (!this.browserContext) {
      this.browserContext = await this.browser.createIncognitoBrowserContext();
    }

    return this.browserContext;
  }

  getLoginTimeout(): number {
    return 30000;
  }

  async newPage(): Promise<Page> {
    return (await this.getBrowserContext()).newPage();
  }

  p(): Promise<Page> {
    if (!this.page) {
      return this.newPage();
    }
    return Promise.resolve(this.page);
  }

  checkPageClosed(): void {
    const { page } = this;
    if (!page) {
      throw new Error('Page is closed');
    }
  }

  abstract login(args: T | undefined): Promise<void>;

  abstract scrapeWebsite(args: T): Promise<ScraperResponse>;

  private async closeBrowserContext(): Promise<void> {
    await (await this.p()).close();

    await (await this.getBrowserContext()).close();
    this.browserContext = null;
    this.page = undefined;
  }

  async callScrapeWebsite(page: Page, args: any): Promise<ScraperResponse> {
    let result;

    try {
      const timeoutError = timeoutPromise('Timeout', this.getLoginTimeout());

      await timeoutError(this.login(args));
      result = await this.scrapeWebsite(args);
    } catch (ex: any) {
      this.childLogger.error(extractErrorMessage(ex));
      result = parseScraperError(ex);
      let filename = `${this.constructor.name}-errore`;
      if (args.idDatiContratto) {
        filename = `${args.idDatiContratto}-${filename}`;
      }
      try {
        await this.saveScreenshot(page, this.childLogger, `${this.constructor.name}-errore`, args.idDatiContratto);
      } catch (screenshotException) {
        const seMsg = extractErrorMessage(screenshotException);
        this.childLogger.error(seMsg);
      }
    }

    return result;
  }

  protected async enableInterceptors(force: boolean = false) {
    if (this.certificatoClientConfig) {
      const {
        path: relPath,
        passphrase,
        manual,
        additionalHeaders
      } = this.certificatoClientConfig as any as CertificateOptions<'p12'>;

      if (!force && manual) {
        return;
      }

      const certificatePath = join(this.certificatiClientBasePath, relPath);

      this.disableInterceptors = await onRequestProxyHandlerFactory(this.page!, this.childLogger || this.logger, certificatePath, passphrase, additionalHeaders);
    }
  }

  async scrape(args: any): Promise<ScraperResponse> {
    this.page = await this.newPage();

    await this.enableInterceptors(false);

    const result = await this.callScrapeWebsite(this.page, args);

    await this.closeBrowserContext();

    return result as ScraperResponse;
  }

  public static isScraperResponse(a: unknown): a is ScraperResponse {
    return !!(a as ScraperResponse).code;
  }

  public static delay(time: number): Promise<void> {
    return new Promise((resolve) => {
      setTimeout(resolve, time);
    });
  }

  checkNoSubmit(payload: T & NoSubmitPayload): void {
    checkNoSubmit(payload);
  }

  async saveScreenshot(page: Page, logger: Logger, name: string, idDatiContratto?: number): Promise<void> {
    const filename = createFilenameForScreenshot(name, idDatiContratto);
    // 12452 se il portale non carica manco la login poi la generazione dello screenshot (page::screenshot) rimane appesa in eterno
    // quindi inserisco un timeout per far fermare tutto e rispondere al chiamante
    const timer = timeoutPromise('Screenshot timeout', 3000);
    await timer(saveScreenshotOnAwsS3(this.s3ClientFactory, page, filename, logger));
  }

  abstract getScraperCodice(): string;
  getScraperCodici(): string[] { return []; }

  abstract getScraperTipologia(): ScraperTipologia;

  getCredenziali(payload?: BasicCredenzialiPayload, overrideFromPayload: boolean = false): Promise<Credenziali> {
    if (overrideFromPayload && payload && payload.username && payload.password) {
      this.logger.info('Credenziali recuperate dal payload!');

      return Promise.resolve({
        username: payload.username,
        password: payload.password
      });
    }

    return delayedRetry(() => getCredenzialiDalServizio(this.childLogger || this.logger, this.credenzialiServiceBaseUrl, this.getScraperCodice(), payload), {
      infoMessage: `credentials service at '${this.credenzialiServiceBaseUrl}' with code '${this.getScraperCodice()}!`
    }, this.childLogger || this.logger);
  }

  getCredenzialiLista(payload?: BasicCredenzialiPayload): Promise<Credenziali[]> {
    return Promise.all(this.getScraperCodici().map(codice => delayedRetry(() => getCredenzialiDalServizio(this.childLogger || this.logger, this.credenzialiServiceBaseUrl, codice, payload), {
      infoMessage: `credentials service at '${this.credenzialiServiceBaseUrl}' with code '${codice}'!`
    }, this.childLogger || this.logger)));
  }
}

export type ScraperResponse = SupermoneyResponse<any>;
