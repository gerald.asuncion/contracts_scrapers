import TimeoutError from '../errors/TimeoutError';

const timeoutPromise = <T = any>(error: string | Error, timeout: number) => (promise: Promise<T>): Promise<T | undefined> => {
  let errTimeout: NodeJS.Timeout;

  return Promise.race([
    promise
      // is syncronous
      .finally(() => clearTimeout(errTimeout)),
    new Promise<undefined>((_, reject) => {
      errTimeout = setTimeout(reject, timeout, typeof error === 'string' ? new TimeoutError(error) : error);
    })
  ]);
};

export default timeoutPromise;
