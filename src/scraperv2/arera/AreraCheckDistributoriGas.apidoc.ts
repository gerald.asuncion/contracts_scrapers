/**
 * @openapi
 *
 * /scraper/v2/arera/gas/distributori/check:
 *  post:
 *    tags:
 *      - v2
 *    description: Va sul portale di arera per trovare i distributori nella città passata. Dopodiché chiama tutti gli scraper disponibili per quei distributori trovati.
 *    consume:
 *      - application/json
 *    produces:
 *      - application/json
 *    responses:
 *      200:
 *        description: SuccessResponse
 *    parameters:
 *      - in: body
 *        description:
 *          pdr e indirizzo da controllare.
 *          L'indirizzo serve per trovare i distributori di quella zona, poi viene passato tutto il payload ai relativi scraper per il controllo.
 *        schema:
 *          type: object
 *          required:
 *            - tipoContratto
 *            - pdr
 *            - regione
 *            - provincia
 *            - comune
 *          properties:
 *            tipoContratto:
 *              type: string
 *            pdr:
 *              type: string
 *            regione:
 *              type: string
 *            provincia:
 *              type: string
 *            comune:
 *              type: string
 *
 * /scraper/queue/areraCheckDistributoriGas:
 *  post:
 *    tags:
 *      - v2
 *      - queue
 *    description:
 *      Inserisce in queue il task per effettuare il check tramite lo scraper AreraCheckDistributoriGas.
 *      Il payload è lo stesso della rotta diretta `/scraper/v2/arera/gas/distributori/check`,
 *      in più va aggiunto il webhook che lo scraper poi andrà a richiamare per notificare l'esito.
 *    consume:
 *      - application/json
 *    produces:
 *      - application/json
 *    responses:
 *      200:
 *        description: SuccessResponse
 */
