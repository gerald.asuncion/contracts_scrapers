import { ScraperResponse, WebScraper } from '../scraper';
import AreraRepo from '../../repo/AreraRepo';
import { QueueTask } from '../../task/QueueTask';
import { AwilixContainer } from 'awilix';
import { AreraCheckDistributoriGasPayload, AreraOptions } from './types';
import { SCRAPER_TIPOLOGIA } from '../ScraperOptions';
import recuperaListaDistributori from './helpers/recuperaListaDistributori';
import { getUnifiedResponseFromMap } from './helpers/creaMultipleResponse';
import { recuperaEsitiDistributori } from './helpers/recuperaEsitiDistributori';
import getNomeScraperDaNomeDistributore from './helpers/getNomeScraperDaNomeDistributore';
import SuccessResponse from '../../response/SuccessResponse';

@QueueTask()
export default class AreraCheckDistributoriGas extends WebScraper<AreraCheckDistributoriGasPayload> {
  private areraRepo: AreraRepo;
  private cradle: AwilixContainer['cradle'];

  constructor(options: AreraOptions) {
    super(options);
    this.areraRepo = options.areraRepo;
    this.cradle = options;
  }

  login(): Promise<void> {
    return Promise.resolve();
  }

  async scrapeWebsite(payload: AreraCheckDistributoriGasPayload): Promise<ScraperResponse> {
    const logger = this.childLogger;

    logger.info(`eseguo il controllo per il payload ${JSON.stringify(payload)}`);

    let [distributori, err] = await recuperaListaDistributori({
      getPage: () => this.p(),
      logger,
      areraRepo: this.areraRepo,
      payload
    });

    if (err) {
      return err;
    }

    distributori = distributori || [];

    logger.info(`chiamo gli scrapers per i distributori ${distributori.join(', ')}`);

    const esiti = await recuperaEsitiDistributori({
      logger,
      payload,
      distributori: distributori.slice(0),
      cradle: this.cradle,
      getNomeScraperDaNomeDistributore: getNomeScraperDaNomeDistributore,
    });

    const result = getUnifiedResponseFromMap(esiti, distributori);

    if (result instanceof SuccessResponse) {
      logger.info(`Success ${JSON.stringify(result)}`);
    } else {
      logger.error(`Ritorno l'errore ${JSON.stringify(result)}`);
    }

    return result;
  }

  getScraperCodice() {
    return 'arera';
  }

  getScraperTipologia() {
    return SCRAPER_TIPOLOGIA.FATTIBILITA;
  }
}
