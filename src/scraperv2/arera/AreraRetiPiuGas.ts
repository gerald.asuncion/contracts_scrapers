import RetiPiuGas from '../terranova/reti-piu-gas';
import { ScraperResponse } from '../scraper';
import FailureResponse from '../../response/FailureResponse';
import SuccessResponse from '../../response/SuccessResponse';
import extractErrorMessage from '../../utils/extractErrorMessage';

interface AreraRetiPiuGasPayload {
  pdr: string;
}

export default class AreraRetiPiuGas extends RetiPiuGas {
  /**
   * @override
   * @param {AreraRetiPiuGasPayload} args Request payload
   */
  async scrapeWebsite(args: AreraRetiPiuGasPayload): Promise<ScraperResponse> {
    let response = await super.scrapeWebsite(args);

    if (response instanceof SuccessResponse) {
      const indirizzo = await this.getIndirizzoVisualizzato();
      if (indirizzo) {
        response = new SuccessResponse(indirizzo);
      }
    } else if (response instanceof FailureResponse) {
      if (response.details.includes('PDR trovati: 0')) {
        response = new FailureResponse('ko-pdr non contendibile');
      }
    }

    return response;
  }

  async getIndirizzoVisualizzato():Promise<string> {
    let indirizzo = '';
    try {
      const page = await this.p();
      await page.waitForSelector('#twsTemplate_Content2_twsModule_TestataPDP');

      const comune = await page.$eval(
        '#twsTemplate_Content2_twsModule_TestataPDP > tbody > tr > td > div > table > tbody > tr.trRptItem > td:nth-child(2)',
        (el) => el.textContent?.trim()
      );
      const via = await page.$eval(
        '#twsTemplate_Content2_twsModule_TestataPDP > tbody > tr > td > div > table > tbody > tr.trRptItem > td:nth-child(3)',
        (el) => el.textContent?.trim()
      );
      const potenza = await page.$eval(
        '#twsTemplate_Content2_twsModule_TestataPDP > tbody > tr > td > div > table > tbody > tr.trRptItem > td:nth-child(4)',
        (el) => el.textContent?.trim()
      );

      indirizzo = `${comune} ${via} - Portata termica: ${potenza}`;
    } catch (ex) {
      this.logger.error(`non sono riuscito a recuperare l'indirizzo - ${extractErrorMessage(ex)}`);
    }

    return indirizzo;
  }
}
