import AreraRepo from "../../repo/AreraRepo";
import ScraperOptions from "../ScraperOptions";

export type AreraDistributore = {
  data_aggiornamento: number;
  id: number;
  codice_istat_comune: string;
  comune: string;
  distributore: string;
};

export type AreraPortaleInfo = {
  distributori: string[];
  nomeComuneTrovato: string;
};

export type AreraCheckDistributoriGasPayload = {
  tipoContratto: string;
  pdr: string;
  comune: string;
  regione: string;
  provincia: string;
};

export type AreraOptions = ScraperOptions & { areraRepo: AreraRepo };

export type AreraPayload = Omit<AreraCheckDistributoriGasPayload, 'tipoContratto'>;
