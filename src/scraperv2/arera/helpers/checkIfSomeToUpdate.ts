export default function checkIfSomeToUpdate(distributori: { data_aggiornamento: number; }[]): boolean {
  const DATA_30_GIORNI_FA = new Date().getTime() - (30 * 24 * 60 * 60 * 1000);
  return !!distributori.find(({ data_aggiornamento }) => data_aggiornamento * 1000 < DATA_30_GIORNI_FA);
}
