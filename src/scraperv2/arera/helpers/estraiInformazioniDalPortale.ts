import { Page } from 'puppeteer';
import { AreraPayload, AreraPortaleInfo } from '../types';
import { Logger } from '../../../logger';
import FailureResponse from '../../../response/FailureResponse';
import waitForVisible from '../../../browser/page/waitForVisible';
import waitForSelectorAndClick from '../../../browser/page/waitForSelectorAndClick';
import selectByFindOption from '../../eni/inserimentocontratto/selectByFindOption';
import ErrorResponse from '../../../response/ErrorResponse';
import waitForSelectorAndSelect from '../../../browser/page/waitForSelectorAndSelect';
import waitForNavigation from '../../../browser/page/waitForNavigation';
import tryOrThrow from '../../../utils/tryOrThrow';
import waitForSelectorAndClickEvaluated from '../../../browser/page/waitForSelectorAndClickEvaluated';

export default async function estraiInformazioniDalPortale(
  page: Page,
  payload: AreraPayload,
  logger: Logger
): Promise<AreraPortaleInfo | FailureResponse | ErrorResponse> {
  const { comune, regione, provincia } = payload;

  await page.goto('https://www.arera.it/ModuliDinamiciPortale/elencooperatori/elencoOperatoriHome', { waitUntil: 'networkidle2' });

  // inserisco i dati per ottenere la lista di distributori
  await waitForVisible(page, '#contains-all');

  await tryOrThrow(() => waitForSelectorAndClickEvaluated(page, '#CKG_2'), 'non sono riuscito a cliccare sul checkbox `Distributori`:');

  try {
    await waitForVisible(page, '#cercaPerComuneComboRegione');
    await page.focus('#cercaPerComuneComboRegione');
    await Promise.all([
      waitForSelectorAndSelect(page, '#cercaPerComuneComboRegione', [regione]),
      page.waitForResponse(() => true)
    ]);
  } catch (ex) {
    return new ErrorResponse('La regione indicata non è valida');
  }

  try {
    await Promise.all([
      selectByFindOption(page, '#cercaPerComuneComboProvincia', provincia, 'provincia'),
      page.waitForResponse(() => true)
    ]);
  } catch (ex) {
    return new ErrorResponse('La provincia indicata non è valida');
  }

  try {
    await waitForSelectorAndSelect(page, '#cercaPerComuneComboComune', [comune]);
  } catch (ex) {
    return new ErrorResponse('Il comune indicato non è valido');
  }

  await Promise.all([
    waitForSelectorAndClick(page, '#butSub'),
    waitForNavigation(page)
  ]);

  // recupero la lista di distributori ed il nome del comune
  try {
    await waitForVisible(page, '#contains-all');
    await waitForVisible(page, '#item'); // la tabella
  } catch (ex) {
    const noDatiErrorMsg = `non ci sono distributori in ${regione} ${provincia} ${comune}`;
    // tabella non renderizzata
    logger.error(noDatiErrorMsg);
    return new ErrorResponse(noDatiErrorMsg);
  }

  const nomeComuneTrovato = await page.evaluate(
    () => $('#formExport > table.align_center > tbody > tr:nth-child(3) > td > strong').html()
  );

  const distributori = await page.evaluate(() => {
    const elements = Array.from(document.querySelectorAll<HTMLTableCellElement>('#item > tbody > tr > td:first-child'));

    return elements.length ? elements.map(({ textContent }) => textContent?.trim() || '') : [];
  });

  return { distributori, nomeComuneTrovato };
}
