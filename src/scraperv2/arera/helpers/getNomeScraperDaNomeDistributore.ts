const map: Record<string, string> = {
  // 'RETIPIÙ SRL': 'areraRetiPiuGas',
  'ITALGAS RETI S.P.A.': 'italgasContendibilitaGas',
  'UNARETI SPA': 'unaretiPdrChecker',
  'TOSCANA ENERGIA S.P.A.': 'italgasToscana',
  CENTRIA: 'centriaPdrCheck',
  'EROGASMET S.P.A.': 'erogasmet',
  '2I RETE GAS S.P.A.': 'dueIReteGas',
  'IRETI S.P.A.': 'iretiCheckPdr',
  'LERETI S.P.A.': 'leretiCheckPdr'
};

export default function getNomeScraperDaNomeDistributore(distributore: string): string | undefined {
  return map[distributore];
}
