import ErrorResponse from "../../../response/ErrorResponse";
import MultipleErrorResponse from "../../../response/MultipleErrorResponse";
import MultipleResponse from "../../../response/MultipleResponse";
import SuccessResponse from "../../../response/SuccessResponse";
import SupermoneyResponse from "../../../response/SupermoneyResponse";
import { ScraperResponse } from "../../scraper";

function mapResponse(response: SupermoneyResponse<unknown>): SupermoneyResponse<unknown> {
  if (response.code === '03') {
    return new ErrorResponse(response.details as string);
  }

  return response;
}

function needErrorResponse(risposte: ScraperResponse[]) {
  return risposte.find((item) => ['05', '03'].includes(item.code));
}

function creaMultipleResponse(risposteScrapers: ScraperResponse[], primoDistributore: string, needExtra = false): SupermoneyResponse<SupermoneyResponse<unknown>[]> {
  const risposte = risposteScrapers.map(mapResponse);

  if (needErrorResponse(risposte)) {
    return new MultipleErrorResponse(risposte, needExtra ? { distributore: primoDistributore } : undefined);
  }

  return new MultipleResponse(risposte, needExtra ? { distributore: primoDistributore } : undefined);
}

const isSuccessResponse = (response: ScraperResponse) => response instanceof SuccessResponse;

export function getUnifiedResponseFromMap(results: Record<string, ScraperResponse>, listaDistributoriOrdinata: string[], needExtra = false) {
  let primoDistributore = listaDistributoriOrdinata[0];

  const responses: ScraperResponse[] = [];

  for (const distributore in results) {
    const result = results[distributore];

    if (isSuccessResponse(result)) {
      if (needExtra) {
        return new SuccessResponse(result.details, { ...result.extra, distributore });
      }

      return result;
    }

    responses.push(result);
  }

  return creaMultipleResponse(responses, primoDistributore, needExtra);
}
