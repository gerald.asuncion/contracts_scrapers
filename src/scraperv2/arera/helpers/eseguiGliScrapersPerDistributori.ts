import { ScraperResponse } from '../../scraper';
import { ScraperResult, ScraperInterface } from '../../../scrapers/ScraperInterface';
import { Logger } from '../../../logger';
import { AreraPayload } from '../types';
import ErrorResponse from '../../../response/ErrorResponse';
import { AwilixContainer } from 'awilix';

async function lanciaScraperAssociatoADistributore(
  distributore: string,
  payload: AreraPayload,
  logger: Logger,
  cradle: AwilixContainer['cradle'],
  getNomeScraperDaNomeDistributore: (distributore: string) => string | undefined,
): Promise<ScraperResult | ScraperResponse> {
  const checkerDaLanciare = getNomeScraperDaNomeDistributore(distributore,);

  if (!checkerDaLanciare) {
    logger.info(`Nessun scraper trovato per il distributore ${distributore}`);
    // eslint-disable-next-line max-len
    return new ErrorResponse(`Il Controllo integrato per il distributore '${distributore}' non è presente. Verificare il punto di fornitura col back-office direttamente sul portale del distributore.`);
  }

  logger.info(`Istanzio lo scraper ${checkerDaLanciare} associato al distributore ${distributore}`);
  const scraper = cradle[checkerDaLanciare] as ScraperInterface;

  logger.info(`Lancio lo scraper ${checkerDaLanciare} associato al distributore ${distributore}`);
  const response = await scraper.scrape(payload);
  logger.info(`Per il distributore ${distributore} dallo scraper ${checkerDaLanciare} ottenuta la risposta ${JSON.stringify(response)}`);

  return response;
}

export default async function* eseguiGliScrapersPerDistributori(
  distributori: string[],
  payload: AreraPayload,
  logger: Logger,
  cradle: AwilixContainer['cradle'],
  getNomeScraperDaNomeDistributore: (distributore: string) => string | undefined,
): AsyncGenerator<{ response: ScraperResult | ScraperResponse; nomeDistributore: string; }> {
  const nomeDistributore = distributori.pop();

  if (nomeDistributore) {
    const response = await lanciaScraperAssociatoADistributore(nomeDistributore, payload, logger, cradle, getNomeScraperDaNomeDistributore);
    yield { response, nomeDistributore };
  }

  if (distributori.length) {
    yield* eseguiGliScrapersPerDistributori(distributori, payload, logger, cradle, getNomeScraperDaNomeDistributore);
  }
}
