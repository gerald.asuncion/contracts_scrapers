import FailureResponse from '../../../response/FailureResponse';
import SuccessResponse from '../../../response/SuccessResponse';
import ErrorResponse from '../../../response/ErrorResponse';

export default function aggiungiDistributoreAllaDetails(response: any, nomeDistributore: string): any {
  if (response instanceof FailureResponse || response instanceof SuccessResponse || response instanceof ErrorResponse) {
    let currDetails = response.details;
    if (response instanceof SuccessResponse) {
      currDetails = response.details ? response.details : 'OK';
    }

    const newDetails = `${nomeDistributore} ${currDetails}`;

    if (response instanceof FailureResponse) {
      return new FailureResponse(newDetails);
    }

    if (response instanceof ErrorResponse) {
      return new ErrorResponse(newDetails);
    }

    if (response instanceof SuccessResponse) {
      return new SuccessResponse(newDetails, response.extra);
    }
  }
  return response;
}
