import { AwilixContainer } from "awilix";
import { Logger } from "../../../logger";
import { ScraperResponse } from "../../scraper";
import { AreraPayload } from "../types";
import aggiungiDistributoreAllaDetails from "./aggiungiDistributoreAllaDetails";
import eseguiGliScrapersPerDistributori from "./eseguiGliScrapersPerDistributori";

export type RecuperaEsitiDistributoriContext = {
  logger: Logger;
  distributori: string[];
  payload: AreraPayload;
  cradle: AwilixContainer['cradle'];
  getNomeScraperDaNomeDistributore: (distributore: string) => string | undefined;
};

export async function recuperaEsitiDistributori({
  logger,
  distributori,
  payload,
  cradle,
  getNomeScraperDaNomeDistributore,
}: RecuperaEsitiDistributoriContext) {
  const esito: Record<string, ScraperResponse> = {};
  for await (const { response, nomeDistributore } of eseguiGliScrapersPerDistributori(distributori, payload, logger, cradle, getNomeScraperDaNomeDistributore)) {
    const scraperResult = aggiungiDistributoreAllaDetails(response, nomeDistributore);
    esito[nomeDistributore] = scraperResult;
  }

  return esito;
}
