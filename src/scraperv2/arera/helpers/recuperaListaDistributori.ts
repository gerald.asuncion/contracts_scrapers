import uniq from 'lodash/uniq';
import { Page } from "puppeteer";
import { Logger } from "../../../logger";
import AreraRepo from "../../../repo/AreraRepo";
import ErrorResponse from "../../../response/ErrorResponse";
import FailureResponse from "../../../response/FailureResponse";
import { AreraPayload } from "../types";
import checkIfSomeToUpdate from "./checkIfSomeToUpdate";
import estraiInformazioniDalPortale from "./estraiInformazioniDalPortale";

export type RecuperaListaDistributoriContext = {
  getPage: () => Promise<Page>;
  logger: Logger;
  areraRepo: AreraRepo;
  payload: AreraPayload;
};

export default async function recuperaListaDistributori({
  getPage,
  logger,
  areraRepo,
  payload
}: RecuperaListaDistributoriContext): Promise<[string[], null] | [null, FailureResponse]> {
  const { comune, regione, provincia } = payload;
  // distributori attualmente salvati su database
  const distributoriCorrenti = await areraRepo.estraiFornitoreDaCodiceIstatComune(comune);
  const isSomeToUpdate = checkIfSomeToUpdate(distributoriCorrenti);

  let nomeComuneTrovato: string;
  let distributoriSulPortale: string[] = [];
  if (!distributoriCorrenti.length || isSomeToUpdate) {
    // recupero la lista di distributori sul portale, ed anche il nome del comune
    const info = await estraiInformazioniDalPortale(await getPage(), payload, logger);

    if (info instanceof FailureResponse || info instanceof ErrorResponse) {
      return [null, info];
    }

    distributoriSulPortale = info.distributori;

    if (!distributoriSulPortale.length) {
      logger.error(`nessun distributore trovato sul portale per ${regione} ${provincia} ${comune}`);
      return [null, new FailureResponse('Tabella senza elementi')];
    }

    nomeComuneTrovato = info.nomeComuneTrovato;
  }

  if (distributoriSulPortale.length) {
    if (isSomeToUpdate) {
      // aggiorno il database
      await Promise.all([
        distributoriCorrenti.map(({ id, distributore }) => {
          const idx = distributoriSulPortale.indexOf(distributore);

          if (idx !== -1) {
            distributoriSulPortale.splice(idx, 1);
            logger.info(`aggiorno il record nel database del distributore ${distributore}`);
            return areraRepo.aggiornaDistributore(id.toString(), distributore);
          }

          return null;
        }).filter(Boolean)
      ]);
    }

    if (distributoriSulPortale.length) {
      // salvo a database i rimanenti
      logger.info(`salvo nel database i distributori ${distributoriSulPortale.join(', ')}`);
      await Promise.all(
        distributoriSulPortale.map((nomeDistributore) => areraRepo.aggiungi(comune, nomeComuneTrovato, nomeDistributore))
      );
    }
  }

  const distributori = uniq(
    distributoriSulPortale.concat(
      distributoriCorrenti.map(({ distributore }) => distributore)
    )
  );

  return [distributori, null];
}
