import evaluateXPathAndClick from '../../browser/page/evaluateXPathAndClick';
import getElementText from '../../browser/page/getElementText';
import waitForNavigation from '../../browser/page/waitForNavigation';
import waitForSelectorAndClick from '../../browser/page/waitForSelectorAndClick';
import waitForSelectorAndType from '../../browser/page/waitForSelectorAndType';
import FailureResponse from '../../response/FailureResponse';
import SuccessResponse from '../../response/SuccessResponse';
import tryOrThrow from '../../utils/tryOrThrow';
import { ScraperResponse, WebScraper } from '../scraper';
import CentriaPayload from './payload/CentriaPayload';
import waitCheckCredentials from '../../browser/page/waitCheckCredentials';
import { SCRAPER_TIPOLOGIA } from '../ScraperOptions';
import { QueueTask } from '../../task/QueueTask';
import { Page } from 'puppeteer';
import { Logger } from '../../logger';

@QueueTask({ scraperName: 'centria' })
export default class CentriaPdrCheck extends WebScraper<CentriaPayload> {
  static readonly LOGIN_URL = 'http://retigas.consiag.it/Portal/Index.aspx?area=b2b';

  async login(args: CentriaPayload | undefined): Promise<void> {
    const logger = this.childLogger;
    const page = await this.p();

    await page.goto(CentriaPdrCheck.LOGIN_URL, { waitUntil: 'networkidle2' });

    const { username, password } = await this.getCredenziali();

    await waitForSelectorAndType(page, '#twsTemplate_Content2_twsModule_txtUser', username);
    logger.info('login > inserito username');

    await waitForSelectorAndType(page, '#twsTemplate_Content2_twsModule_txtPSW', password);
    logger.info('login > inserita password');

    await Promise.all([
      waitForSelectorAndClick(page, '#twsTemplate_Content2_twsModule_btnLogin'),
      waitForNavigation(page)
    ]);

    logger.info('login > cliccato sul pulsante login');

    await Promise.race([
      waitCheckCredentials(page, '#twsTemplate_Content2_twsModule_lblMsg', CentriaPdrCheck.LOGIN_URL, username, logger),
      page.waitForSelector('#twsTemplate_HeaderLogin1_Mainmenu2_aspMainMenu', { timeout: 60000 })
    ]);
    logger.info('login > terminata');
  }

  async scrapeWebsite(payload: CentriaPayload): Promise<ScraperResponse> {
    const logger = this.childLogger;
    const page = await this.p();

    await this.ricercaPdr(page, logger, payload);

    const elResponse = await page.waitForSelector('#twsTemplate_Content1_twsModule_lblMsg');
    const elResponseText: string = await page.evaluate((el: HTMLSpanElement) => el.innerText || '', elResponse);
    const elResponseClassName: string = await page.evaluate((el) => el.className, elResponse);

    if (elResponseText.trim().toLowerCase().includes('pdr trovati: 0')) {
      return new FailureResponse('KO - PDR non contendibile');
    }
    if (elResponseClassName.toLowerCase().includes('error')) {
      return new FailureResponse(`KO - PDR non contendibile: ${elResponseText}`);
    }

    const selectorPre = '#twsTemplate_Content1_twsModule_TestataPDP > tbody > tr > td > div > table > tbody > tr.trRptItem >';

    const elNoteMisuratoreText: string = await getElementText(page, `${selectorPre} td:nth-child(8)`, 'innerText');

    if (elNoteMisuratoreText.includes('CHIUSO')
      && (elNoteMisuratoreText.includes('RIMOSSO') || elNoteMisuratoreText.includes('POSATO'))
    ) {
      const comune = await getElementText(page, `${selectorPre} td:nth-child(2)`, 'innerText');
      const indirizzo = await getElementText(page, `${selectorPre} td:nth-child(3)`, 'innerText');
      const potenza = await getElementText(page, `${selectorPre} td:nth-child(4)`, 'innerText');

      return new SuccessResponse(`${comune}, ${indirizzo} - Portata termica: ${potenza}`);
    }

    return new FailureResponse('KO - errore portale');
  }

  async ricercaPdr(page: Page, logger: Logger, { pdr }: CentriaPayload) {
    await Promise.all([
      evaluateXPathAndClick(page, '//a//span[normalize-space(text()) = "Ricerca Predisposizioni PDR"]/parent::a'),
      waitForNavigation(page)
    ]);

    await tryOrThrow(
      async () => waitForSelectorAndType(page, '#twsTemplate_Content1_twsModule_txtPdr', pdr),
      "Errore durante l'inserimento del campo `pdr`"
    );
    logger.info('scrape > pdr inserito');

    await Promise.all([
      waitForSelectorAndClick(page, '#twsTemplate_Content1_twsModule_btnSearchPDP'),
      waitForNavigation(page)
    ]);
  }

  getScraperCodice() {
    return 'centria-checkpdr';
  }

  getScraperTipologia() {
    return SCRAPER_TIPOLOGIA.FATTIBILITA;
  }
}
