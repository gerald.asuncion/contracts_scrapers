/**
 * @openapi
 *
 * /scraper/v2/eolo/inserimento:
 *  post:
 *    tags:
 *      - v2
 *    description:
 *      Inserisce il contratto, coi dati passati, sul portale di Eolo.
 *      Questa è la rotta diretta allo scraper, quindi non usa il meccanismo del webhook.
 *    consume:
 *      - application/json
 *    produces:
 *      - application/json
 *    responses:
 *      200:
 *        description: SuccessResponse
 *    parameters:
 *      - in: body
 *        description: i dati da inserire.
 *        schema:
 *          type: object
 *          required:
 *            - indirizzoAttivazione
 *            - tipoIndirizzoAttivazione
 *            - numeroCivicoAttivazione
 *            - comuneAttivazione
 *            - capAttivazione
 *            - tipoContratto
 *            - precedenteOperatore
 *            - usiInternet
 *            - interessiInternet
 *            - pacchetto
 *            - tipoInstallazione
 *            - ripetitoreWifi
 *            - router
 *            - intestatarioLineaTelefonicaDiversoDaQuelloDellaConnessioneEOLO
 *            - tipoIntestatario
 *            - nomeIntestatario
 *            - cognomeIntestatario
 *            - emailIntestatario
 *            - codiceFiscaleIntestatario
 *            - dataDiNascitaIntestatario
 *            - comuneDiNascitaIntestatario
 *            - sessoIntestatario
 *            - cellulareIntestatario
 *            - presenzaInstallazione
 *            - datiFatturaDiversiDaFornitura
 *            - tipoDocumento
 *            - numeroDocumento
 *            - comuneRilascioDocumento
 *            - dataRilascioDocumento
 *            - dataScadenzaDocumento
 *          properties:
 *            indirizzoAttivazione:
 *              type: string
 *            tipoIndirizzoAttivazione:
 *              type: string
 *            numeroCivicoAttivazione:
 *              type: string
 *            comuneAttivazione:
 *              type: string
 *            capAttivazione:
 *              type: string
 *            tipoContratto:
 *              type: string
 *            precedenteOperatore:
 *              type: string
 *            nomeAltroOperatore:
 *              type: string
 *              nullable: true
 *            usiInternet:
 *              type: array
 *              items:
 *                type: string
 *            interessiInternet:
 *              type: array
 *              items:
 *                type: string
 *            pacchetto:
 *              type: string
 *            tipoInstallazione:
 *              type: string
 *            ripetitoreWifi:
 *              type: string
 *            router:
 *              type: string
 *            numeroDiTelefono:
 *              type: string
 *              nullable: true
 *            codiceMigrazione:
 *              type: string
 *              nullable: true
 *            intestatarioLineaTelefonicaDiversoDaQuelloDellaConnessioneEOLO:
 *              type: string
 *            tipoIntestatario:
 *              type: string
 *            nomeIntestatarioLineaTelefonica:
 *              type: string
 *              nullable: true
 *            cognomeIntestatarioLineaTelefonica:
 *              type: string
 *              nullable: true
 *            comuneDiNascitaIntestatarioLineaTelefonica:
 *              type: string
 *              nullable: true
 *            codiceFiscaleIntestatarioLineaTelefonica:
 *              type: string
 *              nullable: true
 *            emailIntestatarioLineaTelefonica:
 *              type: string
 *              nullable: true
 *            cellulareIntestatarioLineaTelefonica:
 *              type: string
 *              nullable: true
 *            nomeIntestatario:
 *              type: string
 *            cognomeIntestatario:
 *              type: string
 *            ragioneSocialeIntestatario:
 *              type: string
 *              nullable: true
 *            pivaIntestatario:
 *              type: string
 *              nullable: true
 *            cciaaIntestatario:
 *              type: string
 *              nullable: true
 *            emailIntestatario:
 *              type: string
 *            codiceFiscaleIntestatario:
 *              type: string
 *            tipoIndirizzoIntestatario:
 *              type: string
 *              nullable: true
 *            indirizzoIntestatario:
 *              type: string
 *              nullable: true
 *            civicoIntestatario:
 *              type: string
 *              nullable: true
 *            capIntestatario:
 *              type: string
 *              nullable: true
 *            dataDiNascitaIntestatario:
 *              type: string
 *            comuneDiNascitaIntestatario:
 *              type: string
 *            sessoIntestatario:
 *              type: string
 *            cellulareIntestatario:
 *              type: string
 *            presenzaInstallazione:
 *              type: string
 *            nomePresenzaInstallazione:
 *              type: string
 *              nullable: true
 *            cognomePresenzaInstallazione:
 *              type: string
 *              nullable: true
 *            cellularePresenzaInstallazione:
 *              type: string
 *              nullable: true
 *            emailPresenzaInstallazione:
 *              type: string
 *              nullable: true
 *            datiFatturaDiversiDaFornitura:
 *              type: string
 *            tipoIndirizzoFatturazione:
 *              type: string
 *              nullable: true
 *            indirizzoFatturazione:
 *              type: string
 *              nullable: true
 *            civicoFatturazione:
 *              type: string
 *              nullable: true
 *            comuneFatturazione:
 *              type: string
 *              nullable: true
 *            capFatturazione:
 *              type: string
 *              nullable: true
 *            nomeFatturazione:
 *              type: string
 *              nullable: true
 *            cognomeFatturazione:
 *              type: string
 *              nullable: true
 *            dataDiNascitaFatturazione:
 *              type: string
 *              nullable: true
 *            comuneDiNascitaFatturazione:
 *              type: string
 *              nullable: true
 *            sessoFatturazione:
 *              type: string
 *              nullable: true
 *            codiceFiscaleFatturazione:
 *              type: string
 *              nullable: true
 *            cellulareFatturazione:
 *              type: string
 *              nullable: true
 *            emailFatturazione:
 *              type: string
 *              nullable: true
 *            tipoDocumento:
 *              type: string
 *            numeroDocumento:
 *              type: string
 *            comuneRilascioDocumento:
 *              type: string
 *            dataRilascioDocumento:
 *              type: string
 *            dataScadenzaDocumento:
 *              type: string
 *
 * /scraper/queue/eolo:
 *  post:
 *    tags:
 *      - v2
 *      - queue
 *    description:
 *      Inserisce in queue il task per inserire il contratto sul portale eolo.
 *      Il payload è lo stesso della rotta diretta `/scraper/v2/eolo/inserimento`,
 *      in più va aggiunto l'id dati contratto ed il webhook che lo scraper poi andrà a richiamare per notificare l'esito.
 *    consume:
 *      - application/json
 *    produces:
 *      - application/json
 *    responses:
 *      200:
 *        description: SuccessResponse
 */
