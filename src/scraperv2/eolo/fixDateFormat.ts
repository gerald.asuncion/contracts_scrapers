const fixDateFormat = (date: string): string => date.replace(/ /g, '').replace(/-/g, '/').trim();

export default fixDateFormat;
