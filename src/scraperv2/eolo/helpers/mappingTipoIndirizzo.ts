const map: Record<string, string> = {
  'località': 'localit'
};

export default function(tipoIndirizzo: string): string {
  const _tipoIndirizzo = (tipoIndirizzo || '').toLowerCase();

  return map[_tipoIndirizzo] || _tipoIndirizzo;
}
