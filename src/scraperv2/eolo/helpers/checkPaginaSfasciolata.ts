import { Page } from "puppeteer";
import { getOptionText } from "../../../browser/page/getElementText";
import isVisible from "../../../browser/page/isVisible";
import BlockedScraperError from "../../../errors/BlockedScraperError";
import { Logger } from "../../../logger";

export default async function(page: Page, logger: Logger): Promise<void> {
  if (await isVisible(page, '#main-area > .general-wrapper > .row > h1', 2000)) {
    const message = await getOptionText(page, '#main-area > .general-wrapper > .row > h1')
    logger.error(`Errore Portale: "${message}"`);

    throw new BlockedScraperError(`Errore Portale: "${message}"`);
  }
}
