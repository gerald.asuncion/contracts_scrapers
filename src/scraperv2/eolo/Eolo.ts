import { performance } from 'perf_hooks';
import { Page } from 'puppeteer';
import acceptCookie from '../../browser/page/acceptCookie';
import waitForSelectorAndClick from '../../browser/page/waitForSelectorAndClick';
import waitForSelectorAndClickEvaluated from '../../browser/page/waitForSelectorAndClickEvaluated';
import waitForSelectorAndType from '../../browser/page/waitForSelectorAndType';
// import waitForModalErrorOnConfirmPage from "./telefono/waitForModalErrorOnConfirmPage";
import waitForTimeout from '../../browser/page/waitForTimeout';
import waitForVisible from '../../browser/page/waitForVisible';
import { Logger } from '../../logger';
import { NoSubmitPayload } from '../../payloads/types';
import SuccessResponse from '../../response/SuccessResponse';
import { QueueTask } from '../../task/QueueTask';
import createChildPayloadAwareLogger from '../../utils/createChildPayloadAwareLogger';
import delay from '../../utils/delay';
import tryOrThrow from '../../utils/tryOrThrow';
import { ScraperResponse, WebScraper } from '../scraper';
import { SCRAPER_TIPOLOGIA, ScraperTipologia } from '../ScraperOptions';
import EoloPayload from './EoloPayload';
import checkPaginaSfasciolata from './helpers/checkPaginaSfasciolata';
import mappingTipoIndirizzo from './helpers/mappingTipoIndirizzo';
// import checkIfMobileMenu from './telefono/checkIfMobileMenu';
import chiudiModaleInutile from './telefono/chiudiModaleInutile';
import { clickOnFirstSuggestionV4 } from './telefono/clickOnFirstSuggestionV2';
import inserisciDatiDocumento from './telefono/datipersonali/inserisciDatiDocumento';
import inserisciDatiFatturazione from './telefono/datipersonali/inserisciDatiFatturazione';
import inserisciDatiInstallazione from './telefono/datipersonali/inserisciDatiInstallazione';
import inserisciDatiPresenzaInstallazione from './telefono/datipersonali/inserisciDatiPresenzaInstallazione';
// import openMobileMenu from './telefono/openMobileMenu';
import selectInstallazione from './telefono/selectInstallazione';
import selectInteressiInternet from './telefono/selectInteressiInternet';
import selectInternetOperator from './telefono/selectInternetOperator';
import selectPacchetto from './telefono/selectPacchetto';
import selectRipetitoreWifi from './telefono/selectRipetitoreWifi';
import selectRouter from './telefono/selectRouter';
import selectUsiInternet from './telefono/selectUsiInternet';
import selezionaLineaTelefonica from './telefono/selezionaLineaTelefonica';
import waitClosePromoModal from './telefono/waitClosePromoModal';
import waitFormCoverRemoved from './telefono/waitFormCoverRemoved';
import FailureResponse from '../../response/FailureResponse';
import { EoloVerificaCoperturaPayload } from './payload/EoloVerificaCoperturaPayload';

const waitForNavigationIsNowBroken = (page: Page) => Promise.race([
  page.waitForNavigation({ waitUntil: 'networkidle2' }),
  delay(4000)
]);

@QueueTask()
export default class Eolo extends WebScraper {
  getLoginTimeout(): number { return 60000; }

  async login(args: EoloPayload): Promise<void> {
    const logger = createChildPayloadAwareLogger(this.childLogger, args);
    const { username, password } = await this.getCredenziali();

    const page = await this.p();

    logger.info("login > apro l'home page");

    await page.goto('https://www.eolo.it', { waitUntil: 'networkidle2', timeout: 60000 });

    logger.info('login > chiudo eventuale modale promozionale');

    await waitClosePromoModal(page);

    logger.info("login > controllo se l'utente è già loggato");

    const sel = await page.$('#main-container');
    if (sel) {
      logger.info('login > utente già loggato');
      return Promise.resolve();
    }

    logger.info('accetto i cookie (altrimenti non si raggiunge la fine)');
    await acceptCookie(page, '#onetrust-banner-sdk', '#onetrust-accept-btn-handler');

    logger.info('login > apro la modale per effettuare la login');
    await waitForSelectorAndClickEvaluated(page, '#eoloLoginModal');

    await page.waitForSelector('#eolo-login-ar-PR2020 > div > div > div.eolo-login-modal-body.modal-body > div > div');

    await waitForSelectorAndClickEvaluated(page, '.eolo-tab-2 > a');

    await page.waitForSelector(
      '#eolo-login-ar-PR2020 > div > div > div.eolo-login-modal-body.modal-body > div > div > div > div > div',
      { visible: true }
    );

    await waitForSelectorAndType(page, '#username-cliente-partner', username);
    logger.info('login > inserito username');

    await waitForSelectorAndType(page, '#password-cliente-partner', password);
    logger.info('login > inserita password');

    await Promise.all([
      waitForSelectorAndClick(page, '#eolo-login-submit-button-partner'),
      waitForNavigationIsNowBroken(page)
    ]);

    logger.info('login > effettuata');

    return Promise.resolve();
  }

  async executeEnsuringPageToFront<T = void>(page: Page, cback: () => Promise<T>, period = 500): Promise<T> {
    const sint = setInterval(() => page.bringToFront(), period);
    let result: T;

    try {
      await page.bringToFront();

      result = await cback();

      await page.bringToFront();
    } catch (ex) {
      throw ex
    } finally {
      sint && clearInterval(sint)
    }

    return result;
  }

  async stepIniziale(page: Page, logger: Logger, args?: EoloPayload): Promise<void> {
    logger.info('STEP selezione offerta da catalogo');

    await chiudiModaleInutile(page);

    await delay(1000);

    await waitForSelectorAndClick(page, '#main-container');
    await page.waitForSelector('#main-container #content-wrapper');

    logger.info('vado direttamente al catalogo');
    await this.executeEnsuringPageToFront(page, async () => {
      await delay(100);

      await Promise.all([
        page.evaluate(() => {
          (document?.querySelector('#acquisto > a') as HTMLElement)?.click();
        }),
        waitForNavigationIsNowBroken(page)
      ]);
    }, 250);

    logger.info('accetto i cookie (altrimenti non si raggiunge la fine)');
    await acceptCookie(page, '#onetrust-banner-sdk', '#onetrust-accept-btn-handler');

    await page.waitForSelector('.catalog-list');

    await Promise.all([
      waitForSelectorAndClick(
        page,
        '#table-container-items > div.catalog-list > div:nth-child(1) > div > article > div.catalog-right.promo > div.content-deaktop > a'
      ),
      waitForNavigationIsNowBroken(page)
    ]);

    logger.info('selezionato EOLO più');
  }

  async stepIndirizzoDiCopertura(
    page: Page, {
      indirizzoAttivazione,
      tipoIndirizzoAttivazione /* via, viale, parco, piazza, etc etc */,
      numeroCivicoAttivazione,
      comuneAttivazione,
      capAttivazione,
      tipoContratto,
      precedenteOperatore,
      nomeAltroOperatore
    }: EoloPayload | EoloVerificaCoperturaPayload,
    logger: Logger,
    ifKO?: boolean
  ): Promise<void | FailureResponse | SuccessResponse> {
    logger.info('STEP inserimento indirizzo di copertura');

    await page.waitForSelector('#main-content');

    await delay(500);

    logger.info('accetto i cookie (altrimenti non si raggiunge la fine)');
    await acceptCookie(page, '#onetrust-banner-sdk', '#onetrust-accept-btn-handler');

    // const tipoIndirizzoAttivazioneNormalizzato = mappingTipoIndirizzo(tipoIndirizzoAttivazione);
    // await waitForSelectorAndType(page, '#qualifier', tipoIndirizzoAttivazioneNormalizzato);
    // await clickOnFirstSuggestionV4(page, `tipo indirizzo attivazione (${tipoIndirizzoAttivazioneNormalizzato})`);
    // logger.info(`inserito tipo indirizzo ${tipoIndirizzoAttivazioneNormalizzato}`);

    await waitForSelectorAndType(page, '#city_label', comuneAttivazione);
    await clickOnFirstSuggestionV4(page, `comune attivazione (${comuneAttivazione})`);
    logger.info(`inserito comune ${comuneAttivazione}`);

    const address = `${tipoIndirizzoAttivazione} ${indirizzoAttivazione}`;
    await waitForSelectorAndType(page, '#address', address);
    await clickOnFirstSuggestionV4(page, address);
    logger.info(`inserito indirizzo ${address}`);

    await waitForSelectorAndType(page, '#number', numeroCivicoAttivazione);
    logger.info(`inserito numero civico ${numeroCivicoAttivazione}`);

    /*
    const capInput = await page.waitForSelector('#cap');
    await capInput?.evaluate((el, cap) => { (el as HTMLInputElement).value = cap; }, capAttivazione);
    logger.info(`inserito cap ${capAttivazione}`);
    */

    await selectInternetOperator(page, '#dk0-combobox', '#dk0-listbox', tipoContratto, precedenteOperatore, nomeAltroOperatore);
    logger.info(`selezionato operatore internet precedente ${precedenteOperatore}`);

    await waitForSelectorAndClickEvaluated(
      page,
      '#copertura-eolo-1-form > fieldset > div.form-buttons > div > div > div.col-12.col-lg-5.order-lg-2.col-xl-4.ml-auto > button'
    );

    await waitFormCoverRemoved(page);

    // controllo se non è riuscito a trovare l'indirizzo
    let errMsg;
    try {
      await page.waitForSelector('#results');
      errMsg = await page.$eval('#row-ko-coverage-default > div.col-12.col-lg-8 > p:nth-child(1)', (el) => el.textContent);
    } catch (ex) {
      // se non ha trovato gli elementi del risultato
      // la copertura è ok e vuol dire che è andato avanti
    }

    if (errMsg) {
      // eslint-disable-next-line max-len
      const indirizzoCompleto = `${tipoIndirizzoAttivazione} ${indirizzoAttivazione} ${numeroCivicoAttivazione} ${comuneAttivazione} ${capAttivazione}`;
      logger.error(`${errMsg} ${indirizzoCompleto}`);

      if(ifKO) {
        return new FailureResponse(`KO - Verifica copertura fallita indirizzo ${indirizzoCompleto} non trovato da Eolo`); 
      }
      throw new Error(`Verifica copertura fallita indirizzo ${indirizzoCompleto} non trovato da Eolo`);
    }

    if(ifKO) { 
      return new SuccessResponse(); 
    }
    
  }

  async stepConfigurazionePacchetti(page: Page, args: EoloPayload, logger: Logger): Promise<void> {
    const {
      idDatiContratto, usiInternet, interessiInternet, pacchetto, tipoInstallazione, ripetitoreWifi, router
    } = args;

    logger.info('STEP configurazione pacchetti');

    await acceptCookie(page, '#onetrust-banner-sdk', '#onetrust-accept-btn-handler');
    logger.info('chiuso banner cookie');

    await checkPaginaSfasciolata(page, logger);

    // await page.waitForSelector("#box-message-ewg-1-text > p", { visible: true });
    await page.waitForSelector('#main-content');

    await selectUsiInternet(page, usiInternet, logger);

    await selectInteressiInternet(page, interessiInternet, logger);

    await waitForSelectorAndClickEvaluated(
      page,
      '#main-content > section > div.row > div > fieldset > div.form-buttons > div > div > '
      + 'div.col-12.col-lg-5.order-lg-2.col-xl-4.ml-auto.question-step-button > button'
    );
    await waitFormCoverRemoved(page);

    await selectPacchetto(page, pacchetto, logger);

    await waitFormCoverRemoved(page);

    await selectInstallazione(page, tipoInstallazione, logger);

    await waitFormCoverRemoved(page);

    await selectRipetitoreWifi(page, ripetitoreWifi, logger);

    await waitFormCoverRemoved(page);

    await selezionaLineaTelefonica(page, args, logger);

    await selectRouter(page, router, logger);

    try {
      await waitForSelectorAndClick(
        page,
        '#main-content > section > div.form-buttons > div > div > div.submit-mask.col-12.col-lg-5.order-lg-2.col-xl-4.ml-auto > button'
      );
    } catch (ex: any) {
      logger.info(
        'non sono riuscito a cliccare sulla prima versione (selettore) del pulsante provo col'
        + 'secondo selettore, altrimenti non si procede al prossimo step'
      );
      try {
        await waitForSelectorAndClickEvaluated(
          page,
          '#main-content > section > strong > div.form-buttons > div > div > '
          + 'div.submit-mask.col-12.col-lg-5.order-lg-2.col-xl-4.ml-auto > button'
        );
      } catch (ex2: any) {
        const btnErrMsg = typeof ex2 === 'string' ? ex2 : ex2.message;
        logger.error(`anche la seconda variante non va bene ${btnErrMsg}`);
        throw new Error(
          'Non sono riuscito a cliccare sul pulsante per passare allo step di inserimento dati personali, pulsante non trovato'
        );
      }
    }

    // controllo se è apparsa la modale d'errore
    let errMsg;
    try {
      const errContainer = await page.waitForSelector('#modalError > div > div > div.modal-header > div', { visible: true, timeout: 3000 });
      errMsg = await errContainer?.evaluate((el) => (el as HTMLElement).innerText);
    } catch (ex) {
      // non è apparsa posso proseguire tranquillamente
    }

    if (errMsg) {
      errMsg = errMsg.replace('Attenzione: si prega di verificare i seguenti errori\n', '').trim();
      throw new Error(errMsg);
    }
  }

  async stepDatiPersonali(page: Page, args: EoloPayload, logger: Logger): Promise<void> {
    logger.info('STEP dati personali');

    await tryOrThrow(() => waitForVisible(page, '#installazione_nome'), "la pagina per inseriire i dati dell'installazione non si è caricata in tempo");

    // DATI INSTALLAZIONE
    logger.info('procedo all\'inserimento dei dati dell\'installazione');
    await inserisciDatiInstallazione(page, args, logger);

    // PRESENZA
    logger.info('procedo all\'inserimento dei dati per la presenza durante l\'installazione');
    await inserisciDatiPresenzaInstallazione(page, args, logger);

    // FATTURAZIONE
    logger.info('procedo all\'inserimento dei dati per la fatturazione');
    await inserisciDatiFatturazione(page, args, logger);

    // DOCUMENTO
    logger.info('procedo all\'inserimento dei dati del documento di identità');
    await inserisciDatiDocumento(page, args, logger);

    // vado al passo successivo
    return waitForSelectorAndClick(
      page,
      '#main-content > section > fieldset > div.form-buttons > div > div:nth-child(2) > div.col-12.col-lg-5.order-lg-2.col-xl-4.ml-auto > button'
    );
  }

  async stepConferma(page: Page, logger: Logger, payload: EoloPayload & NoSubmitPayload): Promise<void> {
    logger.info('STEP conferma');

    logger.info('attendo caricamento pagina');
    await waitFormCoverRemoved(page);

    await page.waitForSelector('#flow-form');

    logger.info('pagina caricata');

    // controllo se è apparsa la modale d'errore
    let modalErrMsg;
    try {
      await page.waitForSelector('#modalError', { visible: true, timeout: 3000 });
      modalErrMsg = await page.evaluate(() => document.querySelector<HTMLElement>('#errors')?.innerText.trim());
    } catch (ex) {
      // modale d'errore non presente posso procedere
    }

    if (modalErrMsg) {
      throw new Error(modalErrMsg);
    }

    this.checkNoSubmit(payload);

    logger.info('clicco sul pulsante prosegui');
    return waitForSelectorAndClick(
      page,
      '#main-content > section > fieldset > div.form-buttons > div > div:nth-child(2) '
      + '> div.col-12.col-lg-5.order-lg-2.col-xl-4.ml-auto > button'
    );
  }

  async stepRecuperoCodiceFastlane(page: Page, logger: Logger, args: EoloPayload): Promise<string> {
    // eslint-disable-next-line max-len
    const CODE_SELECTOR = '#main-content > section > div > div.result-highlight-wrapper.no-bkg > div > div > div > article > div:nth-child(2) > h3';
    const CODE_PAGE_LOADING_SELECTOR = '#main-area';

    logger.info('STEP recupero codice fastlane');
    logger.info('attendo caricamento pagina');

    try {
      await page.waitForSelector(CODE_PAGE_LOADING_SELECTOR);

      logger.info('procedo a recuperare il codice fastlane');
      await waitForTimeout(page, 1000);

      await page.waitForSelector(CODE_SELECTOR);

      logger.info('elemento trovato');
      let codiceFastlane: string = await page.evaluate((selector) => {
        const el = document.querySelector(selector);
        return (el as HTMLElement).innerHTML;
      }, CODE_SELECTOR);
      codiceFastlane = codiceFastlane.trim();
      logger.info(`recuperarato codice ${codiceFastlane}`);

      return codiceFastlane;
    } catch (ex: any) {
      const errMsg = typeof ex === 'string' ? ex : ex.message;
      let msg: any;
      if (errMsg.includes(CODE_SELECTOR)) {
        msg = 'codice fast lane non trovato';
      } else {
        if (errMsg.includes(CODE_PAGE_LOADING_SELECTOR)) {
          msg = 'la pagina non si è caricata';
        }

        msg = `Non sono riuscito a recuperare il codice fastlane ${msg}`;
      }

      throw new Error(msg);
    }
  }

  /**
   * ATTENZIONE: Devo lasciare any ad args
   *             perché Eolo viene esteso da EoloCheck che ha un payload differente
   * @param args Request payload
   */
  async scrapeWebsite(args: any): Promise<ScraperResponse> {
    const logger = createChildPayloadAwareLogger(this.childLogger, args);
    const { idDatiContratto } = args;

    const start = performance.now();
    const page = await this.p();

    //try {
      await this.stepIniziale(page, logger, args);

      await this.stepIndirizzoDiCopertura(page, args, logger);

      await this.stepConfigurazionePacchetti(page, args, logger);

      await this.stepDatiPersonali(page, args, logger);

      await this.stepConferma(page, logger, args);

      const codiceFastLane = await this.stepRecuperoCodiceFastlane(page, logger, args);
      const end = performance.now();

      logger.info(`finito in ${(end - start).toFixed(2)} ms codice fastlane: ${codiceFastLane}`);

      return new SuccessResponse(codiceFastLane);
    /*} catch (ex: any) {
      await this.saveScreenshot(page, logger, 'eolo-errore', idDatiContratto);
      const end = performance.now();
      const errMsg = extractErrorMessage(ex);
      logger.error(`finito in ${(end - start).toFixed(2)} ms non sono riuscito a compilare il contratto: ${errMsg}`);
      return new FailureResponse(ex.message);
    }*/
  }

  getScraperCodice() {
    return 'eolo';
  }

  getScraperTipologia(): ScraperTipologia {
    return SCRAPER_TIPOLOGIA.INSERIMENTO;
  }
}
