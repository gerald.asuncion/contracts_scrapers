export type EoloVerificaCoperturaPayload = {
    indirizzoAttivazione: string,
    tipoIndirizzoAttivazione: string,
    numeroCivicoAttivazione: string,
    comuneAttivazione: string,
    capAttivazione: string,
    tipoContratto: string,
    precedenteOperatore: string,
    nomeAltroOperatore: string | null
}