const PREV_OPERATOR: Record<string, string> = {
  'nessuna connessione internet': 'No, non ho internet',
  tim: 'Sì ho internet con TIM',
  vodafone: 'Sì ho internet con Vodafone',
  wind: 'Sì ho internet con Wind',
  altro: 'Sì ho internet con altro operatore'
};

export default function prevOperatorToSelectLabel(prevOperator: string): string {
  return PREV_OPERATOR[prevOperator.toLowerCase()];
}
