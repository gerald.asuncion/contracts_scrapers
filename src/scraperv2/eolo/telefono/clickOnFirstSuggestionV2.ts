import { Page } from 'puppeteer';
import waitForTimeout from '../../../browser/page/waitForTimeout';

// TODO: tenuto per precauzione
export default async function clickOnFirstSuggestionV2(page: Page): Promise<void> {
  const suggestion = await page.waitForSelector(
    '.ui-menu.ui-widget.ui-widget-content.ui-autocomplete.ui-front',
    { visible: true, timeout: 3000 }
  );

  if (!suggestion) {
    throw new Error('suggestion non trovata');
  }

  const success = await suggestion.evaluate((ul) => {
    const li = ul.querySelector<HTMLElement>('li');
    if (!li) {
      return false;
    }
    li.click();
    return true;
  });

  if (!success) {
    throw new Error('suggestion elemento da cliccare non trovato');
  }
}

// TODO: tenuto per precauzione
export async function clickOnFirstSuggestionV3(page: Page, expression: string): Promise<void> {
  const suggestion = await page.waitForXPath(expression, { visible: true, timeout: 3000 });
  // const [suggestion] = await page.$x(expression);

  if (!suggestion) {
    throw new Error('suggestion non trovata');
  }
  suggestion.evaluate((ul) => ul.querySelector<HTMLElement>('li')?.click());
  // const success = await suggestion.evaluate(ul => {
  //     const li = ul.querySelector<HTMLElement>("li");
  //     if (!li) {
  //         return false;
  //     }
  //     li.click();
  //     return true;
  // });

  // if (!success) {
  //     throw new Error(`suggestion elemento da cliccare non trovato`);
  // }
}

export async function clickOnFirstSuggestionV4(
  page: Page,
  suggestionLabel: string,
  delay?: number
): Promise<void> {
  await waitForTimeout(page, delay || 3000);

  const success = await page.evaluate(() => {
    const ul = Array.from(document.querySelectorAll('.ui-menu')).find((el) => {
      const computedStyle = window.getComputedStyle(el);

      return computedStyle.display === 'block';
    });

    if (!ul) {
      return false;
    }

    ul.querySelector<HTMLLIElement>('li')?.click();

    return true;
  });

  if (!success) {
    throw new Error(`suggestion ${suggestionLabel} non trovata`);
  }
}
