import { Page } from 'puppeteer';
import evaluateSelectorAndClick from '../../../browser/page/evaluateSelectorAndClick';

export default async function openMobileMenu(page: Page): Promise<void> {
  await evaluateSelectorAndClick(page, '#header > div.header-main > div > div > div.col-7.col-xs-7 > a.mobile-menu-btn');
  await page.waitForSelector('#mm-1', { visible: true });
}
