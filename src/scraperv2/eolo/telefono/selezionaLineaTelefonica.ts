import { Page } from 'puppeteer';
import waitForSelectorAndClickEvaluated from '../../../browser/page/waitForSelectorAndClickEvaluated';
import waitForSelectorAndType from '../../../browser/page/waitForSelectorAndType';
import { Logger } from '../../../logger';
import EoloPayload from '../EoloPayload';
import mappingTipoIndirizzo from '../helpers/mappingTipoIndirizzo';
import { clickOnFirstSuggestionV4 } from './clickOnFirstSuggestionV2';
import waitFormCoverRemoved from './waitFormCoverRemoved';

export default async function selezionaLineaTelefonica(page: Page, {
  tipoContratto,
  numeroDiTelefono,
  codiceMigrazione,
  intestatarioLineaTelefonicaDiversoDaQuelloDellaConnessioneEOLO,
  tipoIntestatario,
  nomeIntestatarioLineaTelefonica,
  cognomeIntestatarioLineaTelefonica,
  ragioneSocialeIntestatario,
  pivaIntestatario,
  cciaaIntestatario,
  emailIntestatarioLineaTelefonica,
  codiceFiscaleIntestatarioLineaTelefonica,
  tipoIndirizzoIntestatario: _tipoIndirizzoIntestatario,
  indirizzoIntestatario,
  civicoIntestatario,
  comuneDiNascitaIntestatarioLineaTelefonica,
  capIntestatario,
  cellulareIntestatarioLineaTelefonica
}: EoloPayload, logger: Logger): Promise<void> {
  if (tipoContratto === 'attivazione') {
    logger.info('prima attivazione seleziono singola linea telefonica');
    await waitForSelectorAndClickEvaluated(page, 'label[for="tblconfigurazione_voce-1"]');
    await waitFormCoverRemoved(page);
  } else if (tipoContratto === 'switch') {
    logger.info('mantengo linea telefonica');
    await waitForSelectorAndClickEvaluated(page, 'label[for="tblconfigurazione_voce-2"]');
    await waitFormCoverRemoved(page);

    await waitForSelectorAndType(page, '#configurazione_voce_telefono', numeroDiTelefono as string);
    await waitForSelectorAndType(page, '#configurazione_voce_portabilita_codicemigrazione', codiceMigrazione as string);

    if (parseInt(<string>intestatarioLineaTelefonicaDiversoDaQuelloDellaConnessioneEOLO) === 1) {
      await page.evaluate(() => {
        const el = document.querySelector<HTMLLabelElement>('label[for="configurazione_voce_portabilita_sovrascrividatiintestatario0"]');
        el?.click();
      });

      await page.waitForSelector('label[for="configurazione_voce_portabilita_tipoutente"]', { visible: true });

      const tipoIndirizzoIntestatario = _tipoIndirizzoIntestatario ? mappingTipoIndirizzo(_tipoIndirizzoIntestatario) : _tipoIndirizzoIntestatario;

      if (tipoIntestatario.toLowerCase() === 'persona fisica') {
        logger.info('intestatario è una persona fisica');
        await waitForSelectorAndType(page, '#configurazione_voce_portabilita_nome', nomeIntestatarioLineaTelefonica as string);
        logger.info('inserito nome intestatario');

        await waitForSelectorAndType(page, '#configurazione_voce_portabilita_cognome', cognomeIntestatarioLineaTelefonica as string);
        logger.info('inserito cognome intestatario');

        await waitForSelectorAndType(page, '#configurazione_voce_portabilita_mobile', cellulareIntestatarioLineaTelefonica as string);
        logger.info('inserito cellulare intestatario');

        await waitForSelectorAndType(page, '#configurazione_voce_portabilita_email', emailIntestatarioLineaTelefonica as string);
        logger.info('inserita email intestatario');

        await waitForSelectorAndType(page, '#configurazione_voce_portabilita_codicefiscale', codiceFiscaleIntestatarioLineaTelefonica as string);
        logger.info('inserito codice fiscale intestatario');

        await waitForSelectorAndType(page, '#configurazione_voce_portabilita_particellatoponomastica_autocomplete', tipoIndirizzoIntestatario as string);
        await clickOnFirstSuggestionV4(page, `tipo indirizzo intestatario linea telefonica (${tipoIndirizzoIntestatario})`);
        logger.info('inserito tipo via indirizzo intestatario');

        await waitForSelectorAndType(page, '#configurazione_voce_portabilita_indirizzo', indirizzoIntestatario as string);
        logger.info('inserito indirizzo intestatario');

        await waitForSelectorAndType(page, '#configurazione_voce_portabilita_civico', civicoIntestatario as string);
        logger.info('inserito numero civico intestatario');

        await waitForSelectorAndType(page, '#configurazione_voce_portabilita_comune_autocomplete', comuneDiNascitaIntestatarioLineaTelefonica as string);
        await clickOnFirstSuggestionV4(page, `comune di nascita intestatario linea telefonica (${comuneDiNascitaIntestatarioLineaTelefonica})`);
        logger.info('inserito comune intestatario');

        await page.evaluate((cap) => {
          const el = document.querySelector<HTMLInputElement>('#configurazione_voce_portabilita_cap');
          (<HTMLInputElement>el).value = cap;
        }, capIntestatario);
        logger.info('inserito cap intestatario');
      } else {
        logger.info('intestatario è un\'azienda');

        await page.evaluate(() => {
          const el = document.querySelector<HTMLLabelElement>('label[for="configurazione_voce_portabilita_tipoutente1_1"]');
          el?.click();
        });

        await waitForSelectorAndType(page, '#configurazione_voce_portabilita_email', emailIntestatarioLineaTelefonica as string);
        logger.info('inserita email intestatario');

        await waitForSelectorAndType(page, '#configurazione_voce_portabilita_ragionesociale', ragioneSocialeIntestatario as string);
        logger.info('inserita ragione sociale intestatario');

        await waitForSelectorAndType(page, '#configurazione_voce_portabilita_codicefiscale', codiceFiscaleIntestatarioLineaTelefonica as string);
        logger.info('inserita codice fiscale intestatario');

        await waitForSelectorAndType(page, '#configurazione_voce_portabilita_partitaiva', pivaIntestatario as string);
        logger.info('inserita partita iva intestatario');

        const [cciaaCombobox] = await page.$x('/html/body/div[3]/form/div[2]/section/div[2]/div/fieldset/strong/div/div/div[2]/div/div/article[4]/div/div[4]/div[3]/div/fieldset/div[1]/div[1]/div/div[10]/div/div/div');
        cciaaCombobox.click();
        const cciaListContainer = await page.waitForXPath('/html/body/div[3]/form/div[2]/section/div[2]/div/fieldset/strong/div/div/div[2]/div/div/article[4]/div/div[4]/div[3]/div/fieldset/div[1]/div[1]/div/div[10]/div/div/ul', { visible: true });
        const cciaSuccess = await cciaListContainer?.evaluate((ul, name) => {
          const li = ul.querySelector<HTMLLIElement>(`li[text="${name}"]`);

          if (!li) {
            return false;
          }

          li.click();

          return true;
        }, cciaaIntestatario);
        if (!cciaSuccess) {
          throw new Error(`non sono riuscito a selezionare la CCIAA dell\'intestatario, ${cciaaIntestatario} non trovato nella lista`);
        }
        logger.info('inserito CCIAA intestatario');

        await waitForSelectorAndType(page, '#configurazione_voce_portabilita_particellatoponomastica_autocomplete', tipoIndirizzoIntestatario as string);
        await clickOnFirstSuggestionV4(page, `tipo indirizzo intestatario linea telefonica (${tipoIndirizzoIntestatario})`);
        logger.info('inserito tipo via indirizzo intestatario');

        await waitForSelectorAndType(page, '#configurazione_voce_portabilita_indirizzo', indirizzoIntestatario as string);
        logger.info('inserito indirizzo intestatario');

        await waitForSelectorAndType(page, '#configurazione_voce_portabilita_civico', civicoIntestatario as string);
        logger.info('inserito numero civico intestatario');

        await waitForSelectorAndType(page, '#configurazione_voce_portabilita_comune_autocomplete', comuneDiNascitaIntestatarioLineaTelefonica as string);
        await clickOnFirstSuggestionV4(page, `tipo indirizzo intestatario linea telefonica (${comuneDiNascitaIntestatarioLineaTelefonica})`);
        logger.info('inserito comune intestatario');

        await page.evaluate((cap) => {
          const el = document.querySelector<HTMLInputElement>('#configurazione_voce_portabilita_cap');
          (<HTMLInputElement>el).value = cap;
        }, capIntestatario);
        logger.info('inserito cap intestatario');
      }

      logger.info('clicco sulla checkbox per accettare le condizioni');
      await page.evaluate(() => {
        const el = document.querySelector<HTMLLabelElement>('label[for="configurazione_voce_prefisso_portabilita_agreement0"]');
        el?.click();
      });
    } else {
      await page.evaluate(() => {
        const el = document.querySelector<HTMLLabelElement>('label[for="configurazione_voce_prefisso_portabilita_agreement00"]');
        el?.click();
      });
    }
  }

  logger.info(`seleziono linea telefonica (${tipoContratto})`);
}
