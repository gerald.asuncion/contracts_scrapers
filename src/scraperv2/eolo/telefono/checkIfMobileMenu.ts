import { Page } from 'puppeteer';

export default async function checkIfMobileMenu(page: Page): Promise<boolean> {
  const isMobile = await page.waitForFunction((): boolean => {
    const el = document.querySelector('#header > div.header-main > div > div > div.col-7.col-xs-7 > a.mobile-menu-btn');
    if (!el) return false;

    const computedStyle = window.getComputedStyle(el);

    return computedStyle.display !== 'none';
  }, { polling: 100 });

  return isMobile.jsonValue();
}
