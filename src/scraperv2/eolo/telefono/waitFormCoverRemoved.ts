import { Page } from 'puppeteer';

export default async function waitFormCoverRemoved(
  page: Page,
  options?: {
    timeout?: number;
    polling?: string | number;
  }
): Promise<void> {
  const internalOptions = { polling: 100 };
  // try {
  await page.waitForFunction(() => {
    const el = document.querySelector('.form-cover');
    return !el;
  }, options ? { ...internalOptions, ...options } : internalOptions);
  // } catch (ex) {
  //     console.log("ERR: waitCoverRemoved ", ex);
  // }
}
