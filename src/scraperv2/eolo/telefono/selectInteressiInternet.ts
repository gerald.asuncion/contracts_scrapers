import { Page } from 'puppeteer';
import { Logger } from '../../../logger';

const INTERESSI: Record<string, string> = {
  ContenutiSicuriPerBambini: 'contenuti-sicuri-bambini',
  NavigazioneProtetta: 'navigazione-protetta',
  VelocitaStabilita: 'velocita-stabilita',
  PagamentiSicurezza: 'pagamenti-sicurezza'
};

export default async function selectInteressiInternet(
  page: Page,
  interessiInternet: Array<string>,
  logger: Logger
): Promise<void> {
  if (interessiInternet.length) {
    logger.info(`seleziono interessi internet ${interessiInternet.join(', ')}`);
    const interessiIds: Array<string> = interessiInternet
      .map((interesse: string) => INTERESSI[interesse])
      .filter((interesse: any) => !!interesse);
    if (interessiInternet.length !== interessiIds.length) {
      throw new Error(`id dom input per interessi non trovati per tutti [${interessiInternet.join(',')}], attuale mappa: ${JSON.stringify(INTERESSI)}`);
    }

    for (const interesse of interessiIds) {
      const selector = `#${interesse}`;
      logger.info(`seleziono interesse ${selector}`);
      const $el = await page.waitForSelector(selector);
      await $el?.evaluate((el) => {
        const label = el.nextElementSibling as HTMLLabelElement;
        if (label) {
          label.click();
        }
      });
    }
  }
}
