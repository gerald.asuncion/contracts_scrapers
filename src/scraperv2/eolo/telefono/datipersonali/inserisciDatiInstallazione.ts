import { Page } from 'puppeteer';
import waitForSelectorAndType from '../../../../browser/page/waitForSelectorAndType';
import { Logger } from '../../../../logger';
import waitFormCoverRemoved from '../waitFormCoverRemoved';
import checkCodiceFiscale from './checkCodiceFiscale';
import waitForTimeout from '../../../../browser/page/waitForTimeout';
import { clickOnFirstSuggestionV4 } from '../clickOnFirstSuggestionV2';
import fixDateFormat from '../../fixDateFormat';
import EoloPayload from '../../EoloPayload';
import waitForSelectorAndClickEvaluated from '../../../../browser/page/waitForSelectorAndClickEvaluated';
import waitForSelectorAndTypeEvaluated from '../../../../browser/page/waitForSelectorAndTypeEvaluated';

export default async function inserisciDatiInstallazione(
  page: Page,
  {
    nomeIntestatario,
    cognomeIntestatario,
    dataDiNascitaIntestatario,
    comuneDiNascitaIntestatario,
    sessoIntestatario,
    codiceFiscaleIntestatario,
    emailIntestatario,
    cellulareIntestatario
  }: EoloPayload,
  logger: Logger
): Promise<void> {
  await waitForSelectorAndTypeEvaluated(page, '#installazione_nome', nomeIntestatario);
  logger.info('inserito nome installazione');

  await waitForSelectorAndType(page, '#installazione_cognome', cognomeIntestatario);
  logger.info('inserito cognome installazione');

  await waitForSelectorAndType(page, '#installazione_data_nascita', fixDateFormat(dataDiNascitaIntestatario));
  logger.info('inserita data di nascita installazione');

  // eslint-disable-next-line max-len
  await waitForSelectorAndType(page, '#installazione_cityOrState_autocomplete', comuneDiNascitaIntestatario);
  await clickOnFirstSuggestionV4(page, `comune di nascita intestatario (${comuneDiNascitaIntestatario})`);
  logger.info('inserito comune di nascita installazione');

  // eslint-disable-next-line max-len
  const sessoIntallazioneSelector = sessoIntestatario === 'F' ? 'label[for="installazione_gender1_1"]' : 'label[for="installazione_gender0_0"]';
  await waitForSelectorAndClickEvaluated(page, sessoIntallazioneSelector);
  logger.info('selezionato sesso installazione');

  // #installazione_codicefiscale
  await waitForSelectorAndType(page, '#installazione_codicefiscale', codiceFiscaleIntestatario);
  await page.keyboard.press('Tab');
  logger.info('inserito codice fiscale installazione');

  await checkCodiceFiscale(page, '#installazione_codicefiscale ~ .error-text', 'installazione', logger);
  logger.info('controllata validità codice fiscale installazione');

  // #installazione_email
  await waitForSelectorAndType(page, '#installazione_email', emailIntestatario);
  logger.info('inserita email installazione');

  await waitForTimeout(page, 1000);

  await waitForSelectorAndType(page, '#confirmation_installazione_email', emailIntestatario);
  logger.info('inserito conferma email installazione');

  // #installazione_cellulare
  await waitForSelectorAndType(page, '#installazione_cellulare', cellulareIntestatario);
  logger.info('inserito cellulare installazione');
  return waitFormCoverRemoved(page);
}
