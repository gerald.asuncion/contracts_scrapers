import { Page, ElementHandle } from 'puppeteer';
import { Logger } from '../../../../logger';
import extractErrorMessage from '../../../../utils/extractErrorMessage';

export default async function checkCodiceFiscale(
  page: Page,
  selector: string,
  errorMessagePrefix: string,
  logger: Logger
): Promise<boolean> {
  let codiceFiscaleIncorretto: ElementHandle | null = null;
  try {
    codiceFiscaleIncorretto = await page.waitForSelector(selector, { timeout: 1000 });
  } catch (ex) {
    // non ha trovato l'elemento che contiene il messaggio di errore
    // quindi il codice fiscale è corretto
    const msg = extractErrorMessage(ex);
    if (!msg.includes('timeout')) {
      logger.warn('check codice fiscale error: ', ex);
    }
  }

  if (codiceFiscaleIncorretto) {
    throw new Error(`${errorMessagePrefix} codice fiscale non coincide coi dati`);
  }

  return true;
}
