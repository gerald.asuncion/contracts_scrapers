import { Page } from 'puppeteer';
import waitForSelectorAndClickEvaluated from '../../../../browser/page/waitForSelectorAndClickEvaluated';
import waitForSelectorAndType from '../../../../browser/page/waitForSelectorAndType';
import { Logger } from '../../../../logger';
import waitFormCoverRemoved from '../waitFormCoverRemoved';
import EoloPayload from '../../EoloPayload';

export default async function inserisciDatiPresenzaInstallazione(page: Page, {
  presenzaInstallazione,
  nomePresenzaInstallazione,
  cognomePresenzaInstallazione,
  cellularePresenzaInstallazione,
  emailPresenzaInstallazione
}: EoloPayload, logger: Logger): Promise<void> {
  if (presenzaInstallazione.toLowerCase() !== 'si') {
    logger.info('procedo all\'inserimento dei dati riguardanti la persona presente durante l\'installazione');

    await waitForSelectorAndClickEvaluated(page, 'label[for="switch-contact-load-other"]');
    await waitFormCoverRemoved(page);
    // await waitForVisible(page, "#contact-load-other");
    await waitForSelectorAndType(page, '#installazione_altro_nome', nomePresenzaInstallazione as string);
    logger.info('inserito nome presenza installazione');

    await waitForSelectorAndType(page, '#installazione_altro_cognome', cognomePresenzaInstallazione as string);
    logger.info('inserito cognome presenza installazione');

    await waitForSelectorAndType(page, '#installazione_altro_cellulare', cellularePresenzaInstallazione as string);
    logger.info('inserito cellulare presenza installazione');

    await waitForSelectorAndType(page, '#installazione_altro_email', emailPresenzaInstallazione as string);
    logger.info('inserita email presenza installazione');
  }
}
