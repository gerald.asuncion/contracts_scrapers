import { Page } from 'puppeteer';
import waitForSelectorAndClick from '../../../../browser/page/waitForSelectorAndClick';
import waitForSelectorAndClickEvaluated from '../../../../browser/page/waitForSelectorAndClickEvaluated';
import waitForSelectorAndType from '../../../../browser/page/waitForSelectorAndType';
import waitForTimeout from '../../../../browser/page/waitForTimeout';
import waitForVisibleStyled from '../../../../browser/page/waitForVisibleStyled';
import { Logger } from '../../../../logger';
import EoloPayload from '../../EoloPayload';
import mappingTipoIndirizzo from '../../helpers/mappingTipoIndirizzo';
import { clickOnFirstSuggestionV4 } from '../clickOnFirstSuggestionV2';
import checkCodiceFiscale from './checkCodiceFiscale';

export default async function inserisciDatiFatturazione(
  page: Page,
  {
    datiFatturaDiversiDaFornitura,
    tipoIndirizzoFatturazione: _tipoIndirizzoFatturazione,
    indirizzoFatturazione,
    civicoFatturazione,
    comuneFatturazione,
    capFatturazione,
    nomeFatturazione,
    cognomeFatturazione,
    dataDiNascitaFatturazione,
    comuneDiNascitaFatturazione,
    sessoFatturazione,
    codiceFiscaleFatturazione,
    cellulareFatturazione,
    emailFatturazione
  }: EoloPayload,
  logger: Logger
): Promise<void> {
  if (parseInt(<string>datiFatturaDiversiDaFornitura) === 1) {
    await waitForSelectorAndClickEvaluated(page, 'label[for="switch-load-other"]');
    await waitForVisibleStyled(page, '#billing-data-group > div > article > div');

    const tipoIndirizzoFatturazione = _tipoIndirizzoFatturazione ? mappingTipoIndirizzo(_tipoIndirizzoFatturazione) : _tipoIndirizzoFatturazione;
    await waitForSelectorAndType(page, '#fatturazione_particellatopomomastica_autocomplete', tipoIndirizzoFatturazione as string);
    await clickOnFirstSuggestionV4(page, `tipo indirizzo fatturazione (${tipoIndirizzoFatturazione})`);
    logger.info('inserito tipo indirizzo di fatturazione');

    await waitForSelectorAndType(page, '#fatturazione_indirizzo', indirizzoFatturazione as string);
    logger.info('inserito indirizzo di fatturazione');

    await waitForSelectorAndType(page, '#fatturazione_civico', civicoFatturazione as string);
    logger.info('inserito numero civico di fatturazione');

    await waitForSelectorAndType(page, '#fatturazione_comune_autocomplete', comuneFatturazione as string);
    await clickOnFirstSuggestionV4(page, `comune fatturazione (${comuneFatturazione})`);
    logger.info('inserito comune di fatturazione');

    await page.evaluate((cap) => {
      const el = document.querySelector('#fatturazione_cap');
      (el as HTMLInputElement).value = cap;
    }, capFatturazione);
    logger.info('inserito cap di fatturazione');

    await waitForSelectorAndType(page, '#fatturazione_nome', nomeFatturazione as string);
    logger.info('inserito nome di fatturazione');

    await waitForSelectorAndType(page, '#fatturazione_cognome', cognomeFatturazione as string);
    logger.info('inserito cognome di fatturazione');

    await waitForSelectorAndType(page, '#fatturazione_data_nascita', dataDiNascitaFatturazione as string);
    logger.info('inserita data di nascita di fatturazione');

    // eslint-disable-next-line max-len
    await waitForSelectorAndType(page, '#fatturazione_cityOrState_autocomplete', comuneDiNascitaFatturazione as string);
    await clickOnFirstSuggestionV4(page, `comune di nascita fatturazione (${comuneDiNascitaFatturazione})`);
    logger.info('inserito comune di nascita di fatturazione');

    // eslint-disable-next-line max-len
    const sessoFatturazioneSelector = sessoFatturazione === 'F' ? 'label[for="fatturazione_gender1_1"]' : 'label[for="fatturazione_gender0_0"]';
    await waitForSelectorAndClick(page, sessoFatturazioneSelector);
    logger.info('inserito sesso di fatturazione');

    await waitForSelectorAndType(page, '#fatturazione_codicefiscale', codiceFiscaleFatturazione as string);
    logger.info('inserito codice fiscale di fatturazione');
    await page.keyboard.press('Tab');
    logger.info('inserito codice fiscale fatturazione');

    await checkCodiceFiscale(page, '#fatturazione_codicefiscale ~ .error-text', 'fatturazione', logger);
    logger.info('controllata validità codice fiscale installazione');

    await waitForSelectorAndType(page, '#fatturazione_cellulare', cellulareFatturazione as string);
    logger.info('inserito numero di cellulare di fatturazione');

    await waitForSelectorAndType(page, '#fatturazione_email', emailFatturazione as string);
    logger.info('inserita email di fatturazione');

    await waitForTimeout(page, 1000);

    await waitForSelectorAndType(page, '#confirmation_fatturazione_email', emailFatturazione as string);
    logger.info('inserita conferma email di fatturazione');
  }
}
