import { Page } from 'puppeteer';
import waitForSelectorAndType from '../../../../browser/page/waitForSelectorAndType';
import { Logger } from '../../../../logger';
import fixDateFormat from '../../fixDateFormat';
import { clickOnFirstSuggestionV4 } from '../clickOnFirstSuggestionV2';
import EoloPayload from '../../EoloPayload';
import tryOrThrow from '../../../../utils/tryOrThrow';
import waitForTimeout from '../../../../browser/page/waitForTimeout';
import waitForXPathVisible from '../../../../browser/page/waitForXPathVisible';

const DOC_MAP: Record<string, string> = {
  "carta d'identità": '//li[contains(@text, "Carta d\'identità")]',
  patente: '//li[contains(@text, "Patente di guida")]',
  passaporto: '//li[contains(@text, "Passaporto")]',
  permessoSoggiorno: '//li[contains(@text, "Permesso di soggiorno")]'
};

export default async function inserisciDatiDocumento(
  page: Page, {
    tipoDocumento,
    numeroDocumento,
    comuneRilascioDocumento,
    dataRilascioDocumento,
    dataScadenzaDocumento
  }: EoloPayload,
  logger: Logger
): Promise<void> {
  const tipoDocumentoSelector = DOC_MAP[tipoDocumento];

  await tryOrThrow(
    async () => {
      await tryOrThrow(async () => {
        const tipoDocumentoSuggestionContainerSelector = '//div[contains(@id, "-fatturazione_documento_tipo")]';
        await waitForXPathVisible(page, tipoDocumentoSuggestionContainerSelector);
        const [tipoDocumentoCombobox] = await page.$x(tipoDocumentoSuggestionContainerSelector);
        tipoDocumentoCombobox.click();
      }, 'non sono riuscito ad aprire la suggestion');

      // diamo il tempo alla suggestion di apparire
      await waitForTimeout(page, 1000);

      await tryOrThrow(async () => {
        await waitForXPathVisible(page, tipoDocumentoSelector);
        const [tipoDocumentoElement] = await page.$x(tipoDocumentoSelector);
        tipoDocumentoElement.click();
      }, 'non sono riuscito a selezionare il tipo di documento');
    },
    'impossibile selezionare il documento di identità:'
  );

  await waitForSelectorAndType(page, '#fatturazione_documento_numero', numeroDocumento);
  logger.info('inserito numero documento d\'identità');

  await waitForSelectorAndType(page, '#fatturazione_documento_comune_autocomplete', comuneRilascioDocumento);
  await clickOnFirstSuggestionV4(page, `comune rilascio documento (${comuneRilascioDocumento})`);

  logger.info('inserito comune di rilascio documento d\'identità');

  await waitForSelectorAndType(page, '#fatturazione_documento_datarilascio', fixDateFormat(dataRilascioDocumento));
  logger.info('inserita data di rilascio documento d\'identità');

  await waitForSelectorAndType(page, '#fatturazione_documento_datascadenza', fixDateFormat(dataScadenzaDocumento));
  logger.info('inserita data di scadenza documento d\'identità');
}
