import { Page, JSHandle } from 'puppeteer';

export default function waitClosePromoModal(
  page: Page,
  waitOptions?: {
    timeout?: number;
    polling?: string | number;
  }
): Promise<JSHandle> {
  return page.waitForFunction(() => {
    const modalSelector = '#eolo-overlay';
    const container = document.querySelector(modalSelector);
    if (!container) {
      return true;
    }

    if (!container.classList.contains('in')) {
      return true;
    }

    const closeBtn = document.querySelector<HTMLButtonElement>(`${modalSelector} > div > div > div.overlaytextimage-modal-header > button`);
    closeBtn?.click();
    return true;
  }, waitOptions || { polling: 100 });
}
