import { Page } from 'puppeteer';
import waitForSelectorAndClick from '../../../browser/page/waitForSelectorAndClick';

export default async function chiudiModaleInutile(page: Page): Promise<void> {
  // chiudo la nuova modale rompi cazzo
  try {
    await page.waitForSelector('#modal-userinfo-messages', { visible: true, timeout: 1000 });
    await waitForSelectorAndClick(page, '#userinfo-messages > div > div > button');
  } catch (ex) {
    // non è presente quindi non devo fare nulla
  }
}
