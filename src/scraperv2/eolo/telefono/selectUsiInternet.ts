import { Page } from 'puppeteer';
import { Logger } from '../../../logger';

const USI: Record<string, string> = {
  FilmSerieTv: 'film-serie-tv',
  Telelavoro: 'telelavoro',
  SocialMusica: 'youtube-social',
  NavigazioneShopping: 'shopping-online',
  DidatticaDistanza: 'didattica-distanza',
  Gaming: 'gaming'
};

export default async function selectUsiInternet(
  page: Page,
  usiInternet: Array<string>,
  logger: Logger
): Promise<void> {
  if (usiInternet.length) {
    logger.info(`seleziono usi internet ${usiInternet.join(', ')}`);
    const usiIds: Array<string> = usiInternet
      .map((uso: string) => USI[uso])
      .filter((uso: string) => !!uso);
    if (usiInternet.length !== usiIds.length) {
      throw new Error(`id dom input per usi non trovati per tutti [${usiInternet.join(',')}], attuale mappa: ${JSON.stringify(USI)}`);
    }

    for (const uso of usiIds) {
      const selector = `#${uso}`;
      logger.info(`seleziono uso ${selector}`);
      const $el = await page.waitForSelector(selector);
      await $el?.evaluate((el) => {
        const label = el.nextElementSibling as HTMLLabelElement;
        if (label) {
          label.click();
        }
      });
    }
  }
}
