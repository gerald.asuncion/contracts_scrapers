import { Page } from 'puppeteer';
import { Logger } from '../../../logger';

const RIPETITORE_WIFI: Record<string, string> = {
  nessuno: 'label[for="tblconfigurazione_wifi_repeater-0"]',
  '1_ripetitore': 'label[for="tblconfigurazione_wifi_repeater-1"]',
  '2_ripetitori': 'label[for="tblconfigurazione_wifi_repeater-2"]',
  '3_ripetitori': 'label[for="tblconfigurazione_wifi_repeater-3"]'
};

export default async function selectRipetitoreWifi(
  page: Page,
  ripetitoreWifi: string,
  logger: Logger
): Promise<void> {
  const checkboxSelector = RIPETITORE_WIFI[ripetitoreWifi];
  await page.evaluate((selector) => {
    const el = document.querySelector<HTMLLabelElement>(selector);
    el?.click();
  }, checkboxSelector);
  logger.info(`selezionato ${ripetitoreWifi}`);
}
