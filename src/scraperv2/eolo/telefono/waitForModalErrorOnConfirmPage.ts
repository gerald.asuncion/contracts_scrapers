import { Page } from 'puppeteer';

export default async function waitForModalErrorOnConfirmPage(page: Page): Promise<string | null> {
  let result: string | null = null;
  try {
    await page.waitForSelector('#modalError', { visible: true, timeout: 5000 });
    result = await page.evaluate((): string => {
      const el = document.querySelector('#errors');
      return (el as HTMLElement).innerText;
    });
  } catch (ex) {
    // la modale non è apparsa quindi tutto ok
  }
  return Promise.resolve(result);
}
