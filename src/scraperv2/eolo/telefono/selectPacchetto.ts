import { Page } from 'puppeteer';
import { Logger } from '../../../logger';
import checkPaginaSfasciolata from '../helpers/checkPaginaSfasciolata';
import waitFormCoverRemoved from './waitFormCoverRemoved';

async function selezionaPacchetto(page: Page, checkboxSelector: string): Promise<void> {
  await page.evaluate((selector) => {
    const el = document.querySelector<HTMLLabelElement>(selector);
    if (!el?.classList.contains('added')) {
      el?.click();
    }
  }, checkboxSelector);
  return waitFormCoverRemoved(page);
}

async function deselezionaPacchetto(page: Page, checkboxSelector: string): Promise<void> {
  await page.evaluate((selector) => {
    const el = document.querySelector<HTMLLabelElement>(selector);
    if (el?.classList.contains('added')) {
      el?.click();
    }
  }, checkboxSelector);
  return waitFormCoverRemoved(page);
}

function deselezionaPacchettoIntrattenimento(page: Page): Promise<void> {
  return deselezionaPacchetto(page, 'label[for="offerConfigurator_0\\,00"]');
}

function deselezionaPacchettoStudioELavoro(page: Page): Promise<void> {
  return deselezionaPacchetto(page, 'label[for="offerConfigurator_1\\,00"]');
}

function deselezionaPacchettoSicurezza(page: Page): Promise<void> {
  return deselezionaPacchetto(page, 'label[for="offerConfigurator_2\\,00"]');
}

async function deselezionaTuttiPacchetti(page: Page): Promise<void> {
  await deselezionaPacchettoIntrattenimento(page);
  await deselezionaPacchettoStudioELavoro(page);
  await deselezionaPacchettoSicurezza(page);
}

function selezionaPacchettoIntrattenimento(page: Page): Promise<void> {
  return selezionaPacchetto(page, 'label[for="offerConfigurator_0\\,00"]');
}

function selezionaPacchettoStudioELavoro(page: Page): Promise<void> {
  return selezionaPacchetto(page, 'label[for="offerConfigurator_1\\,00"]');
}

function selezionaPacchettoSicurezza(page: Page): Promise<void> {
  return selezionaPacchetto(page, 'label[for="offerConfigurator_2\\,00"]');
}

export default async function selectPacchetto(
  page: Page,
  pacchetto: string,
  logger: Logger
): Promise<void> {
  if (pacchetto !== 'Eolo Piu + Intrattenimento + Sicurezza') {
    logger.info('procedo alla configurazione del pacchetto');

    await checkPaginaSfasciolata(page, logger);

    await page.waitForSelector('#packageContent2');
    await page.evaluate(() => {
      const el = document.querySelector<HTMLLabelElement>('label[for="packageContent2"]');
      el?.click();
    });
    await waitFormCoverRemoved(page);
    await page.waitForFunction(() => {
      const el = document.querySelector('.offer-configurator');
      return el?.classList.contains('active');
    }, { polling: 100 });

    await deselezionaTuttiPacchetti(page);

    if (pacchetto !== 'Eolo Piu') {
      switch (pacchetto) {
        case 'Eolo Piu + Intrattenimento':
          await selezionaPacchettoIntrattenimento(page);
          break;
        case 'Eolo Piu + Sicurezza':
          await selezionaPacchettoSicurezza(page);
          break;
        case 'Eolo Piu + Studio e Lavoro':
          await selezionaPacchettoStudioELavoro(page);
          break;
        case 'Eolo Piu + Intrattenimento + Studio e Lavoro + Sicurezza':
          await selezionaPacchettoIntrattenimento(page);
          await selezionaPacchettoStudioELavoro(page);
          await selezionaPacchettoSicurezza(page);
          break;
        case 'Eolo Piu + Intrattenimento + Studio e Lavoro':
          await selezionaPacchettoIntrattenimento(page);
          await selezionaPacchettoStudioELavoro(page);
          break;
        case 'Eolo Piu + Intrattenimento + Sicurezza':
          await selezionaPacchettoIntrattenimento(page);
          await selezionaPacchettoSicurezza(page);
          break;
        case 'Eolo Piu + Studio e Lavoro + Sicurezza':
          await selezionaPacchettoStudioELavoro(page);
          await selezionaPacchettoSicurezza(page);
          break;
        default:
          logger.warn(`pacchetto ${pacchetto} non gestito`);
          throw new Error(`pacchetto ${pacchetto} non gestito`);
      }
    }
  }
  logger.info(`selezionato pacchetto ${pacchetto}`);
}
