import { Page } from 'puppeteer';
import waitForSelectorAndClickEvaluated from '../../../browser/page/waitForSelectorAndClickEvaluated';
import { Logger } from '../../../logger';
import waitFormCoverRemoved from './waitFormCoverRemoved';

export default async function selectRouter(
  page: Page,
  router: string,
  logger: Logger
): Promise<void> {
  if (router === 'nessun_apparato_a_noleggio') {
    await waitForSelectorAndClickEvaluated(page, 'label[for="tblconfigurazione_accessori-0"]');
    await waitFormCoverRemoved(page);
  }
  logger.info(`selezionato router ${router}`);
}
