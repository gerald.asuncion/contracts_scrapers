import { Page } from 'puppeteer';
import waitForVisibleStyled from '../../../browser/page/waitForVisibleStyled';

export default async function clickOnFirstSuggestion(
  page: Page,
  suggestionSelector: string
): Promise<void> {
  await waitForVisibleStyled(page, suggestionSelector);

  // seleziono il primo elemento
  const suggestion = await page.waitForSelector(suggestionSelector);
  if (!suggestion) {
    throw new Error(`menu suggestion ${suggestionSelector} non trovato`);
  }

  const success = await suggestion.evaluate((ul) => {
    const li = ul.querySelector<HTMLElement>('li');
    if (!li) {
      return false;
    }
    li.click();
    return true;
  });

  if (!success) {
    throw new Error(`suggestion element ${suggestionSelector} non trovato`);
  }
}
