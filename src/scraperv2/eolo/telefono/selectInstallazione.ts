import { Page } from 'puppeteer';
import { Logger } from '../../../logger';

export default async function selectInstallazione(
  page: Page,
  tipoInstallazione: string,
  logger: Logger
): Promise<void> {
  // standard (default): #tblconfigurazione_installazione-0
  // rapida            : #tblconfigurazione_installazione-1
  if (tipoInstallazione.toLowerCase() === 'rapida') {
    await page.evaluate(() => {
      const el = document.querySelector<HTMLInputElement>('#tblconfigurazione_installazione-1');
      el?.click();
    });
  }
  logger.info(`configurata installazione ${tipoInstallazione}`);
}
