import { Page } from 'puppeteer';
import waitForSelectorAndClick from '../../../browser/page/waitForSelectorAndClick';
import waitForSelectorAndType from '../../../browser/page/waitForSelectorAndType';
import waitForTimeout from '../../../browser/page/waitForTimeout';
import waitForVisibleStyled from '../../../browser/page/waitForVisibleStyled';
import prevOperatorToSelectLabel from './prevOperatorToSelectLabel';

export default async function selectInternetOperator(
  page: Page,
  comboboxSelector: string,
  listContainerSelector: string,
  tipoContratto: string,
  precedenteOperatore: string,
  nomeAltroOperatore: string | null
): Promise<void> {
  await waitForSelectorAndClick(page, comboboxSelector);

  let label;
  const tipoContrattoLowerCase = tipoContratto.toLowerCase();
  if (tipoContrattoLowerCase === 'prima attivazione') {
    label = prevOperatorToSelectLabel('nessuna connessione');
  } else if (tipoContrattoLowerCase === 'switch') {
    label = prevOperatorToSelectLabel(precedenteOperatore);
  }

  if (label) {
    await Promise.race([
      waitForVisibleStyled(page, listContainerSelector),
      waitForTimeout(page, 500)
    ]);

    const listContainer = await page.waitForSelector(listContainerSelector);
    const success = await listContainer?.evaluate((ul, text) => {
      const li = ul.querySelector(`li[text="${text}"]`);
      if (!li) {
        return false;
      }
      (li as HTMLElement).click();
      return true;
    }, label);

    if (!success) {
      throw new Error("Non sono riuscito a selezionare l'operatore internet corrente. Elemento dom non trovato.");
    }

    if (tipoContratto === 'switch' && precedenteOperatore === 'altro') {
      if (!nomeAltroOperatore) {
        throw new Error('nome altro operatore internet non trovato');
      }
      await waitForSelectorAndType(page, '#adslName', nomeAltroOperatore);
    }
  }
}
