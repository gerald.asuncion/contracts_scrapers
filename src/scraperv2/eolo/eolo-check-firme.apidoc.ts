/**
 * @openapi
 *
 * /scraper/v2/eolo/check/firme:
 *  post:
 *    tags:
 *      - v2
 *    description:
 *      Controlla lo stato dei contratti passati.
 *    consume:
 *      - application/json
 *    produces:
 *      - application/json
 *    responses:
 *      200:
 *        description: SuccessResponse
 *    parameters:
 *      - in: body
 *        description: La lista di contratti da controllare.
 *        schema:
 *          type: object
 *          required:
 *            - lista
 *          properties:
 *            lista:
 *              type: array
 *              items:
 *                type: object
 *                properties:
 *                  idDatiContratto:
 *                    type: integer
 *                  codiceFiscale:
 *                    type: string
 */
