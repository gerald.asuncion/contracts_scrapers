import Eolo from './Eolo';
import { Contratto } from '../../contratti/Contratto';
import { ScraperResponse } from '../scraper';
import waitForVisible from '../../browser/page/waitForVisible';
import waitForXPathAndClick from '../../browser/page/waitForXPathAndClick';
import waitForXPathVisible from '../../browser/page/waitForXPathVisible';
import ContrattoPayload from '../../contratti/ContrattoPayload';
import { STATO_CONTRATTO } from '../../contratti/StatoContratto';
import GenericSuccessResponse from '../../response/GenericSuccessResponse';
import tryOrThrow from '../../utils/tryOrThrow';
import { QueueTask } from '../../task/QueueTask';
import acceptCookie from '../../browser/page/acceptCookie';

type EoloCheckFirmeV2ItemPayload = Pick<Contratto, 'codiceFiscale'> & Pick<ContrattoPayload, 'idDatiContratto'>;

export type EoloCheckFirmeV2Payload = {
  lista: EoloCheckFirmeV2ItemPayload[];
};

type RisultatoContratto = {
  stato: string;
  codiceContratto: string | null;
}

type EoloCheckSuccesResult = Record<number, RisultatoContratto>;

@QueueTask({ scraperName: 'eoloCheckFirme' })
export default class EoloCheckFirmeV2 extends Eolo {
  async scrapeWebsite({ lista }: EoloCheckFirmeV2Payload): Promise<ScraperResponse> {
    const page = await this.p();

    // vado alla pagina "servizi > contratti"
    await page.goto('https://area-riservata.eolo.it/ar-procacciatori/servizi-e-offerte/i-miei-servizi.html', { waitUntil: 'networkidle2' });
    await acceptCookie(page, '#onetrust-banner-sdk', '#onetrust-accept-btn-handler');

    // attendo che la pagina sia pronta
    await tryOrThrow(async () => {
      await waitForVisible(page, '.main-container');
      await waitForVisible(page, '#containerHeaderTableList');
    }, 'la tabella non è apparsa in tempo:');

    // faccio visualizzare 100 record
    await waitForXPathAndClick(page, '//p[contains(.,"Visualizza")]/following-sibling::div/div/div');
    await waitForXPathVisible(page, '//p[contains(.,"Visualizza")]/following-sibling::div/div/ul');
    await waitForXPathAndClick(page, '//p[contains(.,"Visualizza")]/following-sibling::div/div/ul/li[contains(.,"100")]');
    await waitForVisible(page, '.table-container', { timeout: 60000 });

    // prendo i dati per ogni contratto passato
    const results = await page.evaluate((contrattiDaControllare: EoloCheckFirmeV2ItemPayload[], stati) => {
      const records = Array.from(document.querySelectorAll('.table-container > .table-row.content'));

      return contrattiDaControllare.map<{ idDatiContratto: number; stato: string; codiceContratto: string | null }>(({ idDatiContratto, /* nome, cognome, */ codiceFiscale }) => {
        // const fullName = `${nome} ${cognome}`.toLowerCase();
        const record = records.find(record => record.textContent?.toLowerCase().includes(codiceFiscale.toLowerCase()));
        if (record) {
          const cellaContratto = Array.from(record.children).find(({ children }) => {
            for (const child of Array.from(children)) {
              if (child.textContent?.toLowerCase().includes('codice contratto')) {
                return true;
              }
            }

            return false;
          });

          if (cellaContratto) {
            // console.log(cellaContratto);

            const codiceContrattoEolo = cellaContratto.querySelector('p:not(.label)')?.textContent?.trim() || null;

            return {
              idDatiContratto,
              stato: stati.FIRMATO,
              codiceContratto: codiceContrattoEolo
            };
          }
        }

        return {
          idDatiContratto,
          stato: stati.DA_FIRMARE,
          codiceContratto: null
        };
      });
    }, lista, STATO_CONTRATTO);

    // console.log('results: ', results);
    const statoContratti: EoloCheckSuccesResult = {};
    for(const { idDatiContratto, ...rest } of results) {
      statoContratti[idDatiContratto] = rest;
    }

    return new GenericSuccessResponse<EoloCheckSuccesResult>(statoContratti);
  }
}
