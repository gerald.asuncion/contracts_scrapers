import { Page } from 'puppeteer';
import Eolo from './Eolo';
import { ScraperResponse } from '../scraper';
import ErrorResponse from '../../response/ErrorResponse';
import chiudiModaleInutile from './telefono/chiudiModaleInutile';
import checkIfMobileMenu from './telefono/checkIfMobileMenu';
import waitForSelectorAndClick from '../../browser/page/waitForSelectorAndClick';
import openMobileMenu from './telefono/openMobileMenu';
import waitForVisibleStyled from '../../browser/page/waitForVisibleStyled';
import { STATO_CONTRATTO } from '../../contratti/StatoContratto';
import createChildPayloadAwareLogger from '../../utils/createChildPayloadAwareLogger';
import GenericSuccessResponse from '../../response/GenericSuccessResponse';
import { Logger } from '../../logger';

interface EoloCheckOptions {
  codiceFastlaneArray: Array<string>;
}

interface RisultatoContratto {
  stato: string;
  codiceContratto: string | null;
}

type EoloCheckSuccesResult = Record<string, RisultatoContratto>;

/**
 * @deprecated
 * Il portale di Eolo non risponde nei tempi adeguati
 * Quindi abbiamo creato lo scraper EoloCheckFirmeV2 che fa uso di altra pagina
 */
export default class EoloCheck extends Eolo {
  /**
   * @override
   * @param args request payload
   */
  async scrapeWebsite({ codiceFastlaneArray, ...restArgs }: EoloCheckOptions): Promise<ScraperResponse> {
    const logger = createChildPayloadAwareLogger(this.childLogger, restArgs);

    logger.info('STEP selezione servizio');

    const page = await this.p();

    try {
      await chiudiModaleInutile(page);

      await waitForSelectorAndClick(page, '#main-container');

      logger.info('controllo se è visualizzato mobile');
      const useMobileMenu = await checkIfMobileMenu(page);

      if (useMobileMenu) {
        logger.info('apro il menu mobile');
        await openMobileMenu(page);
      }

      logger.info('apro la sezione servizi del menu');
      await waitForSelectorAndClick(page, '#servizi-e-offerte > a.mm-btn_next');
      await page.waitForSelector('#mm-2', { visible: true, timeout: 3000 });

      logger.info('clicco sul servizio i miei Fast Lane e attendo caricamento pagina');
      await Promise.all([
        waitForSelectorAndClick(page, '#mm-2 > ul > li:nth-child(2) > a'),
        page.waitForNavigation({ waitUntil: 'networkidle2' })
      ]);

      await page.waitForSelector('#search-mobile');
      logger.info('pagina controllo contratti caricata, procedo al check dei codici');

      const statoContratti: EoloCheckSuccesResult = {};
      for (const code of codiceFastlaneArray) {
        try {
          logger.info(`controllo lo stato del contratto relativo al codice fast lane ${code}`);
          // eslint-disable-next-line no-await-in-loop
          const statoContratto = await this.getRisultatoContratto(page, code, logger);
          logger.info(`contratto (${code}) risulta in stato ${JSON.stringify(statoContratto)}`);
          statoContratti[code] = statoContratto;
        } catch (statoContrattoEx: any) {
          // eslint-disable-next-line no-await-in-loop
          await this.saveScreenshot(page, logger, `eolo-check-${code}-error`);
          logger.error(statoContrattoEx.message);
        }
      }

      return new GenericSuccessResponse<EoloCheckSuccesResult>(statoContratti);
    } catch (ex: any) {
      return new ErrorResponse(ex.message);
    }
  }

  async getRisultatoContratto(page: Page, codiceFastlane: string, logger: Logger): Promise<RisultatoContratto> {
    let statoContratto: string = STATO_CONTRATTO.DA_FIRMARE;
    const codiceContratto: string | null = await this.getCodiceContratto(page, codiceFastlane, logger);

    if (codiceContratto) {
      statoContratto = STATO_CONTRATTO.FIRMATO;
    }

    return {
      stato: statoContratto,
      codiceContratto
    };
  }

  /**
   * Restituisce il codice contratto relativo al codice fastlane passato se trovato e valido,
   * altrimenti restituisce NULL
   * @param page           la pagina del browser
   * @param codiceFastlane il codice fastlane da controllare
   */
  async getCodiceContratto(page: Page, codiceFastlane: string, logger: Logger): Promise<string | null> {
    let codiceContratto: string | null = null;

    // devo fare così altrimenti il codice che si va ad inserire
    // si accoderebbe a quello inserito precedentemente
    await page.evaluate((code) => {
      const el = document.querySelector<HTMLInputElement>('#search-mobile');
      if (el) {
        el.value = code;
      }
    }, codiceFastlane);

    await waitForSelectorAndClick(
      page,
      '#main-content > div > div > div.row.sidebar-wrapper > aside > div > div.mobile-filter.d-lg-none > form > fieldset > div > a'
    );

    const btnSelector = '#table-container-items > div.table-container.table-fastlane > div.table-row.content > div.btn-cell > a';

    try {
      await page.waitForSelector(btnSelector);
      await waitForSelectorAndClick(page, btnSelector);

      await waitForVisibleStyled(
        page,
        '#table-container-items > div.table-container.table-fastlane > div.table-row.content.open > div:nth-child(8)'
      );

      const codiceContrattoCorrente = await page.evaluate((): string => <string>$('p:contains(Codice contratto)').next().text());

      logger.info(`per il codice fast lane ${codiceFastlane} recuperato codice contratto ${codiceContratto}`);

      if (codiceContrattoCorrente && codiceContrattoCorrente !== '-' && codiceContrattoCorrente.match(/\d{6}-\d{6}-\d{2}/g)?.length === 1) {
        codiceContratto = codiceContrattoCorrente;
      }

      // await this.saveScreenshot(page, logger, `eolo-check-stato-contratto-${codiceFastlane}`);
    } catch (ex: any) {
      const errMsg = typeof ex === 'string' ? ex : ex.message;
      if (errMsg.includes(btnSelector)) {
        throw new Error(`EoloCheck > contratto relativo al codice fast lane ${codiceFastlane} non trovato`);
      }

      throw new Error(errMsg);
    }

    return codiceContratto;
  }
}
