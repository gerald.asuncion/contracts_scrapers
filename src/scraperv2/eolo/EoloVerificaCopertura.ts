import ErrorResponse from "../../response/ErrorResponse";
import { ScraperResponse } from "../scraper";
import Eolo from "./Eolo";
import { SCRAPER_TIPOLOGIA } from "../ScraperOptions";
import createChildLogger from "../../utils/createChildLogger";
import FailureResponse from "../../response/FailureResponse";
import SuccessResponse from "../../response/SuccessResponse";
import { QueueTask } from "../../task/QueueTask";
import { EoloVerificaCoperturaPayload } from "./payload/EoloVerificaCoperturaPayload";
import EoloPayload from "./EoloPayload";

@QueueTask({ scraperName: 'eoloVerificaCopertura' })
export default class EoloVerificaCopertura extends Eolo {
    
    getScraperTipologia() {
        return SCRAPER_TIPOLOGIA.FATTIBILITA;
    }
    
    async scrapeWebsite(payload: EoloVerificaCoperturaPayload): Promise<ScraperResponse> {

        this.childLogger.info('STEP controllo copertura');
    
        const page = await this.p();

        
        await this.stepIniziale(page, this.childLogger);
        
        return await this.stepIndirizzoDiCopertura(page, payload, this.childLogger, true) as ScraperResponse;
    }
}