import Fastweb from './Fastweb';
import { ScraperResponse } from '../scraper';
import ErrorResponse from '../../response/ErrorResponse';
import FastwebLoginPayload from './payload/FastwebLoginPayload';
import FastwebCheckPayload from './payload/FastwebCheckPayload';
import vaiAllaPaginaConsultazioneFirma from './check-stato-contratto/vaiAllaPaginaConsultazioneFirma';
import controllaStatoCodiciContratto from './check-stato-contratto/controllaStatoCodiciContratto';
import GenericSuccessResponse from '../../response/GenericSuccessResponse';
import extractErrorMessage from '../../utils/extractErrorMessage';
import createChildPayloadAwareLogger from '../../utils/createChildPayloadAwareLogger';
import saveScreenshotOnAwsS3 from '../../browser/page/saveScreenshotOnAwsS3';
import { SCRAPER_TIPOLOGIA } from '../ScraperOptions';
import { QueueTask } from '../../task/QueueTask';

@QueueTask({ scraperName: 'fastwebCheckFirme' })
export default class FastwebCheck extends Fastweb<FastwebCheckPayload & FastwebLoginPayload> {
  /**
   * @override
   */
  async login(payload: FastwebCheckPayload & FastwebLoginPayload): Promise<void> {
    const { username, password } = await this.getCredenziali(payload, true);
    return super.login(<never>{ username, password });
  }

  /**
   * @override
   * @param payload Request payload
   */
  async scrapeWebsite({ codiceContrattoArray, ...rest }: FastwebCheckPayload): Promise<ScraperResponse> {
    const logger = createChildPayloadAwareLogger(this.childLogger, rest);
    let page = await this.p();

    try {
      page = await vaiAllaPaginaConsultazioneFirma(page, logger, this.controllaSessioneDisponibile);
      await page.bringToFront();

      const result: Record<string, string> = {};

      for await (const {
        codiceContratto,
        stato
      } of controllaStatoCodiciContratto(page, codiceContrattoArray, logger)) {
        logger.info(`per il codice contratto ${codiceContratto} recuperato lo stato ${stato}`);
        result[codiceContratto] = stato;
      }

      await this.logout(page, logger);

      logger.info(`restituisco ${JSON.stringify(result)}`);
      return new GenericSuccessResponse<Record<string, string>>(result);
    } catch (ex) {
      await saveScreenshotOnAwsS3(this.s3ClientFactory, page, `${this.constructor.name}-errore`, logger);
      const errMsg = extractErrorMessage(ex);
      logger.error(errMsg);
      await this.logout(page, logger);
      return new ErrorResponse(errMsg);
    }
  }

  /**
   * @override
   */
  getScraperCodice() {
    return 'fastweb-checkfirme';
  }

  /**
   * @override
   */
  getScraperTipologia() {
    return SCRAPER_TIPOLOGIA.FATTIBILITA as any;
  }
}
