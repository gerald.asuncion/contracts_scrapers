import { FastwebVerificaCoperturaPayload } from './payload/FastwebCheckFattibilitaPayload';
import { ScraperResponse } from '../scraper';
import ErrorResponse from '../../response/ErrorResponse';
import FailureResponse from '../../response/FailureResponse';
import vaiAllaPaginaInserisciOrdine from './fattibilita/vaiAllaPaginaInserisciOrdine';
import extractErrorMessage from '../../utils/extractErrorMessage';
import checkCopertura from './fattibilita/copertura/checkCopertura';
import FastwebVerificaScraper from './fattibilita/FastwebVerificaScraper';
import createChildPayloadAwareLogger from '../../utils/createChildPayloadAwareLogger';
import { QueueTask } from '../../task/QueueTask';

@QueueTask()
export default class FastwebVerificaCopertura extends FastwebVerificaScraper<FastwebVerificaCoperturaPayload> {
  /**
   * @override
   * @param payload Request payload
   */
  async scrapeWebsite(payload: FastwebVerificaCoperturaPayload): Promise<ScraperResponse> {
    const logger = createChildPayloadAwareLogger(this.childLogger, payload);
    let page;
    const { idDatiContratto } = payload;

    try {
      page = await this.p();

      await vaiAllaPaginaInserisciOrdine(page, logger, this.controllaSessioneDisponibile);

      const result = await checkCopertura(page, payload, logger);

      if (result instanceof FailureResponse) {
        await this.saveScreenshot(page, logger, 'fastweb-verifica-copertura', idDatiContratto);
      }
      await this.logout(page, logger);

      return result;
    } catch (ex) {
      const errMsg = extractErrorMessage(ex);
      logger.error(`${errMsg}`);
      if (page) {
        try {
          await this.saveScreenshot(page, logger, 'fastweb-verifica-copertura-errore', idDatiContratto);
        } catch (e) {}

        try {
          await this.logout(page, logger);
        } catch (e) {
          console.error(e);
        }
      }
      return new FailureResponse(errMsg);
    }
  }

  /**
   * @override
   */
  getScraperCodice() {
    return 'fastweb-verifica-copertura';
  }
}
