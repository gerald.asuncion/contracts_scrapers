import { Page } from 'puppeteer';
import ifEnabledExecute from '../../../browser/page/ifEnabledExecute';
import inserisciDatoConSuggestion from './inserisciDatoConSuggestion';

export default async function inserisciDatoConSuggestionSeAbilitato(
  page: Page,
  selector: string,
  dato: string,
  fieldName: string
): Promise<void> {
  await ifEnabledExecute(page, selector, async () => {
    await inserisciDatoConSuggestion(page, selector, dato, fieldName);
  });
}
