import { Page } from 'puppeteer';
import inserisciDatoConSuggestionAndApostrophe from '../../inserisciDatoConSuggestionAndApostrophe';

const selector = '#datiAnagraficiRES > div:nth-child(3) > div:nth-child(2) > div.col-md-4.has-error > input';

export default async function inserisciComuneDiNascita(page: Page, nascitaComune: string): Promise<void> {
  await inserisciDatoConSuggestionAndApostrophe(page, selector, nascitaComune, 'comune di nascita', false);
}
