import { Page } from 'puppeteer';
import { FastwebVerificaMorositaPayload } from '../../../payload/FastwebCheckFattibilitaPayload';
import { Logger } from '../../../../../logger';
import waitForSelectorAndType from '../../../../../browser/page/waitForSelectorAndType';
import attendiSparizioneOverlay from '../../attendiSparizioneOverlay';
import waitForSelectorAndClick from '../../../../../browser/page/waitForSelectorAndClick';
import inserisciStatoDiNascita from './inserisciStatoDiNascita';
import inserisciComuneDiNascita from './inserisciComuneDiNascita';
import inserisciProvinciaDiNascita from './inserisciProvinciaDiNascita';
import waitForSelectorAndClickEvaluated from '../../../../../browser/page/waitForSelectorAndClickEvaluated';
import waitForVisible from '../../../../../browser/page/waitForVisible';
import extractErrorMessage from '../../../../../utils/extractErrorMessage';

export default async function inserisciDatiFiscali(
  page: Page,
  {
    nome,
    cognome,
    codiceFiscale,
    sesso,
    dataNascita,
    nascitaProvinciaEstera,
    nascitaStato,
    nascitaComune,
    nascitaProvincia,
    cellulare,
    email
  }: FastwebVerificaMorositaPayload,
  logger: Logger,
  logMsgPrefix: string
): Promise<void> {
  logger.info(`${logMsgPrefix} inserisco il codice fiscale`);
  await waitForSelectorAndType(page, '#datiAnagraficiRES > div:nth-child(1) > div:nth-child(3) > input', codiceFiscale);

  await attendiSparizioneOverlay(page);

  try {
    logger.info('controllo se è apparsa la modale d\'errore sul codice fiscale');
    // eslint-disable-next-line max-len
    const codiceFiscaleModalErrorSelector = 'uib-accordion > [role="tablist"] > .panel-success[is-open="true"] > .in[aria-expanded="true"] > .panel-body > div:nth-child(1)';
    await waitForVisible(page, codiceFiscaleModalErrorSelector, { timeout: 1000 });

    // lo split su '\n' serve per togliere dal messaggio la stringa 'Per continuare clicca "Conferma"'
    const [errMsg] = (await page.$eval(codiceFiscaleModalErrorSelector, (el) => el.textContent?.trim() || '')).split('\n');

    throw new Error(errMsg.trim());
  } catch (exCodiceFiscale) {
    // nulla da fare
    const msg = extractErrorMessage(exCodiceFiscale);
    if (!msg.toLowerCase().includes('timeout')) {
      throw exCodiceFiscale;
    }
  }

  logger.info(`${logMsgPrefix} inserisco il nome`);
  await waitForSelectorAndType(page, '#datiAnagraficiRES > div:nth-child(1) > div:nth-child(1) > input', nome);

  logger.info(`${logMsgPrefix} inserisco il cognome`);
  await waitForSelectorAndType(page, '#datiAnagraficiRES > div:nth-child(1) > div:nth-child(2) > input', cognome);

  logger.info(`${logMsgPrefix} inserisco il sesso`);
  await waitForSelectorAndClickEvaluated(page, `input[name="section1_sesso"][value="${sesso.toUpperCase()}"]`);

  logger.info(`${logMsgPrefix} inserisco la data di nascita`);
  await waitForSelectorAndType(page, '#datiAnagraficiRES > div:nth-child(2) > div:nth-child(2) > p > input', dataNascita);

  logger.info(`${logMsgPrefix} check su provincia di nascita estera "${nascitaProvinciaEstera}"`);
  const nascitaProvinciaEsteraBool = nascitaProvinciaEstera.toLowerCase() === 'si' ? 'true' : 'false';
  await waitForSelectorAndClick(page, `input[name="section1_natoAllEstero"][value="${nascitaProvinciaEsteraBool}"]`);

  logger.info(`${logMsgPrefix} inserisco lo stato di nascita`);
  await inserisciStatoDiNascita(page, nascitaStato);

  if (nascitaProvinciaEstera === 'no') {
    logger.info(`${logMsgPrefix} inserisco il comune di nascita`);
    await inserisciComuneDiNascita(page, nascitaComune);

    logger.info(`${logMsgPrefix} inserisco la provincia di nascita`);
    await inserisciProvinciaDiNascita(page, nascitaProvincia);
  }

  logger.info(`${logMsgPrefix} clicco sul pulsante verifica codice fiscale`);
  await waitForSelectorAndClick(page, '#datiAnagraficiRES > div:nth-child(3) > div.col-md-2 > button');

  await attendiSparizioneOverlay(page);

  try {
    logger.info('controllo se è apparsa la modale d\'errore sul codice fiscale');
    // eslint-disable-next-line max-len
    const codiceFiscaleModalErrorSelector = 'div.alert-danger div span';
    await waitForVisible(page, codiceFiscaleModalErrorSelector, { timeout: 1000 });

    // lo split su '\n' serve per togliere dal messaggio la stringa 'Per continuare clicca "Conferma"'
    const [errMsg] = (await page.$eval(codiceFiscaleModalErrorSelector, (el) => el.textContent?.trim() || '')).split('\n');

    throw new Error(errMsg.trim());
  } catch (exCodiceFiscale) {
    // nulla da fare
    const msg = extractErrorMessage(exCodiceFiscale);
    if (!msg.toLowerCase().includes('timeout')) {
      throw exCodiceFiscale;
    }
  }

  logger.info(`${logMsgPrefix} inserisco il cellulare`);
  await waitForSelectorAndType(page, '#datiAnagraficiRESSHP > div > div:nth-child(1) > input', cellulare);

  logger.info(`${logMsgPrefix} inserisco l'email`);
  await waitForSelectorAndType(page, '#datiAnagraficiRESSHP > div > div:nth-child(2) > input', email);

  logger.info(`${logMsgPrefix} inserisco l'email di conferma`);
  await waitForSelectorAndType(page, '#datiAnagraficiRESSHP > div > div:nth-child(3) > input', email);

  await attendiSparizioneOverlay(page);

  try {
    logger.info('controllo se è apparsa la modale d\'errore sulla mail');
    // eslint-disable-next-line max-len
    const emailErrorSelector = 'div.alert-danger div span';
    await waitForVisible(page, emailErrorSelector, { timeout: 1000 });

    // lo split su '\n' serve per togliere dal messaggio la stringa 'Per continuare clicca "Conferma"'
    const [errMsg] = (await page.$eval(emailErrorSelector, (el) => el.textContent?.trim() || '')).split('\n');

    throw new Error(errMsg.trim());
  } catch (exCodiceFiscale) {
    // nulla da fare
    const msg = extractErrorMessage(exCodiceFiscale);
    if (!msg.toLowerCase().includes('timeout')) {
      throw exCodiceFiscale;
    }
  }
}
