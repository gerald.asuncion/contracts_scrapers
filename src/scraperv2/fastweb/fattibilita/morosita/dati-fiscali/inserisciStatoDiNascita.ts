import { Page } from 'puppeteer';
import inserisciDatoConSuggestion from '../../inserisciDatoConSuggestion';

const selector = '#datiAnagraficiRES > div:nth-child(3) > div.col-md-4.has-error > input';

const MAPPA: Record<string, string> = {
  'unione sovietica': 'unione repubbliche socialiste sovietiche',
  'federazione russa': 'russia'
};

export default async function inserisciStatoDiNascita(page: Page, nascitaStato: string): Promise<void> {
  const lowered = nascitaStato.toLowerCase();
  await inserisciDatoConSuggestion(page, selector, MAPPA[lowered] || nascitaStato, 'stato di nascita', false);
}
