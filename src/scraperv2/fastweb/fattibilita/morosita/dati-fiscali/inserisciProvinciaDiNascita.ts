import { Page } from 'puppeteer';
import inserisciDatoConSuggestionSeAbilitato from '../../inserisciDatoConSuggestionSeAbilitato';

const selector = '#datiAnagraficiRES > div:nth-child(3) > div:nth-child(2) > div.col-md-2 > input';

export default async function inserisciProvinciaDiNascita(page: Page, nascitaProvincia: string): Promise<void> {
  await inserisciDatoConSuggestionSeAbilitato(page, selector, nascitaProvincia, 'provincia di nascita');
}
