import { Page } from 'puppeteer';
import inserisciDatoConSuggestionSeAbilitato from '../../inserisciDatoConSuggestionSeAbilitato';

const selector = '#datiIndirizzoRES_FissoMobile > div:nth-child(2) > div:nth-child(2) > input';

export default async function inserisciProvinciaResidenza(page: Page, attivazioneProvincia: string): Promise<void> {
  await inserisciDatoConSuggestionSeAbilitato(page, selector, attivazioneProvincia, 'provincia di residenza');
}
