import { Page } from 'puppeteer';
import { FastwebVerificaCoperturaPayload } from '../../../payload/FastwebCheckFattibilitaPayload';
import { Logger } from '../../../../../logger';
import inserisciComuneResidenza from './inserisciComuneResidenza';
import inserisciIndirizzoResidenza from './inserisciIndirizzoResidenza';
import inserisciCivicoResidenza from './inserisciCivicoResidenza';
import inserisciCapResidenza from './inserisciCapResidenza';
import inserisciProvinciaResidenza from './inserisciProvinciaResidenza';
import waitForSelectorAndClick from '../../../../../browser/page/waitForSelectorAndClick';
import attendiSparizioneOverlay from '../../attendiSparizioneOverlay';
import { ScraperResponse } from '../../../../scraper';
import FailureResponse from "../../../../../response/FailureResponse";
import SuccessResponse from "../../../../../response/SuccessResponse";
import isDisabled from '../../../../../browser/page/isDisabled';
import waitForTimeout from '../../../../../browser/page/waitForTimeout';
import recuperaEsitoCheck from '../../recuperaEsitoCheck';

export default async function inserisciDatiResidenza(
  page: Page,
  {
    attivazioneComune,
    attivazioneIndirizzo,
    attivazioneCivico,
    attivazioneCap,
    attivazioneProvincia
  }: FastwebVerificaCoperturaPayload,
  logger: Logger,
  closeModalError: boolean,
  logMsgPrefix: string
): Promise<ScraperResponse> {
  logger.info(`${logMsgPrefix} inserisco città di residenza`);
  await inserisciComuneResidenza(page, attivazioneComune);

  logger.info(`${logMsgPrefix} inserisco indirizzo di residenza`);
  await inserisciIndirizzoResidenza(page, attivazioneIndirizzo);

  logger.info(`${logMsgPrefix} inserisco civico residenza`);
  await inserisciCivicoResidenza(page, attivazioneCivico.split('/')[0], attivazioneCivico.split('/')[1]);

  logger.info(`${logMsgPrefix} inserisco cap residenza`);
  await inserisciCapResidenza(page, attivazioneCap);

  logger.info(`${logMsgPrefix} inserisco provincia residenza`);
  await inserisciProvinciaResidenza(page, attivazioneProvincia);

  logger.info(`${logMsgPrefix} clicco sul pulsante verifica dati fiscali`);
  const verificaBtnSelector = '#verificaDatiMobileId > div > div.col-md-4 > button';
  if (await isDisabled(page, verificaBtnSelector)) {
    throw new Error('impossibile completare la verifica dei dati di residenza pulsante `verifica dati fiscali` disabilitato');
  }
  await waitForSelectorAndClick(page, verificaBtnSelector);

  await attendiSparizioneOverlay(page);

  try {
    logger.info(`${logMsgPrefix} attendo apparizione modale d'errore`);
    await page.waitForSelector(
      'body > div.modal.fade.ng-isolate-scope.in > div > div > div.alert.ng-scope.ng-isolate-scope.alert-danger',
      { timeout: 3000 }
    );
    const message = await page.$eval(
      'body > div.modal.fade.ng-isolate-scope.in > div > div > div.alert.ng-scope.ng-isolate-scope.alert-danger > div > span',
      (el) => el.textContent?.trim()
    );

    logger.info(`${logMsgPrefix} apparsa modale d'errore con messaggio: ${message}`);

    if (closeModalError) {
      await waitForSelectorAndClick(page, 'body > div.modal.fade.ng-isolate-scope.in > div > div > div.modal-footer.ng-scope > button');
    }

    return new FailureResponse(message);
  } catch (ex) {
    // modale d'errore non apparsa
  }

  await waitForTimeout(page, 1000);

  const esito = await recuperaEsitoCheck(page, '#verificaDatiMobileId > div > div.col-md-8 > textarea');
  logger.info(`${logMsgPrefix} modale d'errore non apparsa, recuperato esito ${esito}`);

  if (esito?.includes('cliente acquisibile')) {
    return new SuccessResponse('');
  }

  return new FailureResponse(esito);
}
