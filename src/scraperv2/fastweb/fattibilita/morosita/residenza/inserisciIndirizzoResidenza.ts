import { Page } from 'puppeteer';
import inserisciDatoConSuggestion from '../../inserisciDatoConSuggestion';

const selector = '#datiIndirizzoRES_FissoMobile > div:nth-child(1) > div:nth-child(2) > input';

export default async function inserisciIndirizzoResidenza(page: Page, attivazioneIndirizzo: string): Promise<void> {
  await inserisciDatoConSuggestion(page, selector, attivazioneIndirizzo, 'indirizzo di residenza', false);
}
