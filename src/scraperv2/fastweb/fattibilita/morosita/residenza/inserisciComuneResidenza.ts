import { Page } from 'puppeteer';
import inserisciDatoConSuggestionAndApostrophe from '../../inserisciDatoConSuggestionAndApostrophe';

const selector = '#datiIndirizzoRES_FissoMobile > div:nth-child(1) > div:nth-child(1) > input';

export default async function inserisciComuneResidenza(page: Page, attivazioneComune: string): Promise<void> {
  await inserisciDatoConSuggestionAndApostrophe(page, selector, attivazioneComune, 'comune residenza', false);
}
