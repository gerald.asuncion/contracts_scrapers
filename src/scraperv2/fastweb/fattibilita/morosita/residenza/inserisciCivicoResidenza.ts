import { Page } from 'puppeteer';
import inserisciDatoConSuggestion from '../../inserisciDatoConSuggestion';
import selectSuggestion from '../../../helpers/selectSuggestion';

const selector = '#datiIndirizzoRES_FissoMobile > div:nth-child(1) > div:nth-child(3) > input';

export default async function inserisciCivicoResidenza(page: Page, attivazioneCivico: string, civico2?: string): Promise<void> {
  await inserisciDatoConSuggestion(page, selector, attivazioneCivico, 'civico di residenza', false, civico2);
}

export async function inserisciCivicoResidenzaV2(page: Page, civico: string) {
  await selectSuggestion(page, selector, civico.split('/')[0], civico, 'civico di residenza');
}
