import { Page } from 'puppeteer';
import waitForSelectorAndType from '../../../../../browser/page/waitForSelectorAndType';

const selector = '#datiIndirizzoRES_FissoMobile > div:nth-child(2) > div:nth-child(1) > input';

export default async function inserisciCapResidenza(page: Page, attivazioneCap: string): Promise<void> {
  const currentValue = await page.$eval(selector, (el) => (el as HTMLInputElement).value?.trim());

  if (!currentValue) {
    await waitForSelectorAndType(page, selector, attivazioneCap);
  }
}
