import { Page } from 'puppeteer';
import { FastwebVerificaMorositaPayload } from '../../payload/FastwebCheckFattibilitaPayload';
import { Logger } from '../../../../logger';
import { ScraperResponse } from '../../../scraper';
import inserisciDatiFiscali from './dati-fiscali/inserisciDatiFiscali';
import inserisciDatiResidenza from './residenza/inserisciDatiResidenza';

export default async function checkMorosita(
  page: Page,
  payload: FastwebVerificaMorositaPayload,
  logger: Logger,
  logMsgPrefix: string,
  closeModalError = false
): Promise<ScraperResponse> {
  await inserisciDatiFiscali(page, payload, logger, `${logMsgPrefix} - dati fiscali >`);

  return inserisciDatiResidenza(page, payload, logger, closeModalError, `${logMsgPrefix} - dati residenza >`);
}
