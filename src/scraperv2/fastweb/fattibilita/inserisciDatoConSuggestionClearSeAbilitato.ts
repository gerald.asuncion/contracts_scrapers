import { Page } from 'puppeteer';
import ifEnabledExecute from '../../../browser/page/ifEnabledExecute';
import inserisciDatoConSuggestionClear from './inserisciDatoConSuggestionClear';

export default async function inserisciDatoConSuggestionClearSeAbilitato(
  page: Page,
  selector: string,
  dato: string,
  fieldName: string
): Promise<void> {
  await ifEnabledExecute(page, selector, async () => {
    await inserisciDatoConSuggestionClear(page, selector, dato, fieldName);
  });
}
