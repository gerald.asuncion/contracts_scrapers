import { Page } from 'puppeteer';
import waitForTimeout from '../../../browser/page/waitForTimeout';
import extractErrorMessage from '../../../utils/extractErrorMessage';
import getDropdownMenuItemSelector from '../helpers/getDropdownMenuItemSelector';
import getDropdownMenuSelector from '../helpers/getDropdownMenuSelector';
import getPreValue from '../helpers/getPreValue';
import attendiSparizioneOverlay from './attendiSparizioneOverlay';

export default async function inserisciDatoConSuggestionClear(
  page: Page,
  selector: string,
  dato: string,
  fieldName: string,
  pressEnter = true
): Promise<void> {
  await page.waitForSelector(selector);
  const input = await page.$(selector);
  await input?.click({ clickCount: 3 })
  await input?.type(getPreValue(dato.split('-')[0]));
  if (pressEnter) {
    await page.keyboard.press('Enter');
  }

  await attendiSparizioneOverlay(page);

  const menuSelector = getDropdownMenuSelector(selector);

  const menuItemSelector = getDropdownMenuItemSelector(menuSelector);

  try {
    await page.waitForFunction(
      (internalSelector: string) => $(internalSelector).length !== 0,
      { polling: 100, timeout: 30000 },
      menuItemSelector
    );
  } catch (ex) {
    const errMsg = extractErrorMessage(ex);
    if (!errMsg.toLowerCase().includes('timeout')) {
      throw ex;
    }

    throw new Error(`Impossibile selezionare "${dato}" nel campo "${fieldName}"`);
  }

  await waitForTimeout(page, 4000);

  await page.evaluate((internalSelector: string, valoreDaSelezionare: string) => {
    function comparaTesti(primo: string, secondo: string): boolean {
      // eslint-disable-next-line newline-per-chained-call
      const left = primo.toLowerCase().normalize('NFD').replace(/[\u0300-\u036f]/g, '').replace(/'/g, ' ').trim();
      // eslint-disable-next-line newline-per-chained-call
      const right = secondo.toLowerCase().normalize('NFD').replace(/[\u0300-\u036f]/g, '').replace(/'/g, ' ').trim();

      return left === right;
    }

    const $el = $(internalSelector);

    if ($el.length) {
      const ul = $el[0];

      const list = [...ul.querySelectorAll<HTMLLIElement>('li')];

      list.find((li) => comparaTesti(valoreDaSelezionare, li.textContent?.trim() || ''))?.click();
    }
  }, menuSelector, dato.toLowerCase());
}
