import { Page } from 'puppeteer';
import { FastwebVerificaDocumentoPayload } from '../../payload/FastwebCheckFattibilitaPayload';
import {
  ScraperResponse} from '../../../scraper';
import FailureResponse from "../../../../response/FailureResponse";
import SuccessResponse from "../../../../response/SuccessResponse";
import { Logger } from '../../../../logger';
import waitForSelectorAndClick from '../../../../browser/page/waitForSelectorAndClick';
import waitForSelectorAndType from '../../../../browser/page/waitForSelectorAndType';
import inserisciNazionalitaDocumento from './inserisciNazionalitaDocumento';
import inserisciLocalitaRilascioDocumento from './inserisciLocalitaRilascioDocumento';
import waitForVisible from '../../../../browser/page/waitForVisible';
import selezionaTipoDocumento from './selezionaTipoDocumento';
import selezionaEnteEmittente from './selezionaEnteEmittente';
import inserisciProvinciaRilascioDocumento from './inserisciProvinciaRilascioDocumento';
import isDisabled from '../../../../browser/page/isDisabled';
import waitForSelectorAndClickEvaluated from '../../../../browser/page/waitForSelectorAndClickEvaluated';
import recuperaEsitoCheck from '../recuperaEsitoCheck';
import attendiSparizioneOverlay from '../attendiSparizioneOverlay';

const LOG_MSG_PREFIX = 'check documento >';

export default async function checkDocumento(
  page: Page,
  {
    documentoTipo,
    documentoNumero,
    documentoRilasciatoDa,
    documentoEnteRilascio,
    documentoDataRilascio,
    documentoProvinciaRilascio
  }: FastwebVerificaDocumentoPayload,
  logger: Logger,
  closeModalError = false
): Promise<ScraperResponse> {
  // eslint-disable-next-line max-len
  logger.info(`${LOG_MSG_PREFIX} apro il pannello per inserire i dati del documento ed attendo che sia visibile il suo contenuto`);
  await waitForSelectorAndClick(
    page,
    '[heading="Documenti e Selezione Consensi"] > div:nth-child(2) > .panel-body > span:nth-child(1) a[role="button"]'
  );

  await waitForVisible(page, 'input[name="section4_dataDiRilascioDocumento"]');

  logger.info(`${LOG_MSG_PREFIX} inserisco nazionalità documento "Italia"`);
  await inserisciNazionalitaDocumento(page);

  logger.info(`${LOG_MSG_PREFIX} seleziono il tipo di documento`);
  await selezionaTipoDocumento(page, documentoTipo);

  logger.info(`${LOG_MSG_PREFIX} inserisco il numero del documento`);
  await waitForSelectorAndType(page, 'input[name="section4_numeroDocumento"]', documentoNumero);

  logger.info(`${LOG_MSG_PREFIX} inserisco località di rilascio documento`);
  await inserisciLocalitaRilascioDocumento(page, documentoRilasciatoDa);

  logger.info(`${LOG_MSG_PREFIX} seleziono l'ente emittente`);
  await selezionaEnteEmittente(page, documentoEnteRilascio);

  logger.info(`${LOG_MSG_PREFIX} inserisco data di rilascio documento`);
  await waitForSelectorAndType(page, 'input[name="section4_dataDiRilascioDocumento"]', documentoDataRilascio);

  logger.info(`${LOG_MSG_PREFIX} inserisco provincia rilascio documento`);
  await inserisciProvinciaRilascioDocumento(page, documentoProvinciaRilascio);

  // eslint-disable-next-line max-len
  const btnSelector = '[heading="Documenti e Selezione Consensi"] > div:nth-child(2) > .panel-body > span:nth-child(1) .panel-body > fieldset > div:nth-child(4) button';

  if (await isDisabled(page, btnSelector)) {
    throw new Error('impossibile verificare il documento, pulsante disabilitato');
  }

  logger.info(`${LOG_MSG_PREFIX} clicco sul pulsante verifica documento`);
  await waitForSelectorAndClickEvaluated(
    page,
    btnSelector
  );

  await attendiSparizioneOverlay(page/* , { timeout: 3000 } */);

  try {
    logger.info(`${LOG_MSG_PREFIX} attendo apparizione modale d'errore`);
    const modalErrSelector = 'body > div.modal.fade.ng-isolate-scope.in > div > div > div.alert.ng-scope.ng-isolate-scope.alert-danger';
    await page.waitForSelector(modalErrSelector, { timeout: 3000 });
    const message = await page.$eval(
      'body > div.modal.fade.ng-isolate-scope.in > div > div > div.alert.ng-scope.ng-isolate-scope.alert-danger > div > span',
      (el) => el.textContent?.trim()
    );

    logger.info(`${LOG_MSG_PREFIX} apparsa modale d'errore con messaggio: ${message}`);

    if (closeModalError) {
      await waitForSelectorAndClick(page, 'body > div.modal.fade.ng-isolate-scope.in > div > div > div.modal-footer.ng-scope > button');
    }

    return new FailureResponse(message);
  } catch (ex) {
    // modale d'errore non apparsa
  }

  await attendiSparizioneOverlay(page/* , { timeout: 3000 } */);

  const esito = await recuperaEsitoCheck(page, '[heading="Documenti e Selezione Consensi"] textarea');
  logger.info(`${LOG_MSG_PREFIX} modale d'errore non apparsa, recuperato esito ${esito}`);

  if (esito?.includes('documento verificato')) {
    return new SuccessResponse('');
  }

  return new FailureResponse(esito);
}
