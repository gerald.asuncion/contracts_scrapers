import { Page } from 'puppeteer';
import waitForSelectorAndSelect from '../../../../browser/page/waitForSelectorAndSelect';

const TIPO_DOCUMENTO_VALUE: Record<string, string> = {
  "carta d'identità": 'CARTA DI IDENTITA',
  passaporto: 'PASSAPORTO',
  patente: 'PATENTE'
};

export default async function selezionaTipoDocumento(page: Page, documentoTipo: string): Promise<void> {
  const value = TIPO_DOCUMENTO_VALUE[documentoTipo.toLowerCase()];
  if (!value) {
    throw new Error(`tipo documento "${documentoTipo}" non valido`);
  }

  await waitForSelectorAndSelect(page, '#section4_tipoDocumento', [value]);
}
