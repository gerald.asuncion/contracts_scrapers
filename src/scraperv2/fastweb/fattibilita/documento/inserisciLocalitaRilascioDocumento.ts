import { Page } from 'puppeteer';
import inserisciDatoConSuggestionAndApostrophe from '../inserisciDatoConSuggestionAndApostrophe';

const selector = 'input[name="section4_localitaDiRilascioDocumento"]';

export default async function inserisciLocalitaRilascioDocumento(page: Page, documentoRilasciatoDa: string): Promise<void> {
  await inserisciDatoConSuggestionAndApostrophe(page, selector, documentoRilasciatoDa, 'documento rilasciato da', false);
}
