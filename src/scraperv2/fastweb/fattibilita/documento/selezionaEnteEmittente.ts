import { Page } from 'puppeteer';
import waitForSelectorAndSelect from '../../../../browser/page/waitForSelectorAndSelect';

const ENTE_EMITTENTE_VALUE: Record<string, string> = {
  motorizzazione: 'MOTORIZZAZIONE',
  prefettura: 'PREFETTURA',
  comune: 'U.C.O.',
  'u.c.o.': 'U.C.O.'
};

export default async function selezionaEnteEmittente(page: Page, documentoEnteRilascio: string): Promise<void> {
  const value = ENTE_EMITTENTE_VALUE[documentoEnteRilascio.toLowerCase()];
  if (!value) {
    throw new Error(`ente emittente documento "${documentoEnteRilascio}" non valido`);
  }

  await waitForSelectorAndSelect(page, '#section4_enteEmittente', [value]);
}
