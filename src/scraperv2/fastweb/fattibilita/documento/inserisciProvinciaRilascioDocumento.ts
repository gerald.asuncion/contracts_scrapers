import { Page } from 'puppeteer';
import inserisciDatoConSuggestionSeAbilitato from '../inserisciDatoConSuggestionSeAbilitato';

const selector = 'input[name="section4_provinciaDiRilascioDocumento"]';

export default async function inserisciProvinciaRilascioDocumento(page: Page, documentoProvinciaRilascio: string): Promise<void> {
  await inserisciDatoConSuggestionSeAbilitato(page, selector, documentoProvinciaRilascio, 'provincia rilascio documento');
}
