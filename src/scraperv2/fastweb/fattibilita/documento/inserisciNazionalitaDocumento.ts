import { Page } from 'puppeteer';
import inserisciDatoConSuggestion from '../inserisciDatoConSuggestion';

const selector = 'input[name="section4_nazionalitaDocumento"]';

export default async function inserisciNazionalitaDocumento(page: Page, documentoNazionalita = 'italia'): Promise<void> {
  await inserisciDatoConSuggestion(page, selector, documentoNazionalita, 'nazionalità documento', false);
}
