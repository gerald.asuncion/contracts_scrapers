import { Page } from 'puppeteer';
import waitForHiddenStyled from '../../../browser/page/waitForHiddenStyled';

export default async function attendiSparizioneOverlay(
  page: Page,
  waitOptions?: {
    timeout?: number;
    polling?: string | number;
  }
): Promise<void> {
  await waitForHiddenStyled(page, '#overlay', waitOptions);
}
