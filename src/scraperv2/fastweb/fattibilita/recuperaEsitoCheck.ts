import { Page } from 'puppeteer';

export default async function recuperaEsitoCheck(page: Page, selector: string, retry = 3): Promise<string> {
  if (retry <= 0) {
    throw new Error("non sono riuscito a recuperare l'esito");
  }

  let esito = await page.$eval(
    selector,
    (el) => (el as HTMLTextAreaElement).value.trim().toLowerCase()
  );

  if (!esito) {
    esito = await recuperaEsitoCheck(page, selector, retry - 1);
  }

  return esito;
}
