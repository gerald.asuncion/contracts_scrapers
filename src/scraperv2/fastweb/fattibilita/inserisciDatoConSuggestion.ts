import { Page } from 'puppeteer';
import waitForSelectorAndType from '../../../browser/page/waitForSelectorAndType';
import attendiSparizioneOverlay from './attendiSparizioneOverlay';
import extractErrorMessage from '../../../utils/extractErrorMessage';
import waitForTimeout from '../../../browser/page/waitForTimeout';
import getDropdownMenuSelector from '../helpers/getDropdownMenuSelector';
import getDropdownMenuItemSelector from '../helpers/getDropdownMenuItemSelector';
import getPreValue from '../helpers/getPreValue';
import waitForVisible from "../../../browser/page/waitForVisible";
import waitForSelectorAndClick from "../../../browser/page/waitForSelectorAndClick";

export default async function inserisciDatoConSuggestion(
  page: Page,
  selector: string,
  dato: string,
  fieldName: string,
  pressEnter = true,
  secondPart?: string,
): Promise<void> {
  await waitForSelectorAndType(page, selector, getPreValue(dato.split('-')[0]));
  if (pressEnter) {
    await page.keyboard.press('Enter');
  }

  await attendiSparizioneOverlay(page);

  const menuSelector = getDropdownMenuSelector(selector);

  const menuItemSelector = getDropdownMenuItemSelector(menuSelector);

  try {
    await page.waitForFunction(
      (internalSelector: string) => $(internalSelector).length !== 0,
      { polling: 100, timeout: 30000 },
      menuItemSelector
    );
  } catch (ex) {
    let modalError = false;
    try {
      await waitForVisible(page, 'div.alert-danger', {timeout: 2000});
      modalError = true;
    } catch (e) {
      modalError = false;
    }
    if (!modalError) {
      const errMsg = extractErrorMessage(ex);
      if (!errMsg.toLowerCase().includes('timeout')) {
        throw ex;
      }
    }

    throw new Error(`Impossibile selezionare "${dato}" nel campo "${fieldName}"`);
  }

  await waitForTimeout(page, 4000);

  const terzo: string | null = secondPart ? secondPart : null;
  await page.evaluate((internalSelector: string, valoreDaSelezionare: string, partTwo: string | null) => {
    function comparaTesti(primo: string, secondo: string, terzo: string | null): boolean {
      // eslint-disable-next-line newline-per-chained-call
      let first = primo;
      if (terzo) {
        first += `/${terzo}`;
      }
      const left = first.toLowerCase().normalize('NFD').replace(/[\u0300-\u036f]/g, '').replace(/'/g, ' ').trim();
      // eslint-disable-next-line newline-per-chained-call
      const right = secondo.toLowerCase().normalize('NFD').replace(/[\u0300-\u036f]/g, '').replace(/'/g, ' ').trim();

      return left === right;
    }

    const $el = $(internalSelector);

    if ($el.length) {
      const ul = $el[0];

      const list = [...ul.querySelectorAll<HTMLLIElement>('li')];

      list.find((li) => comparaTesti(valoreDaSelezionare, li.textContent?.trim() || '', partTwo))?.click();
    }
  }, menuSelector, dato.toLowerCase(), terzo);
}
