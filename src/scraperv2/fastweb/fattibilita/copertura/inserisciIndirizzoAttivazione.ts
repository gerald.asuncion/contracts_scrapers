import { Page } from 'puppeteer';
import inserisciDatoConSuggestion from '../inserisciDatoConSuggestion';

const selector = '#datiIndirizzoRESSHP1 > div:nth-child(4) > div:nth-child(2) > input';

export default async function inserisciIndirizzoAttivazione(page: Page, attivazioneIndirizzo: string): Promise<void> {
  await inserisciDatoConSuggestion(page, selector, attivazioneIndirizzo, 'indirizzo attivazione', false);
}
