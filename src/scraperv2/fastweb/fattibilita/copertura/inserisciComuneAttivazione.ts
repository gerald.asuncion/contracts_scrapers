import { Page } from 'puppeteer';
import inserisciDatoConSuggestionAndApostrophe from '../inserisciDatoConSuggestionAndApostrophe';

const selector = '#datiIndirizzoRESSHP1 > div:nth-child(4) > div:nth-child(1) > input';

export default async function inserisciComuneAttivazione(page: Page, attivazioneComune: string): Promise<void> {
  await inserisciDatoConSuggestionAndApostrophe(page, selector, attivazioneComune, 'comune attivazione', false);
}
