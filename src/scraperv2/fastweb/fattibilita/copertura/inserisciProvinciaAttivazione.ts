import { Page } from 'puppeteer';
import inserisciDatoConSuggestionSeAbilitato from '../inserisciDatoConSuggestionSeAbilitato';

const selector = '#datiIndirizzoRESSHP1 > div:nth-child(5) > div:nth-child(2) > input';

export default async function inserisciProvinciaAttivazione(page: Page, attivazioneProvincia: string): Promise<void> {
  await inserisciDatoConSuggestionSeAbilitato(page, selector, attivazioneProvincia, 'provincia attivazione');
}
