import { Page } from 'puppeteer';
import { ScraperResponse } from '../../../scraper';
import FailureResponse from '../../../../response/FailureResponse';
import { FastwebVerificaCoperturaPayload } from '../../payload/FastwebCheckFattibilitaPayload';
import { Logger } from '../../../../logger';
import inserisciComuneAttivazione from './inserisciComuneAttivazione';
import inserisciIndirizzoAttivazione from './inserisciIndirizzoAttivazione';
import inserisciCivicoAttivazione from './inserisciCivicoAttivazione';
import inserisciCapAttivazione from './inserisciCapAttivazione';
import inserisciProvinciaAttivazione from './inserisciProvinciaAttivazione';
import waitForSelectorAndClick from '../../../../browser/page/waitForSelectorAndClick';
import attendiSparizioneOverlay from '../attendiSparizioneOverlay';
import GenericSuccessResponse from '../../../../response/GenericSuccessResponse';

const LOG_MSG_PREFIX = 'check copertura >';

export default async function checkCopertura(
  page: Page,
  {
    attivazioneComune,
    attivazioneIndirizzo,
    attivazioneCivico,
    attivazioneCap,
    attivazioneProvincia
  }: FastwebVerificaCoperturaPayload,
  logger: Logger
): Promise<ScraperResponse> {
  logger.info(`${LOG_MSG_PREFIX} inserisco comune attivazione`);
  await inserisciComuneAttivazione(page, attivazioneComune);

  logger.info(`${LOG_MSG_PREFIX} inserisco indirizzo attivazione`);
  await inserisciIndirizzoAttivazione(page, attivazioneIndirizzo);

  logger.info(`${LOG_MSG_PREFIX} inserisco civico attivazione`);
  await inserisciCivicoAttivazione(page, attivazioneCivico.split('/')[0]);

  logger.info(`${LOG_MSG_PREFIX} inserisco cap attivazione`);
  await inserisciCapAttivazione(page, attivazioneCap);

  logger.info(`${LOG_MSG_PREFIX} inserisco provincia attivazione`);
  await inserisciProvinciaAttivazione(page, attivazioneProvincia);

  logger.info(`${LOG_MSG_PREFIX} clicco sul pulsante verifica`);
  await waitForSelectorAndClick(page, '#datiIndirizzoRESSHP2 > div > div:nth-child(1) > button');

  await attendiSparizioneOverlay(page);

  await page.waitForSelector('body > div.modal', { visible: true });

  const label = await page.$eval(
    'body > div.modal .modal-body > fieldset:nth-child(1) > div > div > label.control-label',
    (el) => el.textContent?.trim()
  );
  logger.info(`${LOG_MSG_PREFIX} apparsa modale con messaggio: ${label}`);

  logger.info(`${LOG_MSG_PREFIX} chiudo la modale`);
  await waitForSelectorAndClick(page, 'body > div.modal .modal-footer:not(.ng-hide) button:not(.ng-hide)');

  if (label?.includes('Offerta attivabile')) {
    return new GenericSuccessResponse<string>(label);
  }

  return new FailureResponse(label);
}
