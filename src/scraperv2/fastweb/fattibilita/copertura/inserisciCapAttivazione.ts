import { Page } from 'puppeteer';
import inserisciDatoConSuggestionSeAbilitato from '../inserisciDatoConSuggestionSeAbilitato';

const selector = '#datiIndirizzoRESSHP1 > div:nth-child(5) > div:nth-child(1) > input';

export default async function inserisciCapAttivazione(page: Page, attivazioneCap: string): Promise<void> {
  await inserisciDatoConSuggestionSeAbilitato(page, selector, attivazioneCap, 'cap attivazione');
}
