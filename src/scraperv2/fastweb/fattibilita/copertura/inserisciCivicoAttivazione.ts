import { Page } from 'puppeteer';
import inserisciDatoConSuggestion from '../inserisciDatoConSuggestion';
import selectSuggestion from '../../helpers/selectSuggestion';

const selector = '#datiIndirizzoRESSHP1 > div:nth-child(4) > div:nth-child(3) > input';

export default async function inserisciCivicoAttivazione(page: Page, attivazioneCivico: string): Promise<void> {
  await inserisciDatoConSuggestion(page, selector, attivazioneCivico, 'civico attivazione', false);
}
export async function inserisciCivicoAttivazioneV2(page: Page, civico: string): Promise<void> {
  await selectSuggestion(page, selector, civico.split('/')[0], civico, 'civico attivazione');
}
