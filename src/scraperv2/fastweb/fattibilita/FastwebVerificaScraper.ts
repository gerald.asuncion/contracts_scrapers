import { ScraperResponse } from '../../scraper';
import Fastweb from '../Fastweb';
import FastwebLoginPayload from '../payload/FastwebLoginPayload';
import FastwebInserimentoPayload from '../payload/FastwebInserimentoPayload';
import { SCRAPER_TIPOLOGIA } from '../../ScraperOptions';

export default class FastwebVerificaScraper<T> extends Fastweb<T & FastwebLoginPayload> {
  /**
   * @override
   */
  async login(payload: FastwebLoginPayload): Promise<void> {
    const { username, password } = await this.getCredenziali(payload, true);
    return super.login(<never>{ username, password });
  }

  async scrape(payload: T & FastwebInserimentoPayload): Promise<ScraperResponse> {
    return super.scrape(payload);
  }

  /**
   * @override
   */
  getScraperCodice() {
    return 'fastweb-fattibilita';
  }

  /**
   * @override
   */
  getScraperTipologia() {
    return SCRAPER_TIPOLOGIA.FATTIBILITA as any;
  }
}
