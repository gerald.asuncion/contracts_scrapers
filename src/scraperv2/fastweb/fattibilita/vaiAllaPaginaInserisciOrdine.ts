import { Page } from 'puppeteer';
import { Logger } from '../../../logger';
import waitForSelectorAndClick from '../../../browser/page/waitForSelectorAndClick';
import waitForNavigation from '../../../browser/page/waitForNavigation';
import waitForTimeout from '../../../browser/page/waitForTimeout';

export default async function vaiAllaPaginaInserisciOrdine(page: Page, logger: Logger, controllaSessioneDisponibile: (page: Page) => Promise<void>): Promise<void> {
  logger.info('clicco su "fastweb app"');
  await page.goto('https://fastweb.force.com/partnersales/apex/GlobalSearch?sfdc.tabName=01rw0000000kLSX', { waitUntil: 'networkidle2' });

  await controllaSessioneDisponibile(page);

  logger.info('attendo apertura completa altra pagina');
  await page.waitForSelector('#tabBar');

  logger.info('clicco su "inserisci ordine"');
  await Promise.all([
    waitForSelectorAndClick(page, '#\\30 1rw0000000UdbF_Tab > a'),
    waitForNavigation(page)
  ]);

  logger.info('clicco su DR.0797.0001HQ');
  await page.evaluate(() => {
    $('td:contains(DR.0797.0001HQ)').parent().click();
  });

  await waitForTimeout(page, 2000);
}
