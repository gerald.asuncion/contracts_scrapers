import { Page } from 'puppeteer';
import { ScraperResponse } from '../scraper';
import FornitoriMobilePayload from './fornitori/FornitoriMobilePayload';
import FastwebFornitoriEstrattore from './fornitori/FastwebFornitoriEstrattore';
import waitForSelectorAndClick from '../../browser/page/waitForSelectorAndClick';
import { generateIdSelector } from './inserimento/helpers/selectors';
import { QueueTask } from '../../task/QueueTask';

@QueueTask()
export default class FastwebFornitoriMobile extends FastwebFornitoriEstrattore {
  /**
   * @override
   */
  scrapeWebsite(): Promise<ScraperResponse> {
    return super.scrapeWebsite(<never>FornitoriMobilePayload);
  }

  /**
   * @override
   */
  async clickOnCheckbox(page: Page): Promise<void> {
    this.childLogger.info(`clicco su '${this.checkboxSelector.toUpperCase()}'`);
    await waitForSelectorAndClick(page, generateIdSelector(this.checkboxSelector));
  }
}
