import { MysqlPool } from "../../../mysql";

const SQL = "SELECT codice FROM fastweb_coupons WHERE abilitato = TRUE AND sconto_id = (SELECT id FROM fastweb_sconti fs WHERE label = ?);";

export type FastwebCoupon = {
  id: number;
  codice: string;
  sconto_id: number;
  abilitato: boolean;
};

export default async function getCouponFromDatabase(db: MysqlPool, sconto: string) {
  const [record] = await db.query<FastwebCoupon>(SQL, [sconto]);

  return record;
}
