import { Page } from "puppeteer";
import FastwebInserimentoPayload from "../payload/FastwebInserimentoPayload";
import tryOrThrow from "../../../utils/tryOrThrow";
import waitForSelectorAndClick from "../../../browser/page/waitForSelectorAndClick";
import waitLoadingCarrello from "./waitLoadingCarrello";
import aggiungiOffertaAlCarrello from "./aggiungiOffertaAlCarrello";
import waitForXPathAndClick from "../../../browser/page/waitForXPathAndClick";
import waitForXPathAndSelect from "../../../browser/page/waitForXPathAndSelect";
import getPopupRef from "../../../browser/page/getPopupRef";

export default async function aggiungiOffertaMobile(page: Page, { offertaMobile, isProdottoPortabilita }: FastwebInserimentoPayload) {
  await tryOrThrow(
    () => waitForXPathAndClick(page, '//a[.="Mobile Voce"]'),
    'Non sono riuscito a cliccare sul link `Mobile Voce`:'
  );

  await waitLoadingCarrello(page);

  await tryOrThrow(
    () => aggiungiOffertaAlCarrello(page, offertaMobile as string),
    'non sono riuscito ad aggiungere al carrello l\'offerta mobile:'
  );

  const popupPage = await getPopupRef(page);

  await tryOrThrow(() => waitForXPathAndClick(popupPage, '//button[contains(., "Procedi")]'), 'non sono riuscito a cliccare sul pulsante `Procedi`:');

  await waitLoadingCarrello(page);

  await tryOrThrow(
    () => waitForXPathAndSelect(page, '//div[contains(@id, "repeatAttributes")]/div[1]//select[contains(@id, "onlineSelectList")]', ['Ricarica Automatica']),
    'non sono riuscito a selezionare il tipo `Ricarica Automatica`:'
  );

  await waitLoadingCarrello(page);

  await tryOrThrow(
    () => waitForXPathAndSelect(page, '//div[contains(@id, "repeatAttributes")]/div[2]//select[contains(@id, "onlineSelectList")]', ['One shot/No terminale']),
    'non sono riuscito a selezionare `One shot/No terminale` per la vendita terminale:'
  );

  await waitLoadingCarrello(page);

  if (isProdottoPortabilita) {
    await tryOrThrow(() => waitForXPathAndClick(page, '//div[contains(@class, "main-content-box-header")][contains(., "Configura SIM")]//img'), 'non sono riuscito ad aprire il pannello `Configura SIM`:');

    await tryOrThrow(
      () => waitForXPathAndClick(page, '//label[contains(., "Richiesta MNP")]/parent::div/parent::div//input[contains(@type, "checkbox")]'),
      'non sono riscito a cliccare sul checkbox `Richiesta MNP`:'
    );

    await waitLoadingCarrello(page);

    await tryOrThrow(
      () => waitForXPathAndClick(page, '//label[contains(., "Richiesta TCR")]/parent::div/parent::div//input[contains(@type, "checkbox")]'),
      'non sono riscito a cliccare sul checkbox `Richiesta TCR`:'
    );
  }

  await waitLoadingCarrello(page);

  await waitForSelectorAndClick(page, '#saveButton');

  await waitLoadingCarrello(page);
}
