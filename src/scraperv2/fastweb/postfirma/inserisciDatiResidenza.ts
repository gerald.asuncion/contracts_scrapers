import { Page } from "puppeteer";
import inserisciDatiResidenzaMobile from "./inserisciDatiResidenzaMobile";
import { FastwebPostFirmaPayload } from "../payload/FastwebPostFirmaPayload";
import inserisciDatiResidenzaFisso from "./inserisciDatiResidenzaFisso";

export default async function inserisciDatiResidenza(
  page: Page,
  payload: FastwebPostFirmaPayload
) {
  if (payload.tipoOfferta.toLowerCase() === 'mobile') {
    await inserisciDatiResidenzaMobile(page, payload);
  } else {
    await inserisciDatiResidenzaFisso(page, payload);
  }
}
