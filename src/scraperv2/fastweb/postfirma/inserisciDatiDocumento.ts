import { Page } from "puppeteer";
import FastwebInserimentoPayload from "../payload/FastwebInserimentoPayload";
import waitForSelectorAndClick from "../../../browser/page/waitForSelectorAndClick";
import waitForVisible from "../../../browser/page/waitForVisible";
import inserisciNazionalitaDocumento from "../fattibilita/documento/inserisciNazionalitaDocumento";
import selezionaTipoDocumento from "../fattibilita/documento/selezionaTipoDocumento";
import waitForSelectorAndType from "../../../browser/page/waitForSelectorAndType";
import inserisciLocalitaRilascioDocumento from "../fattibilita/documento/inserisciLocalitaRilascioDocumento";
import selezionaEnteEmittente from "../fattibilita/documento/selezionaEnteEmittente";
import inserisciProvinciaRilascioDocumento from "../fattibilita/documento/inserisciProvinciaRilascioDocumento";
import isDisabled from "../../../browser/page/isDisabled";
import waitForSelectorAndClickEvaluated from "../../../browser/page/waitForSelectorAndClickEvaluated";
import attendiSparizioneOverlay from "../fattibilita/attendiSparizioneOverlay";
import recuperaEsitoCheck from "../fattibilita/recuperaEsitoCheck";
import tryOrThrow, { TryOrThrowError } from "../../../utils/tryOrThrow";

export default async function inserisciDatiDocumento(
  page: Page,
  {
    documentoTipo,
    documentoNumero,
    documentoRilasciatoDa,
    documentoEnteRilascio,
    documentoDataRilascio,
    documentoProvinciaRilascio
  }: FastwebInserimentoPayload
) {
  await tryOrThrow(
    () => waitForSelectorAndClick(page, '[heading="Documenti e Selezione Consensi"] div[heading="Documenti"] a'),
    'non sono riuscito ad aprire il pannello per inserire i dati del documento:'
  );

  await waitForVisible(page, 'input[name="section4_dataDiRilascioDocumento"]');

  await tryOrThrow(() => inserisciNazionalitaDocumento(page), 'non sono riuscito ad inserire la nazionalità del documento:');

  await tryOrThrow(() => selezionaTipoDocumento(page, documentoTipo), 'non sono riuscito a selezionare il tipo di documento:');

  await tryOrThrow(
    () => waitForSelectorAndType(page, 'input[name="section4_numeroDocumento"]', documentoNumero),
    'non sono riuscito ad inserire il numero del documento:'
  );

  await tryOrThrow(
    () => inserisciLocalitaRilascioDocumento(page, documentoRilasciatoDa as string),
    'non sono riuscito ad inserire la località di rilascio del documento:'
  );

  await tryOrThrow(() => selezionaEnteEmittente(page, documentoEnteRilascio), "non sono riuscito a selezionare l'ente emittente:");

  await tryOrThrow(
    () => waitForSelectorAndType(page, 'input[name="section4_dataDiRilascioDocumento"]', documentoDataRilascio),
    'non sono riuscito ad inserire la data di rilascio del documento:'
  );

  await tryOrThrow(
    () => inserisciProvinciaRilascioDocumento(page, documentoProvinciaRilascio as string),
    'non sono riuscito ad inserire la provincia di rilascio del documento:'
  );

  // eslint-disable-next-line max-len
  const btnSelector = '[heading="Documenti e Selezione Consensi"] > div:nth-child(2) > .panel-body > span:nth-child(1) .panel-body > fieldset > div:nth-child(4) button';

  if (await isDisabled(page, btnSelector)) {
    throw new TryOrThrowError('impossibile verificare il documento, pulsante disabilitato');
  }

  await tryOrThrow(() => waitForSelectorAndClickEvaluated(page, btnSelector), 'non sono riuscito a cliccare sul pulsante verifica:');

  let modalError: string | undefined;
  try {
    const modalErrSelector = 'body > div.modal.fade.ng-isolate-scope.in > div > div > div.alert.ng-scope.ng-isolate-scope.alert-danger';
    await page.waitForSelector(modalErrSelector, { timeout: 6000 });
    modalError = await page.$eval(
      'body > div.modal.fade.ng-isolate-scope.in > div > div > div.alert.ng-scope.ng-isolate-scope.alert-danger > div > span',
      (el) => el.textContent?.trim()
    );
  } catch (ex) {
    // modale d'errore non apparsa
  }

  if (modalError) {
    throw new TryOrThrowError(/* modalError */'Documento non verificato');
  }

  await attendiSparizioneOverlay(page, { timeout: 120000 });

  const esito = await tryOrThrow(
    () => recuperaEsitoCheck(page, '[heading="Documenti e Selezione Consensi"] textarea'),
    "non sono riuscito a recuperare l'esito"
  );

  if (!esito?.includes('documento verificato')) {
    throw new TryOrThrowError(/* esito */'Documento non verificato');
  }
}
