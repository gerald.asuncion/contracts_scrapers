const list: string[] = [
  'documento non verificato',
  'cliente moroso',
  'indirizzo non coperto',
  'metodo di pagamento non accettato',
  'non eleggibile per eni cluster 2'
];

export default function isBusinessErrorDaEsitare(ex: Error) {
  for (let msg of list) {
    if (ex.message.toLowerCase().includes(msg)) {
      return true;
    }
  }

  return false;
}
