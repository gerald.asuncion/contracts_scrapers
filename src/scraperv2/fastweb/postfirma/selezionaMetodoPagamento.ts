import { Page } from "puppeteer";
import waitForSelectorAndClickEvaluated from "../../../browser/page/waitForSelectorAndClickEvaluated";

const TIPO_PAGAMENTO_MAP: Record<string, string> = {
  'Addebito su Conto Corrente': 'DIRECT DEBIT',
  'Carta di Credito': 'CARTA DI CREDITO'
};

export default async function selezionaMetodoPagamento(page: Page, tipoPagamento: string, inputSelectorBase = 'input[name="section3_metodoDiPagamento"]') {
  await waitForSelectorAndClickEvaluated(page, `${inputSelectorBase}[value="${TIPO_PAGAMENTO_MAP[tipoPagamento]}"]`);
}
