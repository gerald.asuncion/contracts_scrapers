import { Page } from "puppeteer";
import FastwebInserimentoPayload from "../payload/FastwebInserimentoPayload";
import tryOrThrow from "../../../utils/tryOrThrow";
// import waitForSelectorAndClick from "../../../browser/page/waitForSelectorAndClick";
import waitForSelectorAndType from "../../../browser/page/waitForSelectorAndType";
import inserisciDatoConSuggestion from "../fattibilita/inserisciDatoConSuggestion";
import inserisciDatoConSuggestionSeAbilitato from "../fattibilita/inserisciDatoConSuggestionSeAbilitato";
import waitForSelectorAndClickEvaluated from "../../../browser/page/waitForSelectorAndClickEvaluated";
import waitForXPathAndClick from "../../../browser/page/waitForXPathAndClick";
// import waitForXPathAndClick from "../../../browser/page/waitForXPathAndClick";

export default async function inserisciDatiSpedizione(
  page: Page,
  // envelopId: string,
  {
    recapitoCap,
    recapitoCivico,
    recapitoComune,
    recapitoIndirizzo,
    recapitoProvincia,
    recapitoPresso
  }: FastwebInserimentoPayload
) {
  await tryOrThrow(
    // () => waitForSelectorAndClick(page, 'div[heading="Indirizzo di Copertura e Residenza"] div[heading="Completamento Dati"] a'),
    () => waitForSelectorAndClickEvaluated(page, 'div[heading="Indirizzo di Copertura e Residenza"] div[heading="Completamento Dati"] a'),
    // () => waitForXPathAndClick(page, '//div[@heading="Indirizzo di Copertura e Residenza"]//div[@heading="Completamento Dati"]//a'),
    'non sono riuscito ad aprire il pannello `Completamento Dati`:'
  );

  await tryOrThrow(
    () => waitForXPathAndClick(page, '//div[@heading="Indirizzo di Copertura e Residenza"]//div[@heading="Completamento Dati"]//div[@id="datiIndirizzoSpedizioneRES"]//button[contains(., "Copia Indirizzo di Attivazione")]'),
    'non sono riuscito a cliccare sul pulsante `Copia Indirizzo di Attivazione`:'
  );

  // TODO: sezione commentata perché si è deciso di cliccare su 'copia indirizzo di attivazione'
  // await tryOrThrow(
  //   () => inserisciDatoConSuggestion(page, 'input[name="section2_cittaDiSpedizione2"]', recapitoComune, 'Città di Spedizione Apparato', false),
  //   'non sono riuscito ad inserire il comune di spedizione:'
  // );

  // await tryOrThrow(
  //   () => inserisciDatoConSuggestion(page, 'input[name="section2_indirizzoDiSpedizione2"]', recapitoIndirizzo, 'Indirizzo di Spedizione Apparato', false),
  //   "non sono riuscito ad inserire l'indirizzo di spedizione:"
  // );

  // await tryOrThrow(
  //   () => inserisciDatoConSuggestion(page, 'input[name="section2_civicoDiSpedizione2"]', recapitoCivico, 'Civico di Spedizione Apparato', false),
  //   'non sono riuscito ad inserire il civico di spedizione:'
  // );

  // await tryOrThrow(
  //   async () => {
  //     const recapitoCapSelector = 'input[name="section2_capDiSpedizione2"]';
  //     const currentValue = await page.$eval(recapitoCapSelector, (el) => (el as HTMLInputElement).value?.trim());

  //     if (!currentValue) {
  //       await waitForSelectorAndType(page, recapitoCapSelector, recapitoCap);
  //     }
  //   },
  //   'non sono riuscito ad inserire il cap di spedizione:'
  // );

  // await tryOrThrow(
  //   () => inserisciDatoConSuggestionSeAbilitato(page, 'input[name="section2_provinciaDiSpedizione2"]', recapitoProvincia, 'Provincia di Spedizione Apparato'),
  //   'non sono riuscito ad inserire la provincia di spedizione:'
  // );

  // if (recapitoPresso) {
  //   await tryOrThrow(
  //     () => waitForSelectorAndType(page, 'input[name="section2_pressoSpedizione2"]', recapitoPresso),
  //     'non sono riuscito ad inserire il `recapito presso` di spedizione:'
  //   );
  // }
}
