import { Page } from "puppeteer";
import waitForSelectorAndSelect from "../../../browser/page/waitForSelectorAndSelect";
import getOptionToSelect from "../../eni/inserimentocontratto/getOptionToSelect";

const SELECT_SELECTOR = 'select[name="section2_operatoreFonia"]';

export default async function inserisciOperatoreFonia(page: Page, precendeteOperatore: string) {
  const option = await getOptionToSelect(page, SELECT_SELECTOR, precendeteOperatore);
  await waitForSelectorAndSelect(page, SELECT_SELECTOR, [option as string]);
}
