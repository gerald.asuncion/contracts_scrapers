import { Page } from "puppeteer";
import { FastwebPostFirmaPayload } from "../payload/FastwebPostFirmaPayload";
import tryOrThrow, { TryOrThrowError } from "../../../utils/tryOrThrow";
import waitForSelectorAndClick from "../../../browser/page/waitForSelectorAndClick";
import waitForXPathAndClick from "../../../browser/page/waitForXPathAndClick";
import waitForVisible from "../../../browser/page/waitForVisible";
import getElementText from "../../../browser/page/getElementText";
import waitForTimeout from "../../../browser/page/waitForTimeout";

export default async function inserisciPartnerships(page: Page, { opzioneFisso1, partnership }: FastwebPostFirmaPayload) {
  if (opzioneFisso1 === 'Opzione Now (primi 3 mesi gratis)') {
    await tryOrThrow(
      () => waitForSelectorAndClick(page, 'input[name="section3_policy_nowtv"]'),
      "non sono riuscito ad abilitare l'opzione Now Tv:"
    );

    await tryOrThrow(
      () => waitForXPathAndClick(page, '//button[contains(., "Verifica eleggibilità Now Tv")]'),
      'non sono riuscito a cliccare sul pulsante `Verifica eleggibilità Now Tv`:'
    );

    // catturo la modale
    let alertMsg: string | null = null;
    try {
      await waitForVisible(page, 'div[role="dialog"]');
      alertMsg = await getElementText(page, 'div[role="dialog"] div[role="alert"] > div', 'textContent');
    } catch (ex) {
      // nulla da fare, modale non apparsa
    }

    if (alertMsg && !alertMsg.toLowerCase().includes('opzione now tv vendibile')) {
      throw new TryOrThrowError(alertMsg.trim());
    } else {
      await tryOrThrow(
        () => waitForXPathAndClick(page, '//div[@role="dialog"]//button[contains(., "Avanti")]'),
        'non sono riuscito a cliccare sul pulsante `Avanti` della modale esito Now TV:'
      );

      await waitForTimeout(page, 3000);
    }
  }

  if (partnership === 'Cluster 2 - ENI') {
    await tryOrThrow(
      () => waitForSelectorAndClick(page, 'input[name="section3_policy_ENI"]'),
      "non sono riuscito ad abilitare l'opzione ENI:"
    );

    await tryOrThrow(
      () => waitForXPathAndClick(page, '//button[contains(., "Copia CF del cliente FW")]'),
      'non sono riuscito a cliccare sul pulsante `Copia CF del cliente FW`:'
    );

    await tryOrThrow(
      () => waitForXPathAndClick(page, '//button[contains(., "Verifica eleggibilità ENI")]'),
      'non sono riuscito a cliccare sul pulsante `Verifica eleggibilità ENI`:'
    );

    // catturo la modale
    let alertMsg: string | null = null;
    try {
      await waitForVisible(page, 'div[role="dialog"]');
      alertMsg = await getElementText(page, 'div[role="dialog"] div[role="alert"] > div', 'textContent');
    } catch (ex) {
      // nulla da fare, modale non apparsa
    }

    if (alertMsg && !alertMsg.toLowerCase().includes('il cliente è eleggibile per la partnership eni – cluster 2')) {
      throw new TryOrThrowError(/* alertMsg.trim() */'Non eleggibile per Eni Cluster 2');
    } else {
      await tryOrThrow(
        () => waitForXPathAndClick(page, '//div[@role="dialog"]//button[contains(., "Avanti")]'),
        'non sono riuscito a cliccare sul pulsante `Avanti` della modale esito partnership Eni:'
      );

      await waitForTimeout(page, 3000);
    }
  }
}
