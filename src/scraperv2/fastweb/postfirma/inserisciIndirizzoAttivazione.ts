import { Page } from 'puppeteer';
import inserisciIndirizzo from './inserisciIndirizzo';

const selector = '#datiIndirizzoRESSHP1 > div:nth-child(4) > div:nth-child(2) > input';

export default async function inserisciIndirizzoAttivazione(page: Page, attivazioneIndirizzo: string): Promise<void> {
  await inserisciIndirizzo(page, selector, attivazioneIndirizzo, 'indirizzo attivazione');
}
