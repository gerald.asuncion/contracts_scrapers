import path from 'path';
import fs from 'fs';
import util from 'util';
import { Page } from "puppeteer";
import tryOrThrow from "../../../utils/tryOrThrow";
import getCurrentDayFormatted from '../../../utils/getCurrentDayFormatted';
import waitForXPathAndClick from '../../../browser/page/waitForXPathAndClick';
import setDownloadPath from '../../../browser/page/setDownloadPath';
import mkdir from '../../../utils/mkdir';
import saveFileOnAwsS3 from '../../../utils/saveFileOnAwsS3';
import { S3ClientFactoryResult } from '../../../aws/s3ClientFactory';
import { Logger } from '../../../logger';

const BTN_SCARICAPDA_SELECTOR = '//a[contains(., "Scarica PDA")]';

export default async function scaricaPda(page: Page, logger: Logger, baseDownloadPath: string, s3ClientFactory: S3ClientFactoryResult, idDatiContratto: number) {
  const downloadPath = path.resolve(baseDownloadPath, getCurrentDayFormatted(), 'fastweb-postfirma');

  mkdir(downloadPath);

  await setDownloadPath(page, downloadPath);

  await tryOrThrow(() => waitForXPathAndClick(page, BTN_SCARICAPDA_SELECTOR), 'non sono riuscito a cliccare sul pulsante `Stampa PDA`:');

  logger.info('scaricamento...');
  let fileName;
  while (!fileName || fileName.endsWith('.crdownload')) {
    await new Promise((resolve) => setTimeout(resolve, 100));
    [fileName] = await util.promisify(fs.readdir)(downloadPath);
  }

  const filePath = path.resolve(downloadPath, fileName);
  logger.info(`scaricato il file ${filePath}; ora lo salvo su aws S3`);

  const parts = filePath.split(path.sep);
  await saveFileOnAwsS3(s3ClientFactory, 'InserimentoPdaContrattiFastweb', filePath, `${idDatiContratto}.${parts[parts.length - 1]}`, logger);

  fs.unlinkSync(filePath);
}
