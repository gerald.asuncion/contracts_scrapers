import { Page } from "puppeteer";
import { Logger } from "../../../logger";
import vaiAllaPaginaConsultazioneFirma from "../check-stato-contratto/vaiAllaPaginaConsultazioneFirma";
import waitForSelectorAndClick from "../../../browser/page/waitForSelectorAndClick";
import waitForVisible from "../../../browser/page/waitForVisible";
import waitForXPathAndClick from "../../../browser/page/waitForXPathAndClick";
import { FastwebPostFirmaPayload } from "../payload/FastwebPostFirmaPayload";
import { inserisciCodiceContrattoConRetry } from "../check-stato-contratto/inserisciCodiceContrattoDaControllare";
import { TryOrThrowError } from "../../../utils/tryOrThrow";
import waitForTimeout from "../../../browser/page/waitForTimeout";
import waitForHidden from "../../../browser/page/waitForHidden";
import { generateIdSelector } from "../inserimento/helpers/selectors";

export default async function esitaLaPratica(
  page: Page,
  logger: Logger,
  controllaSessioneDisponibile: (page: Page) => Promise<void>,
  { idEsterno }: FastwebPostFirmaPayload,
  label: string
) {
  const otherPage = await vaiAllaPaginaConsultazioneFirma(page, logger, controllaSessioneDisponibile);
  await otherPage.setViewport({ width: 1920, height: 1080 });

  logger.info("inserisco l'id dati contratto");

  const codiceContratto = idEsterno.toString();

  const codiceInserito = await inserisciCodiceContrattoConRetry(otherPage, codiceContratto);
  if (codiceInserito !== codiceContratto) {
    throw new TryOrThrowError(`non sono riuscito ad inserire correttamente il codice contratto ${idEsterno}`);
  }

  logger.info('attendo caricamento dati');

  await waitForTimeout(otherPage, 100);
  await waitForXPathAndClick(otherPage, '//button[contains(., "Visualizza")]');

  try {
    await waitForHidden(otherPage, 'div[id$="_modal"].ui-widget-overlay', { timeout: 3000 });
  } catch (ex) {
    // sparito prima che potessi fare il wait
  }

  const tableRowSelector = `${generateIdSelector('pdaTable_data')} > tr`;
  await waitForVisible(otherPage, tableRowSelector);

  const noData = await otherPage.evaluate(() => !!$('td:contains(Nessuna firma elettronica trovata.)').length);

  if (noData) {
    throw new TryOrThrowError("nessun record trovato");
  }

  logger.info("esito il contratto");

  await waitForSelectorAndClick(otherPage, 'label[id$="statoInserimentoCPQ_label"]');

  await waitForVisible(otherPage, 'ul[id$="statoInserimentoCPQ_items"]');

  await waitForXPathAndClick(otherPage, `//ul[contains(@id, "statoInserimentoCPQ_items")]//li[contains(., "${label}")]`);

  await waitForVisible(otherPage, 'table > tbody > tr > td:nth-child(10)');

  await waitForSelectorAndClick(otherPage, 'table > tbody > tr > td:nth-child(10) span');

  await waitForXPathAndClick(otherPage, '//button[contains(., "Salva Stato Inserimento CPQ")]');

  return otherPage;
}
