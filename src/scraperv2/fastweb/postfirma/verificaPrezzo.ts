import { Page } from "puppeteer";
import { FastwebPostFirmaPayload } from "../payload/FastwebPostFirmaPayload";
import { MysqlPool } from "../../../mysql";
import tryOrThrow, { TryOrThrowError } from "../../../utils/tryOrThrow";
import getElementText from "../../../browser/page/getElementText";
import getCostoOffertaConSconto, {getCostoOfferteExtra} from "./getCostoOffertaConSconto";

export default async function verificaPrezzo(page: Page, db: MysqlPool, { offertaFisso, sconto1, partnership, opzioneFisso1, opzioneFisso8 }: FastwebPostFirmaPayload) {
  let prezzo = await getCostoOffertaConSconto(db, offertaFisso as string, sconto1, partnership, [opzioneFisso1 as string]);
  if (opzioneFisso8 === "Sconto Fisso Mobile") {
    const sconto = await getCostoOfferteExtra(db, [opzioneFisso8]);
    prezzo += sconto[0];
    prezzo = Math.round(prezzo * 100) / 100
  }

  let prezzoNelCarrelloStr = await tryOrThrow(
    () => getElementText(page, '.single-item-total .cost-recurring-total', 'textContent'),
    'non sono riuscito a recuperare il prezzo nel carrello:'
  );

  if (!prezzoNelCarrelloStr) {
    throw new TryOrThrowError('prezzo nel carrello non trovato');
  }

  prezzoNelCarrelloStr = prezzoNelCarrelloStr.replace(/\n/g, '').replace(/ /g, '').trim();

  const matches = prezzoNelCarrelloStr.match(/\d+|\.|,/g);

  if (!matches) {
    throw new TryOrThrowError('prezzo nel carrello non elaborato');
  }

  const prezzoNelCarrello = Number(matches.join('').replace(',', '.'));

  if (prezzoNelCarrello !== prezzo) {
    throw new TryOrThrowError('Il prezzo dell’offerta non coincide col contratto firmato dal cliente');
  }
}
