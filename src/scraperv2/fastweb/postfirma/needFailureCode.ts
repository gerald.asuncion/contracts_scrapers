const list: string[] = [
  'documento non verificato',
  'cliente moroso',
  'indirizzo non coperto',
  'metodo di pagamento non accettato',
  'prezzo nel carrello non elaborato',
  'il prezzo dell’offerta non coincide col contratto firmato dal cliente',
  'non eleggibile per eni cluster 2',
  'offerta non presente nella tabella prezzi',
  'contratto inserito, esitare la pratica su fastweb cpq',
  'offerta non presente nella tabella prezzi',
  'contratto non inserito, esitare la pratica su fastweb cpq come `cliente irreperibile`',
  'risultano una o più righe con billing account attivo'
];

export default function needFailureCode(message: string): boolean {
  return list.includes(message.toLowerCase());
}
