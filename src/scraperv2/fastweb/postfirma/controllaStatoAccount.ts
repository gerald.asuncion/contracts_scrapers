import { Page } from "puppeteer";
import isVisible from "../../../browser/page/isVisible";
import waitForNavigation from "../../../browser/page/waitForNavigation";
import waitForSelectorAndClick from "../../../browser/page/waitForSelectorAndClick";
import waitForSelectorAndType from "../../../browser/page/waitForSelectorAndType";
import waitForTimeout from "../../../browser/page/waitForTimeout";
import waitForVisible from "../../../browser/page/waitForVisible";
import waitForXPathAndClick from "../../../browser/page/waitForXPathAndClick";
import waitForXPathVisible from "../../../browser/page/waitForXPathVisible";
import { Logger } from "../../../logger";
import FailureResponse from "../../../response/FailureResponse";
import tryOrThrow, { TryOrThrowError } from "../../../utils/tryOrThrow";
import { FastwebPostFirmaPayload } from "../payload/FastwebPostFirmaPayload";

const FASTWEBAPP_LINK_SELECTOR = '//a[contains(., "Fastweb App")]';

export default async function controllaStatoAccount(
  page: Page,
  logger: Logger,
  { codiceFiscale, nome, cognome }: FastwebPostFirmaPayload
) {
  let error;

  page.on("dialog", async (dialog) => {
    try {
      const message = dialog.message();
      logger.info(dialog.message());
      error = new TryOrThrowError(message);
      await dialog.accept();
    } catch (e) {
      const message = "No message on dialog. Only accepted";
      error = new TryOrThrowError(message);
      await dialog.accept();
    }
  });

  await waitForXPathVisible(page, FASTWEBAPP_LINK_SELECTOR, { timeout: 60000 });

  await tryOrThrow(async () => {
    const [el] = await page.$x(FASTWEBAPP_LINK_SELECTOR);
    const url = (await (await el.getProperty("href")).jsonValue()) as string;
    await page.goto(url, { waitUntil: "networkidle2" });
  }, "non sono riuscito a cliccare su `Fastweb App`:");

  await tryOrThrow(
    () =>
      waitForSelectorAndClick(
        page,
        'input[type="checkbox"][name$="AccountOption"]'
      ),
    "non sono riuscito a cliccare sul checkbox per abilitare l'inserimento del codice fiscale:"
  );

  await tryOrThrow(
    () => waitForSelectorAndType(page, 'input[name$="accName"]', nome),
    "non sono riuscito ad inserire il nome:"
  );

  await tryOrThrow(
    () => waitForSelectorAndType(page, 'input[name$="accSurname"]', cognome),
    "non sono riuscito ad inserire il cognome:"
  );

  await tryOrThrow(
    () => waitForSelectorAndType(page, 'input[name$="fCode"]', codiceFiscale),
    "non sono riuscito ad inserire il codice fiscale:"
  );

  await tryOrThrow(
    () => waitForXPathAndClick(page, '//span[contains(., "Cerca")]'),
    "non sono riuscito a cliccare sul pulsante `Cerca`:"
  );

  if (error) {
    throw error;
  }
  await waitForVisible(page, 'span[id$="resultSec"]');
  await waitForTimeout(page, 3000);

  if (await isVisible(page, 'span[id$="resultSec"] > div[class="apexp"]')) {
    await Promise.all([
      waitForNavigation(page),
      waitForSelectorAndClick(
        page,
        'span[id$="resultSec"] > div[class="apexp"] .list > tbody > tr > td:nth-child(1) > a'
      ),
    ]);

    let valid = true;

    try {
      const targetName = await tryOrThrow(async () => {
        const [el] = await page.$x('//a[contains(., "Billing Accounts")]');
        const url: string = await (await el.getProperty("href")).jsonValue();
        const parts = url.split("#");
        return parts[parts.length - 1].replace("_target", "");
      }, "non sono riuscito a recuperare il riferimento al div `Billing Accounts`:");

      valid = await tryOrThrow(
        () =>
          page.evaluate((name: string) => {
            const rows = Array.from(
              document.querySelectorAll(
                `div[id="${name}"] tr.dataRow > td:nth-child(5)`
              )
            );
            return (
              rows.length ===
              rows.filter(
                (row) => row.textContent?.toLowerCase() === "non attivo"
              ).length
            );
          }, targetName),
        "non sono riuscito a recuperare lo stato dell'account:"
      );
    } catch (ex) {
      // sezione 'Billing Accounts' non presente
      // nulla da fare
    }

    if (!valid) {
      return new FailureResponse(
        "Risultano una o più righe con billing account attivo"
      );
    }
  }
}
