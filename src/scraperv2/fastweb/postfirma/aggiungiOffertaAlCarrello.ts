import { Page } from "puppeteer";
import waitForVisible from "../../../browser/page/waitForVisible";
import waitForXPathAndClick from "../../../browser/page/waitForXPathAndClick";
import tryOrThrow from "../../../utils/tryOrThrow";

const OFFERTA_FISSO_MAP: Record<string, string> = {
  'Fastweb Nexxt Casa': 'Fastweb NeXXt Casa',
  'Fastweb Nexxt Casa Plus': 'Fastweb NeXXt Casa Plus',
  'Fastweb Casa Light FWA': 'Fastweb Casa Light FWA',
  'Fastweb Casa FWA': 'Fastweb Casa FWA',
};

const creaSelettore = (offertaFisso: string) => `//td[contains(@class, "col-cartDescriptionOrd")][.="${offertaFisso}"]//parent::tr//td[contains(@class, "showExternalInputClass")]//div[2]//a`;

export default async function aggiungiOffertaAlCarrello(page: Page, offerta: string) {
  await waitForVisible(page, '.productsTableOrd');

  const offertaDaSelezionare = OFFERTA_FISSO_MAP[offerta] || offerta;

  await tryOrThrow(() => waitForXPathAndClick(page, creaSelettore(offertaDaSelezionare)), 'non sono riuscito a cliccare sul pulsante `Aggiungi e Configura`:');
}
