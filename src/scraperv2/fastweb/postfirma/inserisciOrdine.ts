import { Page } from "puppeteer";
import getElementText from "../../../browser/page/getElementText";
import isXPathVisible from "../../../browser/page/isXPathVisible";
import waitForNavigation from "../../../browser/page/waitForNavigation";
import waitForSelectorAndClick from "../../../browser/page/waitForSelectorAndClick";
import waitForTimeout from "../../../browser/page/waitForTimeout";
import waitForVisible from "../../../browser/page/waitForVisible";
import waitForXPathAndClick from "../../../browser/page/waitForXPathAndClick";
import { Logger } from "../../../logger";
import { MysqlPool } from "../../../mysql";
import { NoSubmitPayload } from "../../../payloads/types";
import tryOrThrow, { TryOrThrowError } from "../../../utils/tryOrThrow";
import timeoutPromise from "../../timeoutPromise";
import { FastwebPostFirmaPayload } from "../payload/FastwebPostFirmaPayload";
import aggiungiOffertaAlCarrello from "./aggiungiOffertaAlCarrello";
import aggiungiOffertaMobile from "./aggiungiOffertaMobile";
import gestisciCoupon from "./gestisciCoupon";
import gestisciOpzioni from "./gestisciOpzioni";
import inserisciConsensi from "./inserisciConsensi";
import inserisciDatiAnagrafici from "./inserisciDatiAnagrafici";
import inserisciDatiAttivazione from "./inserisciDatiAttivazione";
import inserisciDatiDocumento from "./inserisciDatiDocumento";
import inserisciDatiPagamento from "./inserisciDatiPagamento";
import inserisciDatiResidenza from "./inserisciDatiResidenza";
import inserisciDatiSpedizione from "./inserisciDatiSpedizione";
import inserisciEnvelopId from "./inserisciEnvelopId";
import inserisciIban from "./inserisciIban";
import inserisciPartnerships from "./inserisciPartnerships";
import inserisciPortabilita from "./inserisciPortabilita";
import selezionaTipoOfferta from "./selezionaTipoOfferta";
import verificaPrezzo from "./verificaPrezzo";
import verificaPrezzoMobile from "./verificaPrezzoMobile";
import waitLoadingCarrello from "./waitLoadingCarrello";

export default async function inserisciOrdine(page: Page, logger: Logger, db: MysqlPool, envelopId: string, payload: FastwebPostFirmaPayload) {
  logger.info('Inserisco i dati del contratto');

  await waitForVisible(page, 'form[name="orderTakingForm"]');
  await waitForTimeout(page, 3000);

  logger.info('seleziono il tipo di offerta');
  await tryOrThrow(() => selezionaTipoOfferta(page, payload), 'non sono riuscito a selezionare il tipo di offerta:');

  logger.info('inserisco i dati anagrafici');
  await tryOrThrow(() => inserisciDatiAnagrafici(page, payload), 'non sono riuscito ad inserire i dati anagrafici:');

  logger.info('inserisco i dati di attivazione');
  const err = await tryOrThrow(() => inserisciDatiAttivazione(page, payload), 'non sono riuscito ad inserire i dati di attivazione:');

  if (err) {
    return err;
  }

  logger.info('inserisco i dati di residenza');
  await tryOrThrow(() => inserisciDatiResidenza(page, payload), 'non sono riuscito ad inserire i dati di residenza:');

  if (payload.tipoOfferta.toLowerCase() !== 'mobile') {
    await waitForTimeout(page, 6000);
    logger.info('inserisco i dati di spedizione');
    await tryOrThrow(() => inserisciDatiSpedizione(page/* , envelopId */, payload), 'non sono riuscito ad inserire i dati di spedizione:');
  }

  logger.info('inserisco le partnership');
  await tryOrThrow(() => inserisciPartnerships(page, payload), 'non sono riuscito ad inserire le partnership:');

  logger.info('inserisco i dati di pagamento');
  await tryOrThrow(() => inserisciDatiPagamento(page, payload), 'non sono riuscito ad inserire i dati di pagamento:');

  logger.info('inserisco i dati del documento');
  await tryOrThrow(() => inserisciDatiDocumento(page, payload), 'non sono riuscito ad inserire i dati del documento:');

  logger.info('inserisco i consensi');
  await tryOrThrow(() => inserisciConsensi(page, payload), 'non sono riuscito a selezionare i consensi:');

  logger.info('vado al carrello');
  await tryOrThrow(
    () => Promise.all([
      waitForNavigation(page),
      waitForXPathAndClick(page, '//button[contains(., "Vai al Carrello")]')
    ]),
    'non sono riuscito a cliccare sul pulsante `Vai al Carrello`:'
  );

  await waitLoadingCarrello(page);

  logger.info("seleziono l'offerta");
  await tryOrThrow(
    () => aggiungiOffertaAlCarrello(
      page,
      payload.tipoOfferta.toLowerCase() === 'mobile' ? payload.offertaMobile as string : payload.offertaFisso as string
    ),
    "non sono riuscito ad aggiungere l'offerta al carrello:"
  );

  logger.info('gestisco il coupon');
  await tryOrThrow(() => timeoutPromise('Coupon Timeout', 360000)(gestisciCoupon(page, db, payload)), 'non sono riuscito a gestire il coupon:');

  logger.info('gestisco le opzioni');
  await gestisciOpzioni(page, payload);

  await waitForTimeout(page, 10000);

  logger.info('controllo il prezzo');
  if (payload.tipoOfferta.toLowerCase() === 'mobile') {
    await verificaPrezzoMobile(page, db, payload);
  } else {
    await verificaPrezzo(page, db, payload);
  }

  if (payload.tipoOfferta.toLowerCase() === 'fisso+mobile') {
    logger.info("aggiungo l'offerta mobile");
    await aggiungiOffertaMobile(page, payload);

    await waitForTimeout(page, 10000);

    logger.info('verifico il prezzo mobile');
    await verificaPrezzoMobile(page, db, payload);
  }

  await waitForTimeout(page, 1000);

  logger.info("clicco sul pulsante `Successivo`");
  await Promise.all([
    waitForNavigation(page),
    waitForSelectorAndClick(page, '#saveButton')
  ]);

  if (payload.isProdottoPortabilita && await isXPathVisible(page, '//div[.="Configura ICCID"]')) {
    logger.info('inserisco i dati di portabilità');
    await tryOrThrow(() => inserisciPortabilita(page, payload), 'non sono riuscito ad inserire i dati di portabilità:');
  } else {
    let errMsg;
    try {
      await waitForVisible(page, 'div[role="alert"]');
      errMsg = await getElementText(page, 'div[role="alert"]', 'textContent');
    } catch (ex) {
      // nessun errore, non devo fare nulla
    }

    if (errMsg) {
      throw new TryOrThrowError(errMsg.trim());
    }
  }

  logger.info("completo i dati della banca");
  await tryOrThrow(() => inserisciIban(page, payload), "non sono riuscito ad inserire l'iban:");

  logger.info("inserisco l'envelop id");
  await tryOrThrow(
    () => inserisciEnvelopId(page, envelopId, !!(payload as NoSubmitPayload).noSubmit),
    "non sono riuscito ad inserire l'envelop id:"
  );
}
