import { Page } from "puppeteer";
import { FastwebPostFirmaPayload } from "../payload/FastwebPostFirmaPayload";
import tryOrThrow, { TryOrThrowError } from "../../../utils/tryOrThrow";
import waitForSelectorAndType from "../../../browser/page/waitForSelectorAndType";
import getOptionToSelect from "../../eni/inserimentocontratto/getOptionToSelect";
import waitForSelectorAndSelect from "../../../browser/page/waitForSelectorAndSelect";
import isEnabled from "../../../browser/page/isEnabled";
import waitForSelectorAndTypeEvaluated from "../../../browser/page/waitForSelectorAndTypeEvaluated";
import waitForVisible from "../../../browser/page/waitForVisible";
import waitForNavigation from "../../../browser/page/waitForNavigation";
import waitForSelectorAndClickEvaluated from "../../../browser/page/waitForSelectorAndClickEvaluated";

const TIPO_DOCUMENTO: Record<string, string> = {
  "carta d'identità": 'string:CARTA DI IDENTITA',
  'passaporto': 'string:PASSAPORTO',
  'patente': 'string:PATENTE'
};

export default async function inserisciPortabilita(
  page: Page,
  {
    nome,
    cognome,
    codiceFiscale,
    cellulareAttuale,
    tipoContratto,
    attualeFornitoreMobile,
    documentoTipo,
    documentoNumero,
    iccid
  }: FastwebPostFirmaPayload
) {
  await tryOrThrow(
    () => waitForSelectorAndType(page, 'input[name="nome0"]', nome),
    'non sono riuscito ad inserire il nome:'
  );

  await tryOrThrow(
    () => waitForSelectorAndType(page, 'input[name="cognome0"]', cognome),
    'non sono riuscito ad inserire il cognome:'
  );

  await tryOrThrow(
    () => waitForSelectorAndType(page, 'input[name="msisdnportato0"]', cellulareAttuale as string),
    'non sono riuscito ad inserire il cellulare attuale:'
  );

  await tryOrThrow(
    async () => {
      const selector = 'select[name="origine0"]';
      if (await isEnabled(page, selector)) {
        const option = await getOptionToSelect(page, selector, tipoContratto);
        await waitForSelectorAndSelect(page, selector, [option as string]);
      }
    },
    'non sono riuscito ad inserire il tipo di contratto:'
  );

  await tryOrThrow(
    () => waitForSelectorAndType(page, 'input[name="cfpiva0"]', codiceFiscale),
    'non sono riuscito ad inserire il codice fiscale:'
  );

  await tryOrThrow(
    async () => {
      const option = await getOptionToSelect(page, 'select[name="operatore0"]', attualeFornitoreMobile as string);
      await waitForSelectorAndSelect(page, 'select[name="operatore0"]', [option as string]);
    },
    "non sono riuscito ad inserire l'attuale fornitore:"
  );

  await tryOrThrow(
    async () => {
      const tipoDocumentoMappato = TIPO_DOCUMENTO[documentoTipo.toLowerCase()] || documentoTipo;
      await waitForSelectorAndSelect(page, 'select[name="tipo0"]', [tipoDocumentoMappato]);
    },
    'non sono riuscito ad inserire il tipo di documento:'
  );

  await tryOrThrow(
    async () => {
      const selector = 'input[name="icciddonating0"]';
      await waitForSelectorAndTypeEvaluated(page, selector, '');
      await waitForSelectorAndType(page, selector, iccid as string);
    },
    'non sono riuscito ad inserire il iccid:'
  );

  await tryOrThrow(
    () => waitForSelectorAndType(page, 'input[name="numerodocumento0"]', documentoNumero),
    'non sono riuscito ad inserire il numero del documento:'
  );

  let errors: (string | undefined)[] = [];

  try {
    errors = await page.evaluate(() => {
      const elements = document.querySelectorAll('.error-message:not(.ng-hide');

      return Array.from(elements).map(el => el.textContent?.trim()).filter(Boolean);
    });
  } catch (ex) {
    // nessun errore apparso
    // nulla da fare
  }

  if (errors && errors.length) {
    throw new TryOrThrowError(errors.join('. '));
  }

  await tryOrThrow(
    async () => {
      const btnProcediSelector = 'a[title="Procedi"]:not(.ng-hide)';
      await waitForVisible(page, btnProcediSelector);
      await Promise.all([
        waitForNavigation(page),
        waitForSelectorAndClickEvaluated(page, btnProcediSelector)
      ]);
    },
    'non sono riuscito a cliccare su `Procedi`:'
  );
}
