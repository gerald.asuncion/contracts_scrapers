import { Page } from "puppeteer";
import FastwebInserimentoPayload from "../payload/FastwebInserimentoPayload";
import tryOrThrow, { TryOrThrowError } from "../../../utils/tryOrThrow";
import waitForSelectorAndType from "../../../browser/page/waitForSelectorAndType";
import waitForXPathAndClick from "../../../browser/page/waitForXPathAndClick";
import waitForVisible from "../../../browser/page/waitForVisible";
import getElementText from "../../../browser/page/getElementText";
import waitForTimeout from "../../../browser/page/waitForTimeout";
import waitLoadingCarrello from "./waitLoadingCarrello";
import { MysqlPool } from "../../../mysql";
import getCouponFromDatabase from "./getCouponFromDatabase";
import waitForXPathVisible from "../../../browser/page/waitForXPathVisible";
import getPopupRef from '../../../browser/page/getPopupRef';

const COUPON_RESULT_SELECTOR = '#status';
const BTN_PROCEDI_SELECTOR = '//button[contains(., "Procedi")]';

async function getCouponStatus(page: Page): Promise<string> {
  await waitForVisible(page, COUPON_RESULT_SELECTOR);

  let result = await getElementText(page, COUPON_RESULT_SELECTOR, 'textContent');

  if (result?.toLowerCase().includes('richiesta in corso')) {
    await waitForTimeout(page, 100);
    result = await getCouponStatus(page);
  }

  return result as string;
}

export default async function gestisciCoupon(
  page: Page,
  db: MysqlPool,
  { sconto1 }: FastwebInserimentoPayload
) {
  const coupon = await getCouponFromDatabase(db, sconto1 as string);

  const popupPage = await getPopupRef(page);

  if (coupon) {
    await tryOrThrow(() => waitForSelectorAndType(popupPage, 'input[type="text"]', coupon.codice), 'non sono riuscito ad inserire il coupon:');

    await tryOrThrow(() => waitForXPathAndClick(popupPage, '//button[contains(., "Valida")]'), 'non sono riuscito a cliccare sul pulsante `Valida`:');

    await waitForVisible(popupPage, COUPON_RESULT_SELECTOR);

    try {
      const result = await getCouponStatus(popupPage);

      if (result.toLowerCase().includes('non valido')) {
        throw new TryOrThrowError(result);
      }
    } catch (ex) {
      // popup sparito prima di poter prendere il risultato
      // in questo caso il coupon è valido
    }
  } else {
    await tryOrThrow(async () => {
      await waitForXPathVisible(popupPage, BTN_PROCEDI_SELECTOR);
      await waitForXPathAndClick(popupPage, BTN_PROCEDI_SELECTOR);
    }, 'non sono riuscito a cliccare sul pulsante `Procedi`:');
  }

  await waitLoadingCarrello(page);
}
