import { Page } from "puppeteer";
import tryOrThrow, { TryOrThrowError } from "../../../utils/tryOrThrow";
import waitForSelectorAndType from "../../../browser/page/waitForSelectorAndType";
import attendiSparizioneOverlay from "../fattibilita/attendiSparizioneOverlay";
import waitForVisible from "../../../browser/page/waitForVisible";
import extractErrorMessage from "../../../utils/extractErrorMessage";
import waitForSelectorAndClickEvaluated from "../../../browser/page/waitForSelectorAndClickEvaluated";
import waitForSelectorAndClick from "../../../browser/page/waitForSelectorAndClick";
import inserisciComuneDiNascita from "../fattibilita/morosita/dati-fiscali/inserisciComuneDiNascita";
import inserisciProvinciaDiNascita from "../fattibilita/morosita/dati-fiscali/inserisciProvinciaDiNascita";
import inserisciStatoDiNascita from "../fattibilita/morosita/dati-fiscali/inserisciStatoDiNascita";
import getElementText from "../../../browser/page/getElementText";
import { FastwebPostFirmaPayload } from "../payload/FastwebPostFirmaPayload";

export default async function inserisciDatiAnagrafici(
  page: Page,
  {
    nome,
    cognome,
    codiceFiscale,
    sesso,
    dataNascita,
    nascitaStato,
    comuneNascitaEstero,
    comuneNascita,
    provinciaNascita,
    cellulare,
    email
  }: FastwebPostFirmaPayload
) {
  await tryOrThrow(
    () => waitForSelectorAndType(page, '#datiAnagraficiRES > div:nth-child(1) > div:nth-child(3) > input', codiceFiscale),
    'non sono riuscito ad inserire il codice fiscale:'
  );

  await attendiSparizioneOverlay(page);

  try {
    // eslint-disable-next-line max-len
    const codiceFiscaleModalErrorSelector = 'uib-accordion > [role="tablist"] > .panel-success[is-open="true"] > .in[aria-expanded="true"] > .panel-body > div:nth-child(1)';
    await waitForVisible(page, codiceFiscaleModalErrorSelector, { timeout: 1000 });

    // lo split su '\n' serve per togliere dal messaggio la stringa 'Per continuare clicca "Conferma"'
    const [errMsg] = (await page.$eval(codiceFiscaleModalErrorSelector, (el) => el.textContent?.trim() || '')).split('\n');

    throw new TryOrThrowError(errMsg.trim());
  } catch (exCodiceFiscale) {
    // nulla da fare
    const msg = extractErrorMessage(exCodiceFiscale);
    if (!msg.toLowerCase().includes('timeout')) {
      throw exCodiceFiscale;
    }
  }

  await tryOrThrow(
    () => waitForSelectorAndType(page, '#datiAnagraficiRES > div:nth-child(1) > div:nth-child(1) > input', nome),
    'non sono riuscito ad inserire il nome:'
  );

  await tryOrThrow(
    () => waitForSelectorAndType(page, '#datiAnagraficiRES > div:nth-child(1) > div:nth-child(2) > input', cognome),
    'non sono risciuto ad inserire il cognome:'
  );


  await tryOrThrow(
    () => waitForSelectorAndClickEvaluated(page, `input[name="section1_sesso"][value="${sesso.toUpperCase()}"]`),
    'non sono riuscito a selezionare il sesso:'
  );

  await tryOrThrow(
    () => waitForSelectorAndType(page, '#datiAnagraficiRES > div:nth-child(2) > div:nth-child(2) > p > input', dataNascita),
    'non sono riuscito ad inserire la data di nascita:'
  );

  const isNatoEstero = comuneNascitaEstero || provinciaNascita.toLowerCase() === 'ee';

  await tryOrThrow(
    () => waitForSelectorAndClick(page, `input[name="section1_natoAllEstero"][value="${isNatoEstero.toString()}"]`),
    'non sono riuscito a selezionare se il comune di nascita sia estero o no:'
  );

  await tryOrThrow(() => inserisciStatoDiNascita(page, nascitaStato || 'italia'), 'non sono riuscito ad inserire lo stato di nascita:');

  if (!isNatoEstero) {
    await tryOrThrow(() => inserisciComuneDiNascita(page, comuneNascita), 'non sono riuscito ad inserire il comune di nascita:');

    await tryOrThrow(() => inserisciProvinciaDiNascita(page, provinciaNascita), 'non sono riuscito ad inserire la provincia di nascita:');
  }

  await tryOrThrow(
    () => waitForSelectorAndClick(page, '#datiAnagraficiRES > div:nth-child(3) > div.col-md-2 > button'),
    'non sono riuscito a cliccare sul pulsante verifica:'
  );

  await attendiSparizioneOverlay(page);

  let errMsg;
  try {
    await waitForVisible(page, 'div[role="dialog"]');
    errMsg = await getElementText(page, 'div[role="dialog"] div[role="alert"] > div', 'textContent');
  } catch (ex) {
    // nessuna modale di errore
  }

  if (errMsg) {
    throw new TryOrThrowError(errMsg.trim());
  }

  await tryOrThrow(
    () => waitForSelectorAndType(page, '#datiAnagraficiRESSHP > div > div:nth-child(1) > input', cellulare),
    'no sono riuscito ad inserire il cellulare:'
  );

  await tryOrThrow(
    () => waitForSelectorAndType(page, '#datiAnagraficiRESSHP > div > div:nth-child(2) > input', email),
    'non sono riuscito ad inserire la mail:'
  );

  await tryOrThrow(
    () => waitForSelectorAndType(page, '#datiAnagraficiRESSHP > div > div:nth-child(3) > input', email),
    'non sono riuscito ad inserire la conferma mail:'
  );

  await attendiSparizioneOverlay(page);

  try {
    await waitForVisible(page, 'div[role="dialog"]');
    errMsg = await getElementText(page, 'div[role="dialog"] div[role="alert"] > div', 'textContent');
  } catch (ex) {
    // nessuna modale di errore
  }

  if (errMsg) {
    throw new TryOrThrowError(errMsg.trim());
  }
}
