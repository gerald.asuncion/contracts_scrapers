import { Page } from "puppeteer";
import { FastwebPostFirmaPayload } from "../payload/FastwebPostFirmaPayload";
import tryOrThrow, { TryOrThrowError } from "../../../utils/tryOrThrow";
import inserisciDatoConSuggestion from "../fattibilita/inserisciDatoConSuggestion";
import waitForSelectorAndType from "../../../browser/page/waitForSelectorAndType";
import inserisciDatoConSuggestionSeAbilitato from "../fattibilita/inserisciDatoConSuggestionSeAbilitato";
import waitForXPathAndClick from "../../../browser/page/waitForXPathAndClick";
import attendiSparizioneOverlay from "../fattibilita/attendiSparizioneOverlay";
import waitForTimeout from "../../../browser/page/waitForTimeout";
import recuperaEsitoCheck from "../fattibilita/recuperaEsitoCheck";
import selectSuggestion from "../helpers/selectSuggestion";
import inserisciIndirizzo from "./inserisciIndirizzo";

export default async function inserisciDatiResidenzaMobile(
  page: Page,
  {
    residenzaComune,
    residenzaIndirizzo,
    residenzaCivico,
    residenzaCap,
    residenzaProvincia
  }: FastwebPostFirmaPayload
) {
  await tryOrThrow(
    () => inserisciDatoConSuggestion(page, 'input[name="section2_cittaDiResidenza2"]', residenzaComune, 'Città Residenza', false),
    'non sono riuscito ad inserire la città di residenza:'
  );

  await tryOrThrow(
    () => inserisciIndirizzo(page, 'input[name="section2_indirizzoDiResidenza2"]', residenzaIndirizzo, 'Indirizzo di Residenza'),
    "non sono riuscito ad inserire l'indirizzo di residenza:"
  );

  await tryOrThrow(
    () => selectSuggestion(page, 'input[name="section2_civicoDiResidenza2"]', residenzaCivico.split('/')[0], residenzaCivico, 'Civico di Residenza'),
    'non sono riuscito ad inserire il civico di residenza:'
  );

  await tryOrThrow(
    async () => {
      const selector = 'input[name="section2_capDiResidenza2"]';
      const currentValue = await page.$eval(selector, (el) => (el as HTMLInputElement).value?.trim());

      if (!currentValue) {
        await waitForSelectorAndType(page, selector, residenzaCap);
      }
    },
    'non sono riuscito ad inserire il cap di residenza:'
  );

  await tryOrThrow(
    () => inserisciDatoConSuggestionSeAbilitato(page, 'input[name="section2_provinciaDiResidenza2"]', residenzaProvincia, 'Provincia di Residenza'),
    'non sono riuscito ad inserire la provincia di residenza:'
  );

  await tryOrThrow(
    () => waitForXPathAndClick(page, '//div[contains(@id, "verificaDatiMobileId")]//button[contains(., "Verifica Dati Fiscali")]'),
    'non sono riuscito a cliccare sul pulsante `Verifica Dati Fiscali`:'
  );


  await attendiSparizioneOverlay(page);

  let modalErrorMsg: string | undefined;
  try {
    await page.waitForSelector(
      'body > div.modal.fade.ng-isolate-scope.in > div > div > div.alert.ng-scope.ng-isolate-scope.alert-danger',
      { timeout: 3000 }
    );
    modalErrorMsg = await page.$eval(
      'body > div.modal.fade.ng-isolate-scope.in > div > div > div.alert.ng-scope.ng-isolate-scope.alert-danger > div > span',
      (el) => el.textContent?.trim()
    );
  } catch (ex) {
    // modale d'errore non apparsa
  }

  if (modalErrorMsg) {
    throw new TryOrThrowError(/* modalErrorMsg */'Indirizzo non coperto');
  }

  await waitForTimeout(page, 1000);

  const esito = await tryOrThrow(
    () => recuperaEsitoCheck(page, '#verificaDatiMobileId > div > div.col-md-8 > textarea'),
    "non sono riuscito a recuperare l'esito:"
  );

  if (!esito?.includes('cliente acquisibile')) {
    throw new TryOrThrowError(/* esito as string */'Cliente moroso');
  }
}
