import { Page } from "puppeteer";
import tryOrThrow from "../../../utils/tryOrThrow";
import selezionaMetodoPagamento from "./selezionaMetodoPagamento";
import waitForSelectorAndClick from "../../../browser/page/waitForSelectorAndClick";
import waitForXPathAndClick from "../../../browser/page/waitForXPathAndClick";
import { FastwebPostFirmaPayload } from "../payload/FastwebPostFirmaPayload";
import inserisciMetodoPagamento from "./inserisciMetodoPagamento";

export default async function inserisciDatiPagamento(page: Page, payload: FastwebPostFirmaPayload) {
  const { tipoOfferta, tipoPagamento } = payload;

  if (tipoPagamento) {
    await tryOrThrow(
      () => selezionaMetodoPagamento(page, tipoPagamento, tipoOfferta.toLowerCase() === 'mobile' ? 'input[name="section3_metodoDiPagamento2"]' : undefined),
      'non sono riuscito a selezionare il tipo di pagamento:'
    );

    await tryOrThrow(
      () => waitForSelectorAndClick(page, 'div[heading="Metodo di Pagamento"] div[heading="Completamento Dati"] a'),
      'non sono riuscito ad aprire il box `Completamento Dati`:'
    );

    if (tipoOfferta.toLowerCase().includes('mobile')) {
      await tryOrThrow(
        () => waitForXPathAndClick(page, '//fieldset[@ng-form="form.section3.Form2"]//div[2]//fieldset//div[6]//button[contains(., "Copia Dati Cliente")]'),
        'non sono riuscito a copiare i dati del cliente:'
      );
    } else {
      await inserisciMetodoPagamento(page, payload);
    }
  }
}
