import { Page } from "puppeteer";
import FastwebInserimentoPayload from "../payload/FastwebInserimentoPayload";
import inserisciComuneResidenza from "../fattibilita/morosita/residenza/inserisciComuneResidenza";
import inserisciIndirizzoResidenza from "./inserisciIndirizzoResidenza";
import { inserisciCivicoResidenzaV2 } from "../fattibilita/morosita/residenza/inserisciCivicoResidenza";
import inserisciCapResidenza from "../fattibilita/morosita/residenza/inserisciCapResidenza";
import inserisciProvinciaResidenza from "../fattibilita/morosita/residenza/inserisciProvinciaResidenza";
import isDisabled from "../../../browser/page/isDisabled";
import waitForSelectorAndClick from "../../../browser/page/waitForSelectorAndClick";
import attendiSparizioneOverlay from "../fattibilita/attendiSparizioneOverlay";
import tryOrThrow, { TryOrThrowError } from "../../../utils/tryOrThrow";
import waitForTimeout from "../../../browser/page/waitForTimeout";
import recuperaEsitoCheck from "../fattibilita/recuperaEsitoCheck";
import waitForSelectorAndType from "../../../browser/page/waitForSelectorAndType";
import isVisible from "../../../browser/page/isVisible";
import getOptionToSelect from "../../eni/inserimentocontratto/getOptionToSelect";
import waitForSelectorAndSelect from "../../../browser/page/waitForSelectorAndSelect";
import waitForXPathAndClick from "../../../browser/page/waitForXPathAndClick";

const UTILIZZOLINEA_SELECT_SELECTOR = 'select[name="section2_utilizzoLinea"]';
const TIPOLINEA_SELECT_SELECTOR = 'select[name="section2_tipoLinea"]';

export default async function inserisciDatiResidenzaFisso(
  page: Page,
  {
    attivazioneScala,
    attivazioneInterno,
    attivazionePiano,
    residenzaComune,
    residenzaIndirizzo,
    residenzaCivico,
    residenzaCap,
    residenzaProvincia,
    clientePossiedeUnaNumerazioneDaPortare,
    clientePossiedeUnaLineaDati
  }: FastwebInserimentoPayload
) {
  await tryOrThrow(() => inserisciComuneResidenza(page, residenzaComune), 'non sono riuscito il comune di residenza:');

  await tryOrThrow(() => inserisciIndirizzoResidenza(page, residenzaIndirizzo), "non sono riuscito ad inserire l'indirizzo di residenza:");

  await tryOrThrow(() => inserisciCivicoResidenzaV2(page, residenzaCivico), 'non sono riuscito ad inserire il civico di residenza:');

  await tryOrThrow(() => inserisciCapResidenza(page, residenzaCap), 'non sono riuscito ad inserire il cap di residenza:');

  await tryOrThrow(() => inserisciProvinciaResidenza(page, residenzaProvincia), 'non sono riuscito ad inserire la provincia di residenza:');

  const verificaBtnSelector = '#verificaDatiMobileId > div > div.col-md-4 > button';
  if (await isDisabled(page, verificaBtnSelector)) {
    throw new TryOrThrowError('impossibile completare la verifica dei dati di residenza pulsante `verifica dati fiscali` disabilitato');
  }
  await tryOrThrow(() => waitForSelectorAndClick(page, verificaBtnSelector), 'non sono riuscito a cliccare sul pulsante verifica:');

  await attendiSparizioneOverlay(page);

  let modalErrorMsg: string | undefined;
  try {
    await page.waitForSelector(
      'body > div.modal.fade.ng-isolate-scope.in > div > div > div.alert.ng-scope.ng-isolate-scope.alert-danger',
      { timeout: 3000 }
    );
    modalErrorMsg = await page.$eval(
      'body > div.modal.fade.ng-isolate-scope.in > div > div > div.alert.ng-scope.ng-isolate-scope.alert-danger > div > span',
      (el) => el.textContent?.trim()
    );
  } catch (ex) {
    // modale d'errore non apparsa
  }

  if (modalErrorMsg) {
    if (modalErrorMsg?.includes('cliente non acquisibile')) {
      throw new TryOrThrowError('Cliente moroso');
    }
    throw new TryOrThrowError(modalErrorMsg);
  }

  await waitForTimeout(page, 1000);

  const esito = await tryOrThrow(
    () => recuperaEsitoCheck(page, '#verificaDatiMobileId > div > div.col-md-8 > textarea'),
    "non sono riuscito a recuperare l'esito"
  );

  if (!esito?.includes('cliente acquisibile')) {
    throw new TryOrThrowError(/* esito as string */'Cliente moroso');
  }

  await tryOrThrow(
    () => waitForSelectorAndClick(page, 'div[heading="Completamento Dati Attivazione e Portabilità"] a'),
    'non sono riuscito ad aprire il pannello `Completamento Dati Attivazione e Portabilità`'
  );

  if (attivazioneScala) {
    await tryOrThrow(
      () => waitForSelectorAndType(page, 'input[name="section2_scala"]', attivazioneScala),
      'non sono riuscito ad inserire la scala:'
    );
  }

  await tryOrThrow(
    () => waitForSelectorAndType(page, 'input[name="section2_piano"]', attivazionePiano || 'N/D'),
    'non sono riuscito ad inserire il piano:'
  );

  if (attivazioneInterno) {
    await tryOrThrow(
      () => waitForSelectorAndType(page, 'input[name="section2_interno"]', attivazioneInterno),
      "non sono riuscito ad inserire l'interno:"
    );
  }

  if (clientePossiedeUnaNumerazioneDaPortare === 'si' || clientePossiedeUnaLineaDati === 'si') {
    if (await isVisible(page, UTILIZZOLINEA_SELECT_SELECTOR)) {
      await tryOrThrow(
        async () => {
          const option = await getOptionToSelect(page, UTILIZZOLINEA_SELECT_SELECTOR, 'Telefono e FAX');
          await waitForSelectorAndSelect(page, UTILIZZOLINEA_SELECT_SELECTOR, [option as string]);
        },
        'non sono riuscito a selezionare `utilizzo linea = Telefono e FAX`'
      );
    }

    if (await isVisible(page, TIPOLINEA_SELECT_SELECTOR)) {
      await tryOrThrow(
        async () => {
          const option = await getOptionToSelect(page, TIPOLINEA_SELECT_SELECTOR, 'Tradizionale');
          await waitForSelectorAndSelect(page, TIPOLINEA_SELECT_SELECTOR, [option as string]);
        },
        'non sono riuscito a selezionare `tipo linea = Tradizionale`'
      );
    }

    await tryOrThrow(
      () => waitForXPathAndClick(page, '//fieldset[@id="phase1_tipologiaFissoMobile"][contains(@ng-form, "Form2")]//div[12]//button[contains(., "Copia Dati Cliente")]'),
      'non sono riuscito a cliccare sul pulsante `Copia Dati Cliente`'
    );

    await tryOrThrow(
      () => waitForXPathAndClick(page, '//fieldset[@id="phase1_tipologiaFissoMobile"][contains(@ng-form, "Form2")]//div[11]//button[contains(., "Copia Indirizzo di Attivazione")]'),
      'non sono riuscito a cliccare sul pulsante `Copia Indirizzo di Attivazione`'
    );
  }
}
