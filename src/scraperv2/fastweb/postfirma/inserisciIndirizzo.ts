import { Page } from "puppeteer";
import waitForSelectorAndType from "../../../browser/page/waitForSelectorAndType";
import waitForTimeout from "../../../browser/page/waitForTimeout";
import extractErrorMessage from "../../../utils/extractErrorMessage";
import attendiSparizioneOverlay from "../fattibilita/attendiSparizioneOverlay";
import getDropdownMenuItemSelector from "../helpers/getDropdownMenuItemSelector";
import getDropdownMenuSelector from "../helpers/getDropdownMenuSelector";
import getPreValue from "../helpers/getPreValue";

export default async function inserisciIndirizzo(page: Page, selector: string, dato: string, fieldName: string) {
  await waitForSelectorAndType(page, selector, getPreValue(dato.split('-')[0]));

  await attendiSparizioneOverlay(page);

  const menuSelector = getDropdownMenuSelector(selector);

  const menuItemSelector = getDropdownMenuItemSelector(menuSelector);

  try {
    await page.waitForFunction(
      (internalSelector: string) => $(internalSelector).length !== 0,
      { polling: 100, timeout: 30000 },
      menuItemSelector
    );
  } catch (ex) {
    const errMsg = extractErrorMessage(ex);
    if (!errMsg.toLowerCase().includes('timeout')) {
      throw ex;
    }

    throw new Error(`Impossibile selezionare "${dato}" nel campo "${fieldName}"`);
  }

  await waitForTimeout(page, 4000);

  await page.evaluate((internalSelector: string, valoreDaSelezionare: string) => {
    function comparaTesti(primo: string, secondo: string): boolean {
      // eslint-disable-next-line newline-per-chained-call
      const left = primo.toLowerCase().normalize('NFD').replace(/[\u0300-\u036f]/g, '').replace(/'/g, ' ').trim();
      // eslint-disable-next-line newline-per-chained-call
      const right = secondo.toLowerCase().normalize('NFD').replace(/[\u0300-\u036f]/g, '').replace(/'/g, ' ').trim();

      return left === right;
    }

    const $el = $(internalSelector);

    if ($el.length) {
      const ul = $el[0];

      const list = [...ul.querySelectorAll<HTMLLIElement>('li')];

      const item = list.find((li) => comparaTesti(valoreDaSelezionare, li.textContent?.trim() || ''));

      if (item) {
        item.click();
      } else {
        if (list.length === 1) {
          list[0].click();
        }
      }
    }
  }, menuSelector, dato.toLowerCase());
}
