import { Page } from "puppeteer";
import { FastwebPostFirmaPayload } from "../payload/FastwebPostFirmaPayload";
import waitForVisible from "../../../browser/page/waitForVisible";
import waitForSelectorAndType from "../../../browser/page/waitForSelectorAndType";
import attendiSparizioneOverlay from "../fattibilita/attendiSparizioneOverlay";
import getElementText from "../../../browser/page/getElementText";
import tryOrThrow, { TryOrThrowError } from "../../../utils/tryOrThrow";
import waitForXPathAndClick from "../../../browser/page/waitForXPathAndClick";
import waitForNavigation from "../../../browser/page/waitForNavigation";

export default async function inserisciIban(page: Page, payload: FastwebPostFirmaPayload) {
  await waitForVisible(page, 'input[name="section3_codiceIban"]');
  await tryOrThrow(
    () => waitForSelectorAndType(page, 'input[name="section3_codiceIban"]', payload.iban as string),
    "non sono riuscito ad inserire l'iban:"
  );

  await attendiSparizioneOverlay(page);

  let errMsg;
  try {
    await waitForVisible(page, 'div[role="alert"]');
    errMsg = await getElementText(page, 'div[role="alert"] > div', 'textContent');
  } catch (ex) {
    // nessun errore, non devo fare nulla
  }

  if (errMsg) {
    throw new TryOrThrowError(/* errMsg.trim() */'Metodo di pagamento non accettato');
  }

  await tryOrThrow(
    () => Promise.all([
      waitForNavigation(page),
      (async () => {
        await waitForXPathAndClick(page, '//button[contains(., "Genera Riepilogo Ordine")]');

        let errMsg;
        try {
          await waitForVisible(page, 'div[role="alert"]');
          errMsg = await getElementText(page, 'div[role="alert"] > div', 'textContent');
        } catch (ex) {
          // nessun errore, non devo fare nulla
        }

        if (errMsg) {
          if (errMsg.includes('il campo Identificativo Registrazione Vocale non è stato valorizzato')) {
            await tryOrThrow(
              () => waitForXPathAndClick(page, '//div[contains(@class, "modal")][contains(@class, "in")]//button[contains(., "Procedi")]'),
              'non sono riuscito a cliccare sul pulsante `Procedi`:'
            );
          } else {
            throw new TryOrThrowError(errMsg.trim());
          }
        }
      })()
    ]),
    'non sono riuscito a cliccare sul pulsante `Genera Riepilogo Ordine`:'
  );
}
