import { MysqlPool } from "../../../mysql";
import { TryOrThrowError } from "../../../utils/tryOrThrow";

const SQL_OFFERTA = 'SELECT * FROM fastweb_offerte WHERE offerta = ?;';
const SQL_SCONTO = 'SELECT * FROM fastweb_sconti WHERE label = ?;';
const SQL_PARTNERSHIPS = 'SELECT * FROM fastweb_partnerships WHERE label = ?;';

export type FastwebOfferta = {
  id: number;
  offerta: string;
  prezzo: number;
  scontabile: boolean;
};

export type FastwebSconto = {
  id: number;
  label: string;
  valore: number;
};

export async function getCostoOfferteExtra(db: MysqlPool, offerte: string[]): Promise<number[]> {
  if (!offerte.length) {
    return [0];
  }

  const offerta = offerte.shift();

  const [offertaTrovata] = await db.query<FastwebOfferta>(SQL_OFFERTA, [offerta]);

  if (!offertaTrovata) {
    throw new TryOrThrowError('offerta non presente nella tabella prezzi');
  }

  return [offertaTrovata.prezzo].concat(await getCostoOfferteExtra(db, offerte));
}

export default async function getCostoOffertaConSconto(db: MysqlPool, offerta: string, sconto?: string, partnership?: string, offerteExtra?: string[]): Promise<number> {
  const [offertaTrovata] = await db.query<FastwebOfferta>(SQL_OFFERTA, [offerta]);

  if (!offertaTrovata) {
    throw new TryOrThrowError('offerta non presente nella tabella prezzi');
  }

  const prezzoBase = offertaTrovata.prezzo;

  if (!offertaTrovata.scontabile || (!sconto && !partnership)) {
    return Number(prezzoBase);
  }

  let scontoValore: number = 0;

  if (sconto) {
    const [scontoTrovato] = await db.query<FastwebSconto>(SQL_SCONTO, [sconto]);
    scontoValore = scontoTrovato.valore;
  }

  let scontoPartnershipValore: number = 0;
  if (partnership) {
    const [scontoPartnershipTrovato] = await db.query<FastwebSconto>(SQL_PARTNERSHIPS, [partnership]);
    scontoPartnershipValore = scontoPartnershipTrovato.valore;
  }

  let offerteExtraSomma = 0;
  if (offerteExtra && offerteExtra.length) {
    const offerteExtraValori = await getCostoOfferteExtra(db, offerteExtra);
    offerteExtraSomma = offerteExtraValori.reduce((acc, current) => acc + current, 0);
    console.log('offerte extra somma: ' + offerteExtraSomma)
  }

  return Number(prezzoBase - scontoValore - scontoPartnershipValore + offerteExtraSomma);
}
