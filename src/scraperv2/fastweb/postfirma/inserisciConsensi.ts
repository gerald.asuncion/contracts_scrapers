import { Page } from "puppeteer";
import FastwebInserimentoPayload from "../payload/FastwebInserimentoPayload";
import tryOrThrow from "../../../utils/tryOrThrow";
import waitForSelectorAndClick from "../../../browser/page/waitForSelectorAndClick";

export default async function inserisciConsensi(
  page: Page,
  {
    consenso1,
    consenso2,
    consenso3
  }: FastwebInserimentoPayload
) {
  await tryOrThrow(
    () => waitForSelectorAndClick(page, '[heading="Documenti e Selezione Consensi"] div[heading="Consensi"] a'),
    'non sono riuscito ad aprire il pannello per inserire i consensi:'
  );

  await tryOrThrow(
    () => waitForSelectorAndClick(page, `input[name="section4_consensoGestioneContrattoFW"][value="${consenso1.toString()}"]`),
    'non sono riuscito a selezionare il consenso `gestione contratto`:'
  );

  await tryOrThrow(
    () => waitForSelectorAndClick(page, `input[name="section4_consensoAnalisiFW"][value="${consenso2.toString()}"]`),
    'non sono riuscito a selezionare il consenso su `analisi`:'
  );

  await tryOrThrow(
    () => waitForSelectorAndClick(page, 'input[name="section4_consensoPubblicazioneElencoTelefonico"][value="true"]'),
    'non sono riuscito a selezionare il consenso su `pubblicazione elenco telefonico`:'
  );
}
