import { MysqlPool } from "../../../mysql";

const SQL_GET_ENVELOP_ID = 'SELECT envelop_id FROM fastweb_envelop_ids WHERE id_dati_contratto = ?;';

const SQL_SALVA_ENVELOP_ID = 'INSERT INTO fastweb_envelop_ids (id_dati_contratto, envelop_id) VALUES (?,?);';

export type FastwebEnvelopId = {
  id: number;
  id_dati_contratto: string;
  envelop_id: string;
};

export default async function getEnvelopIdFromDatabase(db: MysqlPool, idDatiContratto: number): Promise<string | null> {
  const [record] = await db.query<FastwebEnvelopId>(SQL_GET_ENVELOP_ID, [idDatiContratto]);

  return record ? record.envelop_id : null;
}

export async function salvaEnvelopIdSulDatabase(db: MysqlPool, idDatiContratto: number, envelopId: string) {
  await db.query<FastwebEnvelopId>(SQL_SALVA_ENVELOP_ID, [idDatiContratto, envelopId]);
}
