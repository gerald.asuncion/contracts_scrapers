const map: Record<string, string> = {
  'documento non verificato': 'Documento non verificato',
  'cliente moroso': 'Cliente moroso',
  'indirizzo non coperto': 'Indirizzo non coperto',
  'metodo di pagamento non accettato': 'Metodo di pagamento non accettato',
  'prezzo nel carrello non elaborato': 'prezzo nel carrello non elaborato',
  'il prezzo dell’offerta non coincide col contratto firmato dal cliente': 'il prezzo dell’offerta non coincide col contratto firmato dal cliente',
  'non eleggibile per eni cluster 2': 'Non eleggibile per Eni Cluster 2',
  'offerta non presente nella tabella prezzi': 'offerta non presente nella tabella prezzi'
};

export default function aggiustaErrore(exMessage: string) {
  for (let key in map) {
    if (exMessage.toLowerCase().includes(key)) {
      return map[key];
    }
  }

  return exMessage;
}
