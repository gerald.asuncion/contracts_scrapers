import { Page } from "puppeteer";
import waitForVisibleStyled from "../../../browser/page/waitForVisibleStyled";
import waitForHiddenStyled from "../../../browser/page/waitForHiddenStyled";

const SELECTOR = '.loadingBox.overlay';

export default async function waitLoadingCarrello(page: Page) {
  try {
    await waitForVisibleStyled(page, SELECTOR, { timeout: 250 });
  } catch (ex) {
    // nulla da fare
  }
  await waitForHiddenStyled(page, SELECTOR, { timeout: 120000 });
}
