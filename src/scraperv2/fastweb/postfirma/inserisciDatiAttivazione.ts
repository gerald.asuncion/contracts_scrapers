import { Page } from "puppeteer";
import FastwebInserimentoPayload from "../payload/FastwebInserimentoPayload";
import inserisciComuneAttivazione from "../fattibilita/copertura/inserisciComuneAttivazione";
import inserisciIndirizzoAttivazione from "./inserisciIndirizzoAttivazione";
import { inserisciCivicoAttivazioneV2 } from "../fattibilita/copertura/inserisciCivicoAttivazione";
import inserisciCapAttivazione from "../fattibilita/copertura/inserisciCapAttivazione";
import inserisciProvinciaAttivazione from "../fattibilita/copertura/inserisciProvinciaAttivazione";
import waitForSelectorAndClick from "../../../browser/page/waitForSelectorAndClick";
import attendiSparizioneOverlay from "../fattibilita/attendiSparizioneOverlay";
import tryOrThrow, { TryOrThrowError } from "../../../utils/tryOrThrow";
import waitForSelectorAndType from "../../../browser/page/waitForSelectorAndType";
import getOptionToSelect from "../../eni/inserimentocontratto/getOptionToSelect";
import waitForSelectorAndSelect from "../../../browser/page/waitForSelectorAndSelect";
import inserisciOperatoreFonia from "./inserisciOperatoreFonia";
import waitForXPathVisible from "../../../browser/page/waitForXPathVisible";
import FailureResponse from "../../../response/FailureResponse";
import waitForVisible from "../../../browser/page/waitForVisible";
import getElementText from "../../../browser/page/getElementText";
import waitForXPathAndClick from "../../../browser/page/waitForXPathAndClick";

function checkIfOffertaAttivabile(text: string | undefined | null) {
  if (!text?.includes('Offerta attivabile')) {
    throw new TryOrThrowError(/* text as string */"Indirizzo non coperto");
  }
}

export default async function inserisciDatiAttivazione(
  page: Page,
  {
    clientePossiedeUnaNumerazioneDaPortare,
    clientePossiedeUnaLineaDati,
    tipoOfferta,
    codiceMigrazione,
    prefissoNumeroTelefono,
    numeroTelefono,
    attualeFornitoreMobile,
    attualeFornitoreInternet,
    codicePortabilitaDati,
    operatoreProvenienzaDati,
    attivazioneComune,
    attivazioneIndirizzo,
    attivazioneCivico,
    attivazioneCap,
    attivazioneProvincia
  }: FastwebInserimentoPayload
) {
  if (clientePossiedeUnaNumerazioneDaPortare === 'si') {
    await tryOrThrow(
      () => waitForSelectorAndClick(page, 'input[name$="hasNumeroReteFissa"][value="true"]'),
      'non sono riuscito a cliccare su `si` alla domanda `Il Cliente possiede una numerazione da portare?`:'
    );

    await tryOrThrow(
      () => waitForSelectorAndType(page, 'input[name="section_codiceDiMigrazioneFonia"]', codiceMigrazione as string),
      'non sono riuscito ad inserire il codice migrazione fonia:'
    );

    await tryOrThrow(
      () => waitForSelectorAndType(page, 'input[name="section2_numeroDiTelefonoReteFissa"]', numeroTelefono as string),
      'non sono riuscito ad inserire il numero di telefono da portare:'
    );

    await tryOrThrow(
      () => inserisciOperatoreFonia(page, attualeFornitoreInternet as string),
      'non sono riuscito a selezionare operatore fonia:'
    );
  }

  if (clientePossiedeUnaLineaDati === 'si') {
    await tryOrThrow(
      () => waitForSelectorAndClick(page, 'input[name$="hasServizioAdsl"][value="true"]'),
      'non sono riuscito a cliccare su `si` alla domanda `Il Cliente possiede una linea dati?`:'
    );

    await tryOrThrow(
      () => waitForSelectorAndType(page, 'input[name="section_codiceDiMigrazioneDati"]', codicePortabilitaDati as string),
      'non sono riuscito ad inserire il codice portabilità dati:'
    );

    await tryOrThrow(
      async () => {
        const option = await getOptionToSelect(page, 'select[name="section2_operatore"]', operatoreProvenienzaDati as string);
        await waitForSelectorAndSelect(page, 'select[name="section2_operatore"]', [option as string]);
      },
      "non sono riuscito a selezionare l'operatore dati:"
    );
  }

  if (tipoOfferta.toLowerCase() !== 'mobile') {
    // indirizzo attivazione
    await tryOrThrow(() => inserisciComuneAttivazione(page, attivazioneComune), 'non sono riuscito ad inserire il comune di attivazione:');

    await tryOrThrow(() => inserisciIndirizzoAttivazione(page, attivazioneIndirizzo), "non sono riuscito ad inserire l'indirizzo di attivazione");

    await tryOrThrow(() => inserisciCivicoAttivazioneV2(page, attivazioneCivico), 'non sono riuscito ad inserire il civico di attivazione:');

    await tryOrThrow(() => inserisciCapAttivazione(page, attivazioneCap), 'non sono riuscito ad inserire il cap di attivazione:');

    await tryOrThrow(() => inserisciProvinciaAttivazione(page, attivazioneProvincia), 'non sono riuscito ad inserire la provincia di attivazione:');

    await tryOrThrow(() => waitForSelectorAndClick(page, '#datiIndirizzoRESSHP2 > div > div:nth-child(1) > button'), 'non sono riuscito a cliccare sul pulsante verifica:');

    // 14825: controllo se appare la modale di errore sull'operatore telefonico
    try {
      const modalContainerSelector = '//div[contains(@heading, "Verifica la disponibilità del servizio FASTWEB")]';
      await waitForXPathVisible(page, modalContainerSelector);
      const [el] = await page.$x(`${modalContainerSelector}//div[contains(@class, "panel-body")]`);
      const text: string = (await (await el.getProperty('textContent')).jsonValue());
      if (text.toLowerCase().startsWith('Attenzione, il numero risulta attivo su') && text.toLowerCase().includes("confermare l'operatore selezionato")) {
        return new FailureResponse("L'operatore inserito dal cliente non coincide con quello indicato dal portale fastweb");
      }
    } catch (error) {
      // modale non apparsa
      // nulla da fare
    }

    await attendiSparizioneOverlay(page);

    try {
      await waitForVisible(page, 'body > div.modal.in');

      const label = await page.$eval(
        'body > div.modal .modal-body > fieldset:nth-child(1) > div > div > label.control-label:nth-child(1)',
        (el) => el.textContent?.trim()
      );

      checkIfOffertaAttivabile(label);

      try {
        const voceDatiBtnSelector = '//div[contains(@class, "modal")][contains(@class, "in")]//div[contains(@class, "modal-footer")][not(contains(@class, "ng-hide"))]//button[contains(text(), "Voce + Dati")][not(contains(@class, "ng-hide"))]';
        await waitForXPathVisible(page, voceDatiBtnSelector);
        await waitForXPathAndClick(page, voceDatiBtnSelector);
      } catch (ex) {
        const voceDatiBtnSelector = `//div[contains(@class, "modal")][contains(@class, "in")]//div[contains(@class, "modal-footer")][not(contains(@class, "ng-hide"))]//button[contains(text(), "${attualeFornitoreInternet}")][not(contains(@class, "ng-hide"))]`;
        await waitForXPathVisible(page, voceDatiBtnSelector);
        await waitForXPathAndClick(page, voceDatiBtnSelector);
      }
    } catch (ex) {
      // modale non apparsa
    }

    try {
      const modalContainerSelector = 'body > div.modal.in';
      await waitForVisible(page, modalContainerSelector);
      const text = await getElementText(page, modalContainerSelector, 'textContent');

      checkIfOffertaAttivabile(text);

      await waitForXPathAndClick(page, '//div[contains(@class, "modal")][contains(@class, "in")]//div[contains(@class, "modal-footer")][not(contains(@class, "ng-hide"))]//button[contains(text(), "Chiudi")][not(contains(@class, "ng-hide"))]');
    } catch (ex) {
      // modale non apparsa
      // nulla da fare
    }
  }
}
