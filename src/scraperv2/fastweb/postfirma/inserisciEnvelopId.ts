import { Page } from "puppeteer";
import tryOrThrow, { TryOrThrowError } from "../../../utils/tryOrThrow";
import waitForSelectorAndClick from "../../../browser/page/waitForSelectorAndClick";
import waitForVisible from "../../../browser/page/waitForVisible";
import waitForSelectorAndType from "../../../browser/page/waitForSelectorAndType";
import waitForXPathAndClick from "../../../browser/page/waitForXPathAndClick";
import getElementText from "../../../browser/page/getElementText";
import attendiSparizioneOverlay from "../fattibilita/attendiSparizioneOverlay";
import isXPathDisabled from "../../../browser/page/isXPathDisabled";
import waitForTimeout from "../../../browser/page/waitForTimeout";

const ENVELOPID_INPUT_SELECTOR = 'input[name="EnvelopeId"]';

const BUTTON_CONFERMA_SELECTOR = '//button[@id="confermaFirmaElettTMT"][contains(., "Conferma")]';

export default async function inserisciEnvelopId(page: Page, envelopId: string, noSubmit: boolean) {
  await waitForVisible(page, 'form[name="orderSummaryForm"]');
  await waitForTimeout(page, 3000);

  await tryOrThrow(
    () => waitForSelectorAndClick(page, '#firmaElettronicaTMT'),
    'non sono riuscito a cliccare sul pulsante `Firma Elettronica TMT`:'
  );

  await waitForVisible(page, ENVELOPID_INPUT_SELECTOR, { timeout: 60000 });
  await tryOrThrow(
    () => waitForSelectorAndType(page, ENVELOPID_INPUT_SELECTOR, envelopId),
    "non sono riuscito ad inserire l'envelop id:"
  );

  await tryOrThrow(
    () => waitForXPathAndClick(page, '//button[contains(., "Verifica ENVELOPE ID")]'),
    'non sono riuscito a cliccare sul pulsante `Verifica ENVELOPE ID`:'
  );

  await attendiSparizioneOverlay(page, { timeout: 60000 });

  let errMsg;
  try {
    await waitForVisible(page, 'div[role="alert"]');
    errMsg = await getElementText(page, 'div[role="alert"] > div', 'textContent');
  } catch (ex) {
    // nessun errore, non devo fare nulla
  }

  if (errMsg) {
    throw new TryOrThrowError(errMsg.trim());
  }

  await waitForVisible(page, '#showPdaResult');

  let esito = await getElementText(page, '#showPdaResult > div > div:nth-child(1) > div:nth-child(2)', 'textContent');

  esito = esito ? esito.trim() : '';

  if (esito === 'KO') {
    let descrizioneEsito = await getElementText(page, '#showPdaResult > div > div:nth-child(2) > div:nth-child(2)', 'textContent');
    throw new TryOrThrowError(descrizioneEsito?.trim() as string);
  }

  if(await isXPathDisabled(page, BUTTON_CONFERMA_SELECTOR)) {
    throw new TryOrThrowError('il pulsante `Conferma` è disabilitato');
  }

  if (noSubmit) {
    throw new TryOrThrowError('no submit');
  }

  await tryOrThrow(
    () => waitForXPathAndClick(page, BUTTON_CONFERMA_SELECTOR),
    'non sono riuscito a cliccare sul pulsante `Conferma`:'
  );

  await attendiSparizioneOverlay(page, { timeout: 120000 });
}
