import { Page } from "puppeteer";
import { Logger } from "../../../logger";
import { FastwebPostFirmaPayload } from "../payload/FastwebPostFirmaPayload";
import recuperaEnvelopIdDalPortale from "./recuperaEnvelopIdDalPortale";
import { MysqlPool } from "../../../mysql";
import getEnvelopIdFromDatabase, { salvaEnvelopIdSulDatabase } from "./getEnvelopIdFromDatabase";

export async function getEnvelopId(
  page: Page,
  logger: Logger,
  db: MysqlPool,
  payload: FastwebPostFirmaPayload,
  controllaSessioneDisponibile: (page: Page) => Promise<void>,
  options: { baseDownloadPath: string; }
): Promise<[string, Page]> {
  const idEsterno = payload.idEsterno;

  const envelopId = await getEnvelopIdFromDatabase(db, idEsterno);

  if (envelopId) {
    return [envelopId, page];
  }

  const [envelopIdDalPortale, otherPage] = await recuperaEnvelopIdDalPortale(page, logger, payload, controllaSessioneDisponibile, options);

  await salvaEnvelopIdSulDatabase(db, idEsterno, envelopIdDalPortale);

  return [envelopIdDalPortale, otherPage];
}
