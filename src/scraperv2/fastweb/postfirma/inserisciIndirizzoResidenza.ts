import { Page } from 'puppeteer';
import inserisciIndirizzo from './inserisciIndirizzo';

const selector = '#datiIndirizzoRES_FissoMobile > div:nth-child(1) > div:nth-child(2) > input';

export default async function inserisciIndirizzoResidenza(page: Page, attivazioneIndirizzo: string): Promise<void> {
  await inserisciIndirizzo(page, selector, attivazioneIndirizzo, 'indirizzo di residenza');
}
