import { Page } from "puppeteer";
import { MysqlPool } from "../../../mysql";
import { FastwebPostFirmaPayload } from "../payload/FastwebPostFirmaPayload";
import getCostoOffertaConSconto from "./getCostoOffertaConSconto";
import { TryOrThrowError } from "../../../utils/tryOrThrow";

export default async function verificaPrezzoMobile(page: Page, db: MysqlPool, { offertaMobile }: FastwebPostFirmaPayload) {
  const prezzo = await getCostoOffertaConSconto(db, offertaMobile as string);

  const [el] = await page.$x(`//td[contains(@class, "summary-product-name")][.="${offertaMobile}"]/parent::tr/parent::tbody/parent::table/parent::td//td[contains(@class, "cost-recurring-total")]`);

  let prezzoNelCarrelloStr = await (await el.getProperty('textContent')).jsonValue() as string;

  if (!prezzoNelCarrelloStr) {
    throw new TryOrThrowError('prezzo nel carrello non trovato');
  }

  prezzoNelCarrelloStr = prezzoNelCarrelloStr.replace(/\n/g, '').replace(/ /g, '').trim();

  const matches = prezzoNelCarrelloStr.match(/\d+|\.|,/g);

  if (!matches) {
    throw new TryOrThrowError('prezzo nel carrello non elaborato');
  }

  const prezzoNelCarrello = Number(matches.join('').replace(',', '.'));

  if (prezzoNelCarrello !== prezzo) {
    throw new TryOrThrowError('Il prezzo dell’offerta non coincide col contratto firmato dal cliente');
  }
}
