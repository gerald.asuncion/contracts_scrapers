import { Page } from "puppeteer";
import waitForSelectorAndClick from "../../../browser/page/waitForSelectorAndClick";
import waitForSelectorAndSelect from "../../../browser/page/waitForSelectorAndSelect";
import waitForTimeout from "../../../browser/page/waitForTimeout";
import waitForXPathAndClick from "../../../browser/page/waitForXPathAndClick";
import waitForXPathAndSelect from "../../../browser/page/waitForXPathAndSelect";
import waitForXPathVisible from "../../../browser/page/waitForXPathVisible";
import tryOrThrow from "../../../utils/tryOrThrow";
import { FastwebPostFirmaPayload } from "../payload/FastwebPostFirmaPayload";
import waitLoadingCarrello from "./waitLoadingCarrello";

const BOX_OPZIONI_SELECTOR = '//div[contains(@class, "main-content-box-header")][contains(., "Opzioni")]//img';
const BOX_TIPOLOGIA_SCONTO_SELECTOR = '//div[contains(@class, "main-content-box-header")][contains(., "Tipologia Sconto")]//img';
const BOX_SCONTI_SELECTOR = '//div[contains(@class, "main-content-box-header")][contains(., "Sconti")]//img';

export default async function gestisciOpzioni(page: Page, { tipoOfferta, isProdottoPortabilita, opzioneFisso1, opzioneFisso3, opzioneFisso8 }: FastwebPostFirmaPayload) {
  await waitForTimeout(page, 6000);

  if (tipoOfferta.toLowerCase() === 'mobile') {
    await tryOrThrow(() => waitForXPathAndClick(page, '//span[.="TipoMobile"]/parent::div//img'), 'non sono riuscito ad aprire il pannello `TipoMobile`:');

    await tryOrThrow(
      () => waitForXPathAndSelect(page, '//div[contains(@id, "repeatAttributes")]/div[1]//select[contains(@id, "onlineSelectList")]', ['Ricarica Automatica']),
      'non sono riuscito ad selezionare il tipo `Ricarica Automatica`:'
    );

    await waitLoadingCarrello(page);

    await tryOrThrow(
      () => waitForXPathAndSelect(page, '//div[contains(@id, "repeatAttributes")]/div[2]//select[contains(@id, "onlineSelectList")]', ['One shot/No terminale']),
      'non sono riuscito ad selezionare `One shot/No terminale` per la vendita terminale:'
    );

    await waitLoadingCarrello(page);

    if (isProdottoPortabilita) {
      await tryOrThrow(() => waitForXPathAndClick(page, '//div[contains(@class, "main-content-box-header")][contains(., "Configura SIM")]//img'), 'non sono riuscito ad aprire il pannello `Configura SIM`:');

      await tryOrThrow(
        () => waitForXPathAndClick(page, '//label[contains(., "Richiesta MNP")]/parent::div/parent::div//input[contains(@type, "checkbox")]'),
        'non sono riscito a cliccare sul checkbox `Richiesta MNP`:'
      );

      await waitLoadingCarrello(page);

      await tryOrThrow(
        () => waitForXPathAndClick(page, '//label[contains(., "Richiesta TCR")]/parent::div/parent::div//input[contains(@type, "checkbox")]'),
        'non sono riscito a cliccare sul checkbox `Richiesta TCR`:'
      );

      await waitLoadingCarrello(page);
    }
  } else {
    await waitForXPathVisible(page, BOX_OPZIONI_SELECTOR);
    await waitForXPathAndClick(page, BOX_OPZIONI_SELECTOR);

    await waitForXPathAndClick(page, '//label[contains(., "FREEDOM: liberi di cambiare idea")]/parent::div/following-sibling::table//input[@type="checkbox"]');

    if (opzioneFisso1 && opzioneFisso1 === 'Servizio potenziamento Wi Fi') {
      await waitForXPathAndClick(page, '//label[contains(., "Servizio di estensione del segnale Wi Fi")]/parent::div/following-sibling::table//td[contains(@class, "complexActionCol")]//img[@class = "img-add"]');
    }

    await waitLoadingCarrello(page);

    if (opzioneFisso1 && ["Servizio di estensione del segnale Wi Fi", "Servizio potenziamento Wi Fi"].includes(opzioneFisso1)) {
      if (opzioneFisso1 === "Servizio di estensione del segnale Wi Fi") {
        await waitForXPathAndClick(page, '//label[contains(., "Servizio di estensione del segnale Wi Fi")]/parent::div/following-sibling::table//td[contains(@class, "complexActionCol")]//img[@class = "img-add"]');
      } else {
        await waitForXPathAndClick(page, '//label[contains(., "Servizio potenziamento Wi Fi")]/parent::div/following-sibling::table//input[@type="checkbox"]');
      }
    }

    if (opzioneFisso3 && opzioneFisso3 === "Assicurazione Assistenza Casa Quixa") {
      await waitLoadingCarrello(page);
      await waitForXPathAndClick(page, '//label[contains(., "Opzione assicurazione assistenza casa")]/parent::div/following-sibling::table//input[@type="checkbox"]');
    }

    await waitLoadingCarrello(page);

    if (opzioneFisso8 && opzioneFisso8 === "Sconto Fisso Mobile") {
      await waitForXPathAndClick(page, BOX_TIPOLOGIA_SCONTO_SELECTOR);
      await waitForSelectorAndSelect(page, "select[id$='onlineSelectList']", ["Convergenza"]);

      await waitLoadingCarrello(page);

      await waitForXPathAndClick(page, BOX_SCONTI_SELECTOR);
      await waitForXPathAndClick(page, '//label[contains(., "Sconto Fisso Mobile 3 euro")]/parent::div/following-sibling::table//td[contains(@class, "complexActionCol")]//input[@type="checkbox"]');

      await waitLoadingCarrello(page);
    }
  }

  await waitForSelectorAndClick(page, '#saveButton');

  await waitLoadingCarrello(page);
}
