import { Page } from "puppeteer";
import getElementText from "../../../browser/page/getElementText";
import waitForSelectorAndClick from "../../../browser/page/waitForSelectorAndClick";
import waitForSelectorAndClickEvaluated from "../../../browser/page/waitForSelectorAndClickEvaluated";
import waitForSelectorClearAndType from "../../../browser/page/waitForSelectorClearAndType";
import waitForVisible from "../../../browser/page/waitForVisible";
import waitForXPathAndClick from "../../../browser/page/waitForXPathAndClick";
import delay from "../../../utils/delay";
import tryOrThrow, { TryOrThrowError } from "../../../utils/tryOrThrow";
import inserisciDatoConSuggestionAndApostropheClear from "../fattibilita/inserisciDatoConSuggestionAndApostropheClear";
import inserisciDatoConSuggestionClear from "../fattibilita/inserisciDatoConSuggestionClear";
import inserisciDatoConSuggestionClearSeAbilitato from "../fattibilita/inserisciDatoConSuggestionClearSeAbilitato";
import { FastwebPostFirmaPayload } from "../payload/FastwebPostFirmaPayload";

export default async function inserisciMetodoPagamento(
  page: Page,
  {
    nome,
    cognome,
    codiceFiscale,
    sesso,
    dataNascita,
    nascitaStato,
    comuneNascitaEstero,
    comuneNascita,
    provinciaNascita,
    metodoPagamentoNome,
    metodoPagamentoCognome,
    metodoPagamentoSesso,
    metodoPagamentoNascitaData,
    metodoPagamentoNascitaEstero,
    metodoPagamentoNascitaStato,
    metodoPagamentoNascitaComune,
    metodoPagamentoNascitaProvincia,
    metodoPagamentoCodiceFiscale,
    residenzaCivico,
    residenzaCap,
    residenzaComune,
    residenzaIndirizzo,
    residenzaProvincia,
  }: FastwebPostFirmaPayload
) {
  await delay(1000)
  await tryOrThrow(
    () =>
      waitForXPathAndClick(
        page,
        '//div[contains(@heading, "Metodo di Pagamento")]//button[contains(., "Copia Dati Cliente")]'
      ),
    "non sono riuscito a cliccare sul pulsante `Copia Dati Cliente`:"
  );

  const updateDati = metodoPagamentoNome && metodoPagamentoNome?.length > 0 ? true : false;

  if (
    updateDati &&
    metodoPagamentoNome?.toUpperCase() !== nome?.toUpperCase()
  ) {
    await tryOrThrow(
      () =>
        waitForSelectorClearAndType(
          page,
          'input[name="section3_nome"]',
          metodoPagamentoNome
        ),
      "non sono riuscito ad inserire il nome per il metodo di pagamento:"
    );
  }

  if (
    updateDati &&
    metodoPagamentoCognome?.toUpperCase() !== cognome?.toUpperCase()
  ) {
    await tryOrThrow(
      () =>
        waitForSelectorClearAndType(
          page,
          'input[name="section3_cognome"]',
          metodoPagamentoCognome
        ),
      "non sono riuscito ad inserire il cognome per il metodo di pagamento:"
    );
  }

  if (
    updateDati &&
    metodoPagamentoSesso?.toUpperCase() !== sesso?.toUpperCase()
  ) {
    await tryOrThrow(
      () =>
        waitForSelectorAndClickEvaluated(
          page,
          `input[name="section3_sesso"][value="${metodoPagamentoSesso}"]`
        ),
      "non sono riuscito a selezionare il sesso per il metodo di pagamento:"
    );
  }

  if (
    updateDati &&
    metodoPagamentoNascitaData !== dataNascita
  ) {
    await tryOrThrow(
      () =>
        waitForSelectorClearAndType(
          page,
          'input[name="section3_dataDiNascita"]',
          metodoPagamentoNascitaData
        ),
      "non sono riuscito ad inserire la data di nascita per il metodo di pagamento:"
    );
  }

  if (
    updateDati &&
    metodoPagamentoNascitaEstero !== comuneNascitaEstero
  ) {
    await tryOrThrow(
      () =>
        waitForSelectorAndClick(
          page,
          `input[name="section3_natoAllEstero"][value="${metodoPagamentoNascitaEstero.toString()}"]`
        ),
      "non sono riuscito a selezionare se nato all'estero per il metodo di pagamento:"
    );
  }

  if (
    updateDati &&
    metodoPagamentoNascitaStato?.toUpperCase() !== nascitaStato?.toUpperCase()
  ) {
    await tryOrThrow(
      () =>
        inserisciDatoConSuggestionClear(
          page,
          'input[name="section3_statoDiNascita"]',
          metodoPagamentoNascitaStato,
          "Stato di Nascita",
          false
        ),
      "non sono riuscito ad inserire lo stato di nascita per il metodo di pagamento:"
    );
  }

  if (
    updateDati &&
    metodoPagamentoNascitaComune?.toUpperCase() !== comuneNascita?.toUpperCase()
  ) {
    if (!metodoPagamentoNascitaEstero) {
      await tryOrThrow(
        () =>
        inserisciDatoConSuggestionAndApostropheClear(
            page,
            'input[name="section3_luogoDiNascita"]',
            metodoPagamentoNascitaComune,
            "Luogo di Nascita",
            false
          ),
        "non sono riuscito ad inserire il luogo di nascita per il metodo di pagamento:"
      );
    }
  }

  if (
    updateDati &&
    metodoPagamentoNascitaProvincia?.toUpperCase() !==
      provinciaNascita?.toUpperCase()
  ) {
    await tryOrThrow(
      () =>
        inserisciDatoConSuggestionClearSeAbilitato(
          page,
          'input[name="section3_provinciaDiNascita"]',
          metodoPagamentoNascitaProvincia,
          "Provincia di Nascita"
        ),
      "non sono riuscito ad inserire il luogo di nascita per il metodo di pagamento:"
    );
  }

  await tryOrThrow(async () => {
    const cFisc = metodoPagamentoCodiceFiscale ? metodoPagamentoCodiceFiscale : codiceFiscale

    await waitForSelectorClearAndType(
      page,
      'input[name="section3_codiceFiscale"]',
      cFisc
    );

    let err;
    try {
      const alertModalSelector = ".modal .alert";
      await waitForVisible(page, alertModalSelector, { timeout: 90000 });

      err = (await getElementText(
        page,
        `${alertModalSelector} > div > span`,
        "textContent"
      )) as string;
    } catch (ex) {
      // alert codice fiscale non apparso
      // nulla da fare
    }

    if (err) {
      throw new TryOrThrowError(err.trim());
    }
  }, "non sono riuscito ad inserire il codice fiscale per il metodo di pagamento:");

  let err;

  try {
    await waitForVisible(page, ".modal .alert");

    err = await getElementText(
      page,
      ".modal .alert > div > span",
      "textContent"
    );
  } catch (ex) {
    // modale di errore non apparsa
    // nulla da fare
  }

  if (err) {
    throw new TryOrThrowError(err.trim());
  }
}
