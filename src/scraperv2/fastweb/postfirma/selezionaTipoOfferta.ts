import { Page } from "puppeteer";
import waitForSelectorAndClick from "../../../browser/page/waitForSelectorAndClick";
import FastwebInserimentoPayload from "../payload/FastwebInserimentoPayload";

const TIPO_OFFERTA_MAP: Record<string, string> = {
  fisso: 'FISSO',
  'fisso+mobile': 'FISSO E MOBILE',
  'mobile': 'MOBILE'
};

export default async function selezionaTipoOfferta(page: Page, payload: FastwebInserimentoPayload) {
  await waitForSelectorAndClick(page, `input[name$="tipologiaOfferta"][value="${TIPO_OFFERTA_MAP[payload.tipoOfferta.toLowerCase()]}"]`);
}
