import { Page } from "puppeteer";
import { Logger } from "../../../logger";
import path from 'path';
import fs from 'fs';
import util from 'util';
import sp from 'simple-excel-to-json';
import { FastwebPostFirmaPayload } from "../payload/FastwebPostFirmaPayload";
import vaiAllaPaginaConsultazioneFirma from '../check-stato-contratto/vaiAllaPaginaConsultazioneFirma';
import { inserisciCodiceContrattoConRetry } from "../check-stato-contratto/inserisciCodiceContrattoDaControllare";
import { TryOrThrowError } from "../../../utils/tryOrThrow";
import waitForTimeout from "../../../browser/page/waitForTimeout";
import waitForXPathAndClick from "../../../browser/page/waitForXPathAndClick";
import waitForHidden from "../../../browser/page/waitForHidden";
import { generateIdSelector } from "../inserimento/helpers/selectors";
import waitForVisible from "../../../browser/page/waitForVisible";
import getCurrentDayFormatted from "../../../utils/getCurrentDayFormatted";
import setDownloadPath from "../../../browser/page/setDownloadPath";
import mkdir from "../../../utils/mkdir";

export default async function recuperaEnvelopIdDalPortale(
  page: Page,
  logger: Logger,
  {
    idEsterno
  }: FastwebPostFirmaPayload,
  controllaSessioneDisponibile: (page: Page) => Promise<void>,
  {
    baseDownloadPath
  }: { baseDownloadPath: string; }
): Promise<[string, Page]> {
  logger.info("vado alla pagina di check firma per recuperare l'envelop id");
  const codiceContratto = String(idEsterno);
  const internalPage = await vaiAllaPaginaConsultazioneFirma(page, logger, controllaSessioneDisponibile);

  logger.info("inserisco l'id dati contratto");
  const codiceInserito = await inserisciCodiceContrattoConRetry(internalPage, codiceContratto);
  if (codiceInserito !== codiceContratto) {
    throw new TryOrThrowError(`non sono riuscito ad inserire correttamente il codice contratto ${codiceContratto} per recuperarne poi l'evenelop id`);
  }

  logger.info('attendo caricamento dati');

  await waitForTimeout(internalPage, 100);
  await waitForXPathAndClick(internalPage, '//button[contains(., "Visualizza")]');

  try {
    await waitForHidden(internalPage, 'div[id$="_modal"].ui-widget-overlay', { timeout: 10000 });
  } catch (ex) {
    // sparito prima che potessi fare il wait
  }

  const tableRowSelector = `${generateIdSelector('pdaTable_data')} > tr`;
  await waitForVisible(internalPage, tableRowSelector);

  const noData = await internalPage.evaluate(() => !!$('td:contains(Nessuna firma elettronica trovata.)').length);

  if (noData) {
    throw new TryOrThrowError("nessun record trovato per recuperare l'envelop id");
  }

  const downloadPath = path.resolve(baseDownloadPath, getCurrentDayFormatted(), 'fastweb-postfirma');
  mkdir(downloadPath);
  logger.info(`esporto i dati (xlsx) nella cartella ${downloadPath}`);

  await setDownloadPath(internalPage, downloadPath);

  await waitForXPathAndClick(internalPage, '//button[contains(., "Esporta")]');

  logger.info('Scaricamento...');
  let fileName;
  while (!fileName || fileName.endsWith('.crdownload')) {
    await new Promise((resolve) => setTimeout(resolve, 100));
    [fileName] = await util.promisify(fs.readdir)(downloadPath);
  }

  const filePath = path.resolve(downloadPath, fileName);
  logger.info(`Scaricato il file ${filePath}; recupero l'envelop id`);

  const [doc] = sp.parseXls2Json(filePath);
  const envelopId = doc[0]['Envelope_ID'];

  fs.unlinkSync(filePath);

  return [envelopId, internalPage];
}
