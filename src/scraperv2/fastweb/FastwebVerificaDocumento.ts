import { FastwebVerificaDocumentoPayload } from './payload/FastwebCheckFattibilitaPayload';
import { ScraperResponse } from '../scraper';
import ErrorResponse from '../../response/ErrorResponse';
import FailureResponse from '../../response/FailureResponse';
import vaiAllaPaginaInserisciOrdine from './fattibilita/vaiAllaPaginaInserisciOrdine';
import extractErrorMessage from '../../utils/extractErrorMessage';
import checkDocumento from './fattibilita/documento/checkDocumento';
import checkMorosita from './fattibilita/morosita/checkMorosita';
import FastwebVerificaScraper from './fattibilita/FastwebVerificaScraper';
import createChildPayloadAwareLogger from '../../utils/createChildPayloadAwareLogger';
import { QueueTask } from '../../task/QueueTask';

@QueueTask()
export default class FastwebVerificaDocumento extends FastwebVerificaScraper<FastwebVerificaDocumentoPayload> {
  /**
   * @override
   * @param payload Request payload
   */
  async scrapeWebsite(payload: FastwebVerificaDocumentoPayload): Promise<ScraperResponse> {
    const logger = createChildPayloadAwareLogger(this.childLogger, payload);
    let page;
    const { idDatiContratto } = payload;

    try {
      page = await this.p();

      await vaiAllaPaginaInserisciOrdine(page, logger, this.controllaSessioneDisponibile);

      const morositaResult = await checkMorosita(page, payload, logger, 'Check documento');

      if (morositaResult instanceof FailureResponse) {
        await this.saveScreenshot(page, logger, 'fastweb-verifica-documento-morosita', idDatiContratto);
        await this.logout(page, logger);
        return morositaResult;
      }

      const result = await checkDocumento(page, payload, logger);

      if (result instanceof FailureResponse) {
        await this.saveScreenshot(page, logger, 'fastweb-verifica-documento', idDatiContratto);
      }
      await this.logout(page, logger);

      return result;
    } catch (ex) {
      const errMsg = extractErrorMessage(ex);
      logger.error(`${idDatiContratto} ${errMsg}`);
      if (page) {
        await this.saveScreenshot(page, logger, 'fastweb-verifica-documento-errore', idDatiContratto);
        await this.logout(page, logger);
      }
      return new ErrorResponse(errMsg);
    }
  }

  /**
   * @override
   */
  getScraperCodice() {
    return 'fastweb-verifica-documento';
  }
}
