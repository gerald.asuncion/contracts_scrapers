import { FastwebVerificaMorositaPayload } from './payload/FastwebCheckFattibilitaPayload';
import { ScraperResponse } from '../scraper';
import FailureResponse from '../../response/FailureResponse';
import vaiAllaPaginaInserisciOrdine from './fattibilita/vaiAllaPaginaInserisciOrdine';
import extractErrorMessage from '../../utils/extractErrorMessage';
import checkMorosita from './fattibilita/morosita/checkMorosita';
import FastwebVerificaScraper from './fattibilita/FastwebVerificaScraper';
import createChildPayloadAwareLogger from '../../utils/createChildPayloadAwareLogger';
import { QueueTask } from '../../task/QueueTask';

@QueueTask()
export default class FastwebVerificaMorosita extends FastwebVerificaScraper<FastwebVerificaMorositaPayload> {
  /**
   * @override
   * @param payload Request payload
   */
  async scrapeWebsite(payload: FastwebVerificaMorositaPayload): Promise<ScraperResponse> {
    const logger = createChildPayloadAwareLogger(this.childLogger, payload);

    const { idDatiContratto } = payload;
    const page = await this.p();
    let result: ScraperResponse|undefined;

    try {
      await vaiAllaPaginaInserisciOrdine(page, logger, this.controllaSessioneDisponibile);

      result = await checkMorosita(page, payload, logger, 'Check morosità');

      if (result instanceof FailureResponse) {
        throw result.details;
      }

      return result;
    } catch (ex) {
      const errMsg = extractErrorMessage(ex);
      logger.error(`${idDatiContratto} ${errMsg}`);

      if (page) {
        try {
          await this.saveScreenshot(
            page,
            logger,
            result && result instanceof FailureResponse ? 'fastweb-verifica-morosita' : 'fastweb-verifica-morosita-errore',
            idDatiContratto
          );
        } catch (e) {
          logger.error(e);
        }
      }

      return new FailureResponse(errMsg);
    } finally {
      await this.logout(page, logger);
    }
  }

  /**
   * @override
   */
  getScraperCodice() {
    return 'fastweb-verifica-morosita';
  }
}
