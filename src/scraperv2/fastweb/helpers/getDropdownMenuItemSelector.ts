const getDropdownMenuItemSelector = (menuSelector: string): string => `${menuSelector} > li`;

export default getDropdownMenuItemSelector;
