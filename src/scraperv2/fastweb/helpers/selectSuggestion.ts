import { Page } from "puppeteer";
import waitForSelectorAndType from "../../../browser/page/waitForSelectorAndType";
import attendiSparizioneOverlay from "../fattibilita/attendiSparizioneOverlay";
import getDropdownMenuSelector from "./getDropdownMenuSelector";
// import getDropdownMenuItemSelector from "./getDropdownMenuItemSelector";
import waitForVisible from "../../../browser/page/waitForVisible";
import getElementText from "../../../browser/page/getElementText";
import waitForSelectorAndTypeEvaluated from "../../../browser/page/waitForSelectorAndTypeEvaluated";
import waitForTimeout from "../../../browser/page/waitForTimeout";

type SelectSuggestionOptions = {
  waitForSuggestionsTimeout: number;
  maxRetry: number;
};

export default async function selectSuggestion(
  page: Page,
  inputSelector: string,
  valueToInsert: string,
  valueToSelect: string,
  fieldName: string,
  options: SelectSuggestionOptions = { waitForSuggestionsTimeout: 120000, maxRetry: 5 },
  count = 0
) {
  const { waitForSuggestionsTimeout, maxRetry } = options;
  console.log(fieldName + ': ' + count + ' - ' + maxRetry);

  const checkCountAndRetry = async () => {
    count = count + 1;
    if (count > maxRetry) {
      throw new Error(`Impossibile selezionare "${valueToSelect}" nel campo "${fieldName}"; non riesco a cliccare sulla suggestion`);
    }

    await selectSuggestion(page, inputSelector, valueToInsert, valueToSelect, fieldName, options, count);
  }

  await waitForSelectorAndTypeEvaluated(page, inputSelector, '');
  await waitForTimeout(page, 5000);
  await waitForSelectorAndType(page, inputSelector, valueToInsert, undefined, { delay: 100 });

  await attendiSparizioneOverlay(page);

  const menuSelector = getDropdownMenuSelector(inputSelector);

  // const menuItemSelector = getDropdownMenuItemSelector(menuSelector);

  let suggestionApparsa = false;
  try {
    await waitForVisible(page, menuSelector, { timeout: waitForSuggestionsTimeout });
    suggestionApparsa = true;
  } catch (ex) {
    if (count >= maxRetry) {
      throw new Error(`Impossibile selezionare "${valueToSelect}" nel campo "${fieldName}"; suggestion non apparsa`);
    }
  }

  if (!suggestionApparsa) {
    await checkCountAndRetry();
  }

  const result = await page.evaluate((internalSelector: string, valoreDaSelezionare: string) => {
    function comparaTesti(primo: string, secondo: string): boolean {
      // eslint-disable-next-line newline-per-chained-call
      const left = primo.toLowerCase().normalize('NFD').replace(/[\u0300-\u036f]/g, '').replace(/'/g, ' ').trim();
      // eslint-disable-next-line newline-per-chained-call
      const right = secondo.toLowerCase().normalize('NFD').replace(/[\u0300-\u036f]/g, '').replace(/'/g, ' ').trim();

      return left === right;
    }

    const $el = $(internalSelector);
    const ul = $el.length ? $el[0] : null;
    const list = ul ? [...ul.querySelectorAll<HTMLLIElement>('li')] : [];

    if (!list.length) {
      return [];
    }

    const item = list.find((li) => comparaTesti(valoreDaSelezionare, li.textContent?.trim() || ''));

    if (!item) {
      return list.map(li => li.textContent?.trim()).filter(Boolean);
    }

    item.click();

    return null;
  }, menuSelector, valueToSelect.toLowerCase());

  if (result) {
    if (result.length) {
      throw new Error(`Impossibile selezionare "${valueToSelect}" nel campo "${fieldName}"; nessuna corrispondenza trovata nella lista: ${result.join('| ')}`);
    }

    throw new Error(`Impossibile selezionare "${valueToSelect}" nel campo "${fieldName}"; suggestion non trovata`);
  }

  const valueSelected = await getElementText(page, inputSelector, 'value');

  if (valueToSelect.toLowerCase() !== valueSelected.trim().toLowerCase()) {
    await checkCountAndRetry();
  }
}
