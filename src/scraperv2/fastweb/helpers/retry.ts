export const retry = async <T>(cb: () => Promise<T>, count = 2) => {
  try {
    await cb();
  } catch (ex) {
    if (count > 0) {
      await retry(cb, count - 1);
    } else {
      throw ex;
    }
  }
};
