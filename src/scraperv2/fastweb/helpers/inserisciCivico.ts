import { Page } from 'puppeteer';
import extractErrorMessage from '../../../utils/extractErrorMessage';
import { generateIdSelector } from '../inserimento/helpers/selectors';

async function valida(page: Page, selector: string) {
  const inputSelector = generateIdSelector(`${selector}_input`);
  const value = await page.$eval(inputSelector, (el) => (el as HTMLInputElement).value?.trim());
  if (!value) {
    throw new Error(`Campo '${selector}' non valorizzato`)
  }
}

export default async function inserisciCivico(page: Page, cb: () => Promise<void>, selector: string) {
  try {
    await cb();
  } catch (ex) {
    const exMsg = extractErrorMessage(ex);
    if (exMsg === 'Node is either not visible or not an HTMLElement' || exMsg.toLowerCase().includes('timeout')) {
      page.keyboard.press('Tab');
    } else {
      throw ex;
    }
  }
  await valida(page, selector);
}
