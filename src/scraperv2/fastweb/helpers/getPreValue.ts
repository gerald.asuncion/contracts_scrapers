export default function getPreValue(value: string): string {
  const parenthesisIdx = value.indexOf('(');
  if (parenthesisIdx > 0) {
    return value.substr(0, parenthesisIdx - 1);
  }
  const apexIdx = value.indexOf("'");
  if (apexIdx > 0) {
    return value.substr(0, apexIdx);
  }
  return value; // .substr(0, Math.max(value.length - 1, 1)); // > 10 ? 8 : 5);
}
