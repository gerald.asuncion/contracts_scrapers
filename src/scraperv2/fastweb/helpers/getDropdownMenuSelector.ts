const getDropdownMenuSelector = (parentSelector: string): string => `${parentSelector} + ul.dropdown-menu`;
export default getDropdownMenuSelector;
