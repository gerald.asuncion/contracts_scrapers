export default function getPreValueV2(value: string): string {
  const parenthesisIdx = value.indexOf('(');
  if (parenthesisIdx > 0) {
    return value.substr(0, parenthesisIdx - 1);
  }
  return value; // .substr(0, Math.max(value.length - 1, 1)); // > 10 ? 8 : 5);
}
