import Fastweb from './Fastweb';
import { ScraperResponse } from '../scraper';
import ErrorResponse from '../../response/ErrorResponse';
import FailureResponse from '../../response/FailureResponse';
import SuccessResponse from '../../response/SuccessResponse';
import FastwebCheckFattibilitaPayload from './payload/FastwebCheckFattibilitaPayload';
import FastwebLoginPayload from './payload/FastwebLoginPayload';
import GenericSuccessResponse from '../../response/GenericSuccessResponse';
import extractErrorMessage from '../../utils/extractErrorMessage';
import ScraperOptions from '../ScraperOptions';
import FastwebVerificaCopertura from './FastwebVerificaCopertura';
import FastwebVerificaMorosita from './FastwebVerificaMorosita';
import FastwebVerificaDocumento from './FastwebVerificaDocumento';
import createChildPayloadAwareLogger from '../../utils/createChildPayloadAwareLogger';
import { QueueTask } from '../../task/QueueTask';

type FastwebCheckFattibilitaOptions = ScraperOptions & {
  fastwebVerificaCopertura: FastwebVerificaCopertura;
  fastwebVerificaMorosita: FastwebVerificaMorosita;
  fastwebVerificaDocumento: FastwebVerificaDocumento;
};

@QueueTask()
export default class FastwebCheckFattibilita extends Fastweb<FastwebCheckFattibilitaPayload & FastwebLoginPayload> {
  private fastwebVerificaCopertura: FastwebVerificaCopertura;

  private fastwebVerificaMorosita: FastwebVerificaMorosita;

  private fastwebVerificaDocumento: FastwebVerificaDocumento;

  constructor(options: FastwebCheckFattibilitaOptions) {
    super(options);

    this.fastwebVerificaCopertura = options.fastwebVerificaCopertura;
    this.fastwebVerificaMorosita = options.fastwebVerificaMorosita;
    this.fastwebVerificaDocumento = options.fastwebVerificaDocumento;
  }

  /**
   * @override
   */
  login(): Promise<void> {
    return Promise.resolve();
  }

  /**
   * @override
   * @param payload Request payload
   */
  async scrapeWebsite(payload: FastwebCheckFattibilitaPayload): Promise<ScraperResponse> {
    const logger = createChildPayloadAwareLogger(this.childLogger, payload);

    try {
      const result = await Promise.all([
        this.fastwebVerificaCopertura.scrape(payload as any),
        this.fastwebVerificaMorosita.scrape(payload as any),
        this.fastwebVerificaDocumento.scrape(payload as any)
      ]).then(([coperturaResult, morositaResult, documentoResult]) => {
        logger.info(`Check morosità result: ${JSON.stringify(morositaResult)}`);

        logger.info(`Check copertura result: ${JSON.stringify(coperturaResult)}`);

        logger.info(`Check documento result: ${JSON.stringify(documentoResult)}`);

        return this.componiResponse(morositaResult, coperturaResult, documentoResult);
      });

      return result;
    } catch (ex) {
      const errMsg = extractErrorMessage(ex);
      logger.error(errMsg);
      return new ErrorResponse(errMsg);
    }
  }

  private componiResponse(
    morositaResult: ScraperResponse,
    coperturaResult: ScraperResponse,
    documentoResult: ScraperResponse
  ): ScraperResponse {
    let response: ScraperResponse;
    if (
      morositaResult instanceof GenericSuccessResponse
      && coperturaResult instanceof GenericSuccessResponse
      && documentoResult instanceof GenericSuccessResponse
    ) {
      response = new SuccessResponse(`Check copertura: ${coperturaResult.details}`);
    } else {
      const coperturaResponse = this.creaFailureResponseDetail(coperturaResult, 'Check copertura');

      const morositaResponse = this.creaFailureResponseDetail(morositaResult, 'Check morosità');

      const documentoResponse = this.creaFailureResponseDetail(documentoResult, 'Check documento');

      response = new FailureResponse([coperturaResponse, morositaResponse, documentoResponse]);
    }

    return response;
  }

  private creaFailureResponseDetail(result: ScraperResponse, msgPrefix: string): ScraperResponse {
    let response: ScraperResponse;
    if (result instanceof GenericSuccessResponse) {
      response = new SuccessResponse(`${msgPrefix} OK: ${result.details}`);
    } else {
      response = new FailureResponse(`${msgPrefix}: ${result.details}`);
    }
    return response;
  }
}
