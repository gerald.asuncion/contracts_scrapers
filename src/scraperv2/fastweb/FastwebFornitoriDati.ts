import { Page } from 'puppeteer';
import { ScraperResponse } from '../scraper';
import FornitoriInternetPayload from './fornitori/FornitoriInternetPayload';
import FastwebFornitoriEstrattore from './fornitori/FastwebFornitoriEstrattore';
import ScraperOptions from '../ScraperOptions';
import waitForSelectorAndClick from '../../browser/page/waitForSelectorAndClick';
import { QueueTask } from '../../task/QueueTask';

@QueueTask()
export default class FastwebFornitoriDati extends FastwebFornitoriEstrattore {
  constructor(options: ScraperOptions) {
    super(options);
    this.firstComboboxSelector = 'offerta';
    this.secondComboboxSelector = 'offertaFisso';
    this.listContainerSelector = 'operatoreProvenienzaDati_panel';
    this.checkboxSelector = 'div[id$=":npDati"] label[for$=":npDati:0"]';
  }

  /**
   * @override
   */
  scrapeWebsite(): Promise<ScraperResponse> {
    return super.scrapeWebsite(<never>FornitoriInternetPayload);
  }

  /**
   * @override
   */
  async clickOnCheckbox(page: Page): Promise<void> {
    this.childLogger.info('apro il box contenente la suggestion');
    await waitForSelectorAndClick(page, this.checkboxSelector);
  }
}
