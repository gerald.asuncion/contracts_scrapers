import { Page } from 'puppeteer';
import waitForTimeout from '../../../browser/page/waitForTimeout';
import waitForXPathAndType, { InputMode } from '../../../browser/page/waitForXPathAndType';
import waitForXPathVisible from '../../../browser/page/waitForXPathVisible';
import delay from '../../../utils/delay';

const inputSelector = '//label[contains(., "ID")]/parent::div/parent::div/following-sibling::div//input[@type="text"]';

export async function inserisciCodiceContrattoDaControllare(page: Page, codiceContratto: string): Promise<string> {
  const [el] = await page.$x(inputSelector);
  await el.evaluate((iel) => (iel as HTMLInputElement).value = '');
  await waitForTimeout(page, 100);
  await waitForXPathAndType(page, inputSelector, codiceContratto, InputMode.default, undefined, { delay: 200 });
  await waitForTimeout(page, 100);
  const testo = await page.evaluate((iel) => (iel as HTMLInputElement).value, (await page.$x(inputSelector))[0]);
  return testo;
}

export async function inserisciCodiceContrattoDaControllareV2(page: Page, codiceContratto: string, codice: string): Promise<string> {
  const elements = await page.$x(inputSelector);
  const element = elements.length > 0 ? elements[0] : null;
  if (codice.length === 0) {
    await element?.evaluate((iel) => (iel as HTMLInputElement).value = '');
  }
  await element?.type(codiceContratto, { delay: 100 });

  const checkElements = await page.$x(inputSelector);
  const check = checkElements.length > 0 ? checkElements[0] : null;
  const testo = check ? await page.evaluate((element) => (element as HTMLInputElement)?.value, check) : "";

  return testo;
}

export async function inserisciCodiceContrattoConRetry(page: Page, codiceContratto: string, ultimoCodiceContrattoInserito?: string, maxRetry = 10, codice: string = ''): Promise<string> {
  await delay(200)
  const list = codiceContratto.split("");
  list.splice(0, codice.length);

  if (maxRetry <= 0) {
    return ultimoCodiceContrattoInserito || '';
  }

  await waitForXPathVisible(page, '//h1[contains(., "Consultazione Firme Elettroniche")]');

  let codiceInserito = await inserisciCodiceContrattoDaControllareV2(page, list[0], codice);

  if (codiceInserito === codiceContratto) {
    return codiceInserito;
  }
  if (codiceInserito !== codiceContratto) {
    const removed = codiceContratto.split("").splice(0, codiceInserito.length);
    codiceInserito = await inserisciCodiceContrattoConRetry(page, codiceContratto, codiceInserito, --maxRetry, removed.join(""));
  }
  return codiceInserito;
}
