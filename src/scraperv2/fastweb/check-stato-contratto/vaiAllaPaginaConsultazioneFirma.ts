import { Page } from 'puppeteer';
import waitForSelectorAndClick from '../../../browser/page/waitForSelectorAndClick';
import waitForNavigation from '../../../browser/page/waitForNavigation';
import waitForSelectorAndType from '../../../browser/page/waitForSelectorAndType';
import waitForVisible from '../../../browser/page/waitForVisible';
import { Logger } from '../../../logger';
import waitForNewPageLoaded from '../../../browser/page/waitForNewPageLoaded';
import waitForTimeout from '../../../browser/page/waitForTimeout';

export default async function vaiAllaPaginaConsultazioneFirma(page: Page, logger: Logger, controllaSessioneDisponibile: (page: Page) => Promise<void>): Promise<Page> {
  logger.info('clicco su "fastweb app"');
  await page.goto('https://fastweb.force.com/partnersales/apex/GlobalSearch?sfdc.tabName=01rw0000000kLSX', { waitUntil: 'networkidle2' });

  logger.info('controllo se la sessione è già in uso');
  // await waitForTimeout(page, 6000);
  await controllaSessioneDisponibile(page);

  logger.info('attendo apertura completa altra pagina');
  await page.waitForSelector('#tabBar');

  logger.info('clicco su "External Sales Apps"');
  await Promise.all([
    waitForSelectorAndClick(page, '#\\30 1r2X000000R0zs_Tab > a'),
    waitForNavigation(page)
  ]);

  logger.info('clicco su DR.0797.0001HQ');
  await waitForSelectorAndType(page, '.fastweb > div:nth-child(1) .modal-body > input', 'DR.0797.0001HQ');
  await waitForSelectorAndClick(page, '.fastweb table > tbody > tr');

  await waitForVisible(page, '#links p');

  logger.info('clicco su TMT');
  await waitForSelectorAndClick(page, '#links .list-group > span:nth-child(1) > p');

  logger.info('recupero il riferimento alla nuova pagina aperta');
  const otherPage = await waitForNewPageLoaded(page);

  if (!otherPage) {
    throw new Error('riferimento alla pagina TMT non trovato');
  }

  logger.info('apro la pagina di consultazione firma');
  await otherPage.goto('https://tmt.fastweb.it/consultazioneFirmaElettronica.xhtml', { waitUntil: 'networkidle2' });

  return otherPage;
}
