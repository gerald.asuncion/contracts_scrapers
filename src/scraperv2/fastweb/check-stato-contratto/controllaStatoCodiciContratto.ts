import { Page } from 'puppeteer';
import { Logger } from '../../../logger';
import waitForTimeout from '../../../browser/page/waitForTimeout';
import waitForVisible from '../../../browser/page/waitForVisible';
import { generateIdSelector } from '../inserimento/helpers/selectors';
import { STATO_CONTRATTO } from '../../../contratti/StatoContratto';
import waitForHidden from '../../../browser/page/waitForHidden';
import waitForXPathAndClick from '../../../browser/page/waitForXPathAndClick';
import extractErrorMessage from '../../../utils/extractErrorMessage';
import { inserisciCodiceContrattoConRetry } from './inserisciCodiceContrattoDaControllare';

async function controllaStatoCodiceContratto(page: Page, codiceContratto: string, logger: Logger): Promise<string> {
  const codiceInserito = await inserisciCodiceContrattoConRetry(page, codiceContratto);
  if (codiceInserito !== codiceContratto) {
    // non sono riuscito ad inserire correttamente il codice da controllare
    // quindi restituisco direttamente lo stato "DA FIRMARE"
    logger.warn(`non sono riuscito ad inserire correttamente il codice contratto ${codiceContratto} da controllare; quindi restituisco direttamente lo stato '${STATO_CONTRATTO.DA_FIRMARE}'`)
    return STATO_CONTRATTO.DA_FIRMARE;
  }

  await waitForTimeout(page, 100);
  await waitForXPathAndClick(page, '//button[contains(., "Visualizza")]');

  try {
    await waitForHidden(page, 'div[id$="_modal"].ui-widget-overlay', { timeout: 3000 });
  } catch (ex) {
    // sparito prima che potessi fare il wait
  }

  const tableRowSelector = `${generateIdSelector('pdaTable_data')} > tr`;
  await waitForVisible(page, tableRowSelector);

  const noData = await page.evaluate(() => !!$('td:contains(Nessuna firma elettronica trovata.)').length);

  if (noData) {
    return STATO_CONTRATTO.DA_FIRMARE;
  }

  const dataFirma = await page.$eval(`${tableRowSelector} > td:nth-child(5)`, ({ textContent }) => textContent?.trim());

  let stato: string = STATO_CONTRATTO.DA_FIRMARE;

  if (dataFirma && dataFirma !== '---') {
    stato = STATO_CONTRATTO.FIRMATO;
  }

  return stato;
}

export default async function* controllaStatoCodiciContratto(
  page: Page,
  codiciContratto: Array<string>,
  logger: Logger
): AsyncGenerator<{ codiceContratto: string; stato: string; }> {
  const codiceContratto = codiciContratto.pop();

  if (codiceContratto) {
    let stato;
    try {
      stato = await controllaStatoCodiceContratto(page, codiceContratto, logger);
    } catch (ex) {
      logger.warn(extractErrorMessage(ex));
      // è andato storto qualcosa
      // impongo lo stato "DA FIRMARE"
      // così posso andare avanti
      stato = STATO_CONTRATTO.DA_FIRMARE;
    }
    yield { codiceContratto, stato };
  }

  if (codiciContratto.length) {
    yield* controllaStatoCodiciContratto(page, codiciContratto, logger);
  }
}
