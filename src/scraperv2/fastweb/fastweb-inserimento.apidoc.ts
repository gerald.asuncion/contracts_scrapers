/**
 * @openapi
 *
 * /scraper/v2/fastweb/inserimento:
 *  post:
 *    tags:
 *      - v2
 *    description:
 *      Inserisce il contratto, coi dati passati, sul portale di Fastweb.
 *      Questa è la rotta diretta allo scraper, quindi non usa il meccanismo del webhook.
 *    consume:
 *      - application/json
 *    produces:
 *      - application/json
 *    responses:
 *      200:
 *        description: SuccessResponse
 *    parameters:
 *      - in: body
 *        description: i dati da inserire.
 *        schema:
 *          type: object
 *          required:
 *            - username
 *            - password
 *            - tipoOfferta
 *            - partnership
 *            - nome
 *            - cognome
 *            - sesso
 *            - dataNascita
 *            - comuneNascitaEstero
 *            - comuneNascita
 *            - provinciaNascita
 *            - codiceFiscale
 *            - cellulare
 *            - email
 *            - tipoInvio
 *            - documentoTipo
 *            - documentoNumero
 *            - documentoEnteRilascio
 *            - documentoDataRilascio
 *            - residenzaComune
 *            - residenzaIndirizzo
 *            - residenzaCivico
 *            - residenzaCap
 *            - residenzaProvincia
 *            - attivazioneComune
 *            - attivazioneProvincia
 *            - attivazioneIndirizzo
 *            - attivazioneCivico
 *            - attivazioneCap
 *            - sceltaModem
 *            - isProdottoPortabilita
 *            - tipoContratto
 *            - recapitoComune
 *            - recapitoProvincia
 *            - recapitoIndirizzo
 *            - recapitoCivico
 *            - recapitoCap
 *            - consenso1
 *            - consenso2
 *            - consenso3
 *            - tipoPagamento
 *            - iban
 *            - intestazioneContoCorrente
 *            - clientePossiedeUnaNumerazioneDaPortare
 *            - clientePossiedeUnaLineaDati
 *          properties:
 *            codiceComsy:
 *              type: string
 *              nullable: true
 *            username:
 *              type: string
 *            password:
 *              type: string
 *            tipoOfferta:
 *              type: string
 *              enum: [Fisso+mobile, Mobile, Upsell mobile su cb Fisso, Fisso, Upsell fisso su CB mobile]
 *            offertaFisso:
 *              type: string
 *              nullable: true
 *              enum: [Fastweb Nexxt Casa, Fastweb Nexxt Casa Light, Fastweb Casa Light, Fastweb NeXXt Casa Plus]
 *            offertaMobile:
 *              type: string
 *              nullable: true
 *              enum: [Fastweb NeXXt Mobile, Fastweb NeXXt Mobile Maxi, Fastweb Mobile Light]
 *            opzioneMobile:
 *              type: string
 *              nullable: true
 *              enum: [nessuna opzione]
 *            offertaMobile2:
 *              type: string
 *              nullable: true
 *              enum: [nessuna offerta mobile 2]
 *            sconto1:
 *              type: string
 *              nullable: true
 *              enum: [nessuno sconto]
 *            sconto2:
 *              type: string
 *              nullable: true
 *              enum: [nessuno sconto]
 *            partnership:
 *              type: string
 *            opzioneFisso1:
 *              type: string
 *              nullable: true
 *              enum: [nessuna opzione, Opzione Now (primi 3 mesi gratis), Servizio potenziamento Wi Fi, Servizio di estensione del segnale Wi Fi]
 *            opzioneFisso2:
 *              type: string
 *              nullable: true
 *              enum: [nessuna opzione, Opzione Fastweb&Dazn (primi 3 mesi gratis), Servizio potenziamento Wi Fi, Servizio di estensione del segnale Wi Fi]
 *            opzioneFisso3:
 *              type: [nessuna opzione, Assicurazione Assistenza Casa Quixa]
 *              nullable: true
 *            accountCliente:
 *              type: string
 *              nullable: true
 *            nome:
 *              type: string
 *            cognome:
 *              type: string
 *            sesso:
 *              type: string
 *              enum: [M, F]
 *            dataNascita:
 *              type: string
 *            comuneNascitaEstero:
 *              type: boolean
 *            comuneNascita:
 *              type: string
 *            provinciaNascita:
 *              type: string
 *            codiceFiscale:
 *              type: string
 *            cellulare:
 *              type: string
 *            email:
 *              type: string
 *            tipoInvio:
 *              type: string
 *              enum: [EMAIL, SMS]
 *            documentoTipo:
 *              type: string
 *            documentoNumero:
 *              type: string
 *            documentoEnteRilascio:
 *              type: string
 *            documentoDataRilascio:
 *              type: string
 *            documentoRilasciatoDa:
 *              type: string
 *              nullable: true
 *            documentoProvinciaRilascio:
 *              type: string
 *              nullable: true
 *            residenzaComune:
 *              type: string
 *            residenzaIndirizzo:
 *              type: string
 *            residenzaCivico:
 *              type: string
 *            residenzaCap:
 *              type: string
 *            residenzaProvincia:
 *              type: string
 *            attivazioneComune:
 *              type: string
 *            attivazioneProvincia:
 *              type: string
 *            attivazioneIndirizzo:
 *              type: string
 *            attivazioneCivico:
 *              type: string
 *            attivazioneCap:
 *              type: string
 *            attivazioneScala:
 *              type: string
 *              nullable: true
 *            attivazionePiano:
 *              type: string
 *              nullable: true
 *            attivazioneInterno:
 *              type: string
 *              nullable: true
 *            prefissoNumeroTelefono:
 *              type: string
 *              nullable: true
 *            codiceMigrazione:
 *              type: string
 *              nullable: true
 *            attualeFornitoreInternet:
 *              type: string
 *              nullable: true
 *            sceltaModem:
 *              type: string
 *              enum: [FASTWEB, PROPRIETA]
 *            isProdottoPortabilita:
 *              type: boolean
 *            iccid:
 *              type: string
 *              nullable: true
 *            cellulareAttuale:
 *              type: string
 *              nullable: true
 *            attualeFornitoreMobile:
 *              type: string
 *              nullable: true
 *            tipoContratto:
 *              type: string
 *              enum: [ricaricabile]
 *            recapitoComune:
 *              type: string
 *            recapitoProvincia:
 *              type: string
 *            recapitoIndirizzo:
 *              type: string
 *            recapitoCivico:
 *              type: string
 *            recapitoCap:
 *              type: string
 *            recapitoPresso:
 *              type: string
 *              nullable: true
 *            consenso1:
 *              type: boolean
 *            consenso2:
 *              type: boolean
 *            consenso3:
 *              type: boolean
 *            tipoPagamento:
 *              type: string
 *              nullable: true
 *              enum: [Addebito su Conto Corrente]
 *            iban:
 *              type: string
 *              nullable: true
 *            intestazioneContoCorrente:
 *              type: string
 *              nullable: true
 *            clientePossiedeUnaNumerazioneDaPortare:
 *              type: string
 *              enum: [si, no]
 *            clientePossiedeUnaLineaDati:
 *              type: string
 *              enum: [si, no]
 *            codicePortabilitaDati:
 *              type: string
 *              nullable: true
 *            operatoreProvenienzaDati:
 *              type: string
 *              nullable: true
 *
 * /scraper/queue/fastweb:
 *  post:
 *    tags:
 *      - v2
 *      - queue
 *    description:
 *      Inserisce in queue il task per inserire il contratto sul portale Fastweb.
 *      Il payload è lo stesso della rotta diretta `/scraper/v2/fastweb/inserimento`,
 *      in più va aggiunto l'id dati contratto ed il webhook che lo scraper poi andrà a richiamare per notificare l'esito.
 *    consume:
 *      - application/json
 *    produces:
 *      - application/json
 *    responses:
 *      200:
 *        description: SuccessResponse
 */
