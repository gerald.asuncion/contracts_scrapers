import { ElementHandle, Page } from 'puppeteer';
import getElementText from '../../browser/page/getElementText';
import isEnabled from '../../browser/page/isEnabled';
import isVisible from '../../browser/page/isVisible';
import lookForVisibleWithTextElements from '../../browser/page/lookForVisibleWithTextElements';
import waitCheckCredentials from '../../browser/page/waitCheckCredentials';
import waitForNavigation from '../../browser/page/waitForNavigation';
import waitForNewPageLoaded from '../../browser/page/waitForNewPageLoaded';
import waitForSelectorAndClick from '../../browser/page/waitForSelectorAndClick';
import waitForSelectorAndType from '../../browser/page/waitForSelectorAndType';
import waitForTimeout from '../../browser/page/waitForTimeout';
import waitForVisible from '../../browser/page/waitForVisible';
import waitForXPathAndClick from '../../browser/page/waitForXPathAndClick';
import WrongCredentialsError from '../../errors/WrongCredentialsError';
import { Logger } from '../../logger';
import ErrorResponse from '../../response/ErrorResponse';
import FailureResponse from '../../response/FailureResponse';
import SuccessResponse from '../../response/SuccessResponse';
import { QueueTask } from '../../task/QueueTask';
import createChildPayloadAwareLogger from '../../utils/createChildPayloadAwareLogger';
import delay from '../../utils/delay';
import extractErrorMessage from '../../utils/extractErrorMessage';
import stripNonNumeric from '../../utils/stripNonNumeric';
import tryOrThrow, { TryOrThrowError } from '../../utils/tryOrThrow';
import { ScraperResponse, WebScraper } from '../scraper';
import { SCRAPER_TIPOLOGIA } from '../ScraperOptions';
import inserisciCivico from './helpers/inserisciCivico';
import { retry } from './helpers/retry';
import clickButton from './inserimento/controls/clickButton';
import isAutoCompleteOptionVisible from './inserimento/controls/isAutoCompleteOptionVisible';
import isButtonDisabled from './inserimento/controls/isButtonDisabled';
import isCheckboxChecked from './inserimento/controls/isCheckboxChecked';
import isCheckboxVisible from './inserimento/controls/isCheckboxVisible';
import isComboOptionVisible from './inserimento/controls/isComboOptionVisible';
import isRadioVisible from './inserimento/controls/isRadioVisible';
import selectAutoCompleteOption from './inserimento/controls/selectAutoCompleteOption';
import selectCheckbox from './inserimento/controls/selectCheckbox';
import selectComboOption from './inserimento/controls/selectComboOption';
import selectComboOptionEvaluated from './inserimento/controls/selectComboOptionEvaluated';
import selectDate from './inserimento/controls/selectDate';
import selectRadio from './inserimento/controls/selectRadio';
import { generateIdSelector, generateInputNameSelector, generateLabelForSelector } from './inserimento/helpers/selectors';
import readErrorMessages from './inserimento/ui/readErrorMessages';
import waitLoadingMessage from './inserimento/ui/waitLoadingMessage';
import FastwebInserimentoPayload from './payload/FastwebInserimentoPayload';
import FastwebLoginPayload from './payload/FastwebLoginPayload';

@QueueTask()
export default class Fastweb<T extends FastwebLoginPayload = FastwebLoginPayload & FastwebInserimentoPayload> extends WebScraper<T> {
  static readonly LOGIN_URL = 'https://logon.fastweb.it/fwsso/landing.jsp';

  protected nazionalitaDefault = 'ITALIA';

  private config = {
    fastwebAppName: 'Fastweb App',
    externalSalesApp: 'External Sales Apps',
    firmaApp: 'TMT',
    codiceComsy: 'DR.0797.0001HQ'
  };

  /**
   * @override
   */
  getLoginTimeout(): number {
    return 60000;
  }

  async login({ username, password, ...rest }: T): Promise<void> {
    const logger = createChildPayloadAwareLogger(this.childLogger, rest);
    const page = await this.p();

    await page.goto(Fastweb.LOGIN_URL, { waitUntil: 'networkidle2' });

    try {
      const sel = await page.waitForSelector('body > div.dashboard > div.applications', { timeout: 3000 });
      if (sel) {
        logger.info('login > utente già loggato');
        return;
      }
    } catch (ex) {
      // non è già loggato, posso proseguire con la login
    }

    let credenziali
    try {
      credenziali = await this.getCredenziali();
    } catch (e) {
      credenziali = {username, password};
    }

    await waitForSelectorAndType(page, '#username', credenziali.username);
    logger.info('login > inserito username');

    await waitForSelectorAndType(page, '#password', credenziali.password);
    logger.info('login > inserita password');

    await Promise.all([
      waitForSelectorAndClick(page, '#frmLogin > p.submit > input[type=submit]'),
      waitForNavigation(page)
    ]);

    await waitCheckCredentials(page, 'span.errorMessage', Fastweb.LOGIN_URL, username, logger, { timeout: 3000 });
    // await Promise.race([
    //   waitCheckCredentials(page, 'span.errorMessage', Fastweb.LOGIN_URL, username, logger, { timeout: 3000 }),
    //   (async () => {
    //     const content = await page.content();

    //     if (content.toLowerCase().includes('service unavailable')) {
    //       throw new WrongCredentialsError(Fastweb.LOGIN_URL, username);
    //     }
    //   })()
    // ]);

    const content = await page.content();
    if (content.toLowerCase().includes('service unavailable')) {
      throw new WrongCredentialsError(Fastweb.LOGIN_URL, username);
    }

    logger.info('login > terminata');
  }

  async scrapeWebsite(args: T & FastwebInserimentoPayload): Promise<ScraperResponse> {
    const logger = createChildPayloadAwareLogger(this.childLogger, args);

    let page = await this.p();
    try {
      page = await this.navigaAllaForm(page, args, logger);

      return await this.executeScraper(page, args, logger);
    } catch (err) {
      const errMessage = extractErrorMessage(err);

      if (page) {
        await this.saveScreenshot(page, logger, 'fastweb-errore', args.idDatiContratto);
        await this.logout(page, logger);
      }

      logger.error(errMessage);
      return new ErrorResponse(errMessage);
    }
  }

  private async navigaAllaForm(page: Page, { codiceComsy = this.config.codiceComsy, username }: FastwebInserimentoPayload, logger: Logger): Promise<Page> {
    logger.info('Navigazione verso la pagina `ExternalSalesApps`');
    await tryOrThrow(
      () => page.goto('https://fastweb.force.com/partnersales/apex/ExternalSalesApps?sfdc.tabName=01r2X000000R0zs', { waitUntil: 'networkidle2' }),
      'non sono riuscito a raggiungere la pagina `External Sales Apps`'
    );

    await waitForTimeout(page, 3000);

    logger.info('controllo se è apparsa la pagina con qualche errore invece che quella coi codici comsy');
    const content = await page.content();
    if (content.toLowerCase().includes('single sign-on error')) {
      throw new Error(`Il portale non riesce ad autenticare l\'utente ${username}`);
    }

    logger.info('controllo se la sessione fastweb è utilizzabile');
    await this.controllaSessioneDisponibile(page);

    logger.info('attendo caricamento tabella dei codici comsy');
    await waitForVisible(page, '.fastweb div[heading="Seleziona il Codice Comsy"]');
    await tryOrThrow(() => waitForXPathAndClick(
      page,
      `//table//td[contains(., '${codiceComsy}')][contains(concat(" ", normalize-space(@class), " "), " ng-binding ")]`
    ), `non sono riuscito a cliccare su '${codiceComsy}'`);

    const [nnPage] = await Promise.all([
      waitForNewPageLoaded(page),
      waitForXPathAndClick(
        page,
        `//div[@id='links']//div[starts-with(@id,'accordiongroup')]//span/p[contains(., '${this.config.firmaApp}')]`
      )
    ]);

    logger.info('- navigazione a: TMT');

    // l'elemento da cliccare potrebbe essere invisibile (menu mobile/desktop)
    await nnPage.evaluate(() => {
      const elNavItem = document.evaluate(
        "//ul[starts-with(@id,'formTemplate')]/li[contains(., 'Firma Elettronica')]/ul/li/a/span[contains(., 'Compila')]",
        document,
        null,
        XPathResult.ANY_TYPE,
        null
      ).iterateNext();
      (elNavItem as HTMLDivElement)?.click();
    });

    await waitForNavigation(nnPage);

    return nnPage;
  }

  protected async executeScraper(page: Page, args: T & FastwebInserimentoPayload, logger: Logger): Promise<ScraperResponse> {
    /* DEBUG ONLY
    page.addStyleTag({
      content: `
      .card.card-w-title {
          padding-bottom: 320px;
      }`
    });
    */

    logger.info('Inserimento nella form contratti.');

    await this.compilaBoxOfferta(page, args);
    const respAnagrafica = await this.compilaBoxAnagrafica(page, args);
    if (respAnagrafica) {
      await this.saveScreenshot(page, logger, 'fastweb-errore', args.idDatiContratto);
      await this.logout(page, logger);
      return respAnagrafica;
    }
    await this.compilaBoxDocumento(page, args);
    await this.compilaBoxDatiResidenza(page, args);
    await this.compilaBoxIndirizzoAttivazioneLinea(page, args);
    const respAttivazioneLineaNp = await this.compilaBoxIndirizzoAttivazioneLineaNp(page, args);
    if (respAttivazioneLineaNp) {
      await this.saveScreenshot(page, logger, 'fastweb-errore', args.idDatiContratto);
      await this.logout(page, logger);
      return respAttivazioneLineaNp;
    }
    await this.compilaBoxSceltaModem(page, args);
    await this.compilaBoxDettagliMobile(page, args);
    await this.compilaBoxIndirizzoSpedizione(page, args);
    await this.compilaBoxConsensoTrattamentoDati(page, args);
    await this.compilaBoxDettagliPagamento(page, args);

    const result = await this.submitForm(page, args);
    logger.info(`Risposta generata: ${result.code} ${result.details} ${result.message}`);

    await this.logout(page, logger);

    return result;
  }

  private async compilaBoxOfferta(page: Page, args: FastwebInserimentoPayload): Promise<void> {
    const logger = createChildPayloadAwareLogger(this.childLogger, args);
    logger.info('Compilo il box Offerta.');

    const {
      tipoOfferta,
      offertaFisso,
      offertaMobile,
      opzioneMobile = 'nessuna opzione',
      offertaMobile2 = 'nessuna offerta mobile 2',
      sconto1,
      sconto2 = 'nessuno sconto',
      partnership,
      opzioneFisso1 = 'nessuna opzione',
      opzioneFisso2,
      opzioneFisso3 = 'nessuna opzione',
      opzioneFisso8 = 'nessuna opzione'
    } = args;

    await delay(500);
    await selectComboOption(page, 'offerta', tipoOfferta);

    if (offertaFisso) {
      await selectComboOption(page, 'offertaFisso', offertaFisso);
    }

    if (offertaMobile) {
      await selectComboOption(page, 'offertaMobile1', offertaMobile);
    }
    await selectComboOption(page, 'opzioneMobile1', opzioneMobile);
    await selectComboOption(page, 'offertaMobile2', offertaMobile2);

    await selectComboOption(page, 'partnership', partnership);

    try {
      await waitForSelectorAndClick(page, 'div[id$="selezioneENI"] button[type="submit"]', {timeout: 2000})
    } catch (ignore) {}

    // 13550 reintegrato
    // 13061 scommentare quando Fastweb lo gestirà di nuovo
    if (await isComboOptionVisible(page, 'coupon1')) {
      await selectComboOption(page, 'coupon1', sconto1 || 'Nessuno Sconto');
    }

    // 13550 scommentare quando Fastweb lo gestirà di nuovo
    // if (await isComboOptionVisible(page, 'coupon2')) {
    //   // await selectComboOption(page, 'coupon2', sconto2);
    //   const sconto2Mappato = sconto1 ? (sconto1 as string === 'Sconto Coupon -5€ x 12 mesi' ? 'Sconto Coupon 3€ x 12 mesi' : 'nessuno sconto') : 'nessuno sconto';
    //   await selectComboOption(page, 'coupon2', sconto2Mappato);
    // }

    if (opzioneFisso1) {
      await selectComboOption(page, 'opzioneFisso1', opzioneFisso1);
    }

    let opzione2: string | undefined = opzioneFisso2;
    if (offertaFisso?.toLowerCase() === "fastweb nexxt casa plus" && opzioneFisso2?.toLowerCase() === "upplus") {
      opzione2 = "Nessuna Opzione";
    }
    if (opzione2) {
      if (opzione2 === "UpPlus") {
        opzione2 = "FastwebUp Plus";
      }
      await selectComboOption(page, 'opzioneFisso2', opzione2, opzioneFisso2 === "UpPlus");
    }

    if (opzioneFisso3) {
      await selectComboOption(page, 'opzioneFisso3', opzioneFisso3);
    }

    if (opzioneFisso8) {
      await selectComboOption(page, 'opzioneFisso8', opzioneFisso8);
    }
  }

  private async compilaBoxAnagrafica(page: Page, args: FastwebInserimentoPayload): Promise<FailureResponse|null> {
    const logger = createChildPayloadAwareLogger(this.childLogger, args);
    logger.info('Compilo il box Anagrafica.');

    // Box - Anagrafica
    const {
      accountCliente,
      nome,
      cognome,
      sesso,
    } = args;

    if (accountCliente && await isVisible(page, generateInputNameSelector('cliente'))) {
      await waitForSelectorAndType(page, generateInputNameSelector('cliente'), accountCliente);
    }

    await waitForSelectorAndType(page, generateInputNameSelector('nome'), nome);
    await waitForSelectorAndType(page, generateInputNameSelector('cognome'), cognome);
    await selectRadio(page, 'sesso', sesso.toUpperCase());

    const {
      dataNascita,
      comuneNascita,
      comuneNascitaEstero,
      provinciaNascita,
    } = args;

    await selectDate(page, 'dataNascita', stripNonNumeric(dataNascita)); // dd/mm/yyyy

    await tryOrThrow(
      async () => {
        if (comuneNascitaEstero || provinciaNascita === 'EE') {
          await selectComboOption(page, 'nazioneNascita', comuneNascita);
        } else {
          await selectComboOption(page, 'nazioneNascita', this.nazionalitaDefault);
          await selectAutoCompleteOption(
            page,
            'cittaNascita',
            comuneNascita,
            // fixComuneFastweb(mergeNomiCitta(...fixNomiCittaFastweb(fixComuneFastweb(comuneNascita), provinciaNascita))),
            true
          );
        }
      },
      'Verificare il campo Città / Stato di nascita:'
    );

    const {
      codiceFiscale,
      cellulare,
      // numeroTelefono,
      email,
      tipoInvio,
    } = args;

    await waitForSelectorAndType(page, generateInputNameSelector('codFiscale'), codiceFiscale);
    await waitForSelectorAndType(page, generateInputNameSelector('numMobile'), cellulare);

    const mobileWarningMessage = await waitLoadingMessage(page);
    if (mobileWarningMessage) {
      return new FailureResponse(mobileWarningMessage);
    }

    await waitForSelectorAndType(page, generateInputNameSelector('email'), email);

    const emailWarningMessage = await waitLoadingMessage(page);
    if (emailWarningMessage) {
      return new FailureResponse(emailWarningMessage);
    }

    await selectRadio(page, 'tipoInvioRES', tipoInvio.toUpperCase());

    return null;
  }

  private async compilaBoxDocumento(page: Page, args: FastwebInserimentoPayload): Promise<void> {
    const logger = createChildPayloadAwareLogger(this.childLogger, args);
    logger.info('Compilo il box Documento.');

    // Box - Documento
    const {
      documentoTipo,
      documentoNumero,
      documentoEnteRilascio,
      documentoDataRilascio,
      documentoRilasciatoDa,
      documentoProvinciaRilascio
    } = args;

    await selectComboOption(page, 'tipoDocumento', documentoTipo);
    await waitForSelectorAndType(page, generateIdSelector('numero'), documentoNumero);
    await selectComboOption(page, 'emittente', documentoEnteRilascio);
    await selectDate(page, 'dataRilascio', stripNonNumeric(documentoDataRilascio));
    await selectComboOption(page, 'nazionalitaDocumento', this.nazionalitaDefault);

    if (documentoRilasciatoDa) {
      const selector = generateIdSelector('localita');

      if (await isVisible(page, selector) && await isEnabled(page, selector)) {
        await waitForSelectorAndType(page, selector, documentoRilasciatoDa);
      }
    }
    if (documentoProvinciaRilascio) {
      const selector = generateIdSelector('provinciaLocalita');

      if (await isVisible(page, selector) && await isEnabled(page, selector)) {
        await waitForSelectorAndType(page, selector, documentoProvinciaRilascio);
      }
    }
  }

  private async compilaBoxDatiResidenza(page: Page, args: FastwebInserimentoPayload): Promise<void> {
    const logger = createChildPayloadAwareLogger(this.childLogger, args);
    logger.info('Compilo il box Dati Residenza.');

    // Box - Dati Residenza
    const {
      residenzaComune,
      residenzaIndirizzo,
      residenzaCivico,
      residenzaCap,
      residenzaProvincia
    } = args;

    await tryOrThrow(
      () => retry(() => selectAutoCompleteOption(
        page,
        'cittaResidenza',
        residenzaComune,
        // fixComuneResidenza(mergeNomiCitta(...fixNomiCittaFastweb(fixComuneFastweb(residenzaComune), residenzaProvincia))),
        true,
        null,
        true
      )),
      'Verificare il campo Città di residenza'
    );

    await page.keyboard.press('Tab');
    await waitLoadingMessage(page);

    await tryOrThrow(
      () => retry(
        () => selectAutoCompleteOption(page, 'indirizzoResidenza', residenzaIndirizzo, true, { delay: 30 }, true)
      )
    , 'Verificare il campo Indirizzo di residenza');

    await page.keyboard.press('Tab');
    await waitLoadingMessage(page);

    await tryOrThrow(
      () => retry(
        () => inserisciCivico(
          page,
          () => selectAutoCompleteOption(page, 'civicoResidenza', residenzaCivico, false, null, true),
          'civicoResidenza'
        )
      ),
      'Verificare il campo Numero civico di residenza'
    );

    await tryOrThrow(
      async () => {
        const elResidenzaCap = await page.waitForSelector(generateIdSelector('capResidenza'));
        const elResidenzaCapValue = await page.evaluate((element: HTMLInputElement) => element.value || element.textContent, elResidenzaCap);
        if (!elResidenzaCapValue) {
          await waitForSelectorAndType(page, generateIdSelector('capResidenza'), residenzaCap);
        }
      },
      'Verifica il campo Cap di residenza'
    )

    await page.keyboard.press('Tab');
    await waitLoadingMessage(page);
  }

  private async compilaBoxIndirizzoAttivazioneLinea(page: Page, args: FastwebInserimentoPayload): Promise<void> {
    const logger = createChildPayloadAwareLogger(this.childLogger, args);
    // solo per tipo fornitura internet-casa
    logger.info('Compilo il box Indirizzo Attivazione Linea.');

    if (await isAutoCompleteOptionVisible(page, 'cittaAttivazione')) {
      const {
        attivazioneComune,
        attivazioneProvincia,
        attivazioneIndirizzo,
        attivazioneCivico,
        attivazioneCap,
        attivazioneScala,
        attivazionePiano,
        attivazioneInterno
      } = args;

      if (attivazioneComune) {
        await tryOrThrow(
          () => retry(
            () => selectAutoCompleteOption(
              page,
              'cittaAttivazione',
              attivazioneComune,
              // fixComuneAttivazione(mergeNomiCitta(...fixNomiCittaFastweb(fixComuneFastweb(attivazioneComune), attivazioneProvincia))),
              true,
              null,
              true
            )
          ),
          'Verificare il campo Città di attivazione'
        );

        await page.keyboard.press('Tab');
        await waitLoadingMessage(page);
      }
      if (attivazioneIndirizzo) {
        await tryOrThrow(
          () => retry(
            () => selectAutoCompleteOption(page, 'indirizzoAttivazione', attivazioneIndirizzo, true, { delay: 30 }, true)
          )
        , 'Verificare il campo Indirizzo di attivazione');

        await page.keyboard.press('Tab');
        await waitLoadingMessage(page);
      }

      if (attivazioneCivico) {
        await tryOrThrow(
          () => retry(
            async () => {
              await inserisciCivico(
                page,
                () => selectAutoCompleteOption(page, 'civicoAttivazione', attivazioneCivico, false, null, true),
                'civicoAttivazione'
              );
              await page.waitForResponse(() => true);
            }
          ),
          'Verificare il campo Numero civico di attivazione'
        );

        await page.keyboard.press('Tab');
        await waitLoadingMessage(page);
      }

      await this.inserirsciAttivazioneCap(page, attivazioneCap);

      if (attivazioneScala) {
        await waitForSelectorAndType(page, generateIdSelector('scalaAttivazione'), attivazioneScala);
      }
      if (attivazionePiano) {
        await waitForSelectorAndType(page, generateIdSelector('pianoAttivazione'), attivazionePiano);
      }
      if (attivazioneInterno) {
        await waitForSelectorAndType(page, generateIdSelector('internoAttivazione'), attivazioneInterno);
      }

      await delay(500);
    }
  }

  private async compilaBoxIndirizzoAttivazioneLineaNp(page: Page, args: FastwebInserimentoPayload): Promise<FailureResponse | null> {
    const logger = createChildPayloadAwareLogger(this.childLogger, args);
    logger.info('Compilo il box Indirizzo Attivazione Linea NP.');

    const {
      prefissoNumeroTelefono,
      numeroTelefono,
      codiceMigrazione,
      attualeFornitoreInternet,
      clientePossiedeUnaNumerazioneDaPortare,
      clientePossiedeUnaLineaDati,
      codicePortabilitaDati,
      operatoreProvenienzaDati,
      attivazioneCap
    } = args;

    const selectorLabelNpSi = generateLabelForSelector('np', 0);

    if (await isVisible(page, selectorLabelNpSi)) {
      if (prefissoNumeroTelefono && numeroTelefono && codiceMigrazione && attualeFornitoreInternet && clientePossiedeUnaNumerazioneDaPortare === 'si') {
        async function compilePrefix() {
          const elPrefixFisso = await page.$(generateIdSelector('prefixNumFisso'));
          await elPrefixFisso?.click({ clickCount: 3 });
          await waitForSelectorAndType(page, generateIdSelector('prefixNumFisso'), prefissoNumeroTelefono!);
        }

        logger.info('inserisco dati per portabilità fonia');
        await waitForSelectorAndClick(page, selectorLabelNpSi);

        await waitForSelectorAndType(page, generateIdSelector('codMigrazione'), codiceMigrazione);

        await delay(500);
        await compilePrefix();
        await compilePrefix();

        await waitForSelectorAndType(page, generateIdSelector('numFisso'), numeroTelefono);
        await page.keyboard.press('Tab');

        const err = await this.controlloApparizioneModaleErroreNP(page);

        if (err) {
          return err;
        }

        try {
          await selectComboOption(page, 'operatoreProvenienzaFisso', attualeFornitoreInternet);
        } catch (ex) {
          await selectComboOptionEvaluated(page, 'operatoreProvenienzaFisso', attualeFornitoreInternet);
        }
      } else {
        await waitForSelectorAndClick(page, generateLabelForSelector('np', 1));
      }
    }

    await waitLoadingMessage(page);

    const selectorLabelNpDatiSi = generateLabelForSelector('npDati', 0);

    if (await isVisible(page, selectorLabelNpDatiSi)) {
      if (codicePortabilitaDati && operatoreProvenienzaDati && clientePossiedeUnaLineaDati === 'si') {
        logger.info('inserisco dati per portabilità dati');
        await waitForSelectorAndClick(page, selectorLabelNpDatiSi);

        await waitForSelectorAndType(page, generateIdSelector('codMigrazioneDati'), codicePortabilitaDati);
        await Promise.all([
          page.waitForResponse(() => true),
          page.keyboard.press('Tab')
        ]);

        const err = await this.controlloApparizioneModaleErroreNP(page);

        if (err) {
          return err;
        }

        try {
          await selectComboOption(page, 'operatoreProvenienzaDati', operatoreProvenienzaDati);
        } catch (ex) {
          await selectComboOptionEvaluated(page, 'operatoreProvenienzaDati', operatoreProvenienzaDati);
        }
      } else {
        await waitForSelectorAndClick(page, generateLabelForSelector('npDati', 1));
      }
    }

    // ri-controllo se è apparsa la modale d'errore
    const err = await this.controlloApparizioneModaleErroreNP(page);

    if (err) {
      return err;
    }

    await waitForTimeout(page, 3000);

    await this.inserirsciAttivazioneCap(page, attivazioneCap);

    const tecnologia = await getElementText(page, '[id$="tecnologiaAttivazione"]', 'textContent');
    if (tecnologia?.toLowerCase() === 'non vendibile') {
      return new FailureResponse(`Tecnologia ${tecnologia}`);
    }

    return null;
  }

  private async compilaBoxSceltaModem(page: Page, args: FastwebInserimentoPayload): Promise<void> {
    const logger = createChildPayloadAwareLogger(this.childLogger, args);
    logger.info('Compilo il box Scelta Modem.');

    if (await isRadioVisible(page, 'sceltaModem')) {
      const {
        sceltaModem = 'FASTWEB'
      } = args;

      await selectRadio(page, 'sceltaModem', sceltaModem);
    }
  }

  private async compilaBoxDettagliMobile(page: Page, args: FastwebInserimentoPayload): Promise<void> {
    const logger = createChildPayloadAwareLogger(this.childLogger, args);
    logger.info('Compilo il box Dettagli Mobile.');

    const {
      isProdottoPortabilita
    } = args;

    if (isProdottoPortabilita && await isCheckboxVisible(page, 'mnp1')) {
      const {
        iccid,
        cellulareAttuale,
        attualeFornitoreMobile,
        tipoContratto = 'ricaricabile'
      } = args;

      await Promise.all([
        page.waitForResponse(() => true),
        selectCheckbox(page, 'mnp1')
      ]);

      // pulisci il vaore precompilato
      await page.evaluate((selector) => {
        document.querySelector(selector).value = '';
      }, generateIdSelector('iccd_1'));

      if (iccid) {
        await waitForSelectorAndType(page, generateIdSelector('iccd_1'), iccid);
      }
      if (cellulareAttuale) {
        await waitForSelectorAndType(page, generateIdSelector('numeroSim_1'), cellulareAttuale);
      }
      if (attualeFornitoreMobile) {
        await selectComboOption(page, 'operatoreProvenienza_1', attualeFornitoreMobile);
      }
      await selectComboOption(page, 'tipoContratto_1', tipoContratto);

      // const elCheck = await page.waitForSelector(generateIdSelector('trasferisciCredito_1_input'));
      if (!await isCheckboxChecked(page, 'trasferisciCredito_1')) {
        await selectCheckbox(page, 'trasferisciCredito_1');
      }
    }
  }

  private async compilaBoxIndirizzoSpedizione(page: Page, args: FastwebInserimentoPayload): Promise<void> {
    const logger = createChildPayloadAwareLogger(this.childLogger, args);
    logger.info('Compilo il box Indirizzo Spedizione.');

    const {
      recapitoComune,
      recapitoProvincia,
      recapitoIndirizzo,
      recapitoCivico,
      recapitoCap,
      recapitoPresso
    } = args;

    await tryOrThrow(
      () => retry(
        () => selectAutoCompleteOption(
          page,
          'cittaSpedizione',
          recapitoComune,
          // fixComuneSpedizione(mergeNomiCitta(...fixNomiCittaFastweb(fixComuneFastweb(recapitoComune), recapitoProvincia))),
          true,
          null,
          true
        )
      ),
      'Verificare il campo Città di spedizione'
    );

    await page.keyboard.press('Tab');
    await waitLoadingMessage(page);

    await tryOrThrow(
      () => retry(
        () => selectAutoCompleteOption(page, 'indirizzoSpedizione', recapitoIndirizzo, true, null, true)
      ),
      'Verificare il campo Indirizzo di spedizione'
    );

    await page.keyboard.press('Tab');
    await waitLoadingMessage(page);

    await tryOrThrow(
      () => retry(
        () => inserisciCivico(
          page,
          () => selectAutoCompleteOption(page, 'civicoSpedizione', recapitoCivico, false, null, true),
          'civicoSpedizione'
        )
      ),
      'Verificare il campo Numero civico di spedizione'
    );

    await page.keyboard.press('Tab');
    await waitLoadingMessage(page);

    const elSpedizioneCap = await page.waitForSelector(generateIdSelector('capSpedizione'));
    const elSpedizioneCapValue = await page.evaluate((element: HTMLInputElement) => element.value || element.textContent, elSpedizioneCap);
    if (!elSpedizioneCapValue) {
      await waitForSelectorAndType(page, generateIdSelector('capSpedizione'), recapitoCap);
    }

    if (recapitoPresso) {
      await waitForSelectorAndType(page, generateIdSelector('pressoSpedizione'), recapitoPresso);
    }
  }

  private async compilaBoxConsensoTrattamentoDati(page: Page, args: FastwebInserimentoPayload): Promise<void> {
    const logger = createChildPayloadAwareLogger(this.childLogger, args);
    logger.info('Compilo il box Consenso trattamento dati.');

    const {
      consenso1,
      consenso2,
      consenso3
    } = args;

    if (consenso1) {
      await selectCheckbox(page, 'consensoMarketing');
    }
    if (consenso2) {
      await selectCheckbox(page, 'consensoAbitudini');
    }
    if (consenso3) {
      await selectCheckbox(page, 'consensoCessioneDati');
    }
  }

  private async compilaBoxDettagliPagamento(page: Page, args: FastwebInserimentoPayload): Promise<void> {
    const logger = createChildPayloadAwareLogger(this.childLogger, args);
    logger.info('Compilo il box Dettagli Pagamento.');

    const {
      tipoPagamento,
      iban,
      intestazioneContoCorrente
    } = args;

    if (tipoPagamento) {
      if (await isVisible(page, generateIdSelector('tipoPagamento'))) {
        await selectComboOption(page, 'tipoPagamento', tipoPagamento);

        await waitForSelectorAndType(page, generateIdSelector('codiceIban'), iban as string);

        const intestatarioContoSelector = generateIdSelector('intestatarioConto');
        if (intestazioneContoCorrente && await isVisible(page, intestatarioContoSelector)) {
          await waitForSelectorAndType(page, intestatarioContoSelector, intestazioneContoCorrente);
        }
      }
    }
  }

  private async submitForm(page: Page, args: FastwebInserimentoPayload): Promise<SuccessResponse | FailureResponse> {
    const logger = createChildPayloadAwareLogger(this.childLogger, args);
    logger.info('Eseguo il submit della form.');

    await delay(1000);
    if (await isButtonDisabled(page, 'sendPdaButtons')) {
      return new FailureResponse('Il bottone di invio PDA è disabilitato!');
    }

    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    this.checkNoSubmit(args as any);

    await clickButton(page, 'sendPdaButtons');

    // attendo prima il loading che potrebbe ritornare un messaggio di ok o di errore
    const verificaMessage = await waitLoadingMessage(page);
    // se ho errori di validazione li torno come failure
    const errorMessages = await readErrorMessages(page);

    if (errorMessages) {
      return new FailureResponse(`Errori post invio firma: ${errorMessages.replace(/\n/g, ' ')}`);
    }
    if (verificaMessage) {
      try {
        // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
        const contractCode = parseFloat(verificaMessage.match(/\d+/)![0])!;
        return new SuccessResponse(contractCode.toString());
      } catch (exc) {
        logger.error(exc);
        logger.error(`Messaggio di verifica codice non parsato: "${verificaMessage}"`);
      }
      return new FailureResponse(`Messaggio di verifica codice non parsato: "${verificaMessage}"`);
    }
    return new FailureResponse('Errore non specificato!');
  }

  private async controlloApparizioneModaleErroreNP(page: Page): Promise<FailureResponse | null> {
    const modale = (await lookForVisibleWithTextElements(page, '.ui-dialog-content.ui-widget-content'))[0];

    if (modale) {
      const { text } = modale;
      return new FailureResponse(text);
    }

    return null;
  }

  private async inserirsciAttivazioneCap(page: Page, attivazioneCap: string): Promise<void> {
    if (attivazioneCap) {
      const attivazioneCapSelector = generateIdSelector('capAttivazione');
      if (await isVisible(page, attivazioneCapSelector)) {
        const elAttivazioneCap = await page.waitForSelector(attivazioneCapSelector);
        const elAttivazioneCapValue = await page.evaluate(
          (element: HTMLInputElement) => element.value || element.textContent,
          elAttivazioneCap
        );
        if (!elAttivazioneCapValue) {
          await waitForSelectorAndType(page, attivazioneCapSelector, attivazioneCap, undefined, { delay: 100 });
        }
      }
    }
  }

  getScraperCodice() {
    return 'fastweb';
  }

  getScraperTipologia() {
    return SCRAPER_TIPOLOGIA.INSERIMENTO;
  }

  async logout(page: Page, logger: Logger) {
    const logoutBtnSelector = 'a[title="Esci"]';
    try {
      await page.bringToFront();
      try {
        await page.goto('https://fastweb.force.com/partnersales/apex/ExternalSalesApps?sfdc.tabName=01r2X000000R0zs', { waitUntil: 'networkidle2' });
      } catch (e) {
        await page.goto('https://fastweb.force.com/partnersales/apex/GlobalSearch', { waitUntil: 'networkidle2' });
      }
      await waitForSelectorAndClick(page, '#userNavLabel');
      await waitForVisible(page, logoutBtnSelector, { timeout: 60000 });
      await waitForSelectorAndClick(page, logoutBtnSelector);
    } catch (ex) {
      await waitForTimeout(page, 3000);
      const content = await page.content();
      if (content.toLowerCase().includes('impossibile visualizzare la pagina')) {
        // ATTENZIONE: il seguente codice butta fuori dalla sessione chiunque connesso
        // try {
        //   await waitForXPathAndClick(page, '//a[contains(., "Esci")]');
        // } catch (ex2) {
        //   logger.error(`non sono riuscito ad effettuare il logout: ${extractErrorMessage(ex2)}`);
        // }
        logger.error('non sono riuscito ad effettuare il logout: sessione occupata');
      } else {
        logger.error(`non sono riuscito ad effettuare il logout: ${extractErrorMessage(ex)}`);
      }
    }
  }

  async controllaSessioneDisponibile(page: Page) {
    await waitForTimeout(page, 4000);

    const containerSelector = '#flowContainer';
    const bodySelector = `${containerSelector} .flowruntimeBody`;

    let numeroMassimoSessioniContainer: ElementHandle<Element> | null;
    let numeroMassimoSessioniBody: ElementHandle<Element> | null | undefined;
    try {
      // controllo se è apparso il messaggio di errore riguardante il massimo numero di accessi
      numeroMassimoSessioniContainer = await waitForVisible(page, containerSelector);
      numeroMassimoSessioniBody = await waitForVisible(page, bodySelector);
    } catch (ex) {
      // il box col messaggio non è apparso
    }

    // if (numeroMassimoSessioniContainer) {
    //   const testo = await page.evaluate((el: HTMLDivElement) => {
    //     const content = el.querySelector<HTMLDivElement>('.flowruntimeBody');
    //     return content?.textContent || '';
    //   }, numeroMassimoSessioniContainer);
    //   throw new TryOrThrowError(testo);
    // }
    if (numeroMassimoSessioniBody) {
      const testo = (await getElementText(page, bodySelector, 'textContent')) || '';

      if (testo.trim().toLowerCase() !== 'fine del flusso') {
        throw new TryOrThrowError(testo);
      }
    }
  }
}
