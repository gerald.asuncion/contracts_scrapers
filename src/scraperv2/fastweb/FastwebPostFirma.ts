import { Page } from "puppeteer";
import { Logger } from "winston";
import { MysqlPool } from "../../mysql";
import ErrorResponse from "../../response/ErrorResponse";
import FailureResponse from "../../response/FailureResponse";
import SuccessResponse from "../../response/SuccessResponse";
import { QueueTask } from "../../task/QueueTask";
import createChildPayloadAwareLogger from "../../utils/createChildPayloadAwareLogger";
import extractErrorMessage from "../../utils/extractErrorMessage";
import tryOrThrow from "../../utils/tryOrThrow";
import { ScraperResponse } from "../scraper";
import ScraperOptions from "../ScraperOptions";
import Fastweb from "./Fastweb";
import vaiAllaPaginaInserisciOrdine from "./fattibilita/vaiAllaPaginaInserisciOrdine";
import { FastwebPostFirmaPayload } from "./payload/FastwebPostFirmaPayload";
import aggiustaErrore from "./postfirma/aggiustaErrore";
import controllaStatoAccount from "./postfirma/controllaStatoAccount";
import esitaLaPratica from "./postfirma/esitaLaPratica";
import { getEnvelopId } from "./postfirma/getEnvelopId";
import inserisciOrdine from "./postfirma/inserisciOrdine";
import isBusinessErrorDaEsitare from "./postfirma/isBusinessErrorDaEsitare";
import needFailureCode from "./postfirma/needFailureCode";
import scaricaPda from "./postfirma/scaricaPda";

type FastwebPostFirmaOptions = ScraperOptions & {
  mysqlPool: MysqlPool;
};

@QueueTask()
export default class FastwebPostFirma extends Fastweb<FastwebPostFirmaPayload> {
  private mysqlPool: MysqlPool;

  constructor(options: FastwebPostFirmaOptions) {
    super(options);
    this.mysqlPool = options.mysqlPool;
  }

  /**
   * @override
   */
  // async login(): Promise<void> {
  //   const { username, password } = await this.getCredenziali();
  //   return super.login(<never>{ username, password });
  // }

  /**
   * @override
   */
  async scrapeWebsite(payload: FastwebPostFirmaPayload): Promise<ScraperResponse> {
    const logger = createChildPayloadAwareLogger(this.childLogger, payload);

    let page = await this.p();
    try {
      logger.info("controllo lo stato dell'account");
      let err = await tryOrThrow(
        () => controllaStatoAccount(page, logger, payload),
        ''
      );

      if (err) {
        await this.logout(page, logger);
        return err;
      }

      logger.info("recupero l'envelop id");
      const [envelopId, otherPage] = await tryOrThrow(
        () => getEnvelopId(page, logger, this.mysqlPool, payload, this.controllaSessioneDisponibile, { baseDownloadPath: this.baseDownloadPath }),
        "non sono riuscito a recuperare l'envelop id:"
      );

      logger.info(`recuperato envelop id ${envelopId} per il contratto ${payload.idDatiContratto}`);
      page = otherPage; // lo faccio per poter fare logout nel caso di errore

      await tryOrThrow(
        () => vaiAllaPaginaInserisciOrdine(otherPage, logger, this.controllaSessioneDisponibile),
        'non sono riuscito ad aprire la pagina inserisci ordine:'
      );

      err = await inserisciOrdine(otherPage, logger, this.mysqlPool, envelopId.toString(), payload);

      if (err) {
        await this.logout(page, logger);
        return err;
      }

      try {
        await tryOrThrow(
          () => scaricaPda(page, logger, this.baseDownloadPath, this.s3ClientFactory, payload.idDatiContratto),
          'non sono riuscito a scaricare la PDA:'
        );
        // 14789: lo fa già Fastweb
        // await tryOrThrow(
        //   () => esitaLaPratica(page, logger, this.controllaSessioneDisponibile, payload, 'Inserito su CPQ'),
        //   'non sono riuscito ad esitare la pratica:'
        // );
      } catch (ex) {
        logger.error(extractErrorMessage(ex));
        await this.logout(page, logger);
        return new FailureResponse('Contratto inserito, esitare la pratica su Fastweb CPQ');
      }

      await this.logout(page, logger);

      return new SuccessResponse();
    } catch (err) {
      let code = err instanceof FailureResponse || err instanceof ErrorResponse ? err.code : '05';
      let errMessage = extractErrorMessage(err);
      logger.error(errMessage);

      await this.saveMyScreenshot(page, logger, payload.idDatiContratto);

      if (isBusinessErrorDaEsitare(err as Error)) {
        logger.info('esito la pratica come `Cliente Irreperibile`');
        code = '02';
        try {
          await esitaLaPratica(page, logger, this.controllaSessioneDisponibile, payload, 'Cliente irreperibile');
        } catch (exEsitazionePratica) {
          logger.error(`non sono riuscito ad esitare la pratica come \`Cliente Irreperibile\` ${extractErrorMessage(exEsitazionePratica)}`);
          errMessage = 'Contratto non inserito, esitare la pratica su Fastweb CPQ come `Cliente Irreperibile`';
          await this.saveMyScreenshot(page, logger, payload.idDatiContratto);
        }
      }

      if (page) {
        await this.logout(page, logger);
      }

      errMessage = aggiustaErrore(errMessage);

      code = needFailureCode(errMessage) ? '02' : '05';

      if (code === '02') {
        return new FailureResponse(errMessage);
      }
      return new ErrorResponse(errMessage);
    }
  }

  /**
   * @override
   */
  getScraperCodice() {
    return 'fastweb-postfirma';
  }

  /**
   * @override
   */
  async saveMyScreenshot(page: Page, logger: Logger, idDatiContratto?: number): Promise<void> {
    if (page) {
      try {
        await this.saveScreenshot(page, logger, 'fastweb-postfirma-errore', idDatiContratto);
      } catch (screenshotEx) {
        logger.error(extractErrorMessage(screenshotEx));
      }
    }
  }
}
