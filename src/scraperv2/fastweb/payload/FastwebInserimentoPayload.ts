import ContrattoPayload from '../../../contratti/ContrattoPayload';

export default interface FastwebInserimentoPayload extends Required<ContrattoPayload> {
  codiceComsy?: string;

  username: string;
  password: string;

  tipoOfferta: 'Fisso+mobile' | 'Mobile' | 'Upsell mobile su cb Fisso' | 'Fisso' | 'Upsell fisso su CB mobile';
  offertaFisso?: 'Fastweb Nexxt Casa' | 'Fastweb Nexxt Casa Light' | 'Fastweb Casa Light' | 'Fastweb NeXXt Casa Plus' | "Fastweb Casa Light FWA" | "Fastweb Casa FWA",
  offertaMobile?: 'Fastweb NeXXt Mobile' | 'Fastweb NeXXt Mobile Maxi' | 'Fastweb Mobile Light';
  opzioneMobile?: 'nessuna opzione',
  offertaMobile2?: 'nessuna offerta mobile 2',
  sconto1?: 'nessuno sconto' | string,
  sconto2?: 'nessuno sconto' | string,
  partnership: string,
  opzioneFisso1?: 'nessuna opzione' | 'Opzione Now (primi 3 mesi gratis)' | 'Servizio potenziamento Wi Fi' | 'Servizio di estensione del segnale Wi Fi',
  opzioneFisso2?: 'nessuna opzione' | 'Opzione Fastweb&Dazn (primi 3 mesi gratis)' | 'Servizio potenziamento Wi Fi' | 'Servizio di estensione del segnale Wi Fi' | 'UpPlus',
  opzioneFisso3?: 'nessuna opzione' | 'Assicurazione Assistenza Casa Quixa';
  opzioneFisso8?: 'nessuna opzione' | 'Sconto Fisso Mobile' | '';

  accountCliente?: string; // 'account_cliente';

  nome: string; // 'nomeCliente';
  cognome: string; // 'cognomeCliente';
  sesso: 'M' | 'F'; // 'sessoCliente';

  dataNascita: string; // 'dataNascitaCliente';
  comuneNascitaEstero: boolean;
  comuneNascita: string; // 'comuneNascitaCliente';
  provinciaNascita: string; // 'provinciaNascitaCliente';

  codiceFiscale: string; // 'codiceFiscaleCliente';
  cellulare: string; // 'cellulareCliente';
  email: string; // 'emailCliente';

  tipoInvio: 'EMAIL' | 'SMS'; // 'verifica_otp';

  documentoTipo: string; // 'documento_tipo';
  documentoNumero: string; // 'documento_numero';
  documentoEnteRilascio: string; // 'documento_ente_rilascio';
  documentoDataRilascio: string; // 'documento_data_rilascio';
  documentoRilasciatoDa?: string; // 'documento_tipo_ente_rilascio';
  documentoProvinciaRilascio?: string; // 'documento_provincia_rilascio';

  residenzaComune: string; // 'residenza_comune';
  residenzaIndirizzo: string; // 'residenza_indirizzo';
  residenzaCivico: string; // 'residenza_civico + residenza_scala';
  residenzaCap: string; // 'residenza_cap';
  residenzaProvincia: string; // residenza_provincia

  attivazioneComune: string; // 'attivazione_comune';
  attivazioneProvincia: string; // attivazione_provincia
  attivazioneIndirizzo: string; // 'attivazione_indirizzo';
  attivazioneCivico: string; // 'attivazione_civico + attivazione_scala';
  attivazioneCap: string; // 'attivazione_cap';
  attivazioneScala?: string; // 'attivazione_scala';
  attivazionePiano?: string; // 'attivazione_piano';
  attivazioneInterno?: string; // 'attivazione_interno';

  prefissoNumeroTelefono?: string; // 'prefisso_numero_telefono';
  numeroTelefono?: string; // 'numero_telefono';
  codiceMigrazione?: string; // 'codice_migrazione';
  attualeFornitoreInternet?: string; // 'fornitore_internet';

  sceltaModem: 'FASTWEB' | 'PROPRIETA';

  isProdottoPortabilita: boolean;
  iccid?: string; // 'iccid';
  cellulareAttuale?: string; // 'cellulare_attuale';
  attualeFornitoreMobile?: string; // 'attuale_fornitore_mobile';
  tipoContratto: 'ricaricabile';

  recapitoComune: string; // 'recapito_fattura_comune';
  recapitoProvincia: string;
  recapitoIndirizzo: string; // 'recapito_fattura_indirizzo';
  recapitoCivico: string; // 'recapito_fattura_civico';
  recapitoCap: string; // 'recapito_fattura_cap';
  recapitoPresso?: string; // 'recapito_fattura_presso';

  consenso1: boolean;
  consenso2: boolean;
  consenso3: boolean;

  tipoPagamento: 'Addebito su Conto Corrente' | null;
  iban: string | null;
  intestazioneContoCorrente: string | null;

  clientePossiedeUnaNumerazioneDaPortare: 'si' | 'no';
  clientePossiedeUnaLineaDati: 'si' | 'no';
  codicePortabilitaDati?: string;
  operatoreProvenienzaDati?: string;
}
