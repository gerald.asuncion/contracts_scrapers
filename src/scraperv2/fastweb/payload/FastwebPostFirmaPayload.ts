import FastwebInserimentoPayload from "./FastwebInserimentoPayload";

export type FastwebPostFirmaPayload = FastwebInserimentoPayload & {
  idEsterno: number;
  nascitaStato?: string;
  metodoPagamentoNome: string;
  metodoPagamentoCognome: string;
  metodoPagamentoSesso: string;
  metodoPagamentoNascitaData: string;
  metodoPagamentoNascitaEstero: boolean;
  metodoPagamentoNascitaStato: string;
  metodoPagamentoNascitaComune: string;
  metodoPagamentoNascitaProvincia: string;
  metodoPagamentoCodiceFiscale: string;
};
