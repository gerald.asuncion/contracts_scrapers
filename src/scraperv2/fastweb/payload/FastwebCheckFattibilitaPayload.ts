import ContrattoPayload from '../../../contratti/ContrattoPayload';

export type FastwebVerificaCoperturaPayload = ContrattoPayload & {
  attivazioneComune: string,
  attivazioneIndirizzo: string,
  attivazioneCivico: string,
  attivazioneCap: string,
  attivazioneProvincia: string
};

export type FastwebVerificaMorositaPayload = FastwebVerificaCoperturaPayload & {
  nome: string,
  cognome: string,
  codiceFiscale: string,
  sesso: 'm' | 'f',
  dataNascita: string,
  nascitaProvinciaEstera: 'si' | 'no',
  nascitaStato: string, // 'nascita_comune/italia'
  nascitaComune: string,
  nascitaProvincia: string,
  cellulare: string,
  email: string
};

export type FastwebVerificaDocumentoPayload = FastwebVerificaMorositaPayload & {
  documentoTipo: string,
  documentoNumero: string,
  documentoRilasciatoDa: string,
  documentoEnteRilascio: string,
  documentoDataRilascio: string,
  documentoProvinciaRilascio: string
};

type FastwebCheckFattibilitaPayload = FastwebVerificaDocumentoPayload;

export default FastwebCheckFattibilitaPayload;
