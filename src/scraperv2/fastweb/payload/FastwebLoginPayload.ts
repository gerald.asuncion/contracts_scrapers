export default interface FastwebLoginPayload {
  username: string;
  password: string;
}
