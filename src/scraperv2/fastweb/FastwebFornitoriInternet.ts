import { Page } from 'puppeteer';
import { ScraperResponse } from '../scraper';
import FornitoriInternetPayload from './fornitori/FornitoriInternetPayload';
import FastwebFornitoriEstrattore from './fornitori/FastwebFornitoriEstrattore';
import ScraperOptions from '../ScraperOptions';
import waitForSelectorAndClick from '../../browser/page/waitForSelectorAndClick';
import waitForTimeout from '../../browser/page/waitForTimeout';
import { QueueTask } from '../../task/QueueTask';

// si => labelValue = 0
// no => labelValue = 1
const getCheckboxSelector = (id: string, labelValue: number) => `div[id$=":${id}"] label[for$=":${id}:${labelValue}"]`;

@QueueTask()
export default class FastwebFornitoriInternet extends FastwebFornitoriEstrattore {
  private checkboxSelectorDati;

  constructor(options: ScraperOptions) {
    super(options);
    this.firstComboboxSelector = 'offerta';
    this.secondComboboxSelector = 'offertaFisso';
    this.listContainerSelector = 'operatoreProvenienzaFisso_panel';
    this.checkboxSelector = getCheckboxSelector('np', 0);
    this.checkboxSelectorDati = getCheckboxSelector('npDati', 0);
  }

  /**
   * @override
   */
  scrapeWebsite(): Promise<ScraperResponse> {
    return super.scrapeWebsite(<never>FornitoriInternetPayload);
  }

  /**
   * @override
   */
  async clickOnCheckbox(page: Page): Promise<void> {
    this.childLogger.info('apro il box contenente la suggestion');
    await waitForSelectorAndClick(page, this.checkboxSelector);
    await waitForTimeout(page, 30000);
    await waitForSelectorAndClick(page, this.checkboxSelectorDati);
  }
}
