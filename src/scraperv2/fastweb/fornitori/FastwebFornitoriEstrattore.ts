import { Page } from 'puppeteer';
import Fastweb from '../Fastweb';
import { ScraperResponse } from '../../scraper';
import FastwebInserimentoPayload from '../payload/FastwebInserimentoPayload';
import selectComboOption from '../inserimento/controls/selectComboOption';
import waitForTimeout from '../../../browser/page/waitForTimeout';
import { generateIdSelector } from '../inserimento/helpers/selectors';
import GenericSuccessResponse from '../../../response/GenericSuccessResponse';
import ScraperOptions, { SCRAPER_TIPOLOGIA } from '../../ScraperOptions';
import { Logger } from '../../../logger';
import FastwebLoginPayload from '../payload/FastwebLoginPayload';

export default class FastwebFornitoriEstrattore extends Fastweb {
  protected firstComboboxSelector: string;

  protected secondComboboxSelector: string;

  protected checkboxSelector: string;

  protected listContainerSelector: string;

  constructor(options: ScraperOptions) {
    super(options);
    this.firstComboboxSelector = 'offerta';
    this.secondComboboxSelector = 'offertaMobile1';
    this.checkboxSelector = 'mnp1';
    this.listContainerSelector = 'operatoreProvenienza_1_panel';
  }

  /**
   * @override
   */
  async login(payload: FastwebLoginPayload): Promise<void> {
    const { username, password } = await this.getCredenziali(payload, true);
    return super.login(<never>{ username, password });
  }

  // da implementare nella classi figlie
  async clickOnCheckbox(page: Page): Promise<void> {
    throw new Error('Not implemented');
  }

  /**
   * @override
   * @param page    Browser page
   * @param payload Request payload
   */
  async executeScraper(
    page: Page,
    { tipoOfferta, offertaMobile }: FastwebInserimentoPayload,
    logger: Logger
  ): Promise<ScraperResponse> {
    logger.info(`inserisco tipo offerta "${tipoOfferta}"`);
    await selectComboOption(page, this.firstComboboxSelector, tipoOfferta);

    logger.info(`inserisco offerta fissa "${offertaMobile}"`);
    await selectComboOption(page, this.secondComboboxSelector, offertaMobile as string);

    await this.clickOnCheckbox(page);

    waitForTimeout(page, 100);

    logger.info('recupero la lista di fornitori');
    const listContainer = await page.waitForSelector(generateIdSelector(this.listContainerSelector));
    const list: string[] | undefined = await listContainer?.evaluate((el) => {
      const items = el.querySelectorAll<HTMLLIElement>('ul > li');

      return Array.from(items).map(({ textContent }) => textContent?.trim()) as Array<string>;
    });

    await this.logout(page, logger);

    return new GenericSuccessResponse<Array<string>>((list || []).slice(1));
  }

  /**
   * @override
   */
  getScraperCodice() {
    return 'fastweb-fornitori';
  }

  /**
   * @override
   */
  getScraperTipologia() {
    return SCRAPER_TIPOLOGIA.IMPORTDATA as any;
  }
}
