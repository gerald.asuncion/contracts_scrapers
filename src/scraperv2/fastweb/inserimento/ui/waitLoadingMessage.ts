import { Page } from 'puppeteer';
import { WebScraper } from '../../../scraper';
import blurSelection from '../controls/blurSelection';
import checkAlertMessage from '../controls/checkAlertMessage';

export default async (page: Page, blurFirst = true): Promise<string> => {
  if (blurFirst) {
    await blurSelection(page);
    await WebScraper.delay(50);
  }

  await page.waitForSelector('.fa.fa-circle-o-notch.fa-spin', { hidden: true, timeout: 60000 });

  return checkAlertMessage(page);
};
