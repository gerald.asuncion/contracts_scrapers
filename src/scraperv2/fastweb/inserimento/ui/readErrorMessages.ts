import { Page } from 'puppeteer';

export default async (page: Page): Promise<string> => page
  .$$eval(
    '[role="alert"]',
    (elements) => elements
      .map((el) => {
        let id = el.getAttribute('id')?.toString();
        if (id && id.indexOf(':') > 0) {
          // eslint-disable-next-line prefer-destructuring
          id = id.split(':')[1];
        }
        return `${id}: ${el.textContent}`;
      })
      .join(';\n')
  );
