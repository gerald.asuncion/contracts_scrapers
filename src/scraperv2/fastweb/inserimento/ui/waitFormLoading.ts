import { Page } from 'puppeteer';
import waitLoadingMessage from './waitLoadingMessage';

export default async (page: Page): Promise<void> => {
  const message = await waitLoadingMessage(page, false);

  if (message) {
    throw new Error(message);
  }
};
