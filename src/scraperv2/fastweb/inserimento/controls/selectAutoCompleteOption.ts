import { ElementHandle, Page } from 'puppeteer';
import isVisible from '../../../../browser/page/isVisible';
import waitForSelectorAndType from '../../../../browser/page/waitForSelectorAndType';
import { WebScraper } from '../../../scraper';
import { generateIdSelector } from '../helpers/selectors';
import waitFormLoading from '../ui/waitFormLoading';
import blurSelection from './blurSelection';
import selectOption from './selectOption';
import waitForSelectorAndTypeEvaluated from '../../../../browser/page/waitForSelectorAndTypeEvaluated';
import getPreValue from '../../helpers/getPreValue';

export default async (
  page: Page,
  name: string,
  value: string,
  forceIfSingleResult = false,
  typeOptions: { delay: number } | null = null,
  pressEnter = false
): Promise<void> => {
  const inputSelector = generateIdSelector(`${name}_input`);
  const panelSelector = generateIdSelector(`${name}_panel`);

  if (!await isVisible(page, inputSelector)) {
    return;
  }

  // 11345 inserito .split('-')[0] per far apparire la suggestion
  // nei casi degli indirizzi comprendenti il trattino
  // altrimenti fastweb non mostrerebbe la suggestion
  const preValue = getPreValue(value.split('-')[0]);

  await waitForSelectorAndTypeEvaluated(page, inputSelector, '');
  if (typeOptions) {
    await waitForSelectorAndType(page, inputSelector, preValue, undefined, typeOptions);
  } else {
    await waitForSelectorAndType(page, inputSelector, preValue);
  }

  if (pressEnter) {
    await page.keyboard.press('Enter');
  }

  await page.waitForSelector(panelSelector, { visible: true });

  const autocompleteElementsList = await page.$$(`${panelSelector} > ul > li[role="option"]`);
  let selectedComboElement!: ElementHandle<Element>;

  if (forceIfSingleResult && autocompleteElementsList.length === 1) {
    [selectedComboElement] = autocompleteElementsList;
  } else {
    selectedComboElement = await selectOption(
      page,
      autocompleteElementsList,
      'data-item-label',
      value,
      name,
      true
    );
  }

  await Promise.all([
    waitFormLoading(page),
    selectedComboElement.click()
  ]);

  await WebScraper.delay(500);
  await blurSelection(page);
};
