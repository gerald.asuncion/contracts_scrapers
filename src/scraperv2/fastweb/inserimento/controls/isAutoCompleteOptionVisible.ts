import { Page } from 'puppeteer';
import isVisible from '../../../../browser/page/isVisible';
import { generateIdSelector } from '../helpers/selectors';

export default async (page: Page, name: string): Promise<boolean> => {
  const selector = generateIdSelector(`${name}_input`);

  return isVisible(page, selector);
};
