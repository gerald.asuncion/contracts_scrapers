import { Page } from 'puppeteer';
import waitForXPathAndClick from '../../../../browser/page/waitForXPathAndClick';

export default async (page: Page): Promise<string> => {
  const message = await page.evaluate(() => {
    const xPathResult = document.evaluate(
      '//*[@aria-modal="true"][@aria-hidden="false"][1]//*[contains(@class, "ui-dialog-content")]//text()',
      document,
      null,
      XPathResult.ANY_TYPE,
      null
    );

    let result = '';
    let textNode;
    // eslint-disable-next-line no-cond-assign
    while (textNode = xPathResult.iterateNext()) {
      result += textNode.textContent?.trim();
    }

    return result;
  });

  if (message === "Attenzione: al momento non è possibile effettuare il controllo del recapito.Procedere con l'ordine.") {
    await waitForXPathAndClick(page, '//*[@aria-modal="true"][@aria-hidden="false"][1]//div[contains(@class, "ui-dialog-footer")]//button');
    return '';
  }

  return message;
};
