import { Page } from 'puppeteer';
import waitForSelectorAndClick from '../../../../browser/page/waitForSelectorAndClick';
import { generateIdSelector } from '../helpers/selectors';
import isCheckboxVisible from './isCheckboxVisible';

export default async (page: Page, name: string): Promise<void> => {
  if (!await isCheckboxVisible(page, name)) {
    return;
  }

  const selector = generateIdSelector(`${name}`);

  try {
    await waitForSelectorAndClick(page, `${selector} .ui-chkbox-icon.ui-icon`);
  } catch {
    throw new Error(`Checkbox con selettore '${selector}' non trovato!`);
  }
};
