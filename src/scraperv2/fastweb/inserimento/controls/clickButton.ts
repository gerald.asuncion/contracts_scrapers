import { Page } from 'puppeteer';
import waitForSelectorAndClick from '../../../../browser/page/waitForSelectorAndClick';
import { generateIdSelector } from '../helpers/selectors';

export default async (page: Page, name: string): Promise<void> => {
  const selector = generateIdSelector(name);

  await waitForSelectorAndClick(page, `${selector} button[type="submit"][role="button"]`);
};
