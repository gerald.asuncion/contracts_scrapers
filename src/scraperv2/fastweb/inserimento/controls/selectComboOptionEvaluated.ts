import { Page } from 'puppeteer';
import isVisible from '../../../../browser/page/isVisible';
import { generateIdSelector } from '../helpers/selectors';
import blurSelection from './blurSelection';
import waitForSelectorAndClickEvaluated from '../../../../browser/page/waitForSelectorAndClickEvaluated';
import waitForHidden from '../../../../browser/page/waitForHidden';
import delay from '../../../../utils/delay';

export default async (page: Page, name: string, value: string): Promise<void> => {
  const selector = generateIdSelector(name.replace(':', '\\:'));

  if (!await isVisible(page, selector)) {
    return;
  }

  // click per aprire la combo
  await waitForSelectorAndClickEvaluated(page, `${selector} .ui-selectonemenu-trigger > span`);

  // attendo che il loader sparisca
  try {
    // await waitForHidden(page, 'body > [id$="_modal"]');
    await waitForHidden(page, 'body > .ui-widget-overlay');
  } catch (ex) {
    // loader non presente
  }
  await delay(3000);

  // necessario, viene chiusa la suggestion nonostante il primo click
  await waitForSelectorAndClickEvaluated(page, `${selector} .ui-selectonemenu-trigger > span`);

  // prendo gli elementi della combo (non gli option della select, ma quelli del widget)
  const comboElementsListId = await page.$eval(selector, (el) => el.getAttribute('aria-owns'));
  const comboElementsListIdReplaced = generateIdSelector(comboElementsListId?.split(':')[1] as string);

  const optionSelector = `${comboElementsListIdReplaced} li[role="option"][data-label="${value}"]`;
  await waitForSelectorAndClickEvaluated(page, optionSelector);

  await delay(500);
  await blurSelection(page);
};
