import { Page } from 'puppeteer';
import { generateNameXPath } from '../helpers/selectors';

export default async (page: Page, name: string, value: string): Promise<void> => {
  const selector = generateNameXPath(name);

  try {
    const interactiveElement = await page
      .$x(`//input${selector}[@type="radio"][@value="${value}"]/parent::*/following-sibling::*`);

    interactiveElement[0].click();
  } catch {
    throw new Error(`Radiobutton con selettore '${selector}' con il valore '${value}' non trovato!`);
  }

  // assert(inputElement, `Radiobutton con selettore '${selector}' con il valore '${value}' non trovato!`);
};
