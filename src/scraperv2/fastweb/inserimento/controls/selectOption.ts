import {uniqBy} from 'lodash';
import {ElementHandle, Page} from 'puppeteer';
import {TryOrThrowError} from '../../../../utils/tryOrThrow';

type CompareTextOptions = {
  caseInsensitive: boolean;
  accentsInsensitive: boolean;
  accentsReplaceWith: string;
  diacriticalInsensitive: boolean;
};

const compareText = (_left: string | number, _right: string | number, {
                       caseInsensitive = true,
                       accentsInsensitive = true,
                       accentsReplaceWith = '',
                       diacriticalInsensitive = true,
                     }: Partial<CompareTextOptions> = {},
                     contains = true) => {
  let left = _left.toString();
  let right = _right.toString();

  if (caseInsensitive) {
    left = left.toLowerCase();
    right = right.toLowerCase();
  }

  if (accentsInsensitive) {
    left = left.replace(/'/g, accentsReplaceWith).replace(/"/g, accentsReplaceWith);
    right = right.replace(/'/g, accentsReplaceWith).replace(/"/g, accentsReplaceWith);
  }

  if (diacriticalInsensitive) {
    left = left.normalize('NFD').replace(/[\u0300-\u036f]/g, '');
    right = right.normalize('NFD').replace(/[\u0300-\u036f]/g, '');
  }

  return contains ? left.indexOf(right) > -1 : left === right;
};

const repeatThrough = <TP = unknown, TI = unknown>(
  handler: (payload: TP) => TI,
  repeat: (data: TI, payload: TP) => boolean,
  payloads: TP[],
  error: string | Error
) => {
  for (let i = 0; i < payloads.length; i += 1) {
    const payload = payloads[i];
    const res = handler(payload);

    if (!repeat(res, payload)) {
      return res;
    }
  }

  throw typeof error === 'string' ? new TryOrThrowError(error) : error;
};

const sensitivenessSequence: (Partial<CompareTextOptions> | undefined)[] = [
  undefined, {
    caseInsensitive: false
  }, {
    caseInsensitive: false,
    accentsReplaceWith: ' '
  }, {
    diacriticalInsensitive: false
  }, {
    diacriticalInsensitive: false,
    accentsReplaceWith: ' '
  }, {
    caseInsensitive: false,
    diacriticalInsensitive: false
  }, {
    caseInsensitive: false,
    diacriticalInsensitive: false,
    accentsReplaceWith: ' '
  }
];

export default async <T extends ElementHandle<Element>>(
  page: Page,
  elementsList: T[],
  valueAttributeName: string,
  value: string,
  field: string,
  removeDuplicates = false,
  contains = false,
): Promise<ElementHandle<Element>> => {
  const trimmedValue = value.trim();
  let comboElementsListWithLabel = await Promise.all(elementsList.map(async (item) => ({
    item,
    label: await page.evaluate((el, _valueAttributeName) => (el.getAttribute(_valueAttributeName) || '').trim(), item, valueAttributeName)
  })));

  if (removeDuplicates) {
    comboElementsListWithLabel = uniqBy(comboElementsListWithLabel, item => item.label);
  }

  const selectedComboElementsListWithLabel = repeatThrough(
    (payload) => comboElementsListWithLabel.filter(({label}) => compareText(label, trimmedValue, payload, contains)),
    (items) => items.length !== 1,
    sensitivenessSequence,
    `Impossibile selezionare '${trimmedValue}' nel campo '${field}'. Trovati i seguenti suggerimenti ['${
      comboElementsListWithLabel.map(({label}) => label).join("', '")}']!`
  );

  return selectedComboElementsListWithLabel[0].item;
};
