import { Page } from 'puppeteer';
import isVisible from '../../../../browser/page/isVisible';
import waitForSelectorAndClick from '../../../../browser/page/waitForSelectorAndClick';
import { WebScraper } from '../../../scraper';
import { generateIdSelector } from '../helpers/selectors';
import blurSelection from './blurSelection';
import selectOption from './selectOption';

export default async (page: Page, name: string, value: string, contains = false): Promise<void> => {
  const selector = generateIdSelector(name.replace(':', '\\:'));

  if (!await isVisible(page, selector)) {
    return;
  }

  // click per aprire la combo
  await waitForSelectorAndClick(page, `${selector} .ui-selectonemenu-trigger > span`);
  // prendo gli elementi della combo (non gli option della select, ma quelli del widget)
  const comboElementsListId = await page.$eval(selector, (el) => el.getAttribute('aria-owns')?.replace(/:/g, '\\:'));
  const comboElementsList = await page.$$(`#${comboElementsListId} li[role="option"]`);

  const selectedComboElement = await selectOption(
    page,
    comboElementsList,
    'data-label',
    value,
    name,
    false,
    contains,
  );

  await WebScraper.delay(500);
  await selectedComboElement.click();

  await WebScraper.delay(500);
  await blurSelection(page);
};
