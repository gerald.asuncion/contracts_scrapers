import { Page } from 'puppeteer';

export default async (page: Page): Promise<void> => {
  await page.evaluate(() => {
    (document.activeElement as HTMLElement)?.blur();
  }, null);
};
