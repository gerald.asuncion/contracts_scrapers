import { ElementHandle, Page } from 'puppeteer';
import waitForSelectorAndType from '../../../../browser/page/waitForSelectorAndType';
import { generateInputNameSelector } from '../helpers/selectors';
import stripNonNumeric from '../../../../utils/stripNonNumeric';

const selectDate = async (page: Page, name: string, value: string, retry = 10): Promise<void> => {
  if (retry < 0) {
    throw new Error(`Datepicker con name '${name}' ha dato errori multipli!`);
  }

  const selector = generateInputNameSelector(`${name}_input`);

  try {
    await waitForSelectorAndType(page, selector, value);
  } catch {
    throw new Error(`Datepicker con name '${name}' non trovato!`);
  }

  const elHandle = await page.waitForSelector(selector) as ElementHandle<HTMLInputElement>;
  const inputValue = await page.evaluate((el) => el.value, elHandle);
  if (stripNonNumeric(inputValue) !== value) {
    await elHandle.evaluate((el) => {
      el.value = '';
    });
    await selectDate(page, name, value, retry - 1);
  }
};

export default selectDate;
