import { Page } from 'puppeteer';
import getAttribute from '../../../../browser/page/getAttribute';
import { generateIdSelector } from '../helpers/selectors';

export default async (page: Page, name: string): Promise<boolean> => {
  const selector = generateIdSelector(`${name}_input`);

  return getAttribute(page, selector, 'checked');
};
