export const generateIdSelector = (text: string): string => `[id^="j_id_"][id$=":${text}"]`;

export const generateInputNameSelector = (text: string): string => `input[name^="j_id_"][name$=":${text}"]`;

export const generateLabelForSelector = (text: string, id: number): string => `div.ui-radiobutton.ui-widget + label[for$=":${text}:${id}"]`;

// eslint-disable-next-line max-len
export const generateNameXPath = (text: string): string => `[substring(@name, string-length(@name) - string-length("${text}") + 1) = "${text}"]`;
