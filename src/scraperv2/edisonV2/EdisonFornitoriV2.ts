import { Page } from "puppeteer";
import { Logger } from "../../logger";
import EdisonInserimentoV2 from "./EdisonInserimentoV2";
import { ScraperResponse } from "../scraper";
import createChildPayloadAwareLogger from "../../utils/createChildPayloadAwareLogger";
import tryOrThrow from "../../utils/tryOrThrow";
import { EdisonInserimentoV2Payload } from "./payload/EdisonInserimentoV2Payload";
import stepTipologiaContratto from './inserimento/stepTipologiaContratto';
import stepClienteResidenzialePerFornitori from './fornitori/stepClienteResidenziale';
import stepDatiFornitura from "./fornitori/stepDatiFornitura";
import GenericSuccessResponse from "../../response/GenericSuccessResponse";
import ErrorResponse from "../../response/ErrorResponse";

type EdisonResponseBodyActionReturnValue = {
  esito: string;
  infoReturn: {
    listFornitori: string;
    distributoriEE: string;
  };
};

type EdisonResponseBody = {
  actions: {
    returnValue: string | EdisonResponseBodyActionReturnValue;
  }[];
};

export default class EdisonFornitoriV2 extends EdisonInserimentoV2 {
  async vaiAllaPaginaFornitura(page: Page, logger: Logger, payload: EdisonInserimentoV2Payload): Promise<void> {
    await tryOrThrow(() => this.navigaANuovoContratto(page, payload), 'non sono riuscito a navigare fino alla pagina di inserimento contratto');

    logger.info('step anagrafica cliente');
    await tryOrThrow(() => stepTipologiaContratto({ page, logger, stepId: 0, isFirstStep: true, isLastStep: false, steps: 0, result: '' }, payload), 'non sono riuscito a compilare la `tipologia contratto`');

    await tryOrThrow(() => stepClienteResidenzialePerFornitori({ page, logger, stepId: 0, isFirstStep: true, isLastStep: false, steps: 0, result: '' }, payload), 'non sono riuscito a compilare il box `cliente residenziale`');

    logger.info('step dati fornitura');
    await tryOrThrow(() => stepDatiFornitura(page, logger, payload), 'non sono riuscito ad aprire la modale con la lista dei fornitori');
  }

  async scrapeWebsite(payload: EdisonInserimentoV2Payload): Promise<ScraperResponse> {
    const logger = createChildPayloadAwareLogger(this.childLogger, payload);
    const page = await this.p();

    await this.vaiAllaPaginaFornitura(page, logger, payload);

    const resp = await page.waitForResponse(response => response.request().url().includes('other.addContractRowCommodityComponent.getInitData'));
    const result: EdisonResponseBody = await resp.json();
    const action = result.actions.find((action) => typeof action.returnValue !== 'string');
    let listaFornitori: string[] = [];
    if (action) {
      const { infoReturn } = (action.returnValue as EdisonResponseBodyActionReturnValue);
      listaFornitori = infoReturn.listFornitori.split('*__*');
    }

    if (!listaFornitori.length) {
      logger.error('per qualche strano motivo non sono riuscito a recuperare la lista di fornitori');
      await this.saveScreenshot(page, logger, this.constructor.name);
      return new ErrorResponse('lista fornitori non trovata');
    }

    logger.info(`estratti ${listaFornitori.length} fornitori ${payload.tipoOfferta}`);

    return new GenericSuccessResponse(listaFornitori);
  }
}
