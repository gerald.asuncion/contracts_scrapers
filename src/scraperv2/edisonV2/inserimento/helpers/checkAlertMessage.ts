import { Page } from "puppeteer";
import { TryOrThrowError } from "../../../../utils/tryOrThrow";
import waitForVisible from "../../../../browser/page/waitForVisible";
import getElementText from "../../../../browser/page/getElementText";

const alertErrorSelector = '.forceToastMessage';

export default async function checkAlertMessage(page: Page, excludeErrors: string[] = []) {
  let msg: string | null = null;
  try {
    await waitForVisible(page, alertErrorSelector);
    msg = await getElementText(page, alertErrorSelector, 'textContent');
    if (msg) {
      if (msg.toLowerCase().includes('success')) {
        msg = null;
      }
      else {
        msg = await getElementText(page, `${alertErrorSelector} .forceActionsText`, 'textContent');
      }
    }
  }
  catch (ex) {
    // non apparso
  }
  if (msg && !excludeErrors.find(ee => ee.toLowerCase() === msg!.toLowerCase())) {
    throw new TryOrThrowError(msg);
  }
}
