import { Page } from "puppeteer";
import { Logger } from "winston";
import waitForXPathAndClick from "../../../../browser/page/waitForXPathAndClick";
import tryOrThrow from "../../../../utils/tryOrThrow";

/**
 * element is hidden so we use $x: so be sure to have the element inside the dom
 * @param page
 * @param logger
 * @param name
 */
export default async function(page: Page, logger: Logger, name: string): Promise<void> {
  await tryOrThrow(async () => {
    await waitForXPathAndClick(page, `//input[@type="checkbox"][starts-with(@name, "${name}_")]/parent::span/label/span[@class="slds-checkbox_faux"]`);
  }, `Errore durante la selezione della checkbox con nome '${name}'`);
}
