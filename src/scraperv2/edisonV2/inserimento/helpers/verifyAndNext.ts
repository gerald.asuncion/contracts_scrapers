import { Page } from "puppeteer";
import isXPathVisible from "../../../../browser/page/isXPathVisible";
import waitForXPathAndClick from "../../../../browser/page/waitForXPathAndClick";
import waitFormLoading from "../../../../browser/salesforce/lightining-components/helpers/loading";
import { Logger } from "../../../../logger";
import delay from "../../../../utils/delay";
import tryOrThrow, { TryOrThrowError } from "../../../../utils/tryOrThrow";
import hasToastError from "./hasToastError";
import checkAlertMessage from "./checkAlertMessage";

const getBtnVerificaSelector = (label: string) => `//button[@type="submit" or @type="button"][contains(.,"${label}")]`;
const btnAvantiSelector = '//button[@type="button"][. = "Avanti"]';

export default async function(
  page: Page,
  logger: Logger,
  verifyButtonLabel: string,
  {
    noNextClick = false,
    nextClickAwaitingSelector,
    xPathPre = '',
    xPathPost = '',
    excludeErrors = []
  }: {
    noNextClick?: boolean;
    nextClickAwaitingSelector?: string;
    xPathPre?: string;
    xPathPost?: string;
    excludeErrors?: string[];
  } = {}
): Promise<void> {
  const btnVerificaSelector = getBtnVerificaSelector(verifyButtonLabel);

  await tryOrThrow(async () => {
    await delay(1000);

    if (await isXPathVisible(page, `${btnVerificaSelector}[@disabled]`, 1500)) {
      await waitForXPathAndClick(page, btnAvantiSelector);
      await waitFormLoading(page, logger, {
        xPathPre,
        xPathPost
      });

      return;
    }

    await waitForXPathAndClick(page, btnVerificaSelector);
    await delay(1000);

    await waitFormLoading(page, logger, {
      xPathPre,
      xPathPost
    });
    let toastErrorMessages = await hasToastError(page, logger);
    if (toastErrorMessages) {
      throw new TryOrThrowError(toastErrorMessages);
    }

    await waitFormLoading(page, logger, {
      xPathPre,
      xPathPost
    });
    toastErrorMessages = await hasToastError(page, logger);
    if (toastErrorMessages) {
      throw new TryOrThrowError(toastErrorMessages);
    }

    await checkAlertMessage(page, excludeErrors);

    if (!noNextClick || nextClickAwaitingSelector) {
      await delay(500);
      nextClickAwaitingSelector && await page.waitForSelector(nextClickAwaitingSelector);
      await delay(500);

      await waitForXPathAndClick(page, btnAvantiSelector);
      await waitFormLoading(page, logger, {
        xPathPre,
        xPathPost
      });

      toastErrorMessages = await hasToastError(page, logger);
      if (toastErrorMessages == false) {
        await waitForXPathAndClick(page, btnAvantiSelector);
      } else if (toastErrorMessages) {
        throw new TryOrThrowError(toastErrorMessages);
      }
    }
  }, `Errore durante il verify-and-next per il bottone '${verifyButtonLabel}'`);
}
