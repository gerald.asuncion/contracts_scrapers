import { Page } from "puppeteer";
import inputText, { getInputText } from "../../../../browser/salesforce/lightining-components/controls/input";
import retryUntilValue from "../../../../browser/salesforce/lightining-components/helpers/retryUntilValue";
import { Logger } from "../../../../logger";
import delay from "../../../../utils/delay";
import tryOrThrow from "../../../../utils/tryOrThrow";

export default async function(page: Page, logger: Logger, label: string, cap: string, inputOptions: Parameters<typeof getInputText>[2]): Promise<void> {
  await tryOrThrow(async () => {
    await page.keyboard.press('Tab');
    await delay(250);

    if (await getInputText(page, label, inputOptions) !== cap) {
      await retryUntilValue(async () => {
        await inputText(page, logger, label, cap, {
          ...inputOptions,
          clearSelection: true
        });

        await delay(250);

        return getInputText(page, label, inputOptions);
      }, {
        mustMatch: cap
      });
    }
  }, `Errore durante l'inserimento CAP per il campo '${label}'`);
}
