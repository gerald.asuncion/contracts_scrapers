import { Page } from "puppeteer";
import isXPathVisible from "../../../../browser/page/isXPathVisible";
import { expressionContainsClass } from "../../../../browser/salesforce/lightining-components/helpers/selectors";
import { Logger } from "../../../../logger";

export default async function(page: Page, logger: Logger, timeout = 1000): Promise<string|false|null> {
  // const toastErrorMessageSelector = `//div${expressionContainsClass('toastContainer')}//div${expressionContainsClass('toastTitle')}${expressionContainsText('ERROR')}/parent::div/span${expressionContainsClass('toastMessage')}`;
  const toastErrorMessageSelector = `//div${expressionContainsClass('toastContainer')}/div[@data-key="error"]/div${expressionContainsClass('toastContent')}`;

  if (await isXPathVisible(page, toastErrorMessageSelector, timeout)) {
    const toastErrorMessageHandlerList = await page.$x(toastErrorMessageSelector);

    const errorMessages = (await Promise.all(
      toastErrorMessageHandlerList
      .map(async (toastErrorMessageHandler) => {
        const toastErrorMessage = await toastErrorMessageHandler.evaluate((elHandler) => [...elHandler.querySelectorAll('.toastTitle, .toastMessage')].map(el => el.textContent).join(': '));

        if (toastErrorMessage != null && typeof toastErrorMessage !== 'undefined') {
          return toastErrorMessage.trim();
        }
      })
    ))
    .filter(Boolean);

    if (errorMessages.length === 1 && (errorMessages[0] || '')!.toLowerCase() === 'ERROR: Salvare i dati per proseguire'.toLowerCase()) {
      return false;
    }

    if (errorMessages.length) {
      return errorMessages.join('\n');
    }
  }

  return null;
}
