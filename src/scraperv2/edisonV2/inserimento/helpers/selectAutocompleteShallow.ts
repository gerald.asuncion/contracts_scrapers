import { ControlHasNoAllowedValuesError } from "../../../../browser/errors/controls/controlHasNoAllowedValuesError";
import { CollateBestMatchError } from "../../../../browser/errors/methods/collateBestMatchError";
import selectAutocomplete from "../../../../browser/salesforce/lightining-components/controls/autocomplete";
import retryUntilValue from "../../../../browser/salesforce/lightining-components/helpers/retryUntilValue";
import tryOrThrow, { TryOrThrowError } from "../../../../utils/tryOrThrow";

export default async function(...args: Parameters<typeof selectAutocomplete>) {
  await tryOrThrow(async () => {
    await retryUntilValue(async () => {
      try {
        await selectAutocomplete(...args);
      } catch (exc) {
        if (!(exc instanceof TryOrThrowError && exc.hasSource(CollateBestMatchError, ControlHasNoAllowedValuesError))) {
          // altri errori sono dovuti ai controlli del portale quindi eseguiamo una serie di retry limitata
          return '';
        }
      }

      return true;
    }, {
      maxRetry: 6
    });
  }, `Autocomplete campo '${args[2]}'`);
}
