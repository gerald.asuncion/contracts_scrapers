import waitForXPathAndClick from "../../../browser/page/waitForXPathAndClick";
import checkRadioGroup, { radioGroupContainerExpression, radioGropuInputSelector } from "../../../browser/salesforce/lightining-components/controls/radiogroup";
import waitFormLoading from "../../../browser/salesforce/lightining-components/helpers/loading";
import { StepContext } from "../../../browser/salesforce/lightining-components/helpers/steps";
import delay from "../../../utils/delay";
import tryOrThrow from "../../../utils/tryOrThrow";
import { EdisonInserimentoV2Payload } from "../payload/EdisonInserimentoV2Payload";
import verifyAndNext from "./helpers/verifyAndNext";
import isXPathVisible from "../../../browser/page/isXPathVisible";
import waitForTimeout from "../../../browser/page/waitForTimeout";
import isXPathDisabled from "../../../browser/page/isXPathDisabled";

const CONFERMA_INFORMATIVA_PRIVACY_LABEL = 'Conferma accettazione informativa sulla privacy';

export default async function stepAccettazioneAssistenzaCasa({ page, logger }: StepContext, payload: EdisonInserimentoV2Payload): Promise<void> {
  const {
    informativaSullaPrivacy,
    consenso1,
    consenso2,
    consenso3
  } = payload;

  await waitForTimeout(page, 8000);
  if (await isXPathVisible(page, '//h2//span[contains(., "Accettazioni Assistenza Casa")]')) {
    await waitForTimeout(page, 4000);

    const informativaSullaPrivacyValue = informativaSullaPrivacy ? 'Si' : 'No';
    if (!(await isXPathDisabled(page, radioGropuInputSelector(radioGroupContainerExpression(CONFERMA_INFORMATIVA_PRIVACY_LABEL), informativaSullaPrivacyValue)))) {
      await tryOrThrow(async () => {
        await checkRadioGroup(page, CONFERMA_INFORMATIVA_PRIVACY_LABEL, informativaSullaPrivacyValue);
        await checkRadioGroup(page, 'Esprime il suo consenso al trattamento per finalità di marketing e commerciali?', consenso1 ? 'Si' : 'No');
        await checkRadioGroup(page, 'Esprime il suo consenso alla comunicazione dei suoi dati a terzi per finalità di marketing e commerciali dei propri prodotti e servizi?', consenso2 ? 'Si' : 'No');
        await checkRadioGroup(page, 'Conferma accettazione Consenso di Profilazione', consenso3 ? 'Si' : 'No');

        // await waitForXPathAndClick(page, '//button[@type="button"][contains(.,"Verifica dati")]');
        // await delay(500);

        // await waitForXPathAndClick(page, '//button[@type="button"][. = "Avanti"]');

        // await waitFormLoading(page, logger);
      }, "Errore durante la sezione informativa privacy.");

      await verifyAndNext(page, logger, 'Verifica dati', {
        excludeErrors: ['Salvare i dati per proseguire']
      });

      await delay(1000);
      await waitFormLoading(page, logger);
      await delay(2000);

      // TODO: check toaster errors

      await tryOrThrow(async () => {
        await waitForXPathAndClick(page, '//button[@type="button"][. = "Avanti"]');
      }, "Errore durante la sezione assistenza casa.");
    }
  }
}
