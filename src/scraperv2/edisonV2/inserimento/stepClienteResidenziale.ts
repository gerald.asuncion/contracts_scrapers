import waitForSelectorAndClick from "../../../browser/page/waitForSelectorAndClick";
import inputText from "../../../browser/salesforce/lightining-components/controls/input";
import { StepContext } from "../../../browser/salesforce/lightining-components/helpers/steps";
import tryOrThrow from "../../../utils/tryOrThrow";
import { EdisonInserimentoV2Payload } from "../payload/EdisonInserimentoV2Payload";
import verifyAndNext from "./helpers/verifyAndNext";
import waitForXPathAndType from "../../../browser/page/waitForXPathAndType";

export default async function stepClienteResidenziale({ page, logger }: StepContext, payload: EdisonInserimentoV2Payload): Promise<void> {
  await tryOrThrow(async () => {
    const {
      nome,
      cognome,
      codiceFiscale,
    } = payload;

    await waitForSelectorAndClick(page, 'lightning-primitive-icon + span[title="Anagrafica Cliente"]');

    await inputText(page, logger, 'Nome', nome);
    await inputText(page, logger, 'Cognome', cognome);

    await inputText(page, logger, 'Codice Fiscale', codiceFiscale);
  }, "Errore durante la sezione Anagrafica Cliente.");

  await tryOrThrow(async () => {
    const {
      cellularePrincipale,
      email
    } = payload;

    await inputText(page, logger, 'Cellulare Principale', cellularePrincipale);

    await inputText(page, logger, 'Email', email, { equals: true });
    // await inputText(page, logger, 'Conferma Email', email, {
    //   xPathPre: '//h2/span[contains(., "Dati Contatto")]/ancestor::article[1]'
    // });
    await waitForXPathAndType(page, '//input[@name="checkEmail"]', email);
  }, "Errore durante la sezione Contatti Cliente.");

  await verifyAndNext(page, logger, 'Verifica Dati Cliente');
}
