import waitForXPathAndClick from "../../../browser/page/waitForXPathAndClick";
import checkRadioGroup from "../../../browser/salesforce/lightining-components/controls/radiogroup";
import waitFormLoading from "../../../browser/salesforce/lightining-components/helpers/loading";
import { StepContext } from "../../../browser/salesforce/lightining-components/helpers/steps";
import delay from "../../../utils/delay";
import tryOrThrow from "../../../utils/tryOrThrow";
import { EdisonInserimentoV2Payload } from "../payload/EdisonInserimentoV2Payload";
import verifyAndNext from "./helpers/verifyAndNext";

const baseConfigCombobox = {
  valueProperty: 'data-value'
};

export default async function stepConsensi({ page, logger }: StepContext, payload: EdisonInserimentoV2Payload): Promise<void> {
  await tryOrThrow(async () => {
    const {
      informativaSullaPrivacy,
      consenso1,
      consenso2,
      consenso3
    } = payload;

    await checkRadioGroup(page, 'Conferma accettazione informativa sulla privacy', informativaSullaPrivacy ? 'Si' : 'No');
    await checkRadioGroup(page, 'Esprime il suo consenso al trattamento per finalità di marketing e commerciali?', consenso1 ? 'Si' : 'No');
    await checkRadioGroup(page, 'Esprime il suo consenso alla comunicazione dei suoi dati a terzi per finalità di marketing e commerciali dei propri prodotti e servizi?', consenso2 ? 'Si' : 'No');
    await checkRadioGroup(page, 'Conferma accettazione Consenso di Profilazione', consenso3 ? 'Si' : 'No');

    // await waitForXPathAndClick(page, '//button[@type="button"][contains(.,"Verifica dati")]');
    // await delay(500);

    // await waitForXPathAndClick(page, '//button[@type="button"][. = "Avanti"]');

    // await waitFormLoading(page, logger);
  }, "Errore durante la sezione informativa privacy.");

  await verifyAndNext(page, logger, 'Verifica dati');

  await delay(1000);
  await waitFormLoading(page, logger);
  await delay(2000);

  // TODO: check toaster errors

  await tryOrThrow(async () => {
    await waitForXPathAndClick(page, '//button[@type="button"][. = "Avanti"]');
  }, "Errore durante la sezione assistenza casa.");
}
