import { Page } from "puppeteer";
import isXPathVisible from "../../../browser/page/isXPathVisible";
import waitForXPathAndClick from "../../../browser/page/waitForXPathAndClick";
import checkCheckbox from "../../../browser/salesforce/lightining-components/controls/checkbox";
import { selectComboboxV2 } from "../../../browser/salesforce/lightining-components/controls/combobox";
import inputText, { inputTextElementDisabledExpression } from "../../../browser/salesforce/lightining-components/controls/input";
import selectSelect from "../../../browser/salesforce/lightining-components/controls/select";
import { StepContext } from "../../../browser/salesforce/lightining-components/helpers/steps";
import delay from "../../../utils/delay";
import tryOrThrow from "../../../utils/tryOrThrow";
import { EdisonInserimentoV2Payload } from "../payload/EdisonInserimentoV2Payload";
import insertCap from "./helpers/insertCap";
import selectAutocompleteShallow from "./helpers/selectAutocompleteShallow";
import verifyAndNext from "./helpers/verifyAndNext";
import waitForTimeout from "../../../browser/page/waitForTimeout";
import aggiungiVas from "./aggiungiVas";
import waitForXPathAndType, { InputMode } from "../../../browser/page/waitForXPathAndType";

async function vaiAvanti(page: Page, maxRetry = 3): Promise<void> {
  if (maxRetry <= 0) {
    return;
  }

  await waitForTimeout(page, 6000);

  if (!(await isXPathVisible(page, inputTextElementDisabledExpression('Subagenzia', true), 4000))) {
    return;
  }

  if (await isXPathVisible(page, '//button[@type="button"][. = "Avanti"]')) {
    await waitForXPathAndClick(page, '//button[@type="button"][. = "Avanti"]');
  }

  await vaiAvanti(page, maxRetry - 1);
}

export default async function stepDatiFornituraCliente({ page, logger }: StepContext, payload: EdisonInserimentoV2Payload): Promise<void> {
  const {
    recapitoUgualeFornitura,
    recapitoComune,
    recapitoIndirizzo,
    recapitoCivico,
    recapitoCap,
    recapitoProvincia,
    recapitoScala
  } = payload;

  // attendiamo che venga caricata la scfaccimma
  await tryOrThrow(() => page.waitForSelector('article.cDatiFornituraSezioneResidenzialeComponent', { timeout: 60000 }), 'la pagina `dati fornitura cliente` non è apparsa in tempo');

  await tryOrThrow(async () => {
    if (recapitoUgualeFornitura) {
      await waitForXPathAndClick(page, '//label/span[contains(.,"Ind. Spedizione uguale Ind. Fornitura")]');
    } else {
      const controlsOptions = {
        xPathPre: '//article[@data-aura-class="cDatiFornituraIndirizzoRecapitoBolletta"]'
      };
      const autocompleteOptions = {
        ...controlsOptions,
        scraper: 'edison' as any,
        delayItemsSelector: 3000
      };

      await selectAutocompleteShallow(page, logger, 'Comune Spedizione', recapitoComune!, autocompleteOptions);
      await waitForXPathAndType(page, '//article[contains(@class, "cDatiFornituraIndirizzoRecapitoBolletta")]//label[contains(., "Indirizzo Spedizione")]/following-sibling::div//input', recapitoIndirizzo!, InputMode.SelectFullLine);

      await inputText(page, logger, 'Civico Spedizione', recapitoCivico!, controlsOptions);

      await insertCap(page, logger, 'CAP Spedizione', recapitoCap!, controlsOptions);
      await selectComboboxV2(page, logger, 'Provincia Spedizione', recapitoProvincia!, controlsOptions);
      if (recapitoScala) {
        await inputText(page, logger, 'Scala Spedizione', recapitoScala, controlsOptions);
      }

      await waitForXPathAndType(page, '//article[contains(@class, "cDatiFornituraIndirizzoRecapitoBolletta")]//label[contains(., "Indirizzo Spedizione")]/following-sibling::div//input', recapitoIndirizzo!, InputMode.SelectFullLine);
    }
  }, "Errore durante la sezione indirizzo recapito bolletta.");

  // comunicazioni
  await tryOrThrow(async () => {
    const {
      canaleDicontattoPreferito,
      fasciaOrarioDiRicontatto,
      canaleInvioComunicazionePreferito,
      modalitaConfermaAdesione
    } = payload;

    await selectComboboxV2(page, logger, 'Canale di Contatto Preferito', canaleDicontattoPreferito);
    await selectComboboxV2(page, logger, 'Fascia Oraria di Ricontatto', fasciaOrarioDiRicontatto);
    await selectComboboxV2(page, logger, 'Canale Invio Comunicazione Preferito', canaleInvioComunicazionePreferito);
    await selectComboboxV2(page, logger, 'Modalità conferma adesione', modalitaConfermaAdesione);
  }, "Errore durante la sezione comunicazioni.");

  // modalità di pagamento
  await tryOrThrow(async () => {
    const {
      modalitaDiPagamento,
      sepaIban,
      bollettaElettronica,
      intestatarioContodiverso
    } = payload;

    await selectSelect(page, logger, 'Modalità di Pagamento', modalitaDiPagamento);
    await delay(250);

    switch (modalitaDiPagamento) {
      case 'Bollettino_Postale': break;
      case 'RID':
        await inputText(page, logger, 'Codice IBAN', sepaIban);

        if (intestatarioContodiverso) {
          const {
            nomeIntestatarioconto,
            cognomeIntestatarioConto,
            codicefiscaleIntestatarioConto
          } = payload;

          await checkCheckbox(page, 'Titolare C/C diverso Intestatario/Rappr');

          await inputText(page, logger, 'Nome Intestatario Conto', nomeIntestatarioconto);
          await inputText(page, logger, 'Cognome Intestatario Conto', cognomeIntestatarioConto);
          await inputText(page, logger, 'Codice Fiscale Intestatario Conto', codicefiscaleIntestatarioConto);
        }
        // goto next
      case 'Carta_Di_Credito':
        if (bollettaElettronica) {
          await checkCheckbox(page, 'Bolletta Elettronica');
        }
        break;
    }
  }, "Errore durante la sezione modalità di pagamento.");

  // info contratto
  await tryOrThrow(async () => {
    const {
      subagenzia,
    } = payload;

    await inputText(page, logger, 'Subagenzia', subagenzia);
  }, "Errore durante la sezione info contratto.");

  // invio comunicazione
  await tryOrThrow(async () => {
    const {
      modalitaInvioLinkform
    } = payload;

    await checkCheckbox(page, 'AttivaLinkForm');
    await delay(250);

    await checkCheckbox(page, modalitaInvioLinkform.toLowerCase() === 'sms' ? 'Invio Link per SMS' : 'Invio Link per Email');
  }, "Errore durante la sezione invio comunicazione.");


  if (payload.vasAcquistato) {
    await verifyAndNext(page, logger, 'Verifica Dati', {
      noNextClick: true,
      // nextClickAwaitingSelector: 'article.cDatiFornituraSezioneResidenzialeComponent'
    });
    // aggiungo il vas
    await tryOrThrow(() => aggiungiVas(page, logger, payload), 'non sono riuscito ad aggiungere il vas:');
    // fine aggiunta del vas
    await verifyAndNext(page, logger, 'Verifica Dati', {
      nextClickAwaitingSelector: 'article.cDatiFornituraSezioneResidenzialeComponent'
    });
  } else {
    await verifyAndNext(page, logger, 'Verifica Dati', {
      nextClickAwaitingSelector: 'article.cDatiFornituraSezioneResidenzialeComponent'
    });
  }

  // finito lo spinner la modale ci mette molto per ricrearsi quindi i 4s ci stanno tutti

  await vaiAvanti(page);
}
