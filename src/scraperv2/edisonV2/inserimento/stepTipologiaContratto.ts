import { selectComboboxV2 } from "../../../browser/salesforce/lightining-components/controls/combobox";
import inputDatepicker from "../../../browser/salesforce/lightining-components/controls/datepicker";
import waitFormLoading from "../../../browser/salesforce/lightining-components/helpers/loading";
import { StepContext } from "../../../browser/salesforce/lightining-components/helpers/steps";
import { EdisonInserimentoV2Payload } from "../payload/EdisonInserimentoV2Payload";
import tryOrThrow from "../../../utils/tryOrThrow";

export default async function stepTipologiaContratto({ page, logger }: StepContext, payload: EdisonInserimentoV2Payload): Promise<void> {
  const {
    tipoCliente,
    tipoOfferta,
    dataFirma,
    // accordoQuadro,
    serviceType // => modalità vendita
  } = payload;

  await tryOrThrow(
    () => selectComboboxV2(page, logger, 'Tipo Cliente', tipoCliente),
    'non sono riuscito a selezionare il `Tipo di cliente`:'
  );

  await tryOrThrow(
    () => selectComboboxV2(page, logger, 'Tipo Offerta', tipoOfferta),
    'non sono riuscito a selezionare il `Tipo di Offerta`:'
  );
  await inputDatepicker(page, logger, 'Data Firma', dataFirma);

  await page.keyboard.press('Tab');

  await waitFormLoading(page, logger);

  // await selectComboboxV2(page, logger, 'Accordo Quadro', accordoQuadro);
  // await selectComboboxV2(page, logger, 'Modalità Vendita', serviceType);
  await selectComboboxV2(page, logger, 'Modalità ', serviceType); //, '', { equals: true });
}
