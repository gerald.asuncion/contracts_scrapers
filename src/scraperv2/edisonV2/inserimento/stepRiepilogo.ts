import waitForXPathAndClick from "../../../browser/page/waitForXPathAndClick";
import waitForXPathAndClickEvaluated from "../../../browser/page/waitForXPathAndClickEvaluated";
import waitForXPathVisible from "../../../browser/page/waitForXPathVisible";
import waitFormLoading from "../../../browser/salesforce/lightining-components/helpers/loading";
import { expressionContainsClass, expressionContainsText } from "../../../browser/salesforce/lightining-components/helpers/selectors";
import { StepContext } from "../../../browser/salesforce/lightining-components/helpers/steps";
import delay from "../../../utils/delay";
import tryOrThrow from "../../../utils/tryOrThrow";
import { EdisonInserimentoV2Payload } from "../payload/EdisonInserimentoV2Payload";
import checkNoSubmit from "../../../utils/checkNoSubmit";

export default async function stepRiepilogo({ page, logger, result }: StepContext, payload: EdisonInserimentoV2Payload): Promise<void> {
  checkNoSubmit(payload);
  await tryOrThrow(() => Promise.race([
    waitForXPathVisible(page, `//article${expressionContainsClass('cRiepilogoComponent')}//header//h2/span${expressionContainsText('Riepilogo')}`),
    waitForXPathVisible(page, '//lightning-button-menu//span[. = "Mostra più azioni"]')
  ]), 'la pagina di riepilogo non si è caricata in tempo');

  await tryOrThrow(async () => {
    const [elHandlerContratto] = await page.$x('//header//div[contains(text(),"Inserimento Contratto")]');
    const contrattoText = await page.evaluate(name => name.innerText, elHandlerContratto);
    const idContratto = contrattoText.split(' ')[contrattoText.split(' ').length - 1];

    Object.assign(result, {
      idContratto
    });

    await waitForXPathAndClick(page, '//button[@type="button"][. = "Avanti"]');

    await waitFormLoading(page, logger);
  }, "Errore durante la sezione riepilogo.");

  await tryOrThrow(async () => {
    await waitForXPathVisible(page, '//lightning-button-menu//span[. = "Mostra più azioni"]');
    await delay(1000);

    await waitForXPathAndClickEvaluated(page, '//lightning-button-menu//span[. = "Mostra più azioni"]');
    await delay(500);
    await waitForXPathAndClickEvaluated(page, '//runtime_platform_actions-action-renderer[@title="Invia Contratto al WEB"]//a');

    await waitFormLoading(page, logger);
    await delay(3000);

    // TODO: get success message

    // TODO intercept message inside 'div.forceToastManager'
    Object.assign(result, {
      completed: true
    });
  }, "Errore durante la sezione invia contratto.");
}
