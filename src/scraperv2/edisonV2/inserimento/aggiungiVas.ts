import { Page } from "puppeteer";
import { EdisonInserimentoV2Payload } from "../payload/EdisonInserimentoV2Payload";
import waitForTimeout from "../../../browser/page/waitForTimeout";
import waitForXPathAndClick from "../../../browser/page/waitForXPathAndClick";
import waitForVisibleStyled from "../../../browser/page/waitForVisibleStyled";
import waitForVisible from "../../../browser/page/waitForVisible";
import tryOrThrow, { TryOrThrowError } from "../../../utils/tryOrThrow";
import waitForSelectorAndSelect from "../../../browser/page/waitForSelectorAndSelect";
import waitForSelectorAndClick from "../../../browser/page/waitForSelectorAndClick";
import selectSelect, { selectV2 } from "../../../browser/salesforce/lightining-components/controls/select";
import { Logger } from "../../../logger";

const VAS_EDISON: Record<string, string> = {
  'casa relax luce': 'Edison Casa Relax Luce +',
  'casa relax gas': 'Edison Casa Relax Gas +'
};

export default async function aggiungiVas(page: Page, logger: Logger, payload: EdisonInserimentoV2Payload) {
  const { vasAcquistato, tipoAbitazione, impiantoConforme } = payload;

  await waitForTimeout(page, 4000);

  await tryOrThrow(() => waitForXPathAndClick(page, '//button[contains(., "Aggiungi VAS")]'), 'non sono riuscito a cliccare sul pulsante `aggiungi vas`');

  await waitForTimeout(page, 10000);

  await tryOrThrow(() => waitForVisibleStyled(page, '.uiContainerManager > .uiModal.open > .panel'), 'la modale non è apparsa in tempo');

  await tryOrThrow(async () => {
    const selectFornituraSelector = 'select[name="Fornitura"]';
    await waitForVisible(page, selectFornituraSelector);
    const optionToSelect = await page.evaluate((parentSelector) => {
      return (Array.from(document.querySelectorAll(`${parentSelector} option`)).filter(item => item.textContent?.toLowerCase().trim() !== '--none--')[0] as HTMLOptionElement)?.value;
    }, selectFornituraSelector);

    if (!optionToSelect) {
      throw new TryOrThrowError("valore fornitura da selezionare non trovato");
    }

    await waitForSelectorAndSelect(page, selectFornituraSelector, [optionToSelect]);
  }, 'non sono riuscito a selezionare la fornitura:');

  await waitForTimeout(page, 10000);

  await tryOrThrow(() => waitForSelectorAndClick(page, 'button[title="Seleziona VAS"]'), 'non sono riuscito a cliccare sul pulsante `seleziona vas`');

  await waitForTimeout(page, 10000);

  await tryOrThrow(() => selectV2(page, 'select[name="VAS Acquistato"]', VAS_EDISON[vasAcquistato]), 'non sono riuscito a selezionare il vas acquistato');

  await waitForTimeout(page, 10000);

  await tryOrThrow(() => selectV2(page, 'select[name="Modalità di fatturazione"]', 'Fee'), 'non sono riuscito a selezionare la modalità di fatturazione');

  await waitForTimeout(page, 10000);

  await tryOrThrow(async () => {
    const selectImportoFeeSelector = 'select[name="Importo Fee"]';
    await waitForVisible(page, selectImportoFeeSelector);
    const importoDaSelezionare = await page.evaluate((parentSelector) => {
      return (Array.from(document.querySelectorAll(`${parentSelector} option`)).filter(item => item.textContent?.toLowerCase().trim() !== '---select---')[0] as HTMLOptionElement)?.value;
    }, selectImportoFeeSelector);
    if (!importoDaSelezionare) {
      throw new TryOrThrowError("valore dell'importo di fee da selezionare non trovato");
    }

    await waitForSelectorAndSelect(page, selectImportoFeeSelector, [importoDaSelezionare]);
  }, "non sono riuscito a selezionare l'importo della fatturazione:");

  await waitForTimeout(page, 10000);
  await tryOrThrow(() => selectSelect(page, logger, 'Tipo Abitazione', tipoAbitazione as string), 'non sono riuscito a selezionare il tipo di abitazione');
  await tryOrThrow(() => selectSelect(page, logger, 'Impianto conforme', impiantoConforme === '1' ? 'SI' : (impiantoConforme || 'NO')), "non sono riuscito ad inserire se l'impianto è conforme");

  await tryOrThrow(() => waitForSelectorAndClick(page, 'button[title="Verifica dati"]'), 'non sono riuscito a cliccare sul pulsante `verifica dati`');
}
