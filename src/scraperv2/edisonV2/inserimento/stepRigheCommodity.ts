import isXPathVisible from "../../../browser/page/isXPathVisible";
import waitForTimeout from "../../../browser/page/waitForTimeout";
import waitForXPathAndClick from "../../../browser/page/waitForXPathAndClick";
import waitForXPathAndClickEvaluated from "../../../browser/page/waitForXPathAndClickEvaluated";
import waitForXPathAndRun from "../../../browser/page/waitForXPathAndRun";
import selectAutocomplete from "../../../browser/salesforce/lightining-components/controls/autocomplete";
import { comboboxContainerExpression, selectComboboxV2 } from "../../../browser/salesforce/lightining-components/controls/combobox";
import inputText, { getInputText } from "../../../browser/salesforce/lightining-components/controls/input";
import selectSelect, { selectV2 } from "../../../browser/salesforce/lightining-components/controls/select";
import waitFormLoading from "../../../browser/salesforce/lightining-components/helpers/loading";
import retryUntilValue from "../../../browser/salesforce/lightining-components/helpers/retryUntilValue";
import { expressionContainsClass } from "../../../browser/salesforce/lightining-components/helpers/selectors";
import { StepContext } from "../../../browser/salesforce/lightining-components/helpers/steps";
import delay from "../../../utils/delay";
import tryOrThrow, { TryOrThrowError } from "../../../utils/tryOrThrow";
import { EdisonInserimentoV2Payload } from "../payload/EdisonInserimentoV2Payload";
import forTheThirdTimeChangedCheckbox from "./helpers/forTheThirdTimeChangedCheckbox";
import hasToastError from "./helpers/hasToastError";
import insertCap from "./helpers/insertCap";
import selectAutocompleteShallow from "./helpers/selectAutocompleteShallow";
import verifyAndNext from "./helpers/verifyAndNext";
import saveScreenshotOnAwsS3 from "../../../browser/page/saveScreenshotOnAwsS3";
import { S3ClientFactoryResult } from "../../../aws/s3ClientFactory";
import createFilenameForScreenshot from "../../../utils/createFilenameForScreenshot";
import extractErrorMessage from "../../../utils/extractErrorMessage";
import waitForXPathAndType, { InputMode } from "../../../browser/page/waitForXPathAndType";

const baseConfigCombobox = {
  valueProperty: 'data-value'
};

const SCREENSHOT_FILENAME = 'EdisonInserimentoV2.stepRigheCommodity';

export default function stepRigheCommodityFactory(s3ClientFactory: S3ClientFactoryResult) {
  return async function stepRigheCommodity({ page, logger }: StepContext, payload: EdisonInserimentoV2Payload): Promise<void> {
    await waitForXPathAndClick(page, '//button[@type="button"][. = "Aggiungi Commodity"]');
    await delay(2500);

    const {
      tipoOfferta,
      tipoAttivazione
    } = payload;

    const isLuce = tipoOfferta == 'Elettricita';
    const isSwitchIn = tipoAttivazione == 'Switch_In';

    const {
      codicePod,
      codicePdr,
      ripensamento,
      mercatoInteresse
    } = payload;

    if (isLuce) {
      await retryUntilValue(async () => {
        await inputText(page, logger, 'POD', codicePod!, {
          clearSelection: true
        });

        return await getInputText(page, 'POD');
      }, {
        mustMatch: codicePod
      });
    } else {
      await retryUntilValue(async () => {
        await inputText(page, logger, 'PdR', codicePdr!, {
          clearSelection: true
        });

        return await getInputText(page, 'PdR');
      }, {
        mustMatch: codicePdr
      });
    }

    const hndlrTipiAttivazioni = await page.waitForXPath('//select[@name="tipiAttivazioni"]');
    if ((await (await hndlrTipiAttivazioni?.getProperty('value'))?.jsonValue()) !== tipoAttivazione) {
      await hndlrTipiAttivazioni?.select(tipoAttivazione);

      const formLoadingOptions = {
        xPathPost: `[not(${expressionContainsClass('cDatiFornituraRowsDatatableComponent', false)})]`
      }
      await waitFormLoading(page, logger, formLoadingOptions);
    }

    if (ripensamento) {
      await forTheThirdTimeChangedCheckbox(page, logger, 'Flag_Ripensamento');
    }

    if (!isLuce) {
      await selectComboboxV2(page, logger, 'Mercato di Interesse', mercatoInteresse!);
    }

    // dati fornitura

    if (isSwitchIn) {
      await tryOrThrow(async () => {
        const {
          attualeFornitoreLuce,
          mercatoProvenienzaLuce,
          attualeFornitoreGas,
          mercatoProvenienzaGas
        } = payload;

        const additionalConfigInput = {
          xPathPre: '//div[@title="Fornitore Attuale"]'
        };

        await selectAutocomplete(page, logger, 'Fornitore Attuale', isLuce ? attualeFornitoreLuce! : attualeFornitoreGas!, { scraper: 'edison', ...additionalConfigInput });
        await selectComboboxV2(page, logger, 'Mercato di Provenienza', isLuce ? mercatoProvenienzaLuce! : mercatoProvenienzaGas!, additionalConfigInput);
      }, "Errore durante la sezione Fornitore Attuale.");
    }

    await tryOrThrow(async () => {
      const {
        // luce
        spesaAnnoPrecedenteLuce,
        consumoUltimi12mesi,
        // gas
        // TODO: spesaAnnoPrecedenteGas,
        consumoAnnuo,
      } = payload;

      let spesaAnnoPrecedenteGas = 0;

      const additionalConfigInput = {
        xPathPre: '//div[@title="Consumo Spesa"]',
        clearSelection: true
      };

      await inputText(
        page, logger,
        isSwitchIn ? 'Spesa Anno Precedente' : 'Spesa Annua Stimata',
        (isLuce ? spesaAnnoPrecedenteLuce! : spesaAnnoPrecedenteGas!).toString(),
        additionalConfigInput
      );

      if (isLuce) {
        await inputText(
          page, logger,
          isSwitchIn ? 'Consumo Ultimi 12 Mesi' : 'Consumo Annuo Stimato (kWh)',
          consumoUltimi12mesi!.toString(),
          additionalConfigInput
        );
      } else {
        await inputText(
          page, logger,
          isSwitchIn ? 'Consumo Annuo (Smc)' : 'Consumo Annuo Stimato (Smc)',
          consumoAnnuo!.toString(),
          additionalConfigInput
        );
      }
    }, "Errore durante la sezione Consumo Spesa.");

    if (isLuce) {
      // sezione potenza
      //lightning-input//label[contains(., "Spesa Anno Precedente")]/ancestor::lightning-input//input["lightning-input_input"]

      await tryOrThrow(async () => {
        const {
          potDisponibile,
          potImpegnata,
          tipoTensione,
          usoEE,
          tensione,
        } = payload;

        const additionalConfigInput = {
          xPathPre: '//div[@title="Potenza"]'
        };
        const additionalConfigCombobox = {
          ...additionalConfigInput,
          ...baseConfigCombobox
        };

        if (potImpegnata) {
          await selectComboboxV2(page, logger, 'Potenza Impegnata (kW)', potImpegnata, additionalConfigCombobox);

          const potenzaDisponibile = potDisponibile || (
            potImpegnata === 'P4_5' ? '5,00' : String((parseFloat(potImpegnata.substr(1).replace('_', ',')) * 1.1).toFixed(2)).replace('.', ',')
          );
          await inputText(page, logger, 'Potenza Disponibile (kW)', potenzaDisponibile.toString(), {
            ...additionalConfigInput,
            clearSelection: true
          });
        }

        await selectComboboxV2(page, logger, 'Tipo Tensione', tipoTensione!, additionalConfigCombobox);
        await selectComboboxV2(page, logger, 'Tensione (V)', tensione!, additionalConfigCombobox);

        await selectComboboxV2(page, logger, 'Uso EE', usoEE!, additionalConfigCombobox);
      }, "Errore durante la sezione Potenza.");
    } else {
      const {
        categoriaUso
      } = payload;

      const additionalConfigInput = {
        xPathPre: '//div[@title="Potenza"]'
      };
      const additionalConfigCombobox = {
        ...additionalConfigInput,
        ...baseConfigCombobox
      };

      await selectComboboxV2(page, logger, 'Categoria Uso Gas', categoriaUso!, additionalConfigCombobox);
      // Tipologi Pdr => Cliente Domestico default

      if (!isSwitchIn) {
        const {
          classePrelievo,
          potenzialitaMassimaRichiesta
        } = payload;

        const attivazioniConfigInput = {
          xPathPre: '//div[@title="Nuove Attivazioni"]'
        };

        await selectComboboxV2(page, logger, 'Classe di Prelievo', `${(classePrelievo!)[0]} Giorni`, {
          ...attivazioniConfigInput,
          ...baseConfigCombobox
        });
        await inputText(page, logger, 'Potenzialità Massima Richiesta', potenzialitaMassimaRichiesta!, attivazioniConfigInput);
      }
    }

    const {
      titolaritaImmobile
    } = payload;

    await selectComboboxV2(page, logger, 'Titolarità Immobile', titolaritaImmobile, baseConfigCombobox);

    // indirizzo fornitura
    await tryOrThrow(async () => {
      const {
        attivazioneComune,
        attivazioneIndirizzo,
        attivazioneCivico,
        attivazioneCap,
        attivazioneProvincia,
        attivazioneScala,
        flagResidente
      } = payload;

      const indirizzoFornituraOptions = {
        xPathPre: '//article[@data-aura-class="cAddContractIndirizzoFornitura"]'
      };
      const indirizzoFornituraAutocompleteOptions = {
        ...indirizzoFornituraOptions,
        scraper: 'edison' as any,
        delayItemsSelector: 3000
      };

      await selectAutocompleteShallow(page, logger, 'Comune Fornitura', attivazioneComune, indirizzoFornituraAutocompleteOptions);
      await waitForXPathAndType(page, '//article[contains(@class, "cAddContractIndirizzoFornitura")]//label[contains(., "Indirizzo Fornitura")]/following-sibling::div//input', attivazioneIndirizzo, InputMode.SelectFullLine);

      await inputText(page, logger, 'Numero Civico Fornitura', attivazioneCivico, indirizzoFornituraOptions);

      await insertCap(page, logger, 'CAP Fornitura', attivazioneCap, indirizzoFornituraOptions);

      await selectComboboxV2(page, logger, 'Provincia Fornitura', attivazioneProvincia, indirizzoFornituraOptions);
      if (attivazioneScala) {
        const infoAggiuntiveOptions = {
          xPathPre: '//div[@title="Informazioni Aggiuntive"]'
        };

        await waitForXPathAndClickEvaluated(page, '//button[@type="button"][. = "Mostra Info Aggiuntive"]');

        await inputText(page, logger, 'Scala Fornitura', attivazioneScala, infoAggiuntiveOptions);
      }

      await waitForXPathAndType(page, '//article[contains(@class, "cAddContractIndirizzoFornitura")]//label[contains(., "Indirizzo Fornitura")]/following-sibling::div//input', attivazioneIndirizzo, InputMode.SelectFullLine);

      if (flagResidente) {
        await forTheThirdTimeChangedCheckbox(page, logger, 'Residente');
      }
    }, "Errore durante la sezione Indirizzo Attivazione.");


    // offerta -> mostra infor aggiuntive
    await tryOrThrow(async () => {
      const {
        offertaAcquistataLuce,
        offertaAcquistataGas,
        valore
      } = payload;

      const formLoadingOptions = {
        xPathPost: `[not(${expressionContainsClass('cDatiFornituraRowsDatatableComponent', false)})]`
      }

      await waitForXPathAndClick(page, '//button[@type="button"][@name="cercaOfferte"]');
      await waitFormLoading(page, logger, formLoadingOptions);

      await waitForTimeout(page, 3000);

      // TODO: luce/gas

      try {
        await selectV2(page, 'select[name="offerte"]', isLuce ? offertaAcquistataLuce! : offertaAcquistataGas!);
      } catch {
        await selectSelect(page, logger, 'Scegli offerta', isLuce ? offertaAcquistataLuce! : offertaAcquistataGas!, {
          valueNorText: false
        });
      }
      // await waitForXPathAndRun(page, '//div[@title="Offerta"]//select[@name="offerte"]', async (page, elHandler) => elHandler.select(isLuce ? offertaAcquistataLuce! : offertaAcquistataGas!));
      await waitFormLoading(page, logger, formLoadingOptions);

      if (isLuce && await isXPathVisible(page, '//div[@title="Offerta"]/parent::div//select[@name="valore"]', 1500)) {
        await waitForXPathAndRun(page, '//div[@title="Offerta"]/parent::div//select[@name="valore"]', async (page, elHandler) => {
          await elHandler?.select(valore!);
        });
      }
    }, "Errore durante la sezione Offerta.");

    // sezione Nuove Attivazioni
    await tryOrThrow(async () => {
      const {
        fase
      } = payload;

      const nuoveAttivazioniOptions = {
        xPathPre: `//div[@title="Nuove Attivazioni"]`
      }

      if (fase && await isXPathVisible(page, comboboxContainerExpression('Fase'))) {
        await selectComboboxV2(page, logger, 'Fase', fase!, nuoveAttivazioniOptions);
      }
    }, "Errore durante la sezione Nuove Attivazioni.");

    const verifyAndNextConfig = {
      xPathPre: '//div[@data-aura-class="cAddCommodityComponent"]'
    }

    try {
      await verifyAndNext(page, logger, 'Verifica Dati Riga', {
        ...verifyAndNextConfig,
        noNextClick: true,
      });
    } catch (ex) {
      const errMsg = extractErrorMessage(ex);
      if (!errMsg.includes('necessario immettere un valore per il codice REMI')) {
        throw ex;
      }
    }
    // await delay(1000);
    await waitForTimeout(page, 3000);

    let isRemiVisible = true;
    try {
      await page.waitForXPath('//select[@name="remi"]', {
        timeout: 4000
      })
    } catch {
      isRemiVisible = false;
    }

    if (isRemiVisible) {
      const [optionHandler] = await page.$x('//select[@name="remi"]/option[2]');
      const optionValue = (await (await optionHandler.getProperty('value')).jsonValue<string>());

      await (await page.$x('//select[@name="remi"]'))[0].select(optionValue);

      await saveScreenshotOnAwsS3(s3ClientFactory, page, createFilenameForScreenshot(SCREENSHOT_FILENAME, payload.idDatiContratto), logger);

      await verifyAndNext(page, logger, 'Verifica Dati Riga', verifyAndNextConfig);
    } else {
      let toastErrorMessages = await hasToastError(page, logger);
      if (toastErrorMessages) {
        await saveScreenshotOnAwsS3(s3ClientFactory, page, createFilenameForScreenshot(`${SCREENSHOT_FILENAME}-errore`, payload.idDatiContratto), logger);
        throw new TryOrThrowError(toastErrorMessages);
      } else {
        await saveScreenshotOnAwsS3(s3ClientFactory, page, createFilenameForScreenshot(SCREENSHOT_FILENAME, payload.idDatiContratto), logger);
      }
    }
  }
}
