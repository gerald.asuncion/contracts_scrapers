import EdisonFornitoriV2 from "./EdisonFornitoriV2";
import { ScraperResponse } from "../scraper";
import creaPayloadFinto from "../edison/fornitori/creaPayloadFinto";
import { QueueTask } from "../../task/QueueTask";

@QueueTask({ scraperName: 'edisonFornitoriLuce' })
export default class EdisonFornitoriLuceV2 extends EdisonFornitoriV2 {
  async scrapeWebsite(): Promise<ScraperResponse> {
    return super.scrapeWebsite(creaPayloadFinto() as any);
  }
}
