import EdisonFornitoriV2 from "./EdisonFornitoriV2";
import { ScraperResponse } from "../scraper";
import creaPayloadFinto from "../edison/fornitori/creaPayloadFinto";
import { QueueTask } from "../../task/QueueTask";

@QueueTask({ scraperName: 'edisonFornitoriGas' })
export default class EdisonFornitoriGasV2 extends EdisonFornitoriV2 {
  async scrapeWebsite(): Promise<ScraperResponse> {
    return super.scrapeWebsite(creaPayloadFinto('Gas') as any);
  }
}
