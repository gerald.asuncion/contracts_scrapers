import waitForSelectorAndClick from "../../../browser/page/waitForSelectorAndClick";
import inputText from "../../../browser/salesforce/lightining-components/controls/input";
import waitFormLoading from "../../../browser/salesforce/lightining-components/helpers/loading";
import { StepContext } from "../../../browser/salesforce/lightining-components/helpers/steps";
import { EdisonInserimentoV2Payload } from "../payload/EdisonInserimentoV2Payload";
import waitForXPathAndType from "../../../browser/page/waitForXPathAndType";
import waitForXPathAndClick from "../../../browser/page/waitForXPathAndClick";

export default async function stepClienteResidenzialePerFornitori({ page, logger }: StepContext, payload: EdisonInserimentoV2Payload): Promise<void> {
  const {
    nome,
    cognome,
    codiceFiscale,
  } = payload;

  await waitForSelectorAndClick(page, 'lightning-primitive-icon + span[title="Anagrafica Cliente"]');

  await inputText(page, logger, 'Nome', nome);
  await inputText(page, logger, 'Cognome', cognome);

  await inputText(page, logger, 'Codice Fiscale', codiceFiscale);

  const {
    cellularePrincipale,
    email
  } = payload;

  await inputText(page, logger, 'Cellulare Principale', cellularePrincipale);

  await inputText(page, logger, 'Email', email, { equals: true });
  await waitForXPathAndType(page, '//input[@name="checkEmail"]', email);

  await waitForXPathAndClick(page, '//button[contains(text(), "Verifica Dati Cliente")]');

  await waitFormLoading(page, logger);

  await waitForXPathAndClick(page, '//button[contains(text(), "Avanti")]');
}
