import { Page } from "puppeteer";
import { Logger } from "../../../logger";
import waitFormLoading from "../../../browser/salesforce/lightining-components/helpers/loading";
import waitForXPathAndClick from "../../../browser/page/waitForXPathAndClick";
import { EdisonInserimentoV2Payload } from "../payload/EdisonInserimentoV2Payload";

export default async function stepDatiFornitura(page: Page, logger: Logger, payload: EdisonInserimentoV2Payload) {
  await waitFormLoading(page, logger);
  await waitForXPathAndClick(page, '//button[contains(text(), "Aggiungi Commodity")]');
}
