import { Page } from 'puppeteer';
import waitCheckCredentials from '../../browser/page/waitCheckCredentials';
import waitForNavigation from '../../browser/page/waitForNavigation';
import waitForSelectorAndClick from '../../browser/page/waitForSelectorAndClick';
import waitForSelectorAndType from '../../browser/page/waitForSelectorAndType';
import waitFormLoading from '../../browser/salesforce/lightining-components/helpers/loading';
import executeSteps from '../../browser/salesforce/lightining-components/helpers/steps';
import FailureResponse from '../../response/FailureResponse';
import SuccessResponse from '../../response/SuccessResponse';
import createChildPayloadAwareLogger from '../../utils/createChildPayloadAwareLogger';
import tryOrThrow from '../../utils/tryOrThrow';
import { ScraperResponse, WebScraper } from '../scraper';
import stepClienteResidenziale from './inserimento/stepClienteResidenziale';
import stepConsensi from './inserimento/stepConsensi';
import stepDatiFornituraCliente from './inserimento/stepDatiFornituraCliente';
import stepRiepilogo from './inserimento/stepRiepilogo';
import stepRigheCommodityFactory from './inserimento/stepRigheCommodity';
import stepTipologiaContratto from './inserimento/stepTipologiaContratto';
import { EdisonInserimentoV2Payload } from './payload/EdisonInserimentoV2Payload';
import { EdisonLoginPayload } from './payload/EdisonLoginPayload';
import stepAccettazioneAssistenzaCasa from './inserimento/stepAccettazioneAssistenzaCasa';
import { SCRAPER_TIPOLOGIA } from '../ScraperOptions';
import { QueueTask } from '../../task/QueueTask';

@QueueTask({ scraperName: 'edison' })
export default class EdisonInserimentoV2<TPayload = EdisonInserimentoV2Payload> extends WebScraper<EdisonLoginPayload | TPayload> {
  static readonly LOGIN_URL = 'https://edisonenergia.my.salesforce.com/';

  async login({ ...restArgs }: EdisonLoginPayload): Promise<void> {
    const logger = createChildPayloadAwareLogger(this.childLogger, restArgs);
    const { username, password } = await this.getCredenziali();

    const page = await this.p();

    await Promise.all([
      page.goto(EdisonInserimentoV2.LOGIN_URL, { waitUntil: 'networkidle2' }),
      waitForNavigation(page)
    ]);

    await tryOrThrow(async () => {
      await waitForSelectorAndType(page, 'input[id="username"]', username);
      await waitForSelectorAndType(page, 'input[id="password"]', password);
    }, 'non sono riuscito ad inserire username e/o password.');
    logger.info('login > inserite credenziali');

    await Promise.all([
      waitForSelectorAndClick(page, 'input[id="Login"]'),
      page.waitForResponse(() => true)
    ]);

    await Promise.race([
      waitCheckCredentials(page, '#error', EdisonInserimentoV2.LOGIN_URL, username, logger, {
        timeout: 250
      }),
      waitForNavigation(page)
    ]);

    await page.waitForResponse(() => true);

    logger.info('login > terminata');
  }

  async navigaANuovoContratto(page: Page, payload: TPayload): Promise<void> {
    await Promise.all([
      waitForSelectorAndClick(page, 'a[title="Contratti"]'),
      page.waitForResponse(() => true),
      waitForNavigation(page)
    ]);

    await Promise.all([
      waitForSelectorAndClick(page, 'a > div[title="Nuovo"]'),
      page.waitForResponse(() => true),
      waitForNavigation(page)
    ]);
  }

  async scrapeWebsite(payload: TPayload): Promise<ScraperResponse> {
    const logger = createChildPayloadAwareLogger(this.childLogger, payload);
    const page = await this.p();

    await this.navigaANuovoContratto(page, payload);

    await waitFormLoading(page, logger);

    const [errors, result] = await executeSteps(page, logger, [
      stepTipologiaContratto,
      stepClienteResidenziale,
      stepRigheCommodityFactory(this.s3ClientFactory),
      stepDatiFornituraCliente,
      stepConsensi,
      stepAccettazioneAssistenzaCasa,
      stepRiepilogo
    ], () => Promise.resolve(null), payload);

    if (errors) {
      return new FailureResponse(errors);
    }
    return new SuccessResponse(result.idContratto);
  }

  getScraperCodice() {
    return 'edison';
  }

  getScraperTipologia() {
    return SCRAPER_TIPOLOGIA.INSERIMENTO;
  }
}
