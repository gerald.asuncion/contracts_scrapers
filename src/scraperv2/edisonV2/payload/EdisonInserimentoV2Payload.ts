import ContrattoPayload from "../../../contratti/ContrattoPayload";
import { EdisonPayload } from "../../edison/payload/EdisonPayload";

export type EdisonInserimentoV2Payload = Required<ContrattoPayload> & {
  /*
  compare per fornitura elettricità e tipo contratto subentro o allaccio su preposato.
  valori: Monofase
  */
  fase?: 'Monofase' | 'Trifase';
} & EdisonPayload;
