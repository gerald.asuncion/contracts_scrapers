import { Page } from 'puppeteer';
import { EdisonCheckFirmePayload } from './payload/EdisonCheckFirmePayload';
import { ScraperResponse } from '../scraper';
import createChildPayloadAwareLogger from '../../utils/createChildPayloadAwareLogger';
import waitForVisible from '../../browser/page/waitForVisible';
import tryOrThrow from '../../utils/tryOrThrow';
import { STATO_CONTRATTO } from '../../contratti/StatoContratto';
import waitForSelectorAndClick from '../../browser/page/waitForSelectorAndClick';
import GenericSuccessResponse from '../../response/GenericSuccessResponse';
import { Logger } from '../../logger';
import waitForXPathVisible from '../../browser/page/waitForXPathVisible';
import waitForXPathAndType from '../../browser/page/waitForXPathAndType';
import getElementText from '../../browser/page/getElementText';
import ContrattoPayload from '../../contratti/ContrattoPayload';
import EdisonInserimentoV2 from './EdisonInserimentoV2';
import { QueueTask } from '../../task/QueueTask';

const SEARCH_BTN_SELECTOR = 'button[aria-label^="Cerca"]';
const SEARCH_INPUT_SELECTOR = '//lightning-input//input';

async function checkFirmaContratto(page: Page, codiceContratto: string, logger: Logger): Promise<[string, string]> {
  await waitForSelectorAndClick(page, SEARCH_BTN_SELECTOR);
  await waitForXPathVisible(page, SEARCH_INPUT_SELECTOR);

  await page.evaluate((el) => (el as HTMLInputElement).value = '', (await page.$x(SEARCH_INPUT_SELECTOR))[0]);
  await waitForXPathAndType(page, SEARCH_INPUT_SELECTOR, codiceContratto);
  await page.keyboard.press('Enter');

  await waitForVisible(page, '.queryFeedbackHeader');

  logger.info('cercato contratto ' + codiceContratto);

  let text;
  try {
    text = await getElementText(page, '.listDisplays table > tbody > tr > td:nth-of-type(4)', 'textContent');
  } catch (ex) {
    // tabella non apparsa
    // faccio in modo che venga impostato lo stato 'DA FIRMARE' visto che non ho trovato il record
    logger.warn('tabella non apparsa');
    text = '';
  }

  const statoContratto = text?.toLowerCase() === 'inviato a crm' ? STATO_CONTRATTO.FIRMATO : STATO_CONTRATTO.DA_FIRMARE;

  logger.info(`Per il contratto ${codiceContratto} recuperato lo stato (edison) '${text}', convertito in '${statoContratto}'`);

  return [codiceContratto, statoContratto];
}

async function checkFirmaContratti(page: Page, codiciContratto: string[], logger: Logger): Promise<[string, string][]> {
  let result = [];
  const codiceContratto = codiciContratto.pop();
  if (codiceContratto) {
    result.push(await checkFirmaContratto(page, codiceContratto, logger));
  }

  if (codiciContratto.length) {
    result = result.concat(await checkFirmaContratti(page, codiciContratto, logger));
  }

  return result;
}

@QueueTask()
export default class EdisonCheckFirme extends EdisonInserimentoV2<EdisonCheckFirmePayload> {
  /**
   * @override
   * @param payload Payload per il check stato firme
   */
  async scrapeWebsite(payload: EdisonCheckFirmePayload & ContrattoPayload): Promise<ScraperResponse> {
    const logger = createChildPayloadAwareLogger(this.childLogger, payload);
    const page = await this.p();

    await tryOrThrow(() => waitForVisible(page, '.contentArea'), 'la pagina per controllare lo stato delle firme non si è caricata in tempo');

    const result = await checkFirmaContratti(page, payload.codici, logger);

    const mapped = result.reduce((acc, [codice, stato]) => {
      acc[codice] = stato;
      return acc;
    }, Object.create(null));

    return new GenericSuccessResponse<Record<string, string>>(mapped);
  }
}
