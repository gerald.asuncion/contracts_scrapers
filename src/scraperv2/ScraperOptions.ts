import { S3ClientFactoryResult } from '../aws/s3ClientFactory';
import BrowserManager from '../browser/BrowserManager';
import { Logger } from '../logger';
import { MysqlPool } from '../mysql';

export default interface ScraperOptions {
  browser: BrowserManager;
  logger: Logger;
  s3ClientFactory: S3ClientFactoryResult;
  credenzialiServiceBaseUrl: string;
  baseDownloadPath: string;
  certificatiClientBasePath: string;
}

export type ScraperWithDatabaseOptions = ScraperOptions & {
  mysqlPool: MysqlPool;
};

export const SCRAPER_TIPOLOGIA = {
  INSERIMENTO: 'inserimento',
  FATTIBILITA: 'fattibilita',
  CHECKFIRMA: 'check-firma',
  IMPORTDATA: 'import-data'
} as const;

export type ScraperTipologia = typeof SCRAPER_TIPOLOGIA[keyof typeof SCRAPER_TIPOLOGIA];
