/**
 * @openapi
 *
 * /scraper/v2/covercare/zone-disagiate/check:
 *  post:
 *    tags:
 *      - v2
 *    description: Controlla il cap passato col database per indicare se è in una zona disagiata o meno.
 *    consume:
 *      - application/json
 *    produces:
 *      - application/json
 *    responses:
 *      200:
 *        description: SuccessResponse
 *    parameters:
 *      - in: body
 *        description:
 *          bisogna passare il cap da controllare
 *        schema:
 *          type: object
 *          required:
 *            - cap
 *          properties:
 *            cap:
 *              type: string
 */
