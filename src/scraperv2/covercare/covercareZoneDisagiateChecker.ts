import { MysqlArgs, MysqlPoolInterface } from "../../mysql";
import { covercareZoneDisagiateRepo } from "../../repo/CovercareZoneDisagiateRepo";
import { ScraperResponse } from "../scraper";
import { Scraper } from "../types";
import ErrorResponse from "../../response/ErrorResponse";
import FailureResponse from "../../response/FailureResponse";
import SuccessResponse from "../../response/SuccessResponse";
import { SCRAPER_TIPOLOGIA } from "../ScraperOptions";
import { QueueTask } from "../../task/QueueTask";

@QueueTask()
export default class CovercareZoneDisagiateChecker implements Scraper {
  mysqlPool: MysqlPoolInterface;

  constructor(options: MysqlArgs) {
    this.mysqlPool = options.mysqlPool;
  }

  // serve solo per mantenere l'interfaccia Scraper comune agli altri
  async login() {
    return Promise.resolve();
  }

  async scrape({ /* comune, */ cap }: any): Promise<ScraperResponse> {
    let resp;

    try {
      const repo = covercareZoneDisagiateRepo({ mysqlPool: this.mysqlPool });

      const result = await repo.isNotUncomfortable(/* comune, */ cap);

      resp = result ? new SuccessResponse("OK") : new FailureResponse("KO");
    } catch (ex: any) {
      resp = new ErrorResponse(ex);
    }

    return Promise.resolve(resp);
  }

  getScraperCodice() {
    return 'covercare-zonedisagiate';
  }

  getScraperTipologia() {
    return SCRAPER_TIPOLOGIA.FATTIBILITA;
  }
}
