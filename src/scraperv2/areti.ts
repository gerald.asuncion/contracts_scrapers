import { ScraperResponse } from './scraper';
import { Scraper } from "./types";
import SuccessResponse from '../response/SuccessResponse';
import { MysqlPool } from '../mysql';
import ScraperOptions, { SCRAPER_TIPOLOGIA } from './ScraperOptions';
import extractErrorMessage from '../utils/extractErrorMessage';
import { wrapAretiResponseMessage } from './areti/helpers';
import ErrorResponse from '../response/ErrorResponse';

export default class Areti implements Scraper {
  private mysqlPool: MysqlPool;

  constructor({ mysqlPool }: ScraperOptions & { mysqlPool: MysqlPool }) {
    this.mysqlPool = mysqlPool;
  }

  async login(): Promise<void> { return Promise.resolve(); }

  async scrape({ pod }: { pod: string }): Promise<ScraperResponse> {
    const sql = 'SELECT * FROM areti WHERE `pod` = ? AND `contendibile` = ?';
    const args = [pod, 1];
    try {
      await this.mysqlPool.queryOne(sql, args);
    } catch (e) {
      const errMsg = extractErrorMessage(e);
      return new ErrorResponse(wrapAretiResponseMessage(errMsg));
    }
    return new SuccessResponse();
  }

  getScraperCodice() {
    return 'areti';
  }

  getScraperTipologia() {
    return SCRAPER_TIPOLOGIA.FATTIBILITA;
  }
}
