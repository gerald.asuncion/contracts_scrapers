/**
 * @openapi
 *
 * /scraper/v2/iberdrolaV2/check/credit:
 *  post:
 *    tags:
 *      - v2
 *    description: Questa è la rotta diretta allo scraper, quindi non usa il meccanismo del webhook.
 *    consume:
 *      - application/json
 *    produces:
 *      - application/json
 *    responses:
 *      200:
 *        description: SuccessResponse
 *    parameters:
 *      - in: body
 *        schema:
 *          type: object
 *          required:
 *            - tipoFornitura
 *            - prodotto
 *            - cap
 *            - codiceFiscale
 *          properties:
 *            tipoFornitura:
 *              type: string
 *            prodotto:
 *              type: string
 *              enum: [luce, gas, dual]
 *            cap:
 *              type: string
 *            codiceFiscale:
 *              type: string
 *            potenzaDisponibile:
 *              type: string
 *
 * /scraper/queue/iberdrolaCreditCheck:
 *  post:
 *    tags:
 *      - v2
 *      - queue
 *    description:
 *      Il payload è lo stesso della rotta diretta `/scraper/v2/iberdrolaV2/check/credit`,
 *      in più va aggiunto il webhook che lo scraper poi andrà a richiamare per notificare l'esito.
 *    consume:
 *      - application/json
 *    produces:
 *      - application/json
 *    responses:
 *      200:
 *        description: SuccessResponse
 */
