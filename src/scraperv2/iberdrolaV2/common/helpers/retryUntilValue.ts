export default async function retryUntilValue<T>(fn: () => Promise<T>, maxRetry = 10): Promise<T|undefined> {
  if (maxRetry > 0) {
    const value = await fn();

    if (value) {
      return value;
    }

    return retryUntilValue<T>(fn, maxRetry - 1);
  }

  return undefined;
}
