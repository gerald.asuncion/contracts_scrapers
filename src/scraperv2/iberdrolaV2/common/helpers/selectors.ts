export function expressionContainsClass($class: string, addEnclosure = true): string {
  return `${addEnclosure ? '[' : ''}contains(concat(' ', normalize-space(@class), ' '), ' ${$class} ')${addEnclosure ? ']' : ''}`;
}

export function expressionContainsText(text: string, addEnclosure = true): string {
  return `${addEnclosure ? '[' : ''}contains(., "${text}")${addEnclosure ? ']' : ''}`;
}

export function expressionDisabled(): string {
  return '[@disabled]';
}
