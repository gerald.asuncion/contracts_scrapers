import { LabeledError } from './errors';

export default function creaMessaggioDiErrore(errors: LabeledError[]): string {
  return errors.map(({ label = '', message }) => {
    if (message.toLowerCase() === 'error kickbox') {
      return "L' indirizzo e-mail inserito non é valido";
    }

    if (!label || label === 'Notifier') {
      return message;
    }

    return `${label} :: ${message}`;
  }).join('; ');
}
