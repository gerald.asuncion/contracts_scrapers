/* eslint-disable max-len */
import { ElementHandle, Page } from 'puppeteer';
import getElementTextByXPath from '../../../../browser/page/getElementTextByXPath';
import waitForXPathVisible from '../../../../browser/page/waitForXPathVisible';
import { Logger } from '../../../../logger';
import { expressionContainsClass } from './selectors';

export type LabeledError = {
  label: string;
  message: string;
};

function getInnerText(eh: ElementHandle): Promise<string> {
  return eh.evaluate((element) => (element as HTMLElement).innerText);
}

export async function checkNotifyError(page: Page) {
  const containerSelector = '//div[contains(@class, "slds-theme_error")]';
  try {
    await waitForXPathVisible(page, containerSelector);
    // await getElementTextByXPath(page, `${containerSelector}//div[contains(@class, "slds-notify__content")]//lightning-formatted-rich-text`, 'textContent')
    const elements = await page.$x(`${containerSelector}//div[contains(@class, "slds-notify__content")]//lightning-formatted-rich-text//ul/li`);

    const errors = await Promise.all((await Promise.all(elements.map(element => element.getProperty('innerText')))).map(prop => prop.jsonValue<string>()));
    return errors.map(err => err.trim()).filter(Boolean);
  } catch (ex) {
    // nulla da fare
  }

  return [];
}

export default async function checkForErrors(page: Page, logger: Logger): Promise<LabeledError[]|null> {
  const notifyHandlers = await page.$x(
    `//span${expressionContainsClass('slds-icon-utility-error')}/parent::*//lightning-formatted-rich-text`
  );

  const errors: LabeledError[] = (await Promise.all(
    notifyHandlers.map(getInnerText)
  ))
    .map((message) => ({
      label: 'Notifier',
      message
    }));

  const controlsErrorsExpression = `//*[(@role="alert" and (name()="label" or name()="div" and @data-help-message) or (@data-error-message)) or (${expressionContainsClass('slds-has-error', false)}) or (@data-help-text)]`;

  errors.push(...(
    await page.evaluate((expr) => {
      const alertMessages = document.evaluate(
        expr,
        document,
        null,
        XPathResult.ANY_TYPE,
        null
      );

      const messages: LabeledError[] = [];
      let alertMessage;

      // eslint-disable-next-line no-cond-assign
      while (alertMessage = alertMessages.iterateNext()) {
        const root = (alertMessage.getRootNode() as ShadowRoot).host.shadowRoot;
        const labelElement = root?.querySelector('label:not([role="alert"])');

        messages.push({
          label: labelElement?.textContent || '',
          message: alertMessage.textContent || ''
        });
      }

      return messages;
    }, controlsErrorsExpression)
  ) as LabeledError[]);

  // const otherErrors = await checkNotifyError(page);
  // if (otherErrors.length) {
  //   errors.push(...otherErrors.map(message => ({
  //     label: 'Notifier',
  //     message
  //   })));
  // }
  if (errors.length && errors.filter((msg) => !!msg.message).length) {
    return errors.filter((msg) => !!msg.message);
  }

  return null;
}
