/* eslint-disable no-empty */
import { Page } from 'puppeteer';
import { Logger } from 'winston';
import { TryOrThrowError } from '../../../../utils/tryOrThrow';

const defaultRetryTimes = 10;

export default async function retryingWaitFor(
  page: Page,
  logger: Logger,
  itemsSelector: string,
  retryFunc: () => Promise<void>,
  retries = defaultRetryTimes
): Promise<void> {
  await retryFunc();

  try {
    await page.waitForXPath(itemsSelector, {
      timeout: 3000
    });

    return;
  } catch { }

  if (retries < 1) {
    throw new TryOrThrowError(`retryingWaitFor after ${defaultRetryTimes} retries for selector '${itemsSelector}'!`);
  }

  logger.warn(`RetryingWaitFor retried ${defaultRetryTimes + 1 - retries} times for selector '${itemsSelector}'`);

  await retryingWaitFor(page, logger, itemsSelector, retryFunc, retries - 1);
}
