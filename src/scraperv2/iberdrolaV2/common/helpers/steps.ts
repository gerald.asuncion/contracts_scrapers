import { Page } from 'puppeteer';
import { Logger } from '../../../../logger';
import tryOrThrow from '../../../../utils/tryOrThrow';

export type StepContext = {
  page: Page;
  logger: Logger;
  stepId: number;
  steps: number;
  isFirstStep: boolean;
  isLastStep: boolean;
  result: undefined | unknown;
};
/* eslint-disable @typescript-eslint/no-explicit-any */
type StepFunc<TR = void|undefined|null|[any]|[any, any]> = (stepCtx: StepContext, ...params: any[]) => Promise<TR>;

function computePartialContext(
  src: Pick<StepContext, 'page' | 'logger' | 'steps'> & Partial<StepContext>,
  steps: StepFunc[],
  result?: unknown
): StepContext {
  const stepId = src.steps - steps.length;
  const isFirstStep = src.steps === steps.length;
  const isLastStep = steps.length === 1;

  return Object.freeze({
    ...src,
    stepId,
    isFirstStep,
    isLastStep,
    result
  });
}

async function innerExecuteSteps(
  ctx: StepContext,
  steps: StepFunc[],
  stepValidator: StepFunc<void|any>,
  ...args: any[]
): Promise<[undefined, any] | [any]> {
  const step = steps.shift();

  if (!step) {
    return [undefined, ctx.result];
  }

  ctx.logger.info(`Step successivo # ${ctx.stepId}/${ctx.steps} "${step.name}".`);

  const result = await tryOrThrow(() => step(ctx, ...args), `Errore durante lo step "${step.name}"`);
  if (result && !Array.isArray(result)) {
    // throw new Error('Il risultato di uno step deve essere void o [errore|null, result|null]');
  }
  if (result && result[0]) {
    // questo è un errore (lista di errori) intermedio
    return [result[0]];
  }

  const validation = await tryOrThrow(() => stepValidator(ctx, ...args), `Errore durante la validazione "${step.name}"`);
  if (validation) {
    return [validation];
  }

  return innerExecuteSteps(
    computePartialContext(ctx, steps, result && result[1]),
    steps,
    stepValidator,
    ...args
  );
}

export default async function executeSteps(
  page: Page,
  logger: Logger,
  steps: StepFunc[],
  validateStep: StepFunc<void|any>,
  ...args: any[]
): ReturnType<typeof innerExecuteSteps> {
  const ctx: StepContext = computePartialContext({
    page,
    logger,
    steps: steps.length
  }, steps);

  return innerExecuteSteps(ctx, steps, validateStep, ...args);
}
