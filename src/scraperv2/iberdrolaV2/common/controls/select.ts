/* eslint-disable @typescript-eslint/no-non-null-assertion */
import { Page } from 'puppeteer';
import { Logger } from '../../../../logger';
import collateBestMatch from '../../../../browser/helpers/collateBestMatch';
import { expressionContainsClass, expressionContainsText, expressionDisabled } from '../helpers/selectors';

export function selectContainerExpression($class: string): string {
  return `//*${expressionContainsClass($class)}`;
}
function selectSelectorForAddressFields($label: string): string {
  return `//lightning-layout-item//label${expressionContainsText($label)}//parent::div`;
  // return `${selectContainerExpression($class)}//select`;
}

export async function isSelectDisabled(page: Page, logger: Logger, $class: string): Promise<boolean> {
  return (
    !!(await page.$x(`${selectSelectorForAddressFields($class)}//select${expressionDisabled()}`)).length ||
    !!(await page.$x(`${selectSelectorForAddressFields($class)}//input${expressionDisabled()}`)).length
  );
}

export default async function selectSelect(page: Page, logger: Logger, $class: string, value: string): Promise<void> {
  const selectExpression = `${selectSelectorForAddressFields($class)}//select`;
  const optionsExpression = `${selectExpression}/option`;

  const selectHandler = (await page.$x(selectExpression))[0];
  const optionsHandler = await page.$x(optionsExpression);

  const optionsValues = await Promise.all(optionsHandler.map(async (el) => (await el.getProperty('value')).jsonValue<string>()));

  const [, acceptedValue] = collateBestMatch(value, optionsValues, {
    threshold: false
  });

  await selectHandler.select(acceptedValue);
}
