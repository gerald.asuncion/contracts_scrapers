import { Page } from 'puppeteer';
import { Logger } from '../../../../logger';
import tryOrThrow from '../../../../utils/tryOrThrow';

export function datepickerContainerExpression(label: string): string {
  return `//lightning-datepicker//label[contains(.,"${label}")]/ancestor::lightning-datepicker`;
}

export default async function inputDatepicker(page: Page, logger: Logger, label: string, value: string): Promise<void> {
  return tryOrThrow(async () => {
    const containerSelector = datepickerContainerExpression(label);
    const inputSelector = `${containerSelector}//input[@type="text"]`;

    const inputHandler = await page.waitForXPath(inputSelector);

    await inputHandler?.click({ clickCount: 3 });
    await inputHandler?.type(value);
  }, `Errore durante la compilazione del campo Datepicker "${label}" con il valore "${value}".`);
}
