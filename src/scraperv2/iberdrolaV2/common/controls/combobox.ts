/* eslint-disable @typescript-eslint/no-non-null-assertion */
import { Page } from 'puppeteer';
import { Logger } from '../../../../logger';
import tryOrThrow from '../../../../utils/tryOrThrow';
import collateBestMatch from '../../../../browser/helpers/collateBestMatch';
import retryingWaitFor from '../helpers/retryingWaitFor';
import { expressionContainsText } from '../helpers/selectors';
import waitForXPathAndClickEvaluated from '../../../../browser/page/waitForXPathAndClickEvaluated';

export function comboboxContainerExpression(label: string): string {
  return `//lightning-combobox/label${expressionContainsText(label)}//ancestor::lightning-combobox`;
}

export default async function selectCombobox(
  page: Page,
  logger: Logger,
  label: string,
  value: string,
  parentSelector?: string
): Promise<void> {
  let allowedValues: string[] | undefined;

  return tryOrThrow(async () => {
    const containerSelector = `${parentSelector || ''}${comboboxContainerExpression(label)}`;
    const inputSelector = `${containerSelector}//input["lightning-basecombobox_basecombobox"]`;
    const itemsSelector = `${containerSelector}//div[@data-dropdown-element]/lightning-base-combobox-item/span/span`;

    await retryingWaitFor(page, logger, itemsSelector, () => waitForXPathAndClickEvaluated(page, inputSelector));

    const itemsHandlers = await page.$x(itemsSelector);

    allowedValues = (await Promise.all(
      itemsHandlers.map(async (itemHandler) => (await itemHandler.getProperty('title')).jsonValue<string>())
    ));

    // https://stackoverflow.com/questions/47304665/how-to-pass-a-function-in-puppeteers-evaluate-method
    // await page.exposeFunction('findBestMatch', findBestMatch);

    const [acceptedIndex] = collateBestMatch(value, allowedValues, {
      threshold: false
    });

    await itemsHandlers[acceptedIndex].click();
  }, `Errore durante la compilazione del campo Combobox "${label}" con il valore "${value}".${
    allowedValues ? ` I valori ammessi sono: "${allowedValues.join('", "')}".` : ''
  }`);
}

// function shadowDomSelector(element: HTMLElement, ...selectors: string []) {

// }
// shadowDomSelector(null, [
//   `x|//lightning-combobox/label[contains(., "${label}")]//ancestor::lightning-combobox`,
//   '.slds-combobox_container',
//   '*|lightning-base-combobox-item',
//   'attr|title'
// ])
