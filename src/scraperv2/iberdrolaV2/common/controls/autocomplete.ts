/* eslint-disable no-empty */
import { ElementHandle, Page } from 'puppeteer';
import waitForXPathAndClick from '../../../../browser/page/waitForXPathAndClick';
import waitForXPathAndType from '../../../../browser/page/waitForXPathAndType';
import { Logger } from '../../../../logger';
import tryOrThrow, { TryOrThrowError } from '../../../../utils/tryOrThrow';
import collateBestMatch from '../../../../browser/helpers/collateBestMatch';

export function autocompleteContainerExpression(label: string): string {
  return `//c-ibd-lookup//label[contains(., "${label}")]//ancestor::c-ibd-lookup`;
}

function getSelectionExpressions(label: string) {
  const containerSelector = autocompleteContainerExpression(label);
  const inputSelector = `${containerSelector}//input`;
  const itemsSelector = `${containerSelector}//ul/li/span[@data-recordid]`;

  return {
    containerSelector,
    inputSelector,
    itemsSelector
  };
}
async function waitForItems(page: Page, itemsSelector: string): Promise<[string[], ElementHandle<Element>[]]> {
  try {
    await page.waitForXPath(itemsSelector, {
      timeout: 10000
    });
  } catch { }

  const itemsHandler = await page.$x(itemsSelector);
  const itemsData = await Promise.all(
    itemsHandler
      .map(async (eh) => eh.evaluate((el) => (el as HTMLElement).innerText))
  );

  return [itemsData, itemsHandler];
}

// export function clickAutocomplete(page: Page, logger: Logger, label: string): Promise<void> {
//   const {
//     inputSelector
//   } = getSelectionExpressions(label);

//   return waitForXPathAndClick(page, inputSelector);
// }

export async function extractAutocompleteItems(page: Page, logger: Logger, label: string): Promise<string[]> {
  const {
    inputSelector,
    itemsSelector
  } = getSelectionExpressions(label);

  await waitForXPathAndClick(page, inputSelector);

  const [itemsLabel] = await waitForItems(page, itemsSelector);

  return itemsLabel;
}

export default async function selectAutocomplete(page: Page, logger: Logger, label: string, value: string): Promise<void> {
  let allowedValues: string[]|undefined;

  return tryOrThrow(async () => {
    const {
      inputSelector,
      itemsSelector
    } = getSelectionExpressions(label);

    await waitForXPathAndType(page, inputSelector, value);

    try {
      await page.waitForXPath(itemsSelector, {
        timeout: 10000
      });
    } catch { }

    let itemIndex = 0;
    let itemsHandler: ElementHandle<Element>[];

    [allowedValues, itemsHandler] = await waitForItems(page, itemsSelector);

    if (allowedValues.length === 0) {
      throw new TryOrThrowError(`Il campo a valori multipli '${label}' non possiede alcun match per il valore '${value}'!`);
    }
    if (allowedValues.length > 1) {
      [itemIndex] = collateBestMatch(value, allowedValues);
    }

    await itemsHandler[itemIndex].click();
  }, `Errore durante la compilazione del campo Autocomplete "${label}" con il valore "${value}".${
    allowedValues ? ` I valori ammessi sono: "${allowedValues.join('", "')}".` : ''
  }`);
}

export function selezionaFornitoreDistributore(page: Page, fieldLabel: string, codice: string, valueLabel: string) {
  let allowedValues: string[]|undefined;

  return tryOrThrow(async () => {
    const {
      inputSelector,
      itemsSelector
    } = getSelectionExpressions(fieldLabel);

    await waitForXPathAndType(page, inputSelector, codice);

    try {
      await page.waitForXPath(itemsSelector, {
        timeout: 10000
      });
    } catch { }

    let itemIndex = 0;
    let itemsHandler: ElementHandle<Element>[];

    [allowedValues, itemsHandler] = await waitForItems(page, itemsSelector);

    if (allowedValues.length === 0) {
      throw new TryOrThrowError(`Il campo a valori multipli '${fieldLabel}' non possiede alcun match per il valore '${valueLabel}'!`);
    }
    if (allowedValues.length > 1) {
      [itemIndex] = collateBestMatch(valueLabel, allowedValues);
    }

    await itemsHandler[itemIndex].click();
  }, `Errore durante la compilazione del campo Autocomplete "${fieldLabel}" con il valore "${valueLabel}".${
    allowedValues ? ` I valori ammessi sono: "${allowedValues.join('", "')}".` : ''
  }`);
}
