import { Page } from 'puppeteer';
import isXPathVisible from '../../../../browser/page/isXPathVisible';
import waitForXPathAndType from '../../../../browser/page/waitForXPathAndType';
import { Logger } from '../../../../logger';
import tryOrThrow from '../../../../utils/tryOrThrow';
import { expressionDisabled } from '../helpers/selectors';

export function textContainerExpression(label: string): string {
  return `//lightning-input//label[contains(.,"${label}")]/ancestor::lightning-input`;
}

export function isInputVisible(page: Page, selector: string): Promise<boolean> {
  return isXPathVisible(page, textContainerExpression(selector));
}

async function isInputDisabled(page: Page, selector: string): Promise<boolean> {
  return !!(await page.$x(`${selector}${expressionDisabled()}`)).length;
}

export default async function inputText(page: Page, logger: Logger, label: string, value: string): Promise<void> {
  return tryOrThrow(async () => {
    const containerSelector = textContainerExpression(label);
    const inputSelector = `${containerSelector}//input["lightning-input_input"]`;

    if (!(await isInputDisabled(page, inputSelector))) {
      await waitForXPathAndType(page, inputSelector, value);
    }
  }, `Errore durante la compilazione del campo iNPUT "${label}" con il valore "${value}".`);
}

export function getInputText(page: Page, label: string): Promise<string> {
  return tryOrThrow(async () => {
    const containerSelector = textContainerExpression(label);
    const inputSelector = `${containerSelector}//input["lightning-input_input"]`;
    const [xElement] = await page.$x(inputSelector);
    const text = await page.evaluate((el) => el.textContent, xElement);
    return text;
  }, `Errore durante il recuper del valore del campo iNPUT "${label}".`);
}
