/* eslint-disable max-len */
import { extractAutocompleteItems } from '../common/controls/autocomplete';
import { StepContext } from '../common/helpers/steps';
import { IberdrolaInserimentoV2Payload } from '../payload/IberdrolaInserimentoV2Payload';

export default async function stepEstraiFornitori({ page, logger }: StepContext, payload: IberdrolaInserimentoV2Payload, controlLabel: string): Promise<[unknown, string[]]> {
  return [
    undefined,
    await extractAutocompleteItems(page, logger, controlLabel)
  ];
}
