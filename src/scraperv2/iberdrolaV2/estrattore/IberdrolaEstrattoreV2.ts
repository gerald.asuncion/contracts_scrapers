import waitForNavigation from '../../../browser/page/waitForNavigation';
import waitForSelectorAndClick from '../../../browser/page/waitForSelectorAndClick';
import SuccessResponse from '../../../response/SuccessResponse';
import createChildPayloadAwareLogger from '../../../utils/createChildPayloadAwareLogger';
import { ScraperResponse } from '../../scraper';
import waitFormLoading from '../common/helpers/loading';
import executeSteps from '../common/helpers/steps';
import stepEstraiFornitori from './estraiFornitori';
import IberdrolaInserimentoV2 from '../IberdrolaInserimentoV2';
import stepCliente from '../inserimento/cliente';
import stepFornitura from '../inserimento/fornitura';
import stepIndirizzo from '../inserimento/indirizzo';
import nextStep from './next';
import { IberdrolaInserimentoV2Payload } from '../payload/IberdrolaInserimentoV2Payload';
import creaMessaggioDiErrore from '../common/helpers/creaMessaggioDiErrore';
import FailureResponse from '../../../response/FailureResponse';

export default abstract class IberdrolaEstrattoreV2 extends IberdrolaInserimentoV2 {
  protected abstract readonly controlLabel: string;

  async scrapeWebsite(payload: IberdrolaInserimentoV2Payload): Promise<ScraperResponse> {
    const logger = createChildPayloadAwareLogger(this.childLogger, payload);
    const page = await this.p();

    await waitForNavigation(page);

    await waitForSelectorAndClick(page, 'a[title="Nuova Contrattazione"]');

    await waitFormLoading(page, logger);

    const [errors, result] = await executeSteps(page, logger, [
      stepFornitura,
      stepCliente,
      stepIndirizzo,
      stepEstraiFornitori
    ], nextStep, payload, this.controlLabel);

    if (errors) {
      const errMsg = creaMessaggioDiErrore(errors);
      logger.error(errMsg);
      return new FailureResponse(errMsg);
    }

    return new SuccessResponse(result);
  }
}
