import tryOrThrow from '../../../utils/tryOrThrow';
import clickButton from '../common/controls/button';
import checkForErrors from '../common/helpers/errors';
import waitFormLoading from '../common/helpers/loading';
import { StepContext } from '../common/helpers/steps';

export default async function nextStep({ page, logger, isLastStep, stepId }: StepContext): Promise<void|ReturnType<typeof checkForErrors>> {
  let errors = await tryOrThrow(() => checkForErrors(page, logger), 'Sto verificando messaggi di alert ed errori!');
  if (errors) {
    return errors;
  }

  if (isLastStep) {
    return null;
  }

  await Promise.all([
    clickButton(page, logger, 'Continua'),
    waitFormLoading(page, logger)
  ]);

  errors = await tryOrThrow(() => checkForErrors(page, logger), 'Sto verificando messaggi di alert ed errori!');
  if (errors) {
    return errors;
  }

  // come controprova verifichiamo l'id della breadcrumb attiva
  // const elIndex = await tryOrThrow(async () => {
  //   const [elHandler] = await page.$x(`//lightning-progress-step${expressionContainsClass('slds-is-current')}`);
  //   return page.evaluate((el) => el.getAttribute('data-index'), elHandler);
  // }, `Non sono riuscito a selezionare la breadcrumb corrente per verifica step ${stepId}.`) as number;

  // if (elIndex.toString() !== stepId.toString()) {
  //   throw new Error(`Avanzamento allo step ${stepId} non avvenuto correttamente.`);
  // }

  return null;
}
