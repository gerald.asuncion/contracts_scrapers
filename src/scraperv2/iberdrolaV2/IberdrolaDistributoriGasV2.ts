import { QueueTask } from '../../task/QueueTask';
import IberdrolaEstrattoreV2 from './estrattore/IberdrolaEstrattoreV2';

@QueueTask({ scraperName: 'iberdrolaDistributoriGas' })
export default class IberdrolaDistributoriGasV2 extends IberdrolaEstrattoreV2 {
  controlLabel = 'Distributore Gas';
}
