import { MappedValue } from './types';

export function mappingModalitaConfermaContratto(): string {
  return 'FirmaDigitale';
}

export function mappingTipologiaCliente(tipo: MappedValue): string {
  return tipo as string;
}

export function mappingTipologiaDocumento(tipo: MappedValue): string {
  return tipo as string;
}

export function mappingConsenso(consenso: MappedValue): string {
  return consenso ? 'Acconsento' : 'Non Acconsento';
}

export function mappingDesidero(desidero: MappedValue): string {
  return desidero ? 'Desidero' : 'Non desidero';
}

export function mappingResidenza(residenza: MappedValue): string {
  return !residenza ? 'si' : 'no';
}

export function mappingTitolaritaImmobile(titolaritaImmobile: MappedValue): string {
  const map = {
    proprietario: 'Proprietario',
    usufruttuario: 'Usufruttario',
    usufruttuario_altro: 'Usufruttario',
    inquilino: "Titolare di altro diritto sull'immobile",
    comodatario: "Titolare di altro diritto sull'immobile"
  };
  return map[titolaritaImmobile as keyof typeof map] as string;
}

export function mappingUtilizzoGas(value: MappedValue): string {
  const map = {
    riscaldamento: 'C1 Riscaldamento',
    cottura_acqua_calda: 'C2 Uso cottura cibi e/o produzione di acqua calda sanitaria',
    cottura_cibi: 'C3 Riscaldamento + uso cottura cibi e/o produzione di acqua calda sanitaria',
  };
  return map[value as keyof typeof map] as string;
}

export function mappingClassePrelievo(): string {
  return '7';
}

export function mappingPotenzaDisponibile(potenzaImpegnata: string|number): string {
  return potenzaImpegnata.toString() === '4,5' ? '5,00' : ((parseFloat(potenzaImpegnata.toString()) * 1.1).toFixed(2)).replace('.', ',');
}

export function mappingModalitaInvioFattura(value: MappedValue): string {
  return ['mail', 'email'].includes((value as string).toLowerCase()) ? 'Si' : 'No';
}

export function mappingModalitaInvioContratto(value: MappedValue): string {
  return ['mail', 'email'].includes((value as string).toLowerCase()) ? 'Email' : 'Posta';
}

export function mappingMetodoDiPagamento(value: MappedValue): string {
  return (value as string).toLowerCase() === 'bollettino' ? 'Bollettino' : 'Addebito RID';
}

export function mappingPaeseDomiciliazione(value: MappedValue): string {
  return (value as string).toLowerCase() === 'italia' ? 'Italia' : 'Altro';
}

export function mappingAccreditamentoAgente(): string {
  return 'T500740';
}
