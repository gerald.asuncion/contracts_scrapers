export declare type MappedType<T> = T;

export declare type MappedValue = MappedType<unknown>;

// export declare type MappedBoolean = boolean;
