import { ScraperResponse } from '../scraper';
import createChildPayloadAwareLogger from '../../utils/createChildPayloadAwareLogger';
import waitForNavigation from '../../browser/page/waitForNavigation';
import FailureResponse from '../../response/FailureResponse';
import waitForXPathAndClick from '../../browser/page/waitForXPathAndClick';
import SuccessResponse from '../../response/SuccessResponse';
import IberdrolaInserimentoV2 from './IberdrolaInserimentoV2';
import tryOrThrow from '../../utils/tryOrThrow';
import stepCreditCheck from './check/credit/stepCreditCheck';
import { IberdrloaCreditCheckV2Payload } from './payload/IberdrloaCreditCheckV2Payload';
import stepConfermaPrivacy from './check/credit/stepConfermaPrivacy';
import CasoNonGestitoResponse from '../../response/CasoNonGestitoResponse';
// import stepControlloCodiceFiscale from './check/credit/stepControlloCodiceFiscale';
import stepRecuperoEsito from './check/credit/stepRecuperoEsito';
import { SCRAPER_TIPOLOGIA } from '../ScraperOptions';
import { QueueTask } from '../../task/QueueTask';

@QueueTask({ scraperName: 'iberdrolaCreditCheck' })
export default class IberdrloaCreditCheckV2 extends IberdrolaInserimentoV2 {
  async scrapeWebsite(payload: IberdrloaCreditCheckV2Payload): Promise<ScraperResponse> {
    const logger = createChildPayloadAwareLogger(this.childLogger, payload);
    const page = await this.p();

    await waitForNavigation(page);

    logger.info('vado alla pagina del credit check');
    await tryOrThrow(
      () => page.goto('https://iberdrola.force.com/ibergoapp/s/credit-check', { waitUntil: 'networkidle2' }),
      'non sono riuscito a navigare fino alla pagina credit check'
    );

    await tryOrThrow(
      () => waitForXPathAndClick(page, '//button[contains(@name,"newCreditCheck")]'),
      'non sono riuscito ad aprire la modale per inserire i dati da controllare'
    );

    await tryOrThrow(
      () => stepCreditCheck(page, logger, payload),
      'step inserimento dati:'
    );

    await tryOrThrow(
      () => stepConfermaPrivacy(page, logger),
      'non sono riuscito a confermare la privacy'
    );

    const stato = await tryOrThrow(
      // () => stepControlloCodiceFiscale(page, payload.codiceFiscale),
      () => stepRecuperoEsito(page),
      'step controllo codice fiscale:'
    );
    logger.info(`recuperato lo stato ${stato}`);

    if (stato === 'ok') {
      logger.info('restituisco una SuccessResponse');
      return new SuccessResponse();
    }

    if (stato === 'ko') {
      logger.info('restituisco una FailureResponse');
      return new FailureResponse('KO');
    }

    logger.info('restituisco una CasoNonGestitoResponse');
    return new CasoNonGestitoResponse();
  }

  /**
   * @override
   */
  getScraperCodice() {
    return 'iberdrola-checkcredit';
  }

  /**
   * @override
   */
  getScraperTipologia(): any {
    return SCRAPER_TIPOLOGIA.FATTIBILITA;
  }
}
