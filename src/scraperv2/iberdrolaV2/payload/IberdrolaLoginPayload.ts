import ContrattoPayload from '../../../contratti/ContrattoPayload';

export type IberdrolaLoginPayload = Required<ContrattoPayload> & {
  callCenter: string;
};
