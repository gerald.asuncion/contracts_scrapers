import ContrattoPayload from '../../../contratti/ContrattoPayload';
import { MappedType, MappedValue } from '../mapping/types';
import { TipologiaEnergia } from './types/TipologiaEnergia';

export type IberdrolaInserimentoV2Payload = Required<ContrattoPayload> & {
  tipoIntestatario: MappedValue;
  tipoEnergia: MappedType<TipologiaEnergia>;
  pdr: string;
  pod: string;

  primoConsenso: MappedValue; // boolean;
  secondoConsenso: MappedValue; // boolean;
  terzoConsenso: MappedValue; // boolean;

  tipoDocumento: MappedValue;
  numeroDocumento: string;
  codiceFiscaleRappresentante: string;
  nomeRappresentante: string;
  cognomeRappresentante: string;
  dataDiNascitaRappresentante: string;
  telefonoRappresentante: string;
  emailRappresentante: string;
  ragioneSocialeRappresentante: string;
  PECRappresentante?: string;

  cap: string;
  comune: string;
  indirizzo: string;
  tipoStrada: string;
  civico: string;

  capResidenza?: string;
  comuneResidenza: string;
  indirizzoResidenza?: string;
  tipoStradaResidenza?: string;
  civicoResidenza?: string;

  prodottoGas?: string;
  attualeFornitoreGas?: string;
  distributoreGas?: string;
  utilizzoGas?: string;
  consumoGas?: string;

  prodottoEnergia?: string;
  attualeFornitoreElettricita?: string;
  distributoreElettricita?: string;
  potenzaImpegnata?: string | number;
  consumoLuce?: string;
  tensione?: string;

  titolaritaImmobile: string;

  modalitaInvioFattura: string;
  modalitaPagamento: string;
  iban: string;

  codiceABarre: string;
  dataCompletato: string;
};
