import { IberdrolaInserimentoV2Payload } from './IberdrolaInserimentoV2Payload';

export type IberdrolaCheckFirmeV2Payload = {
  codiceCupsArray: string[];
} & IberdrolaInserimentoV2Payload;
