/* eslint-disable import/prefer-default-export */
export const enum TipologiaEnergia {
  elettricita = 'elettricita',
  gas = 'gas',
  dual = 'dual'
}
