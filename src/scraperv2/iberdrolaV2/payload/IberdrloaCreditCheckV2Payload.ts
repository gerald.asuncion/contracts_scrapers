import { IberdrolaInserimentoV2Payload } from './IberdrolaInserimentoV2Payload';

export type IberdrloaCreditCheckV2Payload = {
  tipoFornitura: string;
  prodotto: 'luce' | 'gas' | 'dual';
  cap: string;
  codiceFiscale: string;
  // partitaIva: string;
  potenzaDisponibile?: string;
} & IberdrolaInserimentoV2Payload;
