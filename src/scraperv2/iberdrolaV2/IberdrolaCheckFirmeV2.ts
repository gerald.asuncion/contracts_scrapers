import IberdrolaInserimentoV2 from './IberdrolaInserimentoV2';
import { ScraperResponse } from '../scraper';
import createChildPayloadAwareLogger from '../../utils/createChildPayloadAwareLogger';
import tryOrThrow from '../../utils/tryOrThrow';
import GenericSuccessResponse from '../../response/GenericSuccessResponse';
import waitForNavigation from '../../browser/page/waitForNavigation';
import checkCodiciCups from './check/firme/checkCodiciCups';
import { IberdrolaCheckFirmeV2Payload } from './payload/IberdrolaCheckFirmeV2Payload';
import waitForVisible from '../../browser/page/waitForVisible';
import waitForTimeout from '../../browser/page/waitForTimeout';
import waitForXPathVisible from '../../browser/page/waitForXPathVisible';
import { SCRAPER_TIPOLOGIA } from '../ScraperOptions';
import { IberdrolaLoginPayload } from './payload/IberdrolaLoginPayload';
import { QueueTask } from '../../task/QueueTask';

@QueueTask({ scraperName: 'iberdrolaCheckFirme' })
export default class IberdrolaCheckFirmeV2 extends IberdrolaInserimentoV2 {
  /**
   * TOLGO il call center perché non serve più
   * TODO: buttare via questa funzione appena contracts smette di mandare il call center per questo scraper
   * @override
   * @param payload payload ricevuto
   */
  async login({ callCenter, ...payload }: IberdrolaLoginPayload): Promise<void> {
    return super.login(payload as any);
  }

  /**
   * @override
   * @param payload payload ricevuto
   */
  async scrapeWebsite(payload: IberdrolaCheckFirmeV2Payload): Promise<ScraperResponse> {
    const { codiceCupsArray } = payload;
    const logger = createChildPayloadAwareLogger(this.childLogger, payload);
    const page = await this.p();

    await waitForNavigation(page);

    logger.info('attendo apparizione dashboard');
    await tryOrThrow(
      async () => {
        const el = await waitForVisible(page, 'iframe[title="dashboard"]');
        const frame = await el?.contentFrame();
        if (!frame) {
          logger.warn('iframe contenente la dashboard non trovato');
          await waitForTimeout(page, 3000);
        } else {
          await waitForXPathVisible(frame, '//span[contains(.,"Cruscotto digitale")]');
        }
      },
      'non sono riuscito ad attendere il caricamento della home page'
    );

    logger.info(`controllo i contratti ${codiceCupsArray.join(', ')}`);
    const statoContrattoResponse: Record<string, string> = {};
    for await (const { codiceContratto, stato } of checkCodiciCups(page, logger, codiceCupsArray, this.s3ClientFactory)) {
      logger.info(`contratto ${codiceContratto} ha lo stato ${stato}`);
      statoContrattoResponse[codiceContratto] = stato;
    }

    return new GenericSuccessResponse(statoContrattoResponse);
  }

  /**
   * @override
   */
  getScraperCodice() {
    return 'iberdrola-checkfirma';
  }

  /**
   * @override
   */
  getScraperTipologia(): any {
    return SCRAPER_TIPOLOGIA.CHECKFIRMA;
  }
}
