import { Page } from 'puppeteer';
import { Logger } from '../../logger';
import waitCheckCredentials from '../../browser/page/waitCheckCredentials';
import waitForNavigation from '../../browser/page/waitForNavigation';
import waitForSelectorAndClick from '../../browser/page/waitForSelectorAndClick';
import waitForSelectorAndType from '../../browser/page/waitForSelectorAndType';
import SuccessResponse from '../../response/SuccessResponse';
import createChildPayloadAwareLogger from '../../utils/createChildPayloadAwareLogger';
import tryOrThrow, { TryOrThrowError } from '../../utils/tryOrThrow';
import { ScraperResponse, WebScraper } from '../scraper';
import stepCliente from './inserimento/cliente';
import stepFornitura from './inserimento/fornitura';
import { IberdrolaLoginPayload } from './payload/IberdrolaLoginPayload';
import { IberdrolaInserimentoV2Payload } from './payload/IberdrolaInserimentoV2Payload';
import stepIndirizzo from './inserimento/indirizzo';
import stepDettaglio from './inserimento/dettaglio';
import stepInserimentoFatturazione from './inserimento/fatturazione';
import executeSteps, { StepContext } from './common/helpers/steps';
import nextStep from './inserimento/next';
import waitFormLoading from './common/helpers/loading';
import { expressionContainsClass, expressionContainsText } from './common/helpers/selectors';
import retryUntilValue from './common/helpers/retryUntilValue';
import creaMessaggioDiErrore from './common/helpers/creaMessaggioDiErrore';
import FailureResponse from '../../response/FailureResponse';
import checkForErrors, { LabeledError } from './common/helpers/errors';
import { SCRAPER_TIPOLOGIA } from '../ScraperOptions';
import { QueueTask } from '../../task/QueueTask';

@QueueTask({ scraperName: 'iberdrolaV2' })
export default class IberdrolaInserimentoV2 extends WebScraper<IberdrolaLoginPayload | IberdrolaInserimentoV2Payload> {
  static readonly LOGIN_URL = 'https://iberdrola.force.com/ibergoapp';

  async login(payload: IberdrolaLoginPayload): Promise<void> {
    const logger = createChildPayloadAwareLogger(this.childLogger, payload);
    const { username, password } = await this.getCredenziali(payload);

    const page = await this.p();

    await Promise.all([
      page.goto(IberdrolaInserimentoV2.LOGIN_URL, { waitUntil: 'networkidle2' }),
      waitForNavigation(page)
    ]);

    await tryOrThrow(async () => {
      await waitForSelectorAndType(page, 'input[placeholder="Username"]', username);
      await waitForSelectorAndType(page, 'input[type="password"]', password);
    }, 'non sono riuscito ad inserire username e/o password.');
    logger.info('login > inserite credenziali');

    await Promise.all([
      waitForSelectorAndClick(page, 'button.loginButton'),
      page.waitForResponse(() => true)
    ]);

    await Promise.race([
      waitCheckCredentials(page, '.error[role="alert"]', IberdrolaInserimentoV2.LOGIN_URL, username, logger, {
        timeout: 250
      }),
      waitForNavigation(page)
    ]);

    await page.waitForResponse(() => true);

    logger.info('login > terminata');
  }

  private recuperaCups(page: Page, logger: Logger): Promise<string> {
    return tryOrThrow(async () => {
      // eslint-disable-next-line max-len
      const cupsLabelExpression = `//span${expressionContainsText('Nome opportunità')}/ancestor::*${expressionContainsClass('slds-form-element')}//span[@data-aura-class="uiOutputText"]`;
      const cupsLabelHandler = await page.waitForXPath(cupsLabelExpression, {
        timeout: 60000
      });

      return retryUntilValue(() => page.evaluate((cupsLabelElement) => cupsLabelElement.innerText, cupsLabelHandler));
    }, 'Errore nel recupero il cups!');
  }

  async stepFatturazione(ctx: StepContext, args: IberdrolaInserimentoV2Payload): Promise<void> {
    await stepInserimentoFatturazione(ctx, args);
    try {
      this.checkNoSubmit(args as any);
    } catch (ex: any) {
      throw new TryOrThrowError(ex.message);
    }
  }

  async scrapeWebsite(payload: IberdrolaInserimentoV2Payload): Promise<ScraperResponse> {
    const logger = createChildPayloadAwareLogger(this.childLogger, payload);
    const page = await this.p();

    await waitForNavigation(page);

    await Promise.all([
      waitForSelectorAndClick(page, 'a[title="Nuova Contrattazione"]'),
      page.waitForResponse(() => true),
      waitForNavigation(page)
    ]);

    await waitFormLoading(page, logger);

    this.stepFatturazione = this.stepFatturazione.bind(this);

    const [errors] = await executeSteps(page, logger, [
      stepFornitura,
      stepCliente,
      stepIndirizzo,
      stepDettaglio,
      this.stepFatturazione
    ], nextStep, payload);


    let otherErrors: LabeledError[] | null = null;

    if (!errors) {
      // await page.waitForResponse(() => true);
      await waitFormLoading(page, logger);
      otherErrors = await checkForErrors(page, logger);
    }

    const totalErrors = otherErrors ? otherErrors.concat(errors).filter(Boolean) : errors;

    if (totalErrors) {
      const errMsg = creaMessaggioDiErrore(totalErrors);
      logger.error(errMsg);
      await this.saveScreenshot(page, logger, this.constructor.name, payload.idDatiContratto);
      return new FailureResponse(errMsg);
    }

    const cups = await this.recuperaCups(page, logger);

    return new SuccessResponse(cups);
  }

  getScraperCodice() {
    return 'iberdrola';
  }

  getScraperTipologia() {
    return SCRAPER_TIPOLOGIA.INSERIMENTO;
  }
}
