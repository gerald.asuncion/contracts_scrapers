import { QueueTask } from '../../task/QueueTask';
import IberdrolaEstrattoreV2 from './estrattore/IberdrolaEstrattoreV2';

@QueueTask({ scraperName: 'iberdrolaDistributoriLuce' })
export default class IberdrolaDistributoriLuceV2 extends IberdrolaEstrattoreV2 {
  controlLabel = 'Distributore Elettricità';
}
