/**
 * @openapi
 *
 * /scraper/v2/iberdrolaV2/inserimento:
 *  post:
 *    tags:
 *      - v2
 *    description: Inserisce il contratto, coi dati passati, sul portale di Iberdrola nuovo.
 *      Questa è la rotta diretta allo scraper, quindi non usa il meccanismo del webhook.
 *    consume:
 *      - application/json
 *    produces:
 *      - application/json
 *    responses:
 *      200:
 *        description: SuccessResponse
 *    parameters:
 *      - in: body
 *        schema:
 *          type: object
 *          required:
 *            - idDatiContratto
 *            - callCenter
 *            - tipoIntestatario
 *            - tipoEnergia
 *            - pdr
 *            - pod
 *            - primoConsenso
 *            - secondoConsenso
 *            - terzoConsenso
 *            - tipoDocumento
 *            - numeroDocumento
 *            - codiceFiscaleRappresentante
 *            - nomeRappresentante
 *            - cognomeRappresentante
 *            - dataDiNascitaRappresentante
 *            - telefonoRappresentante
 *            - emailRappresentante
 *            - ragioneSocialeRappresentante
 *            - cap
 *            - comune
 *            - indirizzo
 *            - tipoStrada
 *            - civico
 *            - comuneResidenza
 *            - titolaritaImmobile
 *            - modalitaInvioFattura
 *            - modalitaPagamento
 *            - iban
 *            - codiceABarre
 *            - dataCompletato
 *          properties:
 *            idDatiContratto:
 *              type: number
 *            callCenter:
 *              type: string
 *            tipoIntestatario:
 *              type: string
 *            tipoEnergia:
 *              type: string
 *            enum: [elettricita, gas, dual]
 *            pdr:
 *              type: string
 *            pod:
 *              type: string
 *            primoConsenso:
 *              type: boolean
 *            secondoConsenso:
 *              type: boolean
 *            terzoConsenso:
 *              type: boolean
 *            tipoDocumento:
 *              type: string
 *            numeroDocumento:
 *              type: string
 *            codiceFiscaleRappresentante:
 *              type: string
 *            nomeRappresentante:
 *              type: string
 *            cognomeRappresentante:
 *              type: string
 *            dataDiNascitaRappresentante:
 *              type: string
 *            telefonoRappresentante:
 *              type: string
 *            emailRappresentante:
 *              type: string
 *            ragioneSocialeRappresentante:
 *              type: string
 *            PECRappresentante:
 *              type: string
 *            cap:
 *              type: string
 *            comune:
 *              type: string
 *            indirizzo:
 *              type: string
 *            tipoStrada:
 *              type: string
 *            civico:
 *              type: string
 *            capResidenza:
 *              type: string
 *            comuneResidenza:
 *              type: string
 *            indirizzoResidenza:
 *              type: string
 *            tipoStradaResidenza:
 *              type: string
 *            civicoResidenza:
 *              type: string
 *            prodottoGas:
 *              type: string
 *            attualeFornitoreGas:
 *              type: string
 *            distributoreGas:
 *              type: string
 *            utilizzoGas:
 *              type: string
 *            consumoGas:
 *              type: string
 *            prodottoEnergia:
 *              type: string
 *            attualeFornitoreElettricita:
 *              type: string
 *            distributoreElettricita:
 *              type: string
 *            potenzaImpegnata:
 *              type: string
 *            consumoLuce:
 *              type: string
 *            tensione:
 *              type: string
 *            titolaritaImmobile:
 *              type: string
 *            modalitaInvioFattura:
 *              type: string
 *            modalitaPagamento:
 *              type: string
 *            iban:
 *              type: string
 *            codiceABarre:
 *              type: string
 *            dataCompletato:
 *              type: string
 *
 * /scraper/queue/iberdrola:
 *  post:
 *    tags:
 *      - v2
 *      - queue
 *    description:
 *      Inserisce il contratto, coi dati passati, sul portale di Iberdrola nuovo.
 *      Il payload è lo stesso della rotta diretta `/scraper/v2/iberdrolaV2/inserimento`,
 *      in più va aggiunto il webhook che lo scraper poi andrà a richiamare per notificare l'esito.
 *    consume:
 *      - application/json
 *    produces:
 *      - application/json
 *    responses:
 *      200:
 *        description: SuccessResponse
 */
