import inputText from '../common/controls/input';
import inputCField from '../common/controls/cField';
import selectSelect, { isSelectDisabled } from '../common/controls/select';
import waitFormLoading from '../common/helpers/loading';
import { StepContext } from '../common/helpers/steps';
import { IberdrolaInserimentoV2Payload } from '../payload/IberdrolaInserimentoV2Payload';
import checkForErrors from '../common/helpers/errors';
import delay from '../../../utils/delay';
// import waitForXPathAndClick from '../../../browser/page/waitForXPathAndClick';
// import { expressionContainsClass } from '../common/helpers/selectors';

export default async function stepIndirizzo({ page, logger }: StepContext, payload: IberdrolaInserimentoV2Payload): Promise<void|[unknown[]]> {
  const {
    cap,
    indirizzo,
    civico,
    comune,
    tipoStrada
  } = payload;

  // const blurSelection = (...args: any[]) => waitForXPathAndClick(page, `//*${expressionContainsClass('slds-section__title')}`);
  const blurSelection = (key: 'Enter' | 'Tab' = 'Enter') => page.keyboard.press(key);

  const retry = async (cb: () => Promise<unknown>, count = 4) => {
    if (count > 0) {
      await cb();

      await retry(cb, count - 1);
    }
  };

  await delay(3000);

  // scatena reloading
  await inputText(page, logger, 'CAP', cap);
  await blurSelection('Tab');

  await waitFormLoading(page, logger);
  await delay(3000);

  const errors = await checkForErrors(page, logger);
  if (errors) {
    return [errors];
  }

  // scatena reloading
  // dipende da CAP
  // autopopolato e disabilitato
  if (!await isSelectDisabled(page, logger, 'Paese')) {
    await selectSelect(page, logger, 'Paese', 'TODO');
    await waitFormLoading(page, logger);
  }
  // scatena reloading
  // dipende da CAP e STATE
  // a volte autopopolato e disabilitato
  if (!await isSelectDisabled(page, logger, 'Provincia')) {
    await selectSelect(page, logger, 'Provincia', comune);
    await waitFormLoading(page, logger);
  }

  // scatena reloading
  // dipende da CAP e STATE e COMUNE
  await inputCField(page, logger, 'Indirizzo', indirizzo, true);
  await blurSelection('Tab');

  await waitFormLoading(page, logger);
  await delay(3000);

  if (!await isSelectDisabled(page, logger, 'Tipo di strada')) {
    await retry(() => selectSelect(page, logger, 'Tipo di strada', tipoStrada));
    await blurSelection();
  }

  await inputCField(page, logger, 'Numero', civico, true);
  await blurSelection();

  // => Altre informazioni
  // => L'indirizzo postale e uguale all'indirizzo del punto di fornitura

  return undefined;
}
