import { Page } from 'puppeteer';
import waitForTimeout from '../../../browser/page/waitForTimeout';
import tryOrThrow from '../../../utils/tryOrThrow';
import clickButton from '../common/controls/button';
import checkForErrors, { LabeledError } from '../common/helpers/errors';
import waitFormLoading from '../common/helpers/loading';
import { StepContext } from '../common/helpers/steps';
import { Logger } from '../../../logger';

async function tentoDiTrovareErroriApparsiSulPortale(page: Page, logger: Logger): Promise<LabeledError[] | null> {
  const errors = await tryOrThrow(() => checkForErrors(page, logger), 'Sto verificando messaggi di alert ed errori!');
  return errors;
}

export default async function nextStep({
  page,
  logger,
  isLastStep,
  stepId
}: StepContext): Promise<void|ReturnType<typeof checkForErrors>> {
  let errors = await tentoDiTrovareErroriApparsiSulPortale(page, logger);
  if (errors) {
    return errors;
  }

  await clickButton(page, logger, isLastStep ? 'Finalizzare' : 'Continua');

  await waitFormLoading(page, logger);

  errors = await tentoDiTrovareErroriApparsiSulPortale(page, logger);
  if (errors) {
    return errors;
  }

  await waitForTimeout(page, 1000);

  errors = await tentoDiTrovareErroriApparsiSulPortale(page, logger);
  if (errors) {
    return errors;
  }

  // come controprova verifichiamo l'id della breadcrumb attiva
  // const elIndex = await tryOrThrow(async () => {
  //   const [elHandler] = await page.$x(`//lightning-progress-step${expressionContainsClass('slds-is-current')}`);
  //   return page.evaluate((el) => el.getAttribute('data-index'), elHandler);
  // }, `Non sono riuscito a selezionare la breadcrumb corrente per verifica step ${stepId}.`) as number;

  // if (elIndex.toString() !== (stepId + 1).toString()) {
  //   throw new TryOrThrowError(`Avanzamento allo step ${stepId} non avvenuto correttamente.`);
  // }

  return null;
}
