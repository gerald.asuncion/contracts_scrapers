/* eslint-disable max-len */
import isXPathVisible from '../../../browser/page/isXPathVisible';
import inputDatepicker, { datepickerContainerExpression } from '../common/controls/datepicker';
import inputText from '../common/controls/input';
import { StepContext } from '../common/helpers/steps';
import { mappingConsenso, mappingDesidero, mappingModalitaConfermaContratto, mappingTipologiaCliente, mappingTipologiaDocumento } from '../mapping/inserimento';
import { IberdrolaInserimentoV2Payload } from '../payload/IberdrolaInserimentoV2Payload';
import { selectComboboxV2 } from '../../../browser/salesforce/lightining-components/controls/combobox';

export default async function stepCliente({ page, logger }: StepContext, payload: IberdrolaInserimentoV2Payload): Promise<void> {
  const {
    tipoIntestatario,
    tipoDocumento,
    numeroDocumento,
    codiceFiscaleRappresentante,
    nomeRappresentante,
    cognomeRappresentante,
    dataDiNascitaRappresentante,
    // ragioneSocialeRappresentante,
  } = payload;

  // dati del cliente

  // anagrafica

  await selectComboboxV2(page, logger, 'Tipo di cliente', mappingTipologiaCliente(tipoIntestatario));
  await selectComboboxV2(page, logger, 'Tipo di documento di identità', mappingTipologiaDocumento(tipoDocumento));
  await inputText(page, logger, 'Numero del documento di identità', numeroDocumento);

  await inputText(page, logger, 'Nome', nomeRappresentante);
  await inputText(page, logger, 'Cognome', cognomeRappresentante);

  // residenziale
  if (await isXPathVisible(page, datepickerContainerExpression('Data di nascita'))) {
    await inputDatepicker(page, logger, 'Data di nascita', dataDiNascitaRappresentante);
  }

  await inputText(page, logger, 'Codice Fiscale', codiceFiscaleRappresentante);

  // contatti

  const { telefonoRappresentante, emailRappresentante, PECRappresentante } = payload;

  // await inputText(page, logger, 'Telefono Fisso Cliente', telefonoRappresentante);
  await inputText(page, logger, 'Telefono Cellulare Cliente', telefonoRappresentante);

  // => Cliente in convenzione

  await inputText(page, logger, 'Email', emailRappresentante);
  if (PECRappresentante) {
    await inputText(page, logger, 'PEC', PECRappresentante);
  }

  await selectComboboxV2(page, logger, 'Modalità conferma contratto', mappingModalitaConfermaContratto());

  // consensi

  const {
    primoConsenso,
    secondoConsenso
    // terzoConsenso
  } = payload;

  await selectComboboxV2(
    page,
    logger,
    'Acconsento comunicazioni di marketing anche dopo la cessazione del presente contratto',
    mappingConsenso(primoConsenso)
  );

  await selectComboboxV2(
    page,
    logger,
    'Acconsento all’attività di profilazione',
    mappingConsenso(secondoConsenso)
  );

  // await selectComboboxV2(
  //   page,
  //   logger,
  //   'Non desidero ricevere comunicazioni commerciali',
  //   mappingDesidero(terzoConsenso)
  // );
}
