import inputText from '../common/controls/input';
import { StepContext } from '../common/helpers/steps';
import { IberdrolaInserimentoV2Payload } from '../payload/IberdrolaInserimentoV2Payload';
import { selectComboboxV2 } from '../../../browser/salesforce/lightining-components/controls/combobox';

export default async function stepFornitura({ page, logger }: StepContext, payload: IberdrolaInserimentoV2Payload): Promise<void> {
  const { tipoEnergia, pod, pdr } = payload;

  await selectComboboxV2(page, logger, 'Tipo di Energia', tipoEnergia);

  if (tipoEnergia === 'elettricita' || tipoEnergia === 'dual') {
    await inputText(page, logger, 'POD', pod);
  }
  if (tipoEnergia === 'gas' || tipoEnergia === 'dual') {
    await inputText(page, logger, 'PDR', pdr);
  }
}
