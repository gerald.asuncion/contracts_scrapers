/* eslint-disable max-len */
import isXPathVisible from '../../../browser/page/isXPathVisible';
import selectAutocomplete, { autocompleteContainerExpression, selezionaFornitoreDistributore } from '../common/controls/autocomplete';
import inputText from '../common/controls/input';
import { StepContext } from '../common/helpers/steps';
import {
  mappingClassePrelievo, mappingPotenzaDisponibile, mappingResidenza, mappingTitolaritaImmobile, mappingUtilizzoGas
} from '../mapping/inserimento';
import { IberdrolaInserimentoV2Payload } from '../payload/IberdrolaInserimentoV2Payload';
import tieniSoloIlCodice from './tieniSoloIlCodice';
import { selectComboboxV2 } from '../../../browser/salesforce/lightining-components/controls/combobox';

export default async function stepDettaglio({ page, logger }: StepContext, payload: IberdrolaInserimentoV2Payload): Promise<void> {
  const { titolaritaImmobile, tipoEnergia, indirizzoResidenza } = payload;

  await selectComboboxV2(page, logger, 'Residenza', mappingResidenza(indirizzoResidenza));
  await selectComboboxV2(page, logger, 'Titolarità sull’immobile', mappingTitolaritaImmobile(titolaritaImmobile));

  // TODO basarsi sulla visibilità non sul mapping
  if (tipoEnergia === 'gas' || tipoEnergia === 'dual') {
    const {
      consumoGas,
      attualeFornitoreGas,
      distributoreGas,
      utilizzoGas
    } = payload as Required<IberdrolaInserimentoV2Payload>;

    await inputText(page, logger, 'Consumo annuo Gas (Smc)', consumoGas);

    await selezionaFornitoreDistributore(page, 'Fornitore Gas', tieniSoloIlCodice(attualeFornitoreGas), attualeFornitoreGas);
    await selezionaFornitoreDistributore(page, 'Distributore Gas', tieniSoloIlCodice(distributoreGas), distributoreGas);

    await selectComboboxV2(page, logger, 'Codice Profilo', mappingUtilizzoGas(utilizzoGas));
    await selectComboboxV2(page, logger, 'Classe prelievo', mappingClassePrelievo());
  }

  // TODO basarsi sulla visibilità non sul mapping
  if (tipoEnergia === 'elettricita' || tipoEnergia === 'dual') {
    const {
      consumoLuce,
      tensione,
      potenzaImpegnata,
      attualeFornitoreElettricita,
      distributoreElettricita
    } = payload as Required<IberdrolaInserimentoV2Payload>;

    await inputText(page, logger, 'Consumo annuo Elettricità (kWh)', consumoLuce);
    await selectComboboxV2(page, logger, 'Tensione (V)', tensione);

    await inputText(page, logger, 'Potenza impegnata (kW)', potenzaImpegnata.toString());
    await inputText(page, logger, 'Potenza disponibile (kW)', mappingPotenzaDisponibile(potenzaImpegnata).toString());

    await selezionaFornitoreDistributore(page, 'Fornitore Elettricità', tieniSoloIlCodice(attualeFornitoreElettricita), attualeFornitoreElettricita);
    if (await isXPathVisible(page, autocompleteContainerExpression('Distributore Elettricità'))) {
      await selectAutocomplete(page, logger, 'Distributore Elettricità', distributoreElettricita);
    }
  }
}
