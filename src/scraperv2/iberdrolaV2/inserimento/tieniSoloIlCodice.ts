export const FORNITORE_DISTRIBUTORE_CODICE_SEP = ' - ';

export default function tieniSoloIlCodice(fornitoreDistributore: string): string {
  const parti = fornitoreDistributore.split(FORNITORE_DISTRIBUTORE_CODICE_SEP);
  return parti.length === 1 ? fornitoreDistributore : parti[0];
}
