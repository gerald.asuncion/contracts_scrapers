import waitForTimeout from '../../../browser/page/waitForTimeout';
import waitForXPathAndClick from '../../../browser/page/waitForXPathAndClick';
import inputDatepicker from '../common/controls/datepicker';
import inputText, { isInputVisible } from '../common/controls/input';
import { expressionContainsClass, expressionContainsText } from '../common/helpers/selectors';
import { StepContext } from '../common/helpers/steps';
import {
  mappingAccreditamentoAgente, mappingMetodoDiPagamento, mappingModalitaInvioFattura, mappingPaeseDomiciliazione
} from '../mapping/inserimento';
import { IberdrolaInserimentoV2Payload } from '../payload/IberdrolaInserimentoV2Payload';
import { selectComboboxV2 } from '../../../browser/salesforce/lightining-components/controls/combobox';

const PRODOTTO_SERVIZIO_SEP = '+';

export default async function stepFatturazione({ page, logger }: StepContext, payload: IberdrolaInserimentoV2Payload): Promise<void> {
  // fatturazione e pagamento

  const {
    tipoEnergia,
    modalitaInvioFattura,
    modalitaPagamento,
    emailRappresentante
  } = payload;

  await selectComboboxV2(page, logger, 'Fatturazione elettronica', mappingModalitaInvioFattura(modalitaInvioFattura));

  // await selectComboboxV2(page, logger, 'Modalità di invio del contratto', mappingModalitaInvioContratto(modalitaInvioContratto));
  await selectComboboxV2(page, logger, 'Metodo di Pagamento', mappingMetodoDiPagamento(modalitaPagamento));

  await waitForTimeout(page, 500);

  if (await isInputVisible(page, 'Email')) {
    await inputText(page, logger, 'Email', emailRappresentante);
  }

  // conto bancario

  const sectionConto = await page.$x(`//div${expressionContainsClass('slds-section')}${expressionContainsText('Conto bancario')}`);

  if (sectionConto.length) {
    const {
      iban
    } = payload;
    const paeseDomiciliazione = 'ITA';
    // const bic = 'XXXXXX';

    await selectComboboxV2(page, logger, 'Paese domiciliazione', mappingPaeseDomiciliazione(paeseDomiciliazione));
    // await inputText(page, logger, 'Bic', bic);
    await inputText(page, logger, 'IBAN', iban);
  }

  // condizioni generali

  const {
    codiceABarre,
    dataCompletato
  } = payload;

  const data = `${dataCompletato.substr(0, 2)}/${dataCompletato.substr(2, 2)}/${dataCompletato.substr(4, 4)}`;

  await inputText(page, logger, "Accreditamento dell'agente", mappingAccreditamentoAgente());
  await inputText(page, logger, 'Codice a Barre', codiceABarre);
  await inputDatepicker(page, logger, 'Data di firma', data);

  await waitForTimeout(page, 3000);

  // prodotti/servizi

  const {
    prodottoGas,
    prodottoEnergia
  } = payload;

  if (tipoEnergia === 'elettricita' || tipoEnergia === 'dual') {
    const contrattoEnergia = '22-03A-DINDX';
    const [prodottoVasLuce, servizioVasLuce] = (prodottoEnergia || '').split(PRODOTTO_SERVIZIO_SEP);

    await selectComboboxV2(page, logger, 'Condizioni Economiche Contrattuali Elettricità', contrattoEnergia, { forceEvaluate: true });
    await selectComboboxV2(page, logger, 'Prodotti Elettricità', prodottoVasLuce);

    await waitForXPathAndClick(page, '//lightning-button-icon[@data-energy-type="electric"]/button');

    await waitForTimeout(page, 100);

    if (servizioVasLuce) {
      await selectComboboxV2(page, logger, 'Servizi', servizioVasLuce);
      await waitForXPathAndClick(page, '//slot/lightning-button-icon[not(@data-energy-type)]/button');
    }

    await waitForTimeout(page, 100);
  }
  if (tipoEnergia === 'gas' || tipoEnergia === 'dual') {
    const contrattoGas = '22-03A-DINDX';
    const [prodottoVasGas, servizioVasGas] = (prodottoGas || '').split(PRODOTTO_SERVIZIO_SEP);

    await selectComboboxV2(page, logger, 'Condizioni Economiche Contrattuali Gas', contrattoGas, { forceEvaluate: true });
    await selectComboboxV2(page, logger, 'Prodotti Gas', prodottoVasGas);

    await waitForXPathAndClick(page, '//lightning-button-icon[@data-energy-type="gas"]/button');

    await waitForTimeout(page, 100);

    if (servizioVasGas) {
      await selectComboboxV2(page, logger, 'Servizi', servizioVasGas);
      await waitForXPathAndClick(page, '//slot/lightning-button-icon[not(@data-energy-type)]/button');
    }

    await waitForTimeout(page, 100);
  }
}
