/**
 * @openapi
 *
 * /scraper/v2/iberdrolaV2/check/firme:
 *  post:
 *    tags:
 *      - v2
 *    description: Controlla lo stato dei contratti sul portale di Iberdrola.
 *      Questa è la rotta diretta allo scraper, quindi non usa il meccanismo del webhook.
 *    consume:
 *      - application/json
 *    produces:
 *      - application/json
 *    responses:
 *      200:
 *        description: SuccessResponse
 *    parameters:
 *      - in: body
 *        schema:
 *          type: object
 *          required:
 *            - codiceCupsArray
 *          properties:
 *            codiceCupsArray:
 *              type: array
 *              items:
 *                type: string
 *
 * /scraper/queue/iberdrolaCheckFirme:
 *  post:
 *    tags:
 *      - v2
 *      - queue
 *    description:
 *      Controlla lo stato dei contratti sul portale di Iberdrola.
 *      Il payload è lo stesso della rotta diretta `/scraper/v2/iberdrolaV2/check/firme`,
 *      in più va aggiunto il webhook che lo scraper poi andrà a richiamare per notificare l'esito.
 *    consume:
 *      - application/json
 *    produces:
 *      - application/json
 *    responses:
 *      200:
 *        description: SuccessResponse
 */
