import { Page } from 'puppeteer';
import waitForXPathAndType from '../../../../browser/page/waitForXPathAndType';
import waitForXPathHidden from '../../../../browser/page/waitForXPathHidden';
import tryOrThrow from '../../../../utils/tryOrThrow';

export default async function stepControlloCodiceFiscale(page: Page, codiceFiscale: string): Promise<string> {
  await tryOrThrow(
    async () => {
      await waitForXPathAndType(page, '//input[contains(@name,"Solvencia__c-search-input")]', codiceFiscale);
      await page.keyboard.press('Enter');
    },
    'non sono riuscito ad inserire il codice fiscale per controllarne lo stato'
  );

  await waitForXPathHidden(page, '//div[contains(@class,"spinner_container")]');

  const stato = await tryOrThrow(
    async () => {
      await page.waitForXPath('//table/tbody/tr[1]');
      const cell = await page.waitForXPath('//table/tbody/tr[1]/td[5]');
      const statoInterno: string = await page.evaluate((el) => el.textContent, cell);
      return statoInterno;
    },
    'non sono riuscito a recuperare lo stato dalla tabella'
  );
  return stato.toLowerCase();
}
