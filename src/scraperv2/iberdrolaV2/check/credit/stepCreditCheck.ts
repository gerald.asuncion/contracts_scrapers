import { Page } from 'puppeteer';
import waitForXPathAndClick from '../../../../browser/page/waitForXPathAndClick';
import { selectComboboxV2 } from '../../../../browser/salesforce/lightining-components/controls/combobox';
import { Logger } from '../../../../logger';
import tryOrThrow from '../../../../utils/tryOrThrow';
import inputText from '../../common/controls/input';
import { IberdrloaCreditCheckV2Payload } from '../../payload/IberdrloaCreditCheckV2Payload';

const PRODOTTO_TO_ENERGIA: Record<string, string> = {
  luce: 'Elettricità',
  gas: 'Gas',
  dual: 'Dual'
};

export default async function stepCreditCheck(page: Page, logger: Logger, {
  tipoFornitura,
  prodotto,
  cap,
  codiceFiscale,
  // partitaIva,
  potenzaDisponibile
}: IberdrloaCreditCheckV2Payload): Promise<void> {
  await tryOrThrow(
    () => selectComboboxV2(page, logger, 'Tipo di operazione', "Credit Check"),
    'non sono riuscito a selezionare il tipo di cliente'
  );
  await tryOrThrow(
    () => selectComboboxV2(page, logger, 'Tipo di cliente', tipoFornitura),
    'non sono riuscito a selezionare il tipo di cliente'
  );
  // TODO: forse va reintegrato l'uso di xPathPre
  await tryOrThrow(
    () => selectComboboxV2(page, logger, 'Tipo di Energia', PRODOTTO_TO_ENERGIA[prodotto.toLowerCase()]/* , '//lightning-layout/slot/lightning-layout-item[2]' */),
    'non sono riuscito a selezionare il tipo di energia'
  );
  await tryOrThrow(
    () => inputText(page, logger, 'CAP', cap),
    'non sono riuscito ad inserire il cap'
  );
  await tryOrThrow(
    () => inputText(page, logger, 'Codice Fiscale', codiceFiscale),
    'non sono riuscito ad inserire il codice fiscale'
  );
  // await tryOrThrow(
  //   () => inputText(page, logger, 'Partita IVA', partitaIva),
  //   'non sono riuscito ad inserire la partita iva'
  // );
  if (['luce', 'dual'].includes(prodotto) && potenzaDisponibile) {
    await tryOrThrow(
      () => inputText(page, logger, 'Potenza disponible kW', potenzaDisponibile),
      'non sono riuscito ad inserire la potenza disponibile'
    );
  }
  await tryOrThrow(
    () => selectComboboxV2(page, logger, 'Domiciliato', 'No'),
    'non sono riuscito a selezionare `No` per il domicilio'
  );
  await tryOrThrow(
    () => selectComboboxV2(page, logger, 'Fattura elettronica', 'Si'),
    'non sono riuscito a selezionare `Si` per la fattura elettronica'
  );
  await tryOrThrow(
    () => waitForXPathAndClick(page, '//button[contains(@name,"obtenerCreditCheck")]'),
    'non sono riuscito a cliccare sul pulsante per effettuare il controllo'
  );
}
