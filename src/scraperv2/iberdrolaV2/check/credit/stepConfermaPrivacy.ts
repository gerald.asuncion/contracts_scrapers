import { Page } from 'puppeteer';
import waitForXPathAndClick from '../../../../browser/page/waitForXPathAndClick';
import { Logger } from '../../../../logger';
import clickButton from '../../common/controls/button';

export default async function stepConfermaPrivacy(page: Page, logger: Logger): Promise<void> {
  await clickButton(page, logger, 'Confermare');

  await page.waitForXPath(
    '//p[contains(.,"CREDIT CHECK")]',
    { visible: true, timeout: 30000 }
  );
  await waitForXPathAndClick(page, '//button[contains(.,"OK")]');
}
