import { Page } from 'puppeteer';
import tryOrThrow from '../../../../utils/tryOrThrow';
import waitForXPathVisible from '../../../../browser/page/waitForXPathVisible';

export default async function stepRecuperoEsito(page: Page): Promise<string> {
  await tryOrThrow(() => waitForXPathVisible(page, '//div[contains(@class,"siteforceContentArea")]'), 'non sono riuscito ad attendere il caricamento della pagina contenente l\'esito');

  const esito: string | undefined = await tryOrThrow(
    async () => {
      const el = await page.waitForXPath('//div[contains(@class,"slds-section__content section__content")]//span[contains(.,"Risultato")]/parent::div/following-sibling::div//span//span');
      return await (await el?.getProperty('textContent'))?.jsonValue<string>();
    },
    'non sono riuscito a recuperare l\'esito'
  );

  return (esito as string).toLowerCase();
}
