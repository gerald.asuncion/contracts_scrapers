import { Page } from 'puppeteer';
import waitFormLoading from '../../common/helpers/loading';
import { Logger } from '../../../../logger';
import { STATO_CONTRATTO } from '../../../../contratti/StatoContratto';
import waitForVisible from '../../../../browser/page/waitForVisible';
import waitForSelectorAndType from '../../../../browser/page/waitForSelectorAndType';
import getElementText from '../../../../browser/page/getElementText';
import saveScreenshotOnAwsS3 from '../../../../browser/page/saveScreenshotOnAwsS3';
import { S3ClientFactory, S3ClientFactoryResult } from '../../../../aws/s3ClientFactory';
import createFilenameForScreenshot from '../../../../utils/createFilenameForScreenshot';

export type StatoContrattoResult = {
  codiceContratto: string;
  stato: string;
};

const mappa: Record<string, string> = {
  'in attesa di informazioni': STATO_CONTRATTO.PRONTO,
  'in attesa sms edatalia': STATO_CONTRATTO.DA_FIRMARE,
  'in attesa accettazione cliente': STATO_CONTRATTO.DA_FIRMARE,
  'offerta avanzata': STATO_CONTRATTO.FIRMATO,
  'offerta sottoscritta': STATO_CONTRATTO.FIRMATO,
  'approvazione attiva': STATO_CONTRATTO.FIRMATO,
  'offerta contrattata': STATO_CONTRATTO.FIRMATO,
  'ko non recuperabile delta': STATO_CONTRATTO.FIRMATO
};

const inputSearchSelector = 'input[title^="Ricerca"]';

async function clearInput(page: Page, selector: string) {
  await page.focus(selector);
  await page.keyboard.press('Backspace');
  const text = await getElementText(page, selector, 'value');
  if (text) {
    await clearInput(page, selector);
  }
}

export default async function checkStatoCodiceCup(page: Page, logger: Logger, codiceContratto: string, s3clientFactory: S3ClientFactoryResult): Promise<StatoContrattoResult> {
  await clearInput(page, inputSearchSelector);
  await waitForSelectorAndType(page, inputSearchSelector, codiceContratto);
  await page.keyboard.press('Enter');
  await waitFormLoading(page, logger);

  let stato: string = STATO_CONTRATTO.DA_FIRMARE;
  try {
    // controllo se è apparso il messaggio `nessun elemento da mostrare`
    await waitForVisible(page, '.noResults', { timeout: 3000 });
    logger.warn(`per il contratto ${codiceContratto} non è apparso il risultato sul portale`);
  } catch (ex) {
    // non è apparso posso recuperare lo stato
    const [span] = await page.$x('//div[contains(@class, "resultsItem")]//div[contains(@class, "grid")]//div[contains(@class,"searchScroller")]//div//table//tbody//tr//td[2]//span');
    const statoIberdrola = span ? await page.evaluate((el) => el.textContent, span) as string : '';
    stato = mappa[statoIberdrola.toLowerCase()] || STATO_CONTRATTO.DA_FIRMARE;

    if (stato === STATO_CONTRATTO.DA_FIRMARE) {
      await saveScreenshotOnAwsS3(s3clientFactory, page, createFilenameForScreenshot('IberdrolaCheckFirmeV2', codiceContratto), logger);
    }
  }

  return {
    codiceContratto,
    stato
  };
}
