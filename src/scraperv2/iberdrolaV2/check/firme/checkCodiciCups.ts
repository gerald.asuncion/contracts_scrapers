import { Page } from 'puppeteer';
import { Logger } from '../../../../logger';
import checkStatoCodiceCup, { StatoContrattoResult } from './checkStatoCodiceCup';
import { S3ClientFactoryResult } from '../../../../aws/s3ClientFactory';

export default async function* checkCodiciCups(page: Page, logger: Logger, cups: string[], s3clientFactory: S3ClientFactoryResult): AsyncGenerator<StatoContrattoResult> {
  const cup = cups.pop();

  if (cup) {
    const result = await checkStatoCodiceCup(page, logger, cup, s3clientFactory);
    yield result;
  }

  if (cups.length) {
    yield* checkCodiciCups(page, logger, cups, s3clientFactory);
  }
}
