/**
 * @openapi
 *
 * /scraper/v2/iberdrolaV2/fornitori/gas:
 *  post:
 *    tags:
 *      - v2
 *    description: Restituisce la lista di fornitori gas per Iberdrola.
 *      Questa è la rotta diretta allo scraper, quindi non usa il meccanismo del webhook.
 *      Il payload da passare è lo stesso di `/scraper/v2/iberdrolaV2/inserimento`.
 *    consume:
 *      - application/json
 *    produces:
 *      - application/json
 *    responses:
 *      200:
 *        description: SuccessResponse
 *
 * /scraper/queue/iberdrolaFornitoriGas:
 *  post:
 *    tags:
 *      - v2
 *      - queue
 *    description:
 *      Restituisce la lista di fornitori gas per Iberdrola.
 *      Il payload è lo stesso della rotta diretta `/scraper/v2/iberdrolaV2/fornitori/gas`,
 *      in più va aggiunto il webhook che lo scraper poi andrà a richiamare per notificare l'esito.
 *    consume:
 *      - application/json
 *    produces:
 *      - application/json
 *    responses:
 *      200:
 *        description: SuccessResponse
 */
