import { QueueTask } from '../../task/QueueTask';
import IberdrolaEstrattoreV2 from './estrattore/IberdrolaEstrattoreV2';

@QueueTask({ scraperName: 'iberdrolaFornitoriLuce' })
export default class IberdrolaFornitoriLuceV2 extends IberdrolaEstrattoreV2 {
  controlLabel = 'Fornitore Elettricità';
}
