import { QueueTask } from '../../task/QueueTask';
import IberdrolaEstrattoreV2 from './estrattore/IberdrolaEstrattoreV2';

@QueueTask({ scraperName: 'iberdrolaFornitoriGas' })
export default class IberdrolaFornitoriGasV2 extends IberdrolaEstrattoreV2 {
  controlLabel = 'Fornitore Gas';
}
