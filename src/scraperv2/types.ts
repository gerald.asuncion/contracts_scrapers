import { ScraperResponse } from './scraper';

export interface Scraper {
  login(args: any | undefined): Promise<void>;

  scrape(args: any, params?: any): Promise<ScraperResponse>;
}
