import { ScraperResponse } from '../../scraper';
import FailureResponse from '../../../response/FailureResponse';
import SuccessResponse from '../../../response/SuccessResponse';
import DbScraper, { DbScraperOptions } from '../../DbScraper';
import createChildLogger from '../../../utils/createChildLogger';
import { agsmGasCoperturaRepo } from './AgsmGasCoperturaRepo';
import extractErrorMessage from '../../../utils/extractErrorMessage';
import aggiustaComune from './aggiustaComune';
import { SCRAPER_TIPOLOGIA } from '../../ScraperOptions';

const createErrNoCoperturaIrenMessage = (
  comune: string,
  provincia: string
) => `il Comune "${comune}" (${provincia}) non è coperto dalla fornitura gas di Agsm`;

type AgsmGasCoperturaPayload = {
  comune: string;
  provincia: string;
  tipologia: string;
};

export default class AgsmGasCopertura extends DbScraper {
  constructor(options: DbScraperOptions) {
    super(options);
    this.logger = createChildLogger(options.logger, 'AgsmGasCopertura');
  }

  /**
   * @override
   * @param args payload ricevuto dal chiamante
   */
  async scrape({ comune, provincia, tipologia }: AgsmGasCoperturaPayload): Promise<ScraperResponse> {
    let result;
    const noCoperturaErrMsg = createErrNoCoperturaIrenMessage(comune, provincia);
    try {
      const repo = agsmGasCoperturaRepo({ mysqlPool: this.mysqlPool });
      const record = await repo.getRecord<any>(aggiustaComune(comune), provincia);

      if (record[tipologia] === 'NO') {
        throw new Error(noCoperturaErrMsg);
      }

      result = new SuccessResponse('OK');
    } catch (ex) {
      const errMsg = extractErrorMessage(ex);
      if (errMsg === 'no result') {
        result = new FailureResponse(noCoperturaErrMsg);
      } else {
        result = new FailureResponse(errMsg);
      }
    }

    return result;
  }

  getScraperCodice() {
    return 'agsmgas-copertura';
  }

  getScraperTipologia() {
    return SCRAPER_TIPOLOGIA.FATTIBILITA;
  }
}
