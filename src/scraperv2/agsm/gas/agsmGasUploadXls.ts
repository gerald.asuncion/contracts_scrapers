import path from 'path';
import { Router, Request, Response } from 'express';
import multer from 'multer';
import { Logger } from '../../../logger';
import createChildLogger from '../../../utils/createChildLogger';
import extractErrorMessage from '../../../utils/extractErrorMessage';
import FailureResponse from '../../../response/FailureResponse';
import SuccessResponse from '../../../response/SuccessResponse';
import AgsmGasImporterRepo from '../../../repo/AgsmGasImporterRepo';
import parseXlsxToCsv from '../../areti/parseXlsxToCsv';
import emptyDirectory from '../../../utils/emptyDirectory';
import creaCartellaDownloadAgsm from './createCartellaDownloadAgsm';

interface AgsmGasUploadOptions {
  logger: Logger;
  router: Router;
  baseDownloadPath: string;
  agsmGasImporterRepo: AgsmGasImporterRepo;
}

export default function agsmGasUploadXls({
  logger,
  router,
  baseDownloadPath,
  agsmGasImporterRepo
}: AgsmGasUploadOptions): void {
  const myLogger = createChildLogger(logger, 'AgsmGasUploadXls');

  const downloadDir = creaCartellaDownloadAgsm(baseDownloadPath);
  const upload = multer({ dest: downloadDir });

  myLogger.info('registro agsmGasUploadXls alla rotta `/agsm/gas/upload/xls`');
  router.post('/agsm/gas/upload/xls', upload.single('xls'), async (req: Request, res: Response) => {
    const xlsFilePath = req.file?.path as string;
    myLogger.info(`Ricevuto file ${xlsFilePath}`);

    try {
      myLogger.info('lo converto in csv');
      const parsedCsvPath = path.join(downloadDir, 'iren-remi-parsed.csv');
      const { outputFileName } = await parseXlsxToCsv(xlsFilePath, parsedCsvPath);
      myLogger.info(`creato file csv ${outputFileName}`);

      myLogger.info('salvo i dati del file nel database');
      await agsmGasImporterRepo.loadFileIntoDatabase(outputFileName);

      myLogger.info('svuoto la cartella di download');
      emptyDirectory(downloadDir);

      res.json(new SuccessResponse());
    } catch (ex) {
      const errMsg = extractErrorMessage(ex);
      myLogger.error(errMsg);
      res.json(new FailureResponse(errMsg));
    }
  });
}
