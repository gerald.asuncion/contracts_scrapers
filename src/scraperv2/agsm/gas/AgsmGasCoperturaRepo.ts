import { MysqlPoolInterface, MysqlArgs } from '../../../mysql';

const SQL = 'SELECT * FROM agsm_gas_copertura WHERE comune = ? AND provincia = ?;';

export default class AgsmGasCoperturaRepo {
  private mysqlPool: MysqlPoolInterface;

  constructor(options: MysqlArgs) {
    this.mysqlPool = options.mysqlPool;
  }

  async getRecord<T = unknown>(comune: string, provincia: string): Promise<T> {
    return this.mysqlPool.queryOne(SQL, [comune, provincia]);
  }
}

export function agsmGasCoperturaRepo(options: MysqlArgs): AgsmGasCoperturaRepo {
  return new AgsmGasCoperturaRepo(options);
}
