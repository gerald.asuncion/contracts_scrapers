import { Router, Request, Response } from 'express';
import multer from 'multer';
import { Logger } from '../../../logger';
import createChildLogger from '../../../utils/createChildLogger';
import extractErrorMessage from '../../../utils/extractErrorMessage';
import FailureResponse from '../../../response/FailureResponse';
import SuccessResponse from '../../../response/SuccessResponse';
import AgsmGasImporterRepo from '../../../repo/AgsmGasImporterRepo';
import creaCartellaDownloadAgsm from './createCartellaDownloadAgsm';

interface AgsmGasUploadOptions {
  logger: Logger;
  router: Router;
  baseDownloadPath: string;
  agsmGasImporterRepo: AgsmGasImporterRepo;
}

export default function agsmGasUploadCsv({
  logger,
  router,
  baseDownloadPath,
  agsmGasImporterRepo
}: AgsmGasUploadOptions): void {
  const myLogger = createChildLogger(logger, 'AgsmGasUploadCsv');

  const downloadDir = creaCartellaDownloadAgsm(baseDownloadPath);

  const upload = multer({ dest: downloadDir });

  myLogger.info('registro agsmGasUploadCsv alla rotta `/agsm/gas/upload/csv`');
  router.post('/agsm/gas/upload/csv', upload.single('csv'), async (req: Request, res: Response) => {
    const csvFilePath = req.file?.path as string;
    myLogger.info(`Ricevuto file ${csvFilePath}`);

    try {
      myLogger.info('salvo i dati del file nel database');
      await agsmGasImporterRepo.loadFileIntoDatabase(csvFilePath);

      myLogger.info('dati salvati nel database');
      res.json(new SuccessResponse());
    } catch (ex) {
      const errMsg = extractErrorMessage(ex);
      myLogger.error(errMsg);
      res.json(new FailureResponse(errMsg));
    }
  });
}
