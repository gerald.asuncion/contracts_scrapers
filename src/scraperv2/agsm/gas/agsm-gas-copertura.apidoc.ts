/**
 * @openapi
 *
 * /scraper/v2/agsm/gas/copertura:
 *  post:
 *    tags:
 *      - v2
 *    description: Effettua il controllo sulla copertura da parte di Agsm nel comune interessato.
 *    consume:
 *      - application/json
 *    produces:
 *      - application/json
 *    responses:
 *      200:
 *        description: SuccessResponse
 *    parameters:
 *      - in: body
 *        schema:
 *          type: object
 *          required:
 *            - comune
 *            - provincia
 *            - tipologia
 *          properties:
 *            comune:
 *              type: string
 *            provincia:
 *              type: string
 *            tipologia:
 *              type: string
 */
