import path from 'path';
import fs from 'fs';
import getCurrentDayFormatted from '../../../utils/getCurrentDayFormatted';

export default function creaCartellaDownloadAgsm(baseDownloadPath: string): string {
  const todayDir = path.resolve(baseDownloadPath, getCurrentDayFormatted());

  if (!fs.existsSync(todayDir)) {
    fs.mkdirSync(todayDir);
  }

  const downloadDir = path.resolve(todayDir, 'agsm');
  if (!fs.existsSync(downloadDir)) {
    fs.mkdirSync(downloadDir);
  }

  return downloadDir;
}
