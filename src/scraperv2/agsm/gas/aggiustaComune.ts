const AGSM_COMUNE: Record<string, string> = {
  'reggio calabria': 'reggio di calabria',
  'reggio emilia': "reggio nell'emilia"
};

export default function aggiustaComune(comune: string): string {
  const lowered: string = comune.toLowerCase();

  return AGSM_COMUNE[lowered] || lowered;
}
