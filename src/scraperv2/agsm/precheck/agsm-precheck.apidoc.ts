/**
 * @openapi
 *
 * /scraper/v2/agsm/precheck:
 *  post:
 *    tags:
 *      - v2
 *    description: Effettua il controllo del pod/pdr interessato sul portale di Agsm.
 *    consume:
 *      - application/json
 *    produces:
 *      - application/json
 *    responses:
 *      200:
 *        description: SuccessResponse
 *    parameters:
 *      - in: body
 *        schema:
 *          type: object
 *          required:
 *            - prodotto
 *          properties:
 *            prodotto:
 *              type: string
 *              enum: [luce, gas]
 *            pod:
 *              type: string
 *              nullable: true
 *              description: necessario se prodotto luce
 *            pdr:
 *              type: string
 *              nullable: true
 *              description: necessario se prodotto gas
 *            dataDecorrenza:
 *              type: string
 *              nullable: true
 *              description: necessario se prodotto luce
 *              format: dd/mm/yyyy
 *            codiceFiscale:
 *              type: string
 *              nullable: true
 *              description: presente per contratto di tipo privato, esclusivo rispetto il 'pIva'
 *            pIva:
 *              type: string
 *              nullable: true
 *              description: presente per contratto di tipo business, esclusivo rispetto il 'codiceFiscale'
 */
