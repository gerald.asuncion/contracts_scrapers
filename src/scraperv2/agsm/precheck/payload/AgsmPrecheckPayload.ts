export type AgsmPrecheckPayload = {
  /**
   * tipologia di prodotto
   * @docApiType string
   */
  prodotto: 'luce' | 'gas';
  /**
   * presente e valorizzato solo per 'prodotto = luce'
   * esclusivo rispetto il 'pdr'
   */
  pod?: string;
  /**
   * presente e valorizzato solo per 'prodotto = gas'
   * esclusivo rispetto il 'pod'
   */
  pdr?: string;
  /**
   * presente e valorizzato solo per 'prodotto = luce'
   * format dd/mm/yyyy
   * es: 09/07/2021
   */
  dataDecorrenza?: string;

  /**
   * presente per contratto di tipo privato
   * esclusivo rispetto il 'pIva'
   */
  codiceFiscale?: string;
  /**
   * presente per contratto di tipo business
   * esclusivo rispetto il 'codiceFiscale'
   */
  pIva?: string;
};
