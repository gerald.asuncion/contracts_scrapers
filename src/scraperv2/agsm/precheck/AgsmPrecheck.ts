import { ElementHandle } from 'puppeteer';
import waitForNavigation from '../../../browser/page/waitForNavigation';
import waitForSelectorAndClick from '../../../browser/page/waitForSelectorAndClick';
import waitForSelectorAndRun from '../../../browser/page/waitForSelectorAndRun';
import waitForSelectorAndType from '../../../browser/page/waitForSelectorAndType';
import waitForVisible from '../../../browser/page/waitForVisible';
import waitForXPathAndClick from '../../../browser/page/waitForXPathAndClick';
import waitForXPathAndRun from '../../../browser/page/waitForXPathAndRun';
import ErrorResponse from '../../../response/ErrorResponse';
import FailureResponse from '../../../response/FailureResponse';
import SuccessResponse from '../../../response/SuccessResponse';
import { ScraperResponse, WebScraper } from '../../scraper';
import { AgsmPrecheckPayload } from './payload/AgsmPrecheckPayload';
import { SCRAPER_TIPOLOGIA } from '../../ScraperOptions';
import stripNonNumeric from '../../../utils/stripNonNumeric';
import waitCheckCredentials from '../../../browser/page/waitCheckCredentials';
import waitForXPathVisible from '../../../browser/page/waitForXPathVisible';
import WrongCredentialsError from '../../../errors/WrongCredentialsError';

function containsWords(text: string, ...words: string[]): boolean {
  return text.match(new RegExp(`\\b(${words.join('|')})\\b`, 'gi'))?.length === words.length;
}

export default class AgsmPrecheck extends WebScraper<AgsmPrecheckPayload> {
  static readonly LOGIN_URL = 'https://servizionline.agsm.it/retivendita/Login.aspx?ReturnUrl=%2fRetiVendita';

  static readonly config = {
    navBarLinkContent: 'Pre check contratto',
    reportSelectorLuce: 'Pre-Check EE',
    reportSelectorGas: 'Pre-Check GAS'
  };

  async login(): Promise<void> {
    const logger = this.childLogger;
    const { username, password } = await this.getCredenziali();
    const page = await this.p();

    await page.goto(AgsmPrecheck.LOGIN_URL, { waitUntil: 'networkidle2' });

    await waitForSelectorAndType(page, '#MainContent_Login1_UserName', username);
    logger.info('login > inserito username');

    await waitForSelectorAndType(page, '#MainContent_Login1_Password', password);
    logger.info('login > inserita password');

    await Promise.all([
      waitForNavigation(page),
      waitForSelectorAndClick(page, '#MainContent_Login1_LoginButton')
    ]);

    let err: string | undefined;
    try {
      const invalidCredentialsSelector = '//input[@id = "MainContent_Login1_LoginButton"]/parent::div/following-sibling::div//b';
      await waitForXPathVisible(page, invalidCredentialsSelector);
      const [el] = await page.$x(invalidCredentialsSelector);

      err = await (await el.getProperty('textContent')).jsonValue();
    } catch (ex) {
      // errore credenziali non valide non apparso
      // nulla da fare
    }

    if (err) {
      logger.error(`login fallita ${err.trim()}`);
      throw new WrongCredentialsError(AgsmPrecheck.LOGIN_URL, username);
    }

    logger.info('login > terminata');
  }

  async scrapeWebsite({
    prodotto, pod, pdr, dataDecorrenza, codiceFiscale, pIva
  }: AgsmPrecheckPayload): Promise<ScraperResponse> {
    const logger = this.childLogger;
    const page = await this.p();

    await (await page.waitForXPath('//*[@id="menu"]/li[3]/a'))?.hover();

    await Promise.all([
      waitForNavigation(page),
      waitForXPathAndClick(
        page,
        `//*[@id="menu"]//a[contains(., '${AgsmPrecheck.config.navBarLinkContent}')]`
      )
    ]);

    // select prodotto
    logger.info('scraper > selecting prodotto');

    await waitForXPathAndClick(page, '//*[@id="MainContent_ReportSII_ddlReport_chzn"]/a/span');
    logger.info('scraper > selecting "prodotto" - opened autocomplete');

    let podORpdr: string;

    switch (prodotto) {
      case 'luce':
        await waitForXPathAndClick(
          page,
          `//*[@id="MainContent_ReportSII_ddlReport_chzn"]//li[contains(., '${AgsmPrecheck.config.reportSelectorLuce}')]`
        );

        await waitForSelectorAndClick(page, '#MainContent_ReportSII_txtDataDecorrenza');
        await waitForSelectorAndRun(page, '#MainContent_ReportSII_txtDataDecorrenza', async (handle: ElementHandle<HTMLInputElement> | null) => {
          await handle?.evaluate((element) => {
            element.setSelectionRange(0, 0);
          });
        });
        await waitForSelectorAndType(page, '#MainContent_ReportSII_txtDataDecorrenza', stripNonNumeric(dataDecorrenza as string));

        podORpdr = pod as string;
        break;
      case 'gas':
        await waitForXPathAndClick(
          page,
          `//*[@id="MainContent_ReportSII_ddlReport_chzn"]//li[contains(., '${AgsmPrecheck.config.reportSelectorGas}')]`
        );

        podORpdr = pdr as string;
        break;
      default:
        throw new Error(`Prodotto non implementato: '${prodotto}'`);
    }
    logger.info(`scraper > selecting "prodotto" - selected prodotto inside autocomplete '${prodotto}'`);

    await waitForSelectorAndType(page, '#MainContent_ReportSII_txtPOD', podORpdr);

    if (codiceFiscale) {
      // codiceFiscale => contratto privati
      await waitForSelectorAndType(page, '#MainContent_ReportSII_txtCfCf', codiceFiscale);
    } else {
      // pIva => contratto business
      await waitForSelectorAndType(page, '#MainContent_ReportSII_txtPivaCf', pIva as string);
    }

    await waitForSelectorAndClick(page, '#MainContent_ReportSII_rblCfStraniero_0');

    await Promise.all([
      // waitForNavigation(page),
      waitForVisible(page, '#MainContent_ReportSII_divRis', {
        timeout: 60000
      }),
      waitForSelectorAndClick(page, '#MainContent_ReportSII_btnElabora')
    ]);

    const resultText = (await waitForXPathAndRun(
      page,
      '//*[@id="MainContent_ReportSII_divRis"]/table/tbody/tr[1]/td[2]/text()',
      async (p, elHandle) => elHandle?.evaluate((el) => el.textContent)
    ) || '')
      .replace(/\n/g, ' ')
      .trim();

    if (resultText) {
      if (containsWords(resultText, 'abbinamento', 'pod', 'pdr')) {
        return new SuccessResponse(resultText);
      }

      if (
        containsWords(resultText, 'Presenza', 'dati', 'identificativi', 'cliente', 'alfanumerici')
        || containsWords(resultText, ...'Codice Fiscale o Partita IVA non conforme'.split(' '))
        || containsWords(resultText, ...'cliente inesistente'.split(' '))
        || containsWords(resultText, ...'POD inesistente'.split(' '))
        || containsWords(resultText, ...'PDR inesistente'.split(' '))
        || containsWords(resultText, ...'punto prelievo non attivo'.split(' '))
        || containsWords(resultText, ...'POD attivo SMT caso comma 2.6'.split(' '))
      ) {
        return new FailureResponse(resultText);
      }

      if (containsWords(resultText, ...'Fornitura fornita AGSM'.split(' '))) {
        return new ErrorResponse(
          'Non è possibile effettuare il controllo per il pod/pdr inserito. Utilizzare un altro portale per fare la verifica'
        );
      }
    }

    return new ErrorResponse(resultText || 'Errore generico del portale!');
  }

  getScraperCodice() {
    return 'agsm';
  }

  getScraperTipologia() {
    return SCRAPER_TIPOLOGIA.FATTIBILITA;
  }
}
