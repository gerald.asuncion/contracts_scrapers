/**
 * @openapi
 *
 * /scraper/v2/due-i-rete-gas/checker:
 *  post:
 *    tags:
 *      - v2
 *    description: Va sul portale di 2 I Rete Gas e controlla se il pdr è valido.
 *    consume:
 *      - application/json
 *    produces:
 *      - application/json
 *    responses:
 *      200:
 *        description: SuccessResponse
 *    parameters:
 *      - in: body
 *        schema:
 *          type: object
 *          required:
 *            - tipoContratto
 *            - pdr
 *          properties:
 *            tipoContratto:
 *              type: string
 *            pdr:
 *              type: string
 *
 * /scraper/queue/dueIReteGas:
 *  post:
 *    tags:
 *      - v2
 *      - queue
 *    description:
 *      Inserisce in queue il task per effettuare il check tramite lo scraper DueIReteGas.
 *      Il payload è lo stesso della rotta diretta `/scraper/v2/due-i-rete-gas/checker`,
 *      in più va aggiunto il webhook che lo scraper poi andrà a richiamare per notificare l'esito.
 *    consume:
 *      - application/json
 *    produces:
 *      - application/json
 *    responses:
 *      200:
 *        description: SuccessResponse
 */
