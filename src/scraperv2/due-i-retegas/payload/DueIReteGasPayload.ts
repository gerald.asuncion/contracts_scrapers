type DueIReteGasPayload = /*Required<ContrattoPayload> &*/ {
  pdr: string;
  tipoContratto: 'subentro' | 'attivazione';
};

export default DueIReteGasPayload;
