import { Page } from "puppeteer";
import { getOptionText } from "../../browser/page/getElementText";
import isVisible from "../../browser/page/isVisible";
import isXPathVisible from "../../browser/page/isXPathVisible";
import waitForNavigation from "../../browser/page/waitForNavigation";
import waitForSelectorAndClickEvaluated from "../../browser/page/waitForSelectorAndClickEvaluated";
import waitForSelectorAndType from "../../browser/page/waitForSelectorAndType";
import waitForXPathAndClickEvaluated from "../../browser/page/waitForXPathAndClickEvaluated";
import { CertificateOptions } from "../../credenziali/getScraperCertificato";
import ErrorResponse from "../../response/ErrorResponse";
import FailureResponse from "../../response/FailureResponse";
import SuccessResponse from "../../response/SuccessResponse";
import { QueueTask } from "../../task/QueueTask";
import delay from "../../utils/delay";
import tryOrThrow from "../../utils/tryOrThrow";
import { ScraperResponse, WebScraper } from "../scraper";
import { ScraperTipologia, SCRAPER_TIPOLOGIA } from "../ScraperOptions";
import DueIReteGasPayload from "./payload/DueIReteGasPayload";

const cookie = '4430DB6CA4CEDC64F882DB7A03E84E76.i1jbefour1';

@QueueTask()
export default class DueIReteGas extends WebScraper<DueIReteGasPayload> {
  static readonly HOMEPAGE_URL = 'https://4gas.2iretegas.it/four/home.jsf';
  static readonly HOMEPAGE_DOMAIN = '4gas.2iretegas.it';

  protected certificatoClientConfig: CertificateOptions<'p12'> | undefined = {
    path: 'MANFREDI_ANDREA.p12',
    passphrase: '32516658',
    // manual: true,
    additionalHeaders: {
      'cookie': `JSESSIONID=${cookie}`
    }
  };

  login(payload: DueIReteGasPayload): Promise<void> {
    return Promise.resolve();
  }

  async setCookies(page: Page) {
    // Set-Cookie: JSESSIONID=54526625944089E15B62FE4133C1FE3E.i1jbefour1; Path=/; Secure

    await page.setCookie({
      name: "JSESSIONID",
      value: cookie,
      // sameSite: 'None', // 'Lax',
      secure: true,
      path: '/',
      httpOnly: false,
      domain: DueIReteGas.HOMEPAGE_DOMAIN
    });
  }

  async scrapeWebsite(payload: DueIReteGasPayload): Promise<ScraperResponse> {
    const page = await this.p();

    await this.setCookies(page);

    await page.goto(DueIReteGas.HOMEPAGE_URL, { waitUntil: 'networkidle2' });
    await this.setCookies(page);

    if (await isVisible(page, 'input[name="avvisoLogin"] + input')) {
      await waitForSelectorAndClickEvaluated(page, '#avvisoLoginModalPanelContainer input[name="avvisoLogin"] + input[type="submit"]');
      await this.setCookies(page);

      await delay(5000);
    }

    await Promise.all([
      waitForXPathAndClickEvaluated(page, '//span[text() = "PdR"]/parent::div'),
      waitForNavigation(page)
    ]);
    await this.setCookies(page);

    const {
      pdr,
      tipoContratto
    } = payload;

    await waitForSelectorAndType(page, '#formRicercaPdr\\:numeroPdr', pdr);

    await Promise.all([
      waitForSelectorAndClickEvaluated(page, '#formRicercaPdr\\:btn_ricerca'),
      waitForNavigation(page)
    ]);
    await this.setCookies(page);

    const infoMessage = await this.checkForInfoMessages(page);
    if (infoMessage) {
      return new ErrorResponse(infoMessage);
    }

    const isSubentro = tipoContratto === 'subentro';

    const {
      datiStato,
      datiSocieta,
      datiAttivazione,
      datiModificato,
      datiPotMaxRichiesta,
      datiIndirizzo,
      datiCivico,
      daticap,
      datiComune,
      datiProvincia,
      datiMotivazione,
    } = await this.recuperaDatiPerEsito(page);

    if (!this.checkValue(datiAttivazione, "si")) {
      const getSuccessDetails = () => `${datiIndirizzo} ${datiCivico} ${daticap} ${datiComune} ${datiProvincia} ; Modificato "${datiModificato}" Pot.Max.Richiesta "${datiPotMaxRichiesta}"`;

      const isCessato = this.checkValue(datiStato, "cessato");
      const isInAttesaPrimaAttivazione = this.checkValue(datiStato, "attesa prima attivazione");

      if (isInAttesaPrimaAttivazione) {
        if (isSubentro) {
          return new FailureResponse("ko-procedere con una prima attivazione");
        }
        return new SuccessResponse(getSuccessDetails());
      }

      if (this.checkValue(datiSocieta) && this.checkValue(datiAttivazione, "no") && this.checkMotivazione(datiMotivazione)) {
        if (isSubentro ? isCessato : isInAttesaPrimaAttivazione) {
          return new SuccessResponse(getSuccessDetails());
        }

        if (isSubentro ? isInAttesaPrimaAttivazione : isCessato) {
          return new FailureResponse(isSubentro ? "ko-procedere con prima attivazione" : "ko-procedere con subentro");
        }
      }
    }

    return new FailureResponse('ko-pdr non contendibile');
  }

  getXpathForTdByHeader(title: string, additionalXPath: string = ''): string {
    return `//table[@id="formRicercaPdr:pdrTable"]/tbody/tr/td[(count(//th[. = "${title}"]/preceding-sibling::*))+1]${additionalXPath}`;
  }

  async getTextForTdByHeader(page: Page, title: string, additionalXPath: string = ''): Promise<string> {
    const xpath = this.getXpathForTdByHeader(title, additionalXPath);

    return await isXPathVisible(page, xpath) ? (await (await page.$x(xpath))[0].evaluateHandle(element => element.textContent)).jsonValue() : '';
  }

  async clickTdByHeader(page: Page, title: string, additionalXPath: string = ''): Promise<void> {
    return waitForXPathAndClickEvaluated(page, this.getXpathForTdByHeader(title, additionalXPath));
  }

  async getValueForInputByLabel(page: Page, label: string): Promise<string> {
    return (await (await page.$x(`//span[text() = "${label}"]/following-sibling::span/input`))[0].evaluateHandle(element => (element as HTMLInputElement).value)).jsonValue();
  }

  checkValue(value: string, toCheck: string = "") {
    return value.toLocaleLowerCase() == toCheck.toLocaleLowerCase();
  }

  checkMotivazione(value: string) {
    return /(?=.*?\bmotivazione\b)(?=.*?\bsospensione\b).*/i.test(value);
  }

  async checkForInfoMessages(page: Page): Promise<string|void> {
    try {
      await page.waitForSelector('#formRicercaPdr\\:messaggi li.infos', {
        timeout: 2000
      });
    } catch {
      return;
    }

    return await getOptionText(page, '#formRicercaPdr\\:messaggi li.infos');
  }

  async getFieldValueByLabel(page: Page, label: string, parentSelector?: string) {
    try {
      let selector = `//span[contains(@class, "component_label")][contains(., "${label}")]/following-sibling::span[contains(@class, "component_field")]//input`;

      if (parentSelector) {
        selector = parentSelector + selector;
      }

      const [$el] = await page.$x(selector);
      return (await $el.getProperty('value')).jsonValue<string>();
    } catch (ex) {
      // `non sono riuscito a trovare il valore di \`${label}\`:`
      return '';
    }
  }

  async recuperaDatiPerEsito(page: Page) {
    // const datiPdR = await this.getTextForTdByHeader(page, "PdR");
    const datiStato = await this.getTextForTdByHeader(page, "Stato Fornitura");
    const datiSocieta = await this.getTextForTdByHeader(page, "Società di vendita", "/span/span");
    const datiAttivazione = await this.getTextForTdByHeader(page, "In Attesa Attivazione");
    const datiModificato = await this.getTextForTdByHeader(page, "Modificato");
    const datiPotMaxRichiesta = (await this.getTextForTdByHeader(page, "Pot. Max Richiesta")) || '-';

    const descrizioneConcessione = await this.getTextForTdByHeader(page, "Descrizione Concessione");
    const misuratore = await this.getTextForTdByHeader(page, "Misuratore");
    const correttore = await this.getTextForTdByHeader(page, "Correttore");
    const intestatario = await this.getTextForTdByHeader(page, "Intestatario");
    const richiedente = await this.getTextForTdByHeader(page, "Richiedente");

    await Promise.all([
      this.clickTdByHeader(page, 'PdR', '/a'),
      waitForNavigation(page)
    ]);
    await this.setCookies(page);

    // dati fornitura
    const datiIndirizzo = await this.getValueForInputByLabel(page, "Indirizzo:");
    const datiCivico = await this.getValueForInputByLabel(page, "Civico:");
    const daticap = await this.getValueForInputByLabel(page, "Cap:");
    const datiComune = await this.getValueForInputByLabel(page, "Comune:");
    const datiProvincia = await this.getValueForInputByLabel(page, "Provincia:");

    // dettaglio PdR
    let datiMotivazione = '';
    let datiMotivazioneNonPresenti = true;
    let selectorMotivazione = '//span[@class="component_label"][contains(text(), "Motivazione")]/following-sibling::span/input';
    if (await isXPathVisible(page, selectorMotivazione)) {
      datiMotivazioneNonPresenti = false;

      datiMotivazione = await (await
        (await page.$x(selectorMotivazione))[0]
        .evaluateHandle(element => (element as HTMLInputElement).value)
      ).jsonValue();
    }

    // dati da passare come extra
    const codiceRemiPool = await this.getFieldValueByLabel(page, "Cod. Remi Pool:");
    const ragioneSocialeDistributore = await this.getFieldValueByLabel(page, "Ragione Sociale Distributore:");
    const concessione = await this.getFieldValueByLabel(page, "Concessione:");
    const codiceComuneIstat = await this.getFieldValueByLabel(page, "Codice Comune Istat:");
    const accessibilita = await this.getFieldValueByLabel(page, "Accessibilita':");
    const ubicazione = await this.getFieldValueByLabel(page, "Ubicazione:");
    const classeMisuratore = await this.getFieldValueByLabel(page, "Classe Misuratore:");
    const matricolaContatore = await this.getFieldValueByLabel(page, "Matricola Contatore:");
    const tipologiaPdr = await this.getFieldValueByLabel(page, "Tipologia PDR:");
    const statoContatore = await this.getFieldValueByLabel(page, "Stato Contatore:");
    const matricolaCorrettoriVolumi = await this.getFieldValueByLabel(page, "Matricola correttore volumi:");
    const elettrovalvolaChiusa = await this.getFieldValueByLabel(page, "Elettrovalvola chiusa     :");
    const compLettCiclo = await this.getFieldValueByLabel(page, "Comp. lett. ciclo:");
    const potenzaMassimaErogabile = await this.getFieldValueByLabel(page, "Potenza massima erogabile:");
    const categoriaUso = await this.getFieldValueByLabel(page, "Categoria d'uso:");
    const classePrelievo = await this.getFieldValueByLabel(page, "Classe prelievo:");
    const codiceProfiloPrelievo = await this.getFieldValueByLabel(page, "Codice profilo prelievo:");
    const presenzaTelelettura = await this.getFieldValueByLabel(page, "Presenza Telelettura:");
    const dataMessaInServizio = await this.getFieldValueByLabel(page, "Data di messa in servizio:");
    const titolarita = await this.getFieldValueByLabel(page, "Titolarità:");
    const causaleIn = await this.getFieldValueByLabel(page, "Causale IN:");
    const causaleOut = await this.getFieldValueByLabel(page, "Causale OUT:");
    const pdrContrattualizzato = await this.getFieldValueByLabel(page, "Pdr Contrattualizzato:");
    const servizioEnergetico = await this.getFieldValueByLabel(page, "Servizio Energetico:");
    const revoca = await this.getFieldValueByLabel(page, "Revoca:");
    const nomeClienteFinale = await this.getFieldValueByLabel(page, "Nome:", '//legend[contains(., "Dati Identificativi del Cliente Finale Associato al PDR")]/following-sibling::div');
    const cognomeClienteFinale = await this.getFieldValueByLabel(page, "Cognome:", '//legend[contains(., "Dati Identificativi del Cliente Finale Associato al PDR")]/following-sibling::div');
    const ragioneSocialeClienteFinale = await this.getFieldValueByLabel(page, "Ragione Sociale:", '//legend[contains(., "Dati Identificativi del Cliente Finale Associato al PDR")]/following-sibling::div');
    const codiceFiscaleClienteFinale = await this.getFieldValueByLabel(page, "C.F.:", '//legend[contains(., "Dati Identificativi del Cliente Finale Associato al PDR")]/following-sibling::div');
    const pivaClienteFinale = await this.getFieldValueByLabel(page, "P.IVA:", '//legend[contains(., "Dati Identificativi del Cliente Finale Associato al PDR")]/following-sibling::div');
    const telefonoClienteFinale = await this.getFieldValueByLabel(page, "Telefono:", '//legend[contains(., "Dati Identificativi del Cliente Finale Associato al PDR")]/following-sibling::div');
    const emailClienteFinale = await this.getFieldValueByLabel(page, "Email:", '//legend[contains(., "Dati Identificativi del Cliente Finale Associato al PDR")]/following-sibling::div');
    const nomeBeneficiario = await this.getFieldValueByLabel(page, "Nome:", '//legend[contains(., "Dati Identificativi del beneficiario associato al PDR")]/following-sibling::div');
    const cognomeBeneficiario = await this.getFieldValueByLabel(page, "Cognome:", '//legend[contains(., "Dati Identificativi del beneficiario associato al PDR")]/following-sibling::div');
    const ragioneSocialeBeneficiario = await this.getFieldValueByLabel(page, "Ragione Sociale:", '//legend[contains(., "Dati Identificativi del beneficiario associato al PDR")]/following-sibling::div');
    const codiceFiscaleBeneficiario = await this.getFieldValueByLabel(page, "C.F.:", '//legend[contains(., "Dati Identificativi del beneficiario associato al PDR")]/following-sibling::div');
    const pivaBeneficiario = await this.getFieldValueByLabel(page, "P.IVA:", '//legend[contains(., "Dati Identificativi del beneficiario associato al PDR")]/following-sibling::div');
    const telefonoBeneficiario = await this.getFieldValueByLabel(page, "Telefono:", '//legend[contains(., "Dati Identificativi del beneficiario associato al PDR")]/following-sibling::div');
    const emailBeneficiario = await this.getFieldValueByLabel(page, "Email:", '//legend[contains(., "Dati Identificativi del beneficiario associato al PDR")]/following-sibling::div');
    const inizioAgevolazione = await this.getFieldValueByLabel(page, "Inizio Agevolazione:");
    const fineAgevolazione = await this.getFieldValueByLabel(page, "Fine Agevolazione:");

    return {
      datiStato,
      datiSocieta,
      datiAttivazione,
      datiModificato,
      datiPotMaxRichiesta,
      datiIndirizzo,
      datiCivico,
      daticap,
      datiComune,
      datiProvincia,
      datiMotivazione,
      codiceRemiPool,
      ragioneSocialeDistributore,
      concessione,
      descrizioneConcessione,
      codiceComuneIstat,
      intestatario,
      richiedente,
      accessibilita,
      ubicazione,
      misuratore,
      correttore,
      classeMisuratore,
      matricolaContatore,
      tipologiaPdr,
      statoContatore,
      matricolaCorrettoriVolumi,
      elettrovalvolaChiusa,
      compLettCiclo,
      potenzaMassimaErogabile,
      categoriaUso,
      classePrelievo,
      codiceProfiloPrelievo,
      presenzaTelelettura,
      dataMessaInServizio,
      titolarita,
      causaleIn,
      causaleOut,
      pdrContrattualizzato,
      servizioEnergetico,
      revoca,
      nomeClienteFinale,
      cognomeClienteFinale,
      ragioneSocialeClienteFinale,
      codiceFiscaleClienteFinale,
      pivaClienteFinale,
      telefonoClienteFinale,
      emailClienteFinale,
      nomeBeneficiario,
      cognomeBeneficiario,
      ragioneSocialeBeneficiario,
      codiceFiscaleBeneficiario,
      pivaBeneficiario,
      telefonoBeneficiario,
      emailBeneficiario,
      inizioAgevolazione,
      fineAgevolazione,
    };
  }

  getScraperCodice(): string {
    return "due-i-retegas";
  }

  getScraperTipologia(): ScraperTipologia {
    return SCRAPER_TIPOLOGIA.FATTIBILITA;
  }
}
