/* eslint-disable no-await-in-loop */
import path from 'path';
import { Page } from 'puppeteer';
import * as util from 'util';
import * as fs from 'fs';
import { performance } from 'perf_hooks';
import { ScraperResponse, WebScraper } from '../scraper';
import FailureResponse from '../../response/FailureResponse';
import SuccessResponse from '../../response/SuccessResponse';
import { AretiRepo, AretiCsvInfo } from '../../repo/AretiRepo';
import { Logger } from '../../logger';
import extractErrorMessage from '../../utils/extractErrorMessage';
import waitForSelectorAndClick from '../../browser/page/waitForSelectorAndClick';
import extractErrorStackTrace from '../../utils/extractErrorStackTrace';
import parseXlsxToCsv, { parseCsvHeader } from './parseXlsxToCsv';
import ScraperOptions, { SCRAPER_TIPOLOGIA } from '../ScraperOptions';
import todayDate from '../../utils/todayDate';
import getCurrentDayFormatted from '../../utils/getCurrentDayFormatted';
import emptyDirectory from '../../utils/emptyDirectory';
import waitForNavigation from '../../browser/page/waitForNavigation';
import tryOrThrow from '../../utils/tryOrThrow';
import BlockedScraperError from '../../errors/BlockedScraperError';
import waitForVisible from '../../browser/page/waitForVisible';
import waitForXPathAndClick from '../../browser/page/waitForXPathAndClick';
import getElementText from '../../browser/page/getElementText';
import waitFile from '../../utils/waitFile';
import mkdir from '../../utils/mkdir';
import setDownloadPath from '../../browser/page/setDownloadPath';
import { QueueTask } from '../../task/QueueTask';

const mkdirAsync = util.promisify(fs.mkdir);

async function download(page: Page, downloadDir: string, fileName: string, logger: Logger) {
  const downloadPath = downloadDir;
  await mkdirAsync(downloadPath, { recursive: true });
  logger.info(`Cartella del download: ${downloadPath}`);

  const client = await page.target().createCDPSession();

  await client.send('Page.setDownloadBehavior', {
    behavior: 'allow',
    downloadPath
  });

  logger.info('Scaricamento...');

  const filePath = path.resolve(downloadPath, fileName);
  await waitFile(filePath);
  logger.info(`Scaricato il file: ${filePath}`);

  return filePath;
}

type TipiElementiTabella = 'FOLDER' | 'Xlsx' | 'Csv' | 'Zip';

@QueueTask({ scraperName: 'areti' })
export default class AretiContendibilitaChecker extends WebScraper {
  private aretiRepo: AretiRepo;

  constructor(options: ScraperOptions & { baseDownloadPath: string; aretiRepo: AretiRepo }) {
    super(options);
    this.baseDownloadPath = options.baseDownloadPath;
    this.aretiRepo = options.aretiRepo;
  }

  getLoginTimeout(): number { return 60000; }

  async login(): Promise<void> {
    const loginUrl = 'https://pad.areti.it';
    const { username, password } = await this.getCredenziali();
    this.checkPageClosed();

    const logger = this.childLogger;

    logger.info('login > apro la pagina di login');
    const page = await this.p();
    await page.goto(loginUrl, { waitUntil: 'networkidle0' });
    logger.info('login > inizio');

    await page.waitForSelector('#Email');
    await page.type('#Email', username);
    logger.info('login > inserito username');
    await page.type('#Password', password);
    logger.info('login > inserita password');

    logger.info('login > clicco sul pulsante di login');
    await Promise.all([
      waitForSelectorAndClick(page, '#loginForm > div:nth-child(4) > div > input'),
      page.waitForNavigation({ waitUntil: 'networkidle2' })
    ]);

    logger.info('login > fine');
  }

  async waitLoading(page: Page): Promise<unknown> {
    return Promise.race([
      // waitForHiddenStyled(page, '[id$=":overlay.start"]'),
      waitForNavigation(page),
      page.waitForResponse(() => true)
    ]);
  }

  async ordineCorretto(page: Page, logger: Logger): Promise<boolean> {
    try {
      // eslint-disable-next-line max-len
      await page.waitForXPath('//*[@id="dynTable_wrapper"]//*[@class="dataTables_scrollHead"]//table//th[@class="sorting_desc"][normalize-space()="Data/ora modifica"]', {
        timeout: 250
      });

      return true;
      // eslint-disable-next-line no-empty
    } catch { }

    return false;
  }

  async ordinaTabella(page: Page, logger: Logger): Promise<void> {
    logger.info('Clicco sul tasto di ordinamento della tabella sulla voce `Data/ora modifica`');

    // eslint-disable-next-line max-len
    const selettoreOrdinamento = '#dynTable_wrapper > div.dataTables_scroll > div.dataTables_scrollHead > div > table > thead > tr > th:nth-child(3)';

    while (!await this.ordineCorretto(page, logger)) {
      await Promise.all([
        waitForSelectorAndClick(page, selettoreOrdinamento),
        // delay(1000)
        Promise.race([
          waitForNavigation(page),
          this.waitLoading(page),
          page.waitForResponse(() => true)
        ])
      ]);
    }
  }

  async cartelleInTabella(page: Page, logger: Logger): Promise<number> {
    return page.$$eval('.dataTables_scrollBody tr td a[href*="Type=FOLDER"]', (list) => list.length);
  }

  async tipoRigaTabella(page: Page, logger: Logger, riga = 0): Promise<[string, TipiElementiTabella]> {
    const selettorePrimaCella = `#dynTable > tbody > tr:nth-child(${riga + 1}) > td:nth-child(2) > a`;
    const selettoreUltimaCella = `#dynTable > tbody > tr:nth-child(${riga + 1}) > td:last-child`;

    const [name, href] = await page.$eval(selettorePrimaCella, (anchor) => [
      anchor.innerHTML as string,
      anchor.getAttribute('href') as string
    ]);
    const isFolder = /(\?|&)Type=FOLDER(&|$)/.test(href);
    let type: TipiElementiTabella = 'FOLDER';

    if (!isFolder) {
      type = await page.$eval(selettoreUltimaCella, (tCell) => tCell.innerHTML) as TipiElementiTabella;
    }

    return [
      name,
      type
    ];
  }

  async downloadFile(page: Page, logger: Logger, riga = 0): Promise<[string, string]> {
    const selettoreUltimaCartella = `#dynTable > tbody > tr:nth-child(${riga + 1}) > td:nth-child(2) > a`;

    const res: [string, string] = await page.evaluate<(sel: string) => [string, string]>((selettore) => {
      const el: HTMLAnchorElement | null = document.querySelector(selettore);
      const name = el?.innerText || '';
      const url = el?.href || '';

      return [name, url];
    }, selettoreUltimaCartella);
    return res;
  }

  async navigaAllaFoglia(page: Page, logger: Logger): Promise<void> {
    logger.info('Navigazione alla foglia!');

    const annoCorrente = todayDate().split('-')[0];
    let listaNomiCartelleAccettate = [annoCorrente, (Number(annoCorrente) - 1).toString(), 'gennaio', 'febbraio', 'marzo', 'aprile', 'maggio', 'giugno', 'luglio', 'agosto', 'settembre', 'ottobre', 'novembre', 'dicembre'];

    const creaSelettoreCartella = (index: number) => `#dynTable > tbody > tr:nth-child(${index}) > td:nth-child(2) > a`;

    while ((await this.cartelleInTabella(page, logger)) > 0) {
      await this.ordinaTabella(page, logger);

      let indiceCartella = 1;
      let primoElementoSelettore = creaSelettoreCartella(indiceCartella);
      let trovato = false;
      while (!trovato) {
        const testo = await getElementText(page, primoElementoSelettore, 'textContent');
        if (!listaNomiCartelleAccettate.includes(testo?.toLowerCase() as string)) {
          primoElementoSelettore = creaSelettoreCartella(++indiceCartella);
        } else {
          trovato = true;
        }
      }
      await Promise.all([
        waitForSelectorAndClick(page, primoElementoSelettore),
        waitForNavigation(page)
      ]);
    }

    const breadcrumbText = await page.evaluate(() => {
      const iterator = document.evaluate(
        '//div[@id="breadcrumbs"]//text()',
        document,
        null,
        XPathResult.ANY_TYPE,
        null
      );

      let textNode;
      const textParts: string[] = [];

      // eslint-disable-next-line no-cond-assign
      while (textNode = iterator.iterateNext()) {
        textParts.push(textNode.textContent?.trim() || '');
      }

      return textParts.filter(Boolean).join(' ');
    });

    logger.info(`Foglia raggiunta: '${breadcrumbText}'`);
  }

  async scrapeWebsite(): Promise<ScraperResponse> {
    const logger = this.childLogger;

    const page = await this.p();

    let downloadDir: string | undefined;

    try {
      try {
        // controllo se esiste la modale covid-19
        // logger.info("C'è la modale covid-19");
        await waitForVisible(page, 'div[role="dialog"]');
        logger.info("C'è la modale covid-19");
        // await waitForSelectorAndClick(page, 'button#cancel');
        await waitForXPathAndClick(page, '//button[contains(., "Ok")]');
        await page.waitForResponse(() => true);
      } catch (error) {
        logger.info('La modale covid-19 non esiste');
        await page.waitForSelector('#header > header > div.top-nav > ul > li:nth-child(1) > div > a > button');
      }

      // logger.info('Clicco su `Documenti/pubblicazioni`');
      // await waitForSelectorAndClick(page, '#header > header > div.top-nav > ul > li:nth-child(10) > div > button');

      // logger.info('Clicco sulla sottovoce `Documenti/pubblicazioni`');
      // const documentiPubblicazionLinkSelector = '#Documenti-Pubblicazioni > a';
      // await Promise.all([
      //   waitForSelectorAndClick(page, documentiPubblicazionLinkSelector),
      //   waitForNavigation(page)
      // ]);

      logger.info('Vado alla pagina `Comunicazioni Pod Contendibili`');
      await page.goto(
        'https://pad.areti.it/Documents/Documents.aspx?id=851942&FileName=&Folder=Comunicazioni%20Pod%20Contendibili&Type=FOLDER',
        { waitUntil: 'networkidle2' }
      );

      // try {
      await tryOrThrow(async () => this.navigaAllaFoglia(page, logger), "Impossibile accedere all'ultima cartella");
      // } catch (ex) {
      //   const msg = await page.evaluate(() => {
      //     const el = document.querySelector<HTMLElement>('#dynTable > tbody > tr > td.dataTables_empty');
      //     return el?.innerText;
      //   });
      //   throw new Error(`Impossibile accedere all'ultima cartella ${msg}`);
      // }

      logger.info('Scarico i file');
      downloadDir = path.resolve(this.baseDownloadPath, getCurrentDayFormatted(), 'contendibilita-areti');
      await mkdirAsync(downloadDir, { recursive: true });

      /*const client = await page.target().createCDPSession();

      await client.send('Page.setDownloadBehavior', {
        behavior: 'allow',
        downloadPath: downloadDir
      });*/

      const filesCount = await page.evaluate(() => document.querySelectorAll('#dynTable > tbody > tr').length);

      let files = await Promise.all(
        new Array(filesCount).fill(0)
        .map((_, idx) => this.downloadFile(page, logger, idx))
      );
      logger.info(`file trovati: '${files.map(([filename]) => filename).join("', '")}'`);

      // prendi l'ultimo set di file (ex. PODContendibili.1_2021_09-001.csv e PODContendibili.2_2021_09-001.csv, prendere quello con il ".2_")
      const filesTopId = files.reduce((topId, [ name ]) => {
        const codeDatePart = name.split('.').slice(-2, -1)[0];
        const cTopId = codeDatePart.split('_')[0];
        return Math.max(parseInt(cTopId), topId);
      }, 0).toString();
      files = files
      .filter(([name]) => {
        const codeDatePart = name.split('.').slice(-2, -1)[0];
        const cTopId = codeDatePart.split('_')[0];
        return cTopId == filesTopId;
      });

      logger.info(`file trovati: '${files.map(([filename]) => filename).join("', '")}'`);

      await mkdir(downloadDir);
      await setDownloadPath(page, downloadDir);

      const filePathNames = await Promise.all(
        files.map(async ([filename, url]) => {
          const filePath = path.resolve(downloadDir as string, filename);

          await page.evaluate((iurl, fname) => {
            return fetch(iurl, {
              method: 'GET',
              credentials: 'include'
            }).then(r => r.blob()).then(blob => {
              let anchor = document.createElement('a');
              anchor.href = URL.createObjectURL(blob);
              anchor.setAttribute('download', fname);
              anchor.click();
            });
          }, url, filename);

          logger.info(`file scaricato in: '${filePath}'`);

          // const res = await page.evaluate((iurl) => {
          //   return fetch(iurl, {
          //       method: 'GET',
          //       credentials: 'include',
          //       headers: {
          //         "Content-Type": "text/plain;charset=UTF-8",
          //         // "Content-Type": "application/json;charset=UTF-8",
          //         // 'Content-Type': 'application/x-www-form-urlencoded',
          //       }
          //   })
          //   // .then(r => r.text());
          //   .then(r => r.blob())
          //   .then(r => URL.createObjectURL(r))
          //   ;
          // }, url);
          // const filePath = path.resolve(downloadDir as string, filename);

          // fs.createWriteStream()
          // // fs.writeFileSync(filePath, Buffer.from(res as any as string).toString('base64'));
          // fs.writeFileSync(filePath, res);

          return filePath;
        })
      );

      logger.info('file correttamente scaricati');

      logger.info('converto i file in csv');

      const startConversioneFile = performance.now();

      const importedData: (null | [string, AretiCsvInfo])[] = await Promise.all(filePathNames
        .map(async (filePath, idx) => {
          let fileName: string = filePath;
          const ext = path.extname(filePath);
          let csvInfo: AretiCsvInfo;

          switch (ext) {
            case '.csv':
              csvInfo = await parseCsvHeader(fileName);
              if (!csvInfo) throw new Error('fac');
              break;
            case '.xlsx':
              fileName = path.join(downloadDir as string, `data-${idx}.csv`);

              // eslint-disable-next-line no-case-declarations
              const {
                outputFileName,
                ...csvHeaders
              } = await parseXlsxToCsv(filePath, fileName);
              csvInfo = csvHeaders;
              break;
            default:
              logger.error(`Tipo file '${ext}' non supportato durante l'import su db!`);
              return null;
          }

          return [fileName, csvInfo];
        })) as any;

      // eslint-disable-next-line no-bitwise
      if (~importedData.indexOf(null)) {
        throw new BlockedScraperError('Non é stato possibile aprire uno o piú file: controllare i precedenti log di errori!');
      }

      const endConversioneFile = performance.now();
      logger.info(`file convertiti in ${(endConversioneFile - startConversioneFile).toFixed(2)} ms`);

      logger.info('Inserisco i dati nei file in tabella');
      await this.loadDataIntoDatabase(importedData as [string, AretiCsvInfo][], logger);

      logger.info('terminato');
      return new SuccessResponse('OK');
    } catch (e) {
      const errMsg = extractErrorMessage(e);
      const stackTrace = extractErrorStackTrace(e);
      logger.error(`${errMsg} ${stackTrace}`);
      return new FailureResponse(errMsg);
    } finally {
      logger.info('elimino i file non più necessari');
      if (downloadDir) {
        emptyDirectory(downloadDir);
      }
    }
  }

  getScraperCodice() {
    return 'areti';
  }

  getScraperTipologia() {
    return SCRAPER_TIPOLOGIA.IMPORTDATA;
  }

  private async loadDataIntoDatabase(
    filesNameAndInfo: [string, AretiCsvInfo][],
    logger: Logger
  ) {
    const { aretiRepo } = this;
    try {
      await aretiRepo.loadDataArrayIntoDatabase(
        filesNameAndInfo,
        todayDate()
      );
    } catch (ex) {
      const errMsg = extractErrorMessage(ex);
      logger.error(`Impossibile caricare i dati nel database - ${errMsg}`);
      aretiRepo.rollback();
      throw ex;
    }
  }
}
