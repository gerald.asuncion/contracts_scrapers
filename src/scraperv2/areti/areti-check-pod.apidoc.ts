/**
 * @openapi
 *
 * /scraper/v2/areti/pod/check:
 *  post:
 *    tags:
 *      - v2
 *    description: Controlla se il pod è valido per Areti.
 *    consume:
 *      - application/json
 *    produces:
 *      - application/json
 *    responses:
 *      200:
 *        description: SuccessResponse
 *    parameters:
 *      - in: body
 *        description:
 *          Servono il tipo di contratto ed il pod da controllare.
 *        schema:
 *          type: object
 *          required:
 *            - tipoContratto
 *            - pod
 *          properties:
 *            tipoContratto:
 *              type: string
 *              enum: [subentro, attivazione, switch, allaccio, voltura, attivazione-fibra, mandato, altro]
 *            pod:
 *              type: string
 */
