import { AretiRepo } from '../../repo/AretiRepo';
import ErrorResponse from '../../response/ErrorResponse';
import SuccessResponse from '../../response/SuccessResponse';
import { ScraperResponse, WebScraper } from '../scraper';
import { wrapAretiResponseMessage } from './helpers';
import ScraperOptions, { SCRAPER_TIPOLOGIA } from '../ScraperOptions';
import extractErrorMessage from '../../utils/extractErrorMessage';
import { QueueTask } from '../../task/QueueTask';

interface AretiPodCheckerPayload {
  pod: string;
  tipoContratto: 'subentro' | 'attivazione' | 'switch' | 'allaccio' | 'voltura' | 'attivazione-fibra' | 'mandato' | 'altro';
}

@QueueTask({ scraperName: 'aretiCheckPod' })
export default class AretiPodChecker extends WebScraper {
  private aretiRepo: AretiRepo;

  constructor(options: ScraperOptions & { aretiRepo: AretiRepo; }) {
    super(options);
    this.aretiRepo = options.aretiRepo;
  }

  login(): Promise<void> {
    return Promise.resolve();
  }

  async scrapeWebsite({ pod, tipoContratto }: AretiPodCheckerPayload): Promise<ScraperResponse> {
    const logger = this.childLogger;
    const isSubentro = tipoContratto === 'subentro';

    try {
      logger.info(`start check pod ${pod}`);

      const podCheckResult = await this.aretiRepo.podCheck(pod, isSubentro);

      if (podCheckResult) {
        const msg = wrapAretiResponseMessage(podCheckResult);
        if (podCheckResult.indexOf('OK') !== 0) {
          logger.info(`ritorno una ErrorResponse con messaggio ${msg}`);
          return new ErrorResponse(msg);
        }

        logger.info(`ritorno una SuccessResponse con messaggio ${msg}`);
        return new SuccessResponse(msg);
      }

      logger.info('ritorno una ErrorResponse con messaggio KO - Pod non contendibile');
      return new ErrorResponse(wrapAretiResponseMessage('KO - Pod non contendibile'));
    } catch (e) {
      const errMsg = extractErrorMessage(e);
      logger.error(errMsg);
      return new ErrorResponse(wrapAretiResponseMessage(errMsg));
    }
  }

  getScraperCodice() {
    return 'areti-checkpod';
  }

  getScraperTipologia() {
    return SCRAPER_TIPOLOGIA.FATTIBILITA;
  }
}
