import type { AretiCsvInfo } from '../../repo/AretiRepo';
import csvParser from 'csv-parse';

const xlsx = require('node-xlsx');
const fs = require('fs');

function escapeCellContent(text: string|null): string|null {
  return typeof text === 'string' ? text.replace(/(\\*?)$/g, (match) => {
    if (match.length % 2) {
      return `\\${match}`;
    }
    return match;
  }).replace(/\\*?;/g, (match) => {
    if (match.length % 2) {
      return `\\${match}`;
    }
    return match;
  }) : text;
}

export default async function parseXlsxToCsv(filePath: string, outputFileName: string): Promise<AretiCsvInfo & {
  outputFileName: string;
}> {
  const sheets = xlsx.parse(filePath) as { data: string[][] }[]; // parses a file

  const header = sheets[0].data[0];

  const podIndex = header.indexOf('Cod. punto erogazione Pubbl.');
  const statoIndex = header.indexOf('Stato Contatore');
  const flagIndex = header.indexOf('Flag Pod Contendibil') || header.indexOf('Flag Pod Contendibile');
  const indirizzoIndex = header.indexOf('Indirizzo');
  const internoIndex = header.indexOf('Interno');
  const scalaIndex = header.indexOf('Scala');
  const potenzaIndex = header.indexOf('Potenza disponibile');
  const tensioneIndex = header.indexOf('Tensione di rete');

  const numberOfColumns = header.length;

  const fileHandle = fs.openSync(outputFileName, 'a');

  try {
    for (let i = 0; i < sheets.length; i += 1) {
      const sheetData = sheets[i].data;

      for (let j = 0; j < sheetData.length; j += 1) {
        const row = sheetData[j];
        const line = `${row.map(escapeCellContent).join(';')}\n`;

        fs.appendFileSync(fileHandle, line);
      }
    }
  } finally {
    fs.closeSync(fileHandle);
  }

  return {
    outputFileName,
    podIndex,
    statoIndex,
    flagIndex,
    indirizzoIndex,
    internoIndex,
    scalaIndex,
    potenzaIndex,
    tensioneIndex,
    numberOfColumns
  };
}

export async function parseCsvHeader(filename: string): Promise<AretiCsvInfo> {
  return new Promise((resolve, reject) => {
    const input = fs.readFileSync(filename, { encoding: 'utf8' });
    csvParser(input, {
      delimiter: ';',
      skip_empty_lines: true,
      skip_lines_with_error: true,
      to_line: 2
    }, (err, records, info) => {
      if (err) {
        reject(err);
      } else {
        const header = records[0];
        const podIndex = header.indexOf('Cod. punto erogazione Pubbl.');
        const statoIndex = header.indexOf('Stato Contatore');
        const flagIndex = header.indexOf('Flag Pod Contendibil') || header.indexOf('Flag Pod Contendibile');
        const indirizzoIndex = header.indexOf('Indirizzo');
        const internoIndex = header.indexOf('Interno');
        const scalaIndex = header.indexOf('Scala');
        const potenzaIndex = header.indexOf('Potenza disponibile');
        const tensioneIndex = header.indexOf('Tensione di rete');
        resolve({
          podIndex,
          statoIndex,
          flagIndex,
          indirizzoIndex,
          internoIndex,
          scalaIndex,
          potenzaIndex,
          tensioneIndex,
          numberOfColumns: header.length
        });
      }
    });
  });
}
