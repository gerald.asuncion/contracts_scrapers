/* eslint-disable import/prefer-default-export */
/* eslint-disable max-len */
export const wrapAretiResponseMessage = (msg: string): string => `${msg}. Prima di procedere con la sottoscrizione effettuare un'ulteriore verifica del precheck poichè i dati del portale potrebbe essere non aggiornati.`;
