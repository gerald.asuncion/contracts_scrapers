/**
 * @openapi
 *
 * /scraper/v2/areti/contendibilita/download:
 *  post:
 *    tags:
 *      - v2
 *    description: Effettua il download dei dati di Areti e li salva su database.
 *    consume:
 *      - application/json
 *    produces:
 *      - application/json
 *    responses:
 *      200:
 *        description: SuccessResponse
 *    parameters:
 *      - in: body
 *        description: non serve.
 *        schema:
 *          type: object
 */
