import waitCheckCredentials from '../browser/page/waitCheckCredentials';
import waitForSelectorAndClick from '../browser/page/waitForSelectorAndClick';
import FailureResponse from '../response/FailureResponse';
import SuccessResponse from '../response/SuccessResponse';
import { ScraperResponse, WebScraper } from './scraper';
import { SCRAPER_TIPOLOGIA } from './ScraperOptions';
import estraiSuccessInfo from './eDistribuzione/estraiSuccessInfo';
import { QueueTask } from '../task/QueueTask';

type EDistribuzionePayload = {
  pod: string;
};

@QueueTask()
export default class EDistribuzione extends WebScraper<EDistribuzionePayload> {
  static readonly LOGIN_URL: string = 'https://4pt.e-distribuzione.it/s/login/';

  async login(): Promise<void> {
    const { childLogger } = this;
    const { username, password } = await this.getCredenziali();
    await this.checkPageClosed();
    const page = await this.p();

    childLogger.info('login > inizio');

    await page.goto(EDistribuzione.LOGIN_URL);
    await page.waitForSelector('#sfdc_username_container');

    childLogger.info('login > inserisco username');
    await page.type('#sfdc_username_container input', username);

    childLogger.info('login > inserisco password');
    await page.type('#sfdc_password_container input', password);

    childLogger.info('login > clicco sul pulsante di accesso');
    // eslint-disable-next-line max-len
    await waitForSelectorAndClick(page, '#centerPanel > div > div.slds-col--padded.contentRegion.comm-layout-column > div > div:nth-child(2) > div > div:nth-child(4) > button');

    await Promise.race([
      waitCheckCredentials(
        page,
        '#centerPanel > div > div.slds-col--padded.contentRegion.comm-layout-column > div > div:nth-child(2) > div > span > div > div',
        EDistribuzione.LOGIN_URL,
        username,
        childLogger
      ),
      page.waitForSelector('label.FlowText')
    ]);
    await page.click('label.FlowText');
    await page.click('.btn.FlowFinishBtn');
  }

  checkValidity(content: string): boolean {
    return content.toLowerCase().includes('evasa_') && content.toLowerCase().includes('cessato');
  }

  async scrapeWebsite({ pod }: EDistribuzionePayload): Promise<ScraperResponse> {
    const codiceRichiestaVenditore = 'WEmmggaaSMhhmm';
    const page = await this.p();

    await page.waitForSelector('.slds-has-flexi-truncate li:nth-child(2)');
    await page.click('.slds-has-flexi-truncate li:nth-child(2)');
    await page.click('[title="Ricerche POD"]');
    await page.waitForXPath("//label[contains(., 'PD2 - Ricerca POD Contendibili')]");
    const [check] = await page.$x("//label[contains(., 'PD2 - Ricerca POD Contendibili')]");
    if (check) {
      await check.click();
    }
    await page.click('#ContinueBtnID');
    await page.waitForSelector('[id*="29\\:"]');
    await page.type('[id*="29\\:"]', codiceRichiestaVenditore);
    await page.type('[id*="61\\:"]', pod.toUpperCase());
    await page.click('#requestModal .CustomButton');
    await page.waitForSelector('#requestModal .slds-button.slds-button--brand.buttonDisp');
    await page.click('#requestModal .slds-button.slds-button--brand.buttonDisp');
    await page.waitForSelector('.slds-page-header.forceRecordLayout');
    const content = await page.content();
    const isValid = this.checkValidity(content);

    if (!isValid) {
      return new FailureResponse('');
    }
    return new SuccessResponse(await estraiSuccessInfo(page));
  }

  getScraperCodice() {
    return 'edistribuzione';
  }

  getScraperTipologia() {
    return SCRAPER_TIPOLOGIA.FATTIBILITA;
  }
}
