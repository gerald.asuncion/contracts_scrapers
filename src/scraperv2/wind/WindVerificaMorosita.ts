import { Page } from "puppeteer";
import collateBestMatch from "../../browser/helpers/collateBestMatch";
import { getOptionText } from "../../browser/page/getElementText";
import isVisible from "../../browser/page/isVisible";
import waitCheckCredentials from "../../browser/page/waitCheckCredentials";
import waitForHidden from "../../browser/page/waitForHidden";
import waitForNavigation from "../../browser/page/waitForNavigation";
import waitForSelectorAndClick from "../../browser/page/waitForSelectorAndClick";
import waitForSelectorAndClickEvaluated from "../../browser/page/waitForSelectorAndClickEvaluated";
import waitForSelectorAndType from "../../browser/page/waitForSelectorAndType";
import waitForVisible from "../../browser/page/waitForVisible";
import waitForXPathAndClick from "../../browser/page/waitForXPathAndClick";
import waitForXPathVisible from "../../browser/page/waitForXPathVisible";
import { CertificateOptions } from "../../credenziali/getScraperCertificato";
import { Logger } from "../../logger";
import ErrorResponse from "../../response/ErrorResponse";
import FailureResponse from "../../response/FailureResponse";
import SuccessResponse from "../../response/SuccessResponse";
import { QueueTask } from "../../task/QueueTask";
import createChildLogger from "../../utils/createChildLogger";
import delay from "../../utils/delay";
import tryOrThrow, { TryOrThrowError } from "../../utils/tryOrThrow";
import { ScraperResponse, WebScraper } from "../scraper";
import { ScraperTipologia, SCRAPER_TIPOLOGIA } from "../ScraperOptions";
import { WindVerificaMorositaPayload } from "./payload/WindVerificaMorositaPayload";
import {waitForElementAndNoFails} from "../../browser/page/waitForElementAndNoFails";

@QueueTask()
export default class WindVerificaMorosita extends WebScraper<WindVerificaMorositaPayload> {
  static readonly LOGIN_URL = 'https://ngpos.windtre.it/';

  protected certificatoClientConfig: CertificateOptions<'p12'> | undefined = {
    path: 'WBSPRM02_12-01-2023.WIND.it.p12', // 'WBSPRM02_11022022_WINDit.p12',
    passphrase: '001C8E2062E08CBE' // '8910CB1B4285D9E9' // '723D7629C2E671E6'
  };

  protected hasCertificateErrorFlag: string | boolean = false;
  protected hasPasswordToChange: string | boolean = false;

  getScraperCodice(): string {
    return 'wind-verifica-morosita';
  }

  getScraperCodici(): string[] {
    return [
      this.getScraperCodice(),
      'wind-verifica-morosita--II-livello'
    ]
  }

  getScraperTipologia(): ScraperTipologia {
    return SCRAPER_TIPOLOGIA.FATTIBILITA;
  }

  getLoginTimeout(): number {
    return 300000;
  }

  async hasCertificateError(page: Page) {
    try {
      await waitForVisible(page, '#question-inorder .csc-default .etichetta_errore', {
        timeout: 2000
      });

      this.hasCertificateErrorFlag = await getOptionText(page, '#question-inorder .csc-default .etichetta_errore');
      return true;
    } catch {
    }

    return false;
  }

  async login(payload: WindVerificaMorositaPayload): Promise<void> {
    const logger = createChildLogger(this.childLogger, this.constructor.name);
    const [ credenzialiCertificato, credenziali ] = await this.getCredenzialiLista();

    const page = await this.p();

    await Promise.all([
      page.goto(WindVerificaMorosita.LOGIN_URL, { waitUntil: 'networkidle2' }),
      waitForNavigation(page)
    ]);

    await tryOrThrow(async () => {
      logger.info('Primo set di credenziali in inserimento!');

      const { username, password } = credenzialiCertificato;

      await waitForSelectorAndType(page, '#username', username);
      await waitForSelectorAndType(page, '#password', password);

      await Promise.all([
        waitForSelectorAndClick(page, '[name="submit"]'),
        waitForNavigation(page)
      ]);
    }, 'Primo livello di credenziali (con certificato)!');

    if (await this.hasCertificateError(page)) {
      return;
    }

    await tryOrThrow(async () => {
      // remove interceptors after certificate has been verified because of follow-redirect problems
      await this.disableInterceptors();

      logger.info('Secondo set di credenziali in inserimento!');

      const { username, password } = credenziali;

      await waitForSelectorAndType(page, '#username', username);
      await waitForSelectorAndType(page, '#password', password);

      await Promise.all([
        waitForSelectorAndClick(page, '#submit_login_form'),
        waitForNavigation(page)
      ]);
    }, 'Secondo livello di credenziali (senza certificato)!');

    // check if password must be change
    const errors = await Promise.all([
      waitForElementAndNoFails(page.waitForXPath( "//div[@id='box-login']/h3[contains(.,'La psw è scaduta e deve essere cambiata')]", {timeout: 15000})),
      waitForElementAndNoFails(page.waitForSelector("div.blockMsg div#question span#titolo-question", {timeout: 15000})),
    ]);

    if (errors[0]) {
      const { password } = credenziali;

      await waitForSelectorAndType(page, '#password', password);
      await waitForSelectorAndType(page, '#password_confirm', password);

      await Promise.all([
        waitForSelectorAndClick(page, '#submit_login_form'),
        waitForNavigation(page)
      ]);
    }
    if (errors[1]) {
      const message = await errors[1].evaluate((el: Element) => (el as HTMLElement).innerText);

      if (message.toLowerCase().indexOf("password errata") > -1 || message.toLowerCase().indexOf("password non valida") > -1) {
        this.hasPasswordToChange = message;
        return;
      }
    }

    if (await this.hasCertificateError(page)) {
      return;
    }

    logger.info('Doppia autenticazione e con certificato completata!');

    try {
      await waitCheckCredentials(page, 'form[name="resetPsw"]', WindVerificaMorosita.LOGIN_URL, credenziali.username, logger, {timeout: 5000});
    } catch (ignored) {}

    if (await isVisible(page, '.blockPage > #question')) {
      await waitForSelectorAndClick(page, '.blockPage > #question #question-ok', {
        timeout: 5000
      });
    }

    await page.waitForTimeout(2000);
  }

  async collateBestMatchByXPath(page: Page, value: string, listSelector: string) {
    await waitForXPathVisible(page, listSelector);

    const itemsElementsList = await page.$x(listSelector);

    let itemsElementsListWithLabel = await Promise.all(itemsElementsList.map(async (item) => ({
      item,
      label: await page.evaluate(el => el.textContent, item)
    })));

    const [
      bestMatchIndex
    ] = collateBestMatch(value, itemsElementsListWithLabel.map(({ label }) => label));

    await itemsElementsListWithLabel[bestMatchIndex].item.click();
  }

  async waitForAutoCompleteAndType(page: Page, fieldName: string, value: string) {
    const selectSelector = `input[name="${fieldName}"]`;

    await waitForSelectorAndType(page, selectSelector, value);

    await this.collateBestMatchByXPath(
      page,
      value,
      "//body/ul[contains(concat(' ', normalize-space(@class), ' '), ' ui-autocomplete ') and contains(@style,'display: block')]/li"
    );
  }

  async chekForErrors(page: Page, exclude: string[] = []): Promise<void> {
    if (await isVisible(page, '.blockPage > #question', 1500)) {
      const errorMessage = await getOptionText(page, ".blockPage > #question > #titolo-question");

      if (exclude.includes(errorMessage)) {
        return waitForSelectorAndClick(page, '.blockPage > #question #question-ok', {
          timeout: 5000
        });
      }

      throw new TryOrThrowError(`Error Message: "${errorMessage}"`);
    }
  }

  private async waitForSpinner(page: Page, logger: Logger, visibilityWaiting = 4000) {
    return Promise.race([
      waitForXPathVisible(page, '//*[contains(text(), "momentaneamente indisponibile")]')
      .then(() => getOptionText(page, '#msgWarning'))
      .then(message => Promise.reject(message)),
      async () => {
        const selector = '#spinnerContainer';

        const dt = Date.now();
        try {
          logger.info('waiting for spinner visible');
          await waitForVisible(page, selector, {
            timeout: visibilityWaiting
          });
        } catch {
          return;
        }

        await waitForHidden(page, selector);
      }
    ]);
  }

  private mappaEsiti = (_message: string) => {
    const message = _message.trim().toLowerCase();

    if (message.startsWith("Non è stato trovato nessun cliente con il criterio di ricerca utilizzato.".toLowerCase())) {
      return new SuccessResponse();
    }
    if (~message.indexOf("anagrafica risulta bloccata".toLowerCase()) || ~message.indexOf("Non è possibile procedere con l’inserimento".toLowerCase())) {
      return new FailureResponse("Cliente non acquisibile.");
    }

    return new ErrorResponse(`Errore portale: ${_message || ''}.`);
  }

  private async inserisciIndirizzo(page: Page, logger: Logger, payload: WindVerificaMorositaPayload) {
    const {
      provincia,
      comune,
      indirizzo,
      civico
    } = payload;

    await this.chekForErrors(page, ["Il campo 'Provincia' e' obbligatorio."]);

    await waitForSelectorAndType(page, 'input[name="provincia"]', provincia);
    await this.waitForAutoCompleteAndType(page, 'comune', comune);

    await delay(500);
    await this.waitForAutoCompleteAndType(page, 'indirizzo', indirizzo);

    await waitForSelectorAndType(page, 'input[name="civico"]', civico);

    await this.chekForErrors(page);

    await waitForSelectorAndClick(page, '#verificaIndirizzoPam > span.button');
  }

  async scrapeWebsite(
    // _payload: Partial<WindVerificaMorositaPayload> & Pick<WindVerificaMorositaPayload, 'codiceFiscale'> & Pick<WindVerificaMorositaPayload, 'username'> & Pick<WindVerificaMorositaPayload, 'password'>
    _payload: Partial<WindVerificaMorositaPayload> & Pick<WindVerificaMorositaPayload, 'codiceFiscale'>
  ): Promise<ScraperResponse> {
    if (this.hasPasswordToChange) {
      return new FailureResponse("Credentials Error: " + this.hasPasswordToChange);
    }
    if (this.hasCertificateErrorFlag) {
      return new FailureResponse("Certificate Error: " + this.hasCertificateErrorFlag);
    }

    const logger = createChildLogger(this.childLogger, this.constructor.name);
    const page = await this.p();

    await tryOrThrow(async () => {

      // goto app
      await waitForSelectorAndClickEvaluated(page, '.desktop-fisso a');

      await waitForSelectorAndClick(page, 'input[type="radio"][name="linea"][value="nuova"]');
    }, "navigazione alla form indirizzo");

    const payload: WindVerificaMorositaPayload = {
      comune: "Bari",
      provincia: "Ba",
      indirizzo: "via napoli",
      civico: "19",
      ..._payload
    };

    const {
      indirizzo,
      civico,
      codiceFiscale
    } = payload;

    await tryOrThrow(async () => {
      await delay(500);

      await this.inserisciIndirizzo(page, logger, payload);
    }, "inserimento indirizzo");

    // spinner
    await this.waitForSpinner(page, logger);

    // select from grid
    await tryOrThrow(async () => {
      await page.waitForSelector('table#tblIndirizzi tbody tr td');

      await this.collateBestMatchByXPath(page, `${indirizzo.toUpperCase()}, ${civico}`, '//table[@id="tblIndirizzi"]/tbody/tr/td[1]');

      await Promise.all([
        waitForSelectorAndClick(page, '.cont-butt-avanti > #next-step'),
        waitForNavigation(page)
      ]);
    }, "selezione indirizzo da griglia");

    // spinner
    await this.waitForSpinner(page, logger, 2000);

    await tryOrThrow(async () => {
      // click tab
      await waitForXPathAndClick(page, "//div/h3[contains(concat(' ', normalize-space(@class), ' '), ' ui-accordion-header ')][text()='OFFERTA']");
      await delay(500);
      await waitForSelectorAndClick(page, '#infodiv_LIS515 > p');

      await Promise.all([
        waitForSelectorAndClick(page, '.cont-butt-avanti > #next-step'),
        waitForNavigation(page)
      ]);
    }, "navigazione verso inserimento codice fiscale");

    // spinner
    await this.waitForSpinner(page, logger, 2000);

    // select from grid
    await tryOrThrow(async () => {
      await waitForSelectorAndType(page, 'input[name="cliente"]', codiceFiscale);
      await waitForSelectorAndClick(page, 'input[type="submit"][name="verifica-cf"]');
    }, "inserimento codice fiscale");

    return await Promise.race([
      Promise.race([
        waitForVisible(page, '#tblAttivabili'),
        waitForXPathVisible(page, '//div[@id="question-inorder"]//h2[contains(text(), "Ricerca Cliente:")]')
      ])
      .then(successText => new SuccessResponse()),
      waitForVisible(page, '#msgWarning')
      .then(() => getOptionText(page, '#msgWarning'))
      .then(this.mappaEsiti)
    ]);
  }
}
