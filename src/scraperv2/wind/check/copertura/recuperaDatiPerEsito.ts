import { Page } from "puppeteer";
import getElementText from "../../../../browser/page/getElementText";
import waitForTimeout from "../../../../browser/page/waitForTimeout";
import { TryOrThrowError } from "../../../../utils/tryOrThrow";

export type DatiSito = {
  titoloOfferta: string;
  prezzo: Number;
  isContributoInstallazione: boolean;
  isPrezzoSpeciale: boolean;
};

export default async function recuperaDatiPerEsito(page: Page): Promise<DatiSito> {
  await waitForTimeout(page, 3000);

  let titoloOfferta = await getElementText(page, "#itemRiepilogo > #item_OFFERTA_BASE > span", "textContent") || await getElementText(page, '#master_titolo', 'textContent') || await getElementText(page, '#contenuto_testo_offerta > h4', 'textContent');

  let prezzo = -1;
  let isContributoInstallazione = false;
  let isPrezzoSpeciale = false;
  if (titoloOfferta !== 'Offerta non disponibile') {
    let prezzoSulPortale = await getElementText(page, '.price', 'textContent');

    prezzoSulPortale = (prezzoSulPortale as string).replace(/\n/g, '').replace(/ /g, '').trim();

    const matches = prezzoSulPortale.match(/\d+|\.|,/g);

    if (!matches) {
      throw new TryOrThrowError('prezzo sul portale non elaborato');
    }

    prezzo = Number(matches.join('').replace(',', '.'));

    const pageContent = await page.content();
    isContributoInstallazione = pageContent.includes('Contributo Installazione');
    isPrezzoSpeciale = pageContent.includes('Prezzo speciale per te che sei già nostro cliente');
  }

  return {
    titoloOfferta: (titoloOfferta as string).trim(),
    prezzo,
    isContributoInstallazione,
    isPrezzoSpeciale
  };
}
