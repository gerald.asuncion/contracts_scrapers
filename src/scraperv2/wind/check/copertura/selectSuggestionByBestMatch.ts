import { Page } from "puppeteer";
import waitForXPathVisible from "../../../../browser/page/waitForXPathVisible";
import waitForSelectorAndType from "../../../../browser/page/waitForSelectorAndType";
import collateBestMatch from '../../../../browser/helpers/collateBestMatch';

export default async function selectSuggestionByBestMatch(
  page: Page,
  selectSelector: string,
  listSelector: string,
  valueToInsert: string,
  valueToSelect: string
) {
  await waitForSelectorAndType(page, selectSelector, valueToInsert);
  await waitForXPathVisible(page, listSelector);

  const items = await page.$x(`${listSelector}//li`);
  const itemsValue = await Promise.all(items.map(item => item.evaluate((el) => (el as HTMLElement).innerText)));

  const [index] = collateBestMatch(valueToSelect, itemsValue, { caseless: true });

  await items[index].click();
}
