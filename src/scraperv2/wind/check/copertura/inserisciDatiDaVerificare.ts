import { Page } from "puppeteer";
import { Logger } from "../../../../logger";
import { WindCheckCoperturaPayload } from "../../payload/WindCheckCoperturaPayload";
import waitForModaleVerificaVisible from "./waitForModaleVerificaVisible";
import selectSuggestionByBestMatch from "./selectSuggestionByBestMatch";
import waitForSelectorAndType from "../../../../browser/page/waitForSelectorAndType";
import inserisciNumeroFisso from "./inserisciNumeroFisso";
import inserisciNumeroMobile from "./inserisciNumeroMobile";
import waitForSelectorAndClick from "../../../../browser/page/waitForSelectorAndClick";
import waitForVisible from "../../../../browser/page/waitForVisible";
import tryOrThrow, { TryOrThrowError } from "../../../../utils/tryOrThrow";
import isDisabled from "../../../../browser/page/isDisabled";

const VERIFICA_BTN = '#verificacoperturabutton';
export default async function inserisciDatiDaVerificare(
  page: Page,
  logger: Logger,
  { comune, provincia, indirizzo, civico, numeroFissoPrefisso, numeroFisso, numeroMobile }: WindCheckCoperturaPayload
) {
  await waitForModaleVerificaVisible(page);

  await tryOrThrow(
    () => selectSuggestionByBestMatch(page, 'input[name="comune"]', '//ul[contains(@class, "ui-autocomplete")][1]', comune, `${comune} - ${provincia}`),
    'non sono riuscito a selezionare il comune:'
  );

  await waitForModaleVerificaVisible(page);

  await tryOrThrow(
    () => selectSuggestionByBestMatch(page, 'input[name="indirizzoEsteso"]', '//ul[contains(@class, "ui-autocomplete")][2]', indirizzo, indirizzo),
    "non sono riuscito a selezionare l'indirizzo:"
  );

  await tryOrThrow(
    () => waitForSelectorAndType(page, 'input[name="civico"]', civico),
    'non sono riuscito ad inserire il civico:'
  );

  await waitForModaleVerificaVisible(page);

  if (numeroFissoPrefisso && numeroFisso) {
    await tryOrThrow(
      () => inserisciNumeroFisso(page, numeroFissoPrefisso, numeroFisso),
      'non sono riuscito ad inserire il numero fisso:'
    );
  }

  if (numeroMobile) {
    await tryOrThrow(
      () => inserisciNumeroMobile(page, numeroMobile),
      'non sono riuscito ad inserire il numero mobile:'
    );
  }

  await waitForModaleVerificaVisible(page);

  if (await isDisabled(page, VERIFICA_BTN)) {
    throw new TryOrThrowError('pulsante per la verifica dei dati disabilitato');
  }

  await waitForSelectorAndClick(page, VERIFICA_BTN);

  logger.info("aspetto che appaia la pagina con l'esito");

  await tryOrThrow(
    () => waitForVisible(page, '.inf-inorder'),
    "l'esito non è apparso in tempo:"
  );
}
