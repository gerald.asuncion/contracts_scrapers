import { Page } from "puppeteer";
import waitForXPathVisible from "../../../../browser/page/waitForXPathVisible";

const MODALE_VERIFICA_TITOLO_SELECTOR = '//div[contains(@class, "cvg--popup")]//div[contains(@class, "title")][contains(., "Verifica la copertura")]';

const waitForModaleVerificaVisible = (page: Page) => waitForXPathVisible(page, MODALE_VERIFICA_TITOLO_SELECTOR);

export default waitForModaleVerificaVisible;
