import { Page } from "puppeteer";
import waitForXPathAndClick from "../../../../browser/page/waitForXPathAndClick";
import waitForSelectorAndType from "../../../../browser/page/waitForSelectorAndType";
import waitForSelectorAndTypeEvaluated from "../../../../browser/page/waitForSelectorAndTypeEvaluated";

const PREFISSO_SELECTOR = 'input[id="prefisso"]';

export default async function inserisciNumeroFisso(page: Page, prefisso: string, numero: string) {
  await waitForXPathAndClick(page, '//h6[contains(., "Hai già una linea fissa")]//following-sibling::div//label');

  await waitForSelectorAndTypeEvaluated(page, PREFISSO_SELECTOR, '');
  await waitForSelectorAndType(page, PREFISSO_SELECTOR, prefisso);

  await waitForSelectorAndType(page, 'input[id="telefono"]', numero);
}
