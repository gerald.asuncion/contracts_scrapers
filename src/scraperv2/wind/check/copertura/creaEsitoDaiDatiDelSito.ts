import FailureResponse from "../../../../response/FailureResponse";
import SuccessResponse from "../../../../response/SuccessResponse";
import { WindCheckCoperturaPayload } from "../../payload/WindCheckCoperturaPayload";
import { DatiSito } from "./recuperaDatiPerEsito";
import {Page} from "puppeteer";

const TIPOLOGIA: Record<string, string> = {
  'INTERNET 20': 'ADSL20',
  'INTERNET 100': 'FTTC',
  'INTERNAET 100': 'FTTC',
  'INTERNET 100 NGA': 'FTTC NGA',
  'INTERNET 200': 'FTTC',
  'INTERNET 200 NGA': 'FTTC NGA',
  'INTERNET 7': 'ADSL7',
  'SUPER FIBRA': 'FTTH',
  'SUPER FIBRA *': 'FTTH AREE BIANCHE',
};

const VELOCITA: Record<string, string> = {
  'INTERNET 100': '100mega',
  'INTERNAET 100': '100mega',
  'INTERNET 100 NGA': '100mega',
  'INTERNET 200 NGA': '200mega',
  'INTERNAT 200 NGA': '200mega',
  'INTERNET 200': '200mega',
};

export default async function creaEsitoDaiDatiDelSito(
  {
    titoloOfferta,
    prezzo,
    isContributoInstallazione,
    isPrezzoSpeciale,
  }: DatiSito,
  {
    numeroMobile
  }: WindCheckCoperturaPayload,
  page?: Page
) {
  if (titoloOfferta === 'Offerta non disponibile') {
    return new FailureResponse(titoloOfferta);
  }

  if (numeroMobile && !isPrezzoSpeciale) {
    return new FailureResponse('Cliente non CB - Modificare offerta o numero mobile Wind');
  }

  // https://stackoverflow.com/questions/1495822/replacing-nbsp-from-javascript-dom-text-node
  let re = new RegExp(String.fromCharCode(160), "g");
  const key = [titoloOfferta.replace(re, ' ').toUpperCase(), isContributoInstallazione ? '*' : null].filter(Boolean).join(' ');

  let tipologia = TIPOLOGIA[key];
  let velocita = VELOCITA[key];

  if (!tipologia) {
    if (titoloOfferta.indexOf("FWA") > -1 && page) {
      const speeds = await page.$x(`//div[contains(@class, "speedometer_container_speedometer_title_")]/following-sibling::div//div[contains(text(), "Massima")]/preceding-sibling::div//span[contains(@class, "speedometer_value_speedonumero_")]`);
      const texts = await page.$x(`//div[contains(@class, "speedometer_container_speedometer_title_")]/following-sibling::div//div[contains(text(), "Massima")]/preceding-sibling::div//span[contains(@class, "speedometer_value_speedotext")]`);

      const speed = await speeds[0].evaluate((el) => el.textContent);
      const text = await texts[0].evaluate((el) => el.textContent);
      tipologia = "FWA";
      velocita = `${speed} ${text}`;
    } else {
      return new FailureResponse(`offerta ${titoloOfferta} non gestita, avvisare il proprio account affinché apra ticket per gestirla`);
    }
  }

  return new SuccessResponse(`Tipologia: ${tipologia}; Velocità: ${velocita || 'N/A'}; Prezzo: ${prezzo}€`, {
    tipologia,
    velocita: velocita || null,
    prezzo
  });
}
