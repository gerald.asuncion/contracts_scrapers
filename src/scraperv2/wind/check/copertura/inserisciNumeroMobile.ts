import { Page } from "puppeteer";
import waitForXPathAndClick from "../../../../browser/page/waitForXPathAndClick";
import waitForSelectorAndType from "../../../../browser/page/waitForSelectorAndType";

export default async function inserisciNumeroMobile(page: Page, numero: string) {
  await waitForXPathAndClick(page, '//h6[contains(., "Hai un numero mobile")]//following-sibling::div//label');

  await waitForSelectorAndType(page, 'input[id="cellulare"]', numero);
}
