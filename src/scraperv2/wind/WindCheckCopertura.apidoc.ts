/**
 * @openapi
 *
 * /scraper/v2/wind/check/copertura:
 *  post:
 *    tags:
 *      - v2
 *    description: Va sul portale di Wind ed effettua il controllo sulla copertura.
 *    consume:
 *      - application/json
 *    produces:
 *      - application/json
 *    responses:
 *      200:
 *        description: SuccessResponse
 *    parameters:
 *      - in: body
 *        description:
 *          pod da controllare.
 *        schema:
 *          type: object
 *          required:
 *            - comune
 *            - provincia
 *            - indirizzo
 *            - civico
 *          properties:
 *            comune:
 *              type: string
 *            provincia:
 *              type: string
 *            indirizzo:
 *              type: string
 *            civico:
 *              type: string
 *            numeroFissoPrefisso:
 *              type: string
 *              nullable: true
 *            numeroFisso:
 *              type: string
 *              nullable: true
 *            numeroMobile:
 *              type: string
 *              nullable: true
 *
 * /scraper/queue/windCheckCopertura:
 *  post:
 *    tags:
 *      - v2
 *      - queue
 *    description:
 *      Va sul portale di Wind ed effettua il controllo sulla copertura.
 *      Il payload è lo stesso della rotta diretta `/scraper/v2/wind/check/copertura`,
 *      in più va aggiunto il webhook che lo scraper poi andrà a richiamare per notificare l'esito.
 *    consume:
 *      - application/json
 *    produces:
 *      - application/json
 *    responses:
 *      200:
 *        description: SuccessResponse
 */
