export type WindCheckCoperturaPayload = {
  comune: string;
  provincia: string;
  indirizzo: string;
  civico: string;
  numeroFissoPrefisso?: string;
  numeroFisso?: string;
  numeroMobile?: string;
};
