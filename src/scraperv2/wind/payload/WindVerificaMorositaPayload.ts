import { Credenziali } from "../../../credenziali/getCredenzialiDalServizio";

export type WindVerificaMorositaPayload = { // Credenziali & {
  comune: string;
  provincia: string;
  indirizzo: string;
  civico: string;
  codiceFiscale: string;
};
