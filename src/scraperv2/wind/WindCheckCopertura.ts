import { WebScraper, ScraperResponse } from "../scraper";
import { WindCheckCoperturaPayload } from "./payload/WindCheckCoperturaPayload";
import { ScraperTipologia, SCRAPER_TIPOLOGIA } from "../ScraperOptions";
import { acceptCookieEvaluated } from "../../browser/page/acceptCookie";
import waitForXPathAndClick from "../../browser/page/waitForXPathAndClick";
import inserisciDatiDaVerificare from "./check/copertura/inserisciDatiDaVerificare";
import tryOrThrow from "../../utils/tryOrThrow";
import recuperaDatiPerEsito from "./check/copertura/recuperaDatiPerEsito";
import creaEsitoDaiDatiDelSito from "./check/copertura/creaEsitoDaiDatiDelSito";
import FailureResponse from "../../response/FailureResponse";
import ErrorResponse from "../../response/ErrorResponse";
import extractErrorMessage from "../../utils/extractErrorMessage";
import { QueueTask } from "../../task/QueueTask";
import delay from "../../utils/delay";

const SCREENSHOT_FILENAME = 'wind-check-copertura';

@QueueTask()
export default class WindCheckCopertura extends WebScraper<WindCheckCoperturaPayload> {
  static readonly HOMEPAGE_URL = 'https://www.windtre.it/offerte-fibra/super-fibra-unlimited-partner/';

  login(payload: WindCheckCoperturaPayload): Promise<void> {
    return Promise.resolve();
  }

  async scrapeWebsite(payload: WindCheckCoperturaPayload): Promise<ScraperResponse> {
    const logger = this.childLogger;

    const page = await this.p();

    try {
      await page.setViewport({ width: 1920, height: 1080 });

      logger.info('apro la pagina per la verifica della copertura');
      await page.goto(WindCheckCopertura.HOMEPAGE_URL, { waitUntil: 'networkidle2', timeout: 120000 });

      logger.info('accetto i cookie e apro la modale per la verifica');
      await tryOrThrow(
        () => Promise.all(
          [
            acceptCookieEvaluated(page, 'div#modalCookie', 'a#acceptAll'),
            acceptCookieEvaluated(page, 'div#cookieModal', '//button[text()=\'ACCETTA\']', true),
          ]
        ),
        'non sono riuscito ad accettare i cookie:'
      );

      await waitForXPathAndClick(page, '//a[contains(., "verifica e attiva")]');

      logger.info('inserisco i dati passati');
      await tryOrThrow(
        () => inserisciDatiDaVerificare(page, logger, payload),
        'non sono riuscito ad inserire i dati da verificare:'
      );

      logger.info("recupero i dati per l'esito");
      const dati = await recuperaDatiPerEsito(page);

      logger.info("genero l'esito");
      const esito = await creaEsitoDaiDatiDelSito(dati, payload, page);
      const esitoLogMsg = `Restituisco l'esito ${JSON.stringify(esito)}`;
      if (esito instanceof FailureResponse || esito instanceof ErrorResponse) {
        logger.error(esitoLogMsg);
        try {
          await this.saveScreenshot(page, logger, SCREENSHOT_FILENAME);
        } catch (ex) {
          return new FailureResponse(extractErrorMessage(ex));
        }
      } else {
        logger.info(esitoLogMsg);
      }

      return esito;
    } catch (ex) {
      let errMsg = extractErrorMessage(ex);
      logger.error(errMsg);
      try {
        await this.saveScreenshot(page, logger, SCREENSHOT_FILENAME);
      } catch (scEx) {
        errMsg = extractErrorMessage(scEx);
      }
      return new FailureResponse(errMsg);
    }
  }

  getScraperCodice(): string {
    return 'wind-check-copertura';
  }

  getScraperTipologia(): ScraperTipologia {
    return SCRAPER_TIPOLOGIA.FATTIBILITA;
  }
}
