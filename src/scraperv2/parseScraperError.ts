import TimeoutError from "../errors/TimeoutError";
import WrongCredentialsError from "../errors/WrongCredentialsError";
import ErrorResponse from "../response/ErrorResponse";

export default function parseScraperError(error: Error) {
  if (error instanceof WrongCredentialsError || error instanceof TimeoutError) {
    return error.toResponse();
  }
  return new ErrorResponse(error.message);
}
