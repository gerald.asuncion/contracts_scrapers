/**
 * @openapi
 *
 * /scraper/v2/italgas:
 *  post:
 *    tags:
 *      - v2
 *    description: Va sul portale di Italgas e controlla se il pdr è valido.
 *    consume:
 *      - application/json
 *    produces:
 *      - application/json
 *    responses:
 *      200:
 *        description: SuccessResponse
 *    parameters:
 *      - in: body
 *        description:
 *          pdr da controllare.
 *        schema:
 *          type: object
 *          required:
 *            - pdr
 *            - tipoContratto
 *          properties:
 *            pdr:
 *              type: string
 *            tipoContratto:
 *              type: string
 */
