import { ScraperResponse, WebScraper } from '../scraper';
import stepSelezioneServizio from './steps/stepSelezioneServizio';
// import stepSelezioneRete from './steps/stepSelezioneRete';
import stepInserimentoPdr from './steps/stepInserimentoPdr';
import { ItalgasAbstractOptions } from './option/ItalgasAbstractOptions';
import { ItalgasAbstractPayload } from './payload/ItalgasAbstractPayload';
import stepVerificaEsito from './steps/stepVerificaEsito';
import ErrorResponse from '../../response/ErrorResponse';
import extractErrorMessage from '../../utils/extractErrorMessage';
import FailureResponse from '../../response/FailureResponse';
import CasoNonGestitoResponse from '../../response/CasoNonGestitoResponse';
import { SCRAPER_TIPOLOGIA } from '../ScraperOptions';
import italgasLogin from '../../v3/italgas/utils/login';

export default abstract class ItalgasAbstract extends WebScraper {
  static readonly LOGIN_URL: string = 'https://www.gas2be.it/s/login/';

  protected dominio: string;

  constructor(options: ItalgasAbstractOptions) {
    super(options);
    this.dominio = '00489490011';
  }

  /**
   * @override
   */
  getLoginTimeout(): number { return 60000; }

  async login(): Promise<void> {
    const credenziali = await this.getCredenziali();

    const page = await this.p();

    const logger = this.childLogger;

    return italgasLogin({ page, logger, credenziali, loginUrl: ItalgasAbstract.LOGIN_URL });
  }

  async scrapeWebsite({ pdr, tipoContratto }: ItalgasAbstractPayload): Promise<ScraperResponse> {
    const logger = this.childLogger;

    logger.info('login > terminata');

    try {
      const page = await this.p();

      await stepSelezioneServizio(page, logger);

      // await stepSelezioneRete(page, this.dominio, logger);

      await stepInserimentoPdr(page, pdr, logger);

      logger.info("controllo l'esito");
      const esito = await stepVerificaEsito(page, tipoContratto);

      if (esito) {
        const { details } = esito;
        if (esito instanceof FailureResponse || esito instanceof ErrorResponse) {
          logger.error(details);
        } else {
          logger.info(`esito positivo - ${details}`);
        }

        return esito;
      }

      logger.error('caso non gestito');
      return new CasoNonGestitoResponse();
    } catch (ex) {
      const errMsg = extractErrorMessage(ex);
      logger.error(errMsg);
      return new ErrorResponse(errMsg);
    }
  }

  getScraperCodice() {
    return 'italgas';
  }

  getScraperTipologia() {
    return SCRAPER_TIPOLOGIA.FATTIBILITA;
  }
}
