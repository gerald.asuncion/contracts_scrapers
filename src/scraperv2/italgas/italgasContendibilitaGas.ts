import { QueueTask } from '../../task/QueueTask';
import ItalgasAbstract from './italgas-abstract';
import { ItalgasAbstractOptions } from './option/ItalgasAbstractOptions';

@QueueTask()
export default class ItalgasContendibilitaGas extends ItalgasAbstract {
  constructor(options: ItalgasAbstractOptions) {
    super(options);
    this.dominio = '00489490011';
  }
}
