import { Page } from 'puppeteer';

export default async function getRowFieldText(page: Page, text: string, insertLastChildEl = true): Promise<string> {
  const element = await page.waitForXPath(`//label[starts-with(text(), "${text}")]/following-sibling::*[1]${insertLastChildEl ? '/p' : ''}`);
  return page.evaluate((el) => el.innerText, element);
}
