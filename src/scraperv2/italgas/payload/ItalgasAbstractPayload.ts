export type ItalgasAbstractPayload = {
  pdr: string;
  tipoContratto: string;
};
