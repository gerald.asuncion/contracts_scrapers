import { QueueTask } from '../../task/QueueTask';
import ScraperOptions from '../ScraperOptions';
import ItalgasAbstract from './italgas-abstract';

@QueueTask()
export default class ItalgasAttivazione extends ItalgasAbstract {
  constructor(options: ScraperOptions) {
    super(options);
    this.dominio = '00489490011';
  }
}
