import { Page } from 'puppeteer';
import SuccessResponse from '../../../response/SuccessResponse';
import { ScraperResponse } from '../../scraper';
import getRowFieldText from '../utils/getRowFieldText';

export default async function stepRecuperoRisultato(page: Page): Promise<ScraperResponse> {
  const indirizzo = (await Promise.all([
    await getRowFieldText(page, 'Sigla Toponomastica'),
    await getRowFieldText(page, 'Indirizzo fornitura'),
    await getRowFieldText(page, 'Civico')
  ])).join(' ');

  const comune = (await Promise.all([
    await getRowFieldText(page, 'Comune'),
    await getRowFieldText(page, 'Provincia'),
    await getRowFieldText(page, 'CAP')
  ])).join(' ');

  const extra = [
    await getRowFieldText(page, 'Scala'),
    await (async () => {
      const interno = await getRowFieldText(page, 'Interno');
      return interno ? `interno ${interno}` : '';
    })()
  ].filter(Boolean).join(' ');

  const portata = await getRowFieldText(page, 'Potenzialità PDR');

  const statoDelibera = await getRowFieldText(page, 'Stato Att. delibera 40/14');

  const matricolaContatore = await getRowFieldText(page, 'Matricola contatore');

  // eslint-disable-next-line max-len
  return new SuccessResponse(`${indirizzo}${extra ? ` ${extra}` : ''}, ${comune}, Portata termica: ${portata} Stato Att. delibera 40/14: ${statoDelibera}`, { 'matricola_contatore_arera': matricolaContatore });
}
