import { Page } from 'puppeteer';
import waitForNavigation from '../../../browser/page/waitForNavigation';
import waitForSelectorAndClick from '../../../browser/page/waitForSelectorAndClick';
import { Logger } from '../../../logger';

export default async function stepSelezioneServizio(page: Page, logger: Logger): Promise<void> {
  logger.info('attendo caricamento home page');
  await page.waitForSelector('body > div.cPC_ThemeLayout > div.mainContentArea', { visible: true });

  logger.info('attendo che sparisca overlay di caricamento');
  try {
    await page.waitForSelector(
      // eslint-disable-next-line max-len
      'body > div.cPC_ThemeLayout > div.mainContentArea > div.siteforceSldsOneColLayout.siteforceContentArea > div.slds-col--padded.comm-content-footer.comm-layout-column > div > div:nth-child(5) > div',
      { visible: false }
    );
  } catch (ex) {
    // sparita prima di poterla cercare
  }

  await page.waitForSelector(
    // eslint-disable-next-line max-len
    'body > div.cPC_ThemeLayout > div.mainContentArea > div.siteforceSldsOneColLayout.siteforceContentArea > div.slds-col--padded.comm-content-header.comm-layout-column > div > div > div > span:nth-child(1) > lightning-dynamic-icon',
    { visible: true }
  );

  logger.info('apro il menu');
  await page.evaluate(() => {
    // eslint-disable-next-line max-len
    document.querySelector<HTMLElement>('body > div.cPC_ThemeLayout > div.mainContentArea > div.siteforceSldsOneColLayout.siteforceContentArea > div.slds-col--padded.comm-content-header.comm-layout-column > div > div > div > span:nth-child(1) > lightning-dynamic-icon')?.click();
  });

  logger.info('attendo apertura del menu');
  await page.waitForSelector('#divMenu', { visible: true });

  await waitForSelectorAndClick(page, '#divMenu lightning-vertical-navigation-item-icon:nth-child(3)');
  await page.waitForSelector('#divMenu lightning-vertical-navigation-item-icon:nth-child(3) ~ div a', { visible: true });
  await Promise.all([
    waitForNavigation(page),
    waitForSelectorAndClick(page, '#divMenu lightning-vertical-navigation-item-icon:nth-child(3) ~ div a')
  ]);

  logger.info('selezionato servizio `Lavora sulla rete`');
}
