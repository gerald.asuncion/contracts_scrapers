import { Page } from 'puppeteer';
import waitForSelectorAndClick from '../../../browser/page/waitForSelectorAndClick';
import waitForSelectorAndType from '../../../browser/page/waitForSelectorAndType';
import { Logger } from '../../../logger';
import waitForXPathAndClick from '../../../browser/page/waitForXPathAndClick';
import tryOrThrow from '../../../utils/tryOrThrow';

export default async function stepInserimentoPdr(page: Page, pdr: string, logger: Logger): Promise<void> {
  logger.info('seleziono `Utilità e reportistica`');
  await tryOrThrow(
    () => waitForXPathAndClick(page, '//a[contains(., "Utilità e Reportistica")]'),
    'non sono riuscito a cliccare sul link `Utilità e Reportistica`:'
  );

  logger.info('attendo apertura del menu');
  await page.waitForSelector('#submenu-list-6 > div', { visible: true });

  logger.info('seleziono dal menu che è apparso `Interrogazione PDR`');
  await tryOrThrow(
    () => waitForXPathAndClick(page, '//a[contains(., "Interrogazione Pdr")]'),
    'non sono riuscito a cliccare sul link `Interrogazione PDR`:'
  );

  logger.info('attendo cambio contenuto della pagina');
  await page.waitForSelector('#SIQ001', { visible: true });

  await waitForSelectorAndType(page, '#pdr', pdr);
  logger.info(`inserisco pdr ${pdr}`);

  await waitForSelectorAndClick(page, '#report');
  logger.info('attendo apertura report');
}
