import { Page } from 'puppeteer';
import FailureResponse from '../../../response/FailureResponse';
import getRowFieldText from '../utils/getRowFieldText';
import SuccessResponse from '../../../response/SuccessResponse';
import stepRecuperoRisultato from './stepRecuperoRisultato';
import ErrorResponse from '../../../response/ErrorResponse';

export async function recuperaErrore(page: Page): Promise<string | null> {
  try {
    await page.waitForSelector('#errorDialog', { visible: true, timeout: 3000 });
  } catch (ex) {
    // nessun errore trovato, posso andare oltre
    return null;
  }

  // se scoppia qui significa che lo scraper va fixato => stop scraper
  let errorHtml = await page.evaluate(() => {
    const el = document.querySelector('#errorDialog > div > div > div.modal-body.text-center > p:nth-child(2)');
    return (el as HTMLElement).innerHTML;
  });

  // errorHtml = errorHtml.replace(/<strong>(([a-zA-Z]|\s)*:\s)<\/strong>/g, '');
  errorHtml = errorHtml.replace(/<strong>.+<\/strong>/g, '');

  return errorHtml;
}

export default async function stepVerificaEsito(page: Page, tipoContratto: string): Promise<FailureResponse | SuccessResponse | null> {
  let errore = await recuperaErrore(page);

  if (errore) {
    errore = errore.toLowerCase();
    if (errore.includes("pdr associato ad altra societa' di vendita")) {
      return new FailureResponse('KO - PDR non contendibile');
    }

    if (errore.includes('pdr inesistente')) {
      return new FailureResponse('KO - PDR inesistente');
    }

    if (errore.includes('pdr gestito ad altro distributore') || errore.includes('pdr appartenente ad altro distributore')) {
      return new ErrorResponse('pdr gestito da altro distributore');
    }
  }

  // modale non apparsa passo agli altri tipi di errore
  const societaDiVenditaAbbinata = (await getRowFieldText(page, 'Società di vendita abbinata')).toLowerCase();

  if (societaDiVenditaAbbinata && !societaDiVenditaAbbinata.includes('nessuna società di vendita associata')) {
    return new FailureResponse('KO - PDR non contendibile');
  }

  const statoContatore = (await getRowFieldText(page, 'Stato contatore')).toLowerCase();
  if (statoContatore.includes("chiuso per morosita'")) {
    return new FailureResponse('KO - PDR non contendibile');
  }

  const statoTecnico = (await getRowFieldText(page, 'Stato Tecnico')).toLowerCase();
  if (
    (!societaDiVenditaAbbinata || societaDiVenditaAbbinata.includes('nessuna società di vendita associata'))
    && (statoTecnico === 'in gas' || statoTecnico === 'chiuso')
  ) {
    if (!statoContatore) {
      if (tipoContratto === 'subentro') {
        return new FailureResponse('ko-procedere con una prima attivazione');
      }

      const successResponse = await stepRecuperoRisultato(page);
      return successResponse;
    }

    if (statoContatore === 'chiuso') {
      if (tipoContratto === 'attivazione') {
        return new FailureResponse('ko-procedere con un subentro');
      }

      const successResponse = await stepRecuperoRisultato(page);
      return successResponse;
    }
  }

  if (statoTecnico === 'non in gas' && (!statoContatore || statoContatore === 'pdr non in gas, contatore aperto')) {
    return new FailureResponse('KO - PDR rimosso/interrotto per morosità');
  }

  return null;
}
