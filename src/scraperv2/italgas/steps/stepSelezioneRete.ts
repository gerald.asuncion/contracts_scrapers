import { Page } from 'puppeteer';
import waitForSelectorAndClick from '../../../browser/page/waitForSelectorAndClick';
import waitForSelectorAndSelect from '../../../browser/page/waitForSelectorAndSelect';
import { Logger } from '../../../logger';

export default async function stepSelezioneRete(
  page: Page,
  value: string,
  logger: Logger
): Promise<void> {
  logger.info('attendo caricamento pagina selezione rete');
  await page.waitForSelector('#loginForm', { visible: true });

  const [reteSelezionata] = await waitForSelectorAndSelect(page, 'select[name="dominio"]', [value]);
  if (!reteSelezionata || reteSelezionata !== value) {
    throw new Error(`impossibile selezionare la rete '${value}'`);
  }
  logger.info(`selezionata rete '${value}'`);

  logger.info('proseguo al passo successivo');
  await Promise.all([
    waitForSelectorAndClick(page, '#loginForm > div > div.panel-body > div.text-center > button'),
    page.waitForNavigation({ waitUntil: 'networkidle2' })
  ]);
}
