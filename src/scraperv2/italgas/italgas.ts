import ItalgasAbstract from './italgas-abstract';
import ScraperOptions from '../ScraperOptions';
import { QueueTask } from '../../task/QueueTask';

@QueueTask()
export default class Italgas extends ItalgasAbstract {
  constructor(options: ScraperOptions) {
    super(options);
    this.dominio = '00489490011';
  }
}
