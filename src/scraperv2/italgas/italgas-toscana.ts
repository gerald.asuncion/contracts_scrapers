import { QueueTask } from '../../task/QueueTask';
import ScraperOptions from '../ScraperOptions';
import ItalgasAbstract from './italgas-abstract';

@QueueTask()
export default class ItalgasToscana extends ItalgasAbstract {
  constructor(options: ScraperOptions) {
    super(options);
    this.dominio = '05608890488';
  }
}
