/**
 * @openapi
 *
 * /scraper/v2/italgas/contendibilita/gas:
 *  post:
 *    tags:
 *      - v2
 *    description: Va sul portale di Italgas e controlla se il pdr è valido.
 *    consume:
 *      - application/json
 *    produces:
 *      - application/json
 *    responses:
 *      200:
 *        description: SuccessResponse
 *    parameters:
 *      - in: body
 *        description:
 *          pdr da controllare.
 *        schema:
 *          type: object
 *          required:
 *            - pdr
 *            - tipoContratto
 *          properties:
 *            pdr:
 *              type: string
 *            tipoContratto:
 *              type: string
 *
 * /scraper/queue/italgasContendibilita:
 *  post:
 *    tags:
 *      - v2
 *      - queue
 *    description:
 *      Inserisce in queue il task per effettuare il check tramite lo scraper ItalgasContendibilita.
 *      Il payload è lo stesso della rotta diretta `/scraper/v2/italgas/contendibilita/gas`,
 *      in più va aggiunto il webhook che lo scraper poi andrà a richiamare per notificare l'esito.
 *    consume:
 *      - application/json
 *    produces:
 *      - application/json
 *    responses:
 *      200:
 *        description: SuccessResponse
 */
