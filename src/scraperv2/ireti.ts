import { ScraperResponse, WebScraper } from "./scraper";
import FailureResponse from "../response/FailureResponse";
import SuccessResponse from "../response/SuccessResponse";
import { SCRAPER_TIPOLOGIA } from "./ScraperOptions";

export default class Ireti extends WebScraper {
  private credential = {
    username: 'MAGTUT',
    password: 'IrenMercato21'
  }

  private search = 'http://ee.ireti.it/CNRGWebDistributore/richiestanuovaoperazione.do?todo=interrogaPod';

  async login(): Promise<void> {
    const page = await this.p()
    const login = 'http://ee.ireti.it/CNRGWebDistributore/login.do?distributore=aemd';
    await page.goto(login);
    try {
      await page.type('input[name=user]', this.credential.username);
      await page.type('input[name=password]', this.credential.password);
      await page.click('input[type=submit]');
    } catch (e) {

    }
  }

  async scrapeWebsite({pod}: any): Promise<ScraperResponse> {
    const page = await this.p();
    await page.goto(this.search);
    await page.click('input[name=pod]');
    await page.keyboard.down('Control');
    await page.keyboard.press('KeyA');
    await page.keyboard.up('Control');
    await page.keyboard.press('Backspace');
    await page.type('input[name=pod]', pod);
    await page.click('input[name=bottone]');
    await page.waitForSelector('.tab_dati');
    let message;
    try {
      message = await page.$eval('.tabellaCentrataRiepilogo li', msg => msg.innerHTML);
      if (message) {
        return new FailureResponse(message.replace('\n', '').replace('\t', '').trim());
      }
    } catch (e) {
    }

    return new SuccessResponse();
  }

  getScraperCodice() {
    return 'ireti';
  }

  getScraperTipologia() {
    return SCRAPER_TIPOLOGIA.FATTIBILITA;
  }
}
