import waitCheckCredentials from '../../browser/page/waitCheckCredentials';
import waitForSelectorAndClick from '../../browser/page/waitForSelectorAndClick';
import waitForSelectorAndType from '../../browser/page/waitForSelectorAndType';
import ErrorResponse from '../../response/ErrorResponse';
import SuccessResponse from '../../response/SuccessResponse';
import { QueueTask } from '../../task/QueueTask';
import createChildPayloadAwareLogger from '../../utils/createChildPayloadAwareLogger';
import extractErrorMessage from '../../utils/extractErrorMessage';
import tryOrThrow from '../../utils/tryOrThrow';
import { ScraperResponse, WebScraper } from '../scraper';
import { SCRAPER_TIPOLOGIA } from '../ScraperOptions';
import stepConferma from './linkem/stepConferma';
import stepInserimentoDatiAgenzia from './linkem/stepInserimentoDatiAgenzia';
import stepInserimentoDatiAnagrafici from './linkem/stepInserimentoDatiAnagrafici';
import stepInserimentoDatiOrdine from './linkem/stepInserimentoDatiOrdine';
import stepInserimentoDatiPagamento from './linkem/stepInserimentoDatiPagamento';
import stepNavigaPaginaVenditaProdotti from './linkem/stepNavigaPaginaVenditaProdotti';
import IrenLinkemPayload from './payload/IrenLinkemPayload';

@QueueTask({scraperName: 'irenLinkem'})
export default class IrenLinkem extends WebScraper<IrenLinkemPayload> {
  static readonly LOGIN_URL = 'https://iren-console.ennova.it/#/login';

  async login(payload: IrenLinkemPayload): Promise<void> {
    const logger = createChildPayloadAwareLogger(this.childLogger, payload);
    const { username, password } = await this.getCredenziali();

    logger.info('login > inizio');
    const page = await this.p();

    await page.goto(IrenLinkem.LOGIN_URL, { waitUntil: 'networkidle2' });

    try {
      await page.waitForSelector('.login-form');
    } catch (ex) {
      // utente già loggato, quindi nulla da fare
      logger.info('login > utente già loggato');
      return;
    }

    logger.info('login > inserisco username');
    await tryOrThrow(
      () => waitForSelectorAndType(page, '#page-content-wrapper > div.ng-scope > div > div > form > input:nth-child(1)', username),
      'non sono riuscito ad inserire l\'username'
    );

    logger.info('login > inserisco la password');
    await tryOrThrow(
      () => waitForSelectorAndType(page, '#page-content-wrapper > div.ng-scope > div > div > form > input:nth-child(2)', password),
      'non sono riuscito ad inserire la password'
    );

    logger.info('login > clicco sul pulsante `login`');
    await tryOrThrow(
      () => waitForSelectorAndClick(page, '#page-content-wrapper > div.ng-scope > div > div > form > button'),
      'non sono riuscito a cliccare sul pulsante `login`'
    );

    logger.info('login > controllo se le credenziali sono scadute');
    await waitCheckCredentials(
      page,
      'body > div.modal.fade.ng-isolate-scope.in > div > div > div.modal-body.ng-scope',
      IrenLinkem.LOGIN_URL,
      username,
      logger,
      { timeout: 3000 }
    );

    logger.info('login > fine');
  }

  async scrapeWebsite(payload: IrenLinkemPayload): Promise<ScraperResponse> {
    const logger = createChildPayloadAwareLogger(this.childLogger, payload);

    const page = await this.p();

    try {
      logger.info('attendo creazione home page');
      await tryOrThrow(
        () => page.waitForSelector('#sidebar-wrapper'),
        'l\'home page non si è caricata in tempo'
      );

      logger.info('step > navigazione alla pagina `Vendita prodotti`');
      await tryOrThrow(() => stepNavigaPaginaVenditaProdotti(page), 'step navigazione alla pagina `Vendita prodotti`:');

      logger.info('step > dati dell\'agenzia');
      await tryOrThrow(() => stepInserimentoDatiAgenzia(page, payload.codiceOperatore), 'step dati dell\'agenzia:');

      logger.info('step > dati anagrafici del cliente');
      await tryOrThrow(() => stepInserimentoDatiAnagrafici(page, payload), 'step dati anagrafici del cliente:');

      logger.info('step > dati ordine');
      await tryOrThrow(() => stepInserimentoDatiOrdine(page, payload), 'step dati ordine:');

      logger.info('step > dati pagamento');
      await tryOrThrow(() => stepInserimentoDatiPagamento(page, payload), 'step dati pagamento');

      this.checkNoSubmit(payload);

      logger.info('step > conferma');
      await tryOrThrow(() => stepConferma(page), 'step conferma');

      return new SuccessResponse('ok');
    } catch (ex) {
      await this.saveScreenshot(page, logger, 'iren-linkem-errore', payload.idDatiContratto);
      const errMsg = extractErrorMessage(ex);
      logger.error(errMsg);
      return new ErrorResponse(errMsg);
    }
  }

  getScraperCodice() {
    return 'iren-linkem';
  }

  getScraperTipologia() {
    return SCRAPER_TIPOLOGIA.INSERIMENTO;
  }
}
