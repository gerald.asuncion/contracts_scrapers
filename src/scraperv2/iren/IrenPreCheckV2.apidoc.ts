/**
 * @openapi
 *
 * /scraper/v2/iren/iren-pre-check-switch:
 *  post:
 *    tags:
 *      - v2
 *    description: Effettua il pre check sul portale di Iren.
 *      Questa è la rotta diretta allo scraper, quindi non usa il meccanismo del webhook.
 *    consume:
 *      - application/json
 *    produces:
 *      - application/json
 *    responses:
 *      200:
 *        description: SuccessResponse
 *    parameters:
 *      - in: body
 *        schema:
 *          type: object
 *          required:
 *            - fornitura
 *            - codice
 *            - fornitore
 *            - tipoContratto
 *          properties:
 *            fornitura:
 *              type: string
 *              enum: [Gas, Elettricità]
 *            codice:
 *              type: string
 *            fornitore:
 *              type: string
 *            tipoContratto:
 *              type: string
 *              enum: [switch, subentro, attivazione, voltura]
 *            nome:
 *              type: string
 *              nullable: true
 *            cognome:
 *              type: string
 *              nullable: true
 *            codiceFiscale:
 *              type: string
 *              nullable: true
 *            codiceFiscaleSocieta:
 *              type: string
 *              nullable: true
 *            partitaIva:
 *              type: string
 *              nullable: true
 *            ragioneSociale:
 *              type: string
 *              nullable: true
 *
 * /scraper/queue/irenPreCheckV2:
 *  post:
 *    tags:
 *      - v2
 *      - queue
 *    description:
 *      Effettua il pre check sul portale di Iren.
 *      Il payload è lo stesso della rotta diretta `/scraper/v2/iren/iren-pre-check-switch`,
 *      in più va aggiunto il webhook che lo scraper poi andrà a richiamare per notificare l'esito.
 *    consume:
 *      - application/json
 *    produces:
 *      - application/json
 *    responses:
 *      200:
 *        description: SuccessResponse
 */
