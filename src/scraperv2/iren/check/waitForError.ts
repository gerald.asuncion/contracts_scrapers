import { Page } from "puppeteer";
import waitForVisible from "../../../browser/page/waitForVisible";
import FailureResponse from "../../../response/FailureResponse";
import ErrorResponse from "../../../response/ErrorResponse";

const errorMessageSelector = '.slds-theme--error[data-key="error"] > .toastContent';
const errorsWrapperSelector = '[role="alert"].error.uiMessage.cUtilityLightning';

export default async function waitForError(page: Page) {
  try {
    await waitForVisible(page, errorMessageSelector, { timeout: 5000 });

    let errorMessageElement = await page.$(errorMessageSelector);
    let errorMessage = await page.evaluate(el => el.textContent, errorMessageElement);
    return new ErrorResponse(errorMessage);
  } catch {
    // errore non apparso
    // nulla da fare
  }

  try {
    await waitForVisible(page, '.uiOutputText');
  } catch (ex) {
    let errors;
    try {
      await waitForVisible(page, errorsWrapperSelector);
      // eslint-disable-next-line max-len
      errors = await page.$eval(`${errorsWrapperSelector} > .uiBlock > .bBody`, (el) => (el as HTMLDivElement).innerText?.replace(/\n/g, ' ').trim());
    } catch (ex2) {
      errors = 'Il check ha impiegato troppo tempo';
    }

    if (errors) {
      return new ErrorResponse(errors);
    }
  }
}
