import { Page } from "puppeteer";
import getElementTextByXPath from "../../../browser/page/getElementTextByXPath";

export type DatiEsito = {
  esito: string;
  dettaglioEsitoPrecheck: string;
  venditoreUscente: string;
  esitoCompatibilita: string;
  dettaglioEsitoCompatibilita: string;
};

export default async function recuperaDatiPerEsito(page: Page): Promise<DatiEsito> {
  const esito = await getElementTextByXPath(page, '//label[contains(., "Esito PreCheck")]/following-sibling::span', 'textContent') as string;

  const dettaglioEsitoPrecheck = await getElementTextByXPath(page, '//label[contains(., "Dettaglio esito PreCheck")]/following-sibling::span', 'textContent') as string;

  const venditoreUscente = await getElementTextByXPath(page, '//label[contains(., "Venditore Uscente")]/following-sibling::span', 'textContent') as string;

  const esitoCompatibilita = await getElementTextByXPath(page, '//label[contains(., "Esito Compatibilità")]/following-sibling::span', 'textContent') as string;

  const dettaglioEsitoCompatibilita = await getElementTextByXPath(page, '//label[contains(., "Dettaglio Compatibilità")]/following-sibling::span', 'textContent') as string;

  return {
    esito,
    dettaglioEsitoPrecheck,
    venditoreUscente,
    esitoCompatibilita,
    dettaglioEsitoCompatibilita
  };
}
