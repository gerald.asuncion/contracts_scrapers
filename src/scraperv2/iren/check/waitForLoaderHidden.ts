import { Page } from "puppeteer";
import waitForVisible from "../../../browser/page/waitForVisible";
import waitForHidden from "../../../browser/page/waitForHidden";

const loadingSpinnerSelector = 'lightning-spinner';

export default async function waitForLoaderHidden(page: Page) {
  try {
    await waitForVisible(page, loadingSpinnerSelector, { timeout: 10000 });
  } catch {
  }

  await waitForHidden(page, loadingSpinnerSelector);
}
