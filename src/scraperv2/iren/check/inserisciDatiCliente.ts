import { Page } from "puppeteer";
import capitalize from 'lodash/capitalize';
import { IrenPreCheckPayload } from "../payload/IrenPreCheckPayload";
import tryOrThrow from "../../../utils/tryOrThrow";
import waitForSelectorAndSelect from "../../../browser/page/waitForSelectorAndSelect";
import waitForSelectorAndType from "../../../browser/page/waitForSelectorAndType";
import isBusiness from "../helpers/isBusiness";
import isStraniero from "../../../utils/isStraniero";
import waitForSelectorAndClick from "../../../browser/page/waitForSelectorAndClick";
import waitForXPathAndClick from "../../../browser/page/waitForXPathAndClick";
import waitForLoaderHidden from "./waitForLoaderHidden";

export  default async function inserisciDatiCliente(page: Page, payload: IrenPreCheckPayload) {
  const { fornitura, codice, } = payload;

  await tryOrThrow(
    () => waitForSelectorAndSelect(page, '[name=selectClasse]', [capitalize(fornitura)]),
    'non sono riuscito ad inserire la fornitura:'
  );

  await tryOrThrow(
    () => waitForSelectorAndType(page, '[placeholder="Inserisci un punto di fornitura"]', codice.toUpperCase()),
    'non sono riuscito ad inserire il punto di fornitura:'
  );

  const codiceFiscaleDaInserire = (
    isBusiness(payload) ? payload.codiceFiscaleSocieta : payload.codiceFiscale
  ).toUpperCase();
  await tryOrThrow(
    () => waitForSelectorAndType(page, '[placeholder="Inserisci Codice Fiscale"]', codiceFiscaleDaInserire),
    'non sono riuscito ad inserire il codice fiscale:'
  );

  if (isBusiness(payload)) {
    const { partitaIva, ragioneSociale } = payload;

    await tryOrThrow(
      () => waitForSelectorAndType(page, '[placeholder="Inserisci Partita Iva"]', partitaIva),
      'non sono riscito ad inserire la partita iva:'
    );

    await tryOrThrow(
      () => waitForSelectorAndType(page, '[placeholder="Inserisci ragione sociale"]', ragioneSociale),
      'non sono riscito ad inserire la ragione sociale:'
    );
  } else {
    const { nome, cognome, codiceFiscale } = payload;

    await tryOrThrow(
      () => waitForSelectorAndType(page, '[placeholder="Inserisci Nome"]', nome),
      'non sono riscito ad inserire il nome:'
    );

    await tryOrThrow(
      () => waitForSelectorAndType(page, '[placeholder="Inserisci il Cognome"]', cognome),
      'non sono riscito ad inserire il cognome:'
    );

    if (isStraniero(codiceFiscale)) {
      await tryOrThrow(
        () => waitForSelectorAndClick(page, 'label[for=codiceFiscaleStraniero]'),
        'non sono riuscito ad indicare che è un codice fiscale straniero:'
      );
    }
  }

  await tryOrThrow(
    () => waitForSelectorAndSelect(page, '[name=AzioneComm]', ['Switch in']),
    "non sono riuscito a selezionare l'azione commerciale:"
  );

  await tryOrThrow(
    () => waitForXPathAndClick(page, '//button[contains(., "Check Cliente")]'),
    'non sono riuscito a cliccare sul pulsante ``Check Cliente`:'
  );

  await waitForLoaderHidden(page);
}
