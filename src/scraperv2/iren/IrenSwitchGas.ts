import { Logger } from '../../logger';
import IrenRemiRepo from '../../repo/IrenRemiRepo';
import FailureResponse from '../../response/FailureResponse';
import SuccessResponse from '../../response/SuccessResponse';
import { QueueTask } from '../../task/QueueTask';
import DbScraper, { DbScraperOptions } from '../DbScraper';
import { ScraperResponse } from '../scraper';
import { SCRAPER_TIPOLOGIA } from '../ScraperOptions';
import { IrenFattibilitaPayload } from './IrenFattibilitaPayload';

type IrenSwitchGasOptions = DbScraperOptions & {
  irenRemiRepo: IrenRemiRepo;
  logger: Logger;
};

@QueueTask({scraperName: 'irenSwitchGas'})
export default class IrenSwitchGas extends DbScraper {
  private repo: IrenRemiRepo;

  constructor(options: IrenSwitchGasOptions) {
    super(options);
    this.repo = options.irenRemiRepo;
  }

  async scrape(payload: IrenFattibilitaPayload): Promise<ScraperResponse> {
    const {
      comune,
      cap,
      regione,
      provincia
    } = payload;
    const { logger, repo } = this;
    logger.info(`controllo ${comune} (${cap}) in ${regione} (${provincia})`);

    const result = await repo.isValidByComuneV2(payload);
    const data = await result.data();

    if (data.length) {
      logger.info(`switch gas fattibile per il comune ${comune}`);
      return new SuccessResponse();
    }

    logger.info(`switch gas NON fattibile per il comune ${comune}`);
    return new FailureResponse(`Il comune ${comune} non è coperto dal servizio Iren Switch Gas`);
  }

  getScraperCodice() {
    return 'iren-switchgas';
  }

  getScraperTipologia() {
    return SCRAPER_TIPOLOGIA.FATTIBILITA;
  }
}
