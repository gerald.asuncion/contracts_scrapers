import { DatiEsito } from "../check/recuperaDatiPerEsito";
import { IrenPreCheckV2Payload } from "../payload/IrenPreCheckPayload";
import executorEsitoCallbacksFactory from "./executorEsitoCallbacksFactory";
import getEsitoCallbacksForSwitch from "./getEsitoCallbacksForSwitch";

export default function generaEsitoSwitch(dati: DatiEsito, payload: IrenPreCheckV2Payload) {
  const cb = executorEsitoCallbacksFactory(getEsitoCallbacksForSwitch());
  return cb(dati, payload);
}
