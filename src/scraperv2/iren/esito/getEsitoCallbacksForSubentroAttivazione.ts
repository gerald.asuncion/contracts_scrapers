import { DatiEsito } from "../check/recuperaDatiPerEsito";
import { IrenPreCheckV2Payload } from "../payload/IrenPreCheckPayload";
import FailureResponse from "../../../response/FailureResponse";
import SuccessResponse from "../../../response/SuccessResponse";
import { EsitoCallback } from "./types";

// ATTENZIONE: l'ordine è importante
const getEsitoCallbacksForSubentroAttivazione: () => EsitoCallback[] = () => [
  ({ esito, dettaglioEsitoPrecheck }: DatiEsito, payload: IrenPreCheckV2Payload) => {
    if (
      esito === 'OK' ||
      (esito === 'KO' && [
        "PDR già attivo con l'utente richiedente",
        'Presenza di dati identificativi del cliente finale non corretti per più di 2 caratteri alfanumerici',
        'Presenza di dati identificativi del cliente finale non corretti per meno di 2 caratteri alfanumerici',
        'Cliente inesistente'
      ].includes(dettaglioEsitoPrecheck))
      ) {
      return new FailureResponse('Punto di prelievo non contendibile');
    }
  },
  ({ esito, dettaglioEsitoPrecheck, esitoCompatibilita }: DatiEsito, { fornitore }: IrenPreCheckV2Payload) => {
    if (esito === 'KO' && dettaglioEsitoPrecheck === "Punto di prelievo non attivo" && esitoCompatibilita === 'KO' && fornitore === 'iren') {
      return new FailureResponse('Su questo punto di fornitura è già in attivazione un contratto Iren');
    }
  },
  ({ esito, dettaglioEsitoPrecheck, venditoreUscente }: DatiEsito, payload: IrenPreCheckV2Payload) => {
    if (esito === 'KO' && dettaglioEsitoPrecheck === "Punto di prelievo non attivo") {
      return new SuccessResponse(venditoreUscente);
    }
  },
];

export default getEsitoCallbacksForSubentroAttivazione;
