import { DatiEsito } from "../check/recuperaDatiPerEsito";
import { IrenPreCheckV2Payload } from "../payload/IrenPreCheckPayload";
import { ScraperResponse } from "../../scraper";
import { EsitoCallback } from "./types";

export default function executorEsitoCallbacksFactory(callbacks: EsitoCallback[]) {
  const execute = (dati: DatiEsito, payload: IrenPreCheckV2Payload): ScraperResponse | undefined => {
    if (!callbacks.length) return;

    let cb = callbacks.shift() as EsitoCallback;

    let esito = cb(dati, payload);

    if (esito) {
      return esito;
    }

    return execute(dati, payload);
  }

  return execute;
}
