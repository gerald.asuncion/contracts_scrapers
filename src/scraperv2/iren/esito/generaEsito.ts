import { DatiEsito } from "../check/recuperaDatiPerEsito";
import { IrenPreCheckV2Payload } from "../payload/IrenPreCheckPayload";
import { ScraperResponse } from "../../scraper";
import generaEsitoSwitch from "./generaEsitoSwitch";
import generaEsitoSubentroAttivazione from "./generaEsitoSubentroAttivazione";
import ErrorResponse from "../../../response/ErrorResponse";
import gestioneEsitoComune from "./gestioneEsitoComune";

const CB_MAP: Record<string, (dati: DatiEsito, payload: IrenPreCheckV2Payload) => ScraperResponse | undefined> = {
  'switch': generaEsitoSwitch,
  'voltura': generaEsitoSwitch,
  'subentro': generaEsitoSubentroAttivazione,
  'attivazione': generaEsitoSubentroAttivazione
};

export default function generaEsito(dati: DatiEsito, payload: IrenPreCheckV2Payload) {
  const cb = CB_MAP[payload.tipoContratto];

  if (!cb) {
    throw new Error(`funzione per generare l'esito per il tipo contratto ${payload.tipoContratto} non trovata`);
  }

  return cb(dati, payload) || gestioneEsitoComune(dati, payload) || new ErrorResponse(dati.dettaglioEsitoPrecheck);
}
