import { DatiEsito } from "../check/recuperaDatiPerEsito";
import { IrenPreCheckV2Payload } from "../payload/IrenPreCheckPayload";
import FailureResponse from "../../../response/FailureResponse";
import ErrorResponse from "../../../response/ErrorResponse";
import { EsitoCallback } from "./types";

// ATTENZIONE: l'ordine è importante
const getEsitoCallbacksForCommons: () => EsitoCallback[] = () => [
  ({ dettaglioEsitoPrecheck }: DatiEsito, payload: IrenPreCheckV2Payload) => {
    if ([
      'PDR inesistente',
      'POD inesistente',
      'Presenza di dati identificativi del cliente finale non corretti per più di 2 caratteri alfanumerici',
      'Presenza di dati identificativi del cliente finale non corretti per meno di 2 caratteri alfanumerici',
      'Cliente inesistente'
    ].includes(dettaglioEsitoPrecheck)) {
      return new FailureResponse(dettaglioEsitoPrecheck);
    }
  },
  ({ dettaglioEsitoPrecheck }: DatiEsito, payload: IrenPreCheckV2Payload) => {
    if (dettaglioEsitoPrecheck.toLowerCase().startsWith('sql exception')) {
      return new ErrorResponse('sql exception');
    }
  },
  ({ dettaglioEsitoPrecheck }: DatiEsito, payload: IrenPreCheckV2Payload) => {
    if (dettaglioEsitoPrecheck.toLowerCase().includes("cannot read property 'touppercase' of")) {
      return new ErrorResponse("Inserire i dati dell'intestatario uscente");
    }
  },
  ({ dettaglioEsitoPrecheck }: DatiEsito, payload: IrenPreCheckV2Payload) => {
    if ([
      'errore tecnico',
      "errore nell'applicazione adibita alla chiamata verso la pdc per il pkg1",
      'nessuna risposta dal sistema'
    ].includes(dettaglioEsitoPrecheck.toLowerCase())) {
      return new ErrorResponse(dettaglioEsitoPrecheck);
    }
  }
];

export default getEsitoCallbacksForCommons;
