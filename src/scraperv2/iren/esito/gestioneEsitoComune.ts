import { DatiEsito } from "../check/recuperaDatiPerEsito";
import { IrenPreCheckV2Payload } from "../payload/IrenPreCheckPayload";
import executorEsitoCallbacksFactory from "./executorEsitoCallbacksFactory";
import getEsitoCallbacksForCommons from "./getEsitoCallbacksForCommons";

export default function gestioneEsitoComune(dati: DatiEsito, payload: IrenPreCheckV2Payload) {
  const cb = executorEsitoCallbacksFactory(getEsitoCallbacksForCommons());
  return cb(dati, payload);
}
