import { DatiEsito } from "../check/recuperaDatiPerEsito";
import { IrenPreCheckV2Payload } from "../payload/IrenPreCheckPayload";
import FailureResponse from "../../../response/FailureResponse";
import SuccessResponse from "../../../response/SuccessResponse";
import ErrorResponse from "../../../response/ErrorResponse";
import { EsitoCallback } from "./types";

// ATTENZIONE: l'ordine è importante
const getEsitoCallbacksForSwitch: () => EsitoCallback[] = () => [
  ({ esito, venditoreUscente }: DatiEsito, { fornitore, fornitura }: IrenPreCheckV2Payload) => {
    if (esito === 'OK' && fornitore === 'iren' && fornitura === 'Elettricità' && venditoreUscente === 'Iren Mercato SpA') {
      return new FailureResponse('pod già attivo con iren, impossibile sottoscrivere il contratto, necessario sottoscriverlo con un altro fornitore');
    }
  },
  ({ esito, esitoCompatibilita }: DatiEsito, { fornitore }: IrenPreCheckV2Payload) => {
    if (esito === 'OK' && esitoCompatibilita === 'KO' && fornitore === 'iren') {
      return new FailureResponse('Su questo punto di fornitura è già in attivazione un contratto Iren');
    }
  },
  ({ esito, venditoreUscente/* , esitoCompatibilita */ }: DatiEsito, { fornitore }: IrenPreCheckV2Payload) => {
    if (esito === 'OK' /* && esitoCompatibilita === 'KO' */ && fornitore !== 'iren') {
      return new SuccessResponse(venditoreUscente);
    }
  },
  ({ esito, esitoCompatibilita, venditoreUscente }: DatiEsito, { fornitore }: IrenPreCheckV2Payload) => {
    if (esito === 'OK' && esitoCompatibilita === 'OK' && fornitore === 'iren') {
      return new SuccessResponse(venditoreUscente);
    }
  },
  ({ esito, dettaglioEsitoPrecheck }: DatiEsito, { fornitore }: IrenPreCheckV2Payload) => {
    if (esito === 'KO' && dettaglioEsitoPrecheck === "PDR già attivo con l'utente richiedente" && fornitore === 'iren') {
      return new FailureResponse('pdr già attivo con iren, impossibile sottoscrivere il contratto');
    }
  },
  ({ esito, dettaglioEsitoPrecheck }: DatiEsito, payload: IrenPreCheckV2Payload) => {
    if (esito === 'KO' && dettaglioEsitoPrecheck === "PDR già attivo con l'utente richiedente") {
      return new ErrorResponse('Non è possibile effettuare il controllo per il pod/pdr inserito. Utilizzare un altro portale per fare la verifica');
    }
  },
  ({ esito, dettaglioEsitoPrecheck }: DatiEsito, payload: IrenPreCheckV2Payload) => {
    if (esito === 'KO' && dettaglioEsitoPrecheck === 'Punto di prelievo non attivo') {
      return new FailureResponse(dettaglioEsitoPrecheck);
    }
  }
];

export default getEsitoCallbacksForSwitch;
