import { DatiEsito } from "../check/recuperaDatiPerEsito";
import { IrenPreCheckV2Payload } from "../payload/IrenPreCheckPayload";
import executorEsitoCallbacksFactory from "./executorEsitoCallbacksFactory";
import getEsitoCallbacksForSubentroAttivazione from "./getEsitoCallbacksForSubentroAttivazione";

export default function generaEsitoSubentroAttivazione(dati: DatiEsito, payload: IrenPreCheckV2Payload) {
  const cb = executorEsitoCallbacksFactory(getEsitoCallbacksForSubentroAttivazione());
  return cb(dati, payload);
}
