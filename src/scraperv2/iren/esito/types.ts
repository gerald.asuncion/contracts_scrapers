import { DatiEsito } from "../check/recuperaDatiPerEsito";
import { IrenPreCheckV2Payload } from "../payload/IrenPreCheckPayload";
import { ScraperResponse } from "../../scraper";

export type EsitoCallback = (dati: DatiEsito, payload: IrenPreCheckV2Payload) => ScraperResponse | undefined;
