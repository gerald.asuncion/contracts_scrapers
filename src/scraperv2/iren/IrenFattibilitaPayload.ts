export type IrenFattibilitaPayload = {
  comune: string;
  cap: string;
  provincia: string;
  regione: string;
  webhook: string;
};
