/**
 * @openapi
 *
 * /scraper/v2/iren/copertura-gas-subentro:
 *  post:
 *    tags:
 *      - v2
 *    description: Effettua il controllo della fattibilità pre i contratti subentro sul portale di Iren.
 *      Questa è la rotta diretta allo scraper, quindi non usa il meccanismo del webhook.
 *    consume:
 *      - application/json
 *    produces:
 *      - application/json
 *    responses:
 *      200:
 *        description: SuccessResponse
 *    parameters:
 *      - in: body
 *        schema:
 *          type: object
 *          required:
 *            - comune
 *            - cap
 *            - provincia
 *            - regione
 *          properties:
 *            comune:
 *              type: string
 *            cap:
 *              type: string
 *            provincia:
 *              type: string
 *            regione:
 *              type: string
 *
 * /scraper/queue/irenSubentroGas:
 *  post:
 *    tags:
 *      - v2
 *      - queue
 *    description:
 *      Effettua il controllo della fattibilità pre i contratti subentro sul portale di Iren.
 *      Il payload è lo stesso della rotta diretta `/scraper/v2/iren/copertura-gas-subentro`,
 *      in più va aggiunto il webhook che lo scraper poi andrà a richiamare per notificare l'esito.
 *    consume:
 *      - application/json
 *    produces:
 *      - application/json
 *    responses:
 *      200:
 *        description: SuccessResponse
 */
