import { Page } from 'puppeteer';
import waitCheckCredentials from '../../browser/page/waitCheckCredentials';
import waitForSelectorAndClick from '../../browser/page/waitForSelectorAndClick';
import waitForVisible from '../../browser/page/waitForVisible';
import ErrorResponse from '../../response/ErrorResponse';
import FailureResponse from '../../response/FailureResponse';
import SuccessResponse from '../../response/SuccessResponse';
import SupermoneyResponse from '../../response/SupermoneyResponse';
import extractErrorMessage from '../../utils/extractErrorMessage';
import { ScraperResponse, WebScraper } from '../scraper';
import { SCRAPER_TIPOLOGIA } from '../ScraperOptions';
import { IrenPreCheckPayload } from './payload/IrenPreCheckPayload';
import isBusiness from './helpers/isBusiness';

const PAGE_AVAILABLE_SELECTOR = '[name="selectClasse"]';

export default class IrenPreCheck extends WebScraper<IrenPreCheckPayload> {
  async login(): Promise<void> {
    const loginUrl = 'https://iren.force.com/s/utilitycustomercheck';
    const { username, password } = await this.getCredenziali();
    this.checkPageClosed();

    const page = await this.p();
    const logger = this.childLogger;
    logger.info('login > inizio');

    // se ha mantenuto la sessione e sono già loggato ritorno subito
    await page.goto(loginUrl, { waitUntil: 'networkidle2' });
    const sel = await page.$(PAGE_AVAILABLE_SELECTOR);
    if (sel !== null) {
      logger.info('login > utente già loggato');
      return;
    }
    await page.waitForSelector('#sfdc_username_container input');

    await page.type('#sfdc_username_container input', username);
    logger.info('login > inserito username');

    await page.type('#sfdc_password_container input', password);
    logger.info('login > inserita password');

    await page.click('.loginButton');

    await Promise.race([
      waitCheckCredentials(page, '.uiOutputRichText', loginUrl, username, logger),
      page.waitForSelector(PAGE_AVAILABLE_SELECTOR)
    ]);
  }

  scrapeWebsite(payload: IrenPreCheckPayload): Promise<ScraperResponse> {
    const { fornitura, codice } = payload;

    const logger = this.childLogger;

    return this.p().then(
      async (p) => {
        try {
          logger.info('login > effettuata');

          const fornituraWithCase = fornitura[0].toUpperCase() + fornitura.substr(1).toLowerCase();
          await p.select('[name=selectClasse]', fornituraWithCase);
          logger.info('inserita fornitura');

          await p.type('[placeholder="Inserisci un punto di fornitura"]', codice.toUpperCase());
          logger.info('inserita codice punto di fornitura');

          const codiceFiscaleDaInserire = (
            isBusiness(payload) ? payload.codiceFiscaleSocieta : payload.codiceFiscale
          ).toUpperCase();
          await p.type('[placeholder="Inserisci Codice Fiscale"]', codiceFiscaleDaInserire);
          logger.info('inserito codice fiscale');

          if (isBusiness(payload)) {
            const { partitaIva, ragioneSociale } = payload;

            await p.type('[placeholder="Inserisci Partita Iva"]', partitaIva);
            logger.info('inserito nome');

            await p.type('[placeholder="Inserisci ragione sociale"]', ragioneSociale);
            logger.info('inserito cognome');
          } else {
            const { nome, cognome, codiceFiscale } = payload;

            await p.type('[placeholder="Inserisci Nome"]', nome);
            logger.info('inserito nome');

            await p.type('[placeholder="Inserisci il Cognome"]', cognome);
            logger.info('inserito cognome');

            if (IrenPreCheck.isStraniero(codiceFiscale)) {
              await p.click('label[for=codiceFiscaleStraniero]');
            }
          }

          await p.select('[name=AzioneComm]', 'Switch in');

          await p.click('[placeholder="Inserisci Nome"]');
          await waitForSelectorAndClick(
            p,
            // eslint-disable-next-line max-len
            '#ServiceCommunityTemplate > div.cCenterPanel > div > div.slds-col--padded.contentRegion.comm-layout-column > div > div > footer > button.slds-button.slds-button_neutral.slds-button.slds-button--neutral.slds-button--brand'
          );

          const failure = await this.waitForLoaderHidden(p);
          if (failure) {
            return failure;
          }

          try {
            await p.waitForSelector('.uiOutputText');
          } catch (ex) {
            let errors;
            try {
              const errorsWrapperSelector = '[role="alert"].error.uiMessage.cUtilityLightning';
              await waitForVisible(p, errorsWrapperSelector);
              // eslint-disable-next-line max-len
              errors = await p.$eval(`${errorsWrapperSelector} > .uiBlock > .bBody`, (el) => (el as HTMLDivElement).innerText?.replace(/\n/g, ' ').trim());
            } catch (ex2) {
              logger.error((ex2 as Error).message);
              throw new Error('Il check ha impiegato troppo tempo');
            }

            if (errors) {
              throw new Error(errors);
            }
          }

          await p.waitForXPath("//label[contains(., 'Esito PreCheck')]");
          const [label] = await p.$x("//label[contains(., 'Esito PreCheck')]");
          const esitoEl = await p.evaluateHandle((el) => el.nextElementSibling, label);

          const esito = await (await esitoEl.getProperty('innerHTML')).jsonValue();
          logger.info('recuperato esito pre check');

          if (esito === 'OK') {
            logger.info('esito poisitivo ritorno SuccessResponse');
            return new SuccessResponse();
          }
          const [labelErrEl] = await p.$x("//label[contains(., 'Dettaglio esito PreCheck')]");

          const esitoErrEl = await p.evaluateHandle((el) => el.nextElementSibling, labelErrEl);

          const esitoErr = await (await esitoErrEl.getProperty('innerHTML')).jsonValue() as string;
          const translatedError = this.translateErrorMessage(esitoErr);

          if (translatedError === null) {
            logger.warn(`errore fallimento pre check non trovato restituisco un ErrorResponse/FailureResponse con messaggio "${esitoErr}"`);
            if (esitoErr === "PDR già attivo con l'utente richiedente") {
              return new ErrorResponse(
                'Non è possibile effettuare il controllo per il pod/pdr inserito. Utilizzare un altro portale per fare la verifica'
              );
            }

            return this.mapError(esitoErr);
          }
          logger.info(`recuperato errore fallimento pre check ritorno un FailureResponse con messaggio "${esitoErr}"`);

          return this.mapError(esitoErr);
        } catch (e) {
          const errMsg = extractErrorMessage(e);
          logger.error(`qualcosa è andato storto: ${errMsg}`);
          if (WebScraper.isScraperResponse(e)) {
            logger.error('errore web scraper restituisco direttamente lui');
            return e;
          }

          const errMsgLowered = errMsg.toLowerCase();
          // ticket 11832
          if (errMsgLowered === 'errore durante validazione cliente il codice pod non è formalmente corretto.') {
            return new FailureResponse(errMsg);
          }

          // ticket 11847 porcata
          if (errMsgLowered.includes("cannot read property 'touppercase' of")) {
            return new FailureResponse("Inserire i dati dell'intestatario uscente");
          }

          logger.error('restituisco un ErrorResponse generico');
          if (e instanceof Error) {
            return new ErrorResponse(errMsg);
          }
          return new ErrorResponse(e as any);
        }
      }
    );
  }

  private async waitForLoaderHidden(page: Page): Promise<FailureResponse | undefined> {
    const loadingSpinnerSelector = 'lightning-spinner';
    const errorMessageSelector = '.slds-theme--error[data-key="error"] > .toastContent';

    // wait for lading to disappear
    try {
      // se scoppia non è partito lo spinner, quindi continua
      await page.waitForSelector(loadingSpinnerSelector, {
        timeout: 10000,
        visible: true
      });
    } catch {
    }

    await page.waitForSelector(loadingSpinnerSelector, {
      hidden: true
    });

    try {
      await page.waitForSelector(errorMessageSelector, {
        timeout: 5000
      });

      let errorMessageElement = await page.$(errorMessageSelector);
      let errorMessage = await page.evaluate(el => el.textContent, errorMessageElement)
      return new FailureResponse(errorMessage);
    } catch {
    }
  }

  private static isStraniero(codiceFiscale: string): boolean {
    return codiceFiscale[11].toUpperCase() === 'Z';
  }

  private translateErrorMessage(esitoErr: string) {
    const translation: Record<string, string> = {
      'pdr inesistente': 'Pdr inesistente, controlla i dati e riprova',
      'pod inesistente': 'Pod inesistente, controlla i dati e riprova',
      'punto di prelievo non attivo': 'Punto di prelievo non attivo. Procedere con un contratto di tipo Subentro',
      // eslint-disable-next-line max-len
      'presenza di dati identificativi del cliente finale non corretti per piú di 2 caratteri alfanumerici': 'I dati del cliente non corrispondono a quelli del contatore. Controlla i dati e riprova',
      'errore nella chiamata a sistemi terzi. ok hub': 'La piattaforma non é raggiungibile. Riprova per favore',
    };

    return translation[esitoErr.trim().toLowerCase()] || null;
  }

  private mapError(error: string): SupermoneyResponse<string> {
    const lowered = error.trim().toLowerCase();
    if (lowered.startsWith('sql exception')) {
      return new ErrorResponse('tool di verifica non disponibile, verifica direttamente col back-office su un altro portale');
    }

    if (lowered === 'errore tecnico') {
      return new ErrorResponse(error);
    }

    if (lowered === "errore nell'applicazione adibita alla chiamata verso la pdc per il pkg1") {
      return new ErrorResponse(error);
    }

    if (lowered === 'nessuna risposta dal sistema') {
      return new ErrorResponse(error);
    }

    return new FailureResponse(error);
  }

  getScraperCodice() {
    return 'iren-precheck';
  }

  getScraperTipologia() {
    return SCRAPER_TIPOLOGIA.FATTIBILITA;
  }
}
