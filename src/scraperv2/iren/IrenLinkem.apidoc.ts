/**
 * @openapi
 *
 * /scraper/v2/irenLinkem/inserimento:
 *  post:
 *    tags:
 *      - v2
 *    description: Inserisce il contratto sul portale di Iren Linkem.
 *      Questa è la rotta diretta allo scraper, quindi non usa il meccanismo del webhook.
 *    consume:
 *      - application/json
 *    produces:
 *      - application/json
 *    responses:
 *      200:
 *        description: SuccessResponse
 *    parameters:
 *      - in: body
 *        schema:
 *          type: object
 *          required:
 *            - idDatiContratto
 *            - codiceOperatore
 *            - nome
 *            - cognome
 *            - codiceFiscale
 *            - dataDiNascita
 *            - luogoDiNascita
 *            - telefonoPrimario
 *            - email
 *            - indirizzo
 *            - civicoIndirizzo
 *            - cap
 *            - citta
 *            - provincia
 *            - indirizzoInstallazione
 *            - civicoInstallazione
 *            - capInstallazione
 *            - cittaInstallazione
 *            - provinciaInstallazione
 *            - metodoPagamento
 *            - iban
 *            - tipoPagamento
 *            - durataRateizzazione
 *            - orarioContatto
 *          properties:
 *            idDatiContratto:
 *              type: string
 *            codiceOperatore:
 *              type: string
 *            nome:
 *              type: string
 *            cognome:
 *              type: string
 *            codiceFiscale:
 *              type: string
 *            dataDiNascita:
 *              type: string
 *            luogoDiNascita:
 *              type: string
 *            telefonoPrimario:
 *              type: string
 *            email:
 *              type: string
 *            indirizzo:
 *              type: string
 *            civicoIndirizzo:
 *              type: string
 *            cap:
 *              type: string
 *            citta:
 *              type: string
 *            provincia:
 *              type: string
 *            indirizzoInstallazione:
 *              type: string
 *            civicoInstallazione:
 *              type: string
 *            capInstallazione:
 *              type: string
 *            cittaInstallazione:
 *              type: string
 *            provinciaInstallazione:
 *              type: string
 *            metodoPagamento:
 *              type: string
 *              enum: [Domiciliazione Bancaria, carta di credito]
 *            iban:
 *              type: string
 *              nullable: true
 *            tipoPagamento:
 *              type: string
 *              enum: [Unica Soluzione, Pagamento Rateizzato]
 *            durataRateizzazione:
 *              type: string
 *              nullable: true
 *            orarioContatto:
 *              type: string
 *              enum: [9-11, 11-13, 14-16, 16-18]
 *
 * /scraper/queue/irenLinkem:
 *  post:
 *    tags:
 *      - v2
 *      - queue
 *    description:
 *      Inserisce il contratto sul portale di Iren Linkem.
 *      Il payload è lo stesso della rotta diretta `/scraper/v2/irenLinkem/inserimento`,
 *      in più va aggiunto il webhook che lo scraper poi andrà a richiamare per notificare l'esito.
 *    consume:
 *      - application/json
 *    produces:
 *      - application/json
 *    responses:
 *      200:
 *        description: SuccessResponse
 */
