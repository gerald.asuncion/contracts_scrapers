import { IrenPreCheckPayloadResidential, IrenPreCheckPayloadBusiness } from '../payload/IrenPreCheckPayload';

export default function isBusiness(payload: IrenPreCheckPayloadResidential | IrenPreCheckPayloadBusiness): payload is IrenPreCheckPayloadBusiness {
  // accounts for undefined, null and ''...
  return !!(payload as IrenPreCheckPayloadBusiness).codiceFiscaleSocieta;
}
