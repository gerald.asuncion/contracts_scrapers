import ContrattoPayload from '../../../contratti/ContrattoPayload';

type IrenLinkemPayload = Required<ContrattoPayload> & {
  codiceOperatore: string,
  nome: string,
  cognome: string,
  codiceFiscale: string,
  dataDiNascita: string,
  luogoDiNascita: string,
  telefonoPrimario: string,
  email: string,
  indirizzo: string,
  civicoIndirizzo: string,
  cap: string,
  citta: string,
  provincia: string,

  indirizzoInstallazione: string,
  civicoInstallazione: string,
  capInstallazione: string,
  cittaInstallazione: string,
  provinciaInstallazione: string,

  metodoPagamento: 'Domiciliazione Bancaria' | 'carta di credito',
  iban: string | null,
  tipoPagamento: 'Unica Soluzione' | 'Pagamento Rateizzato',
  durataRateizzazione: string | null;
  orarioContatto: '9-11' | '11-13' | '14-16' | '16-18';
  profiloServizio: "30 mega" | "100 mega" | null; // TODO: REMOVE NULL
};

export default IrenLinkemPayload;
