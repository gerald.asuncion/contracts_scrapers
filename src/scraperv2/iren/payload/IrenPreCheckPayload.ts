export type IrenPreCheckPayloadBase = {
  fornitura: 'Gas' | 'Elettricità';
  codice: string;
};

export type IrenPreCheckPayloadResidential = {
  nome: string;
  cognome: string;
  codiceFiscale: string;
};

export type IrenPreCheckPayloadBusiness = {
  codiceFiscaleSocieta: string;
  partitaIva: string;
  ragioneSociale: string;
};

export type FornitorePayload = {
  fornitore: string;
};

export type TipoContrattoPayload = {
  tipoContratto: string;
};

export type IrenPreCheckPayload = IrenPreCheckPayloadBase & (IrenPreCheckPayloadResidential | IrenPreCheckPayloadBusiness);

export type IrenPreCheckV2Payload = IrenPreCheckPayload & FornitorePayload & TipoContrattoPayload & {webhook: string};
