import IrenRemiRepo from '../../repo/IrenRemiRepo';
import { IrenRemiResponse, IrenRemiRow } from '../../repo/types';
import FailureResponse from '../../response/FailureResponse';
import SuccessResponse from '../../response/SuccessResponse';
import { QueueTask } from '../../task/QueueTask';
import DbScraper, { DbScraperOptions } from '../DbScraper';
import { ScraperResponse } from '../scraper';
import { SCRAPER_TIPOLOGIA } from '../ScraperOptions';
import { IrenFattibilitaPayload } from './IrenFattibilitaPayload';

type IrenSubentroGasOptions = DbScraperOptions & {
  irenRemiRepo: IrenRemiRepo;
};

@QueueTask({scraperName: 'irenSubentroGas'})
export default class IrenSubentroGas extends DbScraper {
  private repo: IrenRemiRepo;

  constructor(options: IrenSubentroGasOptions) {
    super(options);
    this.repo = options.irenRemiRepo;
  }

  async scrape(payload: IrenFattibilitaPayload): Promise<ScraperResponse> {
    const {
      comune,
      cap,
      regione,
      provincia
    } = payload;
    const { logger, repo } = this;
    let response: IrenRemiResponse;
    let data!: IrenRemiRow[];

    try {
      logger.info(`controllo ${comune} (${cap}) in ${regione} (${provincia})`);
      response = await repo.isValidByComuneV2(payload);

      data = await response.data();
    } catch (err) {
      // isValidByComune lancia una eccezione vuota (reject()). in tal caso diamo comune non trovato

      if (err) {
        logger.error(err);
        throw Error("Problemi di connessione a db, contattare l'assistenza!");
      }
    }

    if (data && data.some(({ SUBENTRO }) => SUBENTRO === 'SI')) {
      logger.info(`subentro gas fattibile per il comune ${comune}`);
      return new SuccessResponse();
    }

    logger.info(`subentro gas NON fattibile per il comune ${comune}`);
    return new FailureResponse(`Il comune ${comune} non è coperto dal servizio Iren Subentro Gas`);
  }

  getScraperCodice() {
    return 'iren-subentrogas';
  }

  getScraperTipologia() {
    return SCRAPER_TIPOLOGIA.FATTIBILITA;
  }
}
