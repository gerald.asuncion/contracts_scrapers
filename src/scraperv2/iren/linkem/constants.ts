export const METODO_PAGAMENTO: Record<string, string> = {
  'domiciliazione bancaria': 'Bank Transfer',
  'carta di credito': 'Credit Card'
};

export const TIPO_PAGAMENTO: Record<string, string> = {
  'unica soluzione': 'One-Off',
  'pagamento rateizzato': 'Split Payment'
};
