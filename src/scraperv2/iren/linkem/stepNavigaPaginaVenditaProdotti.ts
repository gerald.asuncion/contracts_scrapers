import { Page } from 'puppeteer';

export default async function stepNavigaPaginaVenditaProdotti(page: Page): Promise<void> {
  // invece di fare quello scritto nelle specifiche navigo direttamente alla pagina
  await page.goto('https://iren-console.ennova.it/#/vendita');
}
