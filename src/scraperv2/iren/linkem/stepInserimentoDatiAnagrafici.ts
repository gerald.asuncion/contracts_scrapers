import { Page } from 'puppeteer';
import ifNotReadonly from '../../../browser/page/ifNotReadonly';
import waitForSelectorAndSelect from '../../../browser/page/waitForSelectorAndSelect';
import waitForSelectorAndType from '../../../browser/page/waitForSelectorAndType';
import waitForSelectorAndTypeByMatch from '../../../browser/page/waitForSelectorAndTypeByMatch';
import waitForXPathAndClick from '../../../browser/page/waitForXPathAndClick';
import waitForXPathVisible from '../../../browser/page/waitForXPathVisible';
import delay from '../../../utils/delay';
import tryOrThrow from '../../../utils/tryOrThrow';
import IrenLinkemPayload from '../payload/IrenLinkemPayload';

export default async function stepInserimentoDatiAnagrafici(
  page: Page,
  {
    nome,
    cognome,
    codiceFiscale,
    dataDiNascita,
    luogoDiNascita,
    telefonoPrimario,
    email,
    indirizzo,
    civicoIndirizzo,
    cap,
    citta,
    provincia,
    orarioContatto,
    profiloServizio
  }: IrenLinkemPayload
): Promise<void> {
  await tryOrThrow(
    () => waitForSelectorAndSelect(page, '#OrTipologiaVendita', ['servizio']),
    'non sono riuscito a selezionare la tipologia di vendita'
  );

  // Iren Casa Online Special Standalone
  await tryOrThrow(
    () => waitForSelectorAndSelect(page, '#OrIdProd', ['a0p1n00000JqDDxAAN']),
    'non sono riuscito a selezionare il prodotto `Iren casa online Special standalone (23,90 euro + 48 euro attivazione)`'
  );

  try {
    const modalBtnSelector = '//strong[contains(., "ATTENZIONE!")]//ancestor::div[contains(concat(" ", normalize-space(@class), " "), " modal-content ")]//button[contains(., "OK")]';
    await waitForXPathVisible(page, modalBtnSelector, { timeout: 3000 });
    if (await waitForXPathAndClick(page, modalBtnSelector), {
      timeout: 3000,
      visible: true
    }) {
      await delay(500);
    }
  } catch (error) {
    // modale non apparsa
    // nulla da fare
  }

  await tryOrThrow(
    () => waitForSelectorAndType(page, '#OrName', nome),
    'non sono riuscito ad inserire il nome'
  );

  await tryOrThrow(
    () => waitForSelectorAndType(page, '#OrCognom', cognome),
    'non sono riuscito ad inserire il cognome'
  );

  await tryOrThrow(
    () => waitForSelectorAndType(page, '#OrCodFisc', codiceFiscale),
    'non sono riuscito ad inserire il codice fiscale'
  );

  // TODO: implementare logica attributo readonly
  const dataDiNascitaSelector = '#OrDatNascit';
  await tryOrThrow(
    () => ifNotReadonly(page, dataDiNascitaSelector, () => waitForSelectorAndType(page, dataDiNascitaSelector, dataDiNascita)),
    'non sono riuscito ad inserire la data di nascita'
  );

  await tryOrThrow(
    () => waitForSelectorAndType(page, '#OrLuogoNascita', luogoDiNascita),
    'non sono riuscito ad inserire il luogo di nascita'
  );

  await tryOrThrow(
    () => waitForSelectorAndType(page, '#OrTelPrimario', telefonoPrimario),
    'non sono riuscito ad inserire il telefono primario'
  );

  await tryOrThrow(
    () => waitForSelectorAndType(page, '#OrEmail', email),
    'non sono riuscito ad inserire l\'email'
  );

  await tryOrThrow(
    () => waitForSelectorAndType(page, '#OrIndirizzo', indirizzo),
    'non sono riuscito a d inserire l\'indirizzo'
  );

  await tryOrThrow(
    () => waitForSelectorAndType(page, '#OrCivico', civicoIndirizzo),
    'non sono riuscito ad inserire il civico'
  );

  await tryOrThrow(
    () => waitForSelectorAndType(page, '#OrCAP', cap),
    'non sono riuscito ad inserire il cap'
  );

  // const indirizzoDaInserire = [indirizzo, civicoIndirizzo, citta].join(', ').toLowerCase();
  // await tryOrThrow(
  //   () => waitForSelectorAndType(page, 'input[name="address"]', indirizzoDaInserire),
  //   'non sono riuscito a d inserire l\'indirizzo'
  // );

  await tryOrThrow(
    () => waitForSelectorAndSelect(page, '#OrProvFatturazione', [provincia]),
    'non sono riuscito ad inserire la provincia'
  );

  await tryOrThrow(
    () => waitForSelectorAndTypeByMatch(page, '#OrComunCitt', citta),
    'non sono riuscito ad inserire il comune/città'
  );

  // let err;
  // try {
  //   await waitForVisible(page, '.modal .modal-body', { timeout: 6000 });
  //   const modalText = await getElementText(page, '.modal .modal-body', 'textContent');
  //   err = new TryOrThrowError(modalText as string);
  // } catch (ex) {
  //   // modale di errore non apparsa, nulla da fare
  // }

  // if (err) {
  //   throw err;
  // }

  await tryOrThrow(
    () => waitForSelectorAndSelect(page, '#OrOrarioContatto', [orarioContatto]),
    'non sono riuscito a selezionare l\'orario di contatto'
  );

  await tryOrThrow(
    () => waitForSelectorAndSelect(page, '#OrProfiloServizio', [profiloServizio || "30 mega"]),
    'non sono riuscito a selezionare l\'orario di contatto'
  );
}
