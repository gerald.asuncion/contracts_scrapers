import { Page } from 'puppeteer';
import waitForSelectorAndClick from '../../../browser/page/waitForSelectorAndClick';
import tryOrThrow from '../../../utils/tryOrThrow';

export default async function stepConferma(page: Page): Promise<void> {
  await tryOrThrow(
    () => page.waitForSelector('form[name="venditaForm"]'),
    'la pagina di riepilogo non è apparsa'
  );

  await tryOrThrow(
    () => waitForSelectorAndClick(page, 'form[name="riepilogoForm"] input[name="comprensioneTermini"]'),
    'non sono riuscito ad abilitare `Ho controllato i dati e confermo che sono corretti`'
  );

  await tryOrThrow(
    // eslint-disable-next-line max-len
    () => waitForSelectorAndClick(page, '#page-content-wrapper > div.ng-scope > form > div.row.ng-scope > riepilogo > div > button.btn.btn-success'),
    'non sono riuscito a cliccare sul pulsante `Conferma` nella pagina di riepilogo'
  );

  await tryOrThrow(
    () => page.waitForSelector('body > div.modal.fade.ng-isolate-scope.in > div'),
    'la modale di conferma non è apparsa'
  );

  await tryOrThrow(
    () => waitForSelectorAndClick(page, '#modal-body > button.btn.btn-primary'),
    'non sono riuscito a cliccare sul pulsante `Conferma` nella modale'
  );

  await tryOrThrow(
    () => page.waitForSelector('body > div.modal.fade.ng-isolate-scope.in > div'),
    'la modale di conferma invio email non è apparsa'
  );

  await tryOrThrow(
    () => waitForSelectorAndClick(page, 'body > div.modal.fade.ng-isolate-scope.in > div > div > div.modal-footer.ng-scope > button'),
    'non sono riuscito a cliccare sul pulsante `Ok` nella modale di conferma invio email'
  );
}
