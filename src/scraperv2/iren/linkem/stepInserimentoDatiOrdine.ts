import { Page } from 'puppeteer';
import waitForSelectorAndSelect from '../../../browser/page/waitForSelectorAndSelect';
import waitForSelectorAndType from '../../../browser/page/waitForSelectorAndType';
import waitForSelectorAndTypeByMatch from '../../../browser/page/waitForSelectorAndTypeByMatch';
import tryOrThrow from '../../../utils/tryOrThrow';
import IrenLinkemPayload from '../payload/IrenLinkemPayload';

export default async function stepInserimentoDatiOrdine(
  page: Page,
  {
    indirizzoInstallazione,
    civicoInstallazione,
    capInstallazione,
    cittaInstallazione,
    provinciaInstallazione
  }: IrenLinkemPayload
): Promise<void> {
  await tryOrThrow(
    () => waitForSelectorAndType(page, '#OrIndPostaleInstallazione', indirizzoInstallazione),
    'non sono riuscito ad inserire l\'indirizzo di installazione'
  );

  await tryOrThrow(
    () => waitForSelectorAndType(page, '#OrCivInstallazione', civicoInstallazione),
    'non sono riuscito ad inserie il civico di installazione'
  );

  await tryOrThrow(
    () => waitForSelectorAndType(page, '#OrCAPInstallazione', capInstallazione),
    'non sono riuscito ad inserire il cap dell\'installazione'
  );

  await tryOrThrow(
    () => waitForSelectorAndSelect(page, '#OrProvInstallazione', [provinciaInstallazione]),
    'non sono riuscito ad inserire la provincia di installazione'
  );

  await tryOrThrow(
    () => waitForSelectorAndTypeByMatch(page, '#OrCittInstallazione', cittaInstallazione),
    'non sono riuscito ad inserire la città di installazione'
  );
}
