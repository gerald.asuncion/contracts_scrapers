import { Page } from 'puppeteer';
import waitForSelectorAndSelect from '../../../browser/page/waitForSelectorAndSelect';
import waitForSelectorAndType from '../../../browser/page/waitForSelectorAndType';
import delay from '../../../utils/delay';
import tryOrThrow from '../../../utils/tryOrThrow';

export default async function stepInserimentoDatiAgenzia(page: Page, codiceOperatore: string): Promise<void> {
  await delay(1500);
  await tryOrThrow(
    () => waitForSelectorAndSelect(page, '#OrGruppoAgente', ['BAR']),
    'non sono riuscito a selezionare il gruppo dell\'agenzia'
  );

  await delay(1000);
  await tryOrThrow(
    () => waitForSelectorAndType(page, '#codiceOperatore', codiceOperatore),
    'non sono riuscito ad inserire il codice operatore'
  );

  // scommentare se viene passato nel payload o ci danno un codice voucher (fisso) da inserire
  // await tryOrThrow(
  //   () => waitForSelectorAndType(page, '#OrCodVoucher', codiceVoucher),
  //   'non sono riuscito ad inserire il codice voucher'
  // );
}
