import { Page } from 'puppeteer';
import IrenLinkemPayload from '../payload/IrenLinkemPayload';
import waitForSelectorAndSelect from '../../../browser/page/waitForSelectorAndSelect';
import waitForSelectorAndType from '../../../browser/page/waitForSelectorAndType';
import waitForSelectorAndClick from '../../../browser/page/waitForSelectorAndClick';
import waitForVisible from '../../../browser/page/waitForVisible';
import { METODO_PAGAMENTO, TIPO_PAGAMENTO } from './constants';
import isDisabled from '../../../browser/page/isDisabled';
import tryOrThrow, { TryOrThrowError } from '../../../utils/tryOrThrow';
import waitForXPathAndRun from "../../../browser/page/waitForXPathAndRun";

export default async function stepInserimentoDatiPagamento(
  page: Page,
  {
    metodoPagamento,
    iban,
    tipoPagamento,
    durataRateizzazione
  }: IrenLinkemPayload
): Promise<void> {
  await tryOrThrow(
    () => waitForSelectorAndSelect(page, '#OrMetodoPag', [METODO_PAGAMENTO[metodoPagamento.toLowerCase()]]),
    'non sono riuscito a selezionare il metodo di pagamento'
  );

  if (metodoPagamento.toLowerCase() === 'domiciliazione bancaria') {
    await tryOrThrow(
      () => waitForSelectorAndType(page, '#OrIBAN', iban as string),
      'non sono riuscito ad inserire l\'iban'
    );

    let errorMessage;
    try {
      errorMessage = await waitForXPathAndRun(
        page,
        "//h3[contains(text(), 'Errore')]/parent::div/following-sibling::div[contains(@class, 'modal-body')]",
        async (p, elHandle) => elHandle?.evaluate((el) => el.textContent),
        {timeout: 5000}
      );
    } catch (e) {}
    if (errorMessage) {
      throw new TryOrThrowError(errorMessage);
    }
  }

  await tryOrThrow(
    () => waitForSelectorAndSelect(page, '#OrTipoPagamento', [TIPO_PAGAMENTO[tipoPagamento.toLowerCase()]]),
    'non sono riuscito a selezionare il tipo di pagamento'
  );

  if (tipoPagamento.toLowerCase() === 'pagamento rateizzato') {
    await tryOrThrow(
      () => waitForSelectorAndSelect(page, '#OrDurataRateizzo', [durataRateizzazione as string]),
      'non sono riuscito a selezione la durata di raetizzazione'
    );
  }

  // devo fare dopo l'inserimento del tipo di pagamento altrimenti non si abilita il pulsante `Completa l'ordine`
  if (metodoPagamento.toLowerCase() === 'domiciliazione bancaria') {
    await tryOrThrow(
      // eslint-disable-next-line max-len
      () => waitForSelectorAndClick(page, '#page-content-wrapper > div.ng-scope > form > div > div > div > div.row.ng-scope > div.col-lg-12.col-md-12.col-sm-12.col-xs-12.ng-scope > button'),
      'noo sono riuscito ad aprire la modale dei consensi'
    );

    await tryOrThrow(
      () => waitForVisible(page, '#informativa'),
      'non ho trovato la modale con l\'informativa'
    );

    await tryOrThrow(
      () => waitForSelectorAndClick(page, '#informativa > button.btn.btn-success'),
      'non sono riuscito ad accettare le condizioni contrattuali'
    );

    await tryOrThrow(
      () => waitForSelectorAndClick(page, '#comprensioneTerminiUno'),
      'non sono riuscito a cliccare su `Ho preso visione` dell\'informativa'
    );
  }

  const completaOrdineBtnSelector = '#page-content-wrapper > div.ng-scope > form > div > div > div > div:nth-child(22) > div > button';
  if (await isDisabled(page, completaOrdineBtnSelector)) {
    throw new TryOrThrowError('Il pulsante `Completa l\'ordine` è disabilitato');
  }

  await tryOrThrow(
    () => waitForSelectorAndClick(page, completaOrdineBtnSelector),
    'non sono riuscito a cliccare sul pulsante `Completa l\'ordine`'
  );
}
