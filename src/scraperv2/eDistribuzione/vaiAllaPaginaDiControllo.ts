import { Page } from "puppeteer";
import { EdistribuzioneCheckPodPayload } from "./types";
import { Logger } from "../../logger";
import delay from "../../utils/delay";
import waitForVisible from "../../browser/page/waitForVisible";
import tryOrThrow from "../../utils/tryOrThrow";
import waitForSelectorAndClick from "../../browser/page/waitForSelectorAndClick";
import waitForSelectorAndType from "../../browser/page/waitForSelectorAndType";

const codiceRichiestaVenditore = 'WEmmggaaSMhhmm';

export default async function vaiAllaPaginaDiControllo(page: Page, logger: Logger, { pod }: EdistribuzioneCheckPodPayload) {
  logger.info('attendo il caricamento del primo componente utile');
  // attendo il caricamento del primo componente utile
  await tryOrThrow(
    () => waitForVisible(page, '#header-overlay > div.cNavBarCon > div > div > div > community_navigation-global-navigation-list '
    + '> div > nav > ul > li:nth-child(2) > community_navigation-global-navigation-item > button'),
    'la pagina non si è caricata in tempo:'
  );

  logger.info('clicco sulla prima voce di menu');
  // clicco sulla prima voce di menu
  await waitForSelectorAndClick(page, '#header-overlay > div.cNavBarCon > div > div > div > community_navigation-global-navigation-list '
    + '> div > nav > ul > li:nth-child(2) > community_navigation-global-navigation-item > button');
  await delay(500);

  logger.info('clicco sulla seconda voce di menu');
  // clicco sulla seconda voce di menu
  await waitForSelectorAndClick(page, '#header-overlay > div.cNavBarCon > div > div > div > community_navigation-global-navigation-list '
    + '> div > nav > ul > li:nth-child(2) > community_navigation-global-navigation-item > ul > li:nth-child(12) '
    + '> community_navigation-global-navigation-item > a');
  await delay(2000);

  logger.info('clicco sul radio input');
  // clicco sul radio input
  await waitForSelectorAndClick(page, '#extensionPopup > div.slds-modal.slds-fade-in-open > div '
    + '> div.slds-modal__content.slds-p-around--medium > lightning-input:nth-child(1)');

  logger.info('clicco sul button `Continua`');
  // clicco sul button `Continua`
  await waitForSelectorAndClick(page, '#ContinueBtnID');
  await delay(2000);

  logger.info("inserisco la stringa `codiceRichiestaVenditore` all'interno del campo `Codice Richiesta Venditore`");
  // inserisco la stringa `codiceRichiestaVenditore` all'interno del campo `Codice Richiesta Venditore`
  await waitForSelectorAndType(page, '.slds-form-element__group .slds-form-element .uiInput input', codiceRichiestaVenditore);
  await delay(500);

  await waitForSelectorAndType(page, '.sldsWidth1-3 .uiInput input', pod);
  await delay(500);

  logger.info('clicco sul button `Continua`');
  // clicco sul button `Continua`
  await waitForSelectorAndClick(page, '#requestModal > div.slds-modal__footer > button.slds-button.CustomButton');

  logger.info('controllo che il pod passato sia valido per il portale');
  let podErrMsg;
  try {
    const $el = await page.waitForSelector('#\\34 38\\:0 > li', { visible: true, timeout: 1000 });
    podErrMsg = await $el?.evaluate((el) => (el as HTMLLIElement).innerText);
    podErrMsg = podErrMsg?.trim();
  } catch (ex) {
    // pod corretto non devo fare nulla
  }
  if (podErrMsg) {
    logger.error(podErrMsg);
    throw new Error(podErrMsg);
  } else {
    logger.info('inserito pod valido');
  }

  logger.info("attendo e clicco sul button `Continua` dell'ultima modale");
  // attendo e clicco sul button `Continua` dell'ultima modale
  // await waitForVisible(page, '#requestModal > div.slds-modal__content.slds-p-around--medium > fieldset > div.slds-modal__footer > button');
  await waitForSelectorAndClick(page, '#requestModal > div.slds-modal__content.slds-p-around--medium > fieldset > div.slds-modal__footer > button');
}
