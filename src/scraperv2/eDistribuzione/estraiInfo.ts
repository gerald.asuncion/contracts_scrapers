import { Page } from "puppeteer";
import { Logger } from "../../logger";
import estraiStato from "./estraiStato";
import delay from "../../utils/delay";
import FailureResponse from "../../response/FailureResponse";
import estraiStatoDelPod from "./estraiStatoDelPod";
import estraiIdRichiesta from "./estraiIdRichiesta";

export type Info = {
  idRichiesta: string;
  stato?: string;
  statoPod?: string;
};

export default async function estraiInfo(page: Page, logger: Logger): Promise<[Info, null | FailureResponse]> {
  logger.info('leggo lo stato della richiesta');
  // leggo lo stato della richiesta
  const stato = await estraiStato(page);
  await delay(500);
  logger.info(`Lo stato della richiesta è ${stato}`);

  logger.info("estraggo l'id della richiesta");
  const idRichiesta = await estraiIdRichiesta(page);

  if (stato === 'Respinta_') {
    return [{ idRichiesta }, new FailureResponse('KO - POD non contendibile')];
  }

  logger.info('leggo lo stato del POD della richiesta');
  // leggo lo stato del POD della richiesta
  const statoPod = await estraiStatoDelPod(page);
  await delay(500);

  return [{ idRichiesta, stato, statoPod }, null];
}
