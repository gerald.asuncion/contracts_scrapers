/**
 * @openapi
 *
 * /scraper/v2/eDistribuzione/pod/checker:
 *  post:
 *    tags:
 *      - v2
 *    description: Effettua il controllo del pod sul sito di E-distribuzione.
 *    consume:
 *      - application/json
 *    produces:
 *      - application/json
 *    responses:
 *      200:
 *        description: SuccessResponse
 *    parameters:
 *      - in: body
 *        schema:
 *          type: object
 *          required:
 *            - tipoContratto
 *            - pod
 *          properties:
 *            tipoContratto:
 *              type: string
 *            pod:
 *              type: string
 *
 * /scraper/queue/eDistribuzionePodChecker:
 *  post:
 *    tags:
 *      - v2
 *      - queue
 *    description:
 *      Effettua il controllo del pod sul sito di E-distribuzione.
 *      Il payload è lo stesso della rotta diretta `/scraper/v2/eDistribuzione/pod/checker`,
 *      in più va aggiunto il webhook che lo scraper poi andrà a richiamare per notificare l'esito.
 *    consume:
 *      - application/json
 *    produces:
 *      - application/json
 *    responses:
 *      200:
 *        description: SuccessResponse
 */
