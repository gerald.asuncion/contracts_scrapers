export type EdistribuzioneCheckPodPayload = {
  pod: string;
};

export type EDistribuzionePodCheckerPayload = EdistribuzioneCheckPodPayload & { tipoContratto: string; };
