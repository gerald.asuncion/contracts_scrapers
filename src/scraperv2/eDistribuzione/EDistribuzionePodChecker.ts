import { ScraperResponse, WebScraper } from '../scraper';
import ErrorResponse from '../../response/ErrorResponse';
import extractErrorMessage from '../../utils/extractErrorMessage';
import WrongCredentialsError from '../../errors/WrongCredentialsError';
import { SCRAPER_TIPOLOGIA } from '../ScraperOptions';
import { EDistribuzionePodCheckerPayload } from './types';
import vaiAllaPaginaDiControllo from './vaiAllaPaginaDiControllo';
import estraiInfoControllaStati from './estraiInfoControllaStati';
import { QueueTask } from '../../task/QueueTask';

@QueueTask()
export default class EDistribuzionePodChecker extends WebScraper<EDistribuzionePodCheckerPayload> {
  /**
   * @override
   */
  getLoginTimeout(): number { return 90000; }

  async login(): Promise<void> {
    const loginUrl = 'https://4pt.e-distribuzione.it/';
    const { username, password } = await this.getCredenziali();

    this.checkPageClosed();

    const logger = this.childLogger;

    const page = await this.p();
    await page.goto(loginUrl, { waitUntil: 'networkidle0' });

    if (await page.$('#header-overlay > div.cHeaderLine.slds-grid > div.cSearchPublisher') !== null) return;

    logger.info("attendo la input per l'inserimento della username");
    // attendo la input per l'inserimento della username
    await page.waitForSelector('input[type=text]');
    logger.info("attendo la input per l'inserimento della password");
    // attendo la input per l'inserimento della passsword
    await page.waitForSelector('input[type=password]');

    logger.info('inserisco la username');
    // inserisco la username
    await page.type('input[type=text]', username);
    logger.info('inserisco la psw');
    // inserisco la password
    await page.type('input[type=password]', password);

    await WebScraper.delay(500);

    try {
      await Promise.all([
        page.click('#centerPanel > div > div.slds-col--padded.contentRegion.comm-layout-column '
          + '> div > div:nth-child(2) > div > div:nth-child(4) > button'),
        page.waitForNavigation({ waitUntil: 'networkidle2' })
      ]);
    } catch (navEx) {
      const navErrMsg = extractErrorMessage(navEx);
      logger.warn(navErrMsg);
      logger.info('controllo se le credenziali sono ancora valide');
      try {
        await page.waitForSelector('#centerPanel > div > div.slds-col--padded.contentRegion.comm-layout-column '
          + '> div > div:nth-child(2) > div > span > div > div');
        logger.error('credenziali scadute');
        throw new WrongCredentialsError(loginUrl, username);
      } catch (ex) {
        if (ex instanceof WrongCredentialsError) {
          throw ex;
        }
        // nulla da fare le credenziali sono corrette
        logger.info('credenziali valide');
        throw new Error(`login > ${navErrMsg}`);
      }
    }

    await page.waitForSelector('.apexp');

    await page.$eval('#thePage\\:j_id2\\:i\\:f\\:pb\\:d\\:Accetta\\.input', (el) => { (el as HTMLInputElement).checked = true; });
    await page.evaluate(() => {
      const el = document.querySelector<HTMLInputElement>('.dataCol input[type=hidden]');
      if (el) {
        el.value = 'true';
      }
    });

    logger.info('clicco sul button della modale');
    await page.click('.FlowFinishBtn');
  }

  async scrapeWebsite(payload: EDistribuzionePodCheckerPayload): Promise<ScraperResponse> {
    const logger = this.childLogger;

    const page = await this.p();

    try {
      logger.info('vado alla pagina per controllare lo stato');
      await vaiAllaPaginaDiControllo(page, logger, payload);

      const result = await estraiInfoControllaStati(page, logger, payload, Date.now());

      return result;
      } catch (e) {
      const errMsg = extractErrorMessage(e);
      logger.error(errMsg);
      return new ErrorResponse(errMsg);
    }
  }

  getScraperCodice() {
    return 'edistribuzione';
  }

  getScraperTipologia() {
    return SCRAPER_TIPOLOGIA.FATTIBILITA;
  }
}
