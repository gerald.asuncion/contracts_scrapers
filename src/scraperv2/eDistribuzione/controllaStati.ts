import { Page } from "puppeteer";
import { Logger } from "../../logger";
import { EDistribuzionePodCheckerPayload } from "./types";
import { Info } from "./estraiInfo";
import FailureResponse from "../../response/FailureResponse";
import ErrorResponse from "../../response/ErrorResponse";
import SuccessResponse from "../../response/SuccessResponse";
import estraiSuccessInfo from "./estraiSuccessInfo";
import { ScraperResponse } from "../scraper";
// import CasoNonGestitoResponse from '../../response/CasoNonGestitoResponse';

export default async function controllaStati(
  page: Page,
  logger: Logger,
  { tipoContratto }: EDistribuzionePodCheckerPayload,
  { stato, statoPod }: Info
): Promise<ScraperResponse | null> {
  logger.info(`Stato = ${stato}`);
  logger.info(`Stato POD = ${statoPod}`);

  if (stato === 'Inserita_' && (statoPod === '' || statoPod === '')) {
    return null;
  }

  if (tipoContratto === 'subentro') {
    if (stato === 'Evasa_' && statoPod === 'Predisposto') {
      return new FailureResponse('KO - Necessario procedere con una Prima Attivazione');
    }

    if (stato === 'Inserita_' && (statoPod === 'Presa in carico' || statoPod === 'Inserita')) {
      // eslint-disable-next-line max-len
      return new ErrorResponse('il portale non ha restuito esito, provare a verificare il punto di fornitura col back-office direttamente sul portale del distributore');
    }

    if (stato === 'Evasa_' && statoPod === 'Cessato') {
      return new SuccessResponse(await estraiSuccessInfo(page));
    }
  }

  if (tipoContratto === 'attivazione') {
    if (stato === 'Inserita_' && (statoPod === 'Presa in carico' || statoPod === 'Inserita')) {
      // eslint-disable-next-line max-len
      return new ErrorResponse('il portale non ha restuito esito, provare a verificare il punto di fornitura col back-office direttamente sul portale del distributore');
    }

    if (stato === 'Evasa_' && statoPod === 'Cessato') {
      return new FailureResponse('KO - Necessario procedere con un Subentro');
    }

    if (stato === 'Evasa_' && statoPod === 'Predisposto') {
      return new SuccessResponse(await estraiSuccessInfo(page));
    }
  }

  logger.error('caso non gestito');
  return new ErrorResponse('Esito non mappato, segnalarlo al back-office di supermoney e Verificare il punto di fornitura col back-office prima di proseguire');
}
