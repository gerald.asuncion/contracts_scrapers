import { Page } from "puppeteer";
import waitForVisible from "../../browser/page/waitForVisible";

const selettoreStatoPod = '.test-id__section:nth-child(4) .test-id__section-content .slds-form .slds-grid:nth-child(2) .slds-col .slds-form-element .slds-form-element__control .test-id__field-value span';

export default async function estraiStatoDelPod(page: Page) {
  await waitForVisible(page, selettoreStatoPod);

  const statoPod = await page.evaluate((_selettoreStatoPod) => document.querySelector(_selettoreStatoPod).innerHTML, selettoreStatoPod);
  return statoPod;
}
