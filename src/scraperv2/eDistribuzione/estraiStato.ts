import { Page } from "puppeteer";
import waitForVisible from "../../browser/page/waitForVisible";

const selettoreStato = '.slds-form .slds-grid:nth-child(3) .slds-col:nth-child(1) .slds-form-element .slds-form-element__control .test-id__field-value span';

export default async function estraiStato(page: Page) {
  await waitForVisible(page, selettoreStato);

  const stato = await page.evaluate((_selettoreStato) => document.querySelector(_selettoreStato).innerHTML, selettoreStato);
  return stato;
}
