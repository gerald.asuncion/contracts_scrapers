import { Page } from "puppeteer";

export default async function estraiIdRichiesta(page: Page): Promise<string> {
  const [el] = await page.$x('//span[contains(., "Id Richiesta")]/parent::div/following-sibling::div');

  return el && (await el.getProperty('textContent')).jsonValue();
}
