import { Page } from 'puppeteer';

// function getInfo(page: Page, selector: string) {
//   return page.$eval(selector, (el) => el.textContent);
// }

async function getInfoByXpath(page: Page, path: string): Promise<string> {
  const [el] = await page.$x(path);
  return (await el.getProperty('textContent')).jsonValue();
}

const getListaDatiDaPrendere = () => [
  // { fieldName: '', xpath: '' },
  { fieldName: 'toponimo',            xpath: '/html/body/div[3]/div[2]/div/div[3]/div/div/div/section/div/div/div/div/article/div[2]/div/div[5]/div/div/div[3]/div[1]/div/div[2]/span' },
  { fieldName: 'via',                 xpath: '/html/body/div[3]/div[2]/div/div[3]/div/div/div/section/div/div/div/div/article/div[2]/div/div[5]/div/div/div[3]/div[2]/div/div[2]/span' },
  { fieldName: 'civico',              xpath: '/html/body/div[3]/div[2]/div/div[3]/div/div/div/section/div/div/div/div/article/div[2]/div/div[5]/div/div/div[4]/div[1]/div/div[2]/span' },
  { fieldName: 'scala',               xpath: '/html/body/div[3]/div[2]/div/div[3]/div/div/div/section/div/div/div/div/article/div[2]/div/div[5]/div/div/div[4]/div[2]/div/div[2]/span' },
  { fieldName: 'interno',             xpath: '/html/body/div[3]/div[2]/div/div[3]/div/div/div/section/div/div/div/div/article/div[2]/div/div[5]/div/div/div[5]/div[2]/div/div[2]/span' },
  { fieldName: 'localita',            xpath: '/html/body/div[3]/div[2]/div/div[3]/div/div/div/section/div/div/div/div/article/div[2]/div/div[5]/div/div/div[7]/div[1]/div/div[2]/span' },
  { fieldName: 'comune',              xpath: '/html/body/div[3]/div[2]/div/div[3]/div/div/div/section/div/div/div/div/article/div[2]/div/div[5]/div/div/div[6]/div[2]/div/div[2]/span' },
  { fieldName: 'provincia',           xpath: '/html/body/div[3]/div[2]/div/div[3]/div/div/div/section/div/div/div/div/article/div[2]/div/div[5]/div/div/div[6]/div[1]/div/div[2]/span' },
  { fieldName: 'cap',                 xpath: '/html/body/div[3]/div[2]/div/div[3]/div/div/div/section/div/div/div/div/article/div[2]/div/div[5]/div/div/div[7]/div[2]/div/div[2]/span' },
  { fieldName: 'potenzaContrattuale', xpath: '/html/body/div[3]/div[2]/div/div[3]/div/div/div/section/div/div/div/div/article/div[2]/div/div[4]/div/div/div[4]/div[1]/div/div[2]' },
  { fieldName: 'tensione',            xpath: '/html/body/div[3]/div[2]/div/div[3]/div/div/div/section/div/div/div/div/article/div[2]/div/div[4]/div/div/div[6]/div[1]/div/div[2]/span' },
  { fieldName: 'codiceIstatComune',   xpath: '/html/body/div[3]/div[2]/div/div[3]/div/div/div/section/div/div/div/div/article/div[2]/div/div[5]/div/div/div[8]/div[1]/div/div[2]/span/span' },
  { fieldName: 'piano',               xpath: '/html/body/div[3]/div[2]/div/div[3]/div/div/div/section/div/div/div/div/article/div[2]/div/div[5]/div/div/div[5]/div[1]/div/div[2]/span' },
  { fieldName: 'codiceEneltel',       xpath: '/html/body/div[3]/div[2]/div/div[3]/div/div/div/section/div/div/div/div/article/div[2]/div/div[3]/div/div/div[2]/div[1]/div/div[2]/span/span' },
  { fieldName: 'misuratoreMatricola', xpath: '/html/body/div[3]/div[2]/div/div[3]/div/div/div/section/div/div/div/div/article/div[2]/div/div[4]/div/div/div[7]/div[1]/div/div[2]/span/span' },
  { fieldName: 'misuratoreTipologia', xpath: '/html/body/div[3]/div[2]/div/div[3]/div/div/div/section/div/div/div/div/article/div[2]/div/div[4]/div/div/div[3]/div[2]/div/div[2]/span/span' },
  { fieldName: 'livelloTensione',     xpath: '/html/body/div[3]/div[2]/div/div[3]/div/div/div/section/div/div/div/div/article/div[2]/div/div[4]/div/div/div[6]/div[2]/div/div[2]/span/span' },
];

async function estraiInfoDaRestituireRecursive(
  page: Page,
  list: Array<{ fieldName: string; xpath: string; }>,
  result: Record<string, string>
): Promise<Record<string, string>> {
  const last = list.pop();

  if (!last) {
    return result;
  }

  const data = await getInfoByXpath(page, last.xpath);

  result[last.fieldName] = data;

  if (list.length) {
    return estraiInfoDaRestituireRecursive(page, list, result);
  }

  console.log({ result });

  return result;
}

export function estraiInfoDaRestituire(page: Page) {
  return estraiInfoDaRestituireRecursive(page, getListaDatiDaPrendere(), {});
}

export default async function estraiSuccessInfo(page: Page): Promise<string> {
  const {
    toponimo,
    via,
    civico,
    scala,
    interno,
    localita,
    comune,
    provincia,
    cap,
    potenzaContrattuale,
    tensione
  } = await estraiInfoDaRestituire(page);

  return [
    toponimo, via, civico, scala, `interno ${interno || '-'}`, localita, comune, provincia, cap,
    potenzaContrattuale ? `potenza ${potenzaContrattuale}` : null,
    tensione ? `tensione ${tensione}` : null
  ].filter(Boolean).join(' ');
}
