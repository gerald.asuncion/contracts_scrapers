import { Page } from "puppeteer";
import { Logger } from "../../logger";
import { EDistribuzionePodCheckerPayload } from "./types";
import estraiInfo, { Info } from "./estraiInfo";
import controllaStati from "./controllaStati";
import ErrorResponse from "../../response/ErrorResponse";
import { ScraperResponse } from "../scraper";

const ESITO_ERRORE = 'Il portale non sta restituendo esito, segnalarlo al back-office di supermoney e Verificare il punto di fornitura col back-office prima di proseguire';

export default async function estraiInfoControllaStati(
  page: Page,
  logger: Logger,
  payload: EDistribuzionePodCheckerPayload,
  timestamp: number,
  count = 1
): Promise<ScraperResponse> {
  logger.info("estraggo l'id della richiesta");

  logger.info(`estraggo gli stati nel portale. tentativo ${count}`);
  const [info, err] = await estraiInfo(page, logger);

  if (err) {
    err.extra = { idRichiesta: info.idRichiesta };
    return err;
  }

  logger.info('controllo gli stati');
  const result = await controllaStati(page, logger, payload, info as Info);

  if (!result) {
    if (((Date.now() - timestamp)/1000) >= 60) {
      return new ErrorResponse(ESITO_ERRORE);
    }

    await page.reload({ waitUntil: 'networkidle2' });

    return estraiInfoControllaStati(page, logger, payload, timestamp, ++count);
  }
  result.extra = { idRichiesta: info.idRichiesta };

  return result;
}
