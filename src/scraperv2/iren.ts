import waitCheckCredentials from '../browser/page/waitCheckCredentials';
import FailureResponse from '../response/FailureResponse';
import SuccessResponse from '../response/SuccessResponse';
import { ScraperResponse, WebScraper } from './scraper';
import { SCRAPER_TIPOLOGIA } from './ScraperOptions';
import getElementText from '../browser/page/getElementText';
import waitForSelectorAndType from '../browser/page/waitForSelectorAndType';
import waitForSelectorAndClick from '../browser/page/waitForSelectorAndClick';
import tryOrThrow from '../utils/tryOrThrow';
import waitForVisible from '../browser/page/waitForVisible';
import { QueueTask } from '../task/QueueTask';

type IrenScraperPayload = {
  codiceFiscale: string;
};

const ESITI_KO = ['Codice Fiscale/PIVA in Rifiuto Amministrativo (NON CARICABILE)'];

/**
 * iren morosita/morosità
 */
@QueueTask({ scraperName: 'iren' })
export default class IrenScraper extends WebScraper<IrenScraperPayload> {
  static readonly LOGIN_URL = 'https://sviluppo.irenmercato.it/IMEProposte/Login.aspx';

  async login(): Promise<void> {
    const { childLogger } = this;
    const { username, password } = await this.getCredenziali();
    await this.checkPageClosed();

    childLogger.info('apertura pagina di login');
    const page = await this.p();
    await page.goto(IrenScraper.LOGIN_URL, { waitUntil: 'networkidle2' });

    childLogger.info('attendo che la pagina si sia caricata');
    await tryOrThrow(() => waitForVisible(page, '.TDmain'), 'la pagina di login non si è caricata in tempo');

    await tryOrThrow(() => waitForSelectorAndType(page, '#txtOperatore', username), `non sono riuscito ad inserire lo username ${username}`);
    childLogger.info(`login > inserito username ${username}`);

    await tryOrThrow(() => waitForSelectorAndType(page, '#txtPassword', password), 'non sono riuscito ad inserire la password');
    childLogger.info('login > inserita password');

    await waitForSelectorAndClick(page, '#btnOK');

    await Promise.race([
      waitCheckCredentials(
        page,
        '#centerPanel > div > div.slds-col--padded.contentRegion.comm-layout-column > div > div:nth-child(2) > div > span > div > div',
        IrenScraper.LOGIN_URL,
        username,
        childLogger
      ),
      page.waitForSelector('#hlRicercaPodPDR')
    ]);
    childLogger.info('login effettuata');
  }

  async scrapeWebsite({ codiceFiscale }: IrenScraperPayload): Promise<ScraperResponse> {
    const logger = this.childLogger;
    const page = await this.p();

    await page.waitForSelector('#hlRicercaPodPDR');
    await page.click('#hlRicercaPodPDR');

    await page.waitForSelector('#txtCFPIVA');
    await page.type('#txtCFPIVA', codiceFiscale);
    logger.info('checking > inserito codice fiscale');

    await page.waitForSelector('#B_Cerca');
    await page.click('#B_Cerca');

    await page.waitForSelector('#LabelCreditCheckEsito');
    const esito = await getElementText(page, '#LabelCreditCheckEsito', 'textContent');

    if (ESITI_KO.includes(esito as string) || ['errato', 'non caricabil', 'NON OK'].some(test => esito?.toLowerCase().includes(test.toLowerCase()))) {
      logger.info(`${codiceFiscale} KO - ${esito}`);
      return new FailureResponse(esito);
    }

    return new SuccessResponse();
  }

  getScraperCodice() {
    return 'iren-morosita';
  }

  getScraperTipologia() {
    return SCRAPER_TIPOLOGIA.FATTIBILITA;
  }
}
