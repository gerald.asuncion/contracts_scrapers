import { Page } from "puppeteer";
import waitForNavigation from "../../browser/page/waitForNavigation";
import waitForSelectorAndType from "../../browser/page/waitForSelectorAndType";
import waitForXPathAndClick from "../../browser/page/waitForXPathAndClick";
import waitForXPathVisible from "../../browser/page/waitForXPathVisible";
import tryOrThrow from "../../utils/tryOrThrow";
import { Credenziali } from "../iberdrola/types";

const AREA_PRIVATA_LINK_SELECTOR = '//span[contains(@class, "Azienda")][contains(., "ACSMAGAM")]/parent::td/following-sibling::td//a[contains(., "Area privata")]';

export default async function login(page: Page, url: string, { username, password }: Credenziali) {
  await page.goto(url, { waitUntil: 'networkidle2' });

  await waitForXPathVisible(page, AREA_PRIVATA_LINK_SELECTOR);
  await Promise.all([
    waitForNavigation(page),
    waitForXPathAndClick(page, AREA_PRIVATA_LINK_SELECTOR)
  ]);

  await tryOrThrow(() => waitForSelectorAndType(page, 'input[name="twsTemplate$Content1$twsModule$txtUser"]', username), "non sono riuscito ad inserire l'username:");
  await tryOrThrow(() => waitForSelectorAndType(page, 'input[name="twsTemplate$Content1$twsModule$txtPSW"]', password), 'non sono riuscito ad inserire la password:');

  // potrebbe uscire un alert col seguente messaggio
  // ATTENZIONE: mancano 0 giorni alla scadenza della password.. Cambiare la password adesso?
  // se clicchiamo su annulla si può proseguire
  await page.on('dialog', async (dialog) => {
    // const message = dialog.message();
    await dialog.dismiss();
  });

  await Promise.all([
    waitForNavigation(page),
    waitForXPathAndClick(page, '//input[contains(@value, "Login")]')
  ]);
}
