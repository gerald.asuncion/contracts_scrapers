/**
 * @openapi
 *
 * /scraper/lereti/check/pdr:
 *  post:
 *    tags:
 *      - v2
 *    description: Va sul portale di Lereti e controlla se il pdr è valido.
 *    consume:
 *      - application/json
 *    produces:
 *      - application/json
 *    responses:
 *      200:
 *        description: SuccessResponse
 *    parameters:
 *      - in: body
 *        description:
 *          pdr da controllare.
 *        schema:
 *          type: object
 *          required:
 *            - pdr
 *            - tipoContratto
 *          properties:
 *            pdr:
 *              type: string
 *            tipoContratto:
 *              type: string
 *              enum: [subentro, attivazione]
 *
 * /scraper/queue/leretiCheckPdr:
 *  post:
 *    tags:
 *      - v2
 *      - queue
 *    description:
 *      Va sul portale di Lereti e controlla se il pdr è valido.
 *      Il payload è lo stesso della rotta diretta `/scraper/lereti/check/pdr`,
 *      in più va aggiunto il webhook che lo scraper poi andrà a richiamare per notificare l'esito.
 *    consume:
 *      - application/json
 *    produces:
 *      - application/json
 *    responses:
 *      200:
 *        description: SuccessResponse
 */
