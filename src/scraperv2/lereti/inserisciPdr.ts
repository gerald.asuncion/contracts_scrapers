import { Page } from "puppeteer";
import waitForSelectorAndClick from "../../browser/page/waitForSelectorAndClick";
import waitForSelectorAndType from "../../browser/page/waitForSelectorAndType";
import waitForXPathAndClickEvaluated from "../../browser/page/waitForXPathAndClickEvaluated";
import tryOrThrow from "../../utils/tryOrThrow";

export default async function inserisciPdr(page: Page, pdr: string) {
  await tryOrThrow(
    () => waitForXPathAndClickEvaluated(page, '//a[contains(., "Ricerca Predisposizioni PDR")]'),
    'non sono riuscito a cliccare su `Ricerca Predisposizioni PDR`:'
  );

  await tryOrThrow(
    () => waitForSelectorAndType(page, 'input[name="twsTemplate$Content2$twsModule$txtPdr"]', pdr),
    'non sono riuscito ad inserire il pdr:'
  );

  await tryOrThrow(
    () => waitForSelectorAndClick(page, 'input[name="twsTemplate$Content2$twsModule$btnSearchPDP"]'),
    'non sono riuscito a cliccare su `Ricerca Predisposizioni PDR`:'
  );
}
