import { BasicPayload } from "../../credenziali/getCredenzialiDalServizio";
import { QueueTask } from "../../task/QueueTask";
import tryOrThrow from "../../utils/tryOrThrow";
import { ScraperResponse, WebScraper } from "../scraper";
import { ScraperTipologia, SCRAPER_TIPOLOGIA } from "../ScraperOptions";
import generaEsito from "./generaEsito";
import inserisciPdr from "./inserisciPdr";
import login from "./login";
import recuperaDatiPerEsito from "./recuperaDatiPerEsito";

export type LeretiCheckPdrPayload = BasicPayload & {
  pdr: string;
  // regione: string;
  // provincia: string;
  // comune: string;
  tipoContratto: 'subentro' | 'attivazione';
};

@QueueTask()
export default class LeretiCheckPdr extends WebScraper<LeretiCheckPdrPayload> {
  static readonly LOGIN_URL: string = 'http://portal.reti.acsm-agam.it/Portal/Index.aspx?m=831G&area=B2B';

  async login(payload: LeretiCheckPdrPayload): Promise<void> {
    const credenziali = await this.getCredenziali(payload);
    const page = await this.p();

    await login(page, LeretiCheckPdr.LOGIN_URL, credenziali);
  }

  async scrapeWebsite(payload: LeretiCheckPdrPayload): Promise<ScraperResponse> {
    const logger = this.childLogger;
    const page = await this.p();

    await inserisciPdr(page, payload.pdr);

    const [datiPerEsito, err] = await tryOrThrow(() => recuperaDatiPerEsito(page), "non sono riuscito a recuperare i dati per l'esito:");

    if (err) {
      return err;
    }

    const esito = generaEsito(datiPerEsito!, payload);
    logger.info(`payload: ${JSON.stringify(payload)} | dati per esito: ${JSON.stringify(datiPerEsito)} | esito: ${JSON.stringify(esito)}`);
    return esito;
  }

  getScraperCodice(): string {
    return 'lereti-precheck-pdr';
  }

  getScraperTipologia(): ScraperTipologia {
    return SCRAPER_TIPOLOGIA.FATTIBILITA;
  }
}
