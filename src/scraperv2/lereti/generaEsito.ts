import CasoNonGestitoResponse from "../../response/CasoNonGestitoResponse";
import FailureResponse from "../../response/FailureResponse";
import SuccessResponse from "../../response/SuccessResponse";
import { LeretiCheckPdrPayload } from "./LeretiCheckPdr";
import { LeretiCheckPdrDatiPerEsito } from "./recuperaDatiPerEsito";

const NOTE_MISURATORE = {
  POSATO: 'posato',
  CHIUSO: 'chiuso',
  RIMOSSO: 'rimosso'
};

export default async function generaEsito({ comune, indirizzo, potenzaMassima, noteMisuratore }: LeretiCheckPdrDatiPerEsito, { tipoContratto }: LeretiCheckPdrPayload) {
  if (tipoContratto === 'subentro') {
    if (!noteMisuratore.length || (noteMisuratore.length === 1 && noteMisuratore[0] === NOTE_MISURATORE.POSATO)) {
      return new FailureResponse("Ko-necessario procedere con una prima attivazione");
    }

    if (
      (noteMisuratore.includes(NOTE_MISURATORE.POSATO) && noteMisuratore.includes(NOTE_MISURATORE.CHIUSO)) ||
      (noteMisuratore.includes(NOTE_MISURATORE.RIMOSSO) && noteMisuratore.includes(NOTE_MISURATORE.CHIUSO))
    ) {
      return new SuccessResponse(`${indirizzo} ${comune} portata termica ${potenzaMassima || '-'}`);
    }
  }

  if (tipoContratto === 'attivazione') {
    if (!noteMisuratore.length || (noteMisuratore.length === 1 && noteMisuratore[0] === NOTE_MISURATORE.POSATO)) {
      return new SuccessResponse(`${indirizzo} ${comune} portata termica ${potenzaMassima || '-'}`);
    }

    if (
      (noteMisuratore.includes(NOTE_MISURATORE.POSATO) && noteMisuratore.includes(NOTE_MISURATORE.CHIUSO)) ||
      (noteMisuratore.includes(NOTE_MISURATORE.RIMOSSO) && noteMisuratore.includes(NOTE_MISURATORE.CHIUSO))
    ) {
      return new FailureResponse("ko-necessario procedere con un subentro");
    }
  }

  return new CasoNonGestitoResponse();
}
