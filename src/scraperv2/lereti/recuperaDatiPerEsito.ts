import { Page } from "puppeteer";
import getElementText from "../../browser/page/getElementText";
import waitForVisible from "../../browser/page/waitForVisible";
import FailureResponse from "../../response/FailureResponse";

export type LeretiCheckPdrDatiPerEsito = {
  comune: string;
  indirizzo: string;
  potenzaMassima: string;
  noteMisuratore: string[];
  matricolaMisuratore: string;
  classeMisuratore: string;
  stato: string;
};

const getCellSelector = (index: number) => `.trRptItem > td:nth-child(${index})`;

export default async function recuperaDatiPerEsito(page: Page): Promise<[null, FailureResponse] | [LeretiCheckPdrDatiPerEsito, null]> {
  let pdrTrovatiMessage = 'PDR trovati: 0';
  try {
    await waitForVisible(page, '.messageError', { timeout: 3000 });
    pdrTrovatiMessage = await getElementText(page, '.messageError', 'textContent') as string;
  } catch (ex) {
    await waitForVisible(page, '.tdIstruzioni', { timeout: 3000 });
    pdrTrovatiMessage = await getElementText(page, '.tdIstruzioni', 'textContent') as string;
  }

  const pdrTrovati = Number(pdrTrovatiMessage.replace('PDR trovati:', '').trim());

  if (!pdrTrovati) {
    return [null, new FailureResponse("ko-pdr non contendibile")];
  }

  await waitForVisible(page, '#twsTemplate_Content2_twsModule_TestataPDP');

  const comune = await getElementText(page, getCellSelector(2), 'textContent') as string;

  const indirizzo = await getElementText(page, getCellSelector(3), 'textContent') as string;

  const potenzaMassima = await getElementText(page, getCellSelector(4), 'textContent') as string;

  const noteMisuratore = await getElementText(page, getCellSelector(8), 'textContent') as string;

  // Classe Misuratore, Stato
  const matricolaMisuratore = await getElementText(page, getCellSelector(5), 'textContent') as string;
  const classeMisuratore = await getElementText(page, getCellSelector(6), 'textContent') as string;
  const stato = await getElementText(page, getCellSelector(7), 'textContent') as string;

  return [{
    comune: comune.replace(/\n/g, '').trim(),
    indirizzo: indirizzo.replace(/\n/g, '').trim(),
    potenzaMassima: potenzaMassima.replace(/\n/g, '').trim(),
    noteMisuratore: noteMisuratore.replace(/\n/g, '').replace(/IL/g, '').replace(/\d*\/\d*/g, '').trim().toLowerCase().split(' ').filter(Boolean),
    matricolaMisuratore,
    classeMisuratore,
    stato
  }, null];
}
