/**
 * @openapi
 *
 * /scraper/ireti/check/pod:
 *  post:
 *    tags:
 *      - v2
 *    description: Effettua il controllo del pod sul portale di Ireti.
 *      Questa è la rotta diretta allo scraper, quindi non usa il meccanismo del webhook.
 *    consume:
 *      - application/json
 *    produces:
 *      - application/json
 *    responses:
 *      200:
 *        description: SuccessResponse
 *    parameters:
 *      - in: body
 *        schema:
 *          type: object
 *          required:
 *            - pod
 *          properties:
 *            pod:
 *              type: string
 *            tipoContratto:
 *              type: string
 *            codicePratica:
 *              type: string
 *
 * /scraper/queue/iretiCheckPod:
 *  post:
 *    tags:
 *      - v2
 *      - queue
 *    description:
 *      Effettua il controllo del pod sul portale di Ireti.
 *      Il payload è lo stesso della rotta diretta `/scraper/ireti/check/pod`,
 *      in più va aggiunto il webhook che lo scraper poi andrà a richiamare per notificare l'esito.
 *    consume:
 *      - application/json
 *    produces:
 *      - application/json
 *    responses:
 *      200:
 *        description: SuccessResponse
 */
