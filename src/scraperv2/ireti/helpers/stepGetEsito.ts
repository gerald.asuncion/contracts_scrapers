import { Page } from "puppeteer";
import ErrorResponse from "../../../response/ErrorResponse";
import FailureResponse from "../../../response/FailureResponse";
import SuccessResponse from "../../../response/SuccessResponse";
import tryOrThrow from "../../../utils/tryOrThrow";
import webHelpers from "./webHelpers";

export default async function(page: Page, type: 'pdr' | 'pod', addressLabels: (string|[string, string])[]) {
  // here only for local declarations
  const {
    selectIframe,
    selectElementByXPath,
    typeElement,
    waitForDefined,
    waitForDelay,
    logger_info
  } = await webHelpers(page, undefined as any);

  async function retrieveAddress(page: Page): Promise<string> {
    const selectIframe: any = null;
    const selectElementByXPath: any = null;

    return tryOrThrow(() => page.evaluate((_addressLabels: (string|[string, string])[]) => {
      logger_info("recupero l'esito");

      const iframe = selectIframe('iframe#contentAreaFrame', 'iframe#isolatedWorkArea');
      const getInputValueByLabel = (label: string) => {
        logger_info(`dati immobile: '${label}'`);
        return selectElementByXPath(`//span[text() = "${label}"]//ancestor::tr[1]//input`, iframe).value;
      };

      return _addressLabels
      .map(arg => {
        if (Array.isArray(arg)) {
          return `${arg[0]} ${getInputValueByLabel(arg[1])}`
        }
        return getInputValueByLabel(arg);
      })
      .join(' ');
    }, addressLabels), "non sono riuscito a recuperare l'indirizzo");
  }

  const esito = await tryOrThrow(() => page.evaluate(() => {
    const iframe = selectIframe('iframe#contentAreaFrame', 'iframe#isolatedWorkArea');

    const elBtnInoltra = selectElementByXPath('//span[text()="Inoltra"]', iframe)!;
    elBtnInoltra.click();

    return waitForDefined(() => selectElementByXPath('//span[text() = "Doc. rif. distr."]/ancestor::span[contains(@class, "lsFlowLayoutItem--wrapping")]//span[contains(@class, "lsTextView--paragraph")]', iframe)!.innerText)
    .catch(() => {
      logger_info("'Doc. rif. distr.': assente");
    })
    .then((docRifDistr) => {
      logger_info(`'Doc. rif. distr.': '${docRifDistr}'`);

      const elTextEsito = selectElementByXPath('//span[text() = "Stato Richiesta"]/ancestor::span[contains(@class, "lsFlowLayoutItem--wrapping")]//span[contains(@class, "lsTextView--paragraph")]', iframe)!;
      logger_info(elTextEsito ? `testo esito: ${elTextEsito.innerText}` : 'testo esito non trovato');

      return elTextEsito.innerText;
    });
  }), "non sono riuscito a ricavare l'esito");

  switch ((esito || '').toLowerCase()) {
    case 'rifiutata':
      // non ha indirizzo
      return new FailureResponse(`ko-${type} non contendibile`);
    case 'positiva':
      return new SuccessResponse(`${type} contendibile, verificare col cliente la corretta tipologia di richiesta. ${await retrieveAddress(page)}`);
  }

  return new ErrorResponse(`eisto non gesito: "${esito}"`);
}
