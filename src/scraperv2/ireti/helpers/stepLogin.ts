import { Page } from "puppeteer";
import isVisible from "../../../browser/page/isVisible";
import waitForNavigation from "../../../browser/page/waitForNavigation";
import waitForSelectorAndClick from "../../../browser/page/waitForSelectorAndClick";
import waitForSelectorAndType from "../../../browser/page/waitForSelectorAndType";
import WrongCredentialsError from "../../../errors/WrongCredentialsError";
import { Logger } from "../../../logger";

let userAgent = 'Mozilla/5.0 (Windows NT 10.0; Trident/7.0; rv:11.0) like Gecko';

export default async function(
  page: Page,
  logger: Logger,
  loginUrl: string,
  username: string,
  password: string,
  payload: any
): Promise<any> {
  await page.setUserAgent(payload.userAgent || userAgent);
  await page.setExtraHTTPHeaders({
    'Accept-Language': 'it-IT,it;q=0.9,en-US;q=0.8,en;q=0.7'
  });

  logger.info(`apro la pagina ${loginUrl}`);
  await page.goto(loginUrl, { waitUntil: 'networkidle2' });

  // controllo se utente già loggato
  if (await isVisible(page, 'form#certLogonForm')) {
    await waitForSelectorAndType(page, 'input#logonuidfield', username);
    logger.info('inserito username');

    await waitForSelectorAndType(page, 'input#logonpassfield', password);
    logger.info('inserita password');

    await Promise.all([
      waitForSelectorAndClick(page, 'input[type="submit"][name="uidPasswordLogon"]'),
      waitForNavigation(page)
    ]);

    if (await isVisible(page, 'form#certLogonForm span.urMsgBarImgError')) {
      // const errorText = getElementText(page, 'form#certLogonForm span.urTxtMsg', 'innerText');
      throw new WrongCredentialsError(loginUrl, username);
    }

    logger.info('credenziali valide');
  }
}
