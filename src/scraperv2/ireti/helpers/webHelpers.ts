import { Page } from "puppeteer";
import { Logger } from "../../../logger";

declare const logger_info: (message: string) => void;

function selectIframe(...querySelectors: string[]): Document {
  return querySelectors.reduce((iframe, selector) => (iframe?.querySelector(selector) as any)?.contentDocument, document) as any;
}

function selectElementByXPath<T = HTMLElement>(expression: string, doc: Document = document): T | null {
  return doc
  .evaluate(
    expression,
    doc,
    null,
    XPathResult.FIRST_ORDERED_NODE_TYPE,
    null
  )
  .singleNodeValue as any;
}

function typeElement(element: HTMLInputElement, text: string) {
  const dt = 30;
  const chars = text.split('');

  const funcDown = () => {
    const key = chars.shift();

    element.dispatchEvent(new KeyboardEvent('keypress', { key }));
    setTimeout(() => element.dispatchEvent(new KeyboardEvent('keydown', { key })));
    setTimeout(() => element.value += key, dt / 2);
    setTimeout(() => element.dispatchEvent(new KeyboardEvent('change')), dt);
    setTimeout(() => element.dispatchEvent(new KeyboardEvent('keyup', { key })), 2 * dt);

    if (chars.length) {
      setTimeout(funcDown, 4 * dt);
    } else {
      setTimeout(() => element.blur(), dt);
    }
  }

  element.focus();
  setTimeout(funcDown, 50);

  return new Promise(res => setTimeout(res, 6 * (1 + chars.length) * dt))
}

function waitForDefined<T>(cback: () => T, {
  errorMessage = 'element not found!'
}: {
  errorMessage?: string
} = {}): Promise<T> {
  return new Promise((res, rej) => {
    let counter = 6 * 40;

    const intrvl = setInterval(() => {
      if (counter-- == 0) {
        clearInterval(intrvl);

        rej(errorMessage);
      } else {
        const result = cback();

        if (result) {
          clearInterval(intrvl);

          res(result);
        }
      }
    }, 250);
  });
}

function waitForDelay(ms = 1000) {
  return new Promise(res => setTimeout(res, ms))
}

export default async function(page: Page, logger: Logger, logPrefix = '') {
  if (await page.evaluate(() => {
    return typeof selectElementByXPath === 'undefined';
  })) {
    logger.info("Injecting helper functions inside window");

    await page.exposeFunction('logger_info', function logger_info(message: string) {
      logger.info(`${logPrefix}${message}`);
    });

    await page.addScriptTag({
      content: `${selectIframe} ${selectElementByXPath} ${typeElement} ${waitForDefined} ${waitForDelay}`
    });
  }

  const onlyWebFunction = (funcName: string) => () => { throw Error(`This function "${funcName}" must be only called inside browser not in nodejs execution environment!`); };

  return {
    selectIframe: onlyWebFunction('selectIframe') as typeof selectIframe,
    selectElementByXPath: onlyWebFunction('selectElementByXPath') as typeof selectElementByXPath,
    typeElement: onlyWebFunction('typeElement') as typeof typeElement,
    waitForDefined: onlyWebFunction('waitForDefined') as typeof waitForDefined,
    waitForDelay: onlyWebFunction('waitForDelay') as typeof waitForDelay,
    logger_info: (message: string) => logger.info(`${logPrefix} NODE ${message}`)
  };
}
