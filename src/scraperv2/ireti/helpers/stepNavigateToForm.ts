import { Page } from "puppeteer";
import ErrorResponse from "../../../response/ErrorResponse";
import delay from "../../../utils/delay";
import webHelpers from "./webHelpers";

export default async function(page: Page, cmdText: string): Promise<ErrorResponse|void> {
  // here only for local declarations
  const {
    selectIframe,
    selectElementByXPath,
    typeElement,
    waitForDefined,
    waitForDelay,
    logger_info
  } = await webHelpers(page, undefined as any);

  const errorMessage = await page.evaluate((_cmdText) => {
    logger_info('Navigazione alla form!');

    const iframe = selectIframe('iframe#contentAreaFrame', 'iframe[title="Nuova richiesta"]');

    if (!iframe) {
      return Promise.reject('Portal server error.')
    }

    return waitForDefined(() => iframe.querySelector('span[aria-label="Richieste di servizio "]') as HTMLButtonElement)
    .then(elBtnRichieste => {
      elBtnRichieste.click();

      return waitForDelay(1000)
      .then(() => {
        return waitForDefined<HTMLButtonElement>(
          () => [...iframe.querySelectorAll('.urMnuTxt > span')]
          .filter(node => (node as HTMLElement).innerText.toLowerCase() == _cmdText.toLowerCase())[0] as HTMLButtonElement, {
            errorMessage: `Menu non trovato: '${_cmdText}'`
          }
        )
        .then(elBtnSC1 => {
          elBtnSC1.click();

          return waitForDelay(2000)
          .then(() => waitForDefined(() => selectElementByXPath('//td/span[text() = "Creazione singola"]', iframe))
            .then(elBtnCS => {
              elBtnCS!.click();
            })
          )
        });
      })
    })
  }, cmdText);

  if (typeof errorMessage === 'string' && errorMessage) {
    return new ErrorResponse(errorMessage as any);
  }

  await delay(2000);
}
