import { Page } from "puppeteer";
import getElementText from "../../../browser/page/getElementText";
import isVisible from "../../../browser/page/isVisible";
import waitForNavigation from "../../../browser/page/waitForNavigation";
import waitForXPathAndClick from "../../../browser/page/waitForXPathAndClick";
import ErrorResponse from "../../../response/ErrorResponse";
import delay from "../../../utils/delay";

export default async function(page: Page): Promise<ErrorResponse|void> {
  if (await isVisible(page, '.ctrlLblReq', 2000)) {
    const errorMessage = await getElementText(page, '.ctrlLblReq', 'innerText');

    if (errorMessage && typeof errorMessage === 'string') {
      return new ErrorResponse(errorMessage);
    }
  }

  await Promise.all([
    waitForXPathAndClick(page, '//div[contains(@aria-label, "Nuova richiesta")]//span[text() = "Nuova richiesta"]'),
    waitForNavigation(page)
  ]);

  await page.waitForXPath('//iframe[@id="contentAreaFrame"]');
  await delay(7500);
}
