import { Page } from "puppeteer";

export default async function(page: Page): Promise<string|null> {
  const selectIframe: any = null;
  const selectElementByXPath: any = null;

  return page.evaluate(() => {
    const iframe = selectIframe('iframe#contentAreaFrame', 'iframe[title="Nuova richiesta"]');

    const elTextEsito = selectElementByXPath('//div[contains(@id, "-icon")][@class="lsMAErrorIcon"]/parent::div/div[contains(@id, "-text")]', iframe)!;

    if (elTextEsito) {
      return elTextEsito.innerText;
    }
    return null;
  });
}
