import { Page } from "puppeteer";
import ErrorResponse from "../../../response/ErrorResponse";
import delay from "../../../utils/delay";
import tryOrThrow from "../../../utils/tryOrThrow";
import checkForErrors from "./checkForErrors";
import webHelpers from "./webHelpers";

export default async (page: Page, pdrOrPodLabel: string, codicePratica: string, pdrOrPod: string) => {
  // here only for local declarations
  const {
    selectIframe,
    selectElementByXPath,
    typeElement,
    waitForDefined,
    waitForDelay,
    logger_info
  } = await webHelpers(page, undefined as any);

  await tryOrThrow(async () => page.evaluate((_pdrOrPodLabel, _codicePratica, _pdrOrPod) => {
    logger_info('compilo la form');

    const iframe = selectIframe('iframe#contentAreaFrame', 'iframe[title="Nuova richiesta"]');

    const elInputCodicePratica = selectElementByXPath<HTMLInputElement>('(//span[text() = "Codice Pratica"]//ancestor::tr)[last()]//input', iframe)!;
    return typeElement(elInputCodicePratica, _codicePratica)
    .then(() => waitForDelay(250))
    .then(() => {
      logger_info('typing "PdR or PoD".');

      const elInputPdr = selectElementByXPath<HTMLInputElement>(`(//span[text() = "${_pdrOrPodLabel}"]//ancestor::tr)[last()]//input`, iframe)!;
      return typeElement(elInputPdr, _pdrOrPod + '')
      .then(() => elInputCodicePratica.focus())
      .then(() => waitForDelay(250))
      .then(() => {
        logger_info('clicking Salva!');

        const elButtonSalva = selectElementByXPath('//span[text()="Salva"]', iframe);
        elButtonSalva!.click();
      })
    });
  }, pdrOrPodLabel, codicePratica, pdrOrPod),
  'non sono riuscito a popolare la form!');

  await delay(1000);

  const error = await checkForErrors(page);
  if (error) {
    return new ErrorResponse(error);
  }

  await delay(1000);

  await tryOrThrow(() => page.evaluate(() => {
    const iframe = selectIframe('iframe[id^="URLSPW-"]');

    const elButtonConferma = selectElementByXPath('//span[text()="Sì"]', iframe)!;
    elButtonConferma.click();
  }), 'non sono riuscito a confermare la form');

  // TODO: wait for response
  // TODO: check for errors
  await delay(3000);
};
