import { Page } from 'puppeteer';
import getElementText from '../../browser/page/getElementText';
import isVisible from '../../browser/page/isVisible';
import waitForNavigation from '../../browser/page/waitForNavigation';
import waitForSelectorAndClick from '../../browser/page/waitForSelectorAndClick';
import waitForSelectorAndType from '../../browser/page/waitForSelectorAndType';
import waitForXPathAndClick from '../../browser/page/waitForXPathAndClick';
import WrongCredentialsError from '../../errors/WrongCredentialsError';
import { Logger } from '../../logger';
import ErrorResponse from '../../response/ErrorResponse';
import FailureResponse from '../../response/FailureResponse';
import SuccessResponse from '../../response/SuccessResponse';
import { QueueTask } from '../../task/QueueTask';
import delay from '../../utils/delay';
import tryOrThrow from '../../utils/tryOrThrow';
import { ScraperResponse, WebScraper } from '../scraper';
import { SCRAPER_TIPOLOGIA } from '../ScraperOptions';
import IretiCheckPdrPayload from './check/IretiCheckPdrPayload';

declare const logger_info: (message: string) => void;
let userAgent = 'Mozilla/5.0 (Windows NT 10.0; Trident/7.0; rv:11.0) like Gecko';

/**
 * TODO: andrebbe reso uguale al check-pod, usando cioé gli helper. ci vorrebbe un tkt a parte
 */
 @QueueTask()
export default class IretiCheckPdr extends WebScraper {
  static readonly LOGIN_URL = 'https://portaledistributore.ireti.it';

  async login(payload: any): Promise<void> {
    const logger = this.childLogger;
    const { username, password } = await this.getCredenziali();

    // TODO: add seeder scraper: 'Ireti check pod'

    logger.info('step > login');

    this.checkPageClosed();

    const page = await this.p();
    // not working  => User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36
		// working      => User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99.0.4844.74 Safari/537.36 Edg/99.0.1150.55

    await page.setUserAgent(payload.userAgent || userAgent);
    await page.setExtraHTTPHeaders({
      'Accept-Language': 'it-IT,it;q=0.9,en-US;q=0.8,en;q=0.7'
    });

    logger.info(`apro la pagina ${IretiCheckPdr.LOGIN_URL}`);
    await page.goto(IretiCheckPdr.LOGIN_URL, { waitUntil: 'networkidle2' });

    // controllo se utente già loggato
    if (await isVisible(page, 'form#certLogonForm')) {
      await waitForSelectorAndType(page, 'input#logonuidfield', username);
      logger.info('inserito username');

      await waitForSelectorAndType(page, 'input#logonpassfield', password);
      logger.info('inserita password');

      await Promise.all([
        waitForSelectorAndClick(page, 'input[type="submit"][name="uidPasswordLogon"]'),
        waitForNavigation(page)
      ]);

      if (await isVisible(page, 'form#certLogonForm span.urMsgBarImgError')) {
        // const errorText = getElementText(page, 'form#certLogonForm span.urTxtMsg', 'innerText');
        throw new WrongCredentialsError(IretiCheckPdr.LOGIN_URL, username);
      }

      logger.info('credenziali valide');
    }
  }

  async scrapeWebsite(payload: IretiCheckPdrPayload): Promise<ScraperResponse> {
    const logger = this.childLogger;
    const page = await this.p();

    return this.wrappedScrape(page, logger, payload)
    .finally(() =>
      this.logout(page)
      .catch(err => {
        logger.error("Error during logout!");
        logger.error(err);
      })
    );
  }

  async wrappedScrape(page: Page, logger: Logger, payload: IretiCheckPdrPayload): Promise<ScraperResponse> {
    if (await isVisible(page, '.ctrlLblReq', 2000)) {
      const errorMessage = await getElementText(page, '.ctrlLblReq', 'innerText');

      if (errorMessage && typeof errorMessage === 'string') {
        return new ErrorResponse(errorMessage);
      }
    }

    await Promise.all([
      waitForXPathAndClick(page, '//div[contains(@aria-label, "Nuova richiesta")]//span[text() = "Nuova richiesta"]'),
      waitForNavigation(page)
    ]);

    await page.waitForXPath('//iframe[@id="contentAreaFrame"]');
    await delay(7500);

    const {
      pdr,
      codicePratica = `SUPERMONEY${(Math.random() * 46656) | 0}`
    } = payload;

    await page.exposeFunction('logger_info', function logger_info(message: string) { logger.info(`[codicePratica=${codicePratica}] ${message}`); });

    function selectIframe(...querySelectors: string[]): Document {
      return querySelectors.reduce((iframe, selector) => (iframe?.querySelector(selector) as any)?.contentDocument, document) as any;
    }
    function selectElementByXPath<T = HTMLElement>(expression: string, doc: Document = document): T | null {
      return doc
      .evaluate(
        expression,
        doc,
        null,
        XPathResult.FIRST_ORDERED_NODE_TYPE,
        null
      )
      .singleNodeValue as any;
    }
    function typeElement(element: HTMLInputElement, text: string) {
      const dt = 30;
      const chars = text.split('');

      const funcDown = () => {
        const key = chars.shift();

        element.dispatchEvent(new KeyboardEvent('keypress', { key }));
        setTimeout(() => element.dispatchEvent(new KeyboardEvent('keydown', { key })));
        setTimeout(() => element.value += key, dt / 2);
        setTimeout(() => element.dispatchEvent(new KeyboardEvent('change')), dt);
        setTimeout(() => element.dispatchEvent(new KeyboardEvent('keyup', { key })), 2 * dt);

        if (chars.length) {
          setTimeout(funcDown, 4 * dt);
        } else {
          setTimeout(() => element.blur(), dt);
        }
      }

      element.focus();
      setTimeout(funcDown, 50);

      return new Promise(res => setTimeout(res, 6 * (1 + chars.length) * dt))
    }
    function waitForDefined<T>(cback: () => T): Promise<T> {
      return new Promise((res, rej) => {
        let counter = 20;
        const intrvl = setInterval(() => {
          if (counter-- == 0) {
            clearInterval(intrvl);

            rej('element not found!');
          } else {
            const result = cback();

            if (result) {
              clearInterval(intrvl);

              res(result);
            }
          }
        }, 250);
      });
    }

    function waitForDelay(ms = 1000) {
      return new Promise(res => setTimeout(res, ms))
    }

    await page.addScriptTag({
      content: `${selectIframe} ${selectElementByXPath} ${typeElement} ${waitForDefined} ${waitForDelay}`
    });

    const errorMessage = await page.evaluate(() => {
      const iframe = selectIframe('iframe#contentAreaFrame', 'iframe[title="Nuova richiesta"]');

      if (!iframe) {
        return Promise.reject('Portal server error.')
      }

      return waitForDefined(() => iframe.querySelector('span[aria-label="Richieste di servizio "]') as HTMLButtonElement)
      .then(elBtnRichieste => {
        elBtnRichieste.click();

        return waitForDelay(1000)
        .then(() => {
          return waitForDefined<HTMLButtonElement>(
            () => [...iframe.querySelectorAll('.urMnuTxt > span')]
            .filter(node => (node as HTMLElement).innerText.toLowerCase() == 'SC1 - Siti Contendibili'.toLowerCase())[0] as HTMLButtonElement
          )
          .then(elBtnSC1 => {
            elBtnSC1.click();

            return waitForDelay(2000)
            .then(() => waitForDefined(() => selectElementByXPath('//td/span[text() = "Creazione singola"]', iframe))
              .then(elBtnCS => {
                elBtnCS!.click();
              })
            )
          });
        })
      })
    });

    if (typeof errorMessage === 'string' && errorMessage) {
      return new ErrorResponse(errorMessage as any);
    }

    await delay(2000);

    await tryOrThrow(() => page.evaluate((_codicePratica, _pdr) => {
      const iframe = selectIframe('iframe#contentAreaFrame', 'iframe[title="Nuova richiesta"]');

      logger_info('typing "Codice Pratica".');

      const elInputCodicePratica = selectElementByXPath<HTMLInputElement>('(//span[text() = "Codice Pratica"]//ancestor::tr)[last()]//input', iframe)!;
      return typeElement(elInputCodicePratica, _codicePratica)
      .then(() => waitForDelay(250))
      .then(() => {
        logger_info('typing "PdR".');

        const elInputPdr = selectElementByXPath<HTMLInputElement>('(//span[text() = "PdR"]//ancestor::tr)[last()]//input', iframe)!;
        return typeElement(elInputPdr, _pdr + '')
        .then(() => elInputCodicePratica.focus())
        .then(() => waitForDelay(250))
        .then(() => {
          logger_info('clicking Salva!');

          const elButtonSalva = selectElementByXPath('//span[text()="Salva"]', iframe);
          elButtonSalva!.click();
        })
      });
    }, codicePratica, pdr), 'non sono riuscito a popolare la form!');

    await delay(1000);

    const error = await this.checkForErrors(page);
    if (error) {
      return new ErrorResponse(error);
    }

    await delay(1000);

    await tryOrThrow(() => page.evaluate(() => {
      const iframe = selectIframe('iframe[id^="URLSPW-"]');

      const elButtonConferma = selectElementByXPath('//span[text()="Sì"]', iframe)!;
      elButtonConferma.click();
    }), 'non sono riuscito a confermare la form');

    // TODO: wait for response
    // TODO: check for errors
    await delay(1000);

    const esito = await tryOrThrow(() => page.evaluate(() => {
      const iframe = selectIframe('iframe#contentAreaFrame', 'iframe#isolatedWorkArea');

      const elBtnInoltra = selectElementByXPath('//span[text()="Inoltra"]', iframe)!;
      elBtnInoltra.click();

      return waitForDefined(() => selectElementByXPath('//span[text() = "Doc. rif. distr."]/ancestor::span[contains(@class, "lsFlowLayoutItem--wrapping")]//span[contains(@class, "lsTextView--paragraph")]', iframe)!.innerText)
      .catch(() => {
        logger_info("'Doc. rif. distr.': assente");
      })
      .then((docRifDistr) => {
        logger_info(`'Doc. rif. distr.': '${docRifDistr}'`);

        const elTextEsito = selectElementByXPath('//span[text() = "Stato Richiesta"]/ancestor::span[contains(@class, "lsFlowLayoutItem--wrapping")]//span[contains(@class, "lsTextView--paragraph")]', iframe)!;
        logger_info(elTextEsito ? `testo esito: ${elTextEsito.innerText}` : 'testo esito non trovato');

        return elTextEsito.innerText;
      });
    }), "non sono riuscito a ricavare l'esito");

    switch (esito.toLowerCase()) {
      case 'rifiutata':
        // non ha indirizzo
        return new FailureResponse("ko-pdr non contendibile");
      case 'positiva':
        return new SuccessResponse("pdr contendibile, verificare col cliente la corretta tipologia di richiesta. " + await this.retrieveAddress(page));
    }

    return new ErrorResponse(`eisto non gesito: "${esito}"`);
  }

  async logout(page: Page) {
    return tryOrThrow(async () => {
      await waitForSelectorAndClick(page, 'a#meAreaHeaderButton');
      await delay(250);
      await waitForSelectorAndClick(page, 'span#logoutBtn-inner > span.sapMBtnFocusDiv');
      await delay(250);
      await waitForXPathAndClick(page, '//div[@id="sap-ui-static"]//footer//button//span[@class="sapMBtnContent"][text() = "OK"]');
      await delay(1000);
    }, 'errore durante il logout');
  }

  async retrieveAddress(page: Page): Promise<string> {
    const selectIframe: any = null;
    const selectElementByXPath: any = null;

    return tryOrThrow(() => page.evaluate(() => {
      const iframe = selectIframe('iframe#contentAreaFrame', 'iframe#isolatedWorkArea');
      const getInputValueByLabel = (label: string) => {
        logger_info(`dati immobile: '${label}'`);
        return selectElementByXPath(`//span[text() = "${label}"]//ancestor::tr[1]//input`, iframe).value;
      };

      return [
        getInputValueByLabel('Via Immobile'),
        getInputValueByLabel('Numero Civico Immobile'),
        // getInputValueByLabel('Cap Immobile'),
        getInputValueByLabel('Località Immobile'),
        getInputValueByLabel('Provincia Immobile'),
        "Portata termica",
        getInputValueByLabel('Potenzialità massima')
      ]
      .join(' ');
    }), "non sono riuscito a recuperare l'indirizzo");
  }

  async checkForErrors(page: Page): Promise<string|null> {
    const selectIframe: any = null;
    const selectElementByXPath: any = null;

    return page.evaluate(() => {
      const iframe = selectIframe('iframe#contentAreaFrame', 'iframe[title="Nuova richiesta"]');

      const elTextEsito = selectElementByXPath('//div[contains(@id, "-icon")][@class="lsMAErrorIcon"]/parent::div/div[contains(@id, "-text")]', iframe)!;

      if (elTextEsito) {
        return elTextEsito.innerText;
      }
      return null;
    });
  }

  getScraperCodice() {
    return 'ireti-checkpdr';
  }

  getScraperTipologia() {
    return SCRAPER_TIPOLOGIA.FATTIBILITA;
  }
}
