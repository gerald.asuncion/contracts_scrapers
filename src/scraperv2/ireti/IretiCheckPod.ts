import { QueueTask } from '../../task/QueueTask';
import { ScraperResponse, WebScraper } from '../scraper';
import { SCRAPER_TIPOLOGIA } from '../ScraperOptions';
import IretiCheckPodPayload from './check/IretiCheckPodPayload';
import stepCompileForm from './helpers/stepCompileForm';
import stepGetEsito from './helpers/stepGetEsito';
import stepLogin from './helpers/stepLogin';
import stepNavigateToForm from './helpers/stepNavigateToForm';
import stepWaitIframesLoading from './helpers/stepWaitIframesLoading';
import webHelpers from './helpers/webHelpers';

@QueueTask()
export default class IretiCheckPod extends WebScraper {
  static readonly LOGIN_URL = 'https://portaledistributore.ireti.it/irj/portal#Shell-home';

  async login(payload: any): Promise<void> {
    const logger = this.childLogger;
    const { username, password } = await this.getCredenziali();
    // const { username = 'IELP0013QZRS', password = 'LupitaLupita22!' } = {};

    logger.info('step > login');

    this.checkPageClosed();

    return stepLogin(await this.p(), logger, IretiCheckPod.LOGIN_URL, username, password, payload);
  }

  async scrapeWebsite(payload: IretiCheckPodPayload): Promise<ScraperResponse> {
    const logger = this.childLogger;

    const page = await this.p();

    let stepError = await stepWaitIframesLoading(page);
    if (stepError) {
      return stepError;
    }

    const {
      pod,
      codicePratica = `SUPERMONEY${(Math.random() * 46656) | 0}`
    } = payload;

    const {
      selectIframe,
      selectElementByXPath,
      typeElement,
      waitForDefined,
      waitForDelay,
      logger_info
    } = await webHelpers(page, logger, `[codicePratica=${codicePratica}]`);

    stepError = await stepNavigateToForm(page, 'SC1 – Verifica sito contendibile');
    if (stepError) {
      return stepError;
    }

    stepError = await stepCompileForm(page, 'Pod/Matricola Misuratore', codicePratica, pod);
    if (stepError) {
      return stepError;
    }

    return stepGetEsito(page, 'pod', [
      "Via Pod",
      "Numero Pod",
      "Località Pod",
      "Cap Pod",
      "Provincia Pod",
      ["Tensione:", "Tensione"],
      ["Potenza:", "Potenza Disponibile"]
    ]);
  }

  getScraperCodice() {
    return 'ireti-checkpod';
  }

  getScraperTipologia() {
    return SCRAPER_TIPOLOGIA.FATTIBILITA;
  }
}
