import { Page } from "puppeteer";
import { Logger } from "../../../logger";
import IretiCheckPodPayload from "./IretiCheckPodPayload";

export interface IretiCheckPodResult {
  esito: string;
  indirizzo?: string;
  tensione?: string;
  potenza?: string;
}

export default async function stepRecuperoRisultatoCheckPod(page: Page, { pod, tipoContratto }: IretiCheckPodPayload, logger: Logger): Promise<IretiCheckPodResult> {
  logger.info('step > recupero risultato check');

  let result: IretiCheckPodResult = { esito: 'OK' };

  const KO_POD_NON_CONTENDIBILE = 'ko-pod non contendibile';

  try {
    logger.info('controllo se è apparso il messaggio di errore');
    await page.waitForSelector('body > div.container > form > div > div:nth-child(1) > ul', { visible: true, timeout: 2000 });
    result.esito = KO_POD_NON_CONTENDIBILE;
  } catch (ex) {
    // nessun errore posso procedere col recupero delle informazioni
    logger.info('nessun messaggio di errore, procedo al recupero dei dati');
    const societaDiVendita = await page.$eval('body > div.container > form > div > div:nth-child(7) > table:nth-child(2) > tbody > tr:nth-child(1) > td:nth-child(2) > input[type=text]', el => (el as HTMLInputElement).value);
    logger.info(`società di vendita = ${societaDiVendita}`);

    if (societaDiVendita) {
      result.esito = KO_POD_NON_CONTENDIBILE;
    } else {
      let statoFornitura = await page.$eval('body > div.container > form > div > div:nth-child(7) > table:nth-child(2) > tbody > tr:nth-child(3) > td:nth-child(2) > input[type=text]', el => (el as HTMLInputElement).value);
      logger.info(`stato fornitura = ${statoFornitura}`);

      statoFornitura = statoFornitura.toLowerCase().trim();

      if (statoFornitura === 'attiva') {
        result.esito = KO_POD_NON_CONTENDIBILE;
      } else {
        if(tipoContratto === 'subentro') {
          if (statoFornitura === 'predisposto') {
            result.esito = `ko-procedere con prima attivazione`;
          }
        } else {
          if(statoFornitura === 'staccata') {
            result.esito = `ko-procedere con subentro`;
          }
        }
      }
    }

    if (result.esito === 'OK') {
      logger.info('recupero indirizzo, tensione e potenza');

      logger.info('recupero la via fornitura');
      const via = await page.$eval('body > div.container > form > div > div:nth-child(7) > table:nth-child(2) > tbody > tr:nth-child(4) > td:nth-child(2) > input[type=text]', el => (el as HTMLInputElement).value);

      logger.info('recupero il numero civico');
      const civico = await page.$eval('body > div.container > form > div > div:nth-child(7) > table:nth-child(2) > tbody > tr:nth-child(4) > td:nth-child(4) > input[type=text]', el => (el as HTMLInputElement).value);

      logger.info('recupero la scala');
      const scala = await page.$eval('body > div.container > form > div > div:nth-child(7) > table:nth-child(3) > tbody > tr:nth-child(1) > td:nth-child(2) > input[type=text]', el => (el as HTMLInputElement).value);

      logger.info('recupero interno');
      const interno = await page.$eval('body > div.container > form > div > div:nth-child(7) > table:nth-child(3) > tbody > tr:nth-child(1) > td:nth-child(6) > input[type=text]', el => (el as HTMLInputElement).value);

      logger.info('recupero la località');
      const localita = await page.$eval('body > div.container > form > div > div:nth-child(7) > table:nth-child(3) > tbody > tr:nth-child(2) > td:nth-child(2) > input[type=text]', el => (el as HTMLInputElement).value);

      logger.info('recupero il cap');
      const cap = await page.$eval('body > div.container > form > div > div:nth-child(7) > table:nth-child(3) > tbody > tr:nth-child(2) > td:nth-child(4) > input[type=text]', el => (el as HTMLInputElement).value);

      result.indirizzo = `${via} ${civico} ${cap} ${localita}${scala ? ` scala ${scala}` : ''}${interno ? `interno ${interno}` : ''}`;

      logger.info('recupero la tensione');
      const tensione = await page.$eval('body > div.container > form > div > div:nth-child(7) > table:nth-child(3) > tbody > tr:nth-child(3) > td:nth-child(6) > input[type=text]', el => (el as HTMLInputElement).value || '');

      logger.info('recupero la potenza');
      const potenza = await page.$eval('body > div.container > form > div > div:nth-child(7) > table:nth-child(3) > tbody > tr:nth-child(4) > td:nth-child(2) > input[type=text]', el => (el as HTMLInputElement).value || '');

      result.tensione = tensione;
      result.potenza = potenza;
    }
  }

  logger.info(`pod ${pod} risultato check ${JSON.stringify(result)}`);
  return result;
}
