export default interface IretiCheckPdrPayload {
  pdr: string;
  tipoContratto?: string;
  codicePratica?: string;
};
