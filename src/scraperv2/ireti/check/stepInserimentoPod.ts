import waitForSelectorAndClick from "../../../browser/page/waitForSelectorAndClick";
import { Page } from "puppeteer";
import { Logger } from "../../../logger";
import waitForNavigation from "../../../browser/page/waitForNavigation";

export default async function stepInserimentoPod(page: Page, pod: string, logger: Logger): Promise<void> {
  logger.info('step > inserimento pod');

  logger.info(`inserisco pod ${pod}`);
  // await waitForSelectorAndType(page, 'body > div.container > form > div > div.tabellaInterna > table > tbody > tr:nth-child(1) > td:nth-child(2) > input', pod);
  await page.evaluate((selectedPod) => {
    const el = document.querySelector<HTMLInputElement>('body > div.container > form > div > div.tabellaInterna > table > tbody > tr:nth-child(1) > td:nth-child(2) > input');
    if(el) {
      el.value = selectedPod;
    }
  }, pod);

  logger.info('clicco sul pulsante `interroga`');
  await Promise.all([
    waitForSelectorAndClick(page, 'body > div.container > form > div > div.tabellaInterna > table > tbody > tr:nth-child(2) > td > input'),
    waitForNavigation(page)
  ]);
}
