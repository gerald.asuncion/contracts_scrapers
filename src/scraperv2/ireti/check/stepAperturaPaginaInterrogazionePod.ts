import waitForSelectorAndClick from "../../../browser/page/waitForSelectorAndClick";
import waitForVisible from "../../../browser/page/waitForVisible";
import { Page } from "puppeteer";
import { Logger } from "../../../logger";
import waitForNavigation from "../../../browser/page/waitForNavigation";

export default async function stepAperturaPaginaInterrogazionePod(page: Page, logger: Logger): Promise<void> {
  logger.info('step > apertura pagina interrogazione pod');

  logger.info('attendo caricamento home page');
  await page.waitForSelector('form[name="genericForm"]');

  logger.info('clicco su `Nuova Prestazione`');
  await waitForSelectorAndClick(page, '#jGM_tile_jGlide_001_1 > div.jGM_content > a:nth-child(1)');

  logger.info('attendo apertura menu');
  await waitForVisible(page, '#jGM_tile_jGlide_001_2');

  logger.info('clicco su `Interrogazione`');
  await waitForSelectorAndClick(page, '#jGM_tile_jGlide_001_2 > div.jGM_content > a:nth-child(3)');

  logger.info('attendo apertura menu');
  await waitForVisible(page, '#jGM_tile_jGlide_001_3');

  logger.info('clicco su `POD`');
  await Promise.all([
    waitForSelectorAndClick(page, '#jGM_tile_jGlide_001_3 > div.jGM_content > a:nth-child(3)'),
    waitForNavigation(page)
  ]);
  // /CNRGWebDistributore/richiestanuovaoperazione.do?todo=interrogaPod
}
