export default interface IretiCheckPodPayload {
  pod: string;
  tipoContratto: string;
  codicePratica?: string;
};
