/**
 * @openapi
 *
 * /scraper/v2/eni/forma-giuridica:
 *  post:
 *    tags:
 *      - v2
 *    description: Restiutisce la lista di forme giuridiche prese dal portale di Eni.
 *    consume:
 *      - application/json
 *    produces:
 *      - application/json
 *    responses:
 *      200:
 *        description: SuccessResponse
 *    parameters:
 *      - in: body
 *        description: non serve.
 *        schema:
 *          type: object
 */
