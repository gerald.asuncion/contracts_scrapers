import { Page } from "puppeteer";
import getElementText from "../../../browser/page/getElementText";
import waitForSelectorAndClickEvaluated from "../../../browser/page/waitForSelectorAndClickEvaluated";
import waitForTimeout from "../../../browser/page/waitForTimeout";
import waitForVisible from "../../../browser/page/waitForVisible";
import timeoutPromise from "../../timeoutPromise";

const ALERT_CONTAINER_SELECTOR = '#pop_UpConsensiPrivacy .sectionBox';
const OK_BTN_SELECTOR = `${ALERT_CONTAINER_SELECTOR} input[value="Continua"]`;

const ASPETTA_ESITO_TIMEOUT = 3000;

async function aspettaCheAppaiaEsito(page: Page) {
  let alertMsg = await getElementText(page, `${ALERT_CONTAINER_SELECTOR} div:nth-child(2)`, 'textContent') as string;
  alertMsg = alertMsg.replace(/\n/g, '').replace(/\t/g, '').trim();
  if (alertMsg.includes('Invio richiesta in corso')) {
    await waitForTimeout(page, 100);
    alertMsg = await aspettaCheAppaiaEsito(page);
  }
  return alertMsg;
}

export default async function checkPrivacyChange(page: Page) {
  try {
    await waitForVisible(page, ALERT_CONTAINER_SELECTOR);

    let alertMsg = await timeoutPromise<string>('Timeout Alert Indirizzo', ASPETTA_ESITO_TIMEOUT)(aspettaCheAppaiaEsito(page)) as string;

    if (['Attenzione, hai modificato i valori della privacy. Desideri continuare?'].includes(alertMsg)) {
      await waitForSelectorAndClickEvaluated(page, OK_BTN_SELECTOR, {timeout: 3000});
    }
  } catch (ignored) {}
}
