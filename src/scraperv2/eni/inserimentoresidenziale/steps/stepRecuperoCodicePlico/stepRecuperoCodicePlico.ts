import { Page } from "puppeteer";
import getElementText from "../../../../../browser/page/getElementText";
import delay from "../../../../../utils/delay";

export default async function stepRecuperoCodicePlico(page: Page): Promise<string> {
  const text = await getElementText(page, '#codiceProposta', 'textContent') as string;

  return text.trim();
}
