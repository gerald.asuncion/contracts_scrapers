import { EniResidenzialeInserimentoPayload } from "../../../types/EniResidenzialeInserimentoPayload";
import waitForVisible from "../../../../../browser/page/waitForVisible";
import waitForSelectorAndType from "../../../../../browser/page/waitForSelectorAndType";
import waitForSelectorAndClick from "../../../../../browser/page/waitForSelectorAndClick";
import waitForNavigation from "../../../../../browser/page/waitForNavigation";
import waitForSelectorAndTypeEvaluated from "../../../../../browser/page/waitForSelectorAndTypeEvaluated";
import { Page } from "puppeteer";

export default async function stepAggiornamentoMail(page: Page, codicePlico: string, email: EniResidenzialeInserimentoPayload['email']) {
  const [homeAnchorElement] = await page.$x('//a[contains(., "Torna alla Home page")]');
  const url = await (await homeAnchorElement.getProperty('href')).jsonValue<string>();

  await page.goto(url, { waitUntil: 'networkidle2' });

  await waitForVisible(page, '#divSingolo > div:nth-child(8)');

  await page.goto('https://webapp.eni.com/domm-web-app/it/eni/domm/switchin/web/cc/goToCercaDoubleOptInContratti.do', { waitUntil: 'networkidle2' });

  await waitForSelectorAndType(page, 'input[id="codiceProposta"]', codicePlico);

  await Promise.all([
    waitForNavigation(page, { timeout: 90000 }),
    waitForSelectorAndClick(page, 'input[value="Cerca"]')
  ]);

  await waitForVisible(page, 'table[class="datagrid"]');

  await waitForSelectorAndClick(page, 'input[title="Reinvia email/SMS"]');

  await waitForVisible(page, '#popupReinviaEmailDoubleOptIn');

  await waitForVisible(page, 'input[id="emailReinvioDoubleOptIn"]');

  await waitForSelectorAndTypeEvaluated(page, 'input[id="emailReinvioDoubleOptIn"]', '');
  await waitForSelectorAndType(page, 'input[id="emailReinvioDoubleOptIn"]', email);

  await waitForSelectorAndClick(page, 'input[type="button"][value*="Reinvia"]');

  try {
    await waitForVisible(page, '#popupReinviaEmailDoubleOptIn_updateAnagDiv');
    await waitForSelectorAndClick(page, '#popupReinviaEmailDoubleOptIn_updateAnagDiv input[value*="Sì"]');
  } catch (ex) {
    // modale di conferma non apparsa
    // nulla da fare
  }

  await waitForVisible(page, '#popupReinviaEmailDoubleOptIn_successDiv', {timeout: 60000});
  await waitForSelectorAndClick(page, '#popupReinviaEmailDoubleOptIn_successDiv input[value*="OK"]', {timeout: 60000});
}
