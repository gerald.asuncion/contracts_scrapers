import { Page } from "puppeteer";
import waitForNavigation from "../../../../../browser/page/waitForNavigation";
import waitForSelectorAndClick from "../../../../../browser/page/waitForSelectorAndClick";
import { NoSubmitPayload } from "../../../../../payloads/types";
import checkNoSubmit from "../../../../../utils/checkNoSubmit";
import extractErrorMessage from "../../../../../utils/extractErrorMessage";
import { EniResidenzialeInserimentoPayload } from "../../../types/EniResidenzialeInserimentoPayload";
import checkAlertConferma from "./checkAlertConferma";

export default async function stepInvioPratica(page: Page, payload: EniResidenzialeInserimentoPayload) {
  checkNoSubmit(payload as NoSubmitPayload);

  try {
    await Promise.all([
      waitForNavigation(page, { timeout: 60000 }),
      waitForSelectorAndClick(page, 'input[value="Invia Double Opt-In"]', { timeout: 120000 })
    ]);
  } catch (ex) {
    const exMsg = extractErrorMessage(ex);

    if (exMsg.includes('Navigation timeout')) {
      await checkAlertConferma(page);
    }
  }
}
