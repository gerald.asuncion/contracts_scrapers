import { Page } from "puppeteer";
import getElementText from "../../../../../browser/page/getElementText";
import waitForNavigation from "../../../../../browser/page/waitForNavigation";
import waitForSelectorAndClickEvaluated from "../../../../../browser/page/waitForSelectorAndClickEvaluated";
import waitForVisible from "../../../../../browser/page/waitForVisible";

export const ALERT_CONTAINER_SELECTOR = '.blockUI.blockMsg.blockPage';
export const CONFERMA_BTN_SELECTOR = `${ALERT_CONTAINER_SELECTOR} input[value="Conferma Double opt-in"]`;

export default async function checkAlertConferma(page: Page) {
  await waitForVisible(page, ALERT_CONTAINER_SELECTOR, { timeout: 60000 });

  let alertMsg = await getElementText(page, ALERT_CONTAINER_SELECTOR, 'textContent') as string;
  alertMsg = alertMsg.replace(/\n/g, '').replace(/\t/g, '').trim();

  if (alertMsg.includes('Si desidera confermare il double opt-in?')) {
    await Promise.all([
      waitForNavigation(page),
      waitForSelectorAndClickEvaluated(page, CONFERMA_BTN_SELECTOR, { timeout: 60000 })
    ]);
  } else {
    throw new Error(alertMsg);
  }
}
