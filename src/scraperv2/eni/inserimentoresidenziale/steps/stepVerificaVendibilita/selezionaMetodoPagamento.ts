import { Page } from "puppeteer";
import waitForSelectorAndClick from "../../../../../browser/page/waitForSelectorAndClick";
import tryOrThrow from "../../../../../utils/tryOrThrow";
import { EniResidenzialeInserimentoPayload } from "../../../types/EniResidenzialeInserimentoPayload";

const SELECTOR_MAP: Record<EniResidenzialeInserimentoPayload['metodoPagamento'], string> = {
  'bollettino': 'input[id$="pagamento_fornitura_bb"]',
  'domiciliazione': 'input[id$="pagamento_fornitura_cc"]'
}

export default async function selezionaMetodoPagamento(page: Page, metodoPagamento: EniResidenzialeInserimentoPayload['metodoPagamento']) {
  return tryOrThrow(
    () => waitForSelectorAndClick(page, SELECTOR_MAP[metodoPagamento]),
    'non sono riuscito a selezionare il metodo di pagamento:'
  )
}
