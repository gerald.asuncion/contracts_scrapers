import { Page } from "puppeteer";
import isVisible from "../../../../../browser/page/isVisible";
import waitForNavigation from "../../../../../browser/page/waitForNavigation";
import waitForSelectorAndClickEvaluated from "../../../../../browser/page/waitForSelectorAndClickEvaluated";
import waitForSelectorAndType from "../../../../../browser/page/waitForSelectorAndType";
import waitForVisible from "../../../../../browser/page/waitForVisible";
import tryOrThrow from "../../../../../utils/tryOrThrow";
import selectByFindOption from "../../../inserimentocontratto/selectByFindOption";
import { EniResidenzialeInserimentoPayload } from "../../../types/EniResidenzialeInserimentoPayload";
import checkAlertIndirizzo from "../../checkAlertIndirizzo";

const BTN_VERIFICA_INDIRIZZO_SELECTOR = 'input[value="Verifica indirizzo"]';
export default async function inserisciIndirizzo(page: Page, payload: EniResidenzialeInserimentoPayload) {
  await waitForVisible(page, '#divViaCliente');

  await tryOrThrow(
    () => selectByFindOption(page, 'select[id$="toponomastica_fattura"]', payload.attivazioneToponimo, ''),
    'non sono riuscito a selezionare il toponimo:'
  );

  await tryOrThrow(
    () => waitForSelectorAndType(page, 'input[id$="indirizzo_fattura"]', payload.attivazioneIndirizzo),
    "non sono riuscito ad inserire l'indirizzo:"
  );

  await tryOrThrow(
    () => waitForSelectorAndType(page, 'input[id$="civico_fattura"]', payload.attivazioneCivico),
    "non sono riuscito ad inserire il civico:"
  );

  if (payload.attivazioneScala) {
    await tryOrThrow(
      () => waitForSelectorAndType(page, 'input[id$="scala_fattura"]', payload.attivazioneScala as string),
      'non sono riuscito ad inserire la scala:'
    );
  }

  await tryOrThrow(
    async () => Promise.all([
      waitForNavigation(page),
      selectByFindOption(page, 'select[id$="provincia_fattura"]', payload.attivazioneProvincia, '')
    ]),
    'non sono riuscito a selezionare la provincia:'
  );

  await tryOrThrow(
    async () => Promise.all([
      waitForNavigation(page),
      selectByFindOption(page, 'select[id$="comune_fattura"]', payload.attivazioneComune, '')
    ]),
    'non sono riuscito a selezionare il comune:'
  );

  await tryOrThrow(
    () => selectByFindOption(page, 'select[id$="cap_fattura"]', payload.attivazioneCap, '', true),
    'non sono riuscito a selezionare il cap:'
  );

  if (await isVisible(page, BTN_VERIFICA_INDIRIZZO_SELECTOR)) {
    await waitForSelectorAndClickEvaluated(page, BTN_VERIFICA_INDIRIZZO_SELECTOR);

    // attendo apparizione modale esito verifica indirizzo
    await checkAlertIndirizzo(page);
  }
}
