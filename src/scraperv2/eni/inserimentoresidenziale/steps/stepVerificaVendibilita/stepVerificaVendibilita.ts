import { Page } from "puppeteer";
import isVisible from "../../../../../browser/page/isVisible";
import waitForNavigation from "../../../../../browser/page/waitForNavigation";
import waitForSelectorAndClick from "../../../../../browser/page/waitForSelectorAndClick";
import waitForSelectorAndType from "../../../../../browser/page/waitForSelectorAndType";
import extractErrorMessage from "../../../../../utils/extractErrorMessage";
import tryOrThrow from "../../../../../utils/tryOrThrow";
import selectByFindOption from "../../../inserimentocontratto/selectByFindOption";
import { EniResidenzialeInserimentoPayload } from "../../../types/EniResidenzialeInserimentoPayload";
import checkValidationErrors, { VALIDATION_SUCCESS_TO_ERRORS } from "../../checkValidationErrors";
import inserisciDatiCliente from "./inserisciDatiCliente";
import inserisciIndirizzo from "./inserisciIndirizzo";
import selezionaDecorrenzaImmediata from "./selezionaDecorrenzaImmediata";
import selezionaMetodoPagamento from "./selezionaMetodoPagamento";
import selezionaTipoFornitura from "./selezionaTipoFornitura";

const TIPOLOGIAUSO_SELECTOR = 'select[id$="tipologiaUsoPower"]';
export default async function stepVerificaVendibilita(page: Page, payload: EniResidenzialeInserimentoPayload): Promise<[null, string[]] | [boolean, null]> {
  await selezionaDecorrenzaImmediata(page, payload.decorrenzaImmediata);

  await Promise.all([
    waitForNavigation(page),
    waitForSelectorAndClick(page, 'input[value="Avanti"]')
  ]);

  await selezionaTipoFornitura(page, payload.tipoFornitura);

  await inserisciDatiCliente(page, payload);

  if (payload.pdr) {
    await tryOrThrow(
      () => waitForSelectorAndType(page, 'input[id$="codicePDR_fornitura"]', payload.pdr as string),
      'non sono riuscito a inserire il PDR:'
    );
  }

  if (payload.pod) {
    await tryOrThrow(
      () => waitForSelectorAndType(page, 'input[id$="codicePOD_fornitura"]', payload.pod as string),
      'non sono riuscito a inserire il POD:'
    );
  }

  // TOOD: appare loading
  await Promise.all([
    waitForNavigation(page),
    waitForSelectorAndClick(page, 'input[value="Verifica"]')
  ]);

  let errors: string[] = await checkValidationErrors(page);

  if (errors.length) {
    return [null, errors];
  }

  await inserisciIndirizzo(page, payload);

  await selezionaMetodoPagamento(page, payload.metodoPagamento);

  if (await isVisible(page, TIPOLOGIAUSO_SELECTOR)) {
    await tryOrThrow(
      () => selectByFindOption(page, TIPOLOGIAUSO_SELECTOR, payload.tipologiaUso as string, ''),
      'non sono riuscito a selezionare la tipologia di uso:'
    );
  }

  // TODO: appare loading
  try {
    await Promise.all([
      waitForNavigation(page, { timeout: 120000 }),
      waitForSelectorAndClick(page, 'input[type="submit"][value="Verifica"]')
    ]);
  } catch (ex) {
    const exMsg = extractErrorMessage(ex);

    if (exMsg.includes('Navigation timeout')) {
      errors = await checkValidationErrors(page);

      if (errors.length) {
        return [null, errors];
      }
    }

    throw ex;
  }

  errors = await checkValidationErrors(page);

  if (errors.length) {
    return [null, errors];
  }

  return [true, null];
}
