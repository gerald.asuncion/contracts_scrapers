import { Page } from "puppeteer";
import waitForSelectorAndClick from "../../../../../browser/page/waitForSelectorAndClick";
import tryOrThrow from "../../../../../utils/tryOrThrow";

export default async function selezionaDecorrenzaImmediata(page: Page, decorrenzaImmediata: boolean) {
  if (decorrenzaImmediata) {
    await tryOrThrow(
      () => waitForSelectorAndClick(page, 'input[id$="decorrenzaImmediata"]'),
      'non sono riuscito ad abilitare la decorrenza immediata:'
    );
  }
}
