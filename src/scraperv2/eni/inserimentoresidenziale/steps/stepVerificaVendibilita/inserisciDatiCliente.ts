import { Page } from "puppeteer";
import waitForSelectorAndType from "../../../../../browser/page/waitForSelectorAndType";
import tryOrThrow from "../../../../../utils/tryOrThrow";
import { EniResidenzialeInserimentoPayload } from "../../../types/EniResidenzialeInserimentoPayload";

export default async function inserisciDatiCliente(page: Page, payload: EniResidenzialeInserimentoPayload) {
  await tryOrThrow(
    () => waitForSelectorAndType(page, 'input[id$="cliente_nome"]', payload.nome),
    'non sono riuscito a inserire il nome del cliente:'
  );

  await tryOrThrow(
    () => waitForSelectorAndType(page, 'input[id$="cliente_cognome"]', payload.cognome),
    'non sono riuscito a inserire il cognome del cliente:'
  );

  await tryOrThrow(
    () => waitForSelectorAndType(page, 'input[id$="cliente_codFiscale"]', payload.codiceFiscale),
    'non sono riuscito a inserire il codice fiscale del cliente:'
  );
}
