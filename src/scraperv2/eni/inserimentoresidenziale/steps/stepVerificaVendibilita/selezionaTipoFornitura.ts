import { Page } from "puppeteer";
import waitForSelectorAndClick from "../../../../../browser/page/waitForSelectorAndClick";
import tryOrThrow from "../../../../../utils/tryOrThrow";
import { EniResidenzialeInserimentoPayload } from "../../../types/EniResidenzialeInserimentoPayload";

const TIPOFORNITURA_MAP: Record<string, string> = {
  'gas': 'GAS',
  'luce': 'POWER',
  'dual': 'DUAL'
};

const getSelector = (tipoFornitura: EniResidenzialeInserimentoPayload['tipoFornitura']) => `label[for$="${TIPOFORNITURA_MAP[tipoFornitura]}"]`;

export default async function selezionaTipoFornitura(page: Page, tipoFornitura: EniResidenzialeInserimentoPayload['tipoFornitura']) {
  return tryOrThrow(
    () => waitForSelectorAndClick(page, getSelector(tipoFornitura)),
    `non sono riuscito a selezionare il tipo di fornitura ${tipoFornitura}:`
  )
}
