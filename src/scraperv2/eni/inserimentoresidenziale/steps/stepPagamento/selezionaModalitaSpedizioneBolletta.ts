import { Page } from "puppeteer";
import tryOrThrow from "../../../../../utils/tryOrThrow";
import selectByFindOption from "../../../inserimentocontratto/selectByFindOption";
import { EniResidenzialeInserimentoPayload } from "../../../types/EniResidenzialeInserimentoPayload";

const SELECTOR_ID_MAP: Record<EniResidenzialeInserimentoPayload['tipoFornitura'], string> = {
  'gas': 'canaleInoltroGas',
  'luce': 'canaleInoltroPow',
  'dual': 'canaleInoltro'
};

const getSelector = (tipoFornitura: EniResidenzialeInserimentoPayload['tipoFornitura']) => `select[id$="${SELECTOR_ID_MAP[tipoFornitura]}"]`;

export default async function selezionaModalitaSpedizioneBolletta(page: Page, payload: EniResidenzialeInserimentoPayload) {
  await tryOrThrow(
    () => selectByFindOption(page, getSelector(payload.tipoFornitura), payload.modalitaSpedizioneBolletta, ''),
    'non sono riuscito a selezionare il metodo di pagamento:'
  );
}
