import capitalize from "lodash/capitalize";
import { Page } from "puppeteer";
import waitForNavigation from "../../../../../browser/page/waitForNavigation";
import waitForSelectorAndClick from "../../../../../browser/page/waitForSelectorAndClick";
import waitForSelectorAndClickEvaluated from "../../../../../browser/page/waitForSelectorAndClickEvaluated";
import waitForVisible from "../../../../../browser/page/waitForVisible";
import waitForXPathAndClick from "../../../../../browser/page/waitForXPathAndClick";
import extractErrorMessage from "../../../../../utils/extractErrorMessage";
import tryOrThrow from "../../../../../utils/tryOrThrow";
import { EniResidenzialeInserimentoPayload } from "../../../types/EniResidenzialeInserimentoPayload";
import inserisciDatiPagamento from "./inserisciDatiPagamento";
import selezionaModalitaSpedizioneBolletta from "./selezionaModalitaSpedizioneBolletta";

export default async function stepPagamento(page: Page, payload: EniResidenzialeInserimentoPayload): Promise<[null, string[]] | [boolean, null]> {
  await selezionaModalitaSpedizioneBolletta(page, payload);

  await tryOrThrow(
    () => Promise.all([
      waitForNavigation(page),
      waitForXPathAndClick(page, `//label[contains(., "${capitalize(payload.metodoPagamento)}")]`)
    ]),
    'non sono riuscito a selezionare `come desideri pagare le bollette?`:'
  );

  if (payload.metodoPagamento !== 'bollettino') {
    const [_, validationErrors] = await inserisciDatiPagamento(page, payload);

    if (validationErrors && validationErrors.length) {
      return [null, validationErrors];
    }
  }

  if (payload.eniWebbolletta) {
    await tryOrThrow(
      () => waitForSelectorAndClickEvaluated(page, 'input[id$="webollettaAttivata"]'),
      'non sono riuscito a cliccare eni web bolletta:'
    );
  }

  await tryOrThrow(
    () => waitForSelectorAndClick(page, `input[id*="venditaExtraCommodity"][value="${payload.extraCommodity === 'si'}"]`),
    'non sono riuscito a selezionare l\'extra commodity:'
  );

  await tryOrThrow(
    () => waitForSelectorAndClick(page, `input[id*="promoEni"][value="${payload.consensoPromoEni === 'si'}"]`),
    'non sono riuscito ad impostare il consenso 1:'
  );

  await tryOrThrow(
    () => waitForSelectorAndClick(page, `input[id*="analisiMercato"][value="${payload.consensoAnalisiMercato === 'si'}"]`),
    'non sono riuscito ad impostare il consenso 2:'
  );

  await tryOrThrow(
    () => waitForSelectorAndClick(page, `input[id*="promoNonEni"][value="${payload.consensoPromoNonEni === 'si'}"]`),
    'non sono riuscito ad impostare il consenso 3:'
  );

  try {
    await Promise.all([
      waitForNavigation(page),
      waitForSelectorAndClick(page, 'input[value="Invia"]')
    ]);
  } catch (ex) {
    const exMsg = extractErrorMessage(ex);

    if (exMsg.includes('Navigation timeout')) {
      try {
        const modalSelector = 'div[id="pop_UpConsensiPrivacy"]';
        await waitForVisible(page, modalSelector);
        await Promise.all([
          waitForNavigation(page),
          waitForSelectorAndClick(page, `${modalSelector} input[value="Continua"]`)
        ]);
      } catch (exContinua) {
        // modale non apparsa
        // nulla da fare
        if (extractErrorMessage(exContinua).includes('Navigation timeout')) {
          throw exContinua;
        }
      }
    }
  }

  return [true, null];
}
