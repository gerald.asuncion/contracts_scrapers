import { Page } from "puppeteer";
import waitForNavigation from "../../../../../browser/page/waitForNavigation";
import waitForSelectorAndClick from "../../../../../browser/page/waitForSelectorAndClick";
import waitForSelectorAndType from "../../../../../browser/page/waitForSelectorAndType";
import tryOrThrow from "../../../../../utils/tryOrThrow";
import { EniResidenzialeInserimentoPayload } from "../../../types/EniResidenzialeInserimentoPayload";
import checkValidationErrors from "../../checkValidationErrors";

export default async function inserisciDatiPagamento(page: Page, payload: EniResidenzialeInserimentoPayload): Promise<[null, string[]] | [boolean, null]> {
  await tryOrThrow(
    () => waitForSelectorAndType(page, 'input[id$="nomePagamento_fornitura"]', payload.intestatarioContoNome as string),
    'non sono riuscito ad inserire il nome per il metodo di pagament:'
  );

  await tryOrThrow(
    () => waitForSelectorAndType(page, 'input[id$="cognomePagamento_fornitura"]', payload.intestatarioContoCognome as string),
    'non sono riuscito ad inserire il cognome per il metodo di pagament:'
  );

  await tryOrThrow(
    () => waitForSelectorAndType(page, 'input[id$="codiceFiscalePagamento_fornitura"]', payload.intestatarioContoCodiceFiscale as string),
    'non sono riuscito ad inserire il codice fiscale per il metodo di pagament:'
  );

  await tryOrThrow(
    () => waitForSelectorAndType(page, 'input[id$="iban_fornitura"]', payload.iban as string),
    "non sono riuscito ad inserire l'iban per il metodo di pagament:"
  );

  await tryOrThrow(
    () => Promise.all([
      waitForNavigation(page),
      waitForSelectorAndClick(page, 'input[id$="buttonCheckIban"][value="Verifica"]')
    ]),
    "non sono riuscito a cliccare per la verific dell'iban:"
  );

  const errors = await checkValidationErrors(page);

  if (errors.length) {
    return [null, errors];
  }

  return [true, null];
}
