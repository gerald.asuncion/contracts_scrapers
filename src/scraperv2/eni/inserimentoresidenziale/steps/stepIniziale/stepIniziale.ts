import { Page } from "puppeteer";
import waitForNavigation from "../../../../../browser/page/waitForNavigation";
import waitForSelectorAndClick from "../../../../../browser/page/waitForSelectorAndClick";
import { EniResidenzialeInserimentoPayload } from "../../../types/EniResidenzialeInserimentoPayload";
import selezionaCodiceAgente from "../../../inserimentocontratto/selezionaCodiceAgente";
import selezionaSegmentoCliente from "../../../inserimentocontratto/selezionaSegmentoCliente";
import selezionaCanale from "./selezionaCanale";
import selezionaOffertaPartnershipFastweb from "./selezionaOffertaPartnershipFastweb";

export default async function stepIniziale(page: Page, payload: EniResidenzialeInserimentoPayload) {
  await selezionaCodiceAgente(page, payload.codiceAgente);

  await selezionaSegmentoCliente(page, payload.segmentoCliente);

  await selezionaOffertaPartnershipFastweb(page, payload.offertaPartnershipFastweb);

  await Promise.all([
    waitForNavigation(page),
    waitForSelectorAndClick(page, 'input[name="actionOverride:selezionaSegmento"]')
  ]);

  await selezionaCanale(page, payload.canale);

  await Promise.all([
    waitForNavigation(page),
    waitForSelectorAndClick(page, 'input[value="Invia"]')
  ]);
}
