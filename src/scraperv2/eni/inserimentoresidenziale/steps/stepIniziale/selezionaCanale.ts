import { Page } from "puppeteer";
import tryOrThrow from "../../../../../utils/tryOrThrow";
import selectByFindOption from "../../../inserimentocontratto/selectByFindOption";

// seleziona.codice_canale
export default async function selezionaCanale(page: Page, canale: string) {
  return tryOrThrow(
    () => selectByFindOption(page, 'select[id$="codice_canale"]', canale, 'codice canale'),
    'non sono riuscito a selezionare il canale:'
  )
}
