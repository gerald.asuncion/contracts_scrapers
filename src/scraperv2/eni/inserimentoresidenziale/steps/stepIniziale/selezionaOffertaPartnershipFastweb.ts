import { Page } from "puppeteer";
import waitForSelectorAndClick from "../../../../../browser/page/waitForSelectorAndClick";
import tryOrThrow from "../../../../../utils/tryOrThrow";
import { EniResidenzialeInserimentoPayload } from "../../../types/EniResidenzialeInserimentoPayload";

const getSelector = (yesOrNo: EniResidenzialeInserimentoPayload['offertaPartnershipFastweb']) => `input[id$="partnershipFastweb${yesOrNo === 'si' ? 'Si' : 'No'}"]`;

export default async function selezionaOffertaPartnershipFastweb(page: Page, yesOrNo: EniResidenzialeInserimentoPayload['offertaPartnershipFastweb']) {
  return tryOrThrow(
    () => waitForSelectorAndClick(page, getSelector(yesOrNo)),
    `non sono riuscito a impostare l'offerta partnership con Fastweb con il valore ${yesOrNo}:`
  );
}
