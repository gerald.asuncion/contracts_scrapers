import { Page } from "puppeteer";
import waitForXPathAndClick from "../../../../../browser/page/waitForXPathAndClick";
import tryOrThrow from "../../../../../utils/tryOrThrow";

const getSelector = (offerta: string) => `//div[contains(@class, "radioButtonDescrizione")][contains(., "${offerta}")]/parent::div//div[contains(@class, "radioButtonSelettore")]//input`;

export default async function selezionaOfferta(page: Page, offerta: string) {
  await tryOrThrow(
    () => waitForXPathAndClick(page, getSelector(offerta)),
    "non sono riuscito a selezionare l'offerta:"
  );
}
