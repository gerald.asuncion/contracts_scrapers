import { Page } from "puppeteer";
import { getInputText } from "../../../../../browser/page/getElementText";
import isEnabled from "../../../../../browser/page/isEnabled";
import waitForSelectorAndType from "../../../../../browser/page/waitForSelectorAndType";
import waitForSelectorAndTypeEvaluated from "../../../../../browser/page/waitForSelectorAndTypeEvaluated";
import tryOrThrow from "../../../../../utils/tryOrThrow";
import { EniResidenzialeInserimentoPayload } from "../../../types/EniResidenzialeInserimentoPayload";

export default async function inserisciContatti(page: Page, payload: EniResidenzialeInserimentoPayload) {
  await tryOrThrow(
    async () => {
      const selector = 'input[id$="cellulare"]';
      await waitForSelectorAndTypeEvaluated(page, selector, '');
      await waitForSelectorAndType(page, selector, payload.cellulare);
    },
    'non sono riuscito ad inserire il cellulare:'
  );

  const needToUpdateEmail = await tryOrThrow<boolean>(
    async () => {
      const selector = 'input[id$="email_principale"]';
      const currentEmail = await getInputText(page, selector);
      const needToUpdateEmail = !!currentEmail && currentEmail != payload.email;
      const needToInsert = !currentEmail || !needToUpdateEmail;
      // console.log({ currentEmail, needToInsert, needToUpdateEmail })

      if (await isEnabled(page, selector)) {
        if (needToInsert) {
          await waitForSelectorAndTypeEvaluated(page, selector, '');
          await waitForSelectorAndType(page, selector, payload.email);
        }
      }

      return needToUpdateEmail;
    },
    'non sono riuscito ad inserire la mail:'
  );

  return needToUpdateEmail;

  // await tryOrThrow(
  //   async () => {
  //     const selector = 'input[id$="email_principale"]';
  //     // await waitForSelectorAndTypeEvaluated(page, selector, '');
  //     // await waitForSelectorAndType(page, selector, payload.email);
  //     await waitForSelectorAndTypeEvaluated(page, selector, payload.email);
  //   },
  //   'non sono riuscito ad inserire la data di emissione del documento:'
  // );
}
