import { Page } from "puppeteer";
import waitForNavigation from "../../../../../browser/page/waitForNavigation";
import waitForSelectorAndClick from "../../../../../browser/page/waitForSelectorAndClick";
import { EniResidenzialeInserimentoPayload } from "../../../types/EniResidenzialeInserimentoPayload";
import checkValidationErrors from "../../checkValidationErrors";
import inserisciContatti from "./inserisciContatti";
import inserisciDatiDocumento from "./inserisciDatiDocumento";
import selezionaOfferta from "./selezionaOfferta";

export type StepDatiClienteResult = {
  needToUpdateEmail: boolean;
};

export default async function stepDatiCliente(page: Page, payload: EniResidenzialeInserimentoPayload): Promise<[null, string[]] | [StepDatiClienteResult, null]> {
  await selezionaOfferta(page, payload.offerta);

  await inserisciDatiDocumento(page, payload);

  const needToUpdateEmail = await inserisciContatti(page, payload);

  await Promise.all([
    waitForNavigation(page),
    waitForSelectorAndClick(page, 'input[value="Avanti"]')
  ]);

  const errors = await checkValidationErrors(page);

  if (errors.length) {
    return [null, errors];
  }

  return [{ needToUpdateEmail }, null];
}
