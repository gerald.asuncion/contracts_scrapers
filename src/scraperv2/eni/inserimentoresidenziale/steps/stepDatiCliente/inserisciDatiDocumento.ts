import { Page } from "puppeteer";
import waitForSelectorAndType from "../../../../../browser/page/waitForSelectorAndType";
import waitForSelectorAndTypeEvaluated from "../../../../../browser/page/waitForSelectorAndTypeEvaluated";
import tryOrThrow from "../../../../../utils/tryOrThrow";
import selectByFindOption from "../../../inserimentocontratto/selectByFindOption";
import { EniResidenzialeInserimentoPayload } from "../../../types/EniResidenzialeInserimentoPayload";

export default async function inserisciDatiDocumento(page: Page, payload: EniResidenzialeInserimentoPayload) {
  await tryOrThrow(
    async () => {
      const selector = 'input[id$="numero_documento"]';
      await waitForSelectorAndTypeEvaluated(page, selector, '');
      await waitForSelectorAndType(page, selector, payload.documentoNumero);
    },
    'non sono riuscito ad inserire il numero di documento:'
  );

  await tryOrThrow(
    () => selectByFindOption(page, 'select[id$="tipo_documento"]', payload.documentoTipo, ''),
    'non sono riuscito a selezionare il tipo di documento:'
  );

  await tryOrThrow(
    async () => {
      const selector = 'input[id$="dataEmissione"]';
      await waitForSelectorAndTypeEvaluated(page, selector, '');
      await waitForSelectorAndType(page, selector, payload.documentoDataEmissione);
    },
    'non sono riuscito ad inserire la data di emissione del documento:'
  );
}
