import { Page } from "puppeteer";
import waitForNavigation from "../../../../../browser/page/waitForNavigation";
import waitForSelectorAndClickEvaluated from "../../../../../browser/page/waitForSelectorAndClickEvaluated";
import waitForSelectorAndType from "../../../../../browser/page/waitForSelectorAndType";
import tryOrThrow from "../../../../../utils/tryOrThrow";
import selectByFindOption from "../../../inserimentocontratto/selectByFindOption";
import { EniResidenzialeInserimentoPayload } from "../../../types/EniResidenzialeInserimentoPayload";
import checkAlertIndirizzo from "../../checkAlertIndirizzo";

export default async function inserisciIndirizzoEsazione(page: Page, payload: EniResidenzialeInserimentoPayload) {
  await tryOrThrow(
    () => selectByFindOption(page, 'select[id$="toponomastica_esazione"]', payload.esazioneToponimo as string, ''),
    "non sono riuscito a selezionare il toponimo di esazione:"
  );

  await tryOrThrow(
    () => waitForSelectorAndType(page, 'input[id$="indirizzo_esazione"]', payload.esazioneIndirizzo as string),
    "non sono riuscito ad inserire l'indirizzo di esazione:"
  );

  await tryOrThrow(
    () => waitForSelectorAndType(page, 'input[id$="civico_esazione"]', payload.esazioneCivico as string),
    "non sono riuscito ad inserire il civico di esazione:"
  );

  if (payload.esazioneScala) {
    await tryOrThrow(
      () => waitForSelectorAndType(page, 'input[id$="scala_esazione"]', payload.esazioneScala as string),
      "non sono riuscito ad inserire la scala di esazione:"
    );
  }

  await tryOrThrow(
    () => Promise.all([
      waitForNavigation(page),
      selectByFindOption(page, 'select[id$="provincia_esazione"]', payload.esazioneProvincia as string, '', true)
    ]),
    'non sono riuscito a selezionare la provincia di esazione:'
  );

  await tryOrThrow(
    () => Promise.all([
      waitForNavigation(page),
      selectByFindOption(page, 'select[id$="comune_esazione"]', payload.esazioneComune as string, '')
    ]),
    'non sono riuscito a selezionare il comune di esazione:'
  );

  await tryOrThrow(
    () => selectByFindOption(page, 'select[id$="cap_esazione"]', payload.esazioneCap as string, ''),
    'non sono riuscito a selezionare il cap della esazione:'
  );

  await waitForSelectorAndClickEvaluated(page, 'input[id$="verificaAddress_esazione"][value="Verifica"]');

  await checkAlertIndirizzo(page);
}
