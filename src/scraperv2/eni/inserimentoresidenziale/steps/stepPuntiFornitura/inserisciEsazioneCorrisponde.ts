import { Page } from "puppeteer";
import selectByFindOption from "../../../inserimentocontratto/selectByFindOption";
import { EniResidenzialeInserimentoPayload } from "../../../types/EniResidenzialeInserimentoPayload";

const SELECTOR = 'select[id$="indirizzoEsazIsFornitura"]';

export default async function inserisciEsazioneCorrisponde(page: Page, esazioneCorrisponde: EniResidenzialeInserimentoPayload['esazioneCorrisponde']) {
  try {
    await selectByFindOption(page, SELECTOR, esazioneCorrisponde, '');
  } catch (ex) {
    if (esazioneCorrisponde == "all'indirizzo di residenza") {
      await selectByFindOption(page, SELECTOR, "all'indirizzo di fornitura", '');
    }
  }
}
