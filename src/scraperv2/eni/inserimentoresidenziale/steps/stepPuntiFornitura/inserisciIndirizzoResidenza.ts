import { Page } from "puppeteer";
import waitForNavigation from "../../../../../browser/page/waitForNavigation";
import waitForSelectorAndClickEvaluated from "../../../../../browser/page/waitForSelectorAndClickEvaluated";
import waitForSelectorAndType from "../../../../../browser/page/waitForSelectorAndType";
import tryOrThrow from "../../../../../utils/tryOrThrow";
import selectByFindOption from "../../../inserimentocontratto/selectByFindOption";
import { EniResidenzialeInserimentoPayload } from "../../../types/EniResidenzialeInserimentoPayload";
import checkAlertIndirizzo from "../../checkAlertIndirizzo";

export default async function inserisciIndirizzoResidenza(page: Page, payload: EniResidenzialeInserimentoPayload) {
  await tryOrThrow(
    () => selectByFindOption(page, 'select[id$="toponomastica_residenza"]', payload.residenzaToponimo as string, ''),
    'non sono riuscito a selezionare il toponimo della residenza:'
  );

  await tryOrThrow(
    () => waitForSelectorAndType(page, 'input[id$="indirizzo_residenza"]', payload.residenzaIndirizzo as string),
    "non sono riuscito ad inserire l'indirizzo di residenza:"
  );

  await tryOrThrow(
    () => waitForSelectorAndType(page, 'input[id$="civico_residenza"]', payload.residenzaCivico as string),
    "non sono riuscito ad inserire il civico di residenza:"
  );

  if (payload.residenzaScala) {
    await tryOrThrow(
      () => waitForSelectorAndType(page, 'input[id$="scala_residenza"]', payload.residenzaScala as string),
      "non sono riuscito ad inserire la scala di residenza:"
    );
  }

  await tryOrThrow(
    () => Promise.all([
      waitForNavigation(page),
      selectByFindOption(page, 'select[id$="provincia_residenza"]', payload.residenzaProvincia as string, '')
    ]),
    'non sono riuscito a selezionare la provincia della residenza:'
  );

  await tryOrThrow(
    () => Promise.all([
      waitForNavigation(page),
      selectByFindOption(page, 'select[id$="comune_residenza"]', payload.residenzaComune as string, '')
    ]),
    'non sono riuscito a selezionare il comune della residenza:'
  );

  await tryOrThrow(
    () => selectByFindOption(page, 'select[id$="cap_residenza"]', payload.residenzaCap as string, '', true),
    'non sono riuscito a selezionare il cap della residenza:'
  );

  await waitForSelectorAndClickEvaluated(page, 'input[id$="verificaAddress_residenza"][value="Verifica"]');

  await checkAlertIndirizzo(page);
}
