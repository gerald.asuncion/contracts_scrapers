import { Page } from "puppeteer";
import tryOrThrow from "../../../../../utils/tryOrThrow";
import { EniResidenzialeInserimentoPayload } from "../../../types/EniResidenzialeInserimentoPayload";
import capitale from 'lodash/capitalize';
import waitForSelectorAndClickEvaluated from "../../../../../browser/page/waitForSelectorAndClickEvaluated";
import selectByFindOption from "../../../inserimentocontratto/selectByFindOption";
import waitForNavigation from "../../../../../browser/page/waitForNavigation";
import waitForSelectorAndClick from "../../../../../browser/page/waitForSelectorAndClick";
import inserisciDatiLuce from "./inserisciDatiLuce";
import inserisciDatiGas from "./inserisciDatiGas";
import inserisciIndirizzoResidenza from "./inserisciIndirizzoResidenza";
import inserisciIndirizzoEsazione from "./inserisciIndirizzoEsazione";
import inserisciEsazioneCorrisponde from "./inserisciEsazioneCorrisponde";
import checkValidationErrors from "../../checkValidationErrors";

export default async function stepPuntiFornitura(page: Page, payload: EniResidenzialeInserimentoPayload): Promise<[null, string[]] | [boolean, null]> {
  await tryOrThrow(
    () => Promise.all([
      waitForNavigation(page),
      waitForSelectorAndClickEvaluated(page, `input[id*="indFornituraIsResidenzaRadio"][value="${capitale(payload.indirizzoFornituraCorrispondeIndirizzoAbitazioneResidenza)}"]`)
    ]),
    `non sono riuscito a selezionare \`L'indirizzo di fornitura corrisponde all'indirizzo dell'abitazione di residenza\` con ${capitale(payload.indirizzoFornituraCorrispondeIndirizzoAbitazioneResidenza)}:`
  );

  if (payload.indirizzoFornituraCorrispondeIndirizzoAbitazioneResidenza === 'no') {
    await inserisciIndirizzoResidenza(page, payload);
  }

  await tryOrThrow(
    () => Promise.all([
      waitForNavigation(page, { timeout: 120000 }),
      inserisciEsazioneCorrisponde(page, payload.esazioneCorrisponde)
    ]),
    'non sono riuscito a selezionare `esazione corrisponde`:'
  );

  if (payload.esazioneCorrisponde === 'ad un altro indirizzo') {
    await inserisciIndirizzoEsazione(page, payload);
  }

  await tryOrThrow(
    () => selectByFindOption(page, 'select[id$="indirizzoInterlocutoreEq"]', payload.indirizzoInterlocutore, ''),
    "non sono riuscito a selezionare l'indirizzo dell'interlocutore:"
  );

  if (['luce', 'dual'].includes(payload.tipoFornitura)) {
    await inserisciDatiLuce(page, payload);
  }

  if (['gas', 'dual'].includes(payload.tipoFornitura)) {
    await inserisciDatiGas(page, payload);
  }

  await Promise.all([
    waitForNavigation(page),
    waitForSelectorAndClick(page, 'input[value="Avanti"]')
  ]);

  const errors = await checkValidationErrors(page);

  if (errors.length) {
    return [null, errors];
  }

  return [true, null];
}
