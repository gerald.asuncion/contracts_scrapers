import { Page } from "puppeteer";
import waitForSelectorAndClick from "../../../../../browser/page/waitForSelectorAndClick";
import { EniResidenzialeInserimentoPayload } from "../../../types/EniResidenzialeInserimentoPayload";

export type UtilizziGas = Exclude<Required<EniResidenzialeInserimentoPayload['utilizzoGas']>, undefined>;

export type UtilizzoGas = UtilizziGas[number];

const UTILIZZOGAS_MAP: Record<UtilizzoGas, string> = {
  'cottura': 'input[name*="cotturaGas_fornitura"]:not([id$="cotturaGas_fornituraH"]):not([type="hidden"])',
  'acqua calda': 'input[name*="acquaGas_fornitura"]:not([type="hidden"])',
  'riscaldamento': 'input[name*="riscaldamentoGas_fornitura"]:not([type="hidden"])'
};

export default async function selezionaUtilizziGas(page: Page, utilizzi: UtilizziGas) {
  if (!utilizzi.length) {
    return;
  }

  const utilizzo = utilizzi.pop() as UtilizzoGas;

  await waitForSelectorAndClick(page, UTILIZZOGAS_MAP[utilizzo]);
  await selezionaUtilizziGas(page, utilizzi);
}
