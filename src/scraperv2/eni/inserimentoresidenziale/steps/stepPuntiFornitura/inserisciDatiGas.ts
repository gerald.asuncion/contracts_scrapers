import { Page } from "puppeteer";
import waitForSelectorAndType from "../../../../../browser/page/waitForSelectorAndType";
import tryOrThrow from "../../../../../utils/tryOrThrow";
import selectByFindOption from "../../../inserimentocontratto/selectByFindOption";
import { EniResidenzialeInserimentoPayload } from "../../../types/EniResidenzialeInserimentoPayload";
import selezionaUtilizziGas, { UtilizziGas } from "./selezionaUtilizziGas";

export default async function inserisciDatiGas(page: Page, payload: EniResidenzialeInserimentoPayload) {
  await tryOrThrow(
    () => selectByFindOption(page, 'select[id$="attualeFornitoreGas_fornitura"]', payload.attualeFornitoreGas as string, ''),
    "non sono riuscito a selezionare l'attuale fornitore gas:"
  );

  await tryOrThrow(
    () => selectByFindOption(page, 'select[id$="tipoAbitazioneGas_fornitura"]', payload.tipologiaAbitazione, ''),
    'non sono riuscito a selezionare la tipologia di abitazione:'
  );

  await tryOrThrow(
    () => selezionaUtilizziGas(page, payload.utilizzoGas as UtilizziGas),
    "non sono riuscito a selezionare l'utilizzo gas:"
  );

  await tryOrThrow(
    () => waitForSelectorAndType(page, 'input[id$="consumoGas_fornitura"]', payload.consumoAnnuoGas as string),
    'non sono riuscito ad inserire il consumo annuo:'
  );

  await tryOrThrow(
    () => selectByFindOption(page, 'select[id$="titolaritaPdr"]', payload.titolaritaFornitura, ''),
    'non sono riuscito a selezionare la titolarità fornitura:'
  );
}
