import { Page } from "puppeteer";
import waitForSelectorAndType from "../../../../../browser/page/waitForSelectorAndType";
import waitForSelectorAndTypeEvaluated from "../../../../../browser/page/waitForSelectorAndTypeEvaluated";
import tryOrThrow from "../../../../../utils/tryOrThrow";
import selectByFindOption from "../../../inserimentocontratto/selectByFindOption";
import { EniResidenzialeInserimentoPayload } from "../../../types/EniResidenzialeInserimentoPayload";

export default async function inserisciDatiLuce(page: Page, payload: EniResidenzialeInserimentoPayload) {
  await tryOrThrow(
    () => selectByFindOption(page, 'select[id$="attualeFornitorePow_fornitura"]', payload.attualeFornitoreEnergia as string, ''),
    'non sono riuscito a selezionare attuale fornitore energia:'
  );

  if (payload.tipoFornitura !== 'dual') {
    await tryOrThrow(
      () => selectByFindOption(page, 'select[id$="tipoAbitazionePow_fornitura"]', payload.tipologiaAbitazione, ''),
      'non sono riuscito a selezionare la tipologia di abitazione:'
    );
  }

  await tryOrThrow(
    () => selectByFindOption(page, 'select[id$="potenzaImpegnata_fornitura"]', payload.potenzaImpegnata as string, ''),
    'non sono riuscito a selezionare la potenza impegnata:'
  );

  await tryOrThrow(
    async () => {
      const selector = 'input[id$="potenzaDisponibile_fornitura"]';
      await waitForSelectorAndTypeEvaluated(page, selector, '');
      await waitForSelectorAndType(page, selector, payload.potenzaDisponibile as string);
    },
    'non sono riuscito ad inserire la potenza disponibile:'
  );

  await tryOrThrow(
    () => waitForSelectorAndType(page, 'input[id$="consumoPow_fornitura"]', payload.consumoAnnuoElettricita as string),
    'non sono riuscito ad inserire il consumo annuo elettricità:'
  );

  if (payload.tipoFornitura !== 'dual') {
    await tryOrThrow(
      () => selectByFindOption(page, 'select[id$="titolaritaPod"]', payload.titolaritaFornitura, ''),
      'non sono riuscito a selezionare la titolarità fornitura:'
    );
  }
}
