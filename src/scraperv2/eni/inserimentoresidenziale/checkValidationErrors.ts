import { Page } from "puppeteer";

// vengono mostrati come success
export const VALIDATION_SUCCESS_TO_ERRORS = [
];

export default async function checkValidationErrors(page: Page) {
  let errors: string[] = [];

  try {
    errors = await page.evaluate((validationSuccessToErrors) => {
      const list = Array.from(document.querySelectorAll<HTMLSpanElement>('.errore_reg'))
        .filter((el) => el.style.display !== 'none')
        .map((el) => el.textContent?.trim().replace(/\n/g, '').replace(/\t/g, '') || '')
        .filter(Boolean);

      const others = Array.from(document.querySelectorAll<HTMLSpanElement>('font[color="green"]'))
        .filter((el) => el.style.display !== 'none')
        .map((el) => el.textContent?.trim().replace(/\n/g, '').replace(/\t/g, '') || '')
        .filter(Boolean)
        .filter(msg => validationSuccessToErrors.includes(msg));

      return list.concat(others);
    }, VALIDATION_SUCCESS_TO_ERRORS);
  } catch (ex) {
    // nessun errore apparso
    // nulla da fare
    console.log(ex)
  }
  return errors;
}
