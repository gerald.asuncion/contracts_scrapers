import { Page } from "puppeteer";
import getElementText from "../../../browser/page/getElementText";
import waitForNavigation from "../../../browser/page/waitForNavigation";
import waitForSelectorAndClick from "../../../browser/page/waitForSelectorAndClick";
import waitForSelectorAndClickEvaluated from "../../../browser/page/waitForSelectorAndClickEvaluated";
import waitForTimeout from "../../../browser/page/waitForTimeout";
import waitForVisible from "../../../browser/page/waitForVisible";
import timeoutPromise from "../../timeoutPromise";

const ALERT_CONTAINER_SELECTOR = '.blockUI.blockMsg.blockPage';
const OK_BTN_SELECTOR = `${ALERT_CONTAINER_SELECTOR} input[value="Ok"]`;

const ASPETTA_ESITO_TIMEOUT = 60000;

async function aspettaCheAppaiaEsito(page: Page) {
  let alertMsg = await getElementText(page, ALERT_CONTAINER_SELECTOR, 'textContent') as string;
  alertMsg = alertMsg.replace(/\n/g, '').replace(/\t/g, '').trim();
  if (alertMsg.includes('Invio richiesta in corso')) {
    await waitForTimeout(page, 100);
    alertMsg = await aspettaCheAppaiaEsito(page);
  }
  return alertMsg;
}

export default async function checkAlertIndirizzo(page: Page) {
  await waitForVisible(page, ALERT_CONTAINER_SELECTOR);

  let alertMsg = await timeoutPromise<string>('Timeout Alert Indirizzo', ASPETTA_ESITO_TIMEOUT)(aspettaCheAppaiaEsito(page)) as string;

  if (['Indirizzo non censito nel nostro sistema.', 'Indirizzo inserito corretto e validato.'].includes(alertMsg)) {
    await Promise.all([
      waitForNavigation(page),
      waitForSelectorAndClickEvaluated(page, OK_BTN_SELECTOR)
    ]);
  } else {
    if (alertMsg.toLowerCase().includes('forse stavi cercando questi indirizzi?')) {
      // confermo l'indirizzo
      await Promise.all([
        waitForNavigation(page),
        waitForSelectorAndClick(page, '#pop_UpStradario input:last-child')
      ]);
    }
  }
}
