import createChildPayloadAwareLogger from "../../utils/createChildPayloadAwareLogger";
import { ScraperResponse, WebScraper } from "../scraper";
import { SCRAPER_TIPOLOGIA } from "../ScraperOptions";
import Eni from "./Eni";
import { EniResidenzialeInserimentoPayload } from "./types/EniResidenzialeInserimentoPayload";
import stepAperturaPaginaInserimentoContratto from "./inserimentocontratto/stepAperturaPaginaInserimentoContratto";
import stepIniziale from "./inserimentoresidenziale/steps/stepIniziale/stepIniziale";
import stepVerificaVendibilita from "./inserimentoresidenziale/steps/stepVerificaVendibilita/stepVerificaVendibilita";
import stepDatiCliente from "./inserimentoresidenziale/steps/stepDatiCliente/stepDatiCliente";
import stepPuntiFornitura from "./inserimentoresidenziale/steps/stepPuntiFornitura/stepPuntiFornitura";
import stepPagamento from "./inserimentoresidenziale/steps/stepPagamento/stepPagamento";
import stepInvioPratica from "./inserimentoresidenziale/steps/stepInvioPratica/stepInvioPratica";
import stepRecuperoCodicePlico from "./inserimentoresidenziale/steps/stepRecuperoCodicePlico/stepRecuperoCodicePlico";
import SuccessResponse from "../../response/SuccessResponse";
import stepAggiornamentoMail from "./inserimentoresidenziale/steps/stepAggiornamentoEmail/stepAggiornamentoMail";
import { QueueTask } from "../../task/QueueTask";
import delay from "../../utils/delay";
import stepAperturaPaginaInserimentoContrattoSubentro from "./inserimentocontratto/stepAperturaPaginaInserimentoContrattoSubentro";

import selezionaOpzioneContatoreCliente from "./inserimentoresidenzialesubentro/steps/selezionaOpzioneContatoreCliente";
import selezionaTipoFornitura from "./inserimentoresidenzialesubentro/steps/selezionaTipoFornitura";
import selezionaOpzioneErogaEnergiaCliente from "./inserimentoresidenzialesubentro/steps/selezionaOpzioneErogaEnergiaCliente";
import inserisciDatiCliente from "./inserimentoresidenzialesubentro/steps/inserisciDatiCliente";
import inserisciDatiIndirizzoFornitura from "./inserimentoresidenzialesubentro/steps/inserisciDatiIndirizzoFornitura";
import verificaIndirizzoFornitura from "./inserimentoresidenzialesubentro/steps/verificaIndirizzoFornitura";
import clickBannerVerificaIndirizzoFornitura from "./inserimentoresidenzialesubentro/steps/clickBannerVerificaIndirizzoFornitura";
import selezionaUtilizziGas from "./inserimentoresidenziale/steps/stepPuntiFornitura/selezionaUtilizziGas";
import inserisciDatiConsumoLuce from "./inserimentoresidenzialesubentro/steps/inserisciDatiConsumoLuce";
import selezionaMetodoPagamento from "./inserimentoresidenzialesubentro/steps/selezionaMetodoPagamento";
import inserisciDatiConsumoGas from "./inserimentoresidenzialesubentro/steps/inserisciDatiConsumoGas";
import inserisciDatiDocumentoCliente from "./inserimentoresidenzialesubentro/steps/inserisciDatiDocumentoCliente";
import inserisciRecapitiCliente from "./inserimentoresidenzialesubentro/steps/inserisciRecapitiCliente";
import selezionaTitolarieta from "./inserimentoresidenzialesubentro/steps/selezionaTitolarieta";
import selezionaDecorrenzaImmediata from "./inserimentoresidenzialesubentro/steps/selezionaDecorrenzaImmediata";
import clickConsensiPrivacy from "./inserimentoresidenzialesubentro/steps/clickConsensiPrivacy";
import selezionaOfferta from "./inserimentoresidenzialesubentro/steps/selezionaOfferta";
import selezionaMonoraria from "./inserimentoresidenzialesubentro/steps/selezionaMonoraria";
import inserisciPagamentoFatturazione from "./inserimentoresidenzialesubentro/steps/insersciPagamentoFatturazione";
import inserisciPreferenzeAppuntamento from "./inserimentoresidenzialesubentro/steps/inserisciPreferenzeAppuntamento";
import selezionaOpzioneModificaImpiantoCliente from "./inserimentoresidenzialesubentro/steps/selezionaOpzioneModificaImpiantoCliente";
import clickBannerAvanti from "./inserimentoresidenzialesubentro/steps/clickBannerAvanti";
import recuperoCodicePlico from "./inserimentoresidenzialesubentro/steps/recuperoCodicePlico";
import checkNoSubmit from "../../utils/checkNoSubmit";
import controlloErrori from "./inserimentoresidenzialesubentro/steps/controlloErrori";

@QueueTask({ scraperName: 'eniResidenziale' })
export default class EniResidenzialeInserimento extends WebScraper<EniResidenzialeInserimentoPayload> {
  static readonly LOGIN_URL = 'https://webapp.eni.com/';

  async login(payload: EniResidenzialeInserimentoPayload): Promise<void> {
    await Eni.prototype.login.call(this, payload as any);
  }

  /**
   * @override
   * @param payload Payload ricevuto
   * @returns esito dell'inserimento
   */
  async scrapeWebsite(payload: EniResidenzialeInserimentoPayload): Promise<ScraperResponse> {
    const logger = createChildPayloadAwareLogger(this.childLogger, payload);

    const page = await this.p();

    if ((await page.content()).toLowerCase().includes('impossibile caricare la pagina')) {
      throw new Error('il portale non carica');
    }

    if(payload.tipologiaContratto === "subentro") {
      logger.info('step apertura pagina inserimento contratto subentro');
      await stepAperturaPaginaInserimentoContrattoSubentro(page, logger);

      logger.info('step seleziona tipo fornitore');
      await selezionaTipoFornitura(page, payload.tipoFornitura);

      logger.info('step seleziona opzione contatore cliente');
      await selezionaOpzioneContatoreCliente(page, payload.tipoFornitura);

      logger.info('step seleziona opzione eroga energia cliente');
      await selezionaOpzioneErogaEnergiaCliente(page, payload.tipoFornitura);

      if(payload.tipoFornitura === "gas") {
        logger.info('step seleziona opzione modifica impianto cliente');
        await selezionaOpzioneModificaImpiantoCliente(page, payload.tipoFornitura);
      }

      logger.info('step inserisci dati cliente');
      await inserisciDatiCliente(page, payload);

      logger.info('step inserisci dati indirizzo fornitura');
      await inserisciDatiIndirizzoFornitura(page, payload);

      logger.info('step click banner verifica indirizzo fornitura');
      await clickBannerVerificaIndirizzoFornitura(page, payload.tipoFornitura);

      if(payload.tipoFornitura === "luce") {
        logger.info('step inserisci dati consumo luce');
        await inserisciDatiConsumoLuce(page, payload);
      }
      if (payload.tipoFornitura === "gas") {
        logger.info('step inserisci dati consumo gas');
        await inserisciDatiConsumoGas(page, payload);
      }

      logger.info('step seleziona metodo di pagamento');
      await selezionaMetodoPagamento(page, payload);

      if(payload.tipoFornitura === "gas") {
        logger.info('step click banner avanti');
        await clickBannerAvanti(page, payload.tipoFornitura, logger);
      }

      logger.info('step inserisci dati documento cliente');
      await inserisciDatiDocumentoCliente(page, payload);
      
      logger.info('step inserisci recapiti cliente');
      await inserisciRecapitiCliente(page, payload);

      //await selezionaTitolarieta(page, payload);

      //await selezionaDecorrenzaImmediata(page, payload);

      logger.info('step click consensi');
      await clickConsensiPrivacy(page, payload.tipoFornitura, logger);

      logger.info('step seleziona offerta');
      await selezionaOfferta(page, payload);

      if(payload.tipoFornitura === "luce") {
        logger.info('step seleziona monoraria');
        await selezionaMonoraria(page, payload);
      }

      logger.info('step inserisci pagamento fatturazione');
      await inserisciPagamentoFatturazione(page, payload);

      await controlloErrori(page);
      
      checkNoSubmit(payload);

      logger.info('step inserisci appuntamento');
      await inserisciPreferenzeAppuntamento(page, payload);


      logger.info('step recupero codice plico');
      const codicePlico = await recuperoCodicePlico(page);

      logger.info(`recuperato codice plico ${codicePlico} per il contratto ${payload.idDatiContratto}`);
      return new SuccessResponse(codicePlico);

    }else {
        logger.info('step aperutura pagina inserimento contratto ');
        await stepAperturaPaginaInserimentoContratto(page, logger);
        
        logger.info('step iniziale');
        await stepIniziale(page, payload);

        logger.info('step verifica vendibilità');
        const [vendibilitaSuccess, vendibilitaErrors] = await stepVerificaVendibilita(page, payload);

        if (vendibilitaErrors && vendibilitaErrors.length) {
          throw new Error(vendibilitaErrors.join('; '));
        }

        logger.info('step dati cliente');
        const [datiClienteSuccess, datiClienteErrors] = await stepDatiCliente(page, payload);

        if (datiClienteErrors && datiClienteErrors.length) {
          throw new Error(datiClienteErrors.join('; '));
        }

        logger.info('step punti fornitura');
        const [puntiFornituraSuccess, puntiFornituraErrors] = await stepPuntiFornitura(page, payload);

        if (puntiFornituraErrors && puntiFornituraErrors.length) {
          throw new Error(puntiFornituraErrors.join('; '));
        }

        logger.info('step pagamento');
        const [pagamentoSuccess, pagamentoErrors] = await stepPagamento(page, payload);

        if (pagamentoErrors && pagamentoErrors.length) {
          throw new Error(pagamentoErrors.join('; '));
        }

        logger.info('step invio pratica');
        await stepInvioPratica(page, payload);

        logger.info('step recupero codice plico');
        await delay(1000); // ? Sometimes puppeteer must wait a little before starting page evaluation
        const codicePlico = await stepRecuperoCodicePlico(page);

        if (datiClienteSuccess?.needToUpdateEmail) {
          logger.info('step aggiornamento email');
          await stepAggiornamentoMail(page, codicePlico, payload.email);
        }

        payload.utilizzoGas ? await selezionaUtilizziGas(page, payload.utilizzoGas) : null

        logger.info(`recuperato codice plico ${codicePlico} per il contratto ${payload.idDatiContratto}`);
        return new SuccessResponse(codicePlico);
    }

  }

  getScraperCodice() {
    return 'eni-residenziale';
  }

  getScraperTipologia() {
    return SCRAPER_TIPOLOGIA.INSERIMENTO;
  }
}
