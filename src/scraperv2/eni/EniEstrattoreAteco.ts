import Eni from './Eni';
import ScraperOptions from '../ScraperOptions';
import { ScraperResponse } from '../scraper';
import FailureResponse from "../../response/FailureResponse";
import SuccessResponse from "../../response/SuccessResponse";
import createChildLogger from '../../utils/createChildLogger';
import stepAperturaPaginaInserimentoContratto from './inserimentocontratto/stepAperturaPaginaInserimentoContratto';
import stepInserimentoAgente from './inserimentocontratto/stepInserimentoAgente';
import stepVerficaVendibilita from './inserimentocontratto/stepVerificaVendibilita';
import eniFornitoriLucePayload from './fornitori/eniFornitoriLucePayload';
import extractErrorMessage from '../../utils/extractErrorMessage';
import estraiCodiciAteco from './ateco/estraiCodiciAteco';
import waitForVisible from '../../browser/page/waitForVisible';
import EniCodiciAtecoRepo from '../../repo/EniCodiciAtecoRepo';
import { QueueTask } from '../../task/QueueTask';
import waitForXPathAndClick from '../../browser/page/waitForXPathAndClick';

interface EniEstrattoreAtecoOptions extends ScraperOptions {
  eniCodiciAtecoRepo: EniCodiciAtecoRepo
}

@QueueTask()
export default class EniEstrattoreAteco extends Eni {
  private eniCodiciAtecoRepo: EniCodiciAtecoRepo;

  constructor(options: EniEstrattoreAtecoOptions) {
    super(options);
    this.childLogger = createChildLogger(options.logger, 'EniCodiciAteco');
    this.eniCodiciAtecoRepo = options.eniCodiciAtecoRepo;
  }

  /**
   * @override
   */
  async scrapeWebsite(): Promise<ScraperResponse> {
    const logger = this.childLogger;

    /**
     * mi serviva un payload fasullo per far arrivare lo scraper
     * alla pagina in cui ci sono le select coi dati che bisogna estrarre
     * quindi ho deciso di usarne uno precedentemente creato per altri scraper
     */
    const payload = eniFornitoriLucePayload;
    const { prodotto } = payload;
    const page = await this.p();

    try {
      await stepAperturaPaginaInserimentoContratto(page, logger);

      await stepInserimentoAgente(page, logger);

      await stepVerficaVendibilita(page, payload, logger);

      await waitForXPathAndClick(page, `//span[contains(., "${prodotto}")]/parent::td//input[@type="radio"]`);
      await waitForVisible(page, '#divSingolo');
      await waitForVisible(page, '#copertura\\.formaGiuridica');

      const lista = await estraiCodiciAteco(page, logger);

      logger.info('lista recuperata, procedo a salvare i dati sul database');

      await this.eniCodiciAtecoRepo.save(lista);

      logger.info('dati salvati sul database');

      return new SuccessResponse();
    } catch (ex) {
      await this.saveScreenshot(page, logger, 'eni-ateco-errore');
      const errMsg = extractErrorMessage(ex);
      logger.error(errMsg);
      return new FailureResponse(errMsg);
    }
  }
}
