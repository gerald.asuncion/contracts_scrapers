import { Page } from 'puppeteer';
import waitForSelectorAndSelect from '../../../../browser/page/waitForSelectorAndSelect';
import waitForSelectorAndType from '../../../../browser/page/waitForSelectorAndType';
import waitForTimeout from '../../../../browser/page/waitForTimeout';
import { Logger } from '../../../../logger';
import { POTENZA_DISPONIBILE, SPINNER_DELAY } from '../../constants';
import EniInserimentoPayload from '../../EniInserimentoPayload';
import selectByFindOption from '../selectByFindOption';

export default async function inserisciFornituraEnergia(
  page: Page,
  {
    tipoFornitura,
    potenza,
    consumoAnnuo
  }: EniInserimentoPayload,
  logger: Logger
): Promise<void> {
  if (tipoFornitura === 'luce') {
    const potenzaStr = potenza as string;
    const potenzaNum = Number(potenzaStr.replace(/,/g, '.'));
    if (potenzaNum > 30) {
      await selectByFindOption(page, 'select#copertura\\.potenzaImpegnata_fornitura', 'altro', 'potenza impegnata');

      await waitForSelectorAndType(page, '#copertura\\.potenzaImpegnata_fornitura_free', potenzaStr/* .replace(/,/g, '.') */);
    } else {
      await selectByFindOption(page, 'select#copertura\\.potenzaImpegnata_fornitura', potenzaStr.replace(/,/g, '.'), 'potenza impegnata');
    }
    logger.info('inserita potenza impegnata');

    let potenzaDisponibile = POTENZA_DISPONIBILE[potenzaStr];
    if (!potenzaDisponibile) {
      potenzaDisponibile = potenzaStr;
    }
    await waitForSelectorAndType(page, '#copertura\\.potenzaDisponibile_fornitura', potenzaDisponibile);
    logger.info('inserita potenza disponibile');

    await waitForSelectorAndType(page, '#copertura\\.consumoPow_fornitura', (<number>consumoAnnuo).toString());
    logger.info('inserito consumo annuo luce');

    await waitForSelectorAndSelect(page, '#copertura\\.tipoMisuratore', ['5']);
    logger.info('inserito tipo misuratore');
  } else {
    await waitForSelectorAndType(page, '#copertura\\.consumoAnnuo', (<number>consumoAnnuo).toString());
    await page.keyboard.press('Tab');
    logger.info('inserito consumo annuo gas');

    await waitForTimeout(page, SPINNER_DELAY);

    await waitForSelectorAndSelect(page, '#copertura\\.apparecchioPrimario', ['CALDAIA INDIVIDUALE']);
  }
}
