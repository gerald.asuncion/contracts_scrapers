import { Page } from 'puppeteer';
import waitForSelectorAndSelect from '../../../../browser/page/waitForSelectorAndSelect';

const MERCATO_PROVENIENZA_VALUE: Record<string, string> = {
  libero: '1',
  tutelato: '2',
  salvaguardia: '3'
};

export default async function selezionaMercatoProvenienza(
  page: Page,
  mercatoProvenienza:string
): Promise<void> {
  await waitForSelectorAndSelect(page, '#copertura\\.mercatoProv', [MERCATO_PROVENIENZA_VALUE[mercatoProvenienza.toLowerCase()]]);
}
