import { Page } from 'puppeteer';
import EniInserimentoPayload from '../../EniInserimentoPayload';
import { Logger } from '../../../../logger';
import waitForSelectorAndClick from '../../../../browser/page/waitForSelectorAndClick';
import waitForSelectorAndSelect from '../../../../browser/page/waitForSelectorAndSelect';
import selectByFindOption from '../selectByFindOption';
import { OCCUPAZIONE_SELECTOR, AGEVOLAZIONE_SELECTOR } from '../../constants';
import selezionaMercatoProvenienza from './selezionaMercatoProvenienza';

export default async function inserisciDatiComuniFornitura(
  page: Page,
  {
    tipoFornitura,
    occupazione,
    attualeFornitore,
    mercatoProvenienza,
    tensione,
    agevolazione
  }: EniInserimentoPayload,
  logger: Logger
): Promise<void> {
  await waitForSelectorAndClick(page, '#copertura\\.fattUnica_no');
  logger.info('impostata fatturazione unica a no');

  let occupazioneSelector = OCCUPAZIONE_SELECTOR[occupazione.toLowerCase()];
  if (!occupazioneSelector) {
    occupazioneSelector = OCCUPAZIONE_SELECTOR.altro;
  }
  await waitForSelectorAndSelect(page, '#copertura\\.titolarita', [occupazioneSelector]);
  logger.info('selezionata occupazione');

  if (tipoFornitura === 'luce') {
    await selectByFindOption(page, '#copertura\\.attualeFornitorePow_fornitura', <string>attualeFornitore, 'attuale fornitore luce');
    logger.info('selezionato attuale fornitore luce');

    await selezionaMercatoProvenienza(page, mercatoProvenienza);
    logger.info('selezionato mercato di provenienza');

    await selectByFindOption(page, '#copertura\\.livelloTensione', `${tensione} V`, 'livello di tensione');
    logger.info('selezionato livello tensione luce');
  } else {
    await selectByFindOption(page, '#copertura\\.attualeFornitoreGas_fornitura', <string>attualeFornitore, 'attuale fornitore gas');
    logger.info('selezionato attuale fornitore gas');
  }

  await waitForSelectorAndSelect(page, '#copertura\\.dichiarazioniFiscali', [AGEVOLAZIONE_SELECTOR[`${agevolazione}`]]);
  logger.info('impostato campo agevolazione');

  if (tipoFornitura === 'luce') {
    const tipoContatoreSelector = '#copertura\\.inserimentoDati > table:nth-child(739) > tbody > tr:nth-child(31) > td:nth-child(3) > select';
    try {
      await selectByFindOption(page, tipoContatoreSelector, 'multiorario', 'tipo contatore');
    } catch (error) {
      await selectByFindOption(page, tipoContatoreSelector, 'opzione verde', 'Opzione verde per la luce');
    }
    logger.info('selezionato tipo contatore multiorario');
  }
}
