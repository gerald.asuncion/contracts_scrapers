import { Page } from 'puppeteer';
import ifEnabledExecute from '../../../../browser/page/ifEnabledExecute';
import waitForSelectorAndType from '../../../../browser/page/waitForSelectorAndType';
import { Logger } from '../../../../logger';
import EniInserimentoPayload from '../../EniInserimentoPayload';
import selezionaCapIndirizzoAttivazione from '../datidelcliente/selezionaCapIndirizzoAttivazione';
import selectByFindOption from '../selectByFindOption';

export default async function inserisciIndirizzoFornitura(
  page: Page,
  {
    tipoIndirizzoAttivazione,
    indirizzoAttivazione,
    civicoIndirizzoAttivazione,
    provinciaIndirizzoAttivazione,
    comuneIndirizzoAttivazione,
    capIndirizzoAttivazione
  }: EniInserimentoPayload,
  logger: Logger
): Promise<void> {
  logger.info('inserisco indirizzo fornitura');

  await ifEnabledExecute(page, '#copertura\\.toponomasticaFornitura', async () => {
    await selectByFindOption(
      page, '#copertura\\.toponomasticaFornitura', <string>tipoIndirizzoAttivazione, 'tipo indirizzo recapito fattura'
    );
    logger.info('inserito tipo indirizzo recapito fattura');
  });

  await ifEnabledExecute(page, '#copertura\\.indirizzoFornitura', async () => {
    await waitForSelectorAndType(page, '#copertura\\.indirizzoFornitura', <string>indirizzoAttivazione);
    logger.info('inserito indirizzo recapito fattura');
  });

  await ifEnabledExecute(page, '#copertura\\.civicoFornitura', async () => {
    await waitForSelectorAndType(page, '#copertura\\.civicoFornitura', <string>civicoIndirizzoAttivazione);
    logger.info('inserito civico indirizzo recapito fattura');
  });

  await ifEnabledExecute(page, '#copertura\\.provinciaFornitura', async () => {
    await Promise.all([
      page.waitForResponse(() => true),
      selectByFindOption(
        page, '#copertura\\.provinciaFornitura', provinciaIndirizzoAttivazione, 'provincia recapito fattura'
      )
    ]);
    logger.info('inserita provincia recapito fattura');
  });

  await ifEnabledExecute(page, '#copertura\\.comuneFornitura', async () => {
    await Promise.all([
      page.waitForResponse(() => true),
      selectByFindOption(page, '#copertura\\.comuneFornitura', comuneIndirizzoAttivazione, 'comune recapito fattura')
    ]);
    logger.info('inserito comune recapito fattura');
  });

  await selezionaCapIndirizzoAttivazione(page, capIndirizzoAttivazione, '#copertura\\.capFornitura', 'cap recapito fattura');
  logger.info('inserito cap recapito fattura');
}
