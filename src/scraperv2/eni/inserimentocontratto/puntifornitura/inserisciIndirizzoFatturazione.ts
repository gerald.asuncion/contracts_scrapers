import { Page } from 'puppeteer';
import waitForSelectorAndClick from '../../../../browser/page/waitForSelectorAndClick';
import waitForSelectorAndSelect from '../../../../browser/page/waitForSelectorAndSelect';
import waitForSelectorAndType from '../../../../browser/page/waitForSelectorAndType';
import { Logger } from '../../../../logger';
import EniInserimentoPayload from '../../EniInserimentoPayload';
import selectByFindOption from '../selectByFindOption';
import waitForModal from '../waitForModal';
import waitModaleConfermaIndirizzoAttivazione from '../waitModaleConfermaIndirizzoAttivazione';
import { selezionaCap } from '../datidelcliente/selezionaCapSedeLegale';

export default async function inserisciIndirizzoFatturazione(
  page: Page,
  {
    tipoIndirizzoRecapitoFattura,
    indirizzoRecapitoFattura,
    civicoRecapitoFattura,
    // scalaRecapitoFattura,
    provinciaRecapitoFattura,
    comuneRecapitoFattura,
    capRecapitoFattura
  }: EniInserimentoPayload,
  logger: Logger
): Promise<void> {
  const selector = '#copertura\\.indirizzoEsazIsFornitura';
  if (comuneRecapitoFattura) {
    logger.info('inserisco indirizzo fatturazione');
    await waitForSelectorAndSelect(page, selector, ['3']);

    await selectByFindOption(
      page, '#copertura\\.toponomastica_esazione', <string>tipoIndirizzoRecapitoFattura, 'tipo indirizzo recapito fattura'
    );
    logger.info('inserito tipo indirizzo recapito fattura');

    await waitForSelectorAndType(page, '#copertura\\.indirizzo_esazione', <string>indirizzoRecapitoFattura);
    logger.info('inserito indirizzo recapito fattura');

    await waitForSelectorAndType(page, '#copertura\\.civico_esazione', <string>civicoRecapitoFattura);
    logger.info('inserito civico indirizzo recapito fattura');

    // if (scalaRecapitoFattura) {
    //   await waitForSelectorAndType(page, '#copertura\\.scala_esazione', <string>scalaRecapitoFattura);
    //   logger.info('inserito scala indirizzo recapito fattura');
    // }

    await Promise.all([
      page.waitForResponse(() => true),
      selectByFindOption(
        page, '#copertura\\.provincia_esazione', provinciaRecapitoFattura as string, 'provincia recapito fattura'
      )
    ]);
    logger.info('inserita provincia recapito fattura');

    await Promise.all([
      page.waitForResponse(() => true),
      selectByFindOption(page, '#copertura\\.comune_esazione', comuneRecapitoFattura, 'comune recapito fattura')
    ]);
    logger.info('inserito comune recapito fattura');

    await selezionaCap(page, <string>capRecapitoFattura, '#copertura\\.cap_esazione', 'cap recapito fattura');
    logger.info('inserito cap recapito fattura');

    logger.info('clicco sul pulsante verifica indirizzo fatturazione');
    await waitForSelectorAndClick(page, '#copertura\\.verificaAddress_esazione');

    await waitModaleConfermaIndirizzoAttivazione(page, logger, { timeout: 60000 });
    await waitForModal(page, logger);
  } else {
    await waitForSelectorAndSelect(page, selector, ['1']);
    logger.info('impostato indirizzo fornitura uguale all\'indirizzo della sede legale');
  }
  return Promise.resolve();
}
