import { Page } from 'puppeteer';
import waitForVisibleStyled from '../../../browser/page/waitForVisibleStyled';
import waitForSelectorAndClick from '../../../browser/page/waitForSelectorAndClick';
import waitForNavigation from '../../../browser/page/waitForNavigation';
import { Logger } from '../../../logger';

type ModalTagSelectors = Partial<Record<'modal' | 'title' | 'button', string>>;

export default async function waitForModal(
  page: Page,
  logger: Logger,
  {
    modal = 'body > div.blockUI.blockMsg.blockPage',
    title = '#pop_UpWarningStradario',
    button = 'body > div.blockUI.blockMsg.blockPage input[type=button]'
  }: ModalTagSelectors = {},
  waitOptions?: {
    timeout?: number;
    polling?: string | number;
  }
): Promise<string | undefined> {
  try {
    await waitForVisibleStyled(page, modal, waitOptions);

    const modalMsg = await page.$eval(title, (el) => (el as HTMLSpanElement).innerText.trim());

    logger.info(`Apparsa modale con messaggio ${modalMsg}`);

    await Promise.all([
      waitForSelectorAndClick(page, button),
      waitForNavigation(page)
    ]);

    return modalMsg;
  } catch (ex) {
    // modale non apparsa posso proseguire tranquillamente
    logger.info('Modale non apparsa');
    return undefined;
  }
}
