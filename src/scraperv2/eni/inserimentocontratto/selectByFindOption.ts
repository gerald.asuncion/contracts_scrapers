import { Page } from 'puppeteer';
import getOptionToSelect from './getOptionToSelect';
import waitForSelectorAndSelect from '../../../browser/page/waitForSelectorAndSelect';
import getSelectOptions from './getSelectOptions';

export default async function selectByFindOption(
  page: Page,
  selector: string,
  optionLabel: string,
  errPrefix: string,
  returnIfOne: boolean = false,
): Promise<void> {
  const value = await getOptionToSelect(page, selector, optionLabel, undefined, returnIfOne);

  if (!value) {
    const options = await getSelectOptions(page, selector);
    throw new Error(`valore per selezionare ${errPrefix} ${optionLabel} non trovato. Valori possibili sono: ${options.map(opt => opt.label).filter(Boolean).join('; ')}`);
  }

  await waitForSelectorAndSelect(page, selector, [value]);
}
