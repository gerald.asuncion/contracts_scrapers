import { Page } from "puppeteer";
import waitForSelectorAndClick from "../../../browser/page/waitForSelectorAndClick";
import waitForSelectorAndType from "../../../browser/page/waitForSelectorAndType";
import waitForVisibleStyled from "../../../browser/page/waitForVisibleStyled";
import tryOrThrow from "../../../utils/tryOrThrow";

export default function selezionaCodiceAgente(page: Page, codiceAgente: string) {
  return tryOrThrow(
    async () => {
      await waitForSelectorAndType(page, '#agenteImpAutoComplete', codiceAgente);
      await waitForVisibleStyled(page, 'body > div.ac_results');
      await waitForSelectorAndClick(page, 'body > div.ac_results > ul > li');
    },
    'non sono riuscito ad inserire il codice agente:'
  )
}
