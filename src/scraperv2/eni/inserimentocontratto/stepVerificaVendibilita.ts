import { Page } from 'puppeteer';
import waitForNavigation from '../../../browser/page/waitForNavigation';
import waitForSelectorAndClick from '../../../browser/page/waitForSelectorAndClick';
import waitForVisible from '../../../browser/page/waitForVisible';
import { Logger } from '../../../logger';
import delay from '../../../utils/delay';
import EniInserimentoPayload from '../EniInserimentoPayload';
import checkErroriVerificaValidita from './verificavendibilita/checkErroriVerificaValidita';
import stepVerificaVendibilitaParteDue from './verificavendibilita/stepVerificaVendibilitaParteDue';
import stepVerificaVendibilitaParteUno from './verificavendibilita/stepVerificaVendibilitaParteUno';
import waitForModal from './waitForModal';

// overlay selector = body > div.blockUI.blockMsg.blockPage

export default async function stepVerficaVendibilita(
  page: Page,
  payload: EniInserimentoPayload,
  logger: Logger
): Promise<void> {
  logger.info('step > verifica vendibilità');

  await waitForVisible(page, '#divSingolo');

  await stepVerificaVendibilitaParteUno(page, payload, logger);

  await checkErroriVerificaValidita(page, logger);

  await waitForModal(page, logger, {
    title: 'body > div.blockUI.blockMsg.blockPage tr.subsectiontitle > td',
    button: 'body > div.blockUI.blockMsg.blockPage tr input[type=submit]'
  }, { timeout: 3000 });
  await waitForModal(page, logger, undefined, { timeout: 3000 });

  // try {
  //   await page.waitForSelector('#copertura\\.toponomasticaFornitura', { timeout: 3000 });
  // } catch (error) {
  //   // non è apparso il box per l'inserimento dell'indirizzo di attivazione
  //   // riclicco sul pulsante verifica per far riapparire eventuali errori
  //   await Promise.all([
  //     waitForSelectorAndClick(page, '#copertura\\.verificaPOD_PDR'),
  //     waitForNavigation(page)
  //   ]);
  // }

  // await checkErroriVerificaValidita(page, logger);

  await stepVerificaVendibilitaParteDue(page, payload, logger);

  await Promise.all([
    delay(100),
    waitForSelectorAndClick(page, '#copertura\\.buttonProcedi'),
    waitForNavigation(page)
  ]);

  await checkErroriVerificaValidita(page, logger);
}
