import { Page } from 'puppeteer';
import ifEnabledExecute from '../../../../browser/page/ifEnabledExecute';
import waitForSelectorAndTypeEvaluated from '../../../../browser/page/waitForSelectorAndTypeEvaluated';
import { Logger } from '../../../../logger';
import EniInserimentoPayload from '../../EniInserimentoPayload';
import selezionaCapIndirizzoAttivazione from '../datidelcliente/selezionaCapIndirizzoAttivazione';
import selezionaComuneIndirizzoAttivazione from './selezionaComuneIndirizzoAttivazione';
import selezionaProvinciaIndirizzoAttivazione from './selezionaProvinciaIndirizzoAttivazione';
import selezionaTipoIndirizzoAttivazione from './selezionaTipoIndirizzoAttivazione';

const indirizzoFornituraSelector = '#copertura\\.indirizzoFornitura';

const civicoFornituraSelector = '#copertura\\.civicoFornitura';

const capFornituraSelector = '#copertura\\.capFornitura';

export default async function inserisciIndirizzoAttivazione(
  page: Page,
  {
    tipoIndirizzoAttivazione,
    indirizzoAttivazione,
    civicoIndirizzoAttivazione,
    provinciaIndirizzoAttivazione,
    comuneIndirizzoAttivazione,
    capIndirizzoAttivazione
  }: EniInserimentoPayload,
  logger: Logger
): Promise<void> {
  await selezionaTipoIndirizzoAttivazione(page, tipoIndirizzoAttivazione);
  logger.info(`selezionato tipo indirizzo attivazione ${tipoIndirizzoAttivazione}`);

  await ifEnabledExecute(page, indirizzoFornituraSelector, async () => {
    await waitForSelectorAndTypeEvaluated(page, indirizzoFornituraSelector, indirizzoAttivazione);
    logger.info(`inserito indirizzo attivazione ${indirizzoAttivazione}`);
  });

  await ifEnabledExecute(page, civicoFornituraSelector, async () => {
    await waitForSelectorAndTypeEvaluated(page, civicoFornituraSelector, civicoIndirizzoAttivazione);
    logger.info(`inserito civico indirizzo attivazione ${civicoIndirizzoAttivazione}`);
  });

  await selezionaProvinciaIndirizzoAttivazione(page, provinciaIndirizzoAttivazione);
  logger.info(`inserita provincia indirizzo attivazione ${provinciaIndirizzoAttivazione}`);

  await selezionaComuneIndirizzoAttivazione(page, comuneIndirizzoAttivazione);
  logger.info(`inserito comune indirizzo attivazione ${comuneIndirizzoAttivazione}`);

  await selezionaCapIndirizzoAttivazione(page, capIndirizzoAttivazione, capFornituraSelector);
  logger.info(`inserito cap indirizzo attivazione ${capIndirizzoAttivazione}`);
}
