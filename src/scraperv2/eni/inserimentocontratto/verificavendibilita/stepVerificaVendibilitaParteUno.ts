import { Page } from 'puppeteer';
import { Logger } from '../../../../logger';
import EniInserimentoPayload from '../../EniInserimentoPayload';
import waitForSelectorAndClickEvaluated from '../../../../browser/page/waitForSelectorAndClickEvaluated';
import waitForSelectorAndType from '../../../../browser/page/waitForSelectorAndType';
import waitForSelectorAndSelect from '../../../../browser/page/waitForSelectorAndSelect';
import waitForSelectorAndTypeEvaluated from '../../../../browser/page/waitForSelectorAndTypeEvaluated';
import waitForSelectorAndClick from '../../../../browser/page/waitForSelectorAndClick';
import waitForNavigation from '../../../../browser/page/waitForNavigation';
import { TIPO_FORNITURA_SELECTOR, MODALITA_PAGAMENTO } from '../../constants';

export default async function stepVerificaVendibilitaParteUno(page: Page, {
  tipoFornitura,
  ragioneSociale,
  partitaIva,
  codiceFiscale,
  consumoAnnuo,
  modalitaPagamento,
  pod,
  pdr
}: EniInserimentoPayload, logger: Logger): Promise<void> {
  await waitForSelectorAndClickEvaluated(page, TIPO_FORNITURA_SELECTOR[tipoFornitura]);
  logger.info(`selezionato tipo fornitura ${tipoFornitura}`);

  await waitForSelectorAndType(page, '#copertura\\.cliente_ragionesociale', ragioneSociale);
  logger.info('inserita ragione sociale');

  await waitForSelectorAndType(page, '#copertura\\.anagraficaPartitaIva', partitaIva);
  logger.info('inserita partita iva');

  await waitForSelectorAndType(page, '#copertura\\.cliente_codFiscale', codiceFiscale);
  logger.info('inserito codice fiscale');

  // default
  // await waitForSelectorAndClick(page, '#copertura\\.clienteCondominio_no');
  // logger.info('selezionato cliente condominio no');

  await waitForSelectorAndSelect(page, '#copertura\\.numFornDaInserire', ['1']);
  logger.info('selezionata singola fornitura');

  await waitForSelectorAndTypeEvaluated(page, '#copertura\\.consumoTotAnnuo', (<number>consumoAnnuo).toString());
  logger.info(`inserito consumo annuo totale ${consumoAnnuo}`);

  if (modalitaPagamento) {
    await waitForSelectorAndClickEvaluated(page, MODALITA_PAGAMENTO[modalitaPagamento]);
    logger.info(`selezionata modalita pagamento ${modalitaPagamento}`);
  }

  if (tipoFornitura === 'luce') {
    await waitForSelectorAndTypeEvaluated(page, '#copertura\\.codicePOD_fornitura', <string>pod);
    logger.info(`inserito pod ${pod}`);
  } else {
    await waitForSelectorAndTypeEvaluated(page, '#copertura\\.codicePDR_fornitura', <string>pdr);
    logger.info(`inserito pdr ${pdr}`);
  }

  logger.info('clicco su verifica');
  await Promise.all([
    waitForSelectorAndClick(page, '#copertura\\.verificaPOD_PDR'),
    waitForNavigation(page, { timeout: 120000 })
  ]);
}
