import { Page } from 'puppeteer';
import { Logger } from '../../../../logger';

export default async function checkErroriVerificaValidita(
  page: Page,
  logger: Logger
): Promise<void> {
  logger.info('controllo se ci sono stati errori');

  const errorsMsg = await page.evaluate(() => {
    const list = document.querySelectorAll<HTMLElement>('.errore_reg');
    const errors = Array.from(list).map((el) => el.innerText.trim()).filter((err) => !!err);
    return errors.join('\n');
  });

  if (errorsMsg) {
    throw new Error(errorsMsg);
  }
}
