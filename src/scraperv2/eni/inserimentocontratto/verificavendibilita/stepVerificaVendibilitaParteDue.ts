import { Page } from 'puppeteer';
import { Logger } from '../../../../logger';
import EniInserimentoPayload from '../../EniInserimentoPayload';
import waitForSelectorAndClick from '../../../../browser/page/waitForSelectorAndClick';
import waitForModal from '../waitForModal';
import waitModaleConfermaIndirizzoAttivazione from '../waitModaleConfermaIndirizzoAttivazione';
import inserisciIndirizzoAttivazione from './inserisciIndirizzoAttivazione';
import isDisabled from '../../../../browser/page/isDisabled';

export default async function stepVerificaVendibilitaParteDue(page: Page, payload: EniInserimentoPayload, logger: Logger): Promise<void> {
  await inserisciIndirizzoAttivazione(page, payload, logger);

  if (!(await isDisabled(page, '#copertura\\.toponomasticaFornitura'))) {
    await waitForSelectorAndClick(page, '#copertura\\.verificaAddressFornitura');

    await waitModaleConfermaIndirizzoAttivazione(page, logger, { timeout: 60000 });
    await waitForModal(page, logger, undefined, { timeout: 3000 });
  }
}
