import { Page } from 'puppeteer';
import selectByFindOption from '../selectByFindOption';
import ifEnabledExecute from '../../../../browser/page/ifEnabledExecute';

const selector = '#copertura\\.toponomasticaFornitura';

export default function selezionaTipoIndirizzoAttivazione(
  page: Page,
  tipoIndirizzoAttivazione: string
): Promise<void> {
  return ifEnabledExecute(page, selector, async () => {
    await selectByFindOption(page, selector, tipoIndirizzoAttivazione, 'il tipo indirizzo di attivazione');
  });
}
