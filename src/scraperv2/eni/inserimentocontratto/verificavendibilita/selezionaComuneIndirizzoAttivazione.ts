import { Page } from 'puppeteer';
import selectByFindOption from '../selectByFindOption';
import ifEnabledExecute from '../../../../browser/page/ifEnabledExecute';

const selector = '#copertura\\.comuneFornitura';

export default function selezionaComuneIndirizzoAttivazione(
  page: Page,
  comuneIndirizzoAttivazione: string
): Promise<void> {
  return ifEnabledExecute(page, selector, async () => {
    await Promise.all([
      page.waitForResponse(() => true),
      selectByFindOption(page, selector, comuneIndirizzoAttivazione, 'comune indirizzo di attivazione')
    ]);
  });
}
