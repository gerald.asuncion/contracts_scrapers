import { Page } from 'puppeteer';
import selectByFindOption from '../selectByFindOption';
import ifEnabledExecute from '../../../../browser/page/ifEnabledExecute';

const selector = '#copertura\\.provinciaFornitura';

function _selezionaProvinciaIndirizzoAttivazione(
  page: Page,
  provinciaIndirizzoAttivazione: string
): Promise<void> {
  return (Promise.all([
    page.waitForResponse(() => true),
    selectByFindOption(
      page,
      selector,
      provinciaIndirizzoAttivazione,
      'la provincia indirizzo di attivazione'
    )
  ])
  .then(() => {}));
}

export default function selezionaProvinciaIndirizzoAttivazione(
  page: Page,
  provinciaIndirizzoAttivazione: string
): Promise<void> {
  return ifEnabledExecute(page, selector, async () => {
    try {
      return await _selezionaProvinciaIndirizzoAttivazione(page, provinciaIndirizzoAttivazione);
    } catch (err) {
    }

    return _selezionaProvinciaIndirizzoAttivazione(page, provinciaIndirizzoAttivazione);
  });
}
