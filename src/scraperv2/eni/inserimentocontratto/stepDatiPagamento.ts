import { Page } from 'puppeteer';
import waitForNavigation from '../../../browser/page/waitForNavigation';
import waitForSelectorAndClick from '../../../browser/page/waitForSelectorAndClick';
import waitForSelectorAndType from '../../../browser/page/waitForSelectorAndType';
import { Logger } from '../../../logger';
import extractErrorMessage from '../../../utils/extractErrorMessage';
import { MODALITA_PAGAMENTO } from '../constants';
import EniInserimentoPayload from '../EniInserimentoPayload';
import checkPrivacyChange from '../inserimentoresidenziale/checkPrivacyChange';
import inserisciDatiIntestatario from './datiintestatario/inserisciDatiIntestatario';
import selezionaConsensi from './datiintestatario/selezionaConsensi';
import checkErroriVerificaValidita from './verificavendibilita/checkErroriVerificaValidita';
import waitForModal from './waitForModal';

export default async function stepDatiPagamento(
  page: Page,
  payload: EniInserimentoPayload,
  logger: Logger,
  checkNoSubmit: () => void
): Promise<void> {
  const { modalitaPagamento, iban } = payload;
  logger.info('step > dati pagamento');

  await waitForSelectorAndClick(page, MODALITA_PAGAMENTO[modalitaPagamento]);
  logger.info('selezionata modalità di pagamento');

  await waitForSelectorAndType(page, '#copertura\\.iban', iban);
  logger.info('inserito iban');

  await inserisciDatiIntestatario(page, payload);
  logger.info('inseriti dati intestatario');

  await waitForSelectorAndClick(page, '#copertura\\.venditaExtraCommodity_no');
  logger.info('impostato extracommodity a no');

  await selezionaConsensi(page, payload);
  logger.info('impostati i consensi');

  checkNoSubmit();

  logger.info('clicco sul pulsante "Invia"');

  try {
    await Promise.all([
      waitForSelectorAndClick(page, '#btnAvanti'),
      checkPrivacyChange(page),
      waitForNavigation(page, { timeout: 60000 })
    ]);
  } catch (ex) {
    const errMsg = extractErrorMessage(ex);
    // se è Navigation Timeout Exceeded molto probabilmente è perché c'è qualche consenso a false
    // ma non fa refresh la pagina quando si clicca sul pulsante avanti
    // non succede sempre
    if (!errMsg.includes('Navigation Timeout Exceeded')) {
      throw ex;
    }
  }

  await checkErroriVerificaValidita(page, logger);

  try {
    logger.info('controllo se è apparsa la modale per il cambio dei consensi');
    await waitForModal(page, logger, {
      modal: 'body > div.blockUI.blockMsg.blockPage',
      title: '#pop_UpConsensiPrivacy',
      button: 'body > div.blockUI.blockMsg.blockPage input[type=button][value="Continua"]'
    }, { timeout: 3000 });

    // a volte serve ricontrollare
    await checkErroriVerificaValidita(page, logger);
  } catch (ex) {
    const errMsg = extractErrorMessage(ex);
    // se è timeout Exceeded molto probabilmente è perché la modale non è apparsa
    if (!errMsg.toLowerCase().includes('timeout')) {
      throw ex;
    }
  }
}
