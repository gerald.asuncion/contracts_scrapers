import { Page } from 'puppeteer';
import EniInserimentoPayload from '../EniInserimentoPayload';
import { Logger } from '../../../logger';
import waitForSelectorAndClickEvaluated from '../../../browser/page/waitForSelectorAndClickEvaluated';
import { TIPO_FORNITURA_SELECTOR } from '../constants';
import waitForVisible from '../../../browser/page/waitForVisible';
import waitForSelectorAndClick from '../../../browser/page/waitForSelectorAndClick';
import waitForNavigation from '../../../browser/page/waitForNavigation';
import waitForModal from './waitForModal';
import checkErroriVerificaValidita from './verificavendibilita/checkErroriVerificaValidita';
import inserisciDatiIntestatario from './datidelcliente/inserisciDatiIntestatario';
import inserisciDatiRappresentante from './datidelcliente/inserisciDatiRappresentante';
import inserisciDatiSedeLegale from './datidelcliente/inserisciDatiSedeLegale';
import waitModaleConfermaIndirizzoAttivazione from './waitModaleConfermaIndirizzoAttivazione';
import waitForXPathAndClick from '../../../browser/page/waitForXPathAndClick';

export default async function stepDatiDelCliente(
  page: Page,
  payload: EniInserimentoPayload,
  logger: Logger
): Promise<void> {
  const { tipoFornitura, prodotto } = payload;
  logger.info('step > inserimento dati del cliente');

  // grazie agli step precedenti parte impostata correttamente
  await waitForSelectorAndClickEvaluated(page, TIPO_FORNITURA_SELECTOR[tipoFornitura]);
  logger.info(`selezionato tipo fornitura ${tipoFornitura}`);

  await waitForXPathAndClick(page, `//span[contains(., "${prodotto}")]/parent::td//input[@type="radio"]`);
  logger.info(`selezionato prodotto ${prodotto}`);

  await waitForVisible(page, '#divSingolo');
  await waitForVisible(page, '#copertura\\.formaGiuridica');

  await inserisciDatiIntestatario(page, payload, logger);

  await inserisciDatiRappresentante(page, payload, logger);

  await inserisciDatiSedeLegale(page, payload, logger);

  logger.info('clicco su verfica');
  await waitForSelectorAndClick(page, '#copertura\\.verificaAddress_residenza');

  await waitModaleConfermaIndirizzoAttivazione(page, logger, { timeout: 60000 });
  await waitForModal(page, logger);

  logger.info('clicco sul pulsante avanti per passare al prossimo step');
  await Promise.all([
    waitForSelectorAndClick(page, '#btnAvanti'),
    waitForNavigation(page)
  ]);

  await checkErroriVerificaValidita(page, logger);
}
