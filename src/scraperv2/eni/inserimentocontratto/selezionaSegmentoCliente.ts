import { Page } from "puppeteer";
import waitForSelectorAndSelect from "../../../browser/page/waitForSelectorAndSelect";
import tryOrThrow, { TryOrThrowError } from "../../../utils/tryOrThrow";
import getSelectOptions from "./getSelectOptions";

const SELECT_SELECTOR = '#seleziona\\.segmento_cliente';

export default async function selezionaSegmentoCliente(page: Page, segmentoCliente: string) {
  return tryOrThrow(
    async () => {
      const options = await getSelectOptions(page, SELECT_SELECTOR);
      const optionToSelect = options.find(({ label }) => label.toLowerCase() === segmentoCliente.toLowerCase());

      if (!optionToSelect) {
        throw new TryOrThrowError(`Option per selezionare \`${segmentoCliente}\` non trovata. Le opzioni disponibili sono: ${ options.map(option => option.label).join('; ')}`);
      }

      await waitForSelectorAndSelect(page, SELECT_SELECTOR, [optionToSelect.value]);
    },
    'non sono riuscito a selezionare il segmento cliente:'
  );
}
