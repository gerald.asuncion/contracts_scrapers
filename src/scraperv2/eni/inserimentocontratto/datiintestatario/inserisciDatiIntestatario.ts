import { Page } from 'puppeteer';
import EniInserimentoPayload from '../../EniInserimentoPayload';
import waitForSelectorAndClick from '../../../../browser/page/waitForSelectorAndClick';
import waitForSelectorAndType from '../../../../browser/page/waitForSelectorAndType';
import selectByFindOption from '../selectByFindOption';
import waitForTimeout from '../../../../browser/page/waitForTimeout';
import waitForSelectorAndTypeEvaluated from '../../../../browser/page/waitForSelectorAndTypeEvaluated';

export default async function inserisciDatiIntestatario(
  page: Page,
  {
    tipoIntestatarioSepa,
    nomeSepa,
    cognomeSepa,
    codiceFiscaleSepa,
    partitaIvaSepa,
    ragioneSocialeSepa,
    codiceFiscaleAziendaSepa,
    codiceFiscaleLegaleRappresentante
  }: EniInserimentoPayload
): Promise<void> {
  await waitForSelectorAndClick(page, '#copertura\\.intestarioEqCliente_no');

  await selectByFindOption(page, '#copertura\\.naturaSoggetto', tipoIntestatarioSepa, 'tipo intestatario sepa');

  if (tipoIntestatarioSepa === 'persona fisica') {
    await waitForSelectorAndType(page, '#copertura\\.cognome_intestatario', cognomeSepa);

    await waitForSelectorAndType(page, '#copertura\\.nome_intestatario', nomeSepa);

    await waitForSelectorAndType(page, '#copertura\\.cf_intestatario', codiceFiscaleSepa);
  } else {
    await waitForSelectorAndType(page, '#copertura\\.pivaIntestatario', partitaIvaSepa);

    await waitForSelectorAndType(page, '#copertura\\.ragione_sociale_intestatario', ragioneSocialeSepa);

    await waitForSelectorAndType(page, '#copertura\\.cf_intestatario', codiceFiscaleAziendaSepa);
  }

  await waitForSelectorAndClick(page, '#copertura\\.sottoscrittoreEqFirmatario_si');

  await waitForTimeout(page, 2000);

  await waitForSelectorAndTypeEvaluated(page, '#copertura\\.codiceFiscaleSottoscrittore', codiceFiscaleLegaleRappresentante);
}
