import { Page } from 'puppeteer';
import EniInserimentoPayload from '../../EniInserimentoPayload';
import waitForSelectorAndClick from '../../../../browser/page/waitForSelectorAndClick';

async function selezionaConsenso(
  page: Page,
  yesSelector: string,
  noSelector: string,
  value: boolean
): Promise<void> {
  if (value) {
    await waitForSelectorAndClick(page, yesSelector);
  } else {
    await waitForSelectorAndClick(page, noSelector);
  }
}

export default async function selezionaConsensi(
  page: Page,
  {
    consensoUno,
    consensoDue,
    consensoTre
  }: EniInserimentoPayload
): Promise<void> {
  await selezionaConsenso(page, '#copertura\\.promoEni_si', '#copertura\\.promoEni_no', consensoUno);

  await selezionaConsenso(page, '#copertura\\.analisiMercato_si', '#copertura\\.analisiMercato_no', consensoDue);

  await selezionaConsenso(page, '#copertura\\.promoNonEni_si', '#copertura\\.promoNonEni_no', consensoTre);
}
