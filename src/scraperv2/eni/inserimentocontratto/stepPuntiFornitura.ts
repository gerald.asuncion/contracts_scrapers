import { Page } from 'puppeteer';
import EniInserimentoPayload from '../EniInserimentoPayload';
import { Logger } from '../../../logger';
import inserisciDatiComuniFornitura from './puntifornitura/inserisciDatiComuniFornitura';
import inserisciIndirizzoFornitura from './puntifornitura/inserisciIndirizzoFornitura';
import inserisciIndirizzoFatturazione from './puntifornitura/inserisciIndirizzoFatturazione';
import inserisciFornituraEnergia from './puntifornitura/inserisciFornituraEnergia';
import waitForSelectorAndClick from '../../../browser/page/waitForSelectorAndClick';
import waitForNavigation from '../../../browser/page/waitForNavigation';
import checkErroriVerificaValidita from './verificavendibilita/checkErroriVerificaValidita';
import { WebScraper } from '../../scraper';

export default async function stepPuntiFornitura(
  page: Page,
  payload: EniInserimentoPayload,
  logger: Logger
): Promise<void> {
  logger.info('step > punti fornitura');

  await inserisciDatiComuniFornitura(page, payload, logger);

  await inserisciIndirizzoFornitura(page, payload, logger);

  await inserisciIndirizzoFatturazione(page, payload, logger);

  await inserisciFornituraEnergia(page, payload, logger);

  await page.evaluate(() => {
    if (document && document.activeElement) {
      (document.activeElement as HTMLElement).blur();
    }
  });
  await WebScraper.delay(500);

  await Promise.all([
    waitForNavigation(page),
    waitForSelectorAndClick(page, '#btnAvanti')
  ]);

  await checkErroriVerificaValidita(page, logger);
}
