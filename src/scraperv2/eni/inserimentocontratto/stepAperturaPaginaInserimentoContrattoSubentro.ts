import { Page } from 'puppeteer';
import waitForNavigation from '../../../browser/page/waitForNavigation';
import { Logger } from '../../../logger';
import delay from '../../../utils/delay';

export default async function stepAperturaPaginaInserimentoContrattoSubentro(
  page: Page,
  logger: Logger
): Promise<void> {
  // logger.info('step apertura pagina inserimento contratto');
  // await waitForVisible(page, '#divSingolo > div:nth-child(8)');

  logger.info('vado alla pagina di inserimento nuovo contratto subentro MULTI 2');

  await page.evaluate(() => location.replace("https://webapp.eni.com/domm-web-app/it/eni/domm/switchin/web/cc/goToTroubleshootingAttivazione.do"));
  await waitForNavigation(page);

  // await waitForSelectorAndClick(page, "a#seleziona\\.goToTroubleshootingAttivazione_icon");
  // in test il goto non sta fungendo
  // await page.goto('https://webapp.eni.com/domm-web-app/it/eni/domm/switchin/web/cc/goToTroubleshootingAttivazione.do', { waitUntil: 'networkidle0' });

  await delay(500);
}
