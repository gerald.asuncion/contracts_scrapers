import { Page } from 'puppeteer';
import waitForVisibleStyled from '../../../browser/page/waitForVisibleStyled';
import waitForSelectorAndClick from '../../../browser/page/waitForSelectorAndClick';
import waitForNavigation from '../../../browser/page/waitForNavigation';
import { Logger } from '../../../logger';
// import saveHtml from '../../../browser/page/saveHtml';
import extractErrorMessage from '../../../utils/extractErrorMessage';
import { CONFERMA_INDIRIZZO_NON_TROVATO_ERR } from '../constants';

export default async function waitModaleConfermaIndirizzoAttivazione(
  page: Page,
  logger: Logger,
  waitOptions?: {
    timeout?: number;
    polling?: string | number;
  }
): Promise<void> {
  try {
    logger.info('attendo apertura modale');
    // await saveHtml(page, 'prima-modale', logger);
    await waitForVisibleStyled(page, 'body > div.blockUI.blockMsg.blockPage', waitOptions);
    // await saveHtml(page, 'apparsa-modale', logger);

    logger.info('attendo che sparisca il loading');
    const waitFuncTimeout = (waitOptions?.timeout || 3000) * 2;
    await page.waitForFunction(() => $('#waitImage:visible').length === 0, { polling: 100, timeout: waitFuncTimeout });
    logger.info('loading sparito');

    logger.info('attendo apparizione contenuto modale');
    await waitForVisibleStyled(page, '#pop_UpStradario', waitOptions);

    logger.info('recupero testo modale');
    const modalMsg = await page.$eval('#pop_UpStradario', (el) => (el as HTMLSpanElement).innerText.trim().replace(/\n/g, ''));

    logger.info(`Apparsa modale conferma indirizzo con messaggio ${modalMsg}`);

    if (modalMsg.toLowerCase().includes('forse stavi cercando questi indirizzi?')) {
      const temp = await page.$eval('#pop_UpStradario input:last-child', (el) => (el as HTMLInputElement).value);
      if (temp) {
        logger.info(`Confermo indirizzo ${temp}`);
        await waitForSelectorAndClick(page, '#pop_UpStradario input:last-child');
      } else {
        throw new Error(CONFERMA_INDIRIZZO_NON_TROVATO_ERR);
      }
    } else {
      await Promise.all([
        waitForSelectorAndClick(page, 'body > div.blockUI.blockMsg.blockPage input[type=button]'),
        waitForNavigation(page)
      ]);
    }
  } catch (ex) {
    const errMsg = extractErrorMessage(ex);
    if (errMsg === CONFERMA_INDIRIZZO_NON_TROVATO_ERR) {
      throw ex;
    } else {
      // modale non apparsa posso proseguire tranquillamente
      logger.warn(`waitModaleConfermaIndirizzoAttivazione - ${errMsg}`);
      // await saveHtml(page, 'errore-modale', logger);
    }
  }
}
