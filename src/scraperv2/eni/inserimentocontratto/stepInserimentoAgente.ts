import { Page } from 'puppeteer';
import waitForNavigation from '../../../browser/page/waitForNavigation';
import waitForSelectorAndClick from '../../../browser/page/waitForSelectorAndClick';
import waitForSelectorAndSelect from '../../../browser/page/waitForSelectorAndSelect';
import waitForSelectorAndType from '../../../browser/page/waitForSelectorAndType';
import waitForVisibleStyled from '../../../browser/page/waitForVisibleStyled';
import { Logger } from '../../../logger';
import extractErrorMessage from '../../../utils/extractErrorMessage';
import { CODICE_AGENTE } from '../constants';
import selezionaCodiceAgente from './selezionaCodiceAgente';
import selezionaSegmentoCliente from './selezionaSegmentoCliente';
import checkErroriVerificaValidita from './verificavendibilita/checkErroriVerificaValidita';

const CODICE_AGENTE_NON_VALIDO_ERR = 'codice agente non valido';

export default async function stepInserimentoAgente(page: Page, logger: Logger): Promise<void> {
  logger.info('step > compilazione agente');

  await selezionaCodiceAgente(page, CODICE_AGENTE);
  logger.info('inserito codice agente');

  await selezionaSegmentoCliente(page, 'partiva iva');
  logger.info('selezionato segmento cliente partita iva');

  logger.info('premo invia per far apparire la select dei comparatori');
  await Promise.all([
    waitForSelectorAndClick(page, '#seleziona\\.selezionaRichiesta > table > tbody > tr > td > input[type=button]'),
    waitForNavigation(page)
  ]);

  logger.info('controllo se il codice agente inserito è valido');
  try {
    // eslint-disable-next-line max-len
    const msg = await page.$eval('#divSingolo > table > tbody > tr:nth-child(5) > td:nth-child(3) > span.errore_reg', (el) => el.textContent?.trim());
    if (msg) {
      throw new Error(CODICE_AGENTE_NON_VALIDO_ERR);
    }
  } catch (ex) {
    // console.error('P.D.: ', ex);
    const errMsg = extractErrorMessage(ex);
    if (errMsg === CODICE_AGENTE_NON_VALIDO_ERR) {
      throw ex;
    }
  }

  await waitForSelectorAndSelect(page, '#seleziona\\.codice_canale', ['CTARGEP1']);
  logger.info('selezionato `Comparatori Tariffe`');

  logger.info('premo di nuovo invia per passare al prossimo step');
  await Promise.all([
    waitForSelectorAndClick(page, '#seleziona\\.selezionaRichiesta > table > tbody > tr > td > input[type=button]'),
    waitForNavigation(page)
  ]);

  logger.info('lascio la data di registrazione corrente e premo avanti');
  await Promise.all([
    waitForSelectorAndClick(page, '#copertura\\.inviaDataFirma'),
    waitForNavigation(page)
  ]);

  await checkErroriVerificaValidita(page, logger);
}
