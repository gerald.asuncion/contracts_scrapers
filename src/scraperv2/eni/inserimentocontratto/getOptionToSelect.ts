import {Page} from 'puppeteer';
import {WebScraper} from '../../scraper';

export default async function getOptionToSelect(
  page: Page,
  selectSelector: string,
  label: string,
  map?: (label: string) => string,
  returnIfOne: boolean = false,
): Promise<string | undefined> {
  await WebScraper.delay(250);

  const labelMappata = map ? map(label) : label.toLowerCase().replace(/à/g, "a'");
  return page.evaluate((selector, tipo, returnIfOne) => {
    const elements = document.querySelectorAll<HTMLOptionElement>(`${selector} option`);
    if (returnIfOne && elements.length === 2 && elements[0].value === "0") {
      return elements[1].value;
    }
    const option = Array.from(elements).filter((el) => {
      const text = el.textContent?.toLowerCase()?.trim();
      return text === tipo;
    })[0];
    return option?.value;
  }, selectSelector, labelMappata, returnIfOne);
}
