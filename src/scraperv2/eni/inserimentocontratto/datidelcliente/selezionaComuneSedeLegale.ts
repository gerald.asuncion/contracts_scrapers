import { Page } from 'puppeteer';
import selectByFindOption from '../selectByFindOption';
import ifEnabledExecute from '../../../../browser/page/ifEnabledExecute';

export default async function selezionaComuneSedeLegale(
  page: Page,
  comuneSedeLegale: string
): Promise<void> {
  const selector = '#copertura\\.comune_residenza';
  await ifEnabledExecute(page, selector, async () => Promise.all([
    page.waitForResponse(() => true),
    selectByFindOption(page, selector, comuneSedeLegale, 'comune sede legale')
  ]));
}
