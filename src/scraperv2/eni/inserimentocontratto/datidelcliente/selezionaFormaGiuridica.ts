import { Page } from 'puppeteer';
import selectByFindOption from '../selectByFindOption';
import ifEnabledExecute from '../../../../browser/page/ifEnabledExecute';

export default async function selezionaFormaGiuridica(
  page: Page,
  formaGiuridica: string
): Promise<void> {
  const selector = '#copertura\\.formaGiuridica';
  await ifEnabledExecute(page, selector, async () => Promise.all([
    page.waitForResponse(() => true),
    selectByFindOption(page, selector, formaGiuridica, 'la forma giuridica')
  ]));
}
