import { Page } from 'puppeteer';
import ifEnabledExecute from '../../../../browser/page/ifEnabledExecute';
import delay from '../../../../utils/delay';
import selectByFindOption from '../selectByFindOption';

export default async function selezionaSettoreMerceologico(
  page: Page,
  settoreMerceologico: string
): Promise<void> {
  const selector = '#copertura\\.settMerceologicoAnag';
  await ifEnabledExecute(page, selector, async () => Promise.all([
    page.waitForResponse(() => true),
    delay(100),
    selectByFindOption(page, selector, settoreMerceologico, 'il settore merceologico')
  ]));
}
