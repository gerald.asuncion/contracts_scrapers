import { Page } from 'puppeteer';
import { selezionaCap } from './selezionaCapSedeLegale';

export default function selezionaCapIndirizzoAttivazione(
  page: Page,
  capSedeLegale: string,
  selector: string,
  name = 'cap indirizzo attivazione'
): Promise<void> {
  return selezionaCap(page, capSedeLegale, selector, name);
}
