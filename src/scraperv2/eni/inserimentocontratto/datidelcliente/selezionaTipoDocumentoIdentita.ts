import { Page } from 'puppeteer';
import selectByFindOption from '../selectByFindOption';
import ifEnabledExecute from '../../../../browser/page/ifEnabledExecute';

export default async function selezionaTipoDocumentoIdentita(
  page: Page,
  tipoDocumentoLegaleRappresentante: string
): Promise<void> {
  const selector = '#copertura\\.tipo_documento';
  await ifEnabledExecute(page, selector, () => selectByFindOption(
    page, selector, tipoDocumentoLegaleRappresentante, 'tipo documento identità'
  ));
}
