import { Page } from 'puppeteer';
import waitForSelectorAndSelect from '../../../../browser/page/waitForSelectorAndSelect';

export default async function inserisciDataDiNascitaLegaleRappresentante(
  page: Page,
  dataDiNascitaLegaleRappresentante: string
): Promise<void> {
  const [
    dataDiNascitaLegaleRappresentanteGiorno,
    dataDiNascitaLegaleRappresentanteMese,
    dataDiNascitaLegaleRappresentanteAnno
  ] = dataDiNascitaLegaleRappresentante.split('/');
  await waitForSelectorAndSelect(page, '#fromdatesmall_Day_ID', [parseInt(dataDiNascitaLegaleRappresentanteGiorno).toString()]);

  await waitForSelectorAndSelect(page, '#fromdatesmall_Month_ID', [parseInt(dataDiNascitaLegaleRappresentanteMese).toString()]);

  await waitForSelectorAndSelect(page, '#fromdatesmall_Year_ID', [parseInt(dataDiNascitaLegaleRappresentanteAnno).toString()]);
}
