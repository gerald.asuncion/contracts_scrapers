import { Page } from 'puppeteer';
import selectByFindOption from '../selectByFindOption';

export default function selezionaTipoInterlocutore(
  page: Page,
  tipoInterlocutore: string
): Promise<void> {
  return selectByFindOption(page, '#copertura\\.tipoInterlocutore', tipoInterlocutore, 'tipo interlocutore');
}
