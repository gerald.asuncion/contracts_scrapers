import { Page } from 'puppeteer';
import waitForSelectorAndClickEvaluated from '../../../../browser/page/waitForSelectorAndClickEvaluated';
import waitForSelectorAndType from '../../../../browser/page/waitForSelectorAndType';
import waitForSelectorAndTypeEvaluated from '../../../../browser/page/waitForSelectorAndTypeEvaluated';
import { Logger } from '../../../../logger';
import { SESSO_SELECTOR } from '../../constants';
import EniInserimentoPayload from '../../EniInserimentoPayload';
import inserisciDataDiNascitaLegaleRappresentante from './inserisciDataDiNascitaLegaleRappresentante';
import inserisciDataRilascioDocumentoLegaleRappresentante from './inserisciDataRilascioDocumentoLegaleRappresentante';
import selezionaComuneNascitaLegaleRappresentante from './selezionaComuneNascitaLegaleRappresentante';
import selezionaProvinciaNascitaLegaleRappresentante from './selezionaProvinciaNascitaLegaleRappresentante';
import selezionaTipoDocumentoIdentita from './selezionaTipoDocumentoIdentita';
import selezionaTipoInterlocutore from './selezionaTipoInterlocutore';

export default async function inserisciDatiRappresentante(
  page: Page,
  {
    tipoInterlocutore,
    nomeLegaleRappresentante,
    cognomeLegaleRappresentante,
    provinciaNascitaLegaleRappresentante,
    comuneNascitaLegaleRappresentante,
    dataDiNascitaLegaleRappresentante,
    sessoLegaleRappresentante,
    tipoDocumentoLegaleRappresentante,
    numeroDocumentoLegaleRappresentante,
    dataRilascioDocumentoLegaleRappresentante,
    comuneRilascioDocumentoLegaleRappresentante,
    cellulare,
    email
  }: EniInserimentoPayload,
  logger: Logger
): Promise<void> {
  await selezionaTipoInterlocutore(page, tipoInterlocutore);
  logger.info('selezionato tipo interlocutore');

  await waitForSelectorAndType(page, '#copertura\\.rapprLegale_nome', nomeLegaleRappresentante);
  logger.info('insertio nome legale rappresentante');

  await waitForSelectorAndType(page, '#copertura\\.rapprLegale_cognome', cognomeLegaleRappresentante);
  logger.info('inserito cognome legale rappresentante');

  await selezionaProvinciaNascitaLegaleRappresentante(page, provinciaNascitaLegaleRappresentante);
  logger.info('selezionata provincia di nascita del legale rappresentante');

  await selezionaComuneNascitaLegaleRappresentante(page, comuneNascitaLegaleRappresentante);
  logger.info('selezionato comune di nascita del legale rappresentante');

  await inserisciDataDiNascitaLegaleRappresentante(page, dataDiNascitaLegaleRappresentante);
  logger.info('inserita data di nascita legale rappresentante');

  await waitForSelectorAndClickEvaluated(
    page,
    SESSO_SELECTOR[sessoLegaleRappresentante.toLowerCase()]
  );

  await selezionaTipoDocumentoIdentita(page, tipoDocumentoLegaleRappresentante);
  logger.info('selezionato tipo documento identità del legale rappresentante');

  await waitForSelectorAndTypeEvaluated(page, '#copertura\\.numero_documento', numeroDocumentoLegaleRappresentante);
  logger.info('inserito numero documento del legale rappresentante');

  await inserisciDataRilascioDocumentoLegaleRappresentante(
    page,
    dataRilascioDocumentoLegaleRappresentante
  );

  await waitForSelectorAndTypeEvaluated(page, '#copertura\\.enteRilascio', comuneRilascioDocumentoLegaleRappresentante);
  logger.info('inserito comune rilascio documento del legale rappresentante');

  await waitForSelectorAndTypeEvaluated(page, '#copertura\\.cellulare', cellulare);
  logger.info('inserito cellulare del legale rappresentante');

  await waitForSelectorAndTypeEvaluated(page, '#copertura\\.email_principale', email);
  logger.info('inseria email del legale rappresentante');
}
