import { Page } from 'puppeteer';
import EniInserimentoPayload from '../../EniInserimentoPayload';
import { Logger } from '../../../../logger';
import selezionaFormaGiuridica from './selezionaFormaGiuridica';
import selezionaSettoreMerceologico from './selezionaSettoreMerceologico';
import selezionaAttivitaMerceologica from './selezionaAttivitaMerceologica';
import waitForSelectorAndType from '../../../../browser/page/waitForSelectorAndType';

export default async function inserisciDatiIntestatario(
  page: Page,
  {
    formaGiuridica,
    settoreMerceologico,
    attivitaMerceologica,
    codiceAteco
  }: EniInserimentoPayload,
  logger: Logger
): Promise<void> {
  await selezionaFormaGiuridica(page, formaGiuridica);
  logger.info(`selezionata forma giuridica ${formaGiuridica}`);

  await selezionaSettoreMerceologico(page, settoreMerceologico);
  logger.info(`selezionato settore merceologico ${settoreMerceologico}`);

  await selezionaAttivitaMerceologica(page, attivitaMerceologica);
  logger.info(`selezionato attività merceologica ${attivitaMerceologica}`);

  await waitForSelectorAndType(page, '#copertura\\.codiceAtecoAnagTestuale', codiceAteco);
  logger.info(`inserito codice ateco ${codiceAteco}`);
}
