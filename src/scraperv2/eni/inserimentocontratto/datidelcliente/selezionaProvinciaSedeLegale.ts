import { Page } from 'puppeteer';
import selectByFindOption from '../selectByFindOption';
import ifEnabledExecute from '../../../../browser/page/ifEnabledExecute';

const selector = '#copertura\\.provincia_residenza';

async function _selezionaProvinciaSedeLegale(
  page: Page,
  provinciaSedeLegale: string
): Promise<void> {
  return Promise.all([
    page.waitForResponse(() => true),
    selectByFindOption(page, selector, provinciaSedeLegale, 'provincia sede legale')
  ])
  .then(() => {});
}



export default function selezionaProvinciaSedeLegale(
  page: Page,
  provinciaSedeLegale: string
): Promise<void> {
  return ifEnabledExecute(page, selector, async () => {
    try {
      return await _selezionaProvinciaSedeLegale(page, provinciaSedeLegale);
    } catch (err) {
    }

    return _selezionaProvinciaSedeLegale(page, provinciaSedeLegale);
  });
}
