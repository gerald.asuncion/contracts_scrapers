import { Page } from 'puppeteer';
import EniInserimentoPayload from '../../EniInserimentoPayload';
import { Logger } from '../../../../logger';
import waitForSelectorAndTypeEvaluated from '../../../../browser/page/waitForSelectorAndTypeEvaluated';
import selezionaTipoIndirizzoSedeLegate from './selezionaTipoIndirizzoSedeLegate';
import selezionaProvinciaSedeLegale from './selezionaProvinciaSedeLegale';
import selezionaComuneSedeLegale from './selezionaComuneSedeLegale';
import selezionaCapSedeLegale from './selezionaCapSedeLegale';

export default async function inserisciDatiSedeLegale(
  page: Page,
  {
    tipoIndirizzoSedeLegale,
    indirizzoSedeLegale,
    civicoSedeLegale,
    // scalaSedeLegale,
    provinciaSedeLegale,
    comuneSedeLegale,
    capSedeLegale
  }: EniInserimentoPayload,
  logger: Logger
): Promise<void> {
  await selezionaTipoIndirizzoSedeLegate(page, tipoIndirizzoSedeLegale);
  logger.info('selezionato tipo indirizzo sede legale');

  await waitForSelectorAndTypeEvaluated(page, '#copertura\\.indirizzo_residenza', indirizzoSedeLegale);
  logger.info('inserito indirizzo sede legale');

  await waitForSelectorAndTypeEvaluated(page, '#copertura\\.civico_residenza', civicoSedeLegale);
  logger.info('inserito civico sede legale');

  // if (scalaSedeLegale) {
  //   await waitForSelectorAndType(page, '#copertura\\.scala_residenza', scalaSedeLegale);
  //   logger.info('inserita scala sede legale');
  // }

  await selezionaProvinciaSedeLegale(page, provinciaSedeLegale);
  logger.info('inserita provincia sede legale');

  await selezionaComuneSedeLegale(page, comuneSedeLegale);
  logger.info('selezionato comune sede legale');

  await selezionaCapSedeLegale(page, capSedeLegale);
  logger.info('selezionato cap sede legale');
}
