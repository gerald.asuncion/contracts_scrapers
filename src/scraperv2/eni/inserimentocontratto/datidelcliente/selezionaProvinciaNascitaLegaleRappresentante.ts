import { Page } from 'puppeteer';
import selectByFindOption from '../selectByFindOption';
import ifEnabledExecute from '../../../../browser/page/ifEnabledExecute';

export default async function selezionaProvinciaNascitaLegaleRappresentante(
  page: Page,
  provinciaNascitaLegaleRappresentante: string
): Promise<void> {
  const selector = '#copertura\\.provinciaNascita';
  await ifEnabledExecute(page, selector, async () => Promise.all([
    page.waitForResponse(() => true),
    selectByFindOption(page, selector, provinciaNascitaLegaleRappresentante, 'provincia di nascita')
  ]));
}
