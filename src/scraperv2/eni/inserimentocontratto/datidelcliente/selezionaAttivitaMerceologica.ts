import { Page } from 'puppeteer';
import selectByFindOption from '../selectByFindOption';
import ifEnabledExecute from '../../../../browser/page/ifEnabledExecute';

export default async function selezionaAttivitaMerceologica(
  page: Page,
  attivitaMerceologica: string
): Promise<void> {
  const selector = '#copertura\\.attMerceologica';
  await ifEnabledExecute(page, selector, async () => Promise.all([
    page.waitForResponse(() => true),
    selectByFindOption(page, selector, attivitaMerceologica, 'attività merceologica')
  ]));
}
