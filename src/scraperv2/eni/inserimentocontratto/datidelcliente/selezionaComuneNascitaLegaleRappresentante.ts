import { Page } from 'puppeteer';
import selectByFindOption from '../selectByFindOption';

export default async function selezionaComuneNascitaLegaleRappresentante(
  page: Page,
  comuneNascitaLegaleRappresentante: string
): Promise<void> {
  await selectByFindOption(
    page,
    '#copertura\\.comuneNascita',
    comuneNascitaLegaleRappresentante,
    'comune di nascita'
  );
}
