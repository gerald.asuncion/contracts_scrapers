import { Page } from 'puppeteer';
import selectByFindOption from '../selectByFindOption';
import ifEnabledExecute from '../../../../browser/page/ifEnabledExecute';

export default async function selezionaTipoIndirizzoSedeLegate(
  page: Page,
  tipoIndirizzoSedeLegale: string
): Promise<void> {
  const selector = '#copertura\\.toponomastica_residenza';
  await ifEnabledExecute(page, selector, () => selectByFindOption(
    page, selector, tipoIndirizzoSedeLegale, 'tipo indirizzo sede legale'
  ));
}
