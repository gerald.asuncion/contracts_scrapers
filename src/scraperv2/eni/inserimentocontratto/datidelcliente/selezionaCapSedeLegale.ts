import { Page } from 'puppeteer';
import ifEnabledExecute from '../../../../browser/page/ifEnabledExecute';
import getSelectOptions from '../getSelectOptions';
import selectByFindOption from '../selectByFindOption';

export async function selezionaCap(
  page: Page,
  cap: string,
  selector: string,
  name: string
): Promise<void> {
  await ifEnabledExecute(page, selector, async () => {
    const options = (await getSelectOptions(page, selector))
    .filter(({ label }) => label.replace(/-/g, '').trim());

    if (!options.length) {
      throw Error(`Nessuna opzione con il '${name}' ('${selector}').`);
    }

    const isNotContained = options.findIndex(({ label, value }) => value === cap) < 0;

    if (isNotContained && options.length > 1) {
      throw Error(`Opzioni multiple non combacianti con il '${name}' ('${selector}').`);
    }

    await selectByFindOption(
      page,
      selector,
      isNotContained ? options[0].value : cap,
      name
    );
  });
}

export default function selezionaCapSedeLegale(
  page: Page,
  capSedeLegale: string
): Promise<void> {
  return selezionaCap(page, capSedeLegale, '#copertura\\.cap_residenza', 'cap sede legale');
}
