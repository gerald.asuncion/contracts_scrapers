import { Page } from 'puppeteer';
import waitForSelectorAndSelect from '../../../../browser/page/waitForSelectorAndSelect';

export default async function inserisciDataRilascioDocumentoLegaleRappresentante(
  page: Page,
  dataRilascioDocumentoLegaleRappresentante: string
): Promise<void> {
  const [rilascioGiorno, rilascioMese, rilascioAnno] = dataRilascioDocumentoLegaleRappresentante.split('/');
  await waitForSelectorAndSelect(page, '#reldatesmall_Day_ID', [parseInt(rilascioGiorno).toString()]);
  await waitForSelectorAndSelect(page, '#reldatesmall_Month_ID', [parseInt(rilascioMese).toString()]);
  await waitForSelectorAndSelect(page, '#reldatesmall_Year_ID', [parseInt(rilascioAnno).toString()]);
}
