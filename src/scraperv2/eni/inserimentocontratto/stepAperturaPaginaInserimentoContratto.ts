import { Page } from 'puppeteer';
import { Logger } from '../../../logger';
import waitForVisible from '../../../browser/page/waitForVisible';

export default async function stepAperturaPaginaInserimentoContratto(
  page: Page,
  logger: Logger
): Promise<void> {
  logger.info('step apertura pagina inserimento contratto');
  await waitForVisible(page, '#divSingolo > div:nth-child(8)');

  logger.info('vado alla pagina di inserimento nuovo contratto');
  await page.goto('https://webapp.eni.com/domm-web-app/it/eni/domm/switchin/web/cc/goNuovoContratto.do', { waitUntil: 'networkidle2' });
}
