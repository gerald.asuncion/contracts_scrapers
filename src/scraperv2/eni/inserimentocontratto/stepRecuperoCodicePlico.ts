import { Page } from 'puppeteer';
import { Logger } from '../../../logger';
import waitForSelectorAndClick from '../../../browser/page/waitForSelectorAndClick';
import waitForNavigation from '../../../browser/page/waitForNavigation';

export default async function stepRecuperoCodicePlico(
  page: Page,
  logger: Logger
): Promise<string | undefined> {
  logger.info('step > recupero codice plico');

  await Promise.all([
    waitForSelectorAndClick(page, '#copertura\\.btnInviaDoubleOptIn'),
    waitForNavigation(page, { timeout: 120000 })
  ]);

  await page.waitForSelector('#codiceProposta');

  const codicePlico = await page.$eval('#codiceProposta', (el) => el.textContent?.trim());

  return codicePlico;
}
