import { Page } from 'puppeteer';
import { WebScraper } from '../../scraper';

export default async function getSelectOptions(
  page: Page,
  selectSelector: string,
): Promise<{ label: string, value: string }[]> {
  await WebScraper.delay(250);

  return page.evaluate((selector, tipo) => {
    const elements = document.querySelectorAll<HTMLOptionElement>(`${selector} option`);

    return Array.from(elements).map(({ textContent, value }) => ({
      label: textContent || value || '',
      value: value || textContent || ''
    }));
  }, selectSelector);
}
