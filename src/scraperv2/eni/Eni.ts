import { WebScraper, ScraperResponse } from '../scraper';
import FailureResponse from '../../response/FailureResponse';
import SuccessResponse from '../../response/SuccessResponse';
import createChildPayloadAwareLogger from '../../utils/createChildPayloadAwareLogger';
import waitForSelectorAndType from '../../browser/page/waitForSelectorAndType';
import waitForSelectorAndClick from '../../browser/page/waitForSelectorAndClick';
import waitForNavigation from '../../browser/page/waitForNavigation';
import EniInserimentoPayload from './EniInserimentoPayload';
import waitForVisibleStyled from '../../browser/page/waitForVisibleStyled';
import stepAperturaPaginaInserimentoContratto from './inserimentocontratto/stepAperturaPaginaInserimentoContratto';
import stepInserimentoAgente from './inserimentocontratto/stepInserimentoAgente';
import stepVerficaVendibilita from './inserimentocontratto/stepVerificaVendibilita';
import waitForVisible from '../../browser/page/waitForVisible';
import stepDatiDelCliente from './inserimentocontratto/stepDatiDelCliente';
import stepPuntiFornitura from './inserimentocontratto/stepPuntiFornitura';
import stepDatiPagamento from './inserimentocontratto/stepDatiPagamento';
import stepRecuperoCodicePlico from './inserimentocontratto/stepRecuperoCodicePlico';
import extractErrorMessage from '../../utils/extractErrorMessage';
import WrongCredentialsError from '../../errors/WrongCredentialsError';
import { SCRAPER_TIPOLOGIA } from '../ScraperOptions';
import { QueueTask } from '../../task/QueueTask';

@QueueTask()
export default class Eni extends WebScraper {
  static readonly LOGIN_URL = 'https://webapp.eni.com/';

  async login(payload: EniInserimentoPayload): Promise<void> {
    const logger = createChildPayloadAwareLogger(this.childLogger, payload);
    const { username, password } = await this.getCredenziali();

    const page = await this.p();

    logger.info('login > inizio');
    await page.goto(Eni.LOGIN_URL);

    logger.info('login > controllo se l\'utente è già loggato');
    try {
      await waitForVisible(page, '#divSingolo > div:nth-child(8) > div:nth-child(1)', { timeout: 3000 });
      logger.info('login > utente già loggato');
      return;
    } catch (ex) {
      // non sono nella home page quindi posso procedere
    }

    await page.waitForSelector('#centreMainFullContainer');

    await waitForSelectorAndType(page, '#txtLoginUserID', username);
    logger.info('login > inserito username');

    await waitForSelectorAndType(page, '#txtPassword', password);
    logger.info('login > inserita password');

    await Promise.all([
      waitForSelectorAndClick(page, '#centreMainFullContainer > form:nth-child(3) > div:nth-child(2) > input:nth-child(2)'),
      waitForNavigation(page)
    ]);

    try {
      await waitForVisibleStyled(page, '#errorMessage', { timeout: 3000 });
      throw new WrongCredentialsError(Eni.LOGIN_URL, username);
    } catch (ex) {
      if (ex instanceof WrongCredentialsError) {
        throw ex;
      }
      // nessun errore posso procedere tranquillamente
    }

    logger.info('login > fine');
  }

  async scrapeWebsite(payload: EniInserimentoPayload): Promise<ScraperResponse> {
    const logger = createChildPayloadAwareLogger(this.childLogger, payload);

    const page = await this.p();

    try {
      await stepAperturaPaginaInserimentoContratto(page, logger);

      await stepInserimentoAgente(page, logger);

      await stepVerficaVendibilita(page, payload, logger);

      await stepDatiDelCliente(page, payload, logger);

      await stepPuntiFornitura(page, payload, logger);

      await stepDatiPagamento(page, payload, logger, () => this.checkNoSubmit(payload as any));

      const codicePlico = await stepRecuperoCodicePlico(page, logger);

      if (!codicePlico) {
        throw new Error('codice plico non trovato');
      }

      logger.info(`recuperato codice plico: ${codicePlico}`);

      return new SuccessResponse(codicePlico);
    } catch (ex) {
      await this.saveScreenshot(page, logger, 'eni-errore', payload.idDatiContratto);
      const errMsg = extractErrorMessage(ex);
      logger.error(errMsg);
      return new FailureResponse(errMsg);
    }
  }

  getScraperCodice() {
    return 'eni';
  }

  getScraperTipologia() {
    return SCRAPER_TIPOLOGIA.INSERIMENTO;
  }
}
