/**
 * @openapi
 *
 * /scraper/v2/eni/fornitori/gas:
 *  post:
 *    tags:
 *      - v2
 *    description: Restituisce la lista di fornitori gas, andandoli a recuperare dal portale.
 *    consume:
 *      - application/json
 *    produces:
 *      - application/json
 *    responses:
 *      200:
 *        description: SuccessResponse
 *    parameters:
 *      - in: body
 *        description: non serve.
 *        schema:
 *          type: object
 */
