export const EniProdotto = {
  TREND_GAS_BUSINESS: 'Trend Business gas (Base)',
  PLACET_FISSA_GAS_ALTRI_USI: 'Eni Plenitude PLACET fissa gas altri usi (Base)',
  PLACET_VARIABILE_GAS_ALTRI_USI: 'Eni Plenitude PLACET variabile gas altri usi (Base)',
  TREND_LUCE_BUSINESS: 'Trend Business Luce (Base)',
  PLACET_FISSA_LUCE_ALTRI_USI: 'Eni Plenitude PLACET fissa luce altri usi (Base)',
  PLACET_VARIABILE_LUCE_ALTRI_USI: 'Eni Plenitude PLACET variabile luce altri usi (Base)'
} as const;

export type EniProdotto = typeof EniProdotto[keyof typeof EniProdotto];
