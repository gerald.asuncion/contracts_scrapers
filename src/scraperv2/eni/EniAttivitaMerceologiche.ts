import DbScraper from '../DbScraper';
import EniCodiciAtecoRepo from '../../repo/EniCodiciAtecoRepo';
import { ScraperResponse } from '../scraper';
import FailureResponse from "../../response/FailureResponse";
import EniActionOptions from './EniActionOptions';
import createChildLogger from '../../utils/createChildLogger';
import createChildPayloadAwareLogger from '../../utils/createChildPayloadAwareLogger';
import extractErrorMessage from '../../utils/extractErrorMessage';
import GenericSuccessResponse from '../../response/GenericSuccessResponse';
import { SCRAPER_TIPOLOGIA } from '../ScraperOptions';
import { QueueTask } from '../../task/QueueTask';

interface EniAttivitaMerceologicheParams {
  settoreMerceologico: string;
}

@QueueTask()
export default class EniAttivitaMerceologiche extends DbScraper {
  private eniCodiciAtecoRepo: EniCodiciAtecoRepo;

  constructor(options: EniActionOptions) {
    super(options);
    this.eniCodiciAtecoRepo = options.eniCodiciAtecoRepo;
    this.logger = createChildLogger(options.logger, 'EniAttivitaMerceologiche');
  }

  async scrape(
    args: any,
    { settoreMerceologico, ...restArgs }: EniAttivitaMerceologicheParams
  ): Promise<ScraperResponse> {
    const logger = createChildPayloadAwareLogger(this.logger, { ...args, ...restArgs });

    try {
      logger.info(`carico le attività merceologiche relative al settore merceologico ${settoreMerceologico}`);
      const repo = this.eniCodiciAtecoRepo;
      const records = await repo.caricaAttivitaMerceologiche(settoreMerceologico);

      return new GenericSuccessResponse<Array<string>>(records);
    } catch (ex) {
      const errMsg = extractErrorMessage(ex);
      logger.error(errMsg);
      return new FailureResponse(errMsg);
    }
  }

  getScraperCodice() {
    return 'eni-attivitamerceologiche';
  }

  getScraperTipologia() {
    return SCRAPER_TIPOLOGIA.IMPORTDATA;
  }
}
