/**
 * @openapi
 *
 * /scraper/v2/eni/codice-ateco/{attivitaMerceologica}:
 *  post:
 *    tags:
 *      - v2
 *    description: Restiutisce la lista dei codici ateco per l'attività merceologia interessata presi dal portale di Eni.
 *    consume:
 *      - application/json
 *    produces:
 *      - application/json
 *    responses:
 *      200:
 *        description: SuccessResponse
 *    parameters:
 *      - in: path
 *        name: attivitaMerceologica
 *        required: true
 *        schema:
 *          type: string
 *        description: l'attività merceologia interessata
 *      - in: body
 *        description: non serve.
 *        schema:
 *          type: object
 */
