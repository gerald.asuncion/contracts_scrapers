/**
 * @openapi
 *
 * /scraper/v2/eniResidenziale/inserimento:
 *  post:
 *    tags:
 *      - v2
 *    description:
 *      Inserisce il contratto, coi dati passati, sul portale di Eni (Residenziale).
 *      Questa è la rotta diretta allo scraper, quindi non usa il meccanismo del webhook.
 *    consume:
 *      - application/json
 *    produces:
 *      - application/json
 *    responses:
 *      200:
 *        description: SuccessResponse
 *    parameters:
 *      - in: body
 *        description: i dati da inserire.
 *        schema:
 *          type: object
 *          required:
 *            - codiceAgente
 *            - segmentoCliente
 *            - offertaPartnershipFastweb
 *            - canale
 *            - decorrenzaImmediata
 *            - tipoFornitura
 *            - nome
 *            - cognome
 *            - codiceFiscale
 *            - attivazioneToponimo
 *            - attivazioneIndirizzo
 *            - attivazioneCivico
 *            - attivazioneProvincia
 *            - attivazioneComune
 *            - attivazioneCap
 *            - metodoPagamento
 *            - offerta
 *            - documentoNumero
 *            - documentoTipo
 *            - documentoDataEmissione
 *            - cellulare
 *            - email
 *            - indirizzoFornituraCorrispondeIndirizzoAbitazioneResidenza
 *            - esazioneCorrisponde
 *            - indirizzoInterlocutore
 *            - tipologiaAbitazione
 *            - titolaritaFornitura
 *            - modalitaSpedizioneBolletta
 *            - eniWebbolletta
 *            - extraCommodity
 *            - consensoPromoEni
 *            - consensoAnalisiMercato
 *            - consensoPromoNonEni
 *          properties:
 *            codiceAgente:
 *              type: string
 *            segmentoCliente:
 *              type: string
 *            offertaPartnershipFastweb:
 *              type: string
 *              enum: [si, no]
 *            canale:
 *              type: string
 *            decorrenzaImmediata:
 *              type: boolean
 *            tipoFornitura:
 *              type: string
 *              enum: [luce, gas, dual]
 *            nome:
 *              type: string
 *            cognome:
 *              type: string
 *            codiceFiscale:
 *              type: string
 *            pod:
 *              type: string
 *              nullable: true
 *              description: necessario se luce o dual
 *            pdr:
 *              type: string
 *              nullable: true
 *              description: necessario se gas o dual
 *            attivazioneToponimo:
 *              type: string
 *            attivazioneIndirizzo:
 *              type: string
 *            attivazioneCivico:
 *              type: string
 *            attivazioneProvincia:
 *              type: string
 *            attivazioneComune:
 *              type: string
 *            attivazioneCap:
 *              type: string
 *            attivazioneScala:
 *              type: string
 *              nullable: true
 *            metodoPagamento:
 *              type: string
 *              enum: [bollettino, domiciliazione]
 *            tipologiaUso:
 *              type: string
 *              nullable: true
 *              enum: [abitazione, pertinenza]
 *            offerta:
 *              type: string
 *            documentoNumero:
 *              type: string
 *            documentoTipo:
 *              type: string
 *              enum: [carta d'identità, patente, passaporto]
 *            documentoDataEmissione:
 *              type: string
 *            cellulare:
 *              type: string
 *            email:
 *              type: string
 *            indirizzoFornituraCorrispondeIndirizzoAbitazioneResidenza:
 *              type: string
 *              enum: [si, no]
 *            residenzaToponimo:
 *              type: string
 *              nullable: true
 *            residenzaIndirizzo:
 *              type: string
 *              nullable: true
 *            residenzaCivico:
 *              type: string
 *              nullable: true
 *            residenzaProvincia:
 *              type: string
 *              nullable: true
 *            residenzaComune:
 *              type: string
 *              nullable: true
 *            residenzaCap:
 *              type: string
 *              nullable: true
 *            residenzaScala:
 *              type: string
 *              nullable: true
 *            esazioneCorrisponde:
 *              type: string
 *              enum: [all'indirizzo di fornitura, all'indirizzo di residenza, ad un altro indirizzo]
 *            esazioneToponimo:
 *              type: string
 *              nullable: true
 *            esazioneIndirizzo:
 *              type: string
 *              nullable: true
 *            esazioneCivico:
 *              type: string
 *              nullable: true
 *            esazioneProvincia:
 *              type: string
 *              nullable: true
 *            esazioneComune:
 *              type: string
 *              nullable: true
 *            esazioneCap:
 *              type: string
 *              nullable: true
 *            esazioneScala:
 *              type: string
 *              nullable: true
 *            indirizzoInterlocutore:
 *              type: string
 *              enum: [indirizzo di residenza, indirizzo di fornitura, indirizzo di esazione, altro indirizzo]
 *            attualeFornitoreEnergia:
 *              type: string
 *              nullable: true
 *            potenzaImpegnata:
 *              type: string
 *              nullable: true
 *            potenzaDisponibile:
 *              type: string
 *              nullable: true
 *            consumoAnnuoElettricita:
 *              type: string
 *              nullable: true
 *            attualeFornitoreGas:
 *              type: string
 *              nullable: true
 *            utilizzoGas:
 *              type: string
 *              nullable: true
 *            consumoAnnuoGas:
 *              type: string
 *              nullable: true
 *            tipologiaAbitazione:
 *              type: string
 *            titolaritaFornitura:
 *              type: string
 *              enum: [Proprietario, Conduttore, Detentore ad altro titolo (ad es. comodato)]
 *            modalitaSpedizioneBolletta:
 *              type: string
 *              enum: [posta sintetica, digitale]
 *            iban:
 *              type: string
 *              nullable: true
 *            eniWebbolletta:
 *              type: boolean
 *            extraCommodity:
 *              type: string
 *              enum: [si, no]
 *            consensoPromoEni:
 *              type: string
 *              enum: [si, no]
 *            consensoAnalisiMercato:
 *              type: string
 *              enum: [si, no]
 *            consensoPromoNonEni:
 *              type: string
 *              enum: [si, no]
 *
 * /scraper/queue/eniResidenziale:
 *  post:
 *    tags:
 *      - v2
 *      - queue
 *    description:
 *      Inserisce in queue il task per inserire il contratto sul portale eni.
 *      Il payload è lo stesso della rotta diretta `/scraper/v2/eniResidenziale/inserimento`,
 *      in più va aggiunto l'id dati contratto ed il webhook che lo scraper poi andrà a richiamare per notificare l'esito.
 *    consume:
 *      - application/json
 *    produces:
 *      - application/json
 *    responses:
 *      200:
 *        description: SuccessResponse
 */
