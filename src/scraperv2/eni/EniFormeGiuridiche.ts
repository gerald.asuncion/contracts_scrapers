import DbScraper from '../DbScraper';
import EniCodiciAtecoRepo from '../../repo/EniCodiciAtecoRepo';
import { ScraperResponse } from '../scraper';
import FailureResponse from "../../response/FailureResponse";
import EniActionOptions from './EniActionOptions';
import createChildLogger from '../../utils/createChildLogger';
import createChildPayloadAwareLogger from '../../utils/createChildPayloadAwareLogger';
import extractErrorMessage from '../../utils/extractErrorMessage';
import GenericSuccessResponse from '../../response/GenericSuccessResponse';
import { SCRAPER_TIPOLOGIA } from '../ScraperOptions';
import { QueueTask } from '../../task/QueueTask';

@QueueTask()
export default class EniFormeGiuridiche extends DbScraper {
  private eniCodiciAtecoRepo: EniCodiciAtecoRepo;

  constructor(options: EniActionOptions) {
    super(options);
    this.eniCodiciAtecoRepo = options.eniCodiciAtecoRepo;
    this.logger = createChildLogger(options.logger, 'EniFormeGiuridiche');
  }

  async scrape(args: any): Promise<ScraperResponse> {
    const logger = createChildPayloadAwareLogger(this.logger, args);

    try {
      logger.info('carico le forme giuridiche');
      const records = await this.eniCodiciAtecoRepo.caricaFormeGiuridiche();

      return new GenericSuccessResponse<Array<string>>(records);
    } catch (ex) {
      const errMsg = extractErrorMessage(ex);
      logger.error(errMsg);
      return new FailureResponse(errMsg);
    }
  }

  getScraperCodice() {
    return 'eni-formegiuridiche';
  }

  getScraperTipologia() {
    return SCRAPER_TIPOLOGIA.IMPORTDATA;
  }
}
