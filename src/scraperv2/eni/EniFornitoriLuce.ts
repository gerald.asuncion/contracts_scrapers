import EniFornitori from './fornitori/EniFornitori';
import { ScraperResponse } from '../scraper';
import eniFornitoriLucePayload from './fornitori/eniFornitoriLucePayload';
import { QueueTask } from '../../task/QueueTask';

@QueueTask()
export default class EniFornitoriLuce extends EniFornitori {
  /**
   * @override
   */
  scrapeWebsite(): Promise<ScraperResponse> {
    return super.scrapeWebsite(eniFornitoriLucePayload);
  }
}
