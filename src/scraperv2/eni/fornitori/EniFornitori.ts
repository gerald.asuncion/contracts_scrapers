import Eni from '../Eni';
import { ScraperResponse } from '../../scraper';
import FailureResponse from '../../../response/FailureResponse';
import EniInserimentoPayload from '../EniInserimentoPayload';
import stepAperturaPaginaInserimentoContratto from '../inserimentocontratto/stepAperturaPaginaInserimentoContratto';
import stepInserimentoAgente from '../inserimentocontratto/stepInserimentoAgente';
import stepVerficaVendibilita from '../inserimentocontratto/stepVerificaVendibilita';
import stepDatiDelCliente from '../inserimentocontratto/stepDatiDelCliente';
import extractErrorMessage from '../../../utils/extractErrorMessage';
import estraiFornitori from './estraiFornitori';
import GenericSuccessResponse from '../../../response/GenericSuccessResponse';
import createChildPayloadAwareLogger from '../../../utils/createChildPayloadAwareLogger';

export default class EniFornitori extends Eni {
  /**
   * @override
   */
  async scrapeWebsite(payload: EniInserimentoPayload): Promise<ScraperResponse> {
    const logger = createChildPayloadAwareLogger(this.childLogger, payload);

    const page = await this.p();

    try {
      await stepAperturaPaginaInserimentoContratto(page, logger);

      await stepInserimentoAgente(page, logger);

      await stepVerficaVendibilita(page, payload, logger);

      await stepDatiDelCliente(page, payload, logger);

      const fornitori = await estraiFornitori(page, payload, logger);

      return new GenericSuccessResponse<Array<string>>(fornitori);
    } catch (ex) {
      await this.saveScreenshot(page, logger, 'eni-fornitori-errore');
      const errMsg = extractErrorMessage(ex);
      logger.error(errMsg);
      return new FailureResponse(errMsg);
    }
  }
}
