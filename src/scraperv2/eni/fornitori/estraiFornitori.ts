import { Page } from 'puppeteer';
import EniInserimentoPayload from '../EniInserimentoPayload';
import { Logger } from '../../../logger';
import waitForSelectorAndClick from '../../../browser/page/waitForSelectorAndClick';
import { OCCUPAZIONE_SELECTOR } from '../constants';
import waitForSelectorAndSelect from '../../../browser/page/waitForSelectorAndSelect';

export default async function estraiFornitori(
  page: Page,
  {
    tipoFornitura,
    occupazione
  }: EniInserimentoPayload,
  logger: Logger
): Promise<Array<string>> {
  await waitForSelectorAndClick(page, '#copertura\\.fattUnica_no');
  logger.info('impostata fatturazione unica a no');

  let occupazioneSelector = OCCUPAZIONE_SELECTOR[occupazione.toLowerCase()];
  if (!occupazioneSelector) {
    occupazioneSelector = OCCUPAZIONE_SELECTOR.altro;
  }
  await waitForSelectorAndSelect(page, '#copertura\\.titolarita', [occupazioneSelector]);
  logger.info('selezionata occupazione');

  // eslint-disable-next-line max-len
  const listSelector = tipoFornitura === 'luce' ? '#copertura\\.attualeFornitorePow_fornitura option' : '#copertura\\.attualeFornitoreGas_fornitura option';
  const fornitori: Array<string> = await page.evaluate((selector): Array<string> => {
    const opzioni = document.querySelectorAll<HTMLOptionElement>(selector);
    return Array.from(opzioni).slice(1).map((el) => el.textContent?.trim()) as Array<string>;
  }, listSelector);

  return fornitori;
}
