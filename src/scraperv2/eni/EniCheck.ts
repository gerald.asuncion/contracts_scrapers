import waitForNavigation from '../../browser/page/waitForNavigation';
import { STATO_CONTRATTO } from '../../contratti/StatoContratto';
import ErrorResponse from '../../response/ErrorResponse';
import GenericSuccessResponse from '../../response/GenericSuccessResponse';
import { QueueTask } from '../../task/QueueTask';
import createChildPayloadAwareLogger from '../../utils/createChildPayloadAwareLogger';
import extractErrorMessage from '../../utils/extractErrorMessage';
import { ScraperResponse } from '../scraper';
import controllaStatoCodiciPlico from './checkstato/controllaStatoCodicePlico';
import Eni from './Eni';
import EniInserimentoPayload from './EniInserimentoPayload';

// TODO: trovare una soluzione intelligente !!!!!!
//       non dovrebbe estendere EniInserimentoPayload
interface EniCheckPayload extends EniInserimentoPayload {
  codicePlicoArray: Array<string>;
}

type EniCheckResult = Record<string, string>;

@QueueTask({ scraperName: 'eniCheckFirme' })
export default class EniCheck extends Eni {
  /**
   * @override
   * @param payload Request payload
   */
  async scrapeWebsite(payload: EniCheckPayload): Promise<ScraperResponse> {
    const logger = createChildPayloadAwareLogger(this.childLogger, payload);
    const page = await this.p();

    try {
      await Promise.all([
        page.goto('https://webapp.eni.com/domm-web-app/it/eni/domm/switchin/web/cc/goToCercaDoubleOptInContratti.do'),
        waitForNavigation(page)
      ]);

      const result: EniCheckResult = {};

      for await (const {
        codicePlico,
        stato
      } of controllaStatoCodiciPlico(page, payload.codicePlicoArray, logger)) {
        let res: STATO_CONTRATTO = stato === 'confermato' ? STATO_CONTRATTO.FIRMATO : STATO_CONTRATTO.DA_FIRMARE;
        if (stato === 'la ricerca non ha restituito risultati.') {
          res = STATO_CONTRATTO.NO_RESULT;
        }
        result[codicePlico] = res;
      }

      return new GenericSuccessResponse<EniCheckResult>(result);
    } catch (ex) {
      await this.saveScreenshot(page, logger, 'eni-check-errore', payload.idDatiContratto);
      return new ErrorResponse(extractErrorMessage(ex));
    }
  }
}
