/**
 * @openapi
 *
 * /scraper/v2/eni/check:
 *  post:
 *    tags:
 *      - v2
 *    description:
 *      Controlla lo stato dei contratti passati.
 *    consume:
 *      - application/json
 *    produces:
 *      - application/json
 *    responses:
 *      200:
 *        description: SuccessResponse
 *    parameters:
 *      - in: body
 *        description: La lista di contratti da controllare.
 *        schema:
 *          type: object
 *          required:
 *            - codicePlicoArray
 *          properties:
 *            codicePlicoArray:
 *              type: array
 *              items:
 *                type: string
 */
