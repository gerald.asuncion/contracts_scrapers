import DbScraper from '../DbScraper';
import EniCodiciAtecoRepo from '../../repo/EniCodiciAtecoRepo';
import { ScraperResponse } from '../scraper';
import FailureResponse from "../../response/FailureResponse";
import EniActionOptions from './EniActionOptions';
import createChildLogger from '../../utils/createChildLogger';
import createChildPayloadAwareLogger from '../../utils/createChildPayloadAwareLogger';
import extractErrorMessage from '../../utils/extractErrorMessage';
import GenericSuccessResponse from '../../response/GenericSuccessResponse';
import { SCRAPER_TIPOLOGIA } from '../ScraperOptions';
import { QueueTask } from '../../task/QueueTask';

interface EniCodiciAtecoParams {
  attivitaMerceologica: string;
}

@QueueTask()
export default class EniCodiciAteco extends DbScraper {
  private eniCodiciAtecoRepo: EniCodiciAtecoRepo;

  constructor(options: EniActionOptions) {
    super(options);
    this.eniCodiciAtecoRepo = options.eniCodiciAtecoRepo;
    this.logger = createChildLogger(options.logger, 'EniCodiciAteco');
  }

  async scrape(
    args: any,
    { attivitaMerceologica, ...restArgs }: EniCodiciAtecoParams
  ): Promise<ScraperResponse> {
    const logger = createChildPayloadAwareLogger(this.logger, { ...args, ...restArgs });

    try {
      logger.info(`carico i codici ateco relativi all'attività merceologica ${attivitaMerceologica}`);
      const repo = this.eniCodiciAtecoRepo;
      const records = await repo.caricaCodiciAteco(attivitaMerceologica);

      return new GenericSuccessResponse<Array<{ label: string; ateco: string; }>>(records);
    } catch (ex) {
      const errMsg = extractErrorMessage(ex);
      logger.error(errMsg);
      return new FailureResponse(errMsg);
    }
  }

  getScraperCodice() {
    return 'eni-codiciateco';
  }

  getScraperTipologia() {
    return SCRAPER_TIPOLOGIA.IMPORTDATA;
  }
}
