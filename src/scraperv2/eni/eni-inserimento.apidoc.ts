/**
 * @openapi
 *
 * /scraper/v2/eni/inserimento:
 *  post:
 *    tags:
 *      - v2
 *    description:
 *      Inserisce il contratto, coi dati passati, sul portale di Eni (Business).
 *      Questa è la rotta diretta allo scraper, quindi non usa il meccanismo del webhook.
 *    consume:
 *      - application/json
 *    produces:
 *      - application/json
 *    responses:
 *      200:
 *        description: SuccessResponse
 *    parameters:
 *      - in: body
 *        description: i dati da inserire.
 *        schema:
 *          type: object
 *          required:
 *            - tipoFornitura
 *            - ragioneSociale
 *            - partitaIva
 *            - codiceFiscale
 *            - consumoAnnuo
 *            - modalitaPagamento
 *            - tipoIndirizzoAttivazione
 *            - indirizzoAttivazione
 *            - civicoIndirizzoAttivazione
 *            - provinciaIndirizzoAttivazione
 *            - comuneIndirizzoAttivazione
 *            - capIndirizzoAttivazione
 *            - prodotto
 *            - formaGiuridica
 *            - settoreMerceologico
 *            - attivitaMerceologica
 *            - codiceAteco
 *            - tipoInterlocutore
 *            - nomeLegaleRappresentante
 *            - cognomeLegaleRappresentante
 *            - provinciaNascitaLegaleRappresentante
 *            - comuneNascitaLegaleRappresentante
 *            - dataDiNascitaLegaleRappresentante
 *            - sessoLegaleRappresentante
 *            - tipoDocumentoLegaleRappresentante
 *            - numeroDocumentoLegaleRappresentante
 *            - dataRilascioDocumentoLegaleRappresentante
 *            - comuneRilascioDocumentoLegaleRappresentante
 *            - cellulare
 *            - email
 *            - tipoIndirizzoSedeLegale
 *            - indirizzoSedeLegale
 *            - civicoSedeLegale
 *            - provinciaSedeLegale
 *            - comuneSedeLegale
 *            - capSedeLegale
 *            - occupazione
 *            - attualeFornitore
 *            - mercatoProvenienza
 *            - agevolazione
 *            - iban
 *            - tipoIntestatarioSepa
 *            - nomeSepa
 *            - cognomeSepa
 *            - codiceFiscaleSepa
 *            - partitaIvaSepa
 *            - ragioneSocialeSepa
 *            - codiceFiscaleAziendaSepa
 *            - codiceFiscaleLegaleRappresentante
 *            - consensoUno
 *            - consensoDue
 *            - consensoTre
 *          properties:
 *            tipoFornitura:
 *              type: string
 *              enum: [luce, gas]
 *            ragioneSociale:
 *              type: string
 *            partitaIva:
 *              type: string
 *            codiceFiscale:
 *              type: string
 *            consumoAnnuo:
 *              type: number
 *            modalitaPagamento:
 *              type: string
 *              enum: [SDD, Bollettino]
 *            pod:
 *              type: string
 *              nullable: true
 *              description: necessario se luce
 *            pdr:
 *              type: string
 *              nullable: true
 *              description: necessario se gas
 *            tipoIndirizzoAttivazione:
 *              type: string
 *            indirizzoAttivazione:
 *              type: string
 *            civicoIndirizzoAttivazione:
 *              type: string
 *            provinciaIndirizzoAttivazione:
 *              type: string
 *            comuneIndirizzoAttivazione:
 *              type: string
 *            capIndirizzoAttivazione:
 *              type: string
 *            prodotto:
 *              type: string
 *              enum: [Link Luce Business, Link Gas Business]
 *            formaGiuridica:
 *              type: string
 *            settoreMerceologico:
 *              type: string
 *            attivitaMerceologica:
 *              type: string
 *            codiceAteco:
 *              type: string
 *            tipoInterlocutore:
 *              type: string
 *              enum: [Cliente, Legale rappresentante, Referente]
 *            nomeLegaleRappresentante:
 *              type: string
 *            cognomeLegaleRappresentante:
 *              type: string
 *            provinciaNascitaLegaleRappresentante:
 *              type: string
 *            comuneNascitaLegaleRappresentante:
 *              type: string
 *            dataDiNascitaLegaleRappresentante:
 *              type: string
 *            sessoLegaleRappresentante:
 *              type: string
 *              enum: [M, F]
 *            tipoDocumentoLegaleRappresentante:
 *              type: string
 *              enum: [carta d'identità, patente, passaporto]
 *            numeroDocumentoLegaleRappresentante:
 *              type: string
 *            dataRilascioDocumentoLegaleRappresentante:
 *              type: string
 *            comuneRilascioDocumentoLegaleRappresentante:
 *              type: string
 *            cellulare:
 *              type: string
 *            email:
 *              type: string
 *            tipoIndirizzoSedeLegale:
 *              type: string
 *            indirizzoSedeLegale:
 *              type: string
 *            civicoSedeLegale:
 *              type: string
 *            provinciaSedeLegale:
 *              type: string
 *            comuneSedeLegale:
 *              type: string
 *            capSedeLegale:
 *              type: string
 *            occupazione:
 *              type: string
 *              enum: [proprietario, inquilino, comodatario, usufruttuario, usufruttuario_altro]
 *            attualeFornitore:
 *              type: string
 *            mercatoProvenienza:
 *              type: string
 *              enum: [Libero, Tutelato, Salvaguardia]
 *            tensione:
 *              type: string
 *              nullable: true
 *              description: necessario se luce
 *            agevolazione:
 *              type: boolean
 *            tipoIndirizzoRecapitoFattura:
 *              type: string
 *              nullable: true
 *            indirizzoRecapitoFattura:
 *              type: string
 *              nullable: true
 *            civicoRecapitoFattura:
 *              type: string
 *              nullable: true
 *            provinciaRecapitoFattura:
 *              type: string
 *              nullable: true
 *            comuneRecapitoFattura:
 *              type: string
 *              nullable: true
 *            capRecapitoFattura:
 *              type: string
 *              nullable: true
 *            potenza:
 *              type: string
 *              nullable: true
 *            iban:
 *              type: string
 *            tipoIntestatarioSepa:
 *              type: string
 *              enum: [persona fisica, persona giuridica]
 *            nomeSepa:
 *              type: string
 *            cognomeSepa:
 *              type: string
 *            codiceFiscaleSepa:
 *              type: string
 *            partitaIvaSepa:
 *              type: string
 *            ragioneSocialeSepa:
 *              type: string
 *            codiceFiscaleAziendaSepa:
 *              type: string
 *            codiceFiscaleLegaleRappresentante:
 *              type: string
 *            consensoUno:
 *              type: boolean
 *            consensoDue:
 *              type: boolean
 *            consensoTre:
 *              type: boolean
 *
 * /scraper/queue/eni:
 *  post:
 *    tags:
 *      - v2
 *      - queue
 *    description:
 *      Inserisce in queue il task per inserire il contratto sul portale eni.
 *      Il payload è lo stesso della rotta diretta `/scraper/v2/eni/inserimento`,
 *      in più va aggiunto l'id dati contratto ed il webhook che lo scraper poi andrà a richiamare per notificare l'esito.
 *    consume:
 *      - application/json
 *    produces:
 *      - application/json
 *    responses:
 *      200:
 *        description: SuccessResponse
 */
