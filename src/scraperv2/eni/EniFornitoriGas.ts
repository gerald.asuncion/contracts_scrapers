import EniFornitori from './fornitori/EniFornitori';
import { ScraperResponse } from '../scraper';
import eniFornitoriGasPayload from './fornitori/eniFornitoriGasPayload';
import { QueueTask } from '../../task/QueueTask';

@QueueTask()
export default class EniFornitoriGas extends EniFornitori {
  /**
   * @override
   */
  scrapeWebsite(): Promise<ScraperResponse> {
    return super.scrapeWebsite(eniFornitoriGasPayload);
  }
}
