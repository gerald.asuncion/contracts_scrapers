/**
 * @openapi
 *
 * /scraper/v2/eni/fornitori/luce:
 *  post:
 *    tags:
 *      - v2
 *    description: Restituisce la lista di fornitori luce, andandoli a recuperare dal portale.
 *    consume:
 *      - application/json
 *    produces:
 *      - application/json
 *    responses:
 *      200:
 *        description: SuccessResponse
 *    parameters:
 *      - in: body
 *        description: non serve.
 *        schema:
 *          type: object
 */
