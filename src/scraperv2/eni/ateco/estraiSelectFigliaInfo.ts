import { Page } from 'puppeteer';
import OptionInfo from '../../../browser/page/OptionInfo';

export default async function* estraiSelectFigliaInfo(
  page: Page,
  action: string,
  parentId: string,
  parentValue: string
): AsyncGenerator<OptionInfo> {
  const list = await page.evaluate((actionName, paramId, paramValue) => {
    function caricaSelectData() {
      const params = new URLSearchParams();
      params.append('action', actionName);
      params.append(paramId, paramValue);
      const url = `https://webapp.eni.com//domm-web-app/framework/forward/forwardwo.jsp?${params.toString()}`;

      return fetch(url).then((resp) => {
        if (!resp.ok) {
          throw new Error(resp.statusText);
        }

        return resp.arrayBuffer();
      })
        .then((buffer) => {
          const decoder = new TextDecoder('iso-8859-1');
          const decoded = decoder.decode(buffer);
          return JSON.parse(decoded);
        })
        .catch(() => []);
    }

    return caricaSelectData();
  }, action, parentId, parentValue);

  for (const info of list) {
    yield info;
  }
}
