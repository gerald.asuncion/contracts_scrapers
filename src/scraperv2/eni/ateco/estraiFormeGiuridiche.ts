import { Page } from 'puppeteer';
import { Logger } from '../../../logger';
import estraiOptionsInfo from '../../../browser/page/estraiOptionsInfo';
import OptionInfo from '../../../browser/page/OptionInfo';

export default async function* estraiFormeGiuridiche(
  page: Page,
  logger: Logger
): AsyncGenerator<OptionInfo> {
  logger.info('estraggo la lista di forme giuridiche');
  const formeGiuridiche = await estraiOptionsInfo(page, '#copertura\\.formaGiuridica');

  for (const formaGiuridica of formeGiuridiche) {
    yield formaGiuridica;
  }
}
