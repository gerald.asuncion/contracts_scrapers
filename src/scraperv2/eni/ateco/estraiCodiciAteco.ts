import { Page } from 'puppeteer';
import EniCodiciAtecoResult from './EniCodiciAtecoResult';
import { Logger } from '../../../logger';
import estraiFormeGiuridiche from './estraiFormeGiuridiche';
import estraiSelectFigliaInfo from './estraiSelectFigliaInfo';

export default async function estraiCodiciAteco(
  page: Page,
  logger: Logger
): Promise<EniCodiciAtecoResult> {
  const result: EniCodiciAtecoResult = [];

  for await (const {
    label: formaGiuridicaLabel,
    value: formaGiuridicaValue
  } of estraiFormeGiuridiche(page, logger)) {
    logger.info(`estraggo la lista di settori merceologici per la forma giuridica ${formaGiuridicaLabel}`);
    for await (const {
      label: settoreMerceologicoLabel,
      value: settoreMerceologicoValue
    } of estraiSelectFigliaInfo(page, 'caricaSettoriMerceologici', 'formaGiuridica', formaGiuridicaValue)) {
      logger.info(`estraggo la lista di attività merceologiche per la forma giuridica ${formaGiuridicaLabel} ed il settore ${settoreMerceologicoLabel}`);
      for await (const {
        label: attivitaMerceologicaLabel,
        value: attivitaMerceologicaValue
      } of estraiSelectFigliaInfo(page, 'caricaAttivitaMerceologiche', 'settMerceologico', settoreMerceologicoValue)) {
        logger.info(`estraggo i codici ateco per la forma giuridica ${formaGiuridicaLabel}, il settore ${settoreMerceologicoLabel} e l'attività merceologica ${attivitaMerceologicaLabel}`);
        for await (const {
          label: codiceAtecoLabel,
          value: codiceAtecoValue
        } of estraiSelectFigliaInfo(page, 'caricaCodiciAteco', 'attMerceologica', attivitaMerceologicaValue)) {
          result.push([
            formaGiuridicaLabel,
            settoreMerceologicoLabel,
            attivitaMerceologicaLabel,
            codiceAtecoLabel,
            codiceAtecoValue
          ]);
        }
      }
    }
  }

  return result;
}
