/**
 * @openapi
 *
 * /scraper/v2/eni/attivita-merceologica/{settoreMerceologico}:
 *  post:
 *    tags:
 *      - v2
 *    description: Restiutisce la lista delle attività merceologiche per il settore merceologico interessato prese dal portale di Eni.
 *    consume:
 *      - application/json
 *    produces:
 *      - application/json
 *    responses:
 *      200:
 *        description: SuccessResponse
 *    parameters:
 *      - in: path
 *        name: settoreMerceologico
 *        required: true
 *        schema:
 *          type: string
 *        description: il settore merceologico interessato
 *      - in: body
 *        description: non serve.
 *        schema:
 *          type: object
 */
