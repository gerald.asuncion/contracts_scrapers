import { EniProdotto } from "./EniProdotto";

export const CODICE_AGENTE = 'AG231615987';

// delay di attesa post selezione campi che fanno aggiornare altri campi
// es: seleziono provinca e poi comune
export const SPINNER_DELAY = 4000;

export const TIPO_FORNITURA_SELECTOR: Record<string, string> = {
  luce: 'input[value="POWER"]',
  gas: 'input[value="GAS"]'
};

export const MODALITA_PAGAMENTO = {
  SDD: 'input[value="CC"]',
  Bollettino: 'input[value="BB"]'
};

export const PRODOTTO_SELECTOR: Record<EniProdotto, string> = {
  'Eni Plenitude PLACET fissa gas altri usi (Base)': 'input[value="BASE221_GPLFIXAU221"]',
  'Eni Plenitude PLACET variabile gas altri usi (Base)': 'input[value="BASE221_GPLVARAU221"]',
  'Trend Business gas (Base)': 'input[value="BASE221_TNDVG221"]',
  'Eni Plenitude PLACET fissa luce altri usi (Base)': 'input[value="BASE221_LPLFIXAU221"]',
  'Eni Plenitude PLACET variabile luce altri usi (Base)': 'input[value="BASE221_LPLVARAU221"]',
  'Trend Business Luce (Base)': 'input[value="BASE221_TNDVL221"]',
};

export const SESSO_SELECTOR: Record<string, string> = {
  m: 'input[value="M"]',
  f: 'input[value="F"]'
};

export const OCCUPAZIONE_SELECTOR: Record<string, string> = {
  proprietario: 'PROPRIETARIO',
  inquilino: 'CONDUTTORE',
  altro: 'TITOLARE_ALTRO_DIRITTO'
};

export const AGEVOLAZIONE_SELECTOR: Record<string, string> = {
  true: '1',
  false: '0'
};

export const POTENZA_DISPONIBILE: Record<string, string> = {
  3: '3,3',
  '4,5': '5',
  6: '6,6',
  10: '11',
  20: '22',
  25: '27,5',
  30: '33'
};

export const CONFERMA_INDIRIZZO_NON_TROVATO_ERR = 'indirizzo da confermare non trovato';
