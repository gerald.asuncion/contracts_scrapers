import { DbScraperOptions } from '../DbScraper';
import EniCodiciAtecoRepo from '../../repo/EniCodiciAtecoRepo';

export default interface EniActionOptions extends DbScraperOptions {
  eniCodiciAtecoRepo: EniCodiciAtecoRepo;
}
