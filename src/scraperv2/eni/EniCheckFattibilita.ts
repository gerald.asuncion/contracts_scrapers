import lookForVisibleWithTextElements from '../../browser/page/lookForVisibleWithTextElements';
import waitForSelectorAndClick from '../../browser/page/waitForSelectorAndClick';
import createChildLogger from '../../utils/createChildLogger';
import createChildPayloadAwareLogger from '../../utils/createChildPayloadAwareLogger';
import extractErrorMessage from '../../utils/extractErrorMessage';
import { ScraperResponse } from '../scraper';
import ErrorResponse from '../../response/ErrorResponse';
import FailureResponse from '../../response/FailureResponse';
import SuccessResponse from '../../response/SuccessResponse';
import ScraperOptions from '../ScraperOptions';
import Eni from './Eni';
import EniInserimentoPayload from './EniInserimentoPayload';
import stepAperturaPaginaInserimentoContratto from './inserimentocontratto/stepAperturaPaginaInserimentoContratto';
import stepInserimentoAgente from './inserimentocontratto/stepInserimentoAgente';
import stepVerificaVendibilitaParteUno from './inserimentocontratto/verificavendibilita/stepVerificaVendibilitaParteUno';
import stepVerificaVendibilitaParteDue from './inserimentocontratto/verificavendibilita/stepVerificaVendibilitaParteDue';
import waitForModal from './inserimentocontratto/waitForModal';
import waitForNavigation from '../../browser/page/waitForNavigation';
import { getOptionText, getInputText } from '../../browser/page/getElementText';
import waitForVisible from '../../browser/page/waitForVisible';
import isDisabled from '../../browser/page/isDisabled';
import { QueueTask } from '../../task/QueueTask';

type EniCheckFattibilitaPayload = Pick<EniInserimentoPayload, 'tipoFornitura' | 'ragioneSociale' | 'partitaIva' | 'codiceFiscale' | 'pod' | 'pdr' | 'consumoAnnuo' | 'idDatiContratto'>;

@QueueTask({ scraperName: 'eniCheckFattibilita' })
export default class EniCheckFattiblita extends Eni {
  constructor(options: ScraperOptions) {
    super(options);
    this.childLogger = createChildLogger(options.logger, 'EniCheckFattibilita');
  }

  /**
   * @override
   * @param payload Request payload
   */
  async scrapeWebsite(_payload: EniCheckFattibilitaPayload): Promise<ScraperResponse> {
    const logger = createChildPayloadAwareLogger(this.childLogger, _payload);

    const payload = {
      ..._payload,
      consumoAnnuo: 1000
    };

    const page = await this.p();

    try {
      await stepAperturaPaginaInserimentoContratto(page, logger);

      logger.info('controllo se è apparsa la form per l\'inserimento dei dati dell\'agente');
      await waitForVisible(page, '#seleziona\\.selezionaRichiesta', { timeout: 60000 });

      await stepInserimentoAgente(page, logger);

      await waitForSelectorAndClick(page, '#copertura\\.clienteCondominio_no');
      logger.info('seleziona \'NO\' per \'Il cliente corrisponde a un condominio?\'');

      await stepVerificaVendibilitaParteUno(page, payload as EniInserimentoPayload, logger);

      const errorMessage = (await lookForVisibleWithTextElements(page, '.error_reg, .errore_reg'))[0];
      if (errorMessage) {
        // casi: 2, 3, 6
        const { text } = errorMessage;

        logger.error(text);
        await this.saveScreenshot(page, logger, `${payload.pod || payload.pdr}-eni-errore`);
        return new FailureResponse(text);
      }

      await waitForModal(page, logger, {
        title: 'body > div.blockUI.blockMsg.blockPage tr.subsectiontitle > td',
        button: 'body > div.blockUI.blockMsg.blockPage tr input[type=submit]'
      }, { timeout: 3000 });

      // ESITO 4
      let esito4Response: FailureResponse | null = null;
      if (await isDisabled(page, '#copertura\\.indirizzoFornitura')) {
        const tipo = await getOptionText(page, '#copertura\\.toponomasticaFornitura option[selected]');

        const indirizzo = await getInputText(page, '#copertura\\.indirizzoFornitura');
        const civico = await getInputText(page, '#copertura\\.civicoFornitura');

        const provincia = await getOptionText(page, '#copertura\\.provinciaFornitura option[selected]');
        const comune = await getOptionText(page, '#copertura\\.comuneFornitura option[selected]');
        const cap = await getOptionText(page, '#copertura\\.capFornitura option[selected]');

        const podOrPdr = payload.tipoFornitura === 'luce' ? 'pod' : 'pdr';
        const indirizzoCompleto = `${tipo} ${indirizzo} ${civico} ${provincia} ${comune} ${cap}`;
        esito4Response = new SuccessResponse(`il ${podOrPdr} risulta già inserito a sistema con questo indirizzo ${indirizzoCompleto}`);
      } else {
        logger.info('inserisco indirizzo fissato per poter cliccare sul pulsante avanti');
        await stepVerificaVendibilitaParteDue(
          page,
          {
            tipoIndirizzoAttivazione: 'via',
            indirizzoAttivazione: 'dante',
            civicoIndirizzoAttivazione: '10',
            provinciaIndirizzoAttivazione: 'milano',
            comuneIndirizzoAttivazione: 'milano',
            capIndirizzoAttivazione: '20121'
          } as never,
          logger
        );
      }

      try {
        await Promise.all([
          waitForSelectorAndClick(page, '#copertura\\.buttonProcedi'),
          waitForNavigation(page)
        ]);
      } catch (ex) {
        const errMsg = extractErrorMessage(ex);
        if (!errMsg.includes('Navigation Timeout Exceeded')) {
          throw ex;
        }
      }

      // ESITO 5 (ATTENZIONE priorità maggiore rispetto ad ESITO 4)
      const esito5ErrorMessage = (await lookForVisibleWithTextElements(page, '.error_reg, .errore_reg'))[0];
      if (esito5ErrorMessage) {
        const { text } = esito5ErrorMessage;

        logger.error(text);
        await this.saveScreenshot(page, logger, `${payload.pod || payload.pdr}-eni-errore`);
        return new FailureResponse(text);
      }

      if (esito4Response) {
        logger.error(esito4Response.details);
        return esito4Response;
      }

      logger.info('check fattibilità OK');
      return new SuccessResponse();
    } catch (ex) {
      await this.saveScreenshot(page, logger, `${payload.pod || payload.pdr}-eni-errore`);
      const errMsg = extractErrorMessage(ex);
      logger.error(errMsg);
      return new ErrorResponse(errMsg);
    }
  }
}
