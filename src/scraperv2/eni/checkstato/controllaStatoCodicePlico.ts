import { Page } from 'puppeteer';
import waitForNavigation from '../../../browser/page/waitForNavigation';
import waitForSelectorAndClick from '../../../browser/page/waitForSelectorAndClick';
import waitForSelectorAndTypeEvaluated from '../../../browser/page/waitForSelectorAndTypeEvaluated';
import waitForVisible from '../../../browser/page/waitForVisible';
import { Logger } from '../../../logger';

async function controllaStatoCodicePlico(page: Page,
  codicePlico: string,
  logger: Logger): Promise<string> {
  await waitForSelectorAndTypeEvaluated(page, '#codiceProposta', codicePlico);

  await Promise.all([
    // eslint-disable-next-line max-len
    waitForSelectorAndClick(page, '#centreMainFullContainer > form > div.pannello.margineSup.margineMenu.margineMenuIE6 > input[type=submit]:nth-child(1)'),
    waitForNavigation(page, { timeout: 120000 })
  ]);

  await waitForVisible(page, '#centreMainFullContainer > div.sectionBox.pannello.margineSup.margineMenu.margineMenuIE6');

  try {
    const stato = await page.$eval(
      "#centreMainFullContainer > div.sectionBox.pannello.margineSup.margineMenu.margineMenuIE6 > table > tbody > tr:nth-child(2) > td",
      (el) => el.textContent?.toLowerCase().trim());
      if (stato === 'la ricerca non ha restituito risultati.') {
        return <string>stato
      }

  } catch (ignored) {}

  // eslint-disable-next-line max-len
  const stato = await page.$eval('#centreMainFullContainer > div.sectionBox.pannello.margineSup.margineMenu.margineMenuIE6 > table > tbody > tr:nth-child(3) > td > table > tbody > tr.datagrid-even > td:nth-child(6)', (el) => el.textContent?.toLowerCase()?.trim());
  logger.info(`per il codice plico ${codicePlico} recuperato lo stato ${stato}`);

  return <string>stato;
}

export default async function* controllaStatoCodiciPlico(
  page: Page,
  codicePlicoArray: Array<string>,
  logger: Logger
): AsyncGenerator<{ codicePlico: string; stato: string; }> {
  const codicePlico = codicePlicoArray.pop();

  if (codicePlico) {
    const stato = await controllaStatoCodicePlico(page, codicePlico, logger);
    yield { codicePlico, stato };
  }

  if (codicePlicoArray.length) {
    yield* controllaStatoCodiciPlico(page, codicePlicoArray, logger);
  }
}
