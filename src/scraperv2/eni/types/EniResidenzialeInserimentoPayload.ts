import ContrattoPayload from "../../../contratti/ContrattoPayload";

export type YesOrNo = 'si' | 'no';

export type EniResidenzialeInserimentoPayload = Required<ContrattoPayload> & {
  tipologiaContratto?: "attivazione" | "subentro";
  codiceAgente: string;
  segmentoCliente: string;
  offertaPartnershipFastweb: YesOrNo;
  canale: string;
  decorrenzaImmediata: boolean;
  tipoFornitura: 'gas' | 'luce' | 'dual';
  nome: string;
  cognome: string;
  codiceFiscale: string;
  sesso?: 'M' | 'F',
  comuneDiRilascio?: string;
  nazionalita?: string;
  luogoNascita?: string;
  dataNascita?: string;
  pod?: string;
  pdr?: string;
  attivazioneToponimo: string;
  attivazioneIndirizzo: string;
  attivazioneCivico: string;
  attivazioneProvincia: string;
  attivazioneComune: string;
  attivazioneCap: string;
  attivazioneScala?: string;
  metodoPagamento: 'bollettino' | 'domiciliazione';
  tipologiaUso?: 'abitazione' | 'pertinenza';
  offerta: string;
  documentoNumero: string;
  documentoTipo: "carta d'identità" | 'patente' | 'passaporto';
  documentoDataEmissione: string;
  documentoComuneRilascio: string;
  cellulare: string;
  email: string;
  indirizzoFornituraCorrispondeIndirizzoAbitazioneResidenza: YesOrNo;
  residenzaToponimo?: string;
  residenzaIndirizzo?: string;
  residenzaCivico?: string;
  residenzaProvincia?: string;
  residenzaComune?: string;
  residenzaCap?: string;
  residenzaScala?: string;
  esazioneCorrisponde: "all'indirizzo di fornitura" | "all'indirizzo di residenza" | 'ad un altro indirizzo';
  esazioneToponimo?: string;
  esazioneIndirizzo?: string;
  esazioneCivico?: string;
  esazioneProvincia?: string;
  esazioneComune?: string;
  esazioneCap?: string;
  esazioneScala?: string;
  indirizzoInterlocutore: 'indirizzo di residenza' | 'indirizzo di fornitura' | 'indirizzo di esazione' | 'altro indirizzo';
  // luce
  attualeFornitoreEnergia?: string;
  potenzaImpegnata?: string;
  potenzaDisponibile?: string;
  consumoAnnuoElettricita?: string;
  // gas
  attualeFornitoreGas?: string;
  utilizzoGas?: Array<'cottura' | 'acqua calda' | 'riscaldamento'>;
  consumoAnnuoGas?: string;
  // luce, gas, dual
  tipologiaAbitazione: string;
  titolaritaFornitura: 'Proprietario' | 'Conduttore' | 'Detentore ad altro titolo (ad es. comodato)';
  modalitaSpedizioneBolletta: 'posta sintetica' | 'digitale';
  iban?: string;
  eniWebbolletta: boolean;
  extraCommodity: YesOrNo;
  consensoPromoEni: YesOrNo;
  consensoAnalisiMercato: YesOrNo;
  consensoPromoNonEni: YesOrNo;
  intestatarioContoNome?: string;
  intestatarioContoCognome?: string;
  intestatarioContoCodiceFiscale?: string;
};
