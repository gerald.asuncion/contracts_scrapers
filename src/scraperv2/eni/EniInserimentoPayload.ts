import { EniProdotto } from './EniProdotto';
import ContrattoPayload from '../../contratti/ContrattoPayload';

export type TipoFornitura = 'luce' | 'gas';

export type ModalitaPagamento = 'SDD' | 'Bollettino';

export type TipoInterlocutore = 'Cliente' | 'Legale rappresentante' | 'Referente';

export type TipoDocumento = "carta d'identità" | 'patente' | 'passaporto';

export type Sesso = 'M' | 'F';

export type EniOccupazione = 'proprietario' | 'inquilino' | 'comodatario' | 'usufruttuario' | 'usufruttuario_altro';

export type MercatoProvenienza = 'Libero' | 'Tutelato' | 'Salvaguardia';

export type TipoIntestataro = 'persona fisica' | 'persona giuridica';

export default interface EniInserimentoPayload extends Required<ContrattoPayload> {
  tipoFornitura: TipoFornitura;

  // verifica vendibilità
  ragioneSociale: string;
  partitaIva: string;
  codiceFiscale: string;
  consumoAnnuo: number;
  modalitaPagamento: ModalitaPagamento;
  pod: string | null;
  pdr: string | null;
  tipoIndirizzoAttivazione: string;
  indirizzoAttivazione: string;
  civicoIndirizzoAttivazione: string;
  provinciaIndirizzoAttivazione: string;
  comuneIndirizzoAttivazione: string;
  capIndirizzoAttivazione: string;

  // dati del cliente
  prodotto: EniProdotto;
  formaGiuridica: string;
  settoreMerceologico: string;
  attivitaMerceologica: string;
  codiceAteco: string;
  tipoInterlocutore: TipoInterlocutore;
  nomeLegaleRappresentante: string;
  cognomeLegaleRappresentante: string;
  provinciaNascitaLegaleRappresentante: string; // possibile estero
  comuneNascitaLegaleRappresentante: string;
  dataDiNascitaLegaleRappresentante: string;
  sessoLegaleRappresentante: Sesso;
  tipoDocumentoLegaleRappresentante: TipoDocumento;
  numeroDocumentoLegaleRappresentante: string;
  dataRilascioDocumentoLegaleRappresentante: string;
  comuneRilascioDocumentoLegaleRappresentante: string;
  cellulare: string;
  email: string;
  tipoIndirizzoSedeLegale: string;
  indirizzoSedeLegale: string;
  civicoSedeLegale: string;
  // scalaSedeLegale: string;
  provinciaSedeLegale: string;
  comuneSedeLegale: string;
  capSedeLegale: string;

  // punti fornitura
  occupazione: EniOccupazione;
  attualeFornitore: string;
  mercatoProvenienza: MercatoProvenienza;
  tensione: string | null;
  agevolazione: boolean;
  tipoIndirizzoRecapitoFattura: string | null;
  indirizzoRecapitoFattura: string | null;
  civicoRecapitoFattura: string | null;
  // scalaRecapitoFattura: string | null;
  provinciaRecapitoFattura: string | null;
  comuneRecapitoFattura: string | null;
  capRecapitoFattura: string | null;
  potenza: string | null;

  // dati pagamento
  iban: string;
  tipoIntestatarioSepa: TipoIntestataro;
  nomeSepa: string;
  cognomeSepa: string;
  codiceFiscaleSepa: string;
  partitaIvaSepa: string;
  ragioneSocialeSepa: string;
  codiceFiscaleAziendaSepa: string;
  codiceFiscaleLegaleRappresentante: string;
  consensoUno: boolean;
  consensoDue: boolean;
  consensoTre: boolean;
}
