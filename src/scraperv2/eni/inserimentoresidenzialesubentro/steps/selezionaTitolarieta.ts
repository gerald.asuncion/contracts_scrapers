import { Page } from "puppeteer";
import { EniResidenzialeInserimentoPayload } from "../../types/EniResidenzialeInserimentoPayload";
import tryOrThrow from "../../../../utils/tryOrThrow";
import waitForSelectorAndClickEvaluated from "../../../../browser/page/waitForSelectorAndClickEvaluated";
import selectByFindOption from "../../inserimentocontratto/selectByFindOption";
import waitForSelectorAndSelect from "../../../../browser/page/waitForSelectorAndSelect";
import waitForSelectorAndClick from "../../../../browser/page/waitForSelectorAndClick";

const SELECTOR_RESIDENZA = {
  'si': 'input[id$="indirizzoResidenzaEqFornitura_SI"]',
  'no': 'input[id$="indirizzoResidenzaEqFornitura_NO"]'
}

const SELECTOR_TITOLARIETA = {
  'propietario': 'input[id$="titolarita_PROPIETARIO"]',
  'conduttore': 'input[id$="titolarita_CONDUTTORE"]',
  'detentore': 'input[id$="titolarita_ALTRO"]'
}

const SELECTOR_USO = {
  'residente': 'input[id$="tipologiaUso_RESIDENTE"]',
  'non residente': 'input[id$="titolarita_NONRESIDENTE"]'
}

const SELECTOR_DESTINAZIONE_USO = {
  'abitazione': 'input[id$="selettorePertin_ABITAZIONE"]',
  'pertinenza': 'input[id$="selettorePertin_PERTINENZA"]'
}

export default async function selezionaTitolarieta(page: Page, payload: EniResidenzialeInserimentoPayload, inputSelectorBase = 'input[name="section3_metodoDiPagamento"]') {
  const { tipoFornitura, tipologiaAbitazione } = payload;
  
  await tryOrThrow(async () => {
    await waitForSelectorAndClick(page, SELECTOR_RESIDENZA["si"]);
  },
  `non sono riuscito selezionare residenza ${tipoFornitura}:`
  );

  await tryOrThrow(async () => {
    await waitForSelectorAndClick(page, SELECTOR_TITOLARIETA["propietario"]);
  },
  `non sono riuscito selezionare titolarità ${tipoFornitura}:`
  );
  
  await tryOrThrow(async () => {
    await waitForSelectorAndClick(page, SELECTOR_USO["non residente"]);
  },
  `non sono riuscito selezionare uso ${tipoFornitura}:`
  );
  
  await tryOrThrow(
    () => waitForSelectorAndSelect(page, 'input[id$="tipologiaDelloStabile"]', [tipologiaAbitazione]),
    'non sono riuscito a selezionare il tipo di abitazione del cliente:'
  );

  await tryOrThrow(async () => {
    await waitForSelectorAndClick(page, SELECTOR_DESTINAZIONE_USO["abitazione"]);
  },
  `non sono riuscito selezionare destinazione uso ${tipoFornitura}:`
  );

  await tryOrThrow(
    () => waitForSelectorAndSelect(page, 'input[id$="modalitaUtilizzoAbitazione"]', ["STANDARD"]),
    'non sono riuscito a selezionare il tipo di abitazione del cliente:'
  );
}
