import { Page } from "puppeteer";
import { EniResidenzialeInserimentoPayload } from "../../types/EniResidenzialeInserimentoPayload";
import tryOrThrow from "../../../../utils/tryOrThrow";
import selectByFindOption from "../../inserimentocontratto/selectByFindOption";
import waitForSelectorAndType from "../../../../browser/page/waitForSelectorAndType";

export default async function inserisciDatiConsumoLuce(page: Page, payload: EniResidenzialeInserimentoPayload) {
  
    await tryOrThrow(
        () => waitForSelectorAndType(page, 'input[id$="consumoAnnuo"]', payload.consumoAnnuoElettricita as string),
        'non sono riuscito ad inserire il consumo annuo elettricità:'
      );
    await tryOrThrow(
        () => selectByFindOption(page, 'select[id$="potenzaImpegnata"]', payload.potenzaImpegnata as string, ''),
        'non sono riuscito a selezionare la potenza impegnata:'
    );

    /*
    await tryOrThrow(
        () => selectByFindOption(page, MODALITÀ_USO_SELECTOR, payload.modalitaUso as string, ''),
        'non sono riuscito a selezionare la tipologia di uso:'
    );
    */
}