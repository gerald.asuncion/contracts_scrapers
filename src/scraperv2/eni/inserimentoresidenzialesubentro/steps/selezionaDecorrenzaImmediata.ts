import { Page } from "puppeteer";
import { EniResidenzialeInserimentoPayload } from "../../types/EniResidenzialeInserimentoPayload";
import tryOrThrow from "../../../../utils/tryOrThrow";
import waitForSelectorAndClickEvaluated from "../../../../browser/page/waitForSelectorAndClickEvaluated";

const SELECTOR_MAP = {
  "Si": 'input[id$="decorrenzaImmediata_SI"]',
  "No": 'input[id$="decorrenzaImmediata_NO"]',
}

export default async function selezionaDecorrenzaImmediata(page: Page, payload: EniResidenzialeInserimentoPayload, inputSelectorBase = 'input[name="section3_metodoDiPagamento"]') {
  const { tipoFornitura, decorrenzaImmediata } = payload;
  
  await tryOrThrow(async () => {
    await waitForSelectorAndClickEvaluated(page, SELECTOR_MAP[decorrenzaImmediata ? "Si" : "No"]);
  },
  `non sono riuscito selezionare la decorrenza ${tipoFornitura}:`
  );
}
