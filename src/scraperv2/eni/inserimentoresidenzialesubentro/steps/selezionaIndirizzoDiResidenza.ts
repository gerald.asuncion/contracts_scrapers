import { Page } from "puppeteer";
import { EniResidenzialeInserimentoPayload } from "../../types/EniResidenzialeInserimentoPayload";
import tryOrThrow from "../../../../utils/tryOrThrow";
import waitForSelectorAndClick from "../../../../browser/page/waitForSelectorAndClick";
import waitForSelectorAndClickEvaluated from "../../../../browser/page/waitForSelectorAndClickEvaluated";

const SELECTOR_MAP = {
  'uguale': 'input[id$="indirizzoDiResidenzaAnag_BOLLETTINO"]',
  'diverso': 'input[id$="indirizzoDiResidenzaAnag_DOMICILIAZIONE"]'
}

export default async function selezionaIndirizzoDiResidenza(page: Page, payload: EniResidenzialeInserimentoPayload, inputSelectorBase = 'input[name="section3_metodoDiPagamento"]') {
  const { tipoFornitura } = payload;
  
  await tryOrThrow(async () => {
    await waitForSelectorAndClickEvaluated(page, SELECTOR_MAP["uguale"]);
  },
  `non sono riuscito selezionare il metodo di pagamento ${tipoFornitura}:`
  );
}
