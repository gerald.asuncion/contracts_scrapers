import { Page } from "puppeteer";
import { EniResidenzialeInserimentoPayload } from "../../types/EniResidenzialeInserimentoPayload";
import tryOrThrow from "../../../../utils/tryOrThrow";
import waitForSelectorAndClick from "../../../../browser/page/waitForSelectorAndClick";
import waitForSelectorAndClickEvaluated from "../../../../browser/page/waitForSelectorAndClickEvaluated";
import waitForVisible from "../../../../browser/page/waitForVisible";
import delay from "../../../../utils/delay";
import controlloErrori from "./controlloErrori";

const SELECTOR_MAP: Record<EniResidenzialeInserimentoPayload['metodoPagamento'], string> = {
  'bollettino': 'input[id$="modalitaPagamentoAnag_BOLLETTINO"]',
  'domiciliazione': 'input[id$="modalitaPagamentoAnag_DOMICILIAZIONE"]'
}

const getSelectorVerifica = () => `#formVerificaAnagrafica > .allineamentoCentro > input[value$="Verifica"]`;
const getSelectorProcedi = () => `#divEsistenzaPratiche > table > tbody > tr:nth-child(2) > td > .allineamentoCentro > input[value$="Procedi"]`;
export default async function selezionaMetodoPagamento(page: Page, payload: EniResidenzialeInserimentoPayload, inputSelectorBase = 'input[name="section3_metodoDiPagamento"]') {
  const { metodoPagamento, tipoFornitura } = payload;
  
  await tryOrThrow(async () => {
    await waitForSelectorAndClickEvaluated(page, SELECTOR_MAP[metodoPagamento]);
  },
  `non sono riuscito selezionare il metodo di pagamento ${tipoFornitura}:`
  );

  await tryOrThrow(async () => {
      await waitForSelectorAndClick(page, getSelectorVerifica());
    },
    `non sono riuscito clickare il bottone verifica metodo pagamento ${tipoFornitura}:`
  );

  /* Controllo Errori, classe fieldError */
  await controlloErrori(page);

  if(metodoPagamento === "domiciliazione") {
    await waitForVisible(page, '#divEsistenzaPratiche');
    await delay(500);
    
    await tryOrThrow(async () => {
      await waitForSelectorAndClick(page, getSelectorProcedi());
    },
    `non sono riuscito clickare il bottone procedi metodo pagamento ${tipoFornitura}:`
  );
  }
}
