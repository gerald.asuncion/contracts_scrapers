import { Page } from "puppeteer";
import { EniResidenzialeInserimentoPayload } from "../../types/EniResidenzialeInserimentoPayload";
import tryOrThrow from "../../../../utils/tryOrThrow";
import waitForSelectorAndClick from "../../../../browser/page/waitForSelectorAndClick";

// Opzioni Si, No, NLS
const TIPOFORNITURA_MAP: Record<string, string> = {
  'gas': 'No',
  'luce': 'No',
  'dual': 'No'
};

const getSelector = (tipoFornitura: EniResidenzialeInserimentoPayload['tipoFornitura']) => `input[id$="tsAperto${TIPOFORNITURA_MAP[tipoFornitura]}"]`;
const getSelectorAvanti = () => `#formAperto > .allineamentoCentro > input[value$="Avanti"]`;

export default async function selezionaOpzioneErogaEnergiaCliente(page: Page, tipoFornitura: EniResidenzialeInserimentoPayload['tipoFornitura']) {
  return tryOrThrow(async () => {
      await waitForSelectorAndClick(page, getSelector(tipoFornitura));
      await waitForSelectorAndClick(page, getSelectorAvanti());
    },
    `non sono riuscito a selezionare l'opzione di erogazione tipo fornitura ${tipoFornitura}:`
  )
}