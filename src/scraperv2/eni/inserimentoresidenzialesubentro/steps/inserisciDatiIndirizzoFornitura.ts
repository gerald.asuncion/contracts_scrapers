import { Page } from "puppeteer";
import tryOrThrow from "../../../../utils/tryOrThrow";
import { EniResidenzialeInserimentoPayload } from "../../types/EniResidenzialeInserimentoPayload";
import waitForSelectorAndType from "../../../../browser/page/waitForSelectorAndType";
import waitForSelectorAndClick from "../../../../browser/page/waitForSelectorAndClick";
import waitForVisibleStyled from "../../../../browser/page/waitForVisibleStyled";

const getSelectorVerifica = () => `input[id$="btnVerificaIndirFornitura"]`;

export default async function inserisciIndirizzoFornitura(page: Page, payload: EniResidenzialeInserimentoPayload) {
  const { attivazioneIndirizzo, attivazioneCivico, attivazioneScala = "", attivazioneProvincia, attivazioneComune, attivazioneCap, tipoFornitura } = payload;

  await tryOrThrow(
    () => waitForSelectorAndType(page, 'input[id$="indirizzoIndirFornitura"]', attivazioneIndirizzo),
    'non sono riuscito a inserire il indirizzo fornitura del cliente:'
  );

  await tryOrThrow(
    () => waitForSelectorAndType(page, 'input[id$="civicoIndirFornitura"]', attivazioneCivico),
    'non sono riuscito a inserire il numero civico del cliente:'
  );

  if(attivazioneScala) {
    await tryOrThrow(
      () => waitForSelectorAndType(page, 'input[id$="scalaIndirFornitura"]', attivazioneScala),
      'non sono riuscito a inserire il numero della scala del cliente:'
    );
  }

  await tryOrThrow(
    async () => {
      await waitForSelectorAndType(page, 'input[id$="siglaProvinciaIndirFornitura"]', attivazioneProvincia);
      await waitForSelectorAndClick(page, 'div[class$="ac_results"]:nth-of-type(2) > ul > li');
  },
    'non sono riuscito a inserire la provincia del cliente:'
  );

  await tryOrThrow(
    async () => {
      await waitForSelectorAndType(page, 'input[id$="cittaIndirFornitura"]', attivazioneComune);
      await waitForSelectorAndClick(page, 'div[class$="ac_results"]:nth-of-type(3) > ul > li');
  },
    'non sono riuscito a inserire il comune del cliente:'
  );

  await tryOrThrow(
    async () => {
      await waitForSelectorAndType(page, 'input[id$="capIndirFornitura"]', attivazioneCap)
      await waitForSelectorAndClick(page, 'div[class$="ac_results"]:nth-of-type(4) > ul > li')
    },
    'non sono riuscito a inserire il cap del cliente:'
  );

  /*
  await tryOrThrow(
    () =>  waitForSelectorAndClick(page, '.ac_results:last-child > ul > li'),
    'non sono riuscito a selezionare il CAP del cliente:'
  );
  */

  return await tryOrThrow(async () => {
    await waitForSelectorAndClick(page, getSelectorVerifica());
  },
  `non sono riuscito a verificare l'indirizzo di fornitura ${tipoFornitura}:`
  );
}
