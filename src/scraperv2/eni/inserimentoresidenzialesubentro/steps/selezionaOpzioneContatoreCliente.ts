import { Page } from "puppeteer";
import { EniResidenzialeInserimentoPayload } from "../../types/EniResidenzialeInserimentoPayload";
import tryOrThrow from "../../../../utils/tryOrThrow";
import waitForSelectorAndClick from "../../../../browser/page/waitForSelectorAndClick";

// Opzioni Si, No, NLS
const TIPOFORNITURA_MAP: Record<string, string> = {
  'gas': 'Si',
  'luce': 'Si',
  'dual': 'Si'
};

const getSelector = (tipoFornitura: EniResidenzialeInserimentoPayload['tipoFornitura']) => `input[id$="tsContatorePresente${TIPOFORNITURA_MAP[tipoFornitura]}"]`;
const getSelectorAvanti = () => `#formContatore > .allineamentoCentro > input[value$="Avanti"]`;

export default async function selezionaOpzioneContatoreCliente(page: Page, tipoFornitura: EniResidenzialeInserimentoPayload['tipoFornitura']) {
  return tryOrThrow(async () => {
      await waitForSelectorAndClick(page, getSelector(tipoFornitura));
      await waitForSelectorAndClick(page, getSelectorAvanti());
    },
    `non sono riuscito a selezionare l'opzione contatore tipo fornitura ${tipoFornitura}:`
  )
}