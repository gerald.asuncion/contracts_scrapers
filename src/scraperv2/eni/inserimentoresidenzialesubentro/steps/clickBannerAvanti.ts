import { Page } from "puppeteer";
import { EniResidenzialeInserimentoPayload } from "../../types/EniResidenzialeInserimentoPayload";
import tryOrThrow from "../../../../utils/tryOrThrow";
import waitForSelectorAndClick from "../../../../browser/page/waitForSelectorAndClick";
import waitForSelectorAndClickEvaluated from "../../../../browser/page/waitForSelectorAndClickEvaluated";
import delay from "../../../../utils/delay";
import waitForVisible from "../../../../browser/page/waitForVisible";
import { Logger } from "../../../../logger";

export default async function clickBannerAvanti(page: Page, tipoFornitura: EniResidenzialeInserimentoPayload['tipoFornitura'], logger: Logger) {
  try{
      await waitForVisible(page, '#popupChangeModalitaOperativa > .Loading > .sectionBox > div > input[value$="Avanti"]');
      await delay(500);
      await waitForSelectorAndClick(page, '#popupChangeModalitaOperativa > .Loading > .sectionBox > div > input[value$="Avanti"]');
  } catch (error) {
      logger.info('modale non comparso skippa modale continua flusso');
  }
}