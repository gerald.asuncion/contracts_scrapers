import { Page } from "puppeteer";
import { EniResidenzialeInserimentoPayload } from "../../types/EniResidenzialeInserimentoPayload";
import tryOrThrow from "../../../../utils/tryOrThrow";
import waitForSelectorAndClick from "../../../../browser/page/waitForSelectorAndClick";
import waitForVisible from "../../../../browser/page/waitForVisible";
import delay from "../../../../utils/delay";
import { Logger } from "../../../../logger";

const getSelectorPromozioni = () => `input[id$="consensiIniziativePromozionaliEni"]`;
const getSelectorAnalisiMercato = () => `input[id$="consensiAnalisiRicerche"]`;
const getSelectorPromozioniTerzi = () => `input[id$="consensiIniziativePromozionaliTerzi"]`;
const getSelectorContinua = () => `#formVerificaDettagli > table > tbody > tr > td > .allineamentoCentro > input[value$="Continua"]`;

export default async function clickConsensiPrivacy(page: Page, tipoFornitura: EniResidenzialeInserimentoPayload['tipoFornitura'], logger: Logger) {
    await tryOrThrow(async () => {
        await waitForSelectorAndClick(page, getSelectorPromozioni());
    },
    `non sono riuscito a cliccare privacy promozioni ${tipoFornitura}:`
    );

    await tryOrThrow(async () => {
        await waitForSelectorAndClick(page, getSelectorAnalisiMercato());
    },
    `non sono riuscito a cliccare privacy analisi mercato ${tipoFornitura}:`
    );

    await tryOrThrow(async () => {
        await waitForSelectorAndClick(page, getSelectorPromozioniTerzi());
    },
    `non sono riuscito a cliccare promozioni terzi ${tipoFornitura}:`
    );

    await tryOrThrow(async () => {
        await waitForSelectorAndClick(page, getSelectorContinua());
    },
    `non sono riuscito a cliccare Continua in privacy ${tipoFornitura}:`
    );

    if(tipoFornitura === "gas") {
        try {
            await waitForVisible(page, '#pop_UpConsensiPrivacy > .Loading > .sectionBox > div > input[value$="Continua"]');
            await delay(500);
            await waitForSelectorAndClick(page, '#pop_UpConsensiPrivacy > .Loading > .sectionBox > div > input[value$="Continua"]');
        } catch (error) {
            logger.info('modale non comparso skippa modale continua flusso');
        }

    }
} 