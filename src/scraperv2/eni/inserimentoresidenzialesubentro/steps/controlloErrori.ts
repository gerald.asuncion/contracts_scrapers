import { Page } from 'puppeteer';
import { TryOrThrowError } from '../../../../utils/tryOrThrow';

export default async function controlloErrori(page: Page, ErrClazz = TryOrThrowError) {

    try {
        await page.waitForSelector(".fieldError", { timeout: 3000 }); 
    } catch (error) {
        return;
    }
    const fieldsErrors: any = await page.$$eval(".fieldError", (elements: Element[]) => {
        return elements.map((element: Element, i) => {
            if(element.textContent) {
                if(element.textContent.replace(/\\n/g, '').trim().replace("\n", '')) {
                    if(element.id.replace("err", "").split(/(?=[A-Z])/).toString().replace(",", " ")) {
                        return element.id.replace("err", "").split(/(?=[A-Z])/).toString().replace(",", " ") + ": " + element.textContent.replace(/\\n/g, '').trim().replace("\n", '');
                    }
                }
            }
                
            return "";
        }).filter(v => v.length > 0);
    });

    if(fieldsErrors && fieldsErrors.length > 0) {
        const error = new ErrClazz(fieldsErrors.toString().replace(",", "\n"));
        throw error; 
    }
  }
  