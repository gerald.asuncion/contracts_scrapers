import { Page } from "puppeteer";
import { EniResidenzialeInserimentoPayload } from "../../types/EniResidenzialeInserimentoPayload";
import tryOrThrow from "../../../../utils/tryOrThrow";
import waitForSelectorAndClick from "../../../../browser/page/waitForSelectorAndClick";
import waitForSelectorAndClickEvaluated from "../../../../browser/page/waitForSelectorAndClickEvaluated";
import waitForSelectorAndType from "../../../../browser/page/waitForSelectorAndType";

const SELECTOR_METODO_PAGAMENTO: Record<EniResidenzialeInserimentoPayload['metodoPagamento'], string> = {
  'bollettino': 'input[id$="modalitaPagamentoAnag_BOLLETTINO"]',
  'domiciliazione': 'input[id$="modalitaPagamentoAnag_DOMICILIAZIONE"]'
}

const SELECTOR_INTESTATARIO = {
    'uguale': 'input[id$="modalitaPagamentoAnag_BOLLETTINO"]',
    'diverso': 'input[id$="modalitaPagamentoAnag_DOMICILIAZIONE"]'
}

const SELECTOR_SPEDIZIONE_BOLLETTA: Record<EniResidenzialeInserimentoPayload['modalitaSpedizioneBolletta'], string> = {
    'digitale': 'input[id$="modalitaPagamentoAnag_BOLLETTINO"]',
    'posta sintetica': 'input[id$="modalitaPagamentoAnag_DOMICILIAZIONE"]'
}

const SELECTOR_ESAZIONE = {
    'uguale fornitura': 'input[id$="modalitaPagamentoAnag_BOLLETTINO"]',
    'uguale residenza': 'input[id$="modalitaPagamentoAnag_BOLLETTINO"]',
    'altro': 'input[id$="modalitaPagamentoAnag_DOMICILIAZIONE"]'
}

const getSelectorContinua = () => `#formVerificaFatturazioneOpzioniVAS > table > tbody > tr > td > .allineamentoCentro > input[value$="Continua"]`;

export default async function inserisciPagamentoFatturazione(page: Page, payload: EniResidenzialeInserimentoPayload) {
  const { metodoPagamento, tipoFornitura, iban, modalitaSpedizioneBolletta } = payload;
  
  /*
  await tryOrThrow(async () => {
    await waitForSelectorAndClickEvaluated(page, SELECTOR_METODO_PAGAMENTO[metodoPagamento]);
  },
  `non sono riuscito selezionare il metodo di pagamento ${tipoFornitura}:`
  );
  */

  await tryOrThrow(
    () => waitForSelectorAndType(page, 'input[id$="iban"]', iban as string),
    'non sono riuscito ad inserire il IBAN:'
  );

  /*
  await tryOrThrow(async () => {
    await waitForSelectorAndClickEvaluated(page, SELECTOR_INTESTATARIO["uguale"]);
  },
  `non sono riuscito selezionare l\'intestatario ${tipoFornitura}:`
  );

  await tryOrThrow(async () => {
    await waitForSelectorAndClickEvaluated(page, SELECTOR_SPEDIZIONE_BOLLETTA[modalitaSpedizioneBolletta]);
  },
  `non sono riuscito selezionare la modalita di spedizione bollette ${tipoFornitura}:`
  );

  await tryOrThrow(async () => {
    await waitForSelectorAndClickEvaluated(page, SELECTOR_ESAZIONE["uguale fornitura"]);
  },
  `non sono riuscito selezionare la esazione ${tipoFornitura}:`
  );
  */


  await tryOrThrow(async () => {
      await waitForSelectorAndClick(page, getSelectorContinua());
    },
    `non sono riuscito clickare il bottone continua ${tipoFornitura}:`
  );
}
