import { Page } from "puppeteer";
import tryOrThrow from "../../../../utils/tryOrThrow";
import { EniResidenzialeInserimentoPayload } from "../../types/EniResidenzialeInserimentoPayload";
import waitForSelectorAndType from "../../../../browser/page/waitForSelectorAndType";
import waitForSelectorAndSelect from "../../../../browser/page/waitForSelectorAndSelect";
import waitForSelectorAndClickEvaluated from "../../../../browser/page/waitForSelectorAndClickEvaluated";
import delay from "../../../../utils/delay";
import waitForHiddenStyled from "../../../../browser/page/waitForHiddenStyled";
import waitForNavigation from "../../../../browser/page/waitForNavigation";
import waitForSelectorClearAndType from "../../../../browser/page/waitForSelectorClearAndType";
import selectByFindOption from "../../inserimentocontratto/selectByFindOption";
import { Logger } from "../../../../logger";

const SELECTOR_SESSO = {
    'M': 'input[id$="sesso_M"]',
    'F': 'input[id$="sesso_F"]'
}

const SELECTOR_TIPO_DOCUMENTO = {
  "carta d'identità": "Carta D'Identita'",
  "patente": 'Patente',
  "passaporto": 'Passaporto',
  "altro": 'Altro'
}

const SELECTOR_NAZIONALITA = {
  "italiana": "ITALIANA",
  "altro": "ALTRO"
}

  
export default async function inserisciDatiDocumentoCliente(page: Page, payload: EniResidenzialeInserimentoPayload) {
  const { 
    tipoFornitura, sesso = "M", documentoTipo, 
    documentoNumero, documentoComuneRilascio = "", documentoDataEmissione = "", 
    nazionalita = "", luogoNascita = "", dataNascita = "", 
} = payload;

/*
await waitForHiddenStyled(page, '', {
  timeout: 60000
});
*/

if(tipoFornitura === "luce") {
  await tryOrThrow(async () => {
    await waitForNavigation(page);
  }, 'non sono riuscito a rindirizzare la pagina'
  );
}

  

  await tryOrThrow(async () => {
    await waitForSelectorAndClickEvaluated(page, SELECTOR_SESSO[sesso]);
  },
  `non sono riuscito selezionare il sesso del cliente ${tipoFornitura}:`
  );

  
  await tryOrThrow(
    () => selectByFindOption(page, 'select[id$="idTipoDocumento"]', SELECTOR_TIPO_DOCUMENTO[documentoTipo], "tipo documento"),
    'non sono riuscito a inserire il tipo del documento del cliente:'
  );

  await tryOrThrow(
    () => waitForSelectorClearAndType(page, 'input[id$="numeroDocumento"]', documentoNumero),
    'non sono riuscito a inserire il numero del documento del cliente:'
  );

  await tryOrThrow(
    () => waitForSelectorClearAndType(page, 'input[id$="enteRilascioDocumento"]', documentoComuneRilascio),
    'non sono riuscito a inserire il comune di rilascio del cliente:'
  );

  await tryOrThrow(
    () => waitForSelectorAndType(page, 'input[id$="dataRilascioDocumento"]', documentoDataEmissione),
    'non sono riuscito a inserire la data di rilascio del cliente:'
  );

  /*
  await tryOrThrow(
    () => waitForSelectorAndSelect(page, 'input[id$="nazionalita"]', [SELECTOR_NAZIONALITA['italiana']]),
    'non sono riuscito a inserire la nazionalita del cliente:'
  );
  */

  await tryOrThrow(
    () => waitForSelectorAndType(page, 'input[id$="comuneNascita"]', luogoNascita),
    'non sono riuscito a inserire il luogo di nascita del cliente:'
  );

  await tryOrThrow(
    () => waitForSelectorAndType(page, 'input[id$="dataNascita"]', dataNascita),
    'non sono riuscito a inserire la data di nascita del cliente:'
  );
}
