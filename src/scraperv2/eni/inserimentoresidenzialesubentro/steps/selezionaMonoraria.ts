import { Page } from "puppeteer";
import { EniResidenzialeInserimentoPayload } from "../../types/EniResidenzialeInserimentoPayload";
import tryOrThrow from "../../../../utils/tryOrThrow";
import selectByFindOption from "../../inserimentocontratto/selectByFindOption";
import waitForVisible from "../../../../browser/page/waitForVisible";
import delay from "../../../../utils/delay";

export default async function selezionaOfferta(page: Page, payload: EniResidenzialeInserimentoPayload) {
    await tryOrThrow(
        async () => {
            await waitForVisible(page, '#campoOpzione_OpzioneBiorariaPow');
            await delay(500);
            await selectByFindOption(page, 'select[id$="campoOpzione_OpzioneBiorariaPow"]', 'Monoraria', 'moniraria')
        },
        'non sono riuscito a selezionare la monoraria:'
    );
}