import { Page } from "puppeteer";
import { EniResidenzialeInserimentoPayload } from "../../types/EniResidenzialeInserimentoPayload";
import tryOrThrow from "../../../../utils/tryOrThrow";
import waitForSelectorAndClick from "../../../../browser/page/waitForSelectorAndClick";

const getSelectorVerifica = () => `input[id$="btnVerificaIndirFornitura"]`;

export default async function verificaIndirizzoFornitura(page: Page, tipoFornitura: EniResidenzialeInserimentoPayload['tipoFornitura']) {
  return tryOrThrow(async () => {
      await waitForSelectorAndClick(page, getSelectorVerifica());
    },
    `non sono riuscito a verificare l'indirizzo di fornitura ${tipoFornitura}:`
  )
}