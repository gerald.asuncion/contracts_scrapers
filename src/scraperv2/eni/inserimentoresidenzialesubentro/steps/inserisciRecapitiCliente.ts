import { Page } from "puppeteer";
import tryOrThrow from "../../../../utils/tryOrThrow";
import { EniResidenzialeInserimentoPayload } from "../../types/EniResidenzialeInserimentoPayload";
import waitForSelectorAndType from "../../../../browser/page/waitForSelectorAndType";
import waitForSelectorClearAndType from "../../../../browser/page/waitForSelectorClearAndType";

export default async function inserisciRecapitiCliente(page: Page, payload: EniResidenzialeInserimentoPayload) {
  const { 
    cellulare, email
} = payload;

  await tryOrThrow(
    () => waitForSelectorClearAndType(page, 'input[id$="cellulare"]', cellulare),
    'non sono riuscito a inserire il cellulare del cliente:'
  );

  await tryOrThrow(
    () => waitForSelectorAndType(page, 'input[id$="email"]', email),
    'non sono riuscito a inserire l\'email del cliente:'
  );

}