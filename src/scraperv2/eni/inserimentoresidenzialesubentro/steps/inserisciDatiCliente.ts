import { Page } from "puppeteer";
import tryOrThrow from "../../../../utils/tryOrThrow";
import { EniResidenzialeInserimentoPayload } from "../../types/EniResidenzialeInserimentoPayload";
import waitForSelectorAndType from "../../../../browser/page/waitForSelectorAndType";
import { TipoFornitura } from '../../EniInserimentoPayload';

export default async function inserisciDatiCliente(page: Page, payload: EniResidenzialeInserimentoPayload) {
  const { tipoFornitura, nome, cognome, codiceFiscale, pod = "", pdr = "" } = payload;

  await tryOrThrow(
    () => waitForSelectorAndType(page, 'input[id$="nome"]', nome),
    'non sono riuscito a inserire il nome del cliente:'
  );

  await tryOrThrow(
    () => waitForSelectorAndType(page, 'input[id$="cognome"]', cognome),
    'non sono riuscito a inserire il cognome del cliente:'
  );

  await tryOrThrow(
    () => waitForSelectorAndType(page, 'input[id$="codiceFiscale"]', codiceFiscale),
    'non sono riuscito a inserire il codice fiscale del cliente:'
  );

  if(tipoFornitura === 'luce') {
    await tryOrThrow(
      () => waitForSelectorAndType(page, 'input[id$="pod"]', pod),
      'non sono riuscito a inserire il POD del cliente:'
    );
  }
  if(tipoFornitura === 'gas') {
    await tryOrThrow(
      () => waitForSelectorAndType(page, 'input[id$="pdr"]', pdr),
      'non sono riuscito a inserire il PDR del cliente:'
    );
  }

}
