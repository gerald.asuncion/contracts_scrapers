import { Page } from "puppeteer";
import { EniResidenzialeInserimentoPayload } from "../../types/EniResidenzialeInserimentoPayload";
import tryOrThrow from "../../../../utils/tryOrThrow";
import waitForSelectorAndClick from "../../../../browser/page/waitForSelectorAndClick";
import waitForSelectorAndClickEvaluated from "../../../../browser/page/waitForSelectorAndClickEvaluated";
import delay from "../../../../utils/delay";
import waitForVisible from "../../../../browser/page/waitForVisible";

export default async function clickBannerVerificaIndirizzoFornitura(page: Page, tipoFornitura: EniResidenzialeInserimentoPayload['tipoFornitura']) {
  return await tryOrThrow(async () => {
      await waitForVisible(page, '#pop_UpIndirizzoStradarioConfermato > input[value$="OK"]');
      await delay(500);
      await waitForSelectorAndClick(page, '#pop_UpIndirizzoStradarioConfermato > input[value$="OK"]');
    },
    `non sono riuscito a clickare OK nel banner di verifica indirizzo ${tipoFornitura}:`
  )
}