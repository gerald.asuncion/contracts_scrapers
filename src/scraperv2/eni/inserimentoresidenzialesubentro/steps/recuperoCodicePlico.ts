import { Page } from "puppeteer";
import getElementText from "../../../../browser/page/getElementText";

const htmlPath = ".sectionBox > table > tbody > tr:nth-child(2) > td:nth-child(2)"

export default async function stepRecuperoCodicePlico(page: Page): Promise<string> {
  const text = await getElementText(page, htmlPath, "innerText") as string;

  return text.trim();
}
