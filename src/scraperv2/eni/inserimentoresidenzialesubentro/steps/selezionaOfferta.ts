import { Page } from "puppeteer";
import { EniResidenzialeInserimentoPayload } from "../../types/EniResidenzialeInserimentoPayload";
import tryOrThrow from "../../../../utils/tryOrThrow";
import selectByFindOption from "../../inserimentocontratto/selectByFindOption";
import waitForSelectorAndClick from "../../../../browser/page/waitForSelectorAndClick";
import selezionaOfferta from "../../inserimentoresidenziale/steps/stepDatiCliente/selezionaOfferta";
import waitForVisible from "../../../../browser/page/waitForVisible";
import delay from "../../../../utils/delay";

export default async function areaSezioneOfferta(page: Page, payload: EniResidenzialeInserimentoPayload) {
    const { tipoFornitura, offerta } = payload;
  
    await tryOrThrow(
        async () => {
            await waitForVisible(page, '#formSelezionaCanale');
            await delay(500);
            await selectByFindOption(page, '#formSelezionaCanale > div > #filtro', 'Comparatori Tariffe', 'filtro offerta')
        },
        'non sono riuscito a selezionare il filtro:'
    );

    await tryOrThrow(async () => {
        await waitForSelectorAndClick(page, '#formSelezionaCanale > div > input[value$="Cerca"]');
    },
    `non sono riuscito a clickare Cerca ${tipoFornitura}:`
    );

    await selezionaOfferta(page, offerta);

    await tryOrThrow(async () => {
        await waitForSelectorAndClick(page, '#formSelezionaProdotto > .allineamentoCentro > input[value$="Conferma"]');
    },
    `non sono riuscito a selezionare Conferma ${tipoFornitura}:`
    );
}