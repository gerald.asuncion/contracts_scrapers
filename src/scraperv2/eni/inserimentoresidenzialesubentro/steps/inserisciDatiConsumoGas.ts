import { Page } from "puppeteer";
import waitForSelectorAndClick from "../../../../browser/page/waitForSelectorAndClick";
import { EniResidenzialeInserimentoPayload } from "../../types/EniResidenzialeInserimentoPayload";
import tryOrThrow from "../../../../utils/tryOrThrow";
import waitForSelectorAndType from "../../../../browser/page/waitForSelectorAndType";
import selectByFindOption from "../../inserimentocontratto/selectByFindOption";
import waitForVisible from "../../../../browser/page/waitForVisible";
import delay from "../../../../utils/delay";

export type UtilizziGas = Exclude<Required<EniResidenzialeInserimentoPayload['utilizzoGas']>, undefined>;

export type UtilizzoGas = UtilizziGas[number];

const UTILIZZOGAS_MAP: Record<UtilizzoGas, string> = {
  'cottura': 'input[id$="utilizzoGasCottura"]',
  'acqua calda': 'input[id$="utilizzoGasAcquaCalda"]',
  'riscaldamento': 'input[id$="utilizzoGasRiscaldamento"]'
};

export default async function selezionaUtilizziGas(page: Page, payload: EniResidenzialeInserimentoPayload) {
  const { utilizzoGas = [], consumoAnnuoGas, potenzaImpegnata } = payload;

  await waitForVisible(page, '#formVerificaAnagrafica');
  await delay(500);

  for (let i = 0; i < utilizzoGas.length; i++) {
    const utilizzo = utilizzoGas[i];
    await waitForSelectorAndClick(page, UTILIZZOGAS_MAP[utilizzo]);
  }

  
  await tryOrThrow(
    async () => {
      await waitForSelectorAndType(page, 'input[id$="potenzialitaGas"]', potenzaImpegnata as string);
      await waitForSelectorAndClick(page, 'input[id$="calcolaPotenzialitaGas"]');
    },
    'non sono riuscito a selezionare la potenza impegnata:'
  );

  await tryOrThrow(
    async () => {
      await waitForSelectorAndType(page, 'input[id$="consumoAnnuo"]', consumoAnnuoGas as string);
      await waitForSelectorAndClick(page, 'input[id$="calcolaConsumoAnnuoGas"]');
    },
    'non sono riuscito ad inserire il consumo annuo gas:'
  );

}
