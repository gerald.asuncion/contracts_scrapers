import { Page } from "puppeteer";
import { EniResidenzialeInserimentoPayload } from "../../types/EniResidenzialeInserimentoPayload";
import tryOrThrow from "../../../../utils/tryOrThrow";
import waitForSelectorAndClick from "../../../../browser/page/waitForSelectorAndClick";

const getSelectorVaiRiepilogo = () => `#formVerificaPreferenzaAppuntamento > table > tbody > tr > td > .allineamentoCentro > input[value$="Vai al riepilogo"]`;
const getSelectorInviaOptIn = () => `#formRiepilogo > table > tbody > tr > td > input[value$="Invia Double Opt-In"]`;

export default async function inserisciOreferenzeAppuntamento(page: Page, payload: EniResidenzialeInserimentoPayload, inputSelectorBase = 'input[name="section3_metodoDiPagamento"]') {
  const { tipoFornitura } = payload;

  await tryOrThrow(async () => {
      await waitForSelectorAndClick(page, getSelectorVaiRiepilogo());
    },
    `non sono riuscito clickare il bottone vai riepilogo ${tipoFornitura}:`
  );

  /* IMPLEMENTARE in fase di TEST */
  await tryOrThrow(async () => {
      await waitForSelectorAndClick(page, getSelectorInviaOptIn());
    },
    `non sono riuscito clickare il bottone invia opt in ${tipoFornitura}:`
  );
}
