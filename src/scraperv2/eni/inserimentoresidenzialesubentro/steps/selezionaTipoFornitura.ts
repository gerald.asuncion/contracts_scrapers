import { Page } from "puppeteer";
import { EniResidenzialeInserimentoPayload } from "../../types/EniResidenzialeInserimentoPayload";
import tryOrThrow from "../../../../utils/tryOrThrow";
import waitForSelectorAndClick from "../../../../browser/page/waitForSelectorAndClick";

const TIPOFORNITURA_MAP: Record<string, string> = {
  'gas': 'Gas',
  'luce': 'Luce',
  'dual': 'Dual'
};

const getSelector = (tipoFornitura: EniResidenzialeInserimentoPayload['tipoFornitura']) => `input[id$="tsTipologiaFornitura${TIPOFORNITURA_MAP[tipoFornitura]}"]`;
const getSelectorAvanti = () => `input[value$="Avanti"]`;

export default async function selezionaTipoFornitura(page: Page, tipoFornitura: EniResidenzialeInserimentoPayload['tipoFornitura']) {
  return tryOrThrow(async () => {
      await waitForSelectorAndClick(page, getSelector(tipoFornitura));
      await waitForSelectorAndClick(page, getSelectorAvanti());
    },
    `non sono riuscito a selezionare il tipo di fornitura ${tipoFornitura}:`
  )
}