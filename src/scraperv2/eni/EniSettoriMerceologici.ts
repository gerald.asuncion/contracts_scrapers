import DbScraper from '../DbScraper';
import EniCodiciAtecoRepo from '../../repo/EniCodiciAtecoRepo';
import EniActionOptions from './EniActionOptions';
import createChildLogger from '../../utils/createChildLogger';
import createChildPayloadAwareLogger from '../../utils/createChildPayloadAwareLogger';
import { ScraperResponse } from '../scraper';
import FailureResponse from "../../response/FailureResponse";
import GenericSuccessResponse from '../../response/GenericSuccessResponse';
import extractErrorMessage from '../../utils/extractErrorMessage';
import { SCRAPER_TIPOLOGIA } from '../ScraperOptions';
import { QueueTask } from '../../task/QueueTask';

interface EniSettoriMerceologiciParams {
  formaGiuridica: string;
}

@QueueTask()
export default class EniSettoriMerceologici extends DbScraper {
  private eniCodiciAtecoRepo: EniCodiciAtecoRepo;

  constructor(options: EniActionOptions) {
    super(options);
    this.eniCodiciAtecoRepo = options.eniCodiciAtecoRepo;
    this.logger = createChildLogger(options.logger, 'EniSettoriMerceologici');
  }

  async scrape(
    args: any,
    { formaGiuridica, ...restArgs }: EniSettoriMerceologiciParams
  ): Promise<ScraperResponse> {
    const logger = createChildPayloadAwareLogger(this.logger, { ...args, ...restArgs });

    try {
      logger.info(`carico i settori merceologici relativi alla forma giuridica ${formaGiuridica}`);
      const records = await this.eniCodiciAtecoRepo.caricaSettoriMerceologici(formaGiuridica);

      return new GenericSuccessResponse<Array<string>>(records);
    } catch (ex) {
      const errMsg = extractErrorMessage(ex);
      logger.error(errMsg);
      return new FailureResponse(errMsg);
    }
  }

  getScraperCodice() {
    return 'eni-settorimerceologici';
  }

  getScraperTipologia() {
    return SCRAPER_TIPOLOGIA.IMPORTDATA;
  }
}
