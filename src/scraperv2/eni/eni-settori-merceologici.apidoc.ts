/**
 * @openapi
 *
 * /scraper/v2/eni/settore-merceologico/{formaGiuridica}:
 *  post:
 *    tags:
 *      - v2
 *    description: Restiutisce la lista di settori merceologici per la forma giuridica interessata presi dal portale di Eni.
 *    consume:
 *      - application/json
 *    produces:
 *      - application/json
 *    responses:
 *      200:
 *        description: SuccessResponse
 *    parameters:
 *      - in: path
 *        name: formaGiuridica
 *        required: true
 *        schema:
 *          type: string
 *        description: La forma giuridica interessata
 *      - in: body
 *        description: non serve.
 *        schema:
 *          type: object
 */
