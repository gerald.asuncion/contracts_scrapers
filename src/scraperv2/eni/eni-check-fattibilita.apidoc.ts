/**
 * @openapi
 *
 * /scraper/v2/eni/fattibilita:
 *  post:
 *    tags:
 *      - v2
 *    description: Effettua il controllo di fattibilità per Eni.
 *    consume:
 *      - application/json
 *    produces:
 *      - application/json
 *    responses:
 *      200:
 *        description: SuccessResponse
 *    parameters:
 *      - in: body
 *        description: i dati da inserire.
 *        schema:
 *          type: object
 *          required:
 *            - tipoFornitura
 *            - ragioneSociale
 *            - partitaIva
 *            - codiceFiscale
 *            - idDatiContratto
 *          properties:
 *            tipoFornitura:
 *              type: string
 *              enum: [luce, gas]
 *            ragioneSociale:
 *              type: string
 *            partitaIva:
 *              type: string
 *            codiceFiscale:
 *              type: string
 *            consumoAnnuo:
 *              type: number
 *            pod:
 *              type: string
 *              nullable: true
 *              description: necessario se luce
 *            pdr:
 *              type: string
 *              nullable: true
 *              description: necessario se gas
 *            idDatiContratto:
 *              type: integer
 */
