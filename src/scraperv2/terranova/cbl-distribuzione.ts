import { TerranovaAbstractScraperArgs } from './terranova-abstract-scraper';
import { TerranovaAbstractScraperType2 } from './terranova-abstract-scraper-type2';

export default class CblDistribuzione extends TerranovaAbstractScraperType2 {
  constructor(options: TerranovaAbstractScraperArgs) {
    const config = {
      credentials: {
        username: 'F.MUTTI',
        password: 'TTURQ4D2'
      },
      loginUrl: 'http://95.110.171.12/Portal/Index.aspx?Idn=0004&area=B2B',
      searchUrl: 'http://95.110.171.12/Portal/Index.aspx?m=835G&area=B2B',
      fields: {
        username: '#twsTemplate_Content1_twsModule_txtUser',
        password: '#twsTemplate_Content1_twsModule_txtPSW',
        loginBtn: '#twsTemplate_Content1_twsModule_btnLogin',
        pdr: '#twsTemplate_Content2_twsModule_cTracciati_cCercaPDR_txtCodicePDP',
        search: '#twsTemplate_Content2_twsModule_cTracciati_cCercaPDR_btn_Cerca',
        message: '#twsTemplate_Content2_twsModule_cTracciati_cCercaPDR_lblMsg',
        select: '#twsTemplate_Content2_twsModule_SceltaTracciato1_ddlCodServizio',
        findBtn: '#twsTemplate_Content2_twsModule_cTracciati_cCercaPDR_btnOpenCercaPDP',
      }
    };
    super(Object.assign({}, options, config));
  }
}
