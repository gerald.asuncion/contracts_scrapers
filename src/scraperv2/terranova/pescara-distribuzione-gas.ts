import { TerranovaAbstractScraper, TerranovaAbstractScraperArgs } from './terranova-abstract-scraper';

export default class PescaraDistribuzioneGas extends TerranovaAbstractScraper {
  constructor(options: TerranovaAbstractScraperArgs) {
    const config = {
      loginUrl: 'http://retigas.pescaradistribuzionegas.it/Portal/Index.aspx?Idn=0032&area=B2B',
      searchUrl: 'http://retigas.pescaradistribuzionegas.it/Portal/Index.aspx?m=831G&area=B2B',
      fields: {
        username: '#twsTemplate_Header1_Contenuto_twsModule_txtUser',
        password: '#twsTemplate_Header1_Contenuto_twsModule_txtPSW',
        loginBtn: '#twsTemplate_Header1_Contenuto_twsModule_btnLogin',
        pdr: '#twsTemplate_Header1_Contenuto_twsModule_txtPdr',
        search: '#twsTemplate_Header1_Contenuto_twsModule_btnSearchPDP',
        message: '#twsTemplate_Header1_Contenuto_twsModule_lblMsg',
      },
      cookies: [{
        name: 'KS',
        value: 'G'
      }]
    };
    super(Object.assign({}, options, config));
  }

  /**
   * @override
   */
  getScraperCodice() {
    return 'pescaradistribuzionegas';
  }
}
