import { TerranovaAbstractScraper, TerranovaAbstractScraperArgs } from './terranova-abstract-scraper';

export default class GasPlus extends TerranovaAbstractScraper {
  constructor(options: TerranovaAbstractScraperArgs) {
    const config = {
      loginUrl: 'http://95.110.171.5/Portal/Index.aspx?idn=001A&mode=GPRETI&area=B2B',
      searchUrl: 'http://95.110.171.5/Portal/Index.aspx?m=831G&area=B2B',
      fields: {
        username: '#twsTemplate_Content2_twsModule_txtUser',
        password: '#twsTemplate_Content2_twsModule_txtPSW',
        loginBtn: '#twsTemplate_Content2_twsModule_btnLogin',
        pdr: '#twsTemplate_Content2_twsModule_txtPdr',
        search: '#twsTemplate_Content2_twsModule_btnSearchPDP',
        message: '#twsTemplate_Content2_twsModule_lblMsg',
      },
      cookies: []
    };
    super(Object.assign({}, options, config));
  }

  /**
   * @override
   */
  getScraperCodice() {
    return 'gasplus';
  }
}
