import { TerranovaAbstractScraper, TerranovaAbstractScraperArgs } from './terranova-abstract-scraper';

export default class MediterraneaEnergia extends TerranovaAbstractScraper {
  constructor(options: TerranovaAbstractScraperArgs) {
    const config = {
      loginUrl: 'http://portale.mediterraneaenergia.net/Portal/Index.aspx?idn=000A&area=B2B',
      searchUrl: 'http://portale.mediterraneaenergia.net/Portal/Index.aspx?m=831G&area=B2B',
      fields: {
        username: '#twsTemplate_Header1_Contenuto_twsModule_txtUser',
        password: '#twsTemplate_Header1_Contenuto_twsModule_txtPSW',
        loginBtn: '#twsTemplate_Header1_Contenuto_twsModule_btnLogin',
        pdr: '#twsTemplate_Header1_Contenuto_twsModule_txtPdr',
        search: '#twsTemplate_Header1_Contenuto_twsModule_btnSearchPDP',
        message: '#twsTemplate_Header1_Contenuto_twsModule_lblMsg',
      },
      cookies: []
    };
    super(Object.assign({}, options, config));
  }

  /**
   * @override
   */
  getScraperCodice() {
    return 'mediterraneaenergia';
  }
}
