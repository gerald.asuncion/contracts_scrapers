import { TerranovaAbstractScraper, TerranovaAbstractScraperArgs } from './terranova-abstract-scraper';

export default class Prealpi extends TerranovaAbstractScraper {
  constructor(options: TerranovaAbstractScraperArgs) {
    const config = {
      loginUrl: 'http://venditori.prealpigas.it/Portal/Index.aspx?idn=0028&area=B2B',
      searchUrl: 'http://venditori.prealpigas.it/Portal/Index.aspx?m=831G&area=B2B',
      fields: {
        username: '#twsTemplate_Content2_twsModule_txtUser',
        password: '#twsTemplate_Content2_twsModule_txtPSW',
        loginBtn: '#twsTemplate_Content2_twsModule_btnLogin',
        pdr: '#twsTemplate_Content2_twsModule_txtPdr',
        search: '#twsTemplate_Content2_twsModule_btnSearchPDP',
        message: '#twsTemplate_Content2_twsModule_lblMsg',
      },
      cookies: [{
        name: 'KS',
        value: 'G'
      }]
    };
    super(Object.assign({}, options, config));
  }

  /**
   * @override
   */
  getScraperCodice() {
    return 'prealpi';
  }
}
