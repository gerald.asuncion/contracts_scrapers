import { Page, Protocol } from 'puppeteer';
import waitCheckCredentials from '../../browser/page/waitCheckCredentials';
import { Logger } from '../../logger';
import FailureResponse from '../../response/FailureResponse';
import SuccessResponse from '../../response/SuccessResponse';
import { ScraperResponse, WebScraper } from '../scraper';
import CasoNonGestitoResponse from '../../response/CasoNonGestitoResponse';
import BrowserManager from '../../browser/BrowserManager';
import ScraperOptions, { SCRAPER_TIPOLOGIA } from '../ScraperOptions';

type TerranovaAbstractScraperPayload = {
  pdr?: string;
  pod?: string;
};

export abstract class TerranovaAbstractScraper extends WebScraper<TerranovaAbstractScraperPayload> {
  private loginUrl: any;

  private searchUrl: any;

  private fields: any;

  private cookies: any;

  constructor(options: TerranovaAbstractScraperArgs) {
    super(options);
    const {
      loginUrl,
      searchUrl,
      fields,
      cookies
    } = options;
    this.loginUrl = loginUrl;
    this.searchUrl = searchUrl;
    this.fields = fields;
    this.cookies = cookies || [];
  }

  async login(): Promise<void> {
    const { fields, loginUrl, searchUrl } = this;
    const login = loginUrl || searchUrl;
    this.checkPageClosed();
    const { username, password } = await this.getCredenziali();

    const page = await this.p();
    const response: any = await page.goto(searchUrl);
    const chain = response.request().redirectChain();
    await page.setCookie(...this.cookies);
    try {
      await page.type(fields.username, username);
      await page.type(fields.password, password);
      await page.click(fields.loginBtn);
      await this.checkLoginStatus(page, loginUrl, username);
      await page.goto(searchUrl);
    } catch (e) {
      if (chain.length > 0) {
        await page.goto(login);
        await page.type(fields.username, username);
        await page.type(fields.password, password);
        await page.click(fields.loginBtn);
        await this.checkLoginStatus(page, loginUrl, username);
      }
    }

    await this.checkLoginStatus(page, loginUrl, username);
    await page.goto(searchUrl);

    await Promise.race([
      waitCheckCredentials(page, '#twsTemplate_Content2_twsModule_lblMsg', loginUrl, username, this.childLogger),
      page.waitForSelector(fields.pdr)
    ]);
  }

  private checkLoginStatus(page: Page, url: string, username: string): Promise<void> {
    return waitCheckCredentials(page, '#twsTemplate_Content2_twsModule_lblMsg', url, username, this.childLogger, { timeout: 1000 });
  }

  /**
   * ATTENZIONE: devo lasciare any per via di chi estende questa classe
   * @param {TerranovaAbstractScraperPayload} payload Request payload
   */
  async scrapeWebsite(payload: any): Promise<ScraperResponse> {
    const { pod, pdr } = payload as TerranovaAbstractScraperPayload;
    const { fields } = this;
    this.checkPageClosed();

    const page = await this.p();
    await page.type(fields.pdr, pdr || pod || '');
    await page.click(fields.search);
    await page.waitForSelector(fields.message);
    const content = await page.content();

    const isTrovatiZero = ['POD', 'PDR'].reduce((res, type) => res || content.includes(`${type} trovati: 0`), false);

    if (isTrovatiZero) {
      const message = await page.$eval(fields.message, (el) => el.textContent) || '';
      return new FailureResponse(message);
    }

    const isPosatoEChiuso = ['POD', 'PDR'].reduce(
      // eslint-disable-next-line max-len
      (res, type) => res || (content.includes(`${type} trovati: 1`) && content.toLowerCase().includes('posato') && content.toLowerCase().includes('chiuso')),
      false
    );

    if (isPosatoEChiuso) {
      return new SuccessResponse();
    }

    this.childLogger.error('caso non gestito');
    return new CasoNonGestitoResponse();
  }

  getScraperCodice() {
    return 'terranova';
  }

  getScraperTipologia(): any {
    return SCRAPER_TIPOLOGIA.FATTIBILITA;
  }
}

export interface TerranovaAbstractScraperArgs extends ScraperOptions {
  credentials: {
    username: string,
    password: string,
  },
  loginUrl?: string,
  searchUrl: string,
  cookies?: Protocol.Network.CookieParam[],
  fields: {
    username: string,
    password: string,
    loginBtn: string,
    pdr: string,
    search: string,
    message: string
  },
  browser: BrowserManager,
  logger: Logger
}
