import { TerranovaAbstractScraperArgs } from './terranova-abstract-scraper';
import { TerranovaAbstractScraperType2 } from './terranova-abstract-scraper-type2';

export default class TeaSei extends TerranovaAbstractScraperType2 {
  constructor(options: TerranovaAbstractScraperArgs) {
    const config = {
      credentials: {
        username: 'FRUSSO',
        password: 'TEA00142'
      },
      loginUrl: 'https://teaswitch.teaspa.it/RetiGas/Index.aspx?idn=0009&area=B2B',
      searchUrl: 'https://teaswitch.teaspa.it/RetiGas/Index.aspx?m=835G&area=B2B',
      fields: {
        username: '#twsTemplate_Content2_twsModule_txtUser',
        password: '#twsTemplate_Content2_twsModule_txtPSW',
        loginBtn: '#twsTemplate_Content2_twsModule_btnLogin',
        pdr: '#twsTemplate_Content2_twsModule_cTracciati_cCercaPDR_txtCodicePDP',
        search: '#twsTemplate_Content2_twsModule_cTracciati_cCercaPDR_btn_Cerca',
        message: '#twsTemplate_Content2_twsModule_cTracciati_cCercaPDR_lblMsg',
        select: '#twsTemplate_Content2_twsModule_SceltaTracciato1_ddlCodServizio',
        findBtn: '#twsTemplate_Content2_twsModule_cTracciati_cCercaPDR_btnOpenCercaPDP',
      },
      cookies: []
    };
    super(Object.assign({}, options, config));
  }
}
