import { Page } from 'puppeteer';
import { WebScraper, ScraperResponse } from '../scraper';
import FailureResponse from '../../response/FailureResponse';
import SuccessResponse from '../../response/SuccessResponse';
import CasoNonGestitoResponse from '../../response/CasoNonGestitoResponse';
import { SCRAPER_TIPOLOGIA } from '../ScraperOptions';
import { QueueTask } from '../../task/QueueTask';
import login from '../../v3/erogasmet/utils/login';
import inserisciPdr from '../../v3/erogasmet/utils/inserisciPdr';

export type ErogasmetPayload = {
  pdr: string;
};

async function getText(page: Page, selector: string): Promise<string> {
  return (await page.$eval(selector, (el) => el.textContent?.trim())) || '';
}

@QueueTask()
export default class Erogasmet extends WebScraper<ErogasmetPayload> {
  static readonly LOGIN_URL: string = 'https://portalero.erogasmet.it/Portal/Index.aspx?m=831G&area=B2B';

  async login(): Promise<void> {
    const logger = this.childLogger;
    const credenziali = await this.getCredenziali();
    const page = await this.p();

    logger.info('effettuo la login');

    return login(page, logger, Erogasmet.LOGIN_URL, credenziali);
  }

  async scrapeWebsite(payload: ErogasmetPayload): Promise<ScraperResponse> {
    const logger = this.childLogger;
    logger.info(`effettuo il check per il pdr ${payload.pdr}`);
    const page = await this.p();

    await inserisciPdr(page, payload);

    const esito = await page.$eval<string>(
      // eslint-disable-next-line max-len
      '#form1 > div:nth-child(22) > table > tbody > tr:nth-child(4) > td > table > tbody > tr > td:nth-child(2) > table:nth-child(4) > tbody > tr:nth-child(2) > td > table:nth-child(2) > tbody > tr:nth-child(1) > td > div',
      (el) => el.textContent?.trim() as string
    );

    if (esito.includes('0')) {
      logger.info(`Per il pdr ${payload.pdr} restituisco la FailureResponse 'ko-pdr non contendibile'`);
      return new FailureResponse('ko-pdr non contendibile');
    }

    const noteMisuratore = (await getText(
      page,
      '#twsTemplate_Content2_twsModule_TestataPDP > tbody > tr > td > div > table > tbody > tr.trRptItem > td:nth-child(8)'
    )).toLowerCase();

    logger.info(`note misuratore: ${noteMisuratore}`);

    if (noteMisuratore.includes('posato') || noteMisuratore.includes('chiuso')) {
      const risultato = await this.getResultInfo(page);
      logger.info(`Per il pdr ${payload.pdr} restituisco la SuccessResponse '${risultato}'`);
      return new SuccessResponse(risultato);
    }

    logger.error('caso non gestito');
    return new CasoNonGestitoResponse();
  }

  async getResultInfo(page: Page): Promise<string> {
    const comune = await getText(
      page,
      '#twsTemplate_Content2_twsModule_TestataPDP > tbody > tr > td > div > table > tbody > tr.trRptItem > td:nth-child(2)'
    );

    const indirizzo = await getText(
      page,
      '#twsTemplate_Content2_twsModule_TestataPDP > tbody > tr > td > div > table > tbody > tr.trRptItem > td:nth-child(3)'
    );

    const potenzaMax = await getText(
      page,
      '#twsTemplate_Content2_twsModule_TestataPDP > tbody > tr > td > div > table > tbody > tr.trRptItem > td:nth-child(4)'
    );

    return `${indirizzo} ${comune} portata termica: ${potenzaMax}`;
  }

  getScraperCodice() {
    return 'erogasmet';
  }

  getScraperTipologia() {
    return SCRAPER_TIPOLOGIA.FATTIBILITA;
  }
}
