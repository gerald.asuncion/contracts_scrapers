import { TerranovaAbstractScraper, TerranovaAbstractScraperArgs } from './terranova-abstract-scraper';

export default class Aeg extends TerranovaAbstractScraper {
  constructor(options: TerranovaAbstractScraperArgs) {
    const config = {
      loginUrl: 'http://retegas.cosvim.linkat.it/Portal/Index.aspx?Idn=003F&area=B2B',
      searchUrl: 'http://retegas.cosvim.linkat.it/Portal/Index.aspx?m=831G&area=B2B',
      fields: {
        username: '#twsTemplate_Content1_twsModule_txtUser',
        password: '#twsTemplate_Content1_twsModule_txtPSW',
        loginBtn: '#twsTemplate_Content1_twsModule_btnLogin',
        pdr: '#twsTemplate_Content1_twsModule_txtPdr',
        search: '#twsTemplate_Content1_twsModule_btnSearchPDP',
        message: '#twsTemplate_Content1_twsModule_lblMsg',
      },
      cookies: [{
        name: 'KS',
        value: 'G'
      }]
    };
    super(Object.assign({}, options, config));
  }

  /**
   * @override
   */
  getScraperCodice() {
    return 'cosvim';
  }
}
