import { Protocol } from 'puppeteer';
import waitCheckCredentials from '../../browser/page/waitCheckCredentials';
import { Logger } from '../../logger';
import FailureResponse from '../../response/FailureResponse';
import SuccessResponse from '../../response/SuccessResponse';
import { ScraperResponse, WebScraper } from '../scraper';
import BrowserManager from '../../browser/BrowserManager';
import ScraperOptions, { SCRAPER_TIPOLOGIA } from '../ScraperOptions';

export abstract class TerranovaAbstractScraperType2 extends WebScraper {
  private credentials: any;

  private loginUrl: any;

  private searchUrl: any;

  private fields: any;

  private cookies: any;

  constructor(options: TerranovaAbstractScraperArgs) {
    super(options);
    const {
      credentials,
      loginUrl,
      searchUrl,
      fields,
      cookies
    } = options;
    this.credentials = credentials;
    this.loginUrl = loginUrl;
    this.searchUrl = searchUrl;
    this.fields = fields;
    this.cookies = cookies || [];
  }

  async login(): Promise<void> {
    const login = this.loginUrl || this.searchUrl;
    const { page } = this;
    const { credentials, fields, searchUrl } = this;
    if (!page) {
      throw new Error('Page is closed');
    }

    const response: any = await page.goto(this.searchUrl);
    const chain = response.request().redirectChain();
    await page.setCookie(...this.cookies);
    try {
      await page.type(fields.username, credentials.username);
      await page.type(fields.password, credentials.password);
      await page.click(fields.loginBtn);
      await page.goto(searchUrl);
    } catch (e) {
      if (chain.length > 0) {
        await page.goto(login);
        await page.type(fields.username, credentials.username);
        await page.type(fields.password, credentials.password);
        await page.click(fields.loginBtn);
      }
    }
    await page.goto(searchUrl);

    await Promise.race([
      waitCheckCredentials(page, '#twsTemplate_Header1_Contenuto_twsModule_lblMsg', login, credentials.username, this.childLogger),
      page.waitForSelector(fields.select)
    ]);
  }

  async scrapeWebsite({ pdr, pod }: any):
  Promise<ScraperResponse> {
    const { page } = this;
    const { fields } = this;
    if (!page) {
      throw new Error('Page is closed');
    }

    await page.select(fields.select, 'A01');

    await page.waitForSelector(fields.findBtn);
    await page.click(fields.findBtn);
    await page.waitForResponse(this.searchUrl);
    await page.waitFor(300);
    await page.type(fields.pdr, pdr || pod || '');
    await page.click(fields.search);
    await page.waitForResponse(this.searchUrl);
    await page.waitFor(300);
    await page.waitForSelector(fields.message);
    const content = await page.content();
    const isInvalid = ['POD', 'PDR'].reduce(
      // eslint-disable-next-line max-len
      (res, type) => res || (content.includes(`${type} non trovato per la prestazione`) || (content.includes(`${type} trovati: 1`) && !content.includes('posato e chiuso'))),
      false
    );
    const message = await page.$eval(fields.message, (el) => el.textContent) || '';
    if (!isInvalid) {
      return new SuccessResponse();
    }
    return new FailureResponse(message);
  }

  getScraperCodice() {
    return 'terranova';
  }

  getScraperTipologia(): any {
    return SCRAPER_TIPOLOGIA.FATTIBILITA;
  }
}

export interface TerranovaAbstractScraperArgs extends ScraperOptions {
  credentials: {
    username: string,
    password: string,
  },
  loginUrl?: string,
  searchUrl: string,
  cookies?: Protocol.Network.CookieParam[],
  fields: {
    username: string,
    password: string,
    loginBtn: string,
    pdr: string,
    search: string,
    message: string
  },
  browser: BrowserManager,
  logger: Logger
}
