import { TerranovaAbstractScraper, TerranovaAbstractScraperArgs } from './terranova-abstract-scraper';

export default class AcsmAgam extends TerranovaAbstractScraper {
  constructor(options: TerranovaAbstractScraperArgs) {
    const config = {
      loginUrl: 'http://portal.reti.acsm-agam.it/Portal/Index.aspx?Idn=0005&area=B2B',
      searchUrl: 'https://portal.reti.acsm-agam.it/Portal/Index.aspx?idn=0005&m=831G&area=B2B',
      fields: {
        username: '#twsTemplate_Content1_twsModule_txtUser',
        password: '#twsTemplate_Content1_twsModule_txtPSW',
        loginBtn: '#twsTemplate_Content1_twsModule_btnLogin',
        pdr: '#twsTemplate_Content2_twsModule_txtPdr',
        search: '#twsTemplate_Content2_twsModule_btnSearchPDP',
        message: '#twsTemplate_Content2_twsModule_lblMsg',
      },
      cookies: []
    };
    super(Object.assign({}, options, config));
  }

  /**
   * @override
   */
  getScraperCodice() {
    return 'acsmagam';
  }
}
