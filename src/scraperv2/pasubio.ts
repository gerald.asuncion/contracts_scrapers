import {
  ScraperResponse,
  WebScraper
} from "./scraper";
import FailureResponse from "../response/FailureResponse";
import SuccessResponse from "../response/SuccessResponse";
import WrongCredentialsError from "../errors/WrongCredentialsError";
import { SCRAPER_TIPOLOGIA } from "./ScraperOptions";

export default class Pasubio extends WebScraper {
  private credentials = {
    username: 'H4CEV7G7rV',
    password: ':G,!2.m4@Ynu34~,l?s7'
  }

  async login(): Promise<void> {
    const page = await this.p();
    var url = 'https://portaledistribuzione.ascopiave.it/pasubio/1508portal';
    await page.goto(url);
    await page.type('#ContentPlaceHolder1_txtUtente', this.credentials.username);
    await page.type('#ContentPlaceHolder1_txtPassword', this.credentials.password);
    await page.click('#ContentPlaceHolder1_btnLogIn');

    try {
      await page.waitFor(100);
      const message = await page.$eval('#ContentPlaceHolder1_lblMessaggioLogIn', msg => msg.innerHTML);
      if (message) {
        await Promise.reject(new WrongCredentialsError(url, this.credentials.username));
      }
    } catch (e) {
      if (e instanceof WrongCredentialsError) {
        throw e;
      }
    }
  }

  async scrapeWebsite({ pdr }: any): Promise<ScraperResponse> {
    const page = await this.p();
    await page.goto('https://portaledistribuzione.ascopiave.it/Pasubio/1508Portal/Resources/AscoConsultazionePdr.aspx?')
    await page.waitForSelector("#ContentPlaceHolder1_txtPdrNazionale");
    await page.type("#ContentPlaceHolder1_txtPdrNazionale", pdr);
    await page.click("#ContentPlaceHolder1_btnCerca");
    await page.waitFor(200);
    try {
      const details = await page.$eval('#ContentPlaceHolder1_lblErroreGenerale', msg => msg.textContent);
      if (details) {
        return new FailureResponse(details);
      }
    } catch (e) {
      // nulla da fare a quanto pare
    }
    return new SuccessResponse();
  }

  getScraperCodice() {
    return 'pasubio';
  }

  getScraperTipologia() {
    return SCRAPER_TIPOLOGIA.FATTIBILITA;
  }
}
