import ContrattoPayload from '../../contratti/ContrattoPayload';

export type IberdrolaLoginPayload = ContrattoPayload & {
  callCenter: string;
};

export type Credenziali = {
  username: string;
  password: string;
};
