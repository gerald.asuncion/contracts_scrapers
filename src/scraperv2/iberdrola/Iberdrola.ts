import { WebScraper, ScraperResponse } from '../scraper';
import { IberdrolaLoginPayload } from './types';
import createChildPayloadAwareLogger from '../../utils/createChildPayloadAwareLogger';
import waitForSelectorAndType from '../../browser/page/waitForSelectorAndType';
import waitCheckCredentials from '../../browser/page/waitCheckCredentials';
import IberdrolaPayload from './IberdrolaPayload';
import stepNavigaAllaPaginaNuovoContratto from './inserimento/stepNavigaAllaPaginaNuovoContratto';
import stepFornitura from './inserimento/stepFornitura';
import stepNuovoCliente from './inserimento/stepNuovoCliente';
import stepIndirizzoCliente from './inserimento/stepInidirizzoCliente';
// import stepDettaglioFornitura from './inserimento/stepDettaglioFornitura';
import stepDettaglioFornituraV1 from './inserimento/stepDettaglioFornituraV1';
import stepFatturazione from './inserimento/stepFatturazione';
import tryOrThrow from '../../utils/tryOrThrow';
import FailureResponse from '../../response/FailureResponse';
import waitForSelectorAndClick from '../../browser/page/waitForSelectorAndClick';
import SuccessResponse from '../../response/SuccessResponse';
import extractErrorMessage from '../../utils/extractErrorMessage';
import { SALESFORCE_NAVIGATION_ERROR } from './utils/checkVisualForceNavigationError';
import { SCRAPER_TIPOLOGIA } from '../ScraperOptions';

const PAGE_AVAILABLE_SELECTOR = '#Opportunity_Tab';

export default class Iberdrola extends WebScraper {
  static readonly LOGIN_URL: string = 'https://iberdrola.force.com/login';

  async login(payload: IberdrolaLoginPayload): Promise<void> {
    const logger = createChildPayloadAwareLogger(this.childLogger, payload);
    const { username, password } = await this.getCredenziali(payload);

    this.checkPageClosed();

    const page = await this.p();

    await page.goto(Iberdrola.LOGIN_URL, { waitUntil: 'networkidle0' });

    // attendo il caricamento del primo elemento utile della pagina successiva alla login
    const sel = await page.$(PAGE_AVAILABLE_SELECTOR);
    if (sel !== null) {
      logger.info('step login > utente già loggato');
      return;
    }
    logger.info('step login');

    await tryOrThrow(() => waitForSelectorAndType(page, '#username', username), 'non sono riuscito ad inserire l\'username');

    await tryOrThrow(() => waitForSelectorAndType(page, '#password', password), 'non sono riuscito ad inserire la password');

    await tryOrThrow(() => waitForSelectorAndClick(page, '#Login'), 'non sono riuscito a cliccare sul pulsante `login`');
    await Promise.race([
      waitCheckCredentials(page, '#error.loginError', Iberdrola.LOGIN_URL, username, logger),
      waitCheckCredentials(page, '#pwdintro', Iberdrola.LOGIN_URL, username, logger),
      page.waitForSelector(PAGE_AVAILABLE_SELECTOR, { timeout: 60000 })
    ]);
  }

  async scrapeWebsite(payload: IberdrolaPayload): Promise<ScraperResponse> {
    const logger = createChildPayloadAwareLogger(this.childLogger, payload);
    const page = await this.p();

    try {
      await page.setExtraHTTPHeaders({ 'Accept-Language': 'it' });

      let alertMsg: string;
      page.on('dialog', async (dialog) => {
        alertMsg = await dialog.message();
        await dialog.dismiss();
      });

      logger.info('step navigazione alla pagina di inserimento nuovo contratto');
      await tryOrThrow(() => stepNavigaAllaPaginaNuovoContratto(page), 'step navigazione alla pagina inserimento contratto:');

      logger.info('step fornitura');
      const stepFornituraResult = await tryOrThrow<FailureResponse | null>(() => stepFornitura(page, payload, logger), 'step fornitura:');
      if (stepFornituraResult) {
        return stepFornituraResult;
      }

      logger.info('step nuovo cliente');
      await tryOrThrow(() => stepNuovoCliente(page, payload, () => alertMsg, logger), 'step nuovo cliente:');

      logger.info('step indirizzo cliente');
      await tryOrThrow(() => stepIndirizzoCliente(page, payload, logger), 'step indirizzo cliente:');

      logger.info('step dettaglio fornitura');
      await tryOrThrow(
        async () => stepDettaglioFornituraV1(page, await this.getBrowserContext(), payload, () => alertMsg, logger),
        'step dettaglio fornitura:'
      );

      logger.info('step fatturazione');
      const codiceContratto = await tryOrThrow(() => stepFatturazione(page, payload, logger, this.s3ClientFactory), 'step fatturazione:');

      logger.info(`Contratto inserito con codice cups: ${codiceContratto}`);

      return new SuccessResponse(codiceContratto);
    } catch (ex) {
      let errorMsg = extractErrorMessage(ex);
      await this.saveScreenshot(page, logger, 'iberdrola-errore', payload.idDatiContratto);

      try {
        const content = await this.page?.content() || '';
        if (content.includes(SALESFORCE_NAVIGATION_ERROR)) {
          errorMsg = SALESFORCE_NAVIGATION_ERROR;
        }
      } catch (ex2) {
        logger.warn(`check se il portale non è riuscito a navigare fallito: ${extractErrorMessage(ex2)}`);
        // nulla da fare
      }

      logger.error(errorMsg);
      return new FailureResponse(errorMsg);
    }
  }

  getScraperCodice() {
    return 'iberdrola';
  }

  getScraperTipologia() {
    return SCRAPER_TIPOLOGIA.INSERIMENTO;
  }
}
