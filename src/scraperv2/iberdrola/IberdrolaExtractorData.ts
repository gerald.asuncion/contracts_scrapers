export default interface IberdrolaExtractorData {
  id: string;
  codice: string;
  nome: string;
}

// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface IberdrolaFornitore extends IberdrolaExtractorData {}

// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface IberdrolaDistributore extends IberdrolaExtractorData {}
