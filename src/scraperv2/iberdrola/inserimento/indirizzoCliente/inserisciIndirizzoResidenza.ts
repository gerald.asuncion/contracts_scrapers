import { Page } from 'puppeteer';
import IberdrolaPayload from '../../IberdrolaPayload';
import waitForSelectorAndType from '../../../../browser/page/waitForSelectorAndType';
import waitForSelectorAndSelect from '../../../../browser/page/waitForSelectorAndSelect';
import tryOrThrow from '../../../../utils/tryOrThrow';
import waitForSelectorAndClick from '../../../../browser/page/waitForSelectorAndClick';
import waitForSelectorAndTypeEvaluated from '../../../../browser/page/waitForSelectorAndTypeEvaluated';
import waitForVisible from '../../../../browser/page/waitForVisible';
import waitLoaderHidden from '../../utils/waitLoaderHidden';
import { selezionaComuneResidenza } from '../../utils/selezionaComune';

// eslint-disable-next-line max-len
const sameMailAndPSAddressesSelector = '#newAddressPage\\:newAddressForm\\:newAddressData\\:addressDetailsIT\\:j_id131\\:sameMailAndPSAddresses';
const capResidenzaSelector = '#newAddressPage\\:newAddressForm\\:newAddressData\\:addressDetailsITmail\\:pepe\\:postalCodeIT';
const indirizzoResidenzaSelector = '.calleMailTextClass';
const tipoStradaResidenzaSelector = '#newAddressPage\\:newAddressForm\\:newAddressData\\:addressDetailsITmail\\:j_id158\\:wayType';
const civicoResidenzaSelector = '#newAddressPage\\:newAddressForm\\:newAddressData\\:addressDetailsITmail\\:j_id162\\:numberMailText';

export default async function inserisciIndirizzoResidenza(
  page: Page,
  {
    indirizzoResidenza,
    capResidenza,
    comuneResidenza,
    tipoStradaResidenza,
    civicoResidenza
  }: IberdrolaPayload
): Promise<void> {
  await tryOrThrow(
    () => waitForSelectorAndClick(page, sameMailAndPSAddressesSelector),
    'non sono riuscito ad aprire il pannello per inserire i dati di residenza'
  );

  await tryOrThrow(async () => {
    await waitForVisible(page, capResidenzaSelector);
    await waitForSelectorAndType(page, capResidenzaSelector, capResidenza);
    await page.keyboard.press('Enter'); // Simula il click di `Enter`
    await waitLoaderHidden(page);
    // await delay(10000);
  }, 'non sono riuscito ad inserire il cap');

  // Seleziona la prima città dal menu a tendina
  const comuneResidenzaFixed = comuneResidenza.replace(/'/g, ' ').replace(/-/g, ' ').replace(/ {2}/g, ' ').toUpperCase();
  await tryOrThrow(() => selezionaComuneResidenza(page, comuneResidenzaFixed), 'non sono riuscito a selezionare il comune di residenza');

  await tryOrThrow(async () => {
    await page.focus(indirizzoResidenzaSelector);
    // Se l'indirizzo non esiste lo inserisco a mano
    await waitForSelectorAndTypeEvaluated(page, indirizzoResidenzaSelector, indirizzoResidenza);
    await page.keyboard.press('Enter'); // Simula il click di `Enter`
  }, 'non sono riuscito ad inserire l\'indirizzo di residenza');

  await tryOrThrow(
    () => waitForSelectorAndSelect(page, tipoStradaResidenzaSelector, [tipoStradaResidenza]),
    'non sono riuscito ad inserire il tipo di strada dell\'indirizzo di residenza'
  );

  await tryOrThrow(
    () => waitForSelectorAndType(page, civicoResidenzaSelector, civicoResidenza),
    'non sono riuscito ad inserire il civico dell\'indirizzo di residenza'
  );
}
