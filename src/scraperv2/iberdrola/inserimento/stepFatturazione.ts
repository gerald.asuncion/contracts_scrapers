import { Page } from 'puppeteer';
import IberdrolaPayload from '../IberdrolaPayload';
import { Logger } from '../../../logger';
import waitForVisible from '../../../browser/page/waitForVisible';
import waitForSelectorAndSelect from '../../../browser/page/waitForSelectorAndSelect';
import waitForSelectorAndType from '../../../browser/page/waitForSelectorAndType';
import waitForSelectorAndClickEvaluated from '../../../browser/page/waitForSelectorAndClickEvaluated';
import waitForNavigation from '../../../browser/page/waitForNavigation';
import tryOrThrow, { TryOrThrowError } from '../../../utils/tryOrThrow';
import waitLoaderHidden from '../utils/waitLoaderHidden';
import delay from '../../../utils/delay';
import inserimentoContratto from './fatturazione/inserimentoContratto';
import selezionaServizioVas from './fatturazione/selezionaServizioVas';
import { S3ClientFactoryResult } from '../../../aws/s3ClientFactory';

const CONDIZIONE_ECONOMICA_GAS = '22-01C-DTUA';
const CONDIZIONE_ECONOMICA_LUCE = '22-01C-DTUAP';
const CODICE_ACCREDITAMENTO_AGENTE = 'CMP157';

const PRODOTTO_ENERGIA_CODICE: Record<string, string> = {
  ecoTuaGiornoENotte: '01t4I000006Q64TQAS', // ECOTUA GIORNO&amp;NOTTE P LUCE 24 - (21-03) - DGENL24P
  ecoTuaGiorno: '01t4I000006Q64dQAC', // ECOTUA GIORNO P LUCE 24 - (21-03) - DGIOL24P
  ecoTuaNotte: '01t4I000006Q64nQAC' // ECOTUA NOTTE&amp;WEEKEND P LUCE 24 - (21-03) - DNEWL24P
};

const PRODOTTO_GAS_CODICE: Record<string, string> = {
  ecoTuaInverno: '01t4I000006Q64xQAC', // ECOTUA INVERNO GAS 24 - (21-03) - DINVG24
  ecoTuaQuattroStagioni: '01t4I000006Q64sQAC' // ECOTUA 4STAGIONI GAS 24 - (21-03) - D4STG24
};

const PRODOTTO_SERVIZIO_SEP = '+';

const SERVIZIO_VAS_CODICE: Record<string, string> = {
  tuttofareluce: '01t4I000007AITtQAO',
  tuttofaregas: '01t58000004YtujAAC'
};

const metodoPagamentoSelector = '#NewOpptyPage\\:newOpptyForm\\:newOpptyData\\:facturacionPagoTMK\\:j_id100\\:metodoPago';
const paisDomicilacionSelector = '#NewOpptyPage\\:newOpptyForm\\:newOpptyData\\:accountDetailsIT\\:j_id147\\:paisDomicilacion';
const saveOpptyBtnSelector = '#NewOpptyPage\\:newOpptyForm\\:newOpptyData\\:j_id46\\:saveOppty';
const condizioneEconomicheSelector = '#NewOpptyPage\\:newOpptyForm\\:newOpptyData\\:fogliPrezi_TMK\\:j_id173\\:HHPPpreciosIT';
const codiceAccreditamentoAgenteSelector = '#NewOpptyPage\\:newOpptyForm\\:newOpptyData\\:fogliPrezi_TMK\\:j_id186\\:acreditacionIT';
const dataCompletamentoSelector = '#NewOpptyPage\\:newOpptyForm\\:newOpptyData\\:fogliPrezi_TMK\\:j_id181\\:fechaFirmaIT';
const selettoreProdottiLuce = '#NewOpptyPage\\:newOpptyForm\\:productsList\\:prodottiList\\:j_id210\\:j_id211\\:productsELE';
const selettoreProdottiGas = '#NewOpptyPage\\:newOpptyForm\\:productsList\\:prodottiList\\:j_id210\\:j_id214\\:productsGAS';
const servizioVasSelectSelector = '#NewOpptyPage\\:newOpptyForm\\:productsList\\:serviciList\\:j_id219\\:j_id220\\:servicios';
const servizioVasBtnSelector = '#NewOpptyPage\\:newOpptyForm\\:productsList\\:serviciList\\:j_id218\\:newService';

export default async function stepFatturazione(
  page: Page,
  payload: IberdrolaPayload,
  logger: Logger,
  s3ClientFactory: S3ClientFactoryResult
): Promise<string> {
  const {
    modalitaPagamento,
    iban,
    tipoEnergia,
    codiceABarre,
    prodottoEnergia: prodottoEnergiaConServizio,
    prodottoGas: prodottoGasConServizio
  } = payload;

  await tryOrThrow(
    () => waitForVisible(page, '#NewOpptyPage\\:newOpptyForm', { timeout: 60000 }),
    'non sono riuscito ad aspettare l\'apertura della pagina'
  );
  if (modalitaPagamento === 'sdd') {
    await tryOrThrow(
      () => waitForSelectorAndSelect(page, metodoPagamentoSelector, ['1']),
      'non sono riuscito a selezionare il metodo di pagamento `sdd`'
    );
    // await delay(500);

    // Imposto paese di domiciliazione a `Italia`
    await tryOrThrow(
      () => waitForSelectorAndSelect(page, paisDomicilacionSelector, ['IT']),
      'non sono riuscito a selezionare il paese di domiciliazione'
    );
    // await delay(500);

    // Aggiungo IBAN
    await tryOrThrow(
      () => waitForSelectorAndType(page, '#NewOpptyPage\\:newOpptyForm\\:newOpptyData\\:accountDetailsIT\\:j_id133\\:iban', iban),
      'non sono riusciot ad inserire l\'iban'
    );
  } else if (modalitaPagamento === 'bollettino') {
    await tryOrThrow(
      () => waitForSelectorAndSelect(page, metodoPagamentoSelector, ['3']),
      'non sono riuscito a selezionare la modalità di pagamento `bollettino`'
    );
  }

  await page.focus(saveOpptyBtnSelector);
  await Promise.all([
    waitForSelectorAndClickEvaluated(page, saveOpptyBtnSelector),
    waitForNavigation(page)
  ]);

  if (tipoEnergia === 'gas' || tipoEnergia === 'dual') {
    await tryOrThrow(
      () => waitForSelectorAndSelect(page, condizioneEconomicheSelector, [CONDIZIONE_ECONOMICA_GAS]),
      'non sono riuscito a selezionare la condizione economica gas'
    );
  } else {
    await tryOrThrow(
      () => waitForSelectorAndSelect(page, condizioneEconomicheSelector, [CONDIZIONE_ECONOMICA_LUCE]),
      'non sono riuscito a selezionare la condizione economica luce'
    );
  }

  await tryOrThrow(
    () => waitForSelectorAndType(page, '#NewOpptyPage\\:newOpptyForm\\:newOpptyData\\:fogliPrezi_TMK\\:j_id175\\:CCBB', codiceABarre),
    'non sono riuscito ad inserire il codice a barre'
  );

  await tryOrThrow(
    () => waitForSelectorAndType(page, codiceAccreditamentoAgenteSelector, CODICE_ACCREDITAMENTO_AGENTE),
    'non sono riuscito ad inserire il `codice di acrreditamento agente`'
  );

  const dataCompletamento = await page.evaluate(() => new Date()
    .toLocaleDateString(navigator.language, {
      day: '2-digit',
      month: '2-digit',
      year: 'numeric',
    }));
  await tryOrThrow(
    () => waitForSelectorAndType(page, dataCompletamentoSelector, dataCompletamento),
    'non sono riuscito ad inserire la data di completamento'
  );

  // await delay(3000);
  await waitLoaderHidden(page);
  // await delay(3000);

  const [prodottoGas, servizioVasGas] = (prodottoGasConServizio || '').split(PRODOTTO_SERVIZIO_SEP);
  const [prodottoEnergia, servizioVasLuce] = (prodottoEnergiaConServizio || '').split(PRODOTTO_SERVIZIO_SEP);

  const servizioVasLuceKey = (servizioVasLuce || '').toLowerCase().replace(/\s/g, '');
  const servizioVasGasKey = (servizioVasGas || '').toLowerCase().replace(/\s/g, '');

  if (tipoEnergia === 'elettricita' || tipoEnergia === 'dual') {
    // Imposto il prodotto energia
    if (prodottoEnergia) {
      const selectEnergiaValore = PRODOTTO_ENERGIA_CODICE[prodottoEnergia];

      if (selectEnergiaValore) {
        const prodottoEnergiaSelezionato = await tryOrThrow(async () => {
          // await page.focus(selettoreProdottiLuce);
          // await waitLoaderHidden(page);
          // await delay(2000);
          const prodottoSelezionato = await waitForSelectorAndSelect(page, selettoreProdottiLuce, [selectEnergiaValore]);
          return prodottoSelezionato;
        }, 'non sono riuscito a selezionare il prodotto energia');
        if (!prodottoEnergiaSelezionato || !prodottoEnergiaSelezionato.length) {
          throw new TryOrThrowError(`non sono riuscito a selezionare il prodotto energia: ${prodottoEnergia}`);
        }
        await waitLoaderHidden(page);
        // await delay(3000);
      } else {
        throw new TryOrThrowError('impossibile impostare prodotto energia; valore per la select non trovato.');
      }
    } else {
      throw new TryOrThrowError('impossibile impostare prodotto energia; prodotto desiderato non passato.');
    }
  }

  if (tipoEnergia === 'gas' || tipoEnergia === 'dual') {
    // Imposto il prodotto gas
    if (prodottoGas) {
      const selectGasValore = PRODOTTO_GAS_CODICE[prodottoGas];

      if (selectGasValore) {
        const prodottoGasSelezionato = await tryOrThrow(async () => {
          // await page.focus(selettoreProdottiGas);
          // await waitLoaderHidden(page);
          // await delay(2000);
          const prodottoSelezionato = await waitForSelectorAndSelect(page, selettoreProdottiGas, [selectGasValore]);
          return prodottoSelezionato;
        }, 'non sono riuscito a selezionare il prodotto gas');
        if (!prodottoGasSelezionato || !prodottoGasSelezionato.length) {
          throw new TryOrThrowError(`non sono riuscito a selezionare il prodotto gas: ${prodottoGas}`);
        }
        await waitLoaderHidden(page);
        // await delay(3000);
      } else {
        throw new TryOrThrowError('impossibile impostare prodotto gas; valore per la select non trovato.');
      }
    } else {
      throw new TryOrThrowError('impossibile impostare prodotto gas; prodotto desiderato non passato.');
    }
  }

  // Aggiungo i prodotti selezionati
  await Promise.all([
    waitForSelectorAndClickEvaluated(page, '#NewOpptyPage\\:newOpptyForm\\:productsList\\:prodottiList\\:j_id209\\:newProduct'),
    waitForNavigation(page)
  ]);
  await delay(3000);

  if (servizioVasLuceKey) {
    await tryOrThrow(
      () => selezionaServizioVas(page, servizioVasSelectSelector, servizioVasBtnSelector, SERVIZIO_VAS_CODICE[servizioVasLuceKey]),
      'nono sono riuscito a selezionare il servizio vas `luce`'
    );
  }

  if (servizioVasGasKey) {
    await tryOrThrow(
      () => selezionaServizioVas(page, servizioVasSelectSelector, servizioVasBtnSelector, SERVIZIO_VAS_CODICE[servizioVasGasKey]),
      'nono sono riuscito a selezionare il servizio vas `gas`'
    );
  }

  const codiceContratto = await inserimentoContratto(page, payload, logger, s3ClientFactory);

  return codiceContratto;
}
