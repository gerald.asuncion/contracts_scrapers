import { Page } from 'puppeteer';
import IberdrolaPayload from '../IberdrolaPayload';
import waitForSelectorAndSelect from '../../../browser/page/waitForSelectorAndSelect';
import waitLoaderHidden from '../utils/waitLoaderHidden';
import waitForSelectorAndType from '../../../browser/page/waitForSelectorAndType';
import waitForSelectorAndTypeEvaluated from '../../../browser/page/waitForSelectorAndTypeEvaluated';
import waitForSelectorAndClickEvaluated from '../../../browser/page/waitForSelectorAndClickEvaluated';
import waitForNavigation from '../../../browser/page/waitForNavigation';
import checkStepErrors from '../utils/checkStepErrors';
import { Logger } from '../../../logger';
import tryOrThrow, { TryOrThrowError } from '../../../utils/tryOrThrow';
import checkVisualForceNavigationError from '../utils/checkVisualForceNavigationError';

const TITOLARE_IMMOBILE_MAP: Record<string, string> = {
  proprietario: '01',
  usufruttuario: '03',
  usufruttuario_altro: '03',
  inquilino: '04',
  comodatario: '04'
};

function calcolaCodiceTitolaritaImmobile(titolaritaImmobile: string): string {
  return TITOLARE_IMMOBILE_MAP[titolaritaImmobile] || '01';
}

const UTILIZZO_GAS_MAP: Record<string, string> = {
  riscaldamento: 'C1',
  cottura_acqua_calda: 'C2',
  cottura_cibi: 'C3'
};

function calcolaCodiceUtilizzoGas(utilizzoGas: string): string {
  return UTILIZZO_GAS_MAP[utilizzoGas] || 'C1';
}

const titolaritaImmobileSelector = '#newDPSPage\\:newDPSForm\\:newDPSData\\:DPSDetails_TMK\\:j_id82\\:titularidad';
const attualeFornitoreGasSelector = '#newDPSPage\\:newDPSForm\\:newDPSData\\:DPSGasDetails_TMK\\:j_id162\\:gasMarketer';
const distributoreGasSelector = '#newDPSPage\\:newDPSForm\\:newDPSData\\:DPSGasDetails_TMK\\:j_id168\\:gasMarketer';
const codiceUtilizzoGasSelector = '#newDPSPage\\:newDPSForm\\:newDPSData\\:DPSGasDetails_TMK\\:j_id152\\:codiceProfilo';
const consumoLuceSelector = '#newDPSPage\\:newDPSForm\\:newDPSData\\:DPSElecDetails_TMK\\:j_id253\\:consumoANual';
const potenzaDisponibileSelector = '#newDPSPage\\:newDPSForm\\:newDPSData\\:DPSElecDetails_TMK\\:j_id261\\:potenciaDisponible';
const potenzaImpegnataSelector = '#newDPSPage\\:newDPSForm\\:newDPSData\\:DPSElecDetails_TMK\\:j_id265\\:potenzaImpegnata';
const attualeFornitoreElettricitaSelector = '#newDPSPage\\:newDPSForm\\:newDPSData\\:DPSElecDetails_TMK\\:j_id269\\:elecMarketer';
const selettoreDistributoreEnergiaElettrica = '#newDPSPage\\:newDPSForm\\:newDPSData\\:DPSElecDetails_TMK\\:j_id275\\:eleDist';

export default async function stepDettaglioFornitura(
  page: Page,
  {
    indirizzoResidenza,
    titolaritaImmobile,
    tipoEnergia,
    attualeFornitoreGas,
    distributoreGas,
    utilizzoGas,
    consumoGas,
    attualeFornitoreElettricita,
    distributoreElettricita,
    potenzaImpegnata,
    tensione,
    consumoLuce
  }: IberdrolaPayload,
  getAlertMsg: () => string | null,
  logger: Logger
): Promise<void> {
  const residenzaValue = indirizzoResidenza ? '2' : '1';
  await tryOrThrow(async () => {
    await waitForSelectorAndSelect(page, '.residenzaClass', [residenzaValue]);
    await waitLoaderHidden(page);
    // await delay(2000);
  }, 'non sono riuscito a selezionare il tipo di residenza');

  await tryOrThrow(
    () => waitForSelectorAndSelect(page, titolaritaImmobileSelector, [calcolaCodiceTitolaritaImmobile(titolaritaImmobile)]),
    'non sono riuscito a selezionare la titolartà dell\'immobile'
  );

  if (tipoEnergia === 'gas' || tipoEnergia === 'dual') {
    await tryOrThrow(async () => {
      await waitForSelectorAndType(page, attualeFornitoreGasSelector, attualeFornitoreGas);

      // await waitForSelectorAndTypeEvaluated(page, '.marketerGasLookUpIdClass', attualeFornitoreGas);
      // await waitForSelectorAndTypeEvaluated(page, '#newDPSPage\\:newDPSForm\\:ComerGaslookUpHidden', attualeFornitoreGas);
      // await waitForSelectorAndTypeEvaluated(page, '#newDPSPage\\:newDPSForm\\:ComerGaslookUpIdHidden', attualeFornitoreGas);
    }, 'non sono riuscito ad inserire l\'attuale fornitore gas');

    await tryOrThrow(async () => {
      await waitForSelectorAndType(page, distributoreGasSelector, distributoreGas);

      // await waitForSelectorAndTypeEvaluated(page, '.distribuitorGasLookUpIdClass', distributoreGas);
      // await waitForSelectorAndTypeEvaluated(page, '#distribuidoraGasId', distributoreGas);

      // await delay(500);
    }, 'non sono riuscito ad inserire il distributore di gas');

    const codiceUtilizzoGas = calcolaCodiceUtilizzoGas(utilizzoGas);
    await tryOrThrow(
      () => waitForSelectorAndSelect(page, codiceUtilizzoGasSelector, [codiceUtilizzoGas]),
      'non sono riuscito ad inserire l\'utilizzo di gas'
    );

    // await delay(2000);

    // Imposto la “Classe prelievo” = 7
    await tryOrThrow(
      () => waitForSelectorAndSelect(page, '#newDPSPage\\:newDPSForm\\:newDPSData\\:DPSGasDetails_TMK\\:j_id157\\:elecTension', ['1']),
      'non sono riuscito a selezionare la classe di prelievo'
    );

    await tryOrThrow(
      () => waitForSelectorAndType(page, '#newDPSPage\\:newDPSForm\\:newDPSData\\:DPSGasDetails_TMK\\:j_id147\\:consumAnual', consumoGas),
      'non sono riuscito ad inserire il consumo di gas'
    );
  }

  if (tipoEnergia === 'elettricita' || tipoEnergia === 'dual') {
    await tryOrThrow(
      () => waitForSelectorAndType(page, consumoLuceSelector, consumoLuce),
      'non sono riuscito ad inserire il consumo di luce'
    );

    await tryOrThrow(
      () => waitForSelectorAndSelect(page, '#newDPSPage\\:newDPSForm\\:newDPSData\\:DPSElecDetails_TMK\\:j_id255\\:j_id259', [tensione]),
      'non sono riuscito ad inserire la tensione'
    );

    // eslint-disable-next-line max-len
    const potenzaDisponibile = potenzaImpegnata === '4,5' ? '5,00' : String(((potenzaImpegnata as number) * 1.1).toFixed(2)).replace('.', ',');

    await tryOrThrow(
      () => waitForSelectorAndType(page, potenzaDisponibileSelector, potenzaDisponibile),
      'non sono riuscito ad inserire la potenza disponibile'
    );

    await tryOrThrow(
      () => waitForSelectorAndType(page, potenzaImpegnataSelector, potenzaImpegnata as string),
      'non sono riuscito ad inserire la potenza impegnata'
    );

    await tryOrThrow(async () => {
      await waitForSelectorAndType(page, attualeFornitoreElettricitaSelector, attualeFornitoreElettricita);

      // await waitForSelectorAndTypeEvaluated(page, '.marketerEleLookUpIdClass', attualeFornitoreElettricita);
      // await waitForSelectorAndTypeEvaluated(page, '#newDPSPage\\:newDPSForm\\:ComerElelookUpHidden', attualeFornitoreElettricita);
      // await waitForSelectorAndTypeEvaluated(page, '#newDPSPage\\:newDPSForm\\:ComerElelookUpIdHidden', attualeFornitoreElettricita);
    }, 'non sono riuscito ad inserie l\'attuale fornitore di elettricità');

    const distributoreEnergiaElettricaValore = await tryOrThrow(
      () => page.evaluate(
        (_selettoreDistributoreEnergiaElettrica) => document.querySelector(_selettoreDistributoreEnergiaElettrica).value,
        selettoreDistributoreEnergiaElettrica
      ),
      'non sono riuscito a controllare se il distributore di elettricità è già valorizzato'
    );

    if (!distributoreEnergiaElettricaValore) {
      await tryOrThrow(async () => {
        await waitForSelectorAndTypeEvaluated(page, selettoreDistributoreEnergiaElettrica, distributoreElettricita);

        // await waitForSelectorAndTypeEvaluated(page, '#newDPSPage\\:newDPSForm\\:DistribElelookUpHidden', distributoreElettricita);
        // await waitForSelectorAndTypeEvaluated(page, '.distribuitorEleLookUpIdClass', distributoreElettricita);
        // await waitForSelectorAndTypeEvaluated(page, '#newDPSPage\\:newDPSForm\\:DistribElelookUpIdHidden', distributoreElettricita);
      }, 'non sono riuscito ad inserire il distributore di elettricità');
    }
  }

  await Promise.all([
    waitForSelectorAndClickEvaluated(page, '#newDPSPage\\:newDPSForm\\:newDPSData\\:DPSButtons\\:bottom\\:iniOppty'),
    Promise.race([
      page.waitForResponse(() => true),
      waitForNavigation(page)
    ])
  ]);

  await checkStepErrors(page, '#newDPSPage\\:pageMessages', logger);

  const alertMsg = getAlertMsg();
  if (alertMsg) {
    throw new TryOrThrowError(alertMsg);
  }

  await checkVisualForceNavigationError(page);
}
