import { Page } from 'puppeteer';
import IberdrolaPayload from '../IberdrolaPayload';
import { Logger } from '../../../logger';
import tryOrThrow, { TryOrThrowError } from '../../../utils/tryOrThrow';
import waitForSelectorAndSelect from '../../../browser/page/waitForSelectorAndSelect';
import waitLoaderHidden from '../utils/waitLoaderHidden';
import waitForSelectorAndClickEvaluated from '../../../browser/page/waitForSelectorAndClickEvaluated';
import waitForNavigation from '../../../browser/page/waitForNavigation';
import checkStepErrors from '../utils/checkStepErrors';
import checkVisualForceNavigationError from '../utils/checkVisualForceNavigationError';
import inserisciDettaglioFornituraGas from './dettaglioFornitura/inserisciDettaglioFornituraGas';
import inserisciDettaglioFornituraElettricita from './dettaglioFornitura/inserisciDettaglioFornituraElettricita';
import BrowserContextManager from '../../../browser/BrowserContextManager';

const TITOLARE_IMMOBILE_DEF = '01';

const TITOLARE_IMMOBILE_MAP: Record<string, string> = {
  proprietario: TITOLARE_IMMOBILE_DEF,
  usufruttuario: '03',
  usufruttuario_altro: '03',
  inquilino: '04',
  comodatario: '04'
};

function calcolaCodiceTitolaritaImmobile(titolaritaImmobile: string): string {
  return TITOLARE_IMMOBILE_MAP[titolaritaImmobile] || TITOLARE_IMMOBILE_DEF;
}

const titolaritaImmobileSelector = '#newDPSPage\\:newDPSForm\\:newDPSData\\:DPSDetails_TMK\\:j_id82\\:titularidad';

export default async function stepDettaglioFornituraV1(
  page: Page,
  browserContext: BrowserContextManager,
  payload: IberdrolaPayload,
  getAlertMsg: () => string | null,
  logger: Logger
): Promise<void> {
  const {
    indirizzoResidenza,
    titolaritaImmobile,
    tipoEnergia
  } = payload;
  const residenzaValue = indirizzoResidenza ? '2' : '1';
  await tryOrThrow(async () => {
    await waitForSelectorAndSelect(page, '.residenzaClass', [residenzaValue]);
    await waitLoaderHidden(page);
    // await delay(2000);
  }, 'non sono riuscito a selezionare il tipo di residenza');

  await tryOrThrow(
    () => waitForSelectorAndSelect(page, titolaritaImmobileSelector, [calcolaCodiceTitolaritaImmobile(titolaritaImmobile)]),
    'non sono riuscito a selezionare la titolartà dell\'immobile'
  );

  if (tipoEnergia === 'gas' || tipoEnergia === 'dual') {
    await tryOrThrow(
      () => inserisciDettaglioFornituraGas(page, browserContext, payload),
      'non sono riuscito ad inserire i dati della fornitura gas:'
    );
  }

  if (tipoEnergia === 'elettricita' || tipoEnergia === 'dual') {
    await tryOrThrow(
      () => inserisciDettaglioFornituraElettricita(page, browserContext, payload),
      'non sono riuscito ad inserire i dati della fornitura luce:'
    );
  }

  await Promise.all([
    waitForSelectorAndClickEvaluated(page, '#newDPSPage\\:newDPSForm\\:newDPSData\\:DPSButtons\\:bottom\\:iniOppty'),
    Promise.race([
      page.waitForResponse(() => true),
      waitForNavigation(page)
    ])
  ]);

  await checkStepErrors(page, '#newDPSPage\\:pageMessages', logger);

  const alertMsg = getAlertMsg();
  if (alertMsg) {
    throw new TryOrThrowError(alertMsg);
  }

  await checkVisualForceNavigationError(page);
}
