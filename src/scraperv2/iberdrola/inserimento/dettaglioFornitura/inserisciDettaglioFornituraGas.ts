import { Page } from 'puppeteer';
import IberdrolaPayload from '../../IberdrolaPayload';
import waitForSelectorAndSelect from '../../../../browser/page/waitForSelectorAndSelect';
import waitForSelectorAndType from '../../../../browser/page/waitForSelectorAndType';
import waitForSelectorAndClick from '../../../../browser/page/waitForSelectorAndClick';
import waitForSelectorAndTypeEvaluated from '../../../../browser/page/waitForSelectorAndTypeEvaluated';
import BrowserContextManager from '../../../../browser/BrowserContextManager';

const UTILIZZO_GAS_DEF = 'C1';

const UTILIZZO_GAS_MAP: Record<string, string> = {
  riscaldamento: UTILIZZO_GAS_DEF,
  cottura_acqua_calda: 'C2',
  cottura_cibi: 'C3'
};

export function calcolaCodiceUtilizzoGas(utilizzoGas: string): string {
  return UTILIZZO_GAS_MAP[utilizzoGas] || UTILIZZO_GAS_DEF;
}

const codiceUtilizzoGasSelector = '#newDPSPage\\:newDPSForm\\:newDPSData\\:DPSGasDetails_TMK\\:j_id152\\:codiceProfilo';
const attualeFornitoreGasSearchSelector = '#ComercilizadoraSearchVLookUp\\:ComercializadoralookupForm\\:block\\:section\\:txtSearch';
const valueFornitoreEnergiaSelector = '#newDPSPage\\:newDPSForm\\:newDPSData\\:DPSGasDetails_TMK\\:j_id162\\:gasMarketer';
const distributoreGasSearchSelector = '#ComercilizadoraSearchVLookUp\\:DistribuidoralookupForm\\:block\\:section\\:txtSearch';
const valueDistributoreGasSelector = '#newDPSPage\\:newDPSForm\\:newDPSData\\:DPSGasDetails_TMK\\:j_id168\\:gasMarketer';

export default async function inserisciDettaglioFornituraGas(
  page: Page,
  browserContext: BrowserContextManager,
  {
    attualeFornitoreGas,
    distributoreGas,
    utilizzoGas,
    consumoGas
  }: IberdrolaPayload
): Promise<void> {
  const codiceUtilizzoGas = calcolaCodiceUtilizzoGas(utilizzoGas);
  await waitForSelectorAndSelect(page, codiceUtilizzoGasSelector, [codiceUtilizzoGas]);
  // await delay(2000);
  // Imposto la “Classe prelievo” = 7
  await waitForSelectorAndSelect(page, '#newDPSPage\\:newDPSForm\\:newDPSData\\:DPSGasDetails_TMK\\:j_id157\\:elecTension', ['1']);

  await waitForSelectorAndType(page, '#newDPSPage\\:newDPSForm\\:newDPSData\\:DPSGasDetails_TMK\\:j_id147\\:consumAnual', consumoGas);

  // ### START calcolo e selezione del codice fornitore uscente
  const fornitorePage = await browserContext.newPage();
  await fornitorePage.goto('https://iberdrola.force.com/apex/IBD_VFP014_PropAdminNewComerSearch?tipo_energia=2&territorio=Italy');
  await waitForSelectorAndType(fornitorePage, attualeFornitoreGasSearchSelector, attualeFornitoreGas);
  await waitForSelectorAndClick(fornitorePage, '#ComercilizadoraSearchVLookUp\\:ComercializadoralookupForm\\:block\\:section\\:btnGo');
  // await delay(2000);
  const selector = '.dataRow.first .dataCell a';
  const valueFornitoreEnergia = await fornitorePage.$eval(selector, (e) => e.innerHTML);

  const fornitoreEnergiaModalFormIdNascosto = await fornitorePage.evaluate(
    // eslint-disable-next-line newline-per-chained-call
    (_selector) => document.querySelector(_selector).getAttribute('onclick').match(/'(.*?)'/).shift().replace(/'/g, ''),
    selector
  );

  await fornitorePage.close();
  await waitForSelectorAndType(page, valueFornitoreEnergiaSelector, valueFornitoreEnergia);
  await waitForSelectorAndTypeEvaluated(page, '.marketerGasLookUpIdClass', fornitoreEnergiaModalFormIdNascosto);
  await waitForSelectorAndTypeEvaluated(page, '#newDPSPage\\:newDPSForm\\:ComerGaslookUpHidden', fornitoreEnergiaModalFormIdNascosto);
  await waitForSelectorAndTypeEvaluated(page, '#newDPSPage\\:newDPSForm\\:ComerGaslookUpIdHidden', fornitoreEnergiaModalFormIdNascosto);
  // ### END
  // ### START calcolo e selezione del codice distributore uscente
  const distributorePage = await browserContext.newPage();
  await distributorePage.goto('https://iberdrola.force.com/apex/IBD_VFP015_PropAdminNewDistribSearch?tipo_energia=2&territorio=Italy');
  await waitForSelectorAndType(distributorePage, distributoreGasSearchSelector, distributoreGas);
  await waitForSelectorAndClick(distributorePage, '#ComercilizadoraSearchVLookUp\\:DistribuidoralookupForm\\:block\\:section\\:btnGo');
  // await delay(2000);
  const valueDistributoreGas = await distributorePage.$eval('.dataRow.first a', (e) => e.innerHTML);

  const distributoreGasModalFormIdNascosto = await distributorePage.evaluate(
    (_selector) => document.querySelector(_selector).getAttribute('onclick').match(/'(.*?)'/).shift()
      .replace(/'/g, ''),
    selector
  );
  await distributorePage.close();

  await waitForSelectorAndType(page, valueDistributoreGasSelector, valueDistributoreGas);
  await waitForSelectorAndTypeEvaluated(page, '.distribuitorGasLookUpIdClass', distributoreGasModalFormIdNascosto);
  await waitForSelectorAndTypeEvaluated(page, '#distribuidoraGasId', distributoreGasModalFormIdNascosto);
  // ### END
}
