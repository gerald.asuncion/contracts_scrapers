import { Page } from 'puppeteer';
import IberdrolaPayload from '../../IberdrolaPayload';
import tryOrThrow from '../../../../utils/tryOrThrow';
import waitForSelectorAndSelect from '../../../../browser/page/waitForSelectorAndSelect';
import waitForSelectorAndType from '../../../../browser/page/waitForSelectorAndType';
import waitForSelectorAndClick from '../../../../browser/page/waitForSelectorAndClick';
import waitForSelectorAndTypeEvaluated from '../../../../browser/page/waitForSelectorAndTypeEvaluated';
import BrowserContextManager from '../../../../browser/BrowserContextManager';

const consumoLuceSelector = '#newDPSPage\\:newDPSForm\\:newDPSData\\:DPSElecDetails_TMK\\:j_id253\\:consumoANual';
const potenzaDisponibileSelector = '#newDPSPage\\:newDPSForm\\:newDPSData\\:DPSElecDetails_TMK\\:j_id261\\:potenciaDisponible';
const potenzaImpegnataSelector = '#newDPSPage\\:newDPSForm\\:newDPSData\\:DPSElecDetails_TMK\\:j_id265\\:potenzaImpegnata';

export default async function inserisciDettaglioFornituraElettricita(
  page: Page,
  browserContext: BrowserContextManager,
  {
    attualeFornitoreElettricita,
    distributoreElettricita,
    potenzaImpegnata,
    tensione,
    consumoLuce
  }: IberdrolaPayload
): Promise<void> {
  await tryOrThrow(
    () => waitForSelectorAndType(page, consumoLuceSelector, consumoLuce),
    'non sono riuscito ad inserire il consumo di luce'
  );

  await tryOrThrow(
    () => waitForSelectorAndSelect(page, '#newDPSPage\\:newDPSForm\\:newDPSData\\:DPSElecDetails_TMK\\:j_id255\\:j_id259', [tensione]),
    'non sono riuscito ad inserire la tensione'
  );

  // eslint-disable-next-line max-len
  const potenzaDisponibile = potenzaImpegnata === '4,5' ? '5,00' : String(((potenzaImpegnata as number) * 1.1).toFixed(2)).replace('.', ',');

  await tryOrThrow(
    () => waitForSelectorAndType(page, potenzaDisponibileSelector, potenzaDisponibile),
    'non sono riuscito ad inserire la potenza disponibile'
  );

  await tryOrThrow(
    () => waitForSelectorAndType(page, potenzaImpegnataSelector, potenzaImpegnata as string),
    'non sono riuscito ad inserire la potenza impegnata'
  );

  // await delay(2000);
  const fornitorePage = await browserContext.newPage();
  await fornitorePage.goto('https://iberdrola.force.com/apex/IBD_VFP014_PropAdminNewComerSearch?tipo_energia=1&territorio=Italy');

  // eslint-disable-next-line max-len
  await waitForSelectorAndType(fornitorePage, '#ComercilizadoraSearchVLookUp\\:ComercializadoralookupForm\\:block\\:section\\:txtSearch', attualeFornitoreElettricita);
  // eslint-disable-next-line max-len
  await waitForSelectorAndClick(fornitorePage, '#ComercilizadoraSearchVLookUp\\:ComercializadoralookupForm\\:block\\:section\\:btnGo');
  // await delay(2000);
  const selector = '.dataRow.first a';

  // eslint-disable-next-line max-len
  const valueFornitoreEnergiaElettricita = await fornitorePage.$eval(selector, (e) => e.innerHTML);

  const fornitoreEnergiaElettricitaModalFormIdNascosto = await fornitorePage.evaluate(
    // eslint-disable-next-line newline-per-chained-call
    (_selector) => document.querySelector(_selector).getAttribute('onclick').match(/'(.*?)'/).shift().replace(/'/g, ''),
    selector
  );

  await fornitorePage.close();
  // eslint-disable-next-line max-len
  await waitForSelectorAndType(page, '#newDPSPage\\:newDPSForm\\:newDPSData\\:DPSElecDetails_TMK\\:j_id269\\:elecMarketer', valueFornitoreEnergiaElettricita);
  await waitForSelectorAndTypeEvaluated(page, '.marketerEleLookUpIdClass', fornitoreEnergiaElettricitaModalFormIdNascosto);
  // eslint-disable-next-line max-len
  await waitForSelectorAndTypeEvaluated(page, '#newDPSPage\\:newDPSForm\\:ComerElelookUpHidden', fornitoreEnergiaElettricitaModalFormIdNascosto);
  // eslint-disable-next-line max-len
  await waitForSelectorAndTypeEvaluated(page, '#newDPSPage\\:newDPSForm\\:ComerElelookUpIdHidden', fornitoreEnergiaElettricitaModalFormIdNascosto);
  // ### END
  // await delay(1000);
  const selettoreDistributoreEnergiaElettrica = '#newDPSPage\\:newDPSForm\\:newDPSData\\:DPSElecDetails_TMK\\:j_id275\\:eleDist';
  const distributoreEnergiaElettricaValore = await page.evaluate(
    (_selettoreDistributoreEnergiaElettrica) => document
      .querySelector(_selettoreDistributoreEnergiaElettrica).value,
    selettoreDistributoreEnergiaElettrica
  );

  // controlla se il distrubutore non è già valorizzato
  if (!distributoreEnergiaElettricaValore) {
    // ### START calcolo e selezione del codice distributore uscente
    const distributorePage = await browserContext.newPage();
    await distributorePage.goto('https://iberdrola.force.com/apex/IBD_VFP015_PropAdminNewDistribSearch?tipo_energia=1&territorio=Italy');
    // eslint-disable-next-line max-len
    await waitForSelectorAndType(distributorePage, '#ComercilizadoraSearchVLookUp\\:DistribuidoralookupForm\\:block\\:section\\:txtSearch', distributoreElettricita);
    // eslint-disable-next-line max-len
    await waitForSelectorAndClick(distributorePage, '#ComercilizadoraSearchVLookUp\\:DistribuidoralookupForm\\:block\\:section\\:btnGo');
    // await delay(2000);
    // eslint-disable-next-line max-len
    const valueDistributoreElettricita = await distributorePage.$eval('.dataRow.first .dataCell a', (e) => e.innerHTML);

    const distributoreElettricitaModalFormIdNascosto = await distributorePage.evaluate(
      // eslint-disable-next-line newline-per-chained-call
      (_selector) => document.querySelector(_selector).getAttribute('onclick').match(/'(.*?)'/).shift().replace(/'/g, ''),
      selector
    );
    await distributorePage.close();

    await waitForSelectorAndTypeEvaluated(page, selettoreDistributoreEnergiaElettrica, valueDistributoreElettricita);
    await waitForSelectorAndTypeEvaluated(page, '#newDPSPage\\:newDPSForm\\:DistribElelookUpHidden', valueDistributoreElettricita);
    await waitForSelectorAndTypeEvaluated(page, '.distribuitorEleLookUpIdClass', distributoreElettricitaModalFormIdNascosto);
    // eslint-disable-next-line max-len
    await waitForSelectorAndTypeEvaluated(page, '#newDPSPage\\:newDPSForm\\:DistribElelookUpIdHidden', distributoreElettricitaModalFormIdNascosto);
    // ### END
  }
}
