import { Page } from 'puppeteer';
import IberdrolaPayload, { TipologiaCliente, TipologiaDocumento, TipologiaDocumentoResidenziale } from '../IberdrolaPayload';
import { Logger } from '../../../logger';
import waitForVisible from '../../../browser/page/waitForVisible';
import waitForSelectorAndSelect from '../../../browser/page/waitForSelectorAndSelect';
import waitForSelectorAndType from '../../../browser/page/waitForSelectorAndType';
import waitForSelectorAndClickEvaluated from '../../../browser/page/waitForSelectorAndClickEvaluated';
import checkStepErrors from '../utils/checkStepErrors';
import waitLoaderHidden from '../utils/waitLoaderHidden';
import tryOrThrow, { TryOrThrowError } from '../../../utils/tryOrThrow';
import waitForTimeout from '../../../browser/page/waitForTimeout';
import checkVisualForceNavigationError from '../utils/checkVisualForceNavigationError';
import delay from '../../../utils/delay';
import waitForNavigation from '../../../browser/page/waitForNavigation';

const SELETTORE_TIPOLOGIA_CLIENTE: TipologiaCliente = {
  residenziale: 'R',
  business: 'P',
};

const SELETTORE_TIPOLOGIA_DOCUMENTO: TipologiaDocumento = {
  residenziale: {
    cartaIdentita: 'CIT',
    passaporto: 'PS',
    patente: 'PA'
  },
  business: 'PI'
};

const modalitaConfermaContratto = 'FirmaDigitale';
const tipologiaClienteSelector = '#newClientPage\\:newClientForm\\:newClientData\\:clientDetailsTMK\\:j_id122\\:j_id124 > div > select';
const tipoDocumentoSelector = '#newClientPage\\:newClientForm\\:newClientData\\:clientDetailsTMK\\:j_id138\\:identityNumberType';
const numeroDocumentoSelector = '#newClientPage\\:newClientForm\\:newClientData\\:clientDetailsTMK\\:j_id148\\:identityNumberValue';
const codiceFiscaleRappresentanteSelector = '#newClientPage\\:newClientForm\\:newClientData\\:clientDetailsTMK\\:j_id158\\:codFisc';
const nomeRappresentanteSelector = '#newClientPage\\:newClientForm\\:newClientData\\:clientDetailsTMK\\:j_id143\\:cifName';
const cognomeRappresentanteSelector = '#newClientPage\\:newClientForm\\:newClientData\\:clientDetailsTMK\\:j_id153\\:surname';
const dataDiNascitaRappresentanteSelector = '#newClientPage\\:newClientForm\\:newClientData\\:clientDetailsTMK\\:j_id163\\:birthDate';
const telefonoRappresentanteSelector = '#newClientPage\\:newClientForm\\:newClientData\\:contactDetailsTMK\\:j_id219\\:secondaryPhone';
const emailRappresentanteSelector = '#newClientPage\\:newClientForm\\:newClientData\\:contactDetailsTMK\\:j_id229\\:email';
const ragioneSocialeRappresentanteSelector = '#newClientPage\\:newClientForm\\:newClientData\\:clientDetailsTMK\\:j_id127\\:businessName';
const pecRappresentanteSelector = '#newClientPage\\:newClientForm\\:newClientData\\:contactDetailsTMK\\:j_id229\\:PEC';
const modalitaConfermaContrattoSelector = '#newClientPage\\:newClientForm\\:newClientData\\:contactDetailsTMK\\:j_id245\\:modoConfirmacion';
// eslint-disable-next-line max-len
const primoConsensoSelector = '#newClientPage\\:newClientForm\\:newClientData\\:contactDetailsTMK\\:j_id214\\:NonContattabileIniziativeTerzi';
// eslint-disable-next-line max-len
const secondoConsensoSelector = '#newClientPage\\:newClientForm\\:newClientData\\:contactDetailsTMK\\:j_id224\\:NonContattabileIniziativeTerzi';
// eslint-disable-next-line max-len
const terzoConsensoSelector = '#newClientPage\\:newClientForm\\:newClientData\\:contactDetailsTMK\\:j_id234\\:NonContattabileIniziativeIberdrola';
const saveClientButtonSelector = '#newClientPage\\:newClientForm\\:newClientData\\:j_id35\\:bottom\\:saveClient';

export default async function stepNuovoCliente(
  page: Page,
  {
    tipoIntestatario,
    tipoDocumento,
    numeroDocumento,
    codiceFiscaleRappresentante,
    nomeRappresentante,
    cognomeRappresentante,
    dataDiNascitaRappresentante,
    telefonoRappresentante,
    emailRappresentante,
    ragioneSocialeRappresentante,
    PECRappresentante,
    primoConsenso,
    secondoConsenso,
    terzoConsenso
  }: IberdrolaPayload,
  getAlertMsg: () => string | null,
  logger: Logger
): Promise<void> {
  await waitForVisible(page, tipologiaClienteSelector);

  await tryOrThrow(
    () => waitForSelectorAndSelect(page, tipologiaClienteSelector, [SELETTORE_TIPOLOGIA_CLIENTE[tipoIntestatario]]),
    'non sono riuscito a selezionare il tipo di intestatario'
  );
  await delay(3000);

  const tipoDocumentoValue = (tipoIntestatario === 'business'
    ? SELETTORE_TIPOLOGIA_DOCUMENTO.business
    : SELETTORE_TIPOLOGIA_DOCUMENTO.residenziale[tipoDocumento]
  ) as keyof TipologiaDocumentoResidenziale;
  await tryOrThrow(
    () => waitForSelectorAndSelect(page, tipoDocumentoSelector, [tipoDocumentoValue]),
    'non sono riuscito a selezionare il tipo di documento'
  );

  await tryOrThrow(
    () => waitForSelectorAndType(page, numeroDocumentoSelector, numeroDocumento),
    'non sono riuscito ad inserire il numero di documento'
  );

  await tryOrThrow(async () => {
    await waitForSelectorAndType(page, codiceFiscaleRappresentanteSelector, codiceFiscaleRappresentante);
    await page.keyboard.press('Enter');
    await waitLoaderHidden(page);
    // await delay(3000);
  }, 'non sono riuscito ad inserire il codice fiscale del rappresentante');

  if (tipoIntestatario === 'residenziale') {
    await tryOrThrow(
      () => waitForSelectorAndType(page, nomeRappresentanteSelector, nomeRappresentante),
      'non sono riuscito ad inserire il nome del rappresentante'
    );

    await tryOrThrow(
      () => waitForSelectorAndType(page, cognomeRappresentanteSelector, cognomeRappresentante),
      'non sono riuscito ad inserire il cognome del rappresentante'
    );

    await tryOrThrow(
      () => waitForSelectorAndType(page, dataDiNascitaRappresentanteSelector, dataDiNascitaRappresentante),
      'non sono riuscito ad inserire la data di nascita del rappresentante'
    );
  }

  await tryOrThrow(
    async () => {
      await waitForVisible(page, telefonoRappresentanteSelector);
      await page.focus(telefonoRappresentanteSelector);
      await waitForSelectorAndType(page, telefonoRappresentanteSelector, telefonoRappresentante);
    },
    'non sono riuscito ad inserire il telefono del rappresentante'
  );

  await tryOrThrow(
    () => waitForSelectorAndType(page, emailRappresentanteSelector, emailRappresentante),
    'non sono riuscito ad inserire l\'email del rappresentante'
  );

  if (tipoIntestatario === 'business') {
    if (ragioneSocialeRappresentante != null) {
      await tryOrThrow(
        () => waitForSelectorAndType(page, ragioneSocialeRappresentanteSelector, ragioneSocialeRappresentante),
        'non sono riuscito ad inserire la ragione sociale del rappresentante'
      );
    }

    if (PECRappresentante != null) {
      await tryOrThrow(
        () => waitForSelectorAndType(page, pecRappresentanteSelector, PECRappresentante),
        'non sono riuscito ad inserire la pec del rappresentante'
      );
    }
  }

  await tryOrThrow(
    () => waitForSelectorAndSelect(page, modalitaConfermaContrattoSelector, [modalitaConfermaContratto]),
    'non sono riuscito ad inserire la modalità di conferma contratto'
  );

  await tryOrThrow(
    () => waitForSelectorAndSelect(page, primoConsensoSelector, [primoConsenso ? 'SI' : 'NO']),
    'non sono riuscito ad inserire il primo consenso'
  );

  await tryOrThrow(
    () => waitForSelectorAndSelect(page, secondoConsensoSelector, [secondoConsenso ? 'SI' : 'NO']),
    'non sono riuscito ad inserire il secondo consenso'
  );

  await tryOrThrow(
    () => waitForSelectorAndSelect(page, terzoConsensoSelector, ['2']), // [terzoConsenso ? '1' : '2']),
    'non sono riuscito ad inserire il terzo consenso'
  );

  await tryOrThrow(
    async () => {
      await page.focus(saveClientButtonSelector);
      await waitForSelectorAndClickEvaluated(page, saveClientButtonSelector, { visible: true });
    },
    'non sono riuscito a cliccare sul pulsante `continua`'
  );

  await waitLoaderHidden(page);
  await waitForTimeout(page, 2000);

  // TODO: trovare il modo di usarlo anche negli altri step
  await checkStepErrors(page, '#newClientPage\\:pageMessages', logger);

  const alertMsg = getAlertMsg();
  if (alertMsg) {
    throw new TryOrThrowError(alertMsg);
  }

  await checkVisualForceNavigationError(page);
}
