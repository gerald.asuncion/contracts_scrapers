import { Page } from 'puppeteer';
import waitForSelectorAndClick from '../../../browser/page/waitForSelectorAndClick';
import waitForNavigation from '../../../browser/page/waitForNavigation';
import tryOrThrow from '../../../utils/tryOrThrow';

export default async function stepNavigaAllaPaginaNuovoContratto(page: Page): Promise<void> {
  await tryOrThrow(() => Promise.all([
    waitForSelectorAndClick(page, '#Opportunity_Tab a'),
    waitForNavigation(page)
  ]), 'non sono riuscito ad aprire la pagina con la lista dei contratti');

  await tryOrThrow(() => Promise.all([
    waitForSelectorAndClick(page, '#hotlist > table > tbody > tr > td.pbButton > input'),
    waitForNavigation(page)
  ]), 'non sono riuscito ad aprire la pagina per l\'inserimento del contratto');
}
