import { Page } from 'puppeteer';
import IberdrolaPayload from '../IberdrolaPayload';
import { Logger } from '../../../logger';
import waitForSelectorAndType from '../../../browser/page/waitForSelectorAndType';
import { selezionaComuneFornitura } from '../utils/selezionaComune';
import waitForSelectorAndSelect from '../../../browser/page/waitForSelectorAndSelect';
import waitLoaderHidden from '../utils/waitLoaderHidden';
import tryOrThrow, { TryOrThrowError } from '../../../utils/tryOrThrow';
import waitForSelectorAndClickEvaluated from '../../../browser/page/waitForSelectorAndClickEvaluated';
import waitForNavigation from '../../../browser/page/waitForNavigation';
import checkVisualForceNavigationError from '../utils/checkVisualForceNavigationError';
import waitForSelectorAndTypeEvaluated from '../../../browser/page/waitForSelectorAndTypeEvaluated';
import inserisciIndirizzoResidenza from './indirizzoCliente/inserisciIndirizzoResidenza';
import checkStepErrors from '../utils/checkStepErrors';

const capSelector = '#newAddressPage\\:newAddressForm\\:newAddressData\\:addressDetailsIT\\:pepe\\:postalCodeIT';
const indirizzoSelector = '#newAddressPage\\:newAddressForm\\:newAddressData\\:addressDetailsIT\\:j_id114\\:wayNameText';
const tipoStradaSelector = '#newAddressPage\\:newAddressForm\\:newAddressData\\:addressDetailsIT\\:j_id120\\:wayType';
const civicoSelector = '#newAddressPage\\:newAddressForm\\:newAddressData\\:addressDetailsIT\\:j_id124\\:numberText';

export default async function stepIndirizzoCliente(
  page: Page,
  payload: IberdrolaPayload,
  logger: Logger
): Promise<void> {
  const {
    cap,
    comune,
    indirizzo,
    tipoStrada,
    civico,
    indirizzoResidenza
  } = payload;

  await tryOrThrow(async () => {
    // await waitForVisible(page, capSelector);
    await waitForSelectorAndType(page, capSelector, cap);
    await page.keyboard.press('Enter'); // Simula il click di `Enter`
    await waitLoaderHidden(page);
    // await delay(10000);
  }, 'non sono riuscito ad inserire il cap');

  // Seleziona la prima città dal menu a tendina
  // Rimuovo il carattere ' sostituendolo con lo spazio vuoto come richiesto su Iberdrola
  // eslint-disable-next-line newline-per-chained-call
  const comuneFixed = comune.replace(/'/g, ' ').replace(/-/g, ' ').replace(/ {2}/g, ' ').trim().toUpperCase();
  const comuneSelezionato = await tryOrThrow(
    () => selezionaComuneFornitura(page, comuneFixed),
    'non sono riuscito a selezionare il comune'
  );
  if (!comuneSelezionato) {
    throw new TryOrThrowError(`comune ${comuneFixed} non selezionato`);
  }

  await waitLoaderHidden(page);

  await tryOrThrow(async () => {
    await page.focus(indirizzoSelector);
    // Se l'indirizzo non esiste lo inserisco a mano
    await waitForSelectorAndTypeEvaluated(page, indirizzoSelector, indirizzo);
    await page.keyboard.press('Enter'); // Simula il click di `Enter`
  }, 'non sono riuscito ad inserire l\'indirizzo');

  await tryOrThrow(
    () => waitForSelectorAndSelect(page, tipoStradaSelector, [tipoStrada]),
    'non sono riuscito a selezionare il tipo di strada'
  );

  await tryOrThrow(
    () => waitForSelectorAndType(page, civicoSelector, civico),
    'non sono riuscito ad inserire il civico'
  );

  // controllo se indirizzo fornitura è DIVERSO da indirizzo residenza
  if (indirizzoResidenza) {
    await tryOrThrow(() => inserisciIndirizzoResidenza(page, payload), 'non sono riuscito ad inserire l\'indirizzo di residenza:');
  }

  // Click su `Salva`
  await Promise.all([
    waitForSelectorAndClickEvaluated(page, '#newAddressPage\\:newAddressForm\\:newAddressData\\:j_id36\\:bottom\\:saveClient'),
    Promise.race([
      waitForNavigation(page),
      page.waitForResponse(() => true)
    ])
  ]);

  await checkStepErrors(page, '#newAddressPage\\:pageMessages', logger);

  await checkVisualForceNavigationError(page);
}
