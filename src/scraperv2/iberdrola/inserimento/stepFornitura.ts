import { Page } from 'puppeteer';
import IberdrolaPayload from '../IberdrolaPayload';
import { Logger } from '../../../logger';
import waitForSelectorAndSelect from '../../../browser/page/waitForSelectorAndSelect';
import waitForSelectorAndType from '../../../browser/page/waitForSelectorAndType';
import waitForSelectorAndClick from '../../../browser/page/waitForSelectorAndClick';
import waitForNewPageLoaded from '../../../browser/page/waitForNewPageLoaded';
import FailureResponse from '../../../response/FailureResponse';
import checkVisualForceNavigationError from '../utils/checkVisualForceNavigationError';
import tryOrThrow from '../../../utils/tryOrThrow';
import checkStepErrors from '../utils/checkStepErrors';
import waitForVisible from '../../../browser/page/waitForVisible';

const SELETTORE_TIPOLOGIA_ENERGIA: Record<string, string> = {
  elettricita: '1',
  gas: '2',
  dual: '3',
};

const tipoEnergiaSelector = '#newCUPSPage\\:newCUPSForm\\:newCUPSData\\:CUPSDetailsTMK\\:j_id98\\:energyType';
const podPdrErrMsgSelector = '.pbBody';

export default async function stepFornitura(
  page: Page,
  { tipoEnergia, pod, pdr }: IberdrolaPayload,
  logger: Logger
): Promise<FailureResponse | null> {
  // attendi il selettore per selezionare il `tipo di energia`
  await tryOrThrow(() => waitForVisible(page, tipoEnergiaSelector), 'la pagina non si  è caricata in tempo');

  await tryOrThrow(
    () => waitForSelectorAndSelect(page, tipoEnergiaSelector, [SELETTORE_TIPOLOGIA_ENERGIA[tipoEnergia]]),
    'non sono riuscito a selezionare il tipo di energia'
  );

  if (['gas', 'dual'].includes(tipoEnergia)) {
    await tryOrThrow(
      () => waitForSelectorAndType(page, '#newCUPSPage\\:newCUPSForm\\:newCUPSData\\:CUPSDetailsTMK\\:j_id109\\:cupsGas', pdr),
      'non sono riuscito ad inserire il pdr'
    );
  }

  if (['elettricita', 'dual'].includes(tipoEnergia)) {
    await tryOrThrow(
      () => waitForSelectorAndType(page, '#newCUPSPage\\:newCUPSForm\\:newCUPSData\\:CUPSDetailsTMK\\:j_id104\\:cupsEleField', pod),
      'non sono riuscito ad inserire il pod'
    );
  }

  // TODO: trovare modo di farlo meglio, non si attende la navigazione in caso non trovi otherPage
  await tryOrThrow(
    () => waitForSelectorAndClick(page, '#newCUPSPage\\:newCUPSForm\\:newCUPSData\\:j_id39\\:bottom\\:saveCUPSAndContinue'),
    'non sono riuscito a cliccare sul pulsante `continua`'
  );

  // await waitLoaderHidden(p);

  const otherPage = await Promise.race([
    waitForNewPageLoaded(page),
    new Promise((resolve) => setTimeout(() => resolve(null), 10000))
  ]);
  if (otherPage) {
    const op = (otherPage as Page);
    await op.waitForSelector(podPdrErrMsgSelector);
    const podPdrErrMsg = await op.$eval(podPdrErrMsgSelector, (el) => el.textContent?.trim());

    await op.close();
    return new FailureResponse(podPdrErrMsg?.replace(/· /g, ''));
  }

  await checkStepErrors(page, '#newCUPSPage\\:pageMessages', logger);

  await checkVisualForceNavigationError(page);

  return null;
}
