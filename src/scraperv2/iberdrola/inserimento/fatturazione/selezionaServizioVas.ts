import { Page } from 'puppeteer';
import waitForSelectorAndSelect from '../../../../browser/page/waitForSelectorAndSelect';
import waitForSelectorAndClick from '../../../../browser/page/waitForSelectorAndClick';
import waitLoaderHidden from '../../utils/waitLoaderHidden';
import waitForTimeout from '../../../../browser/page/waitForTimeout';

export default async function selezionaServizioVas(
  page: Page,
  servizioVasSelectSelector: string,
  servizioVasBtnSelector: string,
  value: string
): Promise<void> {
  await waitForSelectorAndSelect(page, servizioVasSelectSelector, [value]);
  await page.focus(servizioVasBtnSelector);
  await waitForSelectorAndClick(page, servizioVasBtnSelector);
  await waitLoaderHidden(page);
  await waitForTimeout(page, 4000);
}
