import { Page } from 'puppeteer';
import IberdrolaPayload from '../../IberdrolaPayload';
import { Logger } from '../../../../logger';
import tryOrThrow, { TryOrThrowError } from '../../../../utils/tryOrThrow';
import waitForSelectorAndClick from '../../../../browser/page/waitForSelectorAndClick';
import waitLoaderHidden from '../../utils/waitLoaderHidden';
import checkStepErrors from '../../utils/checkStepErrors';
import checkVisualForceNavigationError from '../../utils/checkVisualForceNavigationError';
import waitForNavigation from '../../../../browser/page/waitForNavigation';
import createFilenameForScreenshot from '../../../../utils/createFilenameForScreenshot';
import saveScreenshotOnAwsS3 from '../../../../browser/page/saveScreenshotOnAwsS3';
import { S3ClientFactoryResult } from '../../../../aws/s3ClientFactory';

const codiceCupsSelector = '#NewOpptyPage\\:newOpptyForm\\:newOpptyData\\:j_id46';
const fatturazioneCheckboxSelector = '#NewOpptyPage\\:newOpptyForm\\:newOpptyData\\:facturacionPagoTMK\\:j_id95\\:facturacionElectronica';

export default async function stepInserimentoContratto(
  page: Page,
  {
    idDatiContratto,
    modalitaInvioFattura
  }: IberdrolaPayload,
  logger: Logger,
  s3ClientFactory: S3ClientFactoryResult
): Promise<string> {
  await page.waitForSelector(codiceCupsSelector);
  let codiceContratto = await tryOrThrow(
    () => page.$eval(codiceCupsSelector, ({ textContent }) => textContent?.trim()),
    'non sono riuscito a recuperare il codice cups'
  ) as string;
  if (codiceContratto) {
    codiceContratto = codiceContratto.replace(/\n/g, '');
  }

  if (!codiceContratto || !codiceContratto.match(/CUPS_\d{10}/g)) {
    throw new TryOrThrowError(
      `codice cups (${codiceContratto}) recuperato non valido per il task relativo al contratto ${idDatiContratto}`
    );
  }

  const filename = createFilenameForScreenshot('Iberdrola', idDatiContratto);
  await saveScreenshotOnAwsS3(s3ClientFactory, page, filename, logger);

  // Aggiungo la spunta su `fatturazione elettronica`
  if (modalitaInvioFattura === 'bollettaWeb' || modalitaInvioFattura === 'email') {
    await tryOrThrow(
      () => waitForSelectorAndClick(page, fatturazioneCheckboxSelector),
      'non sono riuscito a selezionare la fatturazione elettronica'
    );
  }

  // Finalizzo l'inserimento del contratto
  await Promise.all([
    waitForSelectorAndClick(page, '#NewOpptyPage\\:newOpptyForm\\:newOpptyData\\:j_id46\\:finalizeOppty'),
    Promise.race([
      waitForNavigation(page),
      page.waitForResponse(() => true)
    ])
  ]);

  try {
    await waitLoaderHidden(page);
    // await delay(3000);
  } catch (ex) {
    // loader già sparito
  }

  await checkStepErrors(page, '#NewOpptyPage\\:pageMessages', logger);

  await checkVisualForceNavigationError(page);

  return codiceContratto;
}
