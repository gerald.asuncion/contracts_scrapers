import { ScraperResponse, WebScraper } from '../scraper';
import ErrorResponse from '../../response/ErrorResponse';
import FailureResponse from '../../response/FailureResponse';
import SuccessResponse from '../../response/SuccessResponse';
import Iberdrola from './Iberdrola';
import IberdrolaPayload from './IberdrolaPayload';
import waitForSelectorAndType from '../../browser/page/waitForSelectorAndType';
import waitCheckCredentials from '../../browser/page/waitCheckCredentials';
import { SCRAPER_TIPOLOGIA } from '../ScraperOptions';

// TODO: fa schifo così lo so, trovare una soluzione per gestire il tipo dei payload per i vari scrapers in modo intelligente
type Payload = {
  tipoFornitura: string;
  prodotto: 'luce' | 'gas';
  cap: string;
  codiceFiscale: string;
  partitaIva: string;
  potenzaDisponibile: string;
} & IberdrolaPayload;

const PAGE_AVAILABLE_SELECTOR = '#Opportunity_Tab';

export default class IberdrolaCreditChecker extends Iberdrola {
  /**
   * @override
   */
  async login(): Promise<void> {
    const loginUrl = 'https://iberdrola.force.com/home/home.jsp';
    const { username, password } = await this.getCredenziali();

    this.checkPageClosed();

    const logger = this.childLogger;

    const page = await this.p();
    await page.goto(loginUrl, { waitUntil: 'networkidle0' });

    // attendo il caricamento del primo elemento utile della pagina successiva alla login
    const sel = await page.$(PAGE_AVAILABLE_SELECTOR);
    if (sel !== null) {
      logger.info('login > utente già loggato');
      return;
    }

    logger.info('login > inserisco lo username');
    await waitForSelectorAndType(page, '#username', username);

    logger.info('login > inserisco la password');
    await waitForSelectorAndType(page, '#password', password);

    await page.click('#Login');
    await Promise.race([
      waitCheckCredentials(page, '#error.loginError', loginUrl, username, logger),
      page.waitForSelector(PAGE_AVAILABLE_SELECTOR, { timeout: 60000 })
    ]);
    logger.info('login > fine');
  }

  /**
   * @override
   */
  scrapeWebsite({
    tipoFornitura,
    prodotto,
    cap,
    codiceFiscale,
    partitaIva,
    potenzaDisponibile
  }: Payload): Promise<ScraperResponse> {
    const logger = this.childLogger;

    const tipoCliente = tipoFornitura === 'residenziale' ? 'R' : 'P';
    // eslint-disable-next-line no-nested-ternary
    const tipoEnergia = prodotto === 'luce' ? 'ELE' : prodotto === 'gas' ? 'GAS' : 'DUAL';

    return this.p().then(
      async (p) => {
        try {
          logger.info('// attendo e clicco su elemento `Credit Check`');
          // attendo e clicco su elemento `Credit Check`
          await p.waitForSelector('#\\30 1r58000000dCJO_Tab');
          await p.click('#\\30 1r58000000dCJO_Tab');

          logger.info('// attendo e clicco su elemento `Crea nuovo/a credit check`');
          // attendo e clicco su elemento `Crea nuovo/a credit check`
          await p.waitForSelector('#hotlist > table > tbody > tr > td.pbButton > input');
          await p.click('#hotlist > table > tbody > tr > td.pbButton > input');

          logger.info('// attendo elemento `Tipo Cliente`');
          // attendo elemento `Tipo Cliente`
          await p.waitForSelector('#newCUPSPage\\:creditCheckFormId\\:creditCheckPageBlockId\\:creditCheckPageBlockSectionId\\:j_id35\\:j_id39');
          await p.select('#newCUPSPage\\:creditCheckFormId\\:creditCheckPageBlockId\\:creditCheckPageBlockSectionId\\:j_id35\\:j_id39', tipoCliente);

          logger.info('// seleziono il tipo di energia');
          // seleziono il tipo di energia
          await p.select('#newCUPSPage\\:creditCheckFormId\\:creditCheckPageBlockId\\:creditCheckPageBlockSectionId\\:j_id41\\:j_id45', tipoEnergia);

          logger.info('// inserisco il CAP');
          // inserisco il CAP
          await p.type('#newCUPSPage\\:creditCheckFormId\\:creditCheckPageBlockId\\:creditCheckPageBlockSectionId\\:j_id47\\:j_id51', cap);
          await WebScraper.delay(500);

          logger.info('// inserisco il codice fiscale');
          // inserisco il codice fiscale
          await p.type('.codiceFiscaleClass', codiceFiscale);
          await WebScraper.delay(500);

          if (tipoFornitura === 'business') {
            await p.type('.cifnifClass', partitaIva);
            await p.type('.potenciaClass', potenzaDisponibile);
          }

          logger.info('// dismiss modale JS');
          // Trick per dismiss JS dialog
          p.on('dialog', async (dialog) => {
            await dialog.accept();
          });

          logger.info('// Clicco su `Eseguire credit check`');
          // Clicco su `Eseguire credit check`
          await p.click('#newCUPSPage\\:creditCheckFormId\\:creditCheckPageBlockId\\:j_id33\\:creditCheckButtonId');

          // controllo se ci sono errori

          const errors = await p.evaluate(() => {
            const elements = document.querySelectorAll('[id^="newCUPSPage"][id*="showMessages"].messageText');
            return Array.from(elements).map((el) => el.textContent).map((str) => str?.replace('Attenzione:', '').trim());
          });

          if (errors && errors.length) {
            const errMsg = errors.join('; ');
            logger.error(errMsg);
            return new ErrorResponse(errMsg);
          }

          // eslint-disable-next-line arrow-body-style
          await p.waitForFunction(() => {
            // eslint-disable-next-line @typescript-eslint/ban-ts-comment
            // @ts-ignore
            return document.querySelector('#newCUPSPage\\:creditCheckFormId\\:creditCheckPageBlockId\\:creditCheckResultId\\:j_id81\\:j_id83').innerHTML !== '&nbsp;';
          }, { polling: 100, timeout: 60000 });

          // eslint-disable-next-line arrow-body-style
          const risultato = await p.evaluate(() => {
            const el = document.querySelector('#newCUPSPage\\:creditCheckFormId\\:creditCheckPageBlockId\\:creditCheckResultId\\:j_id81\\:j_id83');
            return el ? el.innerHTML : '';
          });

          logger.info('// Risultato -> ' + risultato);
          return risultato === 'OK' ? new SuccessResponse('OK') : new FailureResponse('Credit check KO');
        } catch (e: any) {
          return new ErrorResponse(e);
        }
      }
    );
  }

  /**
   * @override
   */
  getScraperCodice() {
    return 'iberdrola-checkcredit';
  }

  /**
   * @override
   */
  getScraperTipologia(): any {
    return SCRAPER_TIPOLOGIA.FATTIBILITA;
  }
}
