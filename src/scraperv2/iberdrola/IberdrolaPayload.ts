import ContrattoPayload from '../../contratti/ContrattoPayload';

export interface TipologiaEnergia {
  elettricita: string;
  gas: string;
  dual: string;
}

export interface TipologiaCliente {
  residenziale: string;
  business: string;
}

export interface TipologiaDocumentoResidenziale {
  cartaIdentita: string;
  passaporto: string;
  patente: string;
}

export interface TipologiaDocumento {
  residenziale: TipologiaDocumentoResidenziale;
  business: string;
}

type IberdrolaPayload = Required<ContrattoPayload> & {
  tipoIntestatario: keyof TipologiaCliente;
  tipoEnergia: keyof TipologiaEnergia;
  tipoDocumento: keyof TipologiaDocumentoResidenziale;
  primoConsenso: string;
  secondoConsenso: string;
  terzoConsenso: string;
  pdr: string;
  pod: string;
  numeroDocumento: string;
  codiceFiscaleRappresentante: string;
  nomeRappresentante: string;
  cognomeRappresentante: string;
  dataDiNascitaRappresentante: string;
  telefonoRappresentante: string;
  emailRappresentante: string;
  ragioneSocialeRappresentante: string;
  PECRappresentante: string;
  cap: string;
  comune: string;
  indirizzo: string;
  tipoStrada: string;
  civico: string;
  indirizzoResidenza: string;
  capResidenza: string;
  comuneResidenza: string;
  tipoStradaResidenza: string;
  civicoResidenza: string;
  attualeFornitoreGas: string;
  distributoreGas: string;
  utilizzoGas: string;
  consumoGas: string;
  attualeFornitoreElettricita: string;
  distributoreElettricita: string;
  potenzaImpegnata: string | number;
  consumoLuce: string;
  tensione: string;
  titolaritaImmobile: string;
  modalitaPagamento: string;
  iban: string;
  codiceABarre: string;
  dataCompletato: string;
  prodottoEnergia: string;
  prodottoGas: string;
  modalitaInvioFattura: string;
};

export default IberdrolaPayload;
