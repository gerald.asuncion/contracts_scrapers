import { ScraperResponse } from '../scraper';
import FailureResponse from '../../response/FailureResponse';
import Iberdrola from './Iberdrola';
import IberdrolaExtractorData from './IberdrolaExtractorData';
import SuccessResponseAny from '../../response/SuccessResponseAny';
import ScraperOptions from '../ScraperOptions';

export default class IberdrolaExtractor<T extends IberdrolaExtractorData> extends Iberdrola {
  protected url: string;

  protected tableNameSelector: string;

  protected tipo: 'fornitori' | 'distributori' | undefined;

  protected sottoTipo: 'luce' | 'gas' | undefined;

  constructor(options: ScraperOptions) {
    super(options);
    this.url = '';
    this.tableNameSelector = '';
  }

  /**
   * @override
   */
  async scrapeWebsite(): Promise<ScraperResponse> {
    this.logger.info(`ricerca ${this.tipo} ${this.sottoTipo}`);
    const list: Array<T> | undefined = await this.scrapeTable(this.url);

    if (!list?.length) {
      this.logger.error(`${this.tipo} ${this.sottoTipo} non trovati`);
      return new FailureResponse(`${this.tipo} ${this.sottoTipo} non trovati`);
    }

    return new SuccessResponseAny({ list: list || [] });
  }

  async scrapeTable(url: string): Promise<Array<T> | undefined> {
    const page = await (await this.getBrowserContext()).newPage();

    this.logger.info(`apro pagina ${url}`);
    await page.goto(url, { waitUntil: 'networkidle2' });

    this.logger.info('recupero gli elementi da cui estrarre i dati');
    const tbodySelector = `#ComercilizadoraSearchVLookUp\\:${this.tableNameSelector}\\:j_id2\\:j_id3\\:searchResults\\:tb`;
    const tBody = await page.waitForSelector(tbodySelector);

    this.logger.info('estraggo i dati');
    const list: Array<T> | undefined = await tBody?.evaluate((el): Array<T> => Array.from(el.querySelectorAll('tr.dataRow'))
      .map((tr) => {
        const [tdCode, tdName] = tr.querySelectorAll('.dataCell');
        if (!tdCode || !tdName) {
          return null;
        }
        const onclick = tdCode.querySelector('a')?.getAttribute('onclick')?.trim() || '';
        const codice = tdCode.textContent?.trim() || '';
        const nome = tdName.textContent?.trim() || '';
        // nome = nome.replace(/""/g, '"').replace(/""/g, '"');

        let id = '';

        if (onclick) {
          id = (onclick.match(/\('(.*)',/) || [])[1] || '';
        }
        return { id, codice, nome };
      }).filter((item): item is T => !!item));

    page.close();

    this.logger.info(`trovati ${list?.length} elementi`);
    return Promise.resolve(list);
  }
}
