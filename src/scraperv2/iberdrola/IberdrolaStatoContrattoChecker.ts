/* eslint-disable no-await-in-loop */
import { ScraperResponse, WebScraper } from '../scraper';
import ErrorResponse from '../../response/ErrorResponse';
import Iberdrola from './Iberdrola';
import IberdrolaPayload from './IberdrolaPayload';
import createChildPayloadAwareLogger from '../../utils/createChildPayloadAwareLogger';
import { STATO_CONTRATTO } from '../../contratti/StatoContratto';
import waitForSelectorAndType from '../../browser/page/waitForSelectorAndType';
import extractErrorMessage from '../../utils/extractErrorMessage';
import GenericSuccessResponse from '../../response/GenericSuccessResponse';
import waitForVisible from '../../browser/page/waitForVisible';
import { SCRAPER_TIPOLOGIA } from '../ScraperOptions';
import { IberdrolaLoginPayload } from './types';

type Payload = IberdrolaPayload & {
  codiceCupsArray: string;
};

const STATI_FIRMATO: string[] = [
  'Offerta Avanzata',
  'Offerta Sottoscritta',
  'Approvazione Attiva',
  'Offerta Contrattata',
  'KO Non Recuperabile Delta'
];

export default class IberdrolaStatoContrattoChecker extends Iberdrola {
  /**
   * TOLGO il call center perché non serve più
   * TODO: buttare via questa funzione appena contracts smette di mandare il call center per questo scraper
   * @override
   * @param payload payload ricevuto
   */
   async login({ callCenter, ...payload }: IberdrolaLoginPayload): Promise<void> {
    return super.login(payload as any);
  }

  /**
   * @override
   * @param payload payload ricevuto
   */
  scrapeWebsite(args: Payload): Promise<ScraperResponse> {
    const { codiceCupsArray } = args;
    const logger = createChildPayloadAwareLogger(this.childLogger, args);

    return this.p().then(
      async (p) => {
        const statoContrattoResponse: Record<string, string> = {};

        try {
          for (const codiceCups of codiceCupsArray) {
            logger.info(`${codiceCups} inizio controllo`);
            await waitForSelectorAndType(p, '#phSearchInput', codiceCups);

            // Click sul tasto "cerca"
            // eslint-disable-next-line @typescript-eslint/no-loop-func
            await p.evaluate(() => {
              // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
              document.querySelector('#phSearchButton')!.parentElement!.click();
            });

            await WebScraper.delay(2000);

            // await this.saveScreenshot(p, logger, `${codiceCups}-primaCheckerStato`);

            let statoContratto;
            try {
              logger.info(`${codiceCups} controllo se è apparso il warning di corrispondenza non trovata`);
              await waitForVisible(p, '.searchResultsMessageContainer', { timeout: 4000 });
              logger.warn(`${codiceCups} nessuna corrispondenza trovata dal portale`);
            } catch (ex) {
              // warning non apparso posso procedere, non sempre è vero

              try {
                logger.info(`${codiceCups} controllo lo stato`);
                const selettorePrimaRigaRisultati = '#Opportunity_body > table > tbody > tr.dataRow.even.last.first > td:nth-child(2)';
                await p.waitForSelector(selettorePrimaRigaRisultati, { timeout: 2000 });
                // eslint-disable-next-line @typescript-eslint/no-loop-func
                statoContratto = await p.evaluate((selector) => document.querySelector(selector).innerHTML, selettorePrimaRigaRisultati);

                logger.info(`${codiceCups} ha lo stato \`${statoContratto}\` nel portale iberdrola`);
              } catch (exControllo) {
                // potrebbe capitare che il warning sulla corrispondenza mostrato dal portale
                // non sia stato catturato comunque e quindi andrebbe in eccezione il recuperto dello stato ugualmente
                // eslint-disable-next-line max-len
                logger.error(`${codiceCups} non sono riuscio a recuperare lo stato nel portale iberdrola \`${extractErrorMessage(exControllo)}\``);
              }

              // await this.saveScreenshot(p, logger, `${codiceCups}-dopoCheckerStato`);
            }

            if (statoContratto) {
              if (statoContratto === 'In attesa di informazioni') {
                statoContrattoResponse[codiceCups] = STATO_CONTRATTO.PRONTO;
              } else if (statoContratto === 'In attesa sms edatalia' || statoContratto === 'In attesa accettazione cliente') {
                statoContrattoResponse[codiceCups] = STATO_CONTRATTO.DA_FIRMARE;
              } else if (
                STATI_FIRMATO.includes(statoContratto)
              ) {
                statoContrattoResponse[codiceCups] = STATO_CONTRATTO.FIRMATO;
              } else {
                statoContrattoResponse[codiceCups] = STATO_CONTRATTO.DA_FIRMARE;
              }
            } else {
              statoContrattoResponse[codiceCups] = STATO_CONTRATTO.DA_FIRMARE;
            }

            logger.info(`${codiceCups} impostato stato ${statoContrattoResponse[codiceCups]}`);
          }

          return new GenericSuccessResponse(statoContrattoResponse);
        } catch (e: any) {
          const errMsg = typeof e === 'string' ? e : e.message;
          logger.error(`qualcosa è andato storto durante il controllo dello stato dei codici cups ${errMsg}`);
          return new ErrorResponse(errMsg);
        }
      }
    );
  }

  /**
   * @override
   */
  getScraperCodice() {
    return 'iberdrola-checkfirma';
  }

  /**
   * @override
   */
  getScraperTipologia(): any {
    return SCRAPER_TIPOLOGIA.CHECKFIRMA;
  }
}
