import IberdrolaExtractor from './IberdrolaExtractor';
import { IberdrolaDistributore } from './IberdrolaExtractorData';

export default class IberdrolaDistributoriGas extends IberdrolaExtractor<IberdrolaDistributore> {
  constructor(options: any) {
    super(options);
    this.url = 'https://iberdrola.force.com/apex/IBD_VFP015_PropAdminNewDistribSearch?distribuidora=&tipo_energia=2&territorio=Italy';
    this.tableNameSelector = 'DistribuidoralookupForm';
    this.tipo = 'distributori';
    this.sottoTipo = 'gas';
  }
}
