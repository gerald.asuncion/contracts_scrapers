import { Page } from 'puppeteer';
import waitForHiddenStyled from '../../../browser/page/waitForHiddenStyled';

export default async function waitLoaderHidden(
  page: Page,
  waitOptions?: {
    timeout?: number;
    polling?: string | number;
  }
): Promise<void> {
  // await page.waitForResponse(() => true);
  await waitForHiddenStyled(page, '[id*=":statusPRUEBA.start"]', waitOptions);
  // await page.waitForResponse(() => true);
}
