import { Credenziali } from '../types';

const CREDENZIALI_DEF: Credenziali = { username: 'mdannm77t11b364y.y000020@externo.com', password: 'Supermoney,2022' };

const CREDENZIALI_MAP: Record<string, Credenziali> = {
  milano: { username: 'sjagpp82d55c421b.y000019@externo.com', password: 'Supermoney,2022' },
  serramanna: CREDENZIALI_DEF,
  ivrea: { username: 'ccclcu67r52g388y.y000021@externo.com', password: 'Supermoney,2022' },
  maglie: { username: 'slddrn76l48d883l.y000018@externo.com', password: 'Supermoney,2022' },
  agrigento: { username: 'slddrn76l48d883l.y000018@externo.com', password: 'Supermoney,2022' },
  molfetta: { username: 'trttms72d14a883o.y000043@externo.com', password: 'Supermoney,2022' },
  checker: { username: 'ccedvd84d21a662m.y000013@externo.com', password: 'Supermoney,2022' }
};

export default function getCredenzialiDaCallCenter(callCenter: string): Credenziali {
  return CREDENZIALI_MAP[callCenter] || CREDENZIALI_DEF;
}
