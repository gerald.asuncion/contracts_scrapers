import { Page } from 'puppeteer';
import { Logger } from '../../../logger';
import extractErrors from './extractErrors';
import { TryOrThrowError } from '../../../utils/tryOrThrow';

export default async function checkStepErrors(page: Page, selector: string, logger: Logger): Promise<void> {
  const errors = await extractErrors(page, selector, logger);
  if (errors.length) {
    throw new TryOrThrowError(errors.filter((err) => !!err).join('\n'));
  }
}
