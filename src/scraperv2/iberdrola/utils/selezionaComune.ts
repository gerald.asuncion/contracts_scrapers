import { Page } from 'puppeteer';
import waitForSelectorAndType from '../../../browser/page/waitForSelectorAndType';
import waitForSelectorAndClick from '../../../browser/page/waitForSelectorAndClick';
import waitLoaderHidden from './waitLoaderHidden';
import waitForTimeout from '../../../browser/page/waitForTimeout';
import { TryOrThrowError } from '../../../utils/tryOrThrow';

function recuperaComuneSelezionato(page: Page, selector: string): Promise<string> {
  return page.evaluate((_selector) => {
    const el = document.querySelector<HTMLElement>(_selector);

    return el?.innerText.toLowerCase().trim() as string;
  }, selector);
}

type SelezionaComuneOptions = {
  openSuggestionSelector: string;
  suggestionSelector: string;
  inputSearchSelector: string;
  firstOptionSelector: string;
  resultSelector: string;
};

export default async function selezionaComune(
  page: Page,
  comune: string,
  {
    openSuggestionSelector,
    suggestionSelector,
    inputSearchSelector,
    firstOptionSelector,
    resultSelector
  }: SelezionaComuneOptions
): Promise<string> {
  let comuneSelezionato = await recuperaComuneSelezionato(page, resultSelector);

  if (!comuneSelezionato || comuneSelezionato.toLowerCase() === 'seleziona una città') {
    await waitForSelectorAndClick(page, openSuggestionSelector);
    await page.waitForSelector(suggestionSelector, { visible: true });

    await waitForSelectorAndType(page, inputSearchSelector, comune);
    await waitForSelectorAndClick(page, firstOptionSelector);

    await waitForTimeout(page, 3000);
    await waitLoaderHidden(page);

    comuneSelezionato = await recuperaComuneSelezionato(page, resultSelector);

    if (!comuneSelezionato?.includes(comune.toLowerCase())) {
      throw new TryOrThrowError(`comune ${comune} non trovato`);
    }

    // const comuneNonTrovato = page.evaluate(() => {
    //     const notFound = document.querySelector('#select2-results-7 > li.select2-no-results');
    //     return !!notFound;
    // });

    // if(comuneNonTrovato) {
    //     throw new TryOrThrowError(`comune ${comune} non trovato`);
    // }

    return comuneSelezionato;
  }

  return comuneSelezionato;
}

export const selezionaComuneFornitura = (page: Page, comune: string): Promise<string> => selezionaComune(page, comune, {
  openSuggestionSelector: '#s2id_newAddressPage\\:newAddressForm\\:newAddressData\\:addressDetailsIT\\:j_id106\\:comune',
  suggestionSelector: 'body > div.select2-drop.select2-display-none.select2-with-searchbox.select2-drop-active',
  inputSearchSelector: '#s2id_autogen7_search',
  firstOptionSelector: '#select2-results-7 > li:first-child',
  resultSelector: '#select2-chosen-7'
});

export const selezionaComuneResidenza = (page: Page, comune: string): Promise<string> => selezionaComune(page, comune, {
  openSuggestionSelector: '#s2id_newAddressPage\\:newAddressForm\\:newAddressData\\:addressDetailsITmail\\:j_id144\\:comune',
  suggestionSelector: '#select2-drop',
  inputSearchSelector: '[id^="s2id_autogen"][id$="_search"]',
  firstOptionSelector: '[id^="select2-results"] > li:first-child',
  resultSelector: '#s2id_newAddressPage\\:newAddressForm\\:newAddressData\\:addressDetailsITmail\\:j_id144\\:comune [id^="select2-chosen"]'
});
