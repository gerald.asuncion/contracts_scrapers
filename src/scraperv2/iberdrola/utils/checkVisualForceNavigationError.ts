import { Page } from 'puppeteer';
import { TryOrThrowError } from '../../../utils/tryOrThrow';

export const SALESFORCE_NAVIGATION_ERROR = 'Si è verificato un errore durante il caricamento di una pagina Visualforce';

export default async function checkVisualForceNavigationError(page: Page): Promise<void> {
  const content = await page.content() || '';
  if (content.includes(SALESFORCE_NAVIGATION_ERROR)) {
    throw new TryOrThrowError(SALESFORCE_NAVIGATION_ERROR);
  }
}
