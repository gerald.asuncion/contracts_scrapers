import { Page } from 'puppeteer';
import { Logger } from '../../../logger';
import extractErrorMessage from '../../../utils/extractErrorMessage';

export default async function extractErrors(page: Page, selector: string, logger: Logger): Promise<Array<string>> {
  let errors: Array<string> = [];
  try {
    const parent = await page.waitForSelector(selector, { visible: true });
    errors = await parent?.evaluate((el) => {
      const elements = Array.from(el.querySelectorAll<HTMLElement>('td > div'))
        .concat(Array.from(el.querySelectorAll<HTMLElement>('ul > li')));
      return elements.map((child) => child.innerText.replace('Errore:', '').replace('Errori', '').replace(/\n/g, '').trim());
    }) || [];
  } catch (ex) {
    const errMsg = extractErrorMessage(ex);
    if (!errMsg.includes('timeout') && !errMsg.includes('Protocol error')) {
      logger.error(errMsg);
    }
  }

  return errors;
}
