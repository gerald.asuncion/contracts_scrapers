import IberdrolaExtractor from './IberdrolaExtractor';
import { IberdrolaDistributore } from './IberdrolaExtractorData';

export default class IberdrolaDistributoriLuce extends IberdrolaExtractor<IberdrolaDistributore> {
  constructor(options: any) {
    super(options);
    this.url = 'https://iberdrola.force.com/apex/IBD_VFP015_PropAdminNewDistribSearch?distribuidora=&tipo_energia=1&territorio=Italy';
    this.tableNameSelector = 'DistribuidoralookupForm';
    this.tipo = 'distributori';
    this.sottoTipo = 'luce';
  }
}
