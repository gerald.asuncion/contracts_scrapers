import IberdrolaExtractor from './IberdrolaExtractor';
import { IberdrolaFornitore } from './IberdrolaExtractorData';

export default class IberdrolaFornitoriLuce extends IberdrolaExtractor<IberdrolaFornitore> {
  constructor(options: any) {
    super(options);
    this.url = 'https://iberdrola.force.com/apex/IBD_VFP014_PropAdminNewComerSearch?comercializadora=&tipo_energia=1&territorio=Italy';
    this.tableNameSelector = 'ComercializadoralookupForm';
    this.tipo = 'fornitori';
    this.sottoTipo = 'luce';
  }
}
