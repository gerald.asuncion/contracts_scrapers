import IberdrolaExtractor from './IberdrolaExtractor';
import { IberdrolaFornitore } from './IberdrolaExtractorData';

export default class IberdrolaFornitoriGas extends IberdrolaExtractor<IberdrolaFornitore> {
  constructor(options: any) {
    super(options);
    this.url = 'https://iberdrola.force.com/apex/IBD_VFP014_PropAdminNewComerSearch?comercializadora=&tipo_energia=2&territorio=Italy';
    this.tableNameSelector = 'ComercializadoralookupForm';
    this.tipo = 'fornitori';
    this.sottoTipo = 'gas';
  }
}
