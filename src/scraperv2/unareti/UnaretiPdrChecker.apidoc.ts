/**
 * @openapi
 *
 * /scraper/v2/unareti/pdr/check:
 *  post:
 *    tags:
 *      - v2
 *    description: Va sul portale di Unareti e controlla se il pdr è valido.
 *    consume:
 *      - application/json
 *    produces:
 *      - application/json
 *    responses:
 *      200:
 *        description: SuccessResponse
 *    parameters:
 *      - in: body
 *        description:
 *          pdr da controllare.
 *        schema:
 *          type: object
 *          required:
 *            - pdr
 *            - tipoContratto
 *          properties:
 *            pdr:
 *              type: string
 *            tipoContratto:
 *              type: string
 *
 * /scraper/queue/unaretiPdrChecker:
 *  post:
 *    tags:
 *      - v2
 *      - queue
 *    description:
 *      Va sul portale di Unareti e controlla se il pdr è valido.
 *      Il payload è lo stesso della rotta diretta `/scraper/v2/unareti/pdr/check`,
 *      in più va aggiunto il webhook che lo scraper poi andrà a richiamare per notificare l'esito.
 *    consume:
 *      - application/json
 *    produces:
 *      - application/json
 *    responses:
 *      200:
 *        description: SuccessResponse
 */
