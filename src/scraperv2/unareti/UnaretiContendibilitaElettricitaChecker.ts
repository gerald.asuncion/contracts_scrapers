import { UnaretiDownloaderOptions } from './types';
import UnaretiDownload from './UnaretiDownload';

export default class UnaretiContendibilitaElettricitaChecker extends UnaretiDownload {
  constructor(options: UnaretiDownloaderOptions) {
    super(options);
    this.loginUrl = 'https://netgateele.unareti.it/Netgate/login?theDistributor=A2A';
    this.dbTableName = 'unareti_contendibilita_luce';
    this.downloadedFileName = 'unareti-luce-update.zip';
  }

  getScraperCodice() {
    return 'unareti-downloadluce';
  }
}
