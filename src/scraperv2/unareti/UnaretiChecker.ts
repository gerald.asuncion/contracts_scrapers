import { WebScraper, ScraperResponse } from '../scraper';
import { MysqlPool } from '../../mysql';
import UnaretiRepo from '../../repo/UnaretiRepo';
import { UnaretiCheckerOptions } from './types';
import { SCRAPER_TIPOLOGIA } from '../ScraperOptions';

export default class UnaretiChecker<T> extends WebScraper<T> {
  protected mysqlPool: MysqlPool;

  protected unaretiRepo: UnaretiRepo;

  constructor(options: UnaretiCheckerOptions) {
    super(options);
    this.mysqlPool = options.mysqlPool;
    this.unaretiRepo = options.unaretiRepo;
  }

  login(): Promise<void> {
    return Promise.resolve();
  }

  scrapeWebsite(args: T): Promise<ScraperResponse> {
    throw new Error('Method not implemented.');
  }

  getScraperCodice() {
    return 'unareti-check';
  }

  getScraperTipologia() {
    return SCRAPER_TIPOLOGIA.FATTIBILITA;
  }
}
