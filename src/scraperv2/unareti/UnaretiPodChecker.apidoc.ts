/**
 * @openapi
 *
 * /scraper/v2/unareti/pod/check:
 *  post:
 *    tags:
 *      - v2
 *    description: Va sul portale di Unareti e controlla se il pod è valido.
 *    consume:
 *      - application/json
 *    produces:
 *      - application/json
 *    responses:
 *      200:
 *        description: SuccessResponse
 *    parameters:
 *      - in: body
 *        description:
 *          pod da controllare.
 *        schema:
 *          type: object
 *          required:
 *            - pod
 *            - tipoContratto
 *          properties:
 *            pod:
 *              type: string
 *            tipoContratto:
 *              type: string
 *
 * /scraper/queue/unaretiPodChecker:
 *  post:
 *    tags:
 *      - v2
 *      - queue
 *    description:
 *      Va sul portale di Unareti e controlla se il pod è valido.
 *      Il payload è lo stesso della rotta diretta `/scraper/v2/unareti/pod/check`,
 *      in più va aggiunto il webhook che lo scraper poi andrà a richiamare per notificare l'esito.
 *    consume:
 *      - application/json
 *    produces:
 *      - application/json
 *    responses:
 *      200:
 *        description: SuccessResponse
 */
