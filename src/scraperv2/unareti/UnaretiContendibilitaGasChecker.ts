import { UnaretiDownloaderOptions } from './types';
import UnaretiDownload from './UnaretiDownload';

export default class UnaretiContendibilitaGasChecker extends UnaretiDownload {
  constructor(options: UnaretiDownloaderOptions) {
    super(options);
    this.loginUrl = 'https://netgategas.unareti.it/Netgate/login?theDistributor=A2A';
    this.dbTableName = 'unareti_contendibilita_gas';
    this.downloadedFileName = 'unareti-gas-update.zip';
  }

  /**
   * @override
   */
  getScraperCodice() {
    return 'unareti-downloadgas';
  }
}
