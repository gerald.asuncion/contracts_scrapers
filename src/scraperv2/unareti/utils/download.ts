/* eslint-disable no-await-in-loop */
import path from 'path';
import { Page } from 'puppeteer';
import fs from 'fs';
import util from 'util';
import extractErrorMessage from '../../../utils/extractErrorMessage';
import { Logger } from '../../../logger';
import waitForVisible from '../../../browser/page/waitForVisible';

export default async function download(page: Page, downloadDir: string, f: () => Promise<void>, logger: Logger): Promise<string> {
  const downloadPath = downloadDir;
  await util.promisify(fs.mkdir)(downloadPath, { recursive: true });
  logger.info(`Cartella del download ${downloadPath}`);

  // controllo se è apparso il pannello di errori
  try {
    // await page.waitForSelector('#message', { visible: true, timeout: 3000 });
    await waitForVisible(page, '#message', { timeout: 6000 });
    const msg = await page.$eval('#message', (el) => el.textContent?.trim());
    throw new Error(`Impossibile scaricare il file: ${msg}`);
  } catch (ex) {
    const errMsg = extractErrorMessage(ex);
    if (errMsg.includes('Impossibile scaricare il file')) {
      throw ex;
    }
    // nulla da fare
  }

  const client = await page.target().createCDPSession();

  await client.send('Page.setDownloadBehavior', {
    behavior: 'allow',
    downloadPath,
  });

  await f();

  logger.info('Scaricamento...');
  let fileName;
  while (!fileName || fileName.endsWith('.crdownload')) {
    await new Promise((resolve) => setTimeout(resolve, 100));
    [fileName] = await util.promisify(fs.readdir)(downloadPath);
  }

  const filePath = path.resolve(downloadPath, fileName);
  logger.info(`Scaricato il file ${filePath}`);

  return filePath;
}
