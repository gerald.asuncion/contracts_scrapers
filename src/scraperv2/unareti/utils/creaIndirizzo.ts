import { UnaretiContendibilitaLuceRecord, UnaretiContendibilitaGasRecord } from '../../../repo/types';

export function creaIndirizzoLuce({
  toponimo,
  nome_strada,
  civico,
  estensione_civico,
  cap,
  comune,
  provincia,
  interno,
  scala,
  potenza_contrattuale,
  valore_di_tensione
}: UnaretiContendibilitaLuceRecord): string {
  return [
    toponimo,
    nome_strada,
    `${civico}${estensione_civico ? `${estensione_civico}` : ''}`,
    cap,
    comune,
    provincia,
    interno ? `interno ${interno}` : null,
    scala ? `scala ${scala}` : null,
    potenza_contrattuale ? `potenza contrattuale ${potenza_contrattuale}` : null,
    valore_di_tensione ? `valore di tensione ${valore_di_tensione} (V)` : null
  ].filter(Boolean).join(' ');
}

export function creaIndirizzoGas({
  toponimo,
  nome_strada,
  civico,
  estensione_civico,
  cap,
  comune,
  provincia,
  interno,
  scala
}: UnaretiContendibilitaGasRecord): string {
  return [
    toponimo,
    nome_strada,
    `${civico}${estensione_civico ? `${estensione_civico}` : ''}`,
    cap,
    comune,
    provincia,
    interno ? `interno ${interno}` : null,
    scala ? `scala ${scala}` : null
  ].filter(Boolean).join(' ');
}
