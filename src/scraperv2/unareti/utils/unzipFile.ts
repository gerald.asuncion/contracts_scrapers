import AdmZip from 'adm-zip';

export default function unzipFile(filePath: string, folder: string) {
  const zip = new AdmZip(filePath);
  const entries = zip.getEntries();
  zip.extractAllTo(folder, true);
  return entries;
}
