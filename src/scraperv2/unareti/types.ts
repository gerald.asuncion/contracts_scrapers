import { ScraperWithDatabaseOptions } from '../ScraperOptions';
import UnaretiRepo from '../../repo/UnaretiRepo';

export type UnaretiCheckerOptions = ScraperWithDatabaseOptions & {
  unaretiRepo: UnaretiRepo;
};

export type UnaretiDownloaderOptions = UnaretiCheckerOptions & {
  baseDownloadPath: string;
};
