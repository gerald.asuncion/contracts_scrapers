import { ScraperResponse } from '../scraper';
import ErrorResponse from '../../response/ErrorResponse';
import FailureResponse from '../../response/FailureResponse';
import SuccessResponse from '../../response/SuccessResponse';
import UnaretiChecker from './UnaretiChecker';
import { creaIndirizzoLuce } from './utils/creaIndirizzo';
import extractErrorMessage from '../../utils/extractErrorMessage';
import CasoNonGestitoResponse from '../../response/CasoNonGestitoResponse';
import { QueueTask } from '../../task/QueueTask';

type UnaretiPodCheckerPayload = {
  pod: string;
  tipoContratto: string;
};

@QueueTask()
export default class UnaretiPodChecker extends UnaretiChecker<UnaretiPodCheckerPayload> {
  /**
   * @override
   * @param payload Request payload
   */
  async scrapeWebsite({ pod, tipoContratto }: UnaretiPodCheckerPayload): Promise<ScraperResponse> {
    const logger = this.childLogger;

    try {
      logger.info(`start check pod ${pod}`);
      const record = await this.unaretiRepo.podCheck(pod);

      if (!record) {
        logger.info(`KO - pod ${pod} non contendibile`);
        return new FailureResponse('KO - POD non contendibile');
      }

      const statoDelPunto = record ? record.stato_del_punto.toLowerCase() : '';

      if (tipoContratto === 'attivazione') {
        if (!['disattivato', 'mai attivato'].includes(statoDelPunto)) {
          logger.info(`KO - pod ${pod} non contendibile`);
          return new FailureResponse('KO - POD non contendibile');
        }

        if (statoDelPunto === 'disattivato') {
          logger.info(`KO - Necessario procedere con un Subentro per il pod ${pod}`);
          return new FailureResponse('KO - Necessario procedere con un Subentro');
        }

        if (statoDelPunto === 'mai attivato') {
          const indirizzo = creaIndirizzoLuce(record);
          logger.info(`risultato OK ritorno indirizzo '${indirizzo}'`);
          return new SuccessResponse(indirizzo);
        }
      }

      if (tipoContratto === 'subentro') {
        if (statoDelPunto === 'mai attivato') {
          logger.info(`KO - Necessario procedere con un prima attivazione per il pod ${pod}`);
          return new FailureResponse('KO - Procedere con prima attivazione');
        }

        if (statoDelPunto === 'disattivato') {
          const indirizzo = creaIndirizzoLuce(record);
          logger.info(`risultato OK ritorno indirizzo '${indirizzo}'`);
          return new SuccessResponse(indirizzo);
        }
      }

      logger.error('caso non gestito');
      return new CasoNonGestitoResponse();
    } catch (e: any) {
      const errMsg = extractErrorMessage(e);
      logger.error(errMsg);
      return new ErrorResponse(e);
    }
  }

  getScraperCodice() {
    return 'unareti-checkpod';
  }
}
