import {ScraperResponse} from "../scraper";
import { Scraper } from "../types";
import FailureResponse from "../../response/FailureResponse";
import SuccessResponse from "../../response/SuccessResponse";
import {MysqlPool} from "../../mysql";
import { SCRAPER_TIPOLOGIA } from "../ScraperOptions";

export default class UnaretiGas implements Scraper {
  private mysqlPool: MysqlPool;

  constructor({mysqlPool}: any) {
    this.mysqlPool = mysqlPool;
  }

  async login(): Promise<void> {
  }

  async scrape({pdr}: { pdr: string }): Promise<ScraperResponse> {
    const sql = 'SELECT * FROM unareti WHERE `Codice PdR` = ? AND `Stato del punto` = ?';
    const args = [pdr, 'DISATTIVATO'];
    try {
      await this.mysqlPool.queryOne(sql, args);
    } catch (e: any) {
      return new FailureResponse(e.message)
    }
    return new SuccessResponse();
  }

  getScraperCodice() {
    return 'unareti-gas';
  }

  getScraperTipologia() {
    return SCRAPER_TIPOLOGIA.FATTIBILITA;
  }
}
