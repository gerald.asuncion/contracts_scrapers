import path from 'path';
import fs from 'fs';
import util from 'util';
import { Page } from 'puppeteer';
import setDownloadPath from '../../browser/page/setDownloadPath';
import waitForNavigation from "../../browser/page/waitForNavigation";
import waitForSelectorAndClick from "../../browser/page/waitForSelectorAndClick";
import waitForSelectorAndType from "../../browser/page/waitForSelectorAndType";
import UnaretiRepo from "../../repo/UnaretiRepo";
import getCurrentDayFormatted from '../../utils/getCurrentDayFormatted';
import mkdir from '../../utils/mkdir';
import tryOrThrow from "../../utils/tryOrThrow";
import { ScraperResponse, WebScraper } from "../scraper";
import { ScraperTipologia, SCRAPER_TIPOLOGIA } from "../ScraperOptions";
import { UnaretiDownloaderOptions } from "./types";
import FailureResponse from '../../response/FailureResponse';
import unzipFile from './utils/unzipFile';
import todayDate from '../../utils/todayDate';
import SuccessResponse from '../../response/SuccessResponse';
import { Logger } from '../../logger';
import extractErrorMessage from '../../utils/extractErrorMessage';
import downloadFileByUrl from '../../browser/page/downloadFileByUrl';
import timeoutPromise from '../timeoutPromise';
import waitFile from '../../utils/waitFile';

export default class UnaretiDownload extends WebScraper {
  private unaretiRepo: UnaretiRepo;
  protected loginUrl: string;
  protected dbTableName: string;
  protected downloadedFileName: string;

  constructor(options: UnaretiDownloaderOptions) {
    super(options);
    this.baseDownloadPath = options.baseDownloadPath;
    this.unaretiRepo = options.unaretiRepo;
    this.loginUrl = '';
    this.dbTableName = '';
    this.downloadedFileName = '';
  }

  /**
   * @override
   */
  async login(): Promise<void> {
    const { username, password } = await this.getCredenziali();
    const page = await this.p();

    await page.goto(this.loginUrl, { waitUntil: 'networkidle0' });

    await tryOrThrow(
      () => waitForSelectorAndType(page, '#username', username),
      'non sono riuscito ad inserire lo username:'
    );

    await tryOrThrow(
      () => waitForSelectorAndType(page, '#password', password),
      'non sono riuscito ad inserire la password:'
    );

    await Promise.all([
      waitForNavigation(page),
      waitForSelectorAndClick(page, '#btnSubmit')
    ]);

    // await Promise.race([
    //   waitCheckCredentials(page, '#message', loginUrl, username, logger),
    //   page.waitForSelector('#nav > li:nth-child(1) > a', { timeout: 60000 })
    // ]);
  }

  /**
   * @override
   */
  async scrapeWebsite(): Promise<ScraperResponse> {
    const logger = this.childLogger;
    const page = await this.p();

    const downloadDir = path.resolve(this.baseDownloadPath, getCurrentDayFormatted(), 'unareti-downloads');
    mkdir(downloadDir);
    await setDownloadPath(page, downloadDir);

    const [$el] = await page.$x('//a[contains(., "Download dati siti contendibili")]');
    const fileUrl = await (await $el.getProperty('href')).jsonValue<string>();
    if (!fileUrl) {
      throw new Error('file da scaricare non trovato');
    }

    logger.info(`Scarico il file ${fileUrl}`);
    const result = await this.downladFileAndUnzip(page, logger, fileUrl, downloadDir);

    if (result instanceof FailureResponse) {
      return result;
    }

    const [filePath, csvFilePath] = result;

    try {
      logger.info(`Inserisco le righe del file CSV nel database`);
      await this.saveDataOnDatabase(csvFilePath, logger);

      logger.info('Cancello i file scaricati');
      fs.unlinkSync(filePath);
      fs.unlinkSync(csvFilePath);

      return new SuccessResponse('');
    } catch (ex) {
      const exMsg = extractErrorMessage(ex);
      if (filePath) {
        fs.unlinkSync(filePath);
      }
      if (csvFilePath) {
        fs.unlinkSync(csvFilePath);
      }
      return new FailureResponse(exMsg);
    }
  }

  async downladFileAndUnzip(page: Page, logger: Logger, fileUrl: string, downloadDir: string): Promise<[string, string] | FailureResponse> {
    await downloadFileByUrl(page, fileUrl, this.downloadedFileName);

    const filePath = path.resolve(downloadDir as string, this.downloadedFileName);
    // console.log('attendo il file ' + filePath);
    const waitFileTimeoutError = timeoutPromise('Download Timeout', 180000);
    await waitFileTimeoutError(waitFile(filePath));
    // throw new Error(`non sono riuscito a scaricare il file ${fileName} a causa ${extractErrorMessage(ex)}`);

    const fileStat = await util.promisify(fs.stat)(filePath);
    if (fileStat.size < 1) return new FailureResponse('Errore nel download del file');

    const [entry] = unzipFile(filePath, downloadDir);
    logger.info(`Scaricato il file ${filePath}`);

    const csvFilePath = path.resolve(downloadDir, entry.name);
    logger.info(`Estratto il file ${csvFilePath}`);

    return [filePath, csvFilePath];
  }

  async saveDataOnDatabase(filePath: string, logger: Logger) {
    logger.info('Prima eseguo il truncate sulla tabella');
    await this.unaretiRepo.truncate(this.dbTableName);

    logger.info('Ora azzero gli indici auto-increment');
    await this.unaretiRepo.resetPrimaryKeyIndex(this.dbTableName);

    logger.info('Ed ora eseguo il caricamento in tabella');
    await this.unaretiRepo.loadDataInfile(filePath, todayDate(), this.dbTableName);
  }

  /**
   * @override
   */
  getScraperCodice(): string {
    throw new Error("Devi fare l'override");
  }

  /**
   * @override
   */
  getScraperTipologia(): ScraperTipologia {
    return SCRAPER_TIPOLOGIA.IMPORTDATA;
  }
}
