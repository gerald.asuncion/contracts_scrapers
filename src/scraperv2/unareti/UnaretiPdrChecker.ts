import { ScraperResponse } from '../scraper';
import ErrorResponse from '../../response/ErrorResponse';
import FailureResponse from '../../response/FailureResponse';
import SuccessResponse from '../../response/SuccessResponse';
import extractErrorMessage from '../../utils/extractErrorMessage';
import UnaretiChecker from './UnaretiChecker';
import { creaIndirizzoGas } from './utils/creaIndirizzo';
import CasoNonGestitoResponse from '../../response/CasoNonGestitoResponse';
import { QueueTask } from '../../task/QueueTask';

type UnaretiPdrCheckerPayload = {
  pdr: string;
  tipoContratto: string;
};

@QueueTask()
export default class UnaretiPdrChecker extends UnaretiChecker<UnaretiPdrCheckerPayload> {
  /**
   * @override
   * @param payload Request payload
   */
  async scrapeWebsite({ pdr, tipoContratto }: UnaretiPdrCheckerPayload): Promise<ScraperResponse> {
    const logger = this.childLogger;

    try {
      logger.info(`start check pdr ${pdr}`);
      const record = await this.unaretiRepo.pdrCheck(pdr);

      if (!record) {
        logger.info('nessun risultato trovato ritorno \'KO - PDR non contendibile\'');
        return new FailureResponse('KO - PDR non contendibile');
      }

      const statoDelPunto = record ? record.stato_del_punto.toLowerCase() : '';

      if (statoDelPunto === 'interrotto') {
        logger.info(`KO - pdr ${pdr} moroso`);
        return new FailureResponse('ko-pdr moroso non acquisibile');
      }

      if (tipoContratto === 'attivazione') {
        if (statoDelPunto === 'disattivato') {
          logger.info(`KO - Necessario procedere con un Subentro per il pdr ${pdr}`);
          return new FailureResponse('KO - Necessario procedere con un Subentro');
        }

        if (statoDelPunto === 'mai attivato') {
          const indirizzo = creaIndirizzoGas(record);
          logger.info(`risultato OK ritorno indirizzo '${indirizzo}'`);
          return new SuccessResponse(indirizzo);
        }
      }

      if (tipoContratto === 'subentro') {
        if (statoDelPunto === 'mai attivato') {
          logger.info(`KO - Necessario procedere con un prima attivazione per il pdr ${pdr}`);
          return new FailureResponse('KO - Procedere con prima attivazione');
        }

        if (statoDelPunto === 'disattivato') {
          const indirizzo = creaIndirizzoGas(record);
          logger.info(`risultato OK ritorno indirizzo '${indirizzo}'`);
          return new SuccessResponse(indirizzo);
        }
      }

      logger.error('caso non gestito');
      return new CasoNonGestitoResponse();
    } catch (e) {
      const errMsg = extractErrorMessage(e);
      logger.error(errMsg);
      return new ErrorResponse(errMsg);
    }
  }

  getScraperCodice() {
    return 'unareti-checkpdr';
  }
}
