import {ScraperResponse} from "../scraper";
import { Scraper } from "../types";
import FailureResponse from "../../response/FailureResponse";
import SuccessResponse from "../../response/SuccessResponse";
import {MysqlPool} from "../../mysql";
import { SCRAPER_TIPOLOGIA } from "../ScraperOptions";

export default class UnaretiLuceNuovaAttivazione implements Scraper {
  private mysqlPool: MysqlPool;

  constructor({mysqlPool}: any) {
    this.mysqlPool = mysqlPool;
  }

  async login(): Promise<void> {
  }

  async scrape({pod}: { pod: string }): Promise<ScraperResponse> {
    const sql = 'SELECT * FROM unareti_luce WHERE `Codice POD` = ? AND `Stato del punto` = ?';
    const args = [pod, 'MAI ATTIVATO'];
    try {
      await this.mysqlPool.queryOne(sql, args);
    } catch (e: any) {
      return new FailureResponse(e.message)
    }
    return new SuccessResponse();
  }

  getScraperCodice() {
    return 'unareti-lucenuovaattivazione';
  }

  getScraperTipologia() {
    return SCRAPER_TIPOLOGIA.FATTIBILITA;
  }
}
