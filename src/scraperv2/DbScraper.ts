import { ScraperResponse } from './scraper';
import { Scraper } from "./types";
import { Logger } from '../logger';
import { MysqlPoolInterface } from '../mysql';
import createChildLogger from '../utils/createChildLogger';

export interface DbScraperOptions {
  logger: Logger;
  mysqlPool: MysqlPoolInterface;
}

export default abstract class DbScraper implements Scraper {
  protected logger: Logger;

  protected mysqlPool: MysqlPoolInterface;

  constructor({ logger, mysqlPool }: DbScraperOptions) {
    this.mysqlPool = mysqlPool;
    this.logger = createChildLogger(logger, this.constructor.name);
  }

  login(): Promise<void> {
    return Promise.resolve();
  }

  abstract scrape(args: any, params?: any): Promise<ScraperResponse>;
}
