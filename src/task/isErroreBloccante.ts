export type ErrorBloccanteConfig = {
  fornitoreErrore: string;
  errore: string;
  bloccante: boolean;
  tipoMatch: 'CONTIENE' | 'FINISCE PER' | 'INIZIA PER' | 'UGUALE A';
};

function matchContiene(errore: string, msg: string): boolean {
  return msg.includes(errore);
}

function matchFiniscePer(errore: string, msg: string): boolean {
  return msg.endsWith(errore);
}

function matchIniziaPer(errore: string, msg: string): boolean {
  return msg.startsWith(errore);
}

function matchUgualeA(errore: string, msg: string): boolean {
  return errore === msg;
}

const ERROR_MATCHER: Record<string, (errore: string, msg: string) => boolean> = {
  contiene: matchContiene,
  'finisce per': matchFiniscePer,
  'inizia per': matchIniziaPer,
  'uguale a': matchUgualeA
};

export default function isErroreBloccante(scraper: string, listaErrori: ErrorBloccanteConfig[], msg: string): boolean {
  let result = false;

  // eslint-disable-next-line max-len
  const filtered = listaErrori.filter((item) => item.bloccante && (item.fornitoreErrore.toLowerCase() === 'all' || item.fornitoreErrore.toLowerCase() === scraper));

  for (const { tipoMatch, errore } of filtered) {
    const matcher = ERROR_MATCHER[tipoMatch.toLowerCase()];
    result = matcher(errore, msg);

    if (result) {
      break;
    }
  }

  return result;
}
