import TaskErrorMailer, { TaskErrorMailerOptions } from './TaskErrorMailer';

interface IrenErrorMailerOptions extends TaskErrorMailerOptions {
  irenMailingList: string;
}

export default class IrenErrorMailer extends TaskErrorMailer {
  constructor(options: IrenErrorMailerOptions) {
    super(options);
    this.mailingList = options.irenMailingList;
    this.pratica = 'Iren';
  }
}

export class IrenLinkemErrorMailer extends IrenErrorMailer {
  constructor(options: IrenErrorMailerOptions) {
    super(options);
    this.pratica = 'Iren Linkem';
  }
}
