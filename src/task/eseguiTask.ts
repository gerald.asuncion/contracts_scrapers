import { AwilixContainer } from 'awilix';
import { Logger } from '../logger';
import TaskRepo, { Task } from '../repo/TaskRepo';
import { TASK_STATUS } from '../repo/TaskStatus';
import ErrorResponse from '../response/ErrorResponse';
import FailureResponse from '../response/FailureResponse';
import GenericSuccessResponse from '../response/GenericSuccessResponse';
import MultipleResponse from '../response/MultipleResponse';
import TimeOutResponse from '../response/TimeOutResponse';
import WrongCredentialsResponse from '../response/WrongCredentialsResponse';
import { ScraperInterface } from '../scrapers/ScraperInterface';
import Validator from '../scrapers/Validator';
import extractErrorMessage from '../utils/extractErrorMessage';
import createWebhookCaller from './createWebhookCaller';
import gestisciTaskInErrore from './gestisciTaskInErrore';
import TaskDispatcherErrorMailer from './TaskDispatcherErrorMailer';
// import BloccoScraperErrorsRepo from '../repo/BloccoScraperErrorsRepo';
import { trasformaPayload } from '../mapping/trasformaPayload';
import { creaErroreV2 } from './creaErrore';

function getValidator(container: AwilixContainer, scraper: string): Validator {
  const name = `${scraper}Validator`;
  if (container.has(name)) {
    return container.resolve<Validator>(name);
  }

  return container.resolve<Validator>('emptyValidator');
}

export default async function eseguiTask(
  container: AwilixContainer,
  task: Task,
  logger: Logger,
  taskRepo: TaskRepo,
  taskDispatcherErrorMailer: TaskDispatcherErrorMailer
  // ,bloccoScraperErrorsRepo: BloccoScraperErrorsRepo
): Promise<void> {
  const containerScoped = container.createScope();

  const callWebhook = createWebhookCaller(logger);

  try {
    logger.info(`elaboro task: ${JSON.stringify(task)}`);

    const validator: Validator = getValidator(containerScoped, task.scraper);
    const parsedPayload = JSON.parse(task.payload);

    // 14869: momentanea, finché quelli di fastweb non ci aggiornano sulle loro nuove offerte
    // if (task.scraper === 'fastweb' && parsedPayload.offertaFisso && parsedPayload.offertaFisso.toLowerCase() === 'fastweb nexxt casa plus') {
    //   throw creaErroreV2(task.scraper, 'erroriBloccanti' in parsedPayload ? parsedPayload.erroriBloccanti : [], 'offerta non inseribile tramite scraper');
    // }

    if (validator) {
      try {
        await validator.unknown(true).validateAsync(parsedPayload);
      } catch (exVal) {
        throw creaErroreV2(task.scraper, 'erroriBloccanti' in parsedPayload ? parsedPayload.erroriBloccanti : [], `payload non valido: ${extractErrorMessage(exVal)}`, '05');
      }
    }

    const scraper = containerScoped.resolve<ScraperInterface>(task.scraper);

    const scraperCodice = scraper.getScraperCodice();

    const payloadTrasformato = await trasformaPayload(scraperCodice, parsedPayload);

    await taskRepo.updatePayloadTrasformato(task, payloadTrasformato);

    const response = await scraper.scrape(payloadTrasformato);
    logger.info(`risposta scraper ${JSON.stringify(response)}`);

    if (!(response instanceof GenericSuccessResponse)) {
      let errMsg;
      let errCode;
      if (response instanceof FailureResponse
        || response instanceof ErrorResponse
        || response instanceof TimeOutResponse
        || response instanceof MultipleResponse
        || response instanceof WrongCredentialsResponse) {
        errMsg = typeof response.details === 'string' ? response.details : JSON.stringify(response.details);
        errCode = response.code;
      } else {
        errMsg = JSON.stringify(response);
        errCode = '05';
      }

      // throw (await creaErrore(parsedPayload.erroriBloccanti, err));
      throw creaErroreV2(task.scraper, 'erroriBloccanti' in parsedPayload ? parsedPayload.erroriBloccanti : [], errMsg, errCode, response as any);
    } else {
      if (task.webhook) {
        logger.info(`invio al webhook ${task.webhook} la response: ${JSON.stringify(response)}`);
        await callWebhook(task.webhook, response);
      }

      logger.info(`salvo il task con lo stato ${TASK_STATUS.COMPLETED}`);
      task.response = JSON.stringify(response);
      task.status = TASK_STATUS.COMPLETED;
      await taskRepo.update(task);
    }
  } catch (ex: any) {
    await gestisciTaskInErrore(ex, logger, taskDispatcherErrorMailer, task, callWebhook, taskRepo);
  }
}
