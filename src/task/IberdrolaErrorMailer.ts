import TaskErrorMailer, { TaskErrorMailerOptions } from './TaskErrorMailer';
import createChildLogger from '../utils/createChildLogger';

interface IberdrolaErrorMailerOptions extends TaskErrorMailerOptions {
  iberdrolaMailingList: string;
}

export default class IberdrolaErrorMailer extends TaskErrorMailer {
  constructor(options: IberdrolaErrorMailerOptions) {
    super(options);
    this.mailingList = options.iberdrolaMailingList;
    this.pratica = 'Iberdrola';
    this.childLogger = createChildLogger(options.logger, 'IberdrolaErrorMailer');
  }
}

export class IberdrolaV2ErrorMailer extends IberdrolaErrorMailer {}
