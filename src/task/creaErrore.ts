import BlockedScraperError from '../errors/BlockedScraperError';
import isErroreBloccante, { ErrorBloccanteConfig } from './isErroreBloccante';
import FailureResponse from '../response/FailureResponse';
import ErrorResponse from '../response/ErrorResponse';
import TimeOutResponse from '../response/TimeOutResponse';
import MultipleResponse from '../response/MultipleResponse';
import WrongCredentialsResponse from '../response/WrongCredentialsResponse';
import SupermoneyResponse from '../response/SupermoneyResponse';
import TaskError from '../errors/TaskError';

// export default async function creaErrore(bloccoScraperErrorsRepo: BloccoScraperErrorsRepo, msg: string): Promise<Error> {
//   return (await bloccoScraperErrorsRepo.isBloccante(msg)) ? new BlockedScraperError(msg) : new Error(msg);
// }

export const isScraperErrorResponse = <T>(response: T) => (
  response instanceof FailureResponse
  || response instanceof ErrorResponse
  || response instanceof TimeOutResponse
  || response instanceof MultipleResponse
  || response instanceof WrongCredentialsResponse
);

export function creaErroreV2(scraper: string, listaErrori: ErrorBloccanteConfig[], msg: string, errCode: string, scraperResponse?: SupermoneyResponse<unknown>): Error {
  if (isErroreBloccante(scraper, listaErrori, msg)) {
    return new BlockedScraperError(msg);
  }

  return new TaskError(msg, errCode, scraperResponse!);
  // const error = isErroreBloccante(scraper, listaErrori, msg) ? new BlockedScraperError(msg) : new Error(msg);
  // (error as any).code = errCode;
  // (error as any).scraperResponse = scraperResponse;
  // return error;
}
