import createChildLogger from '../utils/createChildLogger';
import TaskErrorMailer, { TaskErrorMailerOptions } from './TaskErrorMailer';

interface EoloErrorMailerOptions extends TaskErrorMailerOptions {
  eoloMailingList: string;
}

export default class EoloErrorMailer extends TaskErrorMailer {
  constructor(options: EoloErrorMailerOptions) {
    super(options);
    this.mailingList = options.eoloMailingList;
    this.pratica = 'Eolo';
    this.childLogger = createChildLogger(options.logger, 'EoloErrorMailer');
  }
}
