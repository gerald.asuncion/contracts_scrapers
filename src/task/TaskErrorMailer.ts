import { Mailer } from '../mailer';
import createChildLogger from '../utils/createChildLogger';
import formatDateForMail from './formatDateForMail';
import extractErrorMessage from '../utils/extractErrorMessage';
import { Logger } from '../logger';

export interface TaskErrorMailerOptions {
  logger: Logger;
}

export default abstract class TaskErrorMailer extends Mailer {
  protected mailingList: string;

  protected pratica: string;

  constructor(options: TaskErrorMailerOptions) {
    super(options);
    this.mailingList = '';
    this.pratica = '';
    this.childLogger = createChildLogger(options.logger, this.constructor.name);
  }

  async sendError(idDatiContratto: string, errMsg: string): Promise<void> {
    try {
      await this.send(this.mailingList, `Errore pratica ${this.pratica} ${idDatiContratto}`, `
        <!DOCTYPE html>
        <html lang="it">
        <head>
          <meta charset="UTF-8">
          <meta name="viewport" content="width=device-width, initial-scale=1.0">
          <title>Errore pratica ${this.pratica} ${idDatiContratto}</title>
        </head>
        <body>
          <div>
            <h3>Attenzione:</h3>
            <p>La pratica ${this.pratica} con id ${idDatiContratto} é andata in errore alle ${formatDateForMail(new Date())} per il seguente motivo:</p>
            <p>${errMsg}</p>
          </div>
        </body>
        </html>
      `, '');
    } catch (ex) {
      this.childLogger.error(`Impossibile notificare l'errore via mail - ${extractErrorMessage(ex)}`);
    }
  }
}
