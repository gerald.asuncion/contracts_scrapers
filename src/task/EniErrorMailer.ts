import TaskErrorMailer, { TaskErrorMailerOptions } from './TaskErrorMailer';
import createChildLogger from '../utils/createChildLogger';

export interface EniErrorMailerOptions extends TaskErrorMailerOptions {
  eniMailingList: string;
}

export default class EniErrorMailer extends TaskErrorMailer {
  constructor(options: EniErrorMailerOptions) {
    super(options);
    this.mailingList = options.eniMailingList;
    this.pratica = 'Eni';
    this.childLogger = createChildLogger(options.logger, this.constructor.name);
  }
}
