import EniErrorMailer, { EniErrorMailerOptions } from './EniErrorMailer';

export default class EniResidenzialeInserimentoErrorMailer extends EniErrorMailer {
  constructor(options: EniErrorMailerOptions) {
    super(options);
    this.pratica = 'Eni Residenziale';
  }
}
