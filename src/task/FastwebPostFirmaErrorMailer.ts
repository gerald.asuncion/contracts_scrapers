import TaskErrorMailer, { TaskErrorMailerOptions } from './TaskErrorMailer';
import createChildLogger from '../utils/createChildLogger';

interface FastwebPostFirmaErrorMailerOptions extends TaskErrorMailerOptions {
  fastwebMailingList: string;
}

export default class FastwebPostFirmaErrorMailer extends TaskErrorMailer {
  constructor(options: FastwebPostFirmaErrorMailerOptions) {
    super(options);
    this.mailingList = options.fastwebMailingList;
    this.pratica = 'Fastweb Post Firma';
    this.childLogger = createChildLogger(options.logger, 'FastwebPostFirmaErrorMailer');
  }
}
