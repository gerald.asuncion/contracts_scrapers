import camelCase from 'lodash/camelCase';
import merge from 'lodash/merge';
import ContrattoPayload from '../contratti/ContrattoPayload';
import { Task } from '../repo/TaskRepo';
import { TaskStatus, TASK_STATUS } from '../repo/TaskStatus';

export type QueueTaskOptions = {
  scraperName: string;
  status: TaskStatus;
  retry: number;
  maxRetry: number;
};

export class QueueTaskManager {
  private static map: Record<string, QueueTaskOptions> = {};

  static add(options: QueueTaskOptions) {
    QueueTaskManager.map[options.scraperName] = options;
  }

  static getOptionsByScraperName(scraperName: string): QueueTaskOptions | undefined {
    return QueueTaskManager.map[scraperName];
  }

  static toTaskSettingsByScraperName<P extends ContrattoPayload>(scraperName: string, payload: P): Task {
    const options = QueueTaskManager.getOptionsByScraperName(scraperName);

    if (!options) {
      throw new Error(`Task options for ${scraperName} not found`);
    }

    return {
      scraper: options.scraperName,
      status: options.status,
      retry: options.retry,
      maxRetry: options.maxRetry,
      payload: payload,
      webhook: payload.webhook ? payload.webhook : ''
    }
  }
}

function createBaseOptions(constructorName: string): QueueTaskOptions {
  return {
    scraperName: camelCase(constructorName),
    status: TASK_STATUS.READY,
    retry: 0,
    maxRetry: 0
  };
}

/**
 * (class) Decorator factory to register task for the queue
 * @param options Options to customize task factory
 * @returns decorator
 */
export function QueueTask(options?: Partial<Omit<QueueTaskOptions, 'status'>>) {
  return function queueTaskDecorator<T  extends { new (...args: any[]): {} }>(constructor: T) {
    QueueTaskManager.add(merge({}, createBaseOptions(constructor.name), options));
    return constructor;
  }
}
