import TaskRepo, { Task } from '../repo/TaskRepo';
import extractErrorMessage from '../utils/extractErrorMessage';
import { TASK_STATUS } from '../repo/TaskStatus';
import ErrorResponse from '../response/ErrorResponse';
import { Logger } from '../logger';
import TaskDispatcherErrorMailer from './TaskDispatcherErrorMailer';
import BlockedScraperError from '../errors/BlockedScraperError';
import TaskError from '../errors/TaskError';

function isV3(task: Task) {
  const list = [
    'arera',
    'dueIReteGasV3',
    'eDistribuzioneCheckPod',
    'italgasAttivazioneV3',
    'italgasContendibilitaGasV3',
    'italgasToscanaV3',
    'unaretiCheckPdrV3',
    'unaretiCheckPodV3',
    'iretiCheckPodV3',
    'iretiCheckPdrV3',
    'centriaPdrCheckV3',
    'irenPrecheckV3',
    'leretiCheckPdrV3',
    'erogasmetV3',
  ];

  return list.includes(task.scraper);
}

// function createV3ErrorResponse(ex: Error & { code: string; scraperResponse: SupermoneyResponse<unknown>; }) {
//   return ex.scraperResponse || new FailureResponse(extractErrorMessage(ex));
// }

function createV3ErrorResponse(ex: TaskError) {
  return ex.scraperResponse;
}

export default async function gestisciTaskInErrore(
  ex: Error,
  logger: Logger,
  taskDispatcherErrorMailer: TaskDispatcherErrorMailer,
  task: Task,
  callWebhook: (url: string, msg: unknown) => Promise<unknown>,
  taskRepo: TaskRepo
): Promise<void> {
  const isBlocked = ex instanceof BlockedScraperError;
  const errMsg = extractErrorMessage(ex);
  logger.error(errMsg);

  await taskDispatcherErrorMailer.sendError(task, errMsg);

  task.retry += 1;
  if (task.retry >= task.maxRetry) {
    task.status = TASK_STATUS.ERROR;
    const errResponse = isBlocked ? (ex as BlockedScraperError).toResponse() : (isV3(task) ? createV3ErrorResponse(ex as TaskError) : new ErrorResponse(errMsg));
    // task.response = JSON.stringify(isBlocked ? (ex as BlockedScraperError).toResponse() : (isV3(task) ? createV3ErrorResponse(ex as any) : new FailureResponse(errMsg)));
    task.response = JSON.stringify(errResponse);

    if (task.webhook) {
      logger.info(`invio al webhook ${task.webhook} l'errore ${isBlocked ? '(bloccante)' : ''}: ${JSON.stringify(errResponse)}`);
      await callWebhook(task.webhook, errResponse);
    }
  } else {
    task.status = TASK_STATUS.READY;
  }
  logger.info(`aggiorno il task: ${JSON.stringify(task)}`);
  await taskRepo.update(task);
}
