import { Logger } from '../logger';
import TaskRepo from '../repo/TaskRepo';
import { ScraperResponse } from '../scraperv2/scraper';
import { Scraper } from "../scraperv2/types";
import ErrorResponse from '../response/ErrorResponse';
import SuccessResponse from '../response/SuccessResponse';
import ContrattoPayload from '../contratti/ContrattoPayload';
import createChildLogger from '../utils/createChildLogger';
import { SCRAPER_TIPOLOGIA } from '../scraperv2/ScraperOptions';
import { QueueTaskManager } from './QueueTask';
import extractErrorMessage from '../utils/extractErrorMessage';

interface TaskQueueOptions {
  logger: Logger;
  taskRepo: TaskRepo;
}

interface TaskQueueRequestParams {
  scraper: string;
}

export default class TaskQueue implements Scraper {
  private taskRepo: TaskRepo;

  private logger: Logger;

  constructor({ logger, taskRepo }: TaskQueueOptions) {
    this.taskRepo = taskRepo;
    this.logger = createChildLogger(logger, this.constructor.name);
  }

  login(): Promise<void> {
    return Promise.resolve();
  }

  async scrape(args: ContrattoPayload, { scraper }: TaskQueueRequestParams): Promise<ScraperResponse> {
    const { logger } = this;
    try {
      const task = QueueTaskManager.toTaskSettingsByScraperName(scraper, args);
      logger.info(`aggiungo il task ${JSON.stringify(task)}`);
      await this.taskRepo.add(task);
      return new SuccessResponse();
    } catch (ex: any) {
      const exMsg = extractErrorMessage(ex);
      logger.error(exMsg);
      return new ErrorResponse(exMsg);
    }
  }

  getScraperCodice() {
    return 'taskqueue';
  }

  getScraperTipologia() {
    return SCRAPER_TIPOLOGIA.INSERIMENTO;
  }
}
