import TaskErrorMailer, { TaskErrorMailerOptions } from './TaskErrorMailer';

interface EdisonErrorMailerOptions extends TaskErrorMailerOptions {
  edisonMailingList: string;
}

export default class EdisonErrorMailer extends TaskErrorMailer {
  constructor(options: EdisonErrorMailerOptions) {
    super(options);
    this.mailingList = options.edisonMailingList;
    this.pratica = 'Edison';
  }
}
