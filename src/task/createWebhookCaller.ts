import axios from 'axios';
import extractErrorMessage from '../utils/extractErrorMessage';
import { Logger } from '../logger';
import delayedRetry from '../utils/retry';

export default function createWebhookCaller(logger: Logger): (url: string, msg: unknown) => Promise<unknown> {
  const handleWebhookCatch = (rej: unknown) => logger.error(`Chiamata al webhook fallita: ${extractErrorMessage(rej)}`);
  return (url: string, msg: unknown): Promise<unknown> => delayedRetry(() => axios.post(url, msg).catch(handleWebhookCatch), {
    infoMessage: 'chiamata webhook'
  }, logger);
}
