import { Task } from '../repo/TaskRepo';
import { Logger } from '../logger';
import createChildLogger from '../utils/createChildLogger';
import extractErrorMessage from '../utils/extractErrorMessage';
import ErrorMailerSelector from './ErrorMailerSelector';

interface TaskDispatcherErrorMailerOptions {
  logger: Logger;
  errorMailerSelector: ErrorMailerSelector;
}

export default class TaskDispatcherErrorMailer {
  private logger: Logger;

  private errorMailerSelector: ErrorMailerSelector;

  constructor({ logger, errorMailerSelector }: TaskDispatcherErrorMailerOptions) {
    this.errorMailerSelector = errorMailerSelector;
    this.logger = createChildLogger(logger, 'TaskDispatcherErrorMailer');
  }

  async sendError(task: Task, errorMsg: string): Promise<void> {
    try {
      const mailer = this.errorMailerSelector.select(task);

      if (mailer) {
        const parsedPayload = JSON.parse(task.payload);

        await mailer.sendError(parsedPayload.idDatiContratto, errorMsg);
      }
    } catch (ex) {
      this.logger.error(`Impossibile notificare l'errore via mail - ${extractErrorMessage(ex)}`);
    }
  }
}
