import createChildLogger from '../utils/createChildLogger';
import container from '../taskContainer';
import { Logger } from '../logger';
import TaskRepo, { Task } from '../repo/TaskRepo';
import extractErrorMessage from '../utils/extractErrorMessage';
import { TaskStatus, TASK_STATUS } from '../repo/TaskStatus';
import delay from '../utils/delay';
import NoResultError from '../errors/NoResultError';
import TaskDispatcherErrorMailer from './TaskDispatcherErrorMailer';
import eseguiTask from './eseguiTask';
import gestisciTaskInErrore from './gestisciTaskInErrore';
import createWebhookCaller from './createWebhookCaller';
// import BloccoScraperErrorsRepo from '../repo/BloccoScraperErrorsRepo';
import GracefulShutdownController from '../graceful/GracefulShutdownController';

const NO_TASK_DELAY = 10000; // in millisecondi

const logger = createChildLogger(container.resolve<Logger>('logger'), 'TaskConsumer');
const taskRepo = container.resolve<TaskRepo>('taskRepo');
const taskDispatcherErrorMailer = container.resolve<TaskDispatcherErrorMailer>('taskDispatcherErrorMailer');
// const bloccoScraperErrorsRepo = container.resolve<BloccoScraperErrorsRepo>('bloccoScraperErrorsRepo');
const gracefulShutdownController = container.resolve<GracefulShutdownController>('gracefulShutdownController');

async function getTask(status: TaskStatus): Promise<Task | undefined> {
  try {
    return await taskRepo.getOneByStatus(status, logger);
  } catch (ex) {
    if (!(ex instanceof NoResultError)) {
      logger.error('WAITING_TASK_DISCOVERY:taskConsumer::getTask - rethrowing!');
      logger.error(ex);

      throw ex;
    }

    logger.error('WAITING_TASK_DISCOVERY:taskConsumer::getTask - catch (ex instanceof NoResultError)!');
    logger.error(ex);
  }
}

async function getTaskAndProcess(status: TaskStatus = 'READY') {
  if (gracefulShutdownController.isStoppable()) {
    logger.info('graceful shutdown - stopping task consumer');
    return;
  }

  // const task = await getTask(status);
  const task = await taskRepo.getOneByStatus(status, logger);
  const callWebhook = createWebhookCaller(logger);

  if (task) {
    gracefulShutdownController.lock();

    try {
      await eseguiTask(container, task, logger, taskRepo, taskDispatcherErrorMailer/* , bloccoScraperErrorsRepo */);
    } catch (ex: any) {
      // TODO: to remove
      await gestisciTaskInErrore(ex, logger, taskDispatcherErrorMailer, task, callWebhook, taskRepo);
    } finally {
      gracefulShutdownController.free();
    }
  } else {
    logger.warn(`Nessun TASK trovato in stato ${TASK_STATUS.READY}. Attendo ${NO_TASK_DELAY / 1000} secondi per la prossima elaborazione.`);
    // gracefulShutdownController.free();
    await delay(NO_TASK_DELAY);
  }

  await getTaskAndProcess(status);
}

export default async function taskConsumer(): Promise<void> {
  try {
    await getTaskAndProcess();
  } catch (ex) {
    const errMsg = extractErrorMessage(ex);
    logger.error(errMsg);

    await taskConsumer();
  }
}
