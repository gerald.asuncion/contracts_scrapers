import { Task } from '../repo/TaskRepo';
import TaskErrorMailer from './TaskErrorMailer';
import container from '../taskContainer';

export default class ErrorMailerSelector {
  select({ scraper }: Task): TaskErrorMailer | null {
    let mailer = null;

    try {
      mailer = container.resolve<TaskErrorMailer>(`${scraper}ErrorMailer`);
    } catch (ex) {
      // il mailer non esiste
      // nulla da fare
    }
    return mailer;
  }
}
