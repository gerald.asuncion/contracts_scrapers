import TaskErrorMailer, { TaskErrorMailerOptions } from './TaskErrorMailer';
import createChildLogger from '../utils/createChildLogger';

interface FastwebErrorMailerOptions extends TaskErrorMailerOptions {
  fastwebMailingList: string;
}

export default class FastwebErrorMailer extends TaskErrorMailer {
  constructor(options: FastwebErrorMailerOptions) {
    super(options);
    this.mailingList = options.fastwebMailingList;
    this.pratica = 'Fastweb';
    this.childLogger = createChildLogger(options.logger, 'FastwebErrorMailer');
  }
}
