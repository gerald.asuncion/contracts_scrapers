import config from 'config';
import winston, { createLogger, transports, format } from 'winston';
import logFormatter from './logFormatter';
import formatTimestamp from './logTimestampFormatter';

const { combine, timestamp, label } = format;

export const loggerTransports: transports.FileTransportInstance[] = [
  new transports.File({
    filename: `${config.get('loggerConfig.path')}scraper.log`
  })
];

// in fase di sviluppo uso anche il transportConsole
// if(!NODE_ENV) {
//     loggerTransports.push(new transports.Console());
// }

export const logger: winston.Logger = createLogger({
  format: combine(
    label({ label: 'SM-SCRAPERS' }),
    timestamp({ format: formatTimestamp }),
    logFormatter
  ),
  transports: loggerTransports,
});

const loggerHttpTransports: transports.FileTransportInstance[] = [
  new transports.File({ filename: `${config.get('loggerConfig.path')}http.log` })
];

export const loggerHttp: winston.Logger = createLogger({
  format: combine(
    label({ label: 'SM-SCRAPERS-HTTP' }),
    timestamp({ format: formatTimestamp }),
    logFormatter
  ),
  transports: loggerHttpTransports,
});

export const loggerStream = {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  write: (message: any, encoding?: string): void => {
    loggerHttp.info(message.replace(/\n/g, '').trim());
  }
};

export type Logger = winston.Logger;

// export function createComponentLogger(componentName: string) {
//     return logger.child({ componentName });
// }
