module.exports = {
  "extends": [
    "airbnb-typescript/base",
    "plugin:@typescript-eslint/recommended"
  ],
  "files": [".ts", ".js"],
  "parserOptions": {
    "project": ["./tsconfig.eslint.json"]
  },
  "rules": {
    "no-param-reassign": ["error", { "props": false }],
    "class-methods-use-this": "off",
    "comma-dangle": "off",
    "@typescript-eslint/comma-dangle": ["error", {
      "arrays": "never",
      "objects": "only-multiline",
      "imports": "never",
      "exports": "never",
      "functions": "never"
    }],
    "no-restricted-syntax": "off",
    "radix": ["error", "as-needed"],
    "max-len": ["error", { "code": 160 }]
  }
}
