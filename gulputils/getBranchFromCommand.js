const getCommandArgument = require("./getCommandArgument");

const ORIGIN_BRANCH_COMMAND_KEY = '--branch';
const ORIGIN_BRANCH_DEF = 'master';

function getBranchFromCommand(argv) {
  return getCommandArgument(argv, ORIGIN_BRANCH_COMMAND_KEY) || ORIGIN_BRANCH_DEF;
}

module.exports = getBranchFromCommand;
