const rimraf = require('rimraf');

module.exports = function cleanOutputFolder(cb) {
  rimraf('output/**/*', { glob: { ignore: '.gitignore' } }, cb);
};
