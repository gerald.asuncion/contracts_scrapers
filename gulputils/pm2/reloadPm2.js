const shell = require('gulp-shell');

function reloadPm2() {
  return shell.task([
    'echo riavvio il server...',
    'pm2 del ecosystem.config.js --env test',
    'pm2 start ecosystem.config.js --env test',
    'echo riavvio terminato'
  ]);
}

module.exports = reloadPm2;
