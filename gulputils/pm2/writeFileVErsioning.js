const fs = require('fs');

const writeFileVersioning = (path) => (cb) => {
  fs.writeFile(path, Date.now().toString(), { flag: 'w', encoding: 'utf-8' }, cb);
}

module.exports = writeFileVersioning;
