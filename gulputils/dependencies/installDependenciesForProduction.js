const installDependencies = require('./installDependencies');

module.exports = () => installDependencies(true);
