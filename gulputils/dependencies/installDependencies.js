const shell = require('gulp-shell');

function installDependencies(production /* boolean */) {
  return shell.task([
    `echo aggiorno le dipendenze ${production ? "per lo zip" : 'per la build'}...`,
    [
      'yarn install',
      '--frozen-lockfile',
      production ? '--production' : undefined,
      '&& yarn cache clean'
    ].filter(Boolean)
    .join(' '),
    'echo aggiornamento dipendenze terminato'
  ]);
}

module.exports = installDependencies;
