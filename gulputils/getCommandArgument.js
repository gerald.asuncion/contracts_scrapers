function getCommandArgument(argv, key) {
  const idx = argv.indexOf(key);
  let result = null;
  if(idx !== -1) {
    result = argv[idx+1];
  }
  return result;
}

module.exports = getCommandArgument;
