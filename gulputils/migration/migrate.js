const shell = require('gulp-shell');

function migrate(env) {
  return shell.task([
    `sequelize db:migrate --env ${env}`,
    `sequelize db:seed:all --env ${env}`
  ]);
}

module.exports = migrate;
