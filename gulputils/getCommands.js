const getBranchFromCommand = require("./getBranchFromCommand");

function getCommands() {
  const argv = process.argv.slice(2);

  const branch = getBranchFromCommand(argv);

  return { branch };
}

module.exports = getCommands;
