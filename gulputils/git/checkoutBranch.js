const shell = require('gulp-shell');

function checkoutBranch(branch) {
  if(!branch) {
    throw new Error('Non è stato scelto il branch da rilasciare');
  }

  return shell.task([
    'echo scarico gli aggiornamenti...',
    'git fetch -pv',
    'git reset --hard',
    `git checkout ${branch}`,
    `git reset --hard origin/${branch}`,
    `git pull origin ${branch}`,
    'echo aggiornamento branch terminato'
  ]);
}

module.exports = checkoutBranch;
