const { series } = require('gulp');
const cleanBuild = require('./cleanBuild');
const compile = require('./compile');
const copySrcJsonToBuild = require('./copySrcJsonToBuild');

const build = (serviceName) => series(cleanBuild(serviceName), compile(serviceName));
exports.build = build;

const buildApi = series(build('api'), copySrcJsonToBuild);
exports.buildApi = buildApi;

const buildTaskRunner = build('taskrunner');
exports.buildTaskRunner = buildTaskRunner;
