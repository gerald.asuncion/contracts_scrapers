const rimraf = require('rimraf');

module.exports = (serviceName) => function cleanBuild(cb) {
  return rimraf(`build/${serviceName}`, cb);
}
