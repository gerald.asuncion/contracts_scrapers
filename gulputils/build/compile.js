const shell = require('gulp-shell');

const tsconfigFileMap = {
  'api': 'tsconfig.api.json',
  'taskrunner': 'tsconfig.taskrunner.json'
};

module.exports = function compile(serviceName) {
  const tsconfigFile = tsconfigFileMap[serviceName];

  return shell.task([
    `echo effettuo la build del servizio ${serviceName}...`,
    `yarn tsc -p ${tsconfigFile}`,
    'echo build terminata'
  ]);
}
