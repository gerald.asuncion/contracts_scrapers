const { src, dest } = require('gulp');

module.exports = function copySrcJsonToBuild() {
  return src('./src/**/*.json').pipe(dest('./build/api/src'));
};
