# Linee guida per il progetto Scrapers

In questo file vengono raccolti gli standard per quanto riguarda il progetto scraper.

## Come si fanno gli Scraper

Il consiglio più grande che posso darvi per la creazione degli scraper è di cercare di comprendere la logica di funzionamento di quel particolare sito (perché ogni sito è diverso dagli altri proprio come le persone sono simili, ma diverse fra loro). Se farete questo vi sarà più semplice creare lo scraper.
E' troppo facile scaricare la colpa al sito di turno se poi lo scraper creato è stato fatto in maniera "superficiale", se non ci credete provate anche solo a guardare la differenza nel codice che c'è tra lo scraper di Iberdrola pre 15/01/2021 (prima dell'arrivo di Pietro Marino) e quello rifatto in estate 2021.
Il vecchio andava in errore molte più volte perché non si è gestita nemmeno la navigazione tra le pagine, ma si è ricorso all'uso dei delay (che non sono mai buoni ed utili, non sempre si può attendere solo 3 secondi). Quello fatto a settembre 2021 non conta perché è cambiato il sito.
 
- gli scraper sono completamente contenuti all'interno della loro cartella che si troverà sotto 'src/scraper[v<N>]'
  - a meno di utility che si trovano sotto 'src/browser' e simili
- estendono WebScraper<T> dove T è il payload
  - possono essere classi astratte da reimplementare (ex. Fastweb)
  - implementano (direttamente o indeirettamente) i metodi login e scrapeWebsite dichiarati in WebScraper
- i tipi payload vanno in una sottocartella 'payload'
  - i payload, se lo scraper lo richiede, devono estendere da 'ContrattoPayload' sotto 'src/contratti/ContrattoPayload.ts' per avere la proprietà idDatiContratto, comune a tutti gli scraper di inserimento contratto
  - payload per login e scraper possono essere separati
    - entrambi, se lo scraper lo richiede, devono estendere ContrattoPayload
- i metodi di login, scrapeWebsite devono entrambi, ove richiesto, instanziare un payload-aware-logger (non istanziabile dal costruttore) per poter loggare in automatico l'idDatiContratto
  - Altrimenti basta creare un logger utilizzando la funzione createChildLogger
- Si consiglia di suddividere la funzione scrapeWebsite che fa il lavoro sporco in sotto funzioni, che d'ora in avanti chiameremo step, in modo da suddividere la logica
  - questi step a loro volta saranno composte da altre funzioni, quelle elementari (quelle funzioni per l'interazione col browser create da noi) come waitForSelectorAndType o più complesse che fanno uso di quelle elementari
- Importante ogni step, ogni funzione al suo interno e ogni uso delle funzioni elementari deve essere wrappata usando la funzione tryOrThrow in modo da poter restituire un messaggio d'errore più parlante
- Gli errori, nel details della FailureResponse e della ErrorResponse, vanno sempre restiutiti come semplici stringhe, non si possono restituire liste di errori od oggetti strutturati perché il chiamante (contracts) non saprebbe gestirli
- Quando si fa la navigazione tra pagine a seguito di un click o simili bisogna sempre fare in modo che lo scraper attendi il caricamento, il modo più semplice è usare la funzione waitForNavigation
  - poi ci sono casi particolari in cui non è sufficiente attendere la navigazione, ad esempio se la pagina viene caricata in un nuovo tab del browser
- quando si implementa la funzione login inserire anche il controllo per capire se le credenziali sono scadute e restituire l'errore WrongCredentials
- quando lo scraper va in errore bisogna assicurarsi che generi lo screenshot della pagina e farlo finire su AWS S3
  - per fare questo usare la funzione saveScreenshotOnAwsS3
  - WebScraper gestisce già il caso catturando le eccezioni che avvengono nella funzione scrapeWebsite di cui le classi figlie ne fanno l'override
