const { series } = require('gulp');
const checkoutBranch = require('./gulputils/git/checkoutBranch');
// const installDependencies = require('./gulputils/dependencies/installDependencies');
const installDependenciesForBuild = require('./gulputils/dependencies/installDependenciesForBuild');
const installDependenciesForProduction = require('./gulputils/dependencies/installDependenciesForProduction');
// const reloadPm2 = require('./gulputils/pm2/reloadPm2');
const syncProd = require('./gulputils/sync/syncProd');
const { buildApi, buildTaskRunner } = require('./gulputils/build/build');
const migrate = require('./gulputils/migration/migrate');
const cleanOutputFolder = require('./gulputils/cleanOutputFolder');
const writeFileVersioning = require('./gulputils/pm2/writeFileVErsioning');

exports.buildApi = buildApi;
exports.buildTaskRunner = buildTaskRunner;

const deployInTestApi = series(checkoutBranch('test'), installDependenciesForBuild(), buildApi, writeFileVersioning('./api.version.txt'), migrate('test'), installDependenciesForProduction()/* , reloadPm2() */);
exports.deployInTestApi = deployInTestApi;

const deployInTestTaskrunner = series(checkoutBranch('test'), installDependenciesForBuild(), buildTaskRunner, writeFileVersioning('./taskrunner.version.txt'), migrate('test'), installDependenciesForProduction()/* , reloadPm2() */);
exports.deployInTestTaskrunner = deployInTestTaskrunner;

const deployInTest = series(checkoutBranch('test'), installDependenciesForBuild(), buildApi, buildTaskRunner, writeFileVersioning('./api.version.txt'), writeFileVersioning('./taskrunner.version.txt'), migrate('test'), installDependenciesForProduction()/* , reloadPm2() */);
exports.deployInTest = deployInTest;

const deployInProdApi = series(checkoutBranch('master'), installDependenciesForBuild(), buildApi, writeFileVersioning('./api.version.txt'), migrate('production'), installDependenciesForProduction(), syncProd());
exports.deployInProdApi = deployInProdApi;

const deployInProdTaskrunner = series(checkoutBranch('master'), installDependenciesForBuild(), buildTaskRunner, writeFileVersioning('./taskrunner.version.txt'), migrate('production'), installDependenciesForProduction(), syncProd());
exports.deployInProdTaskrunner = deployInProdTaskrunner;

const deployInProd = series(checkoutBranch('master'), installDependenciesForBuild(), buildApi, buildTaskRunner, writeFileVersioning('./api.version.txt'), writeFileVersioning('./taskrunner.version.txt'), migrate('production'), installDependenciesForProduction(), syncProd());
exports.deployInProd = deployInProd;

const deployInDevApi = series(/* installDependenciesForBuild(),  */cleanOutputFolder, buildApi, writeFileVersioning('./api.version.txt'), migrate('development'), installDependenciesForProduction());
exports.deployInDevApi = deployInDevApi;

const deployInDevTaskrunner = series(/* installDependenciesForBuild(),  */cleanOutputFolder, buildTaskRunner, writeFileVersioning('./taskrunner.version.txt'), migrate('development'), installDependenciesForProduction());
exports.deployInDevTaskrunner = deployInDevTaskrunner;

exports.deployInDev = series(/* installDependenciesForBuild(),  */cleanOutputFolder, buildApi, buildTaskRunner, writeFileVersioning('./api.version.txt'), writeFileVersioning('./taskrunner.version.txt'), migrate('development'), installDependenciesForProduction());
