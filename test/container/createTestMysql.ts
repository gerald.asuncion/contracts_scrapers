/* eslint-disable @typescript-eslint/no-explicit-any */
import { MysqlPoolInterface } from '../../src/mysql';

export type TestMysqlOptions = {
  query?: (sql: string, args: any) => Promise<any[]>;
  queryAll?: (sql: string, args: any) => Promise<any[]>;
  queryOne?: (sql: string, args: any) => Promise<any>;
  truncateTable?: (table: string) => Promise<any>;
  insertIntoChunk?: (table: string, allData: string[][], chunk: number) => Promise<any>;
};

export default function createTestMysql({
  query = () => Promise.resolve([]),
  queryAll = () => Promise.resolve([]),
  queryOne = () => Promise.resolve({}),
  truncateTable = () => Promise.resolve(),
  insertIntoChunk = () => Promise.resolve()
}: TestMysqlOptions): () => MysqlPoolInterface {
  return () => ({
    query: jest.fn(query),
    queryAll: jest.fn(queryAll),
    queryOne: jest.fn(queryOne),
    truncateTable: jest.fn(truncateTable),
    insertIntoChunk: jest.fn(insertIntoChunk)
  });
}
