import { AwilixContainer, createContainer, asValue, asClass } from 'awilix';
import config from 'config';
import createTestLogger from './createTestLogger';
import BrowserManager from '../../src/browser/BrowserManager';

export type Register<T extends object> = (cnt: AwilixContainer<T>) => void;

export default function createTestContainer<T extends object>(register?: Register<T>): AwilixContainer<T> {
  const container = createContainer();
  const logger = createTestLogger();
  container.register('logger', asValue(logger));

  if (register) {
    register(container);
  }

  return container;
}

export async function createTestContainerWithBrowser<T extends object>(register?: Register<T>): Promise<AwilixContainer<T>> {
  const container = createTestContainer(register);

  container.register('puppeteerOptions', asValue(config.get('puppeteer.browser.options')));
  container.register('browser', asClass(BrowserManager).singleton());

  return container;
}
