import {
  createLogger, transports, format, Logger
} from 'winston';
import logFormatter from '../../src/logFormatter';

const {
  combine, timestamp, label
} = format;

export default function createTestLogger(): Logger {
  return createLogger({
    format: combine(
      label({ label: 'SM-SCRAPERS-TEST' }),
      timestamp({ format: 'YYYY-MM-dd HH:mm:ss' }),
      logFormatter
    ),
    transports: [new transports.Console()],
  });
}
