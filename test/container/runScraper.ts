import { AwilixContainer } from 'awilix';
import { Scraper, ScraperResponse } from '../../src/scraperv2/scraper';

export default async function runScraper<P>(
  container: AwilixContainer,
  scraperName: string,
  payload: P,
  params?: any
): Promise<jest.JestMatchers<ScraperResponse>> {
  const scraper: Scraper = container.resolve(scraperName);
  const resp: ScraperResponse = await scraper.scrape(payload, params);
  return expect(resp);
}
