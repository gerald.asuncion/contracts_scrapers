import tryOrThrow, { TryOrThrowError } from '../../src/utils/tryOrThrow';

describe('tryOrThrow', () => {
  it('not emit error', () => {
    expect(() => tryOrThrow<boolean>(async () => true, 'nessun errore')).not.toThrow();
  });

  it('emit error', async () => {
    await expect(tryOrThrow(() => { throw new TryOrThrowError('ciao') }, 'errore: ')).rejects.toThrow(TryOrThrowError);
  });
});
