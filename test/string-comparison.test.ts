import { levenshtein } from 'string-comparison';

describe('levenshtein::similarity', () => {
  it('si no => 0', () => {
    expect(levenshtein.similarity('si', 'no')).toBe(0);
  });

  it('si si => 1', () => {
    expect(levenshtein.similarity('si', 'si')).toBe(1);
  });

  it('Desidero & Non Desidero => 0', () => {
    expect(levenshtein.similarity('Desidero', 'Non Desidero')).toBeLessThan(0.8);
  });
});
