import fixFormatoComune from '../../../src/scraperv2/eolo/fixFormatoComune';

describe('fixFormatoComune', () => {
  it('FORLI\' to FORLì', () => {
    expect(fixFormatoComune('FORLI\'')).toEqual('forlì');
  });

  it('CANTU\' to CANTù', () => {
    expect(fixFormatoComune('CANTU\'')).toEqual('cantù');
  });

  it('PETRONA\' to PETRONà', () => {
    expect(fixFormatoComune('PETRONA\'')).toEqual('petronà');
  });

  it('Lecco NON cambia', () => {
    expect(fixFormatoComune('Lecco')).toEqual('lecco');
  });

  it('Barzano\' to Barzanò', () => {
    expect(fixFormatoComune('Barzano\'')).toEqual('barzanò');
  });
});
