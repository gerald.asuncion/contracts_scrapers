import { AwilixContainer, asClass, asFunction } from 'awilix';
import { createTestContainerWithBrowser } from '../../container/createTestContainer';
import AgsmGasCopertura from '../../../src/scraperv2/agsm/gas/AgsmGasCopertura';
import { Logger } from '../../../src/logger';
import runScraper from '../../container/runScraper';
import createTestMysql from '../../container/createTestMysql';
import SuccessResponse from '../../../src/response/SuccessResponse';

const SCRAPER_NAME = 'agsmGasCopertura';

const MOCK_DB = [
  { comune: 'Abbadia Lariana', provincia: 'LC', subentro: 'SI', switch: 'NO' },
  { comune: 'Mandello del Lario', provincia: 'LC', subentro: 'NO', switch: 'SI' }
];

function findItem(args: Array<string>) {
  return new Promise((resolve, reject) => {
    const record = MOCK_DB.find(item => item.comune.toLowerCase() === args[0].toLowerCase() && item.provincia.toLowerCase() === args[1].toLowerCase());
    if(!record) {
      reject('no result');
    }

    resolve(record);
  });
}

const nonCopertoMessage = (comune: string, provincia: string): string => `il Comune "${comune}" (${provincia}) non è coperto dalla fornitura gas di Agsm`;

type AgsmGasCoperturaContainerTest = {
  [SCRAPER_NAME]: AgsmGasCopertura,
  logger: Logger
};

describe('Agsm Copertura Gas', () => {
  let container: AwilixContainer<AgsmGasCoperturaContainerTest>;

  let successResponse: SuccessResponse;

  beforeAll(async () => {
    const mysqlFactory = createTestMysql({
      queryOne: (sql, args) => findItem(args)
    });

    container = await createTestContainerWithBrowser((cnt: AwilixContainer<AgsmGasCoperturaContainerTest>) => {
      cnt.register('mysqlPool', asFunction(mysqlFactory).singleton());
      cnt.register(SCRAPER_NAME, asClass<AgsmGasCopertura>(AgsmGasCopertura).singleton());
    });

    successResponse = new SuccessResponse('OK');
  });

  it('should KO - comune non in lista', async () => {
    const comune = 'PPP';
    const provincia = 'LC';
    (await runScraper(container, SCRAPER_NAME, { comune, provincia, tipologia: 'subentro' })).toMatchObject({
      code: '02',
      details: nonCopertoMessage(comune, provincia)
    });
  });

  it('should KO - comune non in lista (provincia errata)', async () => {
    const comune = 'Abbadia Lariana';
    const provincia = 'BG';
    (await runScraper(container, SCRAPER_NAME, { comune, provincia, tipologia: 'subentro' })).toMatchObject({
      code: '02',
      details: nonCopertoMessage(comune, provincia)
    });
  });

  it('should KO - non è subentrabile', async () => {
    const comune = 'Mandello del Lario';
    const provincia = 'LC';
    (await runScraper(container, SCRAPER_NAME, { comune, provincia, tipologia: 'subentro' })).toMatchObject({
      code: '02',
      details: nonCopertoMessage(comune, provincia)
    });
  });

  it('should KO - non è switchabile', async () => {
    const comune = 'Abbadia Lariana';
    const provincia = 'LC';
    (await runScraper(container, SCRAPER_NAME, { comune, provincia, tipologia: 'switch' })).toMatchObject({
      code: '02',
      details: nonCopertoMessage(comune, provincia)
    });
  });

  it('should OK - subentrabile', async () => {
    (await runScraper(container, SCRAPER_NAME, { comune: 'Abbadia Lariana', provincia: 'LC', tipologia: 'subentro' })).toEqual(successResponse);
  });

  it('should OK - switchabile', async () => {
    (await runScraper(container, SCRAPER_NAME, { comune: 'Mandello del Lario', provincia: 'LC', tipologia: 'switch' })).toEqual(successResponse);
  });
});
