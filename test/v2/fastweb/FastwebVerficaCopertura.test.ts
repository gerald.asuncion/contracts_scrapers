import { AwilixContainer, asClass } from 'awilix';
import { createTestContainerWithBrowser } from '../../container/createTestContainer';
import FastwebVerificaCopertura from '../../../src/scraperv2/fastweb/FastwebVerificaCopertura';
import { Logger } from '../../../src/logger';
import { ONE_MINUTE } from '../../constants';
import runScraper from '../../container/runScraper';

const SCRAPER_NAME = 'fastwebVerficaCopertura';

type FastwebVerificaCoperturaContainerTest = {
  [SCRAPER_NAME]: FastwebVerificaCopertura,
  logger: Logger
};

describe('Fastweb Verifica Copertura', () => {
  let container: AwilixContainer<FastwebVerificaCoperturaContainerTest>;

  beforeAll(async () => {
    container = await createTestContainerWithBrowser((cnt: AwilixContainer<FastwebVerificaCoperturaContainerTest>) => {
      cnt.register(SCRAPER_NAME, asClass<FastwebVerificaCopertura>(FastwebVerificaCopertura).singleton());
    });
  });

  it('should OK', async () => {
    (await runScraper(container, SCRAPER_NAME, {
      "idDatiContratto": 666,
      "attivazioneComune": "lecco",
      "attivazioneIndirizzo": "via francesco petrarca",
      "attivazioneCivico": "19",
      "attivazioneCap": "23900",
      "attivazioneProvincia": "lc"
    })).toMatchObject({
      code: '01',
      details: 'Offerta attivabile in Fibra Misto Rame FTTN.'
    });
  }, ONE_MINUTE);
});
