import { AwilixContainer, asClass } from 'awilix';
import { createTestContainerWithBrowser } from '../../container/createTestContainer';
import FastwebVerificaMorosita from '../../../src/scraperv2/fastweb/FastwebVerificaMorosita';
import { Logger } from '../../../src/logger';
import { THREE_MINUTE } from '../../constants';
import runScraper from '../../container/runScraper';

const SCRAPER_NAME = 'fastwebVerficaMorosita';

type FastwebVerificaMorositaContainerTest = {
  [SCRAPER_NAME]: FastwebVerificaMorosita,
  logger: Logger
};

describe('Fastweb Verifica Morosita', () => {
  let container: AwilixContainer<FastwebVerificaMorositaContainerTest>;

  beforeAll(async () => {
    container = await createTestContainerWithBrowser((cnt: AwilixContainer<FastwebVerificaMorositaContainerTest>) => {
      cnt.register(SCRAPER_NAME, asClass<FastwebVerificaMorosita>(FastwebVerificaMorosita).singleton());
    });
  });

  it('should KO', async () => {
    (await runScraper(container, SCRAPER_NAME, {
      "idDatiContratto": 666,
      "attivazioneComune": "lecco",
      "attivazioneIndirizzo": "via francesco petrarca",
      "attivazioneCivico": "19",
      "attivazioneCap": "23900",
      "attivazioneProvincia": "lc",
      "nome": "vincenzo",
      "cognome": "ciliberti",
      "codiceFiscale": "CLBVCN77T22I158C",
      "sesso": "m",
      "dataNascita": "22/12/1977",
      "nascitaProvinciaEstera": "no",
      "nascitaStato": "italia",
      "nascitaComune": "san severo",
      "nascitaProvincia": "Foggia",
      "cellulare": "3460214383",
      "email": "multiservicepescara@libero.it"
    })).toMatchObject({
      code: '02',
      details: 'Attenzione, cliente non acquisibile'
    });
  }, THREE_MINUTE);
});
