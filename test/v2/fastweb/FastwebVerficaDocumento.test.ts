import { AwilixContainer, asClass } from 'awilix';
import { createTestContainerWithBrowser } from '../../container/createTestContainer';
import FastwebVerificaDocumento from '../../../src/scraperv2/fastweb/FastwebVerificaDocumento';
import { Logger } from '../../../src/logger';
import { THREE_MINUTE } from '../../constants';
import runScraper from '../../container/runScraper';

const SCRAPER_NAME = 'fastwebVerficaDocumento';

type FastwebVerificaDocumentoContainerTest = {
  [SCRAPER_NAME]: FastwebVerificaDocumento,
  logger: Logger
};

describe('Fastweb Verifica Documento', () => {
  let container: AwilixContainer<FastwebVerificaDocumentoContainerTest>;

  beforeAll(async () => {
    container = await createTestContainerWithBrowser((cnt: AwilixContainer<FastwebVerificaDocumentoContainerTest>) => {
      cnt.register(SCRAPER_NAME, asClass<FastwebVerificaDocumento>(FastwebVerificaDocumento).singleton());
    });
  });

  it('should KO', async () => {
    (await runScraper(container, SCRAPER_NAME, {
      "idDatiContratto": 666,
      "attivazioneComune": "lecco",
      "attivazioneIndirizzo": "via francesco petrarca",
      "attivazioneCivico": "19",
      "attivazioneCap": "23900",
      "attivazioneProvincia": "lc",
      "nome": "vincenzo",
      "cognome": "ciliberti",
      "codiceFiscale": "CLBVCN77T22I158C",
      "sesso": "m",
      "dataNascita": "22/12/1977",
      "nascitaProvinciaEstera": "no",
      "nascitaStato": "italia",
      "nascitaComune": "san severo",
      "nascitaProvincia": "Foggia",
      "cellulare": "3460214383",
      "email": "multiservicepescara@libero.it",
      "documentoTipo": "carta d'identità",
      "documentoNumero": "AV3716269",
      "documentoRilasciatoDa": "Mandello del Lario",
      "documentoEnteRilascio": "comune",
      "documentoDataRilascio": "07/03/2015",
      "documentoProvinciaRilascio": "lc"
    })).toMatchObject({
      code: '02',
      details: 'Attenzione, cliente non acquisibile'
    });
  }, THREE_MINUTE);
});
