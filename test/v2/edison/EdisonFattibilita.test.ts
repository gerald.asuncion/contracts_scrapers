import { AwilixContainer, asClass, asFunction } from 'awilix';
import createTestContainer from '../../container/createTestContainer';
import EdisonFattibilita from '../../../src/scraperv2/edison/EdisonFattibilita';
import EdisonBlacklistRepo from '../../../src/repo/EdisonBlacklistRepo';
import { Logger } from '../../../src/logger';
import { ONE_MINUTE } from '../../constants';
import runScraper from '../../container/runScraper';
import createTestMysql from '../../container/createTestMysql';

const SCRAPER_NAME = 'edisonFattibilita';

const MOCK_DB = [
  { comune: 'napoli', cap: '80126' }
];

const FAILURE_MESSAGE = "Il comune di attivazione risulta per Edison in blacklist, non è possibile quindi procedere alla sottoscrizione del contratto";

function findItems(args: Array<string>): Promise<any[]> {
  return new Promise((resolve, reject) => {
    const records = MOCK_DB.filter(({ comune, cap }) => {
      return comune.toLowerCase() === args[0].toLowerCase() && cap.toLowerCase() === args[1].toLowerCase()
    });
    resolve(records);
  });
}

type EdisonFattibilitaContainerTest = {
  [SCRAPER_NAME]: EdisonFattibilita,
  edisonBlacklistRepo: EdisonBlacklistRepo,
  logger: Logger
};

describe('Edison Fattibilita', () => {
  let container: AwilixContainer<EdisonFattibilitaContainerTest>;

  beforeAll(async () => {
    const mysqlFactory = createTestMysql({
      query: (sql, args) => findItems(args)
    });

    container = await createTestContainer((cnt: AwilixContainer<EdisonFattibilitaContainerTest>) => {
      cnt.register('mysqlPool', asFunction(mysqlFactory).singleton());
      cnt.register('edisonBlacklistRepo', asClass(EdisonBlacklistRepo).singleton());
      cnt.register(SCRAPER_NAME, asClass<EdisonFattibilita>(EdisonFattibilita).singleton());
    });
  });

  it('should OK', async () => {
    (await runScraper(container, SCRAPER_NAME, {
      "cap": "23826",
      "comune": "lecco"
    })).toHaveProperty('code', '01');
  }, ONE_MINUTE);

  it('should KO', async () => {
    (await runScraper(container, SCRAPER_NAME, {
      "cap": "80126",
      "comune": "napoli"
    })).toHaveProperty('code', '02');
  }, ONE_MINUTE);

  it(`should KO with message ${FAILURE_MESSAGE}`, async () => {
    (await runScraper(container, SCRAPER_NAME, {
      "cap": "80126",
      "comune": "napoli"
    })).toHaveProperty('details', FAILURE_MESSAGE);
  }, ONE_MINUTE);
});
