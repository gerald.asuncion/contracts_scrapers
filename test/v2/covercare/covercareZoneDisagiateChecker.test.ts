import { asClass, asFunction, AwilixContainer } from 'awilix';
import CovercareZoneDisagiateChecker from '../../../src/scraperv2/covercare/covercareZoneDisagiateChecker';
import createTestContainer from '../../container/createTestContainer';
import createTestMysql from '../../container/createTestMysql';
import { MysqlPoolInterface } from '../../../src/mysql';
import runScraper from '../../container/runScraper';
import FailureResponse from '../../../src/response/FailureResponse';
import SuccessResponse from '../../../src/response/SuccessResponse';

const SCRAPER_NAME = 'covercareZoneDisagiateChecker';

type CovercareZoneDisagiateContainerTest = {
  [SCRAPER_NAME]: CovercareZoneDisagiateChecker;
  'mysqlPool': MysqlPoolInterface;
};

const MOCK_DB = [
  { comune: 'Abbadia Lariana', cap: '23821', provincia: 'LC' },
  { comune: 'Mandello del Lario', cap: '23826', provincia: 'LC' }
];

describe('cover care zone disagiate', () => {
  let container: AwilixContainer<CovercareZoneDisagiateContainerTest>;

  let successResponse: SuccessResponse;
  let failureResponse: FailureResponse;

  beforeAll(() => {
    const mysqlFactory = createTestMysql({
      query: (sql, args) => Promise.resolve(MOCK_DB.filter((item) => item.cap === args[0]))
    });

    container = createTestContainer((cnt: AwilixContainer<CovercareZoneDisagiateContainerTest>) => {
      cnt.register('mysqlPool', asFunction(mysqlFactory).singleton());
      cnt.register(SCRAPER_NAME, asClass(CovercareZoneDisagiateChecker).singleton());
    });

    successResponse = new SuccessResponse('OK');
    failureResponse = new FailureResponse('KO');
  });

  it('should KO - cap in list', async () => {
    (await runScraper(container, SCRAPER_NAME, { comune: 'Mandello del Lario', cap: '23826' })).toEqual(failureResponse);
  });

  it('should OK - cap not in list', async () => {
    (await runScraper(container, SCRAPER_NAME, { comune: 'Monza', cap: '20900' })).toEqual(successResponse);
  });
});
