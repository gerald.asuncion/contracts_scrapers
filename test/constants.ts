export const HALF_MINUTE = 30000;

export const ONE_MINUTE = HALF_MINUTE * 2;

export const TWO_MINUTE = ONE_MINUTE * 2;

export const THREE_MINUTE = ONE_MINUTE * 3;
