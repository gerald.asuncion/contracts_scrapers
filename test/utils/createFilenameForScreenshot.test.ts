import createFilenameForScreenshot from '../../src/utils/createFilenameForScreenshot';
import { padStartDate } from '../../src/utils/pad';

function formatTimestamp(): string {
  const date = new Date();
  const first = [
    date.getFullYear(),
    padStartDate(date.getMonth()),
    padStartDate(date.getDate())
  ].join('');

  const second = [
    padStartDate(date.getHours()),
    padStartDate(date.getMinutes()),
    padStartDate(date.getSeconds())
  ].join('');

  // YYYYMMddHHmmss
  return `${first}${second}`;
}

describe('createFilenameForScreenshot', () => {
  it('senza idDatiContratto', () => {
    const res = `${formatTimestamp()}.prova`;
    expect(createFilenameForScreenshot('prova')).toEqual(res);
  });

  it('con idDatiContratto', () => {
    const idDatiContratto = 333;
    const res = `${idDatiContratto}.${formatTimestamp()}.prova`;
    expect(createFilenameForScreenshot('prova', idDatiContratto)).toEqual(res);
  });
});
