import * as ts from "typescript";
import { relative } from 'path';

// https://typescript-api-playground.glitch.me/#example=ts-type-checking-source
// https://www.npmjs.com/package/ts-morph
// https://learning-notes.mistermicheels.com/javascript/typescript/compiler-api/
// https://ts-ast-viewer.com/

export type SchemaTypeNode = {
  signatureType: 'default' | 'identifier' | 'query' | 'baseType' | 'literal' | 'mapped' | 'tuple' | 'union' | 'intersection' | 'operator' | 'index' | 'typeLiteral' | 'qualifiedName' | 'functionType';
  name?: string;
  type?: SchemaTypeIdentifierInfo;
  arguments?: SchemaTypeNode[];
  members?: SchemaMemberType[];
};

export type SchemaDocumentationInfo = {
  comment: string;
  tags: ts.JSDocTagInfo[];
}
export type SchemaSymbolType = {
  name: string;
  type: string;
  documentation: SchemaDocumentationInfo;
}
export type SchemaSignatureType = {
  parameters: SchemaSymbolType[];
  returnType: string;
  documentation: SchemaDocumentationInfo;
}

export type SchemaMemberType = {
  type: string;
  definition_effective?: string;
  definition?: SchemaTypeNode; // SchemaTypeNode[];
  name: string;
  modifiers: string[];
  isOptional: boolean;
  documentation: SchemaDocumentationInfo;
  signatures: SchemaSignatureType[];
  signatureIs: string;
}

export type SchemaTypePrameterInfo = {
  name: string;
  constraint: string;
  expression: string;
}

export type SchemaTypeIdentifierInfo = {
  name: string;
  file: string;
  sources?: string[];
  //hasMergingDeclaration: boolean;
  //hasModuleDeclaration: boolean;
  //hasInternalDeclaration: boolean;
  isExported: boolean;
  exportedName?: string;
}
/*export type IdentityInfo = TypeIdentifierInfo & {
  // isExported: boolean;
}*/

export type SchemaHeritageInfo<TIsClass> = TIsClass extends ts.InterfaceDeclaration ? {
  extends: SchemaTypeIdentifierInfo[];
} : {
  extends: SchemaTypeIdentifierInfo;
  implements: SchemaTypeIdentifierInfo[];
}

export type SchemaTypeInfo<T = unknown> = SchemaTypeIdentifierInfo & SchemaHeritageInfo<T> & {
  documentation: SchemaDocumentationInfo;
  exported: string;
  modifiers: string[];
  typeParameters?: SchemaTypePrameterInfo[];
  constructors?: SchemaSignatureType[];
  definition?: SchemaTypeNode;
  members?: SchemaMemberType[];
  signatureIs: 'class' | 'interface' | 'type';
}

/** Generate documentation for all classes in a set of .ts files */
export default function init(
  files: string[]
): SchemaTypeInfo[] {
  const output: SchemaTypeInfo[] = [];
  // Build a program using the set of root file names in fileNames
  const program = ts.createProgram(files, {
    noEmitOnError: true,
    noImplicitAny: true,
    target: ts.ScriptTarget.ES5,
    module: ts.ModuleKind.CommonJS
  });

  // Get the checker, we will use it to find more about classes
  const checker = program.getTypeChecker();

  // Visit every sourceFile in the program
  for (const sourceFile of program.getSourceFiles()) {
    if (!sourceFile.isDeclarationFile) {
      // Walk the tree to search for classes
      ts.forEachChild(sourceFile, visit);
    }
  }

  return output;

  /** visit nodes finding exported classes */
  function visit(node: ts.Node) {
    // Only consider exported nodes
    if (!isNodeExported(node)) {
      return;
    }

    /*
    node.name.escapedText => ContractPayload
    node.name.text => ContractPayload
    node.localSymbol.escapedName => ContractPayload
    node.localSymbol.exportSymbol.escapedName => default
    node.members[].name.text/escapedText => idDatiContratto
    */

    if (ts.isTypeAliasDeclaration(node)) {
      // console.log('------------ Payload TypeAlias:', node.name!.getText());

      let symbol = checker.getSymbolAtLocation(node.name)!;
      let typeInfo = serializeTypeAlias(symbol, node);

      output.push(typeInfo);
    } else if (ts.isInterfaceDeclaration(node) || ts.isClassDeclaration(node) && !!node.name) {
      // console.log('------------ Payload Construct:', node.name!.getText());

      let symbol = checker.getSymbolAtLocation(node.name!)!;
      let typeInfo = serializeConstruct(symbol, node);

      output.push(typeInfo);
    } else if (ts.isModuleDeclaration(node)) {
      // This is a namespace, visit its children
      ts.forEachChild(node, visit);
    } else {
      if (ts.isCallSignatureDeclaration(node)) {
        console.log('visit', node);
      }
    }
  }

  function serializeDocumentation(symbol: ts.Signature|ts.Symbol) {
    return {
      comment: ts.displayPartsToString(symbol!.getDocumentationComment(checker)),
      tags: symbol.getJsDocTags(),
    }
  }

  /** Serialize a symbol into a json object */
  function serializeSymbol(symbol: ts.Symbol, typeSymbol?: ts.Type): SchemaSymbolType {
    typeSymbol = typeSymbol || checker.getTypeOfSymbolAtLocation(symbol, symbol.valueDeclaration!);

    return {
      documentation: serializeDocumentation(symbol),
      name: symbol.getEscapedName().toString(),
      type: checker.typeToString(typeSymbol).replace(/"/g, "'")
    };
  }

  /** Serialize a signature (call or construct) */
  function serializeSignature(signature: ts.Signature): SchemaSignatureType {
    return {
      documentation: serializeDocumentation(signature),
      returnType: checker.typeToString(signature.getReturnType()).replace(/"/g, "'"),
      parameters: signature.parameters.map(parameter => serializeSymbol(parameter))
    };
  }

  function serializeMember(member: ts.Symbol): SchemaMemberType {
    const declaration = member.valueDeclaration || member.getDeclarations()![0]!;
    const typeSymbol = checker.getTypeAtLocation(declaration);
    const callSignatures = typeSymbol.getCallSignatures();

    const memberInfo: SchemaMemberType = {
      ...serializeSymbol(member!, typeSymbol),
      signatures: callSignatures.map(serializeSignature),
      modifiers: [],
      isOptional: false,
      signatureIs: ts.SyntaxKind[declaration.kind]
    };

    if (ts.isPropertySignature(declaration) || ts.isMethodSignature(declaration)) {
      memberInfo.isOptional = !!declaration.questionToken;

      if (declaration.type && ts.isUnionTypeNode(declaration.type)) {
        memberInfo.definition_effective = declaration.type.types
        .map(t => checker.typeToString(checker.getTypeAtLocation(t)))
        .map(text => text.replace(/"/g, "'"))
        .join(' | ');
        //memberInfo.definition = declaration.type.types.map(serializeTypeNode);
        memberInfo.definition = serializeTypeNode(declaration.type);
      }
    }

    return memberInfo;
  }

  function serializeType(node: ts.NamedDeclaration, symbol?: ts.Symbol): SchemaTypeIdentifierInfo {
    // checker.getAliasedSymbol(node.symbol);
    symbol = symbol || checker.getSymbolAtLocation(node);

    if (symbol) {
      if (ts.isIdentifier(node) && (symbol.flags & 2097152) !== 0) {
        symbol = checker.getAliasedSymbol(symbol);
      }
    } else {
      if (ts.isImportClause(node) || ts.isImportDeclaration(node) || ts.isImportSpecifier(node)) {
        symbol = checker.getAliasedSymbol(checker.getSymbolAtLocation(node.name))
      }
    }

    const source = node.getSourceFile(); // hcTypSymbol.getDeclarations()[0].getSourceFile().imports
    const file = relative(process.cwd(), source.fileName);
    const isExported = isNodeExported(node);

    let exportedName: string|undefined = undefined;
    let sources: (string[])|undefined = undefined;

    if (symbol) {
      exportedName = checker.getExportSymbolOfSymbol(symbol).getName();

      if (symbol.declarations) {
        sources = symbol.declarations
        .map(declaration => relative(process.cwd(), declaration.getSourceFile().fileName))
        // .filter(source => source !== file)
        ;

        if (sources.length === 0) {
          sources = undefined;
        }
      }
    } else {
      sources = [
        relative(process.cwd(), source.fileName)
      ];
    }

    return {
      // TODO: get imported symbols' source path
      // file: source.isDeclarationFile ? 'ISDECLARATIONFILE' : relative(process.cwd(), source.fileName),
      name: (node.name || node).getText(),
      file,
      ...sources && {
        sources
      },
      isExported,
      ...exportedName && {
        exportedName
      }
    };
  }

  function serializeClassLike(node: ts.ClassDeclaration | ts.InterfaceDeclaration) { // ts.ClassLikeDeclarationBase
    const result = { } as SchemaHeritageInfo<typeof node>;

    node.heritageClauses!
    .forEach(hc => {
      const isHcExtends = hc.token === ts.SyntaxKind.ExtendsKeyword;

      hc.types
      .map(hcType => {
        // hcType => expresisonwithtypearguments
        const hcTypSymbol = checker.getSymbolAtLocation(hcType.expression)!;
        const hcNode = hcTypSymbol.getDeclarations()![0] as ts.InterfaceDeclaration;

        const nodeId = serializeType(hcNode);
        // TODO: get definition source file
        if (isHcExtends) {
          if (ts.isInterfaceDeclaration(node)) {
            (result as any as SchemaHeritageInfo<typeof node>).extends = [
              ...(result as any as SchemaHeritageInfo<typeof node>).extends || [],
              nodeId
            ];
          } else {
            result.extends = nodeId;
          }
        } else {
          if (!ts.isInterfaceDeclaration(node)) {
            (result as any as SchemaHeritageInfo<typeof node>).implements = [
              ...(result as any as SchemaHeritageInfo<typeof node>).implements || [],
              nodeId
            ];
          }
        }
      })
    });

    return result;
  }

  function serializeTypeParameter(tp: ts.TypeParameterDeclaration): SchemaTypePrameterInfo {
    return {
      name: tp.getText(),
      constraint: tp.constraint ? tp.constraint.getFullText() : '',
      expression: tp.expression ? tp.expression.getFullText() : ''
    }
  }

  function signatureIs(node: ts.ClassDeclaration | ts.InterfaceDeclaration | ts.TypeAliasDeclaration) {
    return ts.isClassDeclaration(node) ? 'class' : ts.isInterfaceDeclaration(node) ? 'interface' : 'type';
  }

  function serializeAliased(symbol: ts.Symbol, node: ts.ClassDeclaration | ts.InterfaceDeclaration | ts.TypeAliasDeclaration): SchemaTypeInfo {
    const typeInfo: SchemaTypeInfo = {
      //...serializeIdentity(symbol, node),
      ...serializeType(node, symbol),
      signatureIs: signatureIs(node),
      documentation: serializeDocumentation(symbol),
      modifiers: node.modifiers?.map(m => m.getText()) || [],
      //constructors: [],
      // members: []
    } as any;

    if (typeInfo.signatureIs !== 'type') {
      typeInfo.members = [];
    }

    if (node.typeParameters) {
      typeInfo.typeParameters = node.typeParameters
      .map(serializeTypeParameter)
    }

    if (symbol.members) {
      symbol.members
      .forEach(member => {
        const declaration = member.valueDeclaration || member.getDeclarations()![0]!;

        if (ts.isPropertyDeclaration(declaration) || ts.isPropertySignature(declaration)) {
          typeInfo.members!.push(serializeMember(member));
        } else if (ts.isMethodDeclaration(declaration) || ts.isMethodSignature(declaration)) {
          /*
          const signature = checker.getSignatureFromDeclaration(declaration)!;
          const returnType = checker.getReturnTypeOfSignature(signature);
          const parameters = declaration.parameters; // array of Parameters
          // const docs = declaration.jsDoc; // array of js docs
          */

          typeInfo.members!.push(serializeMember(member));
        } else if (ts.isConstructorDeclaration(declaration)) {
          // console.log('isConstructorDeclaration');
        } else if (ts.isTypeParameterDeclaration(declaration)) {
          // console.log('isTypeParameterDeclaration');
        } else {
          console.log('default', declaration.kind, ts.SyntaxKind[declaration.kind]);
        }

        // typeInfo.members.push(serializeMember(member));
      });
    }

    return typeInfo;
  }

  function serializeTypeNode(ta: ts.TypeNode): SchemaTypeNode {
    if (ts.isToken(ta)) {
      return {
        signatureType: 'baseType',
        name: ts.SyntaxKind[ta.kind || 0].replace(/Keyword/, '').toLowerCase()
      };
    }

    if (ts.isLiteralTypeNode(ta)) {
      return {
        signatureType: 'literal',
        name: ta.getText().replace(/^'|'$/g, '')
      };
    }
    if (ts.isMappedTypeNode(ta)) {
      return {
        signatureType: 'mapped',
        name: ta.getText().replace(/^'|'$/g, '')
      };
    }
    if (ts.isTupleTypeNode(ta)) {
      return {
        signatureType: 'tuple',
        arguments: ta.elementTypes.map(serializeTypeNode)
      };
    }
    if (ts.isUnionTypeNode(ta)) {
      return {
        signatureType: 'union',
        arguments: ta.types.map(serializeTypeNode)
      };
    }
    if (ts.isIntersectionTypeNode(ta)) {
      return {
        signatureType: 'intersection',
        arguments: ta.types.map(serializeTypeNode)
      };
    }
    if (ts.isTypeOperatorNode(ta)) {
      return {
        signatureType: 'operator',
        name: ts.SyntaxKind[ta.operator || 0],
        arguments: [
          serializeTypeNode(ta.type)
        ]
      }
    }
    if (ts.isIndexedAccessTypeNode(ta)) {
      return {
        signatureType: 'index',
        arguments: [
          serializeTypeNode(ta.objectType),
          serializeTypeNode(ta.indexType)
        ]
      };
    }
    if (ts.isParenthesizedTypeNode(ta)) {
      return serializeTypeNode(ta.type);
    }
    // TODO: others 'isTypeXXXNode'

    if (ts.isTypeQueryNode(ta)) {
      return {
        signatureType: 'query',
        arguments: [
          serializeTypeEntityName(ta.exprName)
        ]
      }
    }
    if (ts.isTypeReferenceNode(ta)) {
      return serializeTypeReferenceNode(ta);
    }
    if (ts.isTypeLiteralNode(ta)) {
      return {
        signatureType: 'typeLiteral',
        // type: { },
        members: ta.members.map(tam => {
          const tamSymbol = checker.getSymbolAtLocation(tam.name!)!;
          return serializeMember(tamSymbol);
        })
      };
    }
    if (ts.isFunctionTypeNode(ta)) {
      return {
        signatureType: 'functionType',
        type: serializeTypeNode(ta.type).type,
        members: ta.parameters.map(tam => {
          const tamSymbol = checker.getSymbolAtLocation(tam.name!)!;
          return serializeMember(tamSymbol);
        })
      };
    }

    throw 'Not Implemented EXC!';
    const tamSymbol = checker.getSymbolAtLocation(ta)!;
    return {
      signatureType: 'default',
      members: [
        serializeMember(tamSymbol)
      ]
    };
  }

  function serializeTypeEntityName(typeName: ts.EntityName): SchemaTypeNode {
    if (ts.isIdentifier(typeName)) {
      const result = {
        signatureType: 'identifier',
        type: serializeType(typeName)
      } as SchemaTypeNode;

      return result;
    }

    // if (ts.isQualifiedName(typeName)) {
    return {
      signatureType: 'qualifiedName',
      arguments: [
        serializeTypeEntityName(typeName.left),
        serializeTypeEntityName(typeName.right)
      ]
    }
    // }
  }

  function serializeTypeReferenceNode(node: ts.TypeReferenceNode) {
    const {
      typeName,
      typeArguments,
    } = node;

    const result = serializeTypeEntityName(typeName);

    if (ts.isIdentifier(typeName)) {
      // LookForVisibleWithTextElementsResult

      if (typeArguments) {
        result.arguments = typeArguments
        .map(serializeTypeNode);
      }
    }

    return result;
  }

  function serializeTypeAlias(symbol: ts.Symbol, node: ts.TypeAliasDeclaration): SchemaTypeInfo {
    const payloadInfo = serializeAliased(symbol, node);

    payloadInfo.definition = serializeTypeNode(node.type);

    return payloadInfo;
  }

  function serializeConstruct(symbol: ts.Symbol, node: ts.ClassDeclaration | ts.InterfaceDeclaration): SchemaTypeInfo {
    const payloadInfo = serializeAliased(symbol, node);

    if (node.heritageClauses) {
      Object.assign(
        payloadInfo,
        serializeClassLike(node)
      )
    }

    if (ts.isClassDeclaration(node)) {
      payloadInfo.constructors = checker.getTypeOfSymbolAtLocation(
        symbol,
        symbol.valueDeclaration!
      )
      .getConstructSignatures()
      .map(serializeSignature);
    }

    return payloadInfo;
  }

  /** True if this is visible outside this file, false otherwise */
  function isNodeExported(node: ts.Node): boolean {
    return (
      (ts.getCombinedModifierFlags(node as ts.Declaration) & ts.ModifierFlags.Export) !== 0 ||
      (!!node.parent && node.parent.kind === ts.SyntaxKind.SourceFile)
    );
  }
}
