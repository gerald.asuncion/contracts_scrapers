import { flatten } from "lodash";
import { getTypeLiteralMembers } from "./helpers/literal";
import { SchemaMemberType, SchemaTypeNode, SchemaTypeInfo } from "../schema";
import { WikiMember, WikiType, WikiAggregatedType } from "./types";


function includeMember(member: SchemaMemberType): boolean {
  return !member.documentation.tags
  .find(({ name }) => name === '@ignore');
}

function aggregateTypes(stn: SchemaTypeNode, apexesOnLiterals = true, depth = 0): (string[])|string|null {
  switch(stn.signatureType) {
    case 'baseType':
      return stn.name!;
    case 'literal':
      return `${apexesOnLiterals ? "'" : ""}${stn.name}${apexesOnLiterals ? "'" : ""}`;
    case 'union':
    case 'tuple':
    case 'identifier':
      break;
    default:
      console.log(stn.signatureType);
      throw '';
  }

  const result = stn.arguments!
  .map(arg => aggregateTypes(arg, apexesOnLiterals, depth + 1))
  .filter(Boolean)
  .join(' | ');

  if (depth) {
    switch(stn.signatureType) {
      case 'union':
        return `(${result})`;
      case 'tuple':
        return `[${result}]`;
      case 'identifier':
        return `{${result}}`;
    }
  }

  return result;
}

function memberConverter(member: SchemaMemberType): WikiMember {
  const result = {
    title: member.name,
    type: member.type as any,
    value: undefined,
    required: !member.isOptional,
    description: undefined
  } as WikiMember;

  // docApiType
  member.documentation.tags.length && console.log(member.documentation.tags);

  if (member.documentation.comment) {
    result.description = member.documentation.comment.replace(/\r?\n|\r/g, ' ');
  }

  if (member.definition_effective) {
    result.value = member.definition_effective;
  }
  if (member.definition) {
    result.value = aggregateTypes(member.definition, false);
  }

  // docApi override

  const apiTags = member.documentation.tags
  .filter(({ name }) => name.indexOf('docApi') === 0)
  .reduce((prev, { name, text }) => ({
    ...prev,
    [name]: text.trim()
  }), {} as Record<string, string>);

  if (apiTags.docApiType === 'true') {
    result.type = member.definition_effective;
  } else if (apiTags.docApiType) {
    result.type = apiTags.docApiType;
  }

  return result;
}

function typeConverter(info: SchemaTypeInfo): WikiType {
  const result: WikiType = {
    name: info.name,
    fields: (info.members || [])
    .filter(includeMember)
    .reduce((fields, member) => ({
      ...fields,
      [member.name]: memberConverter(member)
    }), {})
  };

  if (info.documentation.comment) {
    result.documentation = info.documentation.comment;
  }

  return result;
}

export function aggregateFactory(aggregate: WikiAggregatedType): WikiType {
  const wikiType = typeConverter(aggregate.info);

  // get inherited chain


  // get members from inherited classes (not interfraces nor types)
  const inheritedMembers = flatten(
    aggregate.inherited
    .map(getTypeLiteralMembers)
  )
  .filter(includeMember);

  const ensambledMembers = flatten(
    aggregate.ensambled
    .map(getTypeLiteralMembers)
  )
  .filter(includeMember);

  // get members for classes ot TypeLiteral
  const ownMembers = getTypeLiteralMembers(aggregate.info);

  [
    ...inheritedMembers,
    ...ensambledMembers,
    ...ownMembers
  ]
  .forEach(memmber => {
    wikiType.fields[memmber.name] = memberConverter(memmber);
  })

  return wikiType;
}
