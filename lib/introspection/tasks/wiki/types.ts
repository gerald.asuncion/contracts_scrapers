import { SchemaTypeIdentifierInfo, SchemaTypeInfo } from "../schema"


export type WikiMember = {
  required: boolean;
  type: 'string' | 'number' | 'boolean' | 'Date' | 'null' | string;
  value?: any;
  title: string;
  description?: string;
}

export type WikiType = {
  name: string;
  documentation?: string;
  fields: {[key: string]: WikiMember}
}

export type WikiAggregatedType = {
  references: SchemaTypeIdentifierInfo[];
  info: SchemaTypeInfo;
  inherited: SchemaTypeInfo[];
  ensambled: SchemaTypeInfo[];
  // definitions: SchemaTypeIdentifierInfo[];
  // members: SchemaMemberType[];
  // returned: SchemaTypeInfo[];
}
