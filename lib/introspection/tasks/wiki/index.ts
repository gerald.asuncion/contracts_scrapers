import { flatten, uniq } from "lodash";
import { isAbsolute, relative } from "path";
import schemaInit, { SchemaTypeInfo } from "../schema/index";
import { aggregateFactory } from "./factory";
import { getEnsambledTypesChain } from "./helpers/ensamble";
import { getInheritedTypesChain } from "./helpers/inherited";
import { WikiAggregatedType } from "./types";


function isSubDir(parent: string, dir: string) {
  const rel = relative(parent, dir);
  return rel && !rel.startsWith('..') && !isAbsolute(rel);
}

function navigateTypeChain(typeInfo: SchemaTypeInfo, typeInfoList: SchemaTypeInfo[], result?: SchemaTypeInfo[]): SchemaTypeInfo[] {
  if (!result) {
    result = [typeInfo];
  }

  const inheritedClassesTypesChain = typeInfo.signatureIs === 'class' ?
    getInheritedTypesChain(typeInfo, typeInfoList, 'class') :
    getInheritedTypesChain(typeInfo, typeInfoList);
  const ensambledTypesChain = getEnsambledTypesChain(typeInfo, typeInfoList);

  const nestedResult = uniq([
    ...result,
    ...inheritedClassesTypesChain,
    ...ensambledTypesChain
  ]);

  if (nestedResult.length === result.length) {
    return result;
  }
  return flatten(
    [
      ...inheritedClassesTypesChain,
      ...ensambledTypesChain
    ]
    .map(typeInfo => navigateTypeChain(typeInfo, typeInfoList, nestedResult))
  );
}


function getAggregated(typeInfo: SchemaTypeInfo, typeInfoList: SchemaTypeInfo[]): WikiAggregatedType {
  // const allReferencedTypesList = getReferencedTypes(typeInfo, typeInfoList);

  const inheritedClassesTypesChain = (
    typeInfo.signatureIs === 'class' ?
      getInheritedTypesChain(typeInfo, typeInfoList, 'class') :
      getInheritedTypesChain(typeInfo, typeInfoList)
  )
  .filter(Boolean);
  const ensambledTypesChain = getEnsambledTypesChain(typeInfo, typeInfoList)
  .filter(Boolean);

  const inheritedClassesTypesTree = inheritedClassesTypesChain
  .map(ti => navigateTypeChain(ti, typeInfoList));
  const ensambledTypesChainTree = ensambledTypesChain
  .map(ti => navigateTypeChain(ti, typeInfoList));

  return {
    references: [], // allReferencedTypesList,
    info: typeInfo,
    inherited: uniq(flatten(inheritedClassesTypesTree)),
    ensambled: uniq(flatten(ensambledTypesChainTree))
  }
}

export default function init(
  files: string[]
): any {
  const fullTypesList: SchemaTypeInfo[] = schemaInit(files);

  const lookFor = {
    file: './src/scraperv2/',
    // name: /(?=.*payload)(?=.*fastweb)/i
    name: /(?=.*payload)/i
  } as {
    file: string;
    name?: RegExp
  }

  const wikiTypesList = fullTypesList
  .filter(typeOut => typeOut.file && isSubDir(lookFor.file, typeOut.file))
  .filter(typeOut =>
    !lookFor.name || lookFor.name!.test(typeOut.name) ||
    typeOut.exportedName && lookFor.name!.test(typeOut.exportedName)
  );

  const wikiAggregatedList = wikiTypesList
  .map(wt => getAggregated(wt, fullTypesList))
  .map(aggregateFactory);

  return wikiAggregatedList
  .reduce((res, eType) => ({
    ...res,
    [eType.name]: eType
  }), {});
}
