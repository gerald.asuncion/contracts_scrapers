import { flatten } from "lodash";
import { SchemaTypeIdentifierInfo, SchemaTypeInfo, SchemaTypeNode } from "../../schema";
import { getDefinitionTypeFromIdentifier } from "./identifier";


function getIdentifierDefinitions(definition: SchemaTypeNode, results: SchemaTypeIdentifierInfo[] = []): SchemaTypeIdentifierInfo[] {
  if (definition.signatureType === 'identifier') {
    results.push(definition.type as SchemaTypeIdentifierInfo);
  }
  if (definition.arguments) {
    definition.arguments.map(argument => getIdentifierDefinitions(argument, results));
  }

  return results;
}

export function getReferencedTypesByMembers(typeInfo: SchemaTypeInfo, typeInfoList: SchemaTypeInfo[]): SchemaTypeInfo[] {
  return flatten(
    (typeInfo.members || [])
    .filter(member => member.definition)
    .map((member) => getIdentifierDefinitions(member.definition))
  )
  .map(identifier => getDefinitionTypeFromIdentifier(identifier, typeInfoList));
}
