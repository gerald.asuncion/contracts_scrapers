import { flatten, uniq } from "lodash";
import { SchemaTypeInfo, SchemaTypeNode } from "../../schema";
import { getDefinitionTypeFromIdentifier } from "./identifier";


export function flattenSchemaTypeArguments(definition: SchemaTypeNode): SchemaTypeNode[] {
  return flatten(
    (definition?.arguments || [])
    .map(definitionArg => ([
      definitionArg,
      ...flattenSchemaTypeArguments(definitionArg),
    ]))
    .filter(Boolean)
  );
}

/**
 * Get union and intersection types/classes/interfaces
 * @param typeInfo
 * @param typeInfoList
 * @returns
 */
export function getEnsambledTypesChain(typeInfo: SchemaTypeInfo, typeInfoList: SchemaTypeInfo[]): SchemaTypeInfo[] {
  if (!typeInfo.definition) {
    return [];
  }

  if (typeInfo.definition.signatureType === 'identifier') {
    return [
      getDefinitionTypeFromIdentifier(typeInfo.definition.type!, typeInfoList)
    ];
  }

  return uniq(
    flatten(
      flattenSchemaTypeArguments(typeInfo.definition)
      .map(schemaType => schemaType.type)
      .filter(Boolean)
      .map(identifier => getDefinitionTypeFromIdentifier(identifier, typeInfoList))
      .filter(Boolean)
      .map(ensambled => [
        ensambled,
        ...getEnsambledTypesChain(ensambled, typeInfoList)
      ])
    )
  );
}
