import { flatten } from "lodash";
import { SchemaTypeIdentifierInfo, SchemaTypeInfo } from "../../schema";
import { getDefinitionTypeFromIdentifier } from "./identifier";


/**
 * Get directly inherited types
 * @param typeInfo
 * @returns
 */
 function getInheritedTypes(typeInfo: SchemaTypeInfo) {
  return [
    ...(Array.isArray(typeInfo.extends) ? typeInfo.extends as SchemaTypeIdentifierInfo[] : [ typeInfo.extends ]),
    ...(typeInfo.implements || [])
  ]
  .filter(Boolean);
}

/**
 * Get inherited types chain
 * @param typeInfo
 * @param typeInfoList
 * @returns
 */
export function getInheritedTypesChain(typeInfo: SchemaTypeInfo, typeInfoList: SchemaTypeInfo[], ...filterSignatures: SchemaTypeInfo['signatureIs'][]): SchemaTypeInfo[] {
  let extended = getInheritedTypes(typeInfo)
  .map(identifier => getDefinitionTypeFromIdentifier(identifier, typeInfoList));

  let nestedChain = flatten(
    extended
    .filter(Boolean)
    .map(typeInfo => [
      extended,
      ...getInheritedTypesChain(typeInfo, typeInfoList)
    ])
  );

  let result = flatten(nestedChain);

  if (filterSignatures.length > 0) {
    result = result
    .filter(stInfo => filterSignatures.includes(stInfo.signatureIs));
  }

  return result;
}
