import { SchemaTypeIdentifierInfo, SchemaTypeInfo } from "../../schema";


function typeEquals({ name, sources }: SchemaTypeIdentifierInfo, { name: otherName, sources: otherSources }: SchemaTypeIdentifierInfo): boolean {
  return name === otherName && [...sources].sort().join('|') == [...otherSources].sort().join('|');
}

export function getDefinitionTypeFromIdentifier(typeInfo: SchemaTypeIdentifierInfo, typeInfoList: SchemaTypeInfo[]): SchemaTypeInfo;
export function getDefinitionTypeFromIdentifier(typeInfo: SchemaTypeIdentifierInfo[], typeInfoList: SchemaTypeInfo[]): SchemaTypeInfo[];
export function getDefinitionTypeFromIdentifier(typeInfo: SchemaTypeIdentifierInfo[]|SchemaTypeIdentifierInfo, typeInfoList: SchemaTypeInfo[]): any {
  if (Array.isArray(typeInfo)) {
    return typeInfo.map(_typeInfo => typeInfoList.find((otherTypeInfo) => typeEquals(_typeInfo, otherTypeInfo)));
  }
  return typeInfoList.find((otherTypeInfo) => typeEquals(typeInfo, otherTypeInfo));
}
