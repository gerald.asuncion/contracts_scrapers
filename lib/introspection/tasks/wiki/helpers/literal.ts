import { flatten } from "lodash";
import { SchemaMemberType, SchemaTypeInfo } from "../../schema";
import { flattenSchemaTypeArguments } from "./ensamble";


/**
 * get members for classes ot TypeLiteral
 * @param typeInfo
 * @returns
 */
export function getTypeLiteralMembers(typeInfo: SchemaTypeInfo): SchemaMemberType[] {
  return [
    ...flatten([
      typeInfo.definition,
      ...flattenSchemaTypeArguments(typeInfo.definition)
    ]
    .map(definition => {
      if (definition && definition.signatureType === 'typeLiteral') {
        return definition.members || [];
      }
      return [];
    })),
    ...typeInfo.members || []
  ];
}
