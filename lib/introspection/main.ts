import { join, resolve } from 'path';
import { multiGlob, saveFile } from "./utils";


const [task] = process.argv.slice(2);

const options = {
  source: ["./src/**/*.ts"],
  outDir: resolve(__dirname, `../../output/${task}/`)
};

execute(task as any)
.catch(err => {
  console.error(err)
})
.then(() => console.log('end'));

export async function execute(
  task: 'schema' | 'wiki'
) {
  const {
    source,
    outDir
  } = options;

  // TODO: treat errors
  const files = await multiGlob(source);
  const output: any = require(`./tasks/${task}/index`).default(files);

  const now = Date.now();

  if (Array.isArray(output)) {
    await saveFile(join(outDir, `task-${now}.json`), output);
  } else {
    await Promise.all(
      Object.entries(output)
      .map(async ([name, item]) =>
        saveFile(join(outDir, now.toString(), `${name}.json`), item)
      )
    );
  }

  return output;
}
