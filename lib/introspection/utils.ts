import { join, dirname } from 'path';
import { promises as fs } from 'fs';
const { writeFile, mkdir } = fs;
const glob = require("glob");


export const singleGlob = (dir: string) => new Promise<string[]>((res, rej) =>
  glob(dir, function (er: any, files: string[]) {
    if (er) {
      rej(er);
      return;
    }
    console.log(`singleGlob
- dir:, '${dir}' ['${join(process.cwd(), dir)}']
- files #:, ${files.length}`);

    res(files);
  })
);

export const multiGlob = async (dirList: string[]) => (await Promise.all(
  dirList.map(singleGlob)
))
.reduce((list, fileList) => ([
  ...list,
  ...fileList.filter(file => !list.includes(file))
]));

export async function saveFile(filePath: string, data: any) {
  await mkdir(dirname(filePath), { recursive: true });
  return writeFile(filePath, JSON.stringify(data, undefined, 2));
}
