#!/bin/bash

set -e # exit on first error

while [ "$1" != "" ]; do
  case $1 in
    -b|--branch)
      shift
      BRANCH="$1"
      ;;
    --env)
      shift
      ENV="$1"
      ;;
  esac
  shift
done

echo "---------"
echo ""
echo "Execute migrations and seeders for env: ($ENV) on branch: $BRANCH."

if [ "$BRANCH" ]; then
  echo ""
  echo "Checkout del branch $BRANCH"
  git fetch -pv
  git reset --hard
  git checkout $BRANCH
  git reset --hard origin/$BRANCH
  git pull origin $BRANCH
  echo "Checkout terminato"
fi

if [ $? -ne 0 ]; then
  exit 1
fi

echo ""
echo "Eseguo le migrazioni"
yarn sequelize db:migrate --env $ENV
echo "migrazioni eseguite"

echo ""
echo "Terminata esecuzione migrazioni e seeders"
