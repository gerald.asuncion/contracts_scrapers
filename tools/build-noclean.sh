#!/bin/bash

set -e # exit on first error

BUILD_FOLDER="build"
TIMESTAMP="$(date +%s%3N)"

API_VERSION_FILE="api.version.txt"
TASKRUNNER_VERSION_FILE="taskrunner.version.txt"

ENV="development"

while [ "$1" != "" ]; do
  case $1 in
    -b|--branch)
      shift
      BRANCH="$1"
      ;;
    --env)
      shift
      ENV="$1"
      ;;
    --api)
      BUILD_API=true
      ;;
    --taskrunner)
      BUILD_TASKRUNNER=true
      ;;
    --all)
      BUILD_API=true
      BUILD_TASKRUNNER=true
      ;;
  esac
  shift
done

echo "Build ($ENV) branch: $BRANCH; build api: $BUILD_API; build task runner: $BUILD_TASKRUNNER"

if [ "$BRANCH" ]; then
  echo "Checkout del branch $BRANCH"
  git fetch -pv
  git reset --hard
  git checkout $BRANCH
  git reset --hard origin/$BRANCH
  git pull origin $BRANCH
  echo "Checkout terminato"
fi

echo "Installo tutte le dipendenze"
yarn install --network-timeout 600000 --frozen-lockfile

if [ $? -ne 0 ]; then
  exit 1
fi
echo "installazione dipendenze terminata"

echo "Rimuovo la cartella $BUILD_FOLDER"
if [ -d "$BUILD_FOLDER" ]; then rm -Rf $BUILD_FOLDER; fi
echo "Rimozione cartelle terminato"

if [ "$BUILD_API" = true ]; then
  echo "Effettuo la build di API"
  yarn tsc -p tsconfig.api.json
  echo "Build di API terminata"
fi

if [ "$BUILD_TASKRUNNER" = true ]; then
  echo "Effettuo la build di TASK RUNNER"
  yarn tsc -p tsconfig.taskrunner.json
  echo "Build di TASK RUNNER terminata"
fi

echo "Eseguo le migrazioni"
yarn sequelize db:migrate --env $ENV
echo "migrazioni eseguite"

if [ $? -ne 0 ]; then
  exit 1
fi
echo "Terminata installazione dipendenze"

if [ "$BUILD_API" = true ]; then
  echo "Creo il file $API_VERSION_FILE"
  echo "$TIMESTAMP" > $API_VERSION_FILE
fi

if [ "$BUILD_TASKRUNNER" = true ]; then
  echo "Creo il file $TASKRUNNER_VERSION_FILE"
  echo "$TIMESTAMP" > $TASKRUNNER_VERSION_FILE
fi
echo "$(date +%s%3N)" > $TASKRUNNER_VERSION_FILE
