## WIND

info:
openssl cms -cmsout -in smime.p7s -noout -print
openssl cms -cmsout -in smime.p7s -inform DER -noout -print
openssl pkcs12 -info -in keyStore.p12

openssl enc -aes-256-cbc -e -in smime.p7s -out out.p12 -k 8910CB1B4285D9E9
openssl enc -aes-128-cbc -e -in smime.p7s -out out.p12 -k 8910CB1B4285D9E9
openssl enc -sha-128-cbc -e -in smime.p7s -out out.p12 -k 8910CB1B4285D9E9
openssl enc -e -in smime.p7s -out WBSPRM03_12-01-2023.WIND.it.p12 -k 8910CB1B4285D9E9

openssl pkcs7 -inform DER -in smime.p7s -out out.p12 -outform DER -k 8910CB1B4285D9E9

openssl pkcs7 -print_certs -in smime.p7s -inform DER -out WBSPRM03_12-01-2023.WIND.it.p12

cat certificate.pem | openssl x509 -noout -enddate -inform DER
cat smime.p7s | openssl pkcs7 -noout -enddate -inform DER
