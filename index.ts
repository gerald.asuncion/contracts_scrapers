import container from './src/container';
import supermoneyScraperApi from './src/SupermoneyScraperApiServer';
import gracefulStartup from './src/graceful/gracefulStartup';
import GracefulShutdownController from './src/graceful/GracefulShutdownController';
import readFileVersioning from './src/readFileVersioning';

const { gracefulShutdownController, logger } = container.cradle;

const version = readFileVersioning('api.version.txt');

const server = supermoneyScraperApi.start();
server.timeout = 0;

function gracefulShutdown(signal: string) {
  logger.info(`API[pid:${process.pid}] - graceful shutdown by ${signal}`);
  (gracefulShutdownController as GracefulShutdownController).terminate();
  server.close(() => {
    logger.info(`API[pid:${process.pid}] - server closed`);
    process.exit(0);
  });
}

process.on('SIGTERM', gracefulShutdown);
process.on('SIGINT', gracefulShutdown);

logger.info(`API[pid:${process.pid}][v:${version.replace(/\n/g, '').trim()}] graceful startup - invio ready a pm2`);
gracefulStartup();
