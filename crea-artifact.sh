#!/bin/bash

set -e # exit on first error

ARTIFACT_DIST_FOLDER="artifact"
# ARTIFACT_FILENAME="scrapers.supermoney.zip"
ARTIFACT_FILENAME="scrapers.supermoney.tar.gz"
ARTIFACT_FILEPATH=$ARTIFACT_DIST_FOLDER/$ARTIFACT_FILENAME

if [ -d "$ARTIFACT_DIST_FOLDER" ]; then
  echo "cartella $ARTIFACT_DIST_FOLDER già presente"
  # rm /$ARTIFACT_DIST_FOLDER/*
  if [ -f "$ARTIFACT_FILEPATH" ]; then rm $ARTIFACT_FILEPATH; fi
else
  mkdir $ARTIFACT_DIST_FOLDER
fi

echo "Creo lo zip"
# zip -r $ARTIFACT_FILEPATH package.json build node_modules api.version.txt taskrunner.version.txt config ecosystem.config.js
tar -zcvf $ARTIFACT_FILEPATH package.json build node_modules api.version.txt taskrunner.version.txt config ecosystem.config.js
echo "fatto"
