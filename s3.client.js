const { S3Client, ListBucketsCommand, PutObjectCommand, ListBucketsCommandInput, ListObjectsCommand, ListObjectsCommandInput } = require('@aws-sdk/client-s3');
const path = require('path');
const fs = require('fs');

const config = {
  "bucket": "test-contracts.supermoney.eu",
  "userAccessId": "user_s3_test-contracts.supermoney.eu",
  "userAccessKeyId": "AKIAUSSZ7HGWQWXBOK4M",
  "userSecretAccessKEy": "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",
  "region": "eu-west-1"
};

function todayDate() {
  const today = new Date();
  const dd = String(today.getDate()).padStart(2, '0');
  const mm = String(today.getMonth() + 1).padStart(2, '0'); // January is 0!
  const yyyy = today.getFullYear();

  return `${yyyy}-${mm}-${dd}`;
}

function createS3Client({ userAccessKeyId, userSecretAccessKEy, region }) {
  const client = new S3Client({
    credentials: {
      accessKeyId: userAccessKeyId,
      secretAccessKey: userSecretAccessKEy
    },
    region
  });
  return client;
}

function createFilename(name, idDatiContratto /* optional */) {
  return [
    idDatiContratto || null,
    Date.now(),
    name
  ].filter(Boolean).join('.');
}

function createFileKey(filename) {
  const date = todayDate().split('-');

  return encodeURI(`scraper/screenshot/${date[0]}/${date[1]}/${date[2]}/${filename}.png`);
}

async function listBuckets(options /* optional */) {
  const client = createS3Client(config);

  let result;
  try {
    const command = new ListBucketsCommand(options || {});
    result = await client.send(command);
  } catch (error) {
    console.error(error);
    throw error;
  }

  client.destroy();

  return result;
}

async function listObjects(options) {
  const client = createS3Client(config);

  let result;
  try {
    const command = new ListObjectsCommand(options);
    result = await client.send(command);
  } catch (error) {
    console.error(error);
    throw error;
  }

  client.destroy();

  return result;
}

async function saveImage(folder = '20210917', filename = 'ItalgasContendibilitaGas-errore.png') {
  const client = createS3Client(config);

  let result;
  try {
    const fileKey = createFileKey(createFilename(filename.split('.')[0], '7999'));
    const file = fs.createReadStream(path.join(__dirname, 'output', folder, filename));

    const params = { Bucket: config.bucket, Key: fileKey, Body: file };

    console.log('params: ', params);
    const command = new PutObjectCommand(params);
    result = await client.send(command);
  } catch (error) {
    console.error(error);
    throw error;
  }

  client.destroy();

  return result;
}

(async () => {
  try {
    // const result = await listBuckets();
    // const result = await saveImage();
    // const result = await listObjects({ Delimiter: "/", Bucket: config.bucket });
    const result = await listObjects({ Prefix: encodeURIComponent('scraper') + '/', Bucket: config.bucket });
    console.log(`Saved files under ${config.bucket}:\n`, (result.Contents || []).map(content => content.Key).join('\n'));
  } catch (ex) {
    console.error(`AWS ERR: ${ex.message}`);
  }
})();
