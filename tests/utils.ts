import {Browser, launch} from "puppeteer";
import {MysqlPoolInterface} from "../src/mysql";

export const createTestBrowser = () => launch({
    headless: false,
    slowMo: 25,
    devtools: false,
    ignoreHTTPSErrors: true,
    // args: ['--start-fullscreen'],
    defaultViewport: { //--window-size in args
        width: 1280,
        height: 882
    }
});

export function createTestMysql(): MysqlPoolInterface {
    return {
        query: jest.fn(),
        queryAll: jest.fn(),
        queryOne: jest.fn(),
        truncateTable: jest.fn(),
        insertIntoChunk: jest.fn()
    }
};

export function buildScraper(browser: Promise<Browser>) {
    return (builder: any) => async (args: any, expectedResponse: { success: boolean, results?: string[] }) => {

        const scraper = await builder({browser});

        const result = await scraper.scrape(args);

        expect(result).toEqual(expectedResponse)
    };
}

export const successResponse = {success: true};
export const failedResponse = {success: false};
