import {Browser, launch} from "puppeteer";
import iretiEmiliaScraper from "../src/scrapers/ireti/ireti-emilia-scraper";


describe('Ireti Emilia Scraper',  () => {
    let browser: Promise<Browser>;
    const openBrowser = () => launch({
        headless: false,
        slowMo:50,
        defaultViewport: { //--window-size in args
            width: 1880,
            height: 882
        },
        ignoreHTTPSErrors: true
    });

    browser = openBrowser();
    beforeAll(() => {
    });

    afterAll(() => {
       browser.then(b => b.close());
    });


    const successResponse = {success: true};
    const failedResponse = {success: false};

    const notValidPdr = '00881106493602';

    const checkIretiEmiliaScraper = async (pdr:string , expectedResponse:{success: boolean}) => {

        const scraper = await iretiEmiliaScraper({browser});

        const result = await scraper.scrape({ pdr} );

        expect(result).toEqual(expectedResponse)
    };

    it('should return a failure with invented pdr', async () => checkIretiEmiliaScraper(notValidPdr, failedResponse), 50000);
});
