import {buildScraper, createTestBrowser, failedResponse} from "./utils";
import teaSeiScraper from "../src/scrapers/tea-sei/tea-sei-scraper";


describe('Tea Sei Scraper', () => {

    const browser = createTestBrowser();


    const checkScraper = buildScraper(browser)(teaSeiScraper);

    beforeAll(() => {

    });

    afterAll(() => {
        browser.then(b => b.close());
    });


    const inventedPdr1 = {pdr:'12345678901234'};
    const inventedPdr2 = {pdr:'09876543219876'};

    const validPod = 'IT001E89269404';


    it('should return a failure for invented pdr 1', async () => checkScraper(inventedPdr1, failedResponse), 60000);
    it('should return a failure for invented pdr 2', async () => checkScraper(inventedPdr2, failedResponse), 60000);



});
