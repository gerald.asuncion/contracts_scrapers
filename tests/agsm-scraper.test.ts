import {Browser} from "puppeteer";
import agsmLuceScraper from '../src/scrapers/agsm/agsm-luce-scraper'
import agsmGasScraper from '../src/scrapers/agsm/agsm-gas-scraper'
import {buildScraper, createTestBrowser} from "./utils";

describe('Agsm Scraper', () => {
  let browser: Promise<Browser> = createTestBrowser();

  const checkGasScraper = buildScraper(browser)(agsmGasScraper);
  const checkLuceScraper = buildScraper(browser)(agsmLuceScraper);

  const successResponse = {success: true};
  const failedResponse = {success: false};

  const inventedPdr1 = 'IT012E00210022';
  const inventedPdr2 = 'IT001E89269404';

  const validPod = 'IT001E89269404';

  it('should return a failure for invented pod 1', async () => checkLuceScraper({pod:inventedPdr2}, failedResponse), 50000);
  it('should return a failure for invented pdr 1', async () => checkGasScraper({pdr:inventedPdr1}, failedResponse), 50000);
  it('should return a failure for invented pdr 2', async () => checkGasScraper({pdr:inventedPdr2}, failedResponse), 50000);
  it('should return a failure for invented pod 2', async () => checkLuceScraper({pod:inventedPdr2}, failedResponse), 50000);
});
