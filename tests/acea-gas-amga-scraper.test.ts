import {Browser} from "puppeteer";
import scraper from "../src/scrapers/terranova/acea-gas-aps-amga-scraper";
import {buildScraper, createTestBrowser} from "./utils";

describe('Acea gas amga Scraper', () => {
  let browser: Promise<Browser> = createTestBrowser();

  const checkScraper = buildScraper(browser)(scraper);

  const successResponse = {success: true};
  const failedResponse = {success: false};

  const inventedPdr1 = 'IT012E00210022';
  const inventedPdr2 = 'IT001E89269404';

  const validPod = 'IT001E89269404';

  it('should return a failure for invented pdr 1', async () => checkScraper({pdr:inventedPdr1}, failedResponse), 50000);
  it('should return a failure for invented pdr 2', async () => checkScraper({pdr:inventedPdr2}, failedResponse), 50000);
});
