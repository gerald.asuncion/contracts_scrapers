import unaretiScraper from "../src/scrapers/unareti/unareti-scraper";
import {createTestMysql} from "./utils";

describe('Unareti scraper', () => {
    const successResponse = {success: true};

    it('should return a success', async () => {
        const mysqlPool = createTestMysql();
        const scraper = unaretiScraper({mysqlPool});
        const result = await scraper.scrape({
            pdr: '00102400000004'
        });
        expect(mysqlPool.queryOne).toBeCalled();
        expect(mysqlPool.queryOne).toBeCalledWith('SELECT * FROM unareti WHERE `Codice PdR` = ? AND `Stato del punto` = ?', ['00102400000004','DISATTIVATO']);
    }, 15000);
});
