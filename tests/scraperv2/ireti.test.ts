import {createTestBrowser} from "../utils";
import Ireti from "../../src/scraperv2/ireti";

const pods = [
  'IT012E00210022',
  'IT001E93683609',
  'IT001E17909789',
  'IT020E00127776',
  'IT001E91771882',
]

describe("IRETI scraper", () => {
    it("should success", async () => {
      const browser = createTestBrowser();
      const logger = console;
      const responses = pods.map(async pod => {
        // @ts-ignore
        const ireti = new Ireti({browser, logger});
        return  await ireti.scrape({pod})
      });
      console.log(await Promise.all(responses));
    }, 5000000)
});
