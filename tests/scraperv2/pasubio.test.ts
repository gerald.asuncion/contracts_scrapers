import {createTestBrowser} from "../utils";
import Pasubio from "../../src/scraperv2/pasubio";

describe("Pasubio scraper", () => {
    it("should success", async () => {
        const browser = createTestBrowser();
        const logger = console;
        // @ts-ignore
        const ned = new Pasubio({
            browser,
            logger
        });

       const res = await ned.scrape({pdr: '00778844552'});

       console.log(res);
    }, 50000)
});
