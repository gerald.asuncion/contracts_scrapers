import {createTestBrowser} from "../utils";
import Italgas from "../../src/scraperv2/italgas/italgas";

const pdrs = [
    '00880001672356',
    '00881106493602',
]

describe("Italgas scraper", () => {
    it("should success", async () => {
        const browser = createTestBrowser();

        const logger = console;
        const responses = pdrs.map(async pdr => {
            // @ts-ignore
            const italgas = new Italgas({browser, logger});
            return  await italgas.scrape({pdr})
        });

        console.log(await Promise.all(responses));
    }, 5000000)
});
