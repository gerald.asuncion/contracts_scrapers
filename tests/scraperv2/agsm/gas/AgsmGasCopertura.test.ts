import { asClass, asFunction, AwilixContainer } from "awilix";
import { MysqlPoolInterface } from "../../../../src/mysql";
import { Scraper } from "../../../../src/scraperv2/types";
import AgsmGasCopertura from "../../../../src/scraperv2/agsm/gas/AgsmGasCopertura";
import createTestContainer from "../../utils/createTestContainer";
import createTestLogger from "../../utils/createTestLogger";
import SuccessResponse from "../../../../src/response/SuccessResponse";
import FailureResponse from "../../../../src/response/FailureResponse";
import { ScraperResponse } from "../../../../src/scraperv2/scraper";

const MOCK_DB = [
  { comune: "Abbadia Lariana", provincia: "LC", subentro: "SI", switch: "NO" },
  { comune: "Mandello del Lario", provincia: "LC", subentro: "NO", switch: "SI" }
];

function findItem(args: Array<string>) {
  return new Promise((resolve, reject) => {
    const record = MOCK_DB.find(item => item.comune === args[0] && item.provincia === args[1]);
    if(!record) {
      reject('no result');
    }

    resolve(record);
  });
}

function createTestMysql(): MysqlPoolInterface {
  return {
    query          : jest.fn(),
    queryAll       : jest.fn(),
    queryOne       : jest.fn((sql, args) => findItem(args as any)),
    truncateTable  : jest.fn(),
    insertIntoChunk: jest.fn()
  }
};

describe("AgsmGasCopertura", () => {
    let container: AwilixContainer<any>;

    let successResponse: SuccessResponse;

    beforeAll(() => {
      container = createTestContainer((cnt: AwilixContainer<any>) => {
        cnt.register("agsmGasCopertura", asClass(AgsmGasCopertura).singleton());
        cnt.register("mysqlPool", asFunction(createTestMysql).singleton());
        cnt.register("logger", asFunction(createTestLogger).singleton());
      });

      successResponse = new SuccessResponse("OK");
    });

    function getScraper(): Scraper {
      return container.resolve("agsmGasCopertura");
    }

    it('should KO - comune non in lista', async () => {
      const comune = "PPP";
      const resp: ScraperResponse = await getScraper().scrape({ comune, provincia: "LC", tipologia: "subentro" });
      expect(resp).toEqual(new FailureResponse(`il Comune "${comune}" non è coperto dalla fornitura gas di Agsm`));
    });

    it('should KO - comune non in lista (provincia errata)', async () => {
      const comune = "Abbadia Lariana";
      const resp: ScraperResponse = await getScraper().scrape({ comune, provincia: "BG", tipologia: "subentro" });
      expect(resp).toEqual(new FailureResponse(`il Comune "${comune}" non è coperto dalla fornitura gas di Agsm`));
    });

    it('should KO - non è subentrabile', async () => {
      const comune = "Mandello del Lario";
      const resp: ScraperResponse = await getScraper().scrape({ comune, provincia: "LC", tipologia: "subentro" });
      expect(resp).toEqual(new FailureResponse(`il Comune "${comune}" non è coperto dalla fornitura gas di Agsm`));
    });

    it('should KO - non è switchabile', async () => {
      const comune = "Abbadia Lariana";
      const resp: ScraperResponse = await getScraper().scrape({ comune, provincia: "LC", tipologia: "switch" });
      expect(resp).toEqual(new FailureResponse(`il Comune "${comune}" non è coperto dalla fornitura gas di Agsm`));
    });

    it('should OK - subentrabile', async () => {
      const resp: ScraperResponse = await getScraper().scrape({ comune: "Abbadia Lariana", provincia: "LC", tipologia: "subentro" });
      expect(resp).toEqual(successResponse);
    });

    it('should OK - switchabile', async () => {
      const resp: ScraperResponse = await getScraper().scrape({ comune: "Mandello del Lario", provincia: "LC", tipologia: "switch" });
      expect(resp).toEqual(successResponse);
    });
});
