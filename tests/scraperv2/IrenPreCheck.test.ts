import {createTestBrowser} from "../utils";
import IrenPreCheck from "../../src/scraperv2/iren/IrenPreCheck";

describe("IrenPreCheck scraper luce", () => {
    it("should success", async () => {
        const browser = createTestBrowser();

        const logger = console;

        const scraper = new IrenPreCheck({browser, logger});

        const res = await scraper.scrape({
          fornitura: 'Gas',
          codice: '12345678901234',
          nome: 'Test',
          cognome: 'test',
          codiceFiscale: 'TSTTST80A01Z112R'
        })

        console.log(res);
    }, 5000000)
});
