import { AwilixContainer, createContainer } from "awilix";
export default function createTestContainer(register?: Function): AwilixContainer<any> {
    const container = createContainer();

    if (register) {
      register(container);
    }

    return container;
}
