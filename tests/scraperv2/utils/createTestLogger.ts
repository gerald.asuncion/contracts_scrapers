import config from "config";
import winston, { createLogger, transports, format } from "winston";

const { combine, timestamp, label, printf/* , prettyPrint */ } = format;

const myFormat = printf(({ level, message, timestamp, componentName }) => {
  return `${componentName ? `${componentName} | ` : ''}${level.toUpperCase()} | ${timestamp} | ${message}`;
});

export type Logger = winston.Logger;

export default function createTestLogger() {
  return createLogger({
      format: combine(
        label({ label: 'SM-SCRAPERS-TEST' }),
        timestamp({ format: 'YYYY-MM-dd HH:mm:ss' }),
        myFormat
      ),
      transports: [new transports.File({ filename: config.get('loggerConfig.path') + 'test.log' })],
  });
}
