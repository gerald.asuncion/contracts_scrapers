import {createTestBrowser} from "../utils";
import ItalgasAttivazione from "../../src/scraperv2/italgas/italgas-attivazione";

const pdrs = [
  '00880001672356',
  '00881106493602',
]

describe("Italgas Nuova Attivazione scraper", () => {
    it("should success", async () => {
      const browser = createTestBrowser();
      const logger = console;
      const responses = pdrs.map(async pdr => {
        // @ts-ignore
        const italgasAttivazione = new ItalgasAttivazione({browser, logger});
        return  await italgasAttivazione.scrape({pdr})
      });
      console.log(await Promise.all(responses));
    }, 5000000)
});
