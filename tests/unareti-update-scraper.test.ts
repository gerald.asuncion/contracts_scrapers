import {createTestBrowser, createTestMysql, failedResponse, successResponse} from "./utils";
import unaretiUpdateScraper from "../src/scrapers/unareti/unareti-update-scraper";
import {logger} from "../src/logger";


describe('Unareti Update scraper', () => {
    const toItTeam = 'emiliano.tuzia@blastingnews.com';
    const baseDownloadPath = './output';
    const browser = createTestBrowser();
    const mailer = {
        send:jest.fn()
    };

    beforeAll(() => {

    });

    afterAll(() => {
        browser.then(b => b.close());
    });

    it('should return a success for imported POD file', async () => {
        const mysqlPool = createTestMysql();
        const scraper = unaretiUpdateScraper({browser, mailer, mysqlPool, logger, baseDownloadPath, toItTeam});
        const result = await scraper.scrape('');
        expect(result).toEqual(successResponse)
    }, 15000);
});
