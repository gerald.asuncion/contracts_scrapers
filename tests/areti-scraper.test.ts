import aretiScraper from "../src/scrapers/areti/areti-scraper";
import {createTestMysql} from "./utils";

describe('Areti scraper', () => {
    it('should return a success', async () => {
        const mysqlPool = createTestMysql();
        const scraper = aretiScraper({mysqlPool});
        const result = await scraper.scrape({
            pod: 'IT002E0000330A'
        });
        expect(mysqlPool.queryOne).toBeCalled();
        expect(mysqlPool.queryOne).toBeCalledWith('SELECT * FROM areti WHERE `pod` = ? AND `contendibile` = ?', ['IT002E0000330A',1]);
    }, 15000);
});
