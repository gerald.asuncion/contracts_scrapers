import {ComuniRepo} from "../src/repo/ComuniRepo";

describe('Comuni Repo', () => {

    const repo = new ComuniRepo();

    it('should reject promise on fake comune', async () => {
        const fakeComune = 'FakeComune';

        try{
            const comune = await repo.findComune(fakeComune);
            expect(false).toBe(true)
        }catch (e) {
            expect(true).toBe(true)
        }
    });
    it('should resolve promise on real comune', async () => {
        const fakeComune = 'FakeComune';

        try{

            const comune = await repo.findComune('Milano');
            expect(true).toBe(true)
        }catch (e) {
            expect(false).toBe(true)
        }
    });
    it('should reject promise on fake cap', async () => {
        const fakeCap = '00000';

        try{

            const comune = await repo.findComuneByCap(fakeCap);
            console.log(comune);
            expect(false).toBe(true)
        }catch (e) {
            expect(true).toBe(true)
        }
    });

    it('should resolve promise on real cap', async () => {

        try{

            const comune = await repo.findComuneByCap('00054');
            console.log(comune);
            expect(true).toBe(true)
        }catch (e) {
            expect(false).toBe(true)
        }
    })

});
