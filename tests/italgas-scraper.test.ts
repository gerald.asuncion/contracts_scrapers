import {Browser, launch} from "puppeteer";
import italgasScraper from "../src/scrapers/italgas/italgas-scraper";
import {failedResponse, successResponse} from "./utils";


describe('Italgas Scraper',  () => {
    let browser: Promise<Browser>;
    const openBrowser = () => launch({
        headless: false,
        slowMo:50,
        defaultViewport: { //--window-size in args
            width: 1280,
            height: 882
        }
    });

    browser = openBrowser();
    beforeAll(() => {
    });

    afterAll(() => {
       browser.then(b => b.close());
    });



    const inventedPdr = '12345678901234';
    const notAvailablePdr = '00881106493602';
    const validPdr = '00881103022099';


    const checkItalgasScraper = async (pdr:string , expectedResponse:{success: boolean}) => {

        const scraper = await italgasScraper({browser});

        const result = await scraper.scrape({ pdr} );

        expect(result).toEqual(expectedResponse)
    };


    it('should return a failure with invented pdr', async () => checkItalgasScraper(inventedPdr, failedResponse), 50000);
    it('should return a failure with not available pdr ', async () => checkItalgasScraper(notAvailablePdr, failedResponse), 50000);
    it('should return a success for available pdr ', async () => checkItalgasScraper(validPdr, successResponse), 50000);


});
