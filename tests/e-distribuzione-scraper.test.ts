import scraper from "../src/scrapers/e-distribuzione/e-distribuzione-scraper";
import {buildScraper, createTestBrowser} from "./utils";
import {Browser} from "puppeteer";

describe('E distribuzione', () => {

    let browser: Promise<Browser> = createTestBrowser();

    beforeAll(() => {

    });

    afterAll(() => {
        browser.then(b => b.close());
    });
    const checkScraper = buildScraper(browser)(scraper);

    const successResponse = {success: true};
    const failedResponse = {success: false};

    const inventedPdr1 = 'IT012E00210022';


    const validPod = 'IT001E93683609';


    it('should return a failure for invented pod 1', async () => checkScraper({pod: inventedPdr1}, failedResponse), 50000);
    it('should return a success for valid pod ', async () => checkScraper({pod: validPod}, successResponse), 50000);

    it('should return a failure for invented pod 2', async () => checkScraper({pod: inventedPdr1}, failedResponse), 50000);
    it('should return a success for valid pod 2', async () => checkScraper({pod: validPod}, successResponse), 50000);

})
