import {Browser, launch} from "puppeteer";
import iretiScraper from "../src/scrapers/ireti/ireti-scraper";


describe('Ireti Scraper', () => {
    let browser: Promise<Browser>;
    const openBrowser = () => launch({
        headless: false,
        slowMo: 50,
        defaultViewport: { //--window-size in args
            width: 1280,
            height: 882
        }
    });
    browser = openBrowser();
    beforeAll(() => {

    });

    afterAll(() => {
        browser.then(b => b.close());
    });


    const successResponse = {success: true};
    const failedResponse = {success: false};

    const inventedPod1 = 'IT012E00210022';
    const inventedPod2 = 'IT001E89269404';

    const validPod = 'IT001E89269404';


    const checkIretiScraper = async (pod: string, expectedResponse: { success: boolean }) => {

        const scraper = await iretiScraper({browser});

        const result = await scraper.scrape({pod});

        expect(result).toEqual(expectedResponse)
    };


    const pods = [
        'IT012E00210022',
        'IT001E89269404',
        'IT001E28473123',
        'IT001E39100957',
        'IT001E61212068',
        'IT020E00483592',
        'IT001E16435545',
        'IT001E98393293',
        'IT001E04027093',
        'IT001E01450317',
        'IT001E83697672',
        'IT001E31624908',
        'IT001E73969526',
        'IT002E2593067A',
        'IT001E80298125',
        'IT006E00295249',
        'IT001E25733473',
        'IT001E54556278',
        'IT001E56299563',
        'IT001E90774921',
        'IT002E5670086A',
        'IT001E56685729',
        'IT001E10351220',
        'IT020E00688001',
        'IT001E22519518',
        'IT002E9008653A',
        'IT001E84244070',
        'IT001E94315612',
        'IT001E91303108',
        'IT001E91369958',
        'IT001E46894274',
        'IT001E91127994',
        'IT012E00455152',
        'IT001E22882182',
        'IT001E85047940',
        'IT001E78234234',
        'IT020E00496812',
        'IT001E68446477',
        'IT001E73315878',
        'IT001E70299557',
        'IT001E70255169',
        'IT001E93683609',
        'IT001E17909789',
        'IT020E00127776',
        'IT001E91771882',
        'IT001E53381204',
        'IT001E80227433',
        'IT001E98937373',
        'IT001E79155101',
        'IT001E76045634',
        'IT013E00016277',
    ]



    it('should return a failure for invented pod 1', async () => checkIretiScraper(inventedPod1, failedResponse), 50000);
    it('should return a failure for invented pod 2', async () => checkIretiScraper(inventedPod2, failedResponse), 50000);


});
