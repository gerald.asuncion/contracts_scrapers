const concat = (x, y) =>
    x.concat(y)

const flatMap = (f, xs) =>
    xs.map(f).reduce(concat, [])

Array.prototype.flatMap = function (f) {
    return flatMap(f, this)
}

describe("flatmap", () => {
    it("should return an empty array", () => {
        const arr = [];


        const result = arr.flatMap(a => [a]);
        expect(result).toEqual([]);
    })

    it("should return a flat array", () => {

        const a = [1, 2, 3, 4]

        const result = a.flatMap(v => [v, v + 1, v * 2])

        expect(result).toEqual([1, 2, 2, 2, 3, 4, 3, 4, 6, 4, 5, 8])
        console.log(result)
    })
})
