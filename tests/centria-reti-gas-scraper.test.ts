import {Browser, launch} from "puppeteer";
import centriaRetiGasScraper from "../src/scrapers/ireti/centria-reti-gas-scraper";
import {failedResponse, successResponse} from "./utils";

describe('Centria Reti Gas Scraper',  () => {
    let browser: Promise<Browser>;
    const openBrowser = () => launch({
        headless: false,
        slowMo:10,
        defaultViewport: { //--window-size in args
            width: 1280,
            height: 882
        }
    });

    browser = openBrowser();
    beforeAll(() => {
    });

    afterAll(() => {
       browser.then(b => b.close());
    });



    const inventedPdr = '12345678901234';
    const notAvailablePdr = '00881106493602';
    const validPdr = '00881103022099';


    const checkScraper = async (pdr:string , expectedResponse:{success: boolean}) => {
        const scraper = await centriaRetiGasScraper({browser});
        const result = await scraper.scrape({ pdr} );
        expect(result).toEqual(expectedResponse)
    };


    it('should return a failure with invented pdr', async () => checkScraper(inventedPdr, failedResponse), 50000);
    it('should return a failure with not available pdr ', async () => checkScraper(notAvailablePdr, failedResponse), 50000);
});
