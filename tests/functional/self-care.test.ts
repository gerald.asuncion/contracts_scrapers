import {createTestBrowser} from "../utils";
import {Browser} from "puppeteer";

const baseData = {
    sm:{
        nome: 'yuri',
        cognome: 'passamonti',
        cellulare: '3333333331',
        email: 'yuri.passamonti@blastingnews.com'
    },
    contracts: {
        pdr: '00881103022099',
        tipologiaPdr: 'domestico',
        dataNascita: '09111981',
        sesso: 'M',
        codiceFiscale: 'PSSYRU81S09H501C',
        comuneNascita: 'roma',
        attivazioneVia: 'via fasulla',
        attivazioneCivico: '123',
        attivazioneInterno: 'A1',
        attivazioneCitta: 'roma',
        cellulare: '3336352787',
        documentoTipo: 'patente',
        documentoNumero: 'AA123456AA',
        documentoData: '10102010',
        documentoRilasciato: 'Re',
        paymentMethod: 'bollettino',
        residenza: 'residente',
        occupazione: 'proprietario',
    }
};

async function compileSelfCare(browser: Promise<Browser>, data: any, luce:boolean, gas: boolean):Promise<void> {
    const b = await browser;
    const page = await b.newPage();
    if(luce){
        await page.goto('https://test.energia.supermoney.eu/energia-elettrica/?secondLanded');
    }else{
        await page.goto('https://test.energia.supermoney.eu/gas-riscaldamento/?secondLanded');
    }
    await page.waitForSelector('.js-nome');
    await page.click('label[for="not-active"]');
    await page.type('.form-group--desktop .js-nome', data.sm.nome);
    await page.type('.form-group--desktop .form-input.js-cognome', data.sm.cognome);
    await page.type('.form-group--desktop .js-cellulare', data.sm.cellulare);
    await page.type('.form-group--desktop .js-email', data.sm.email);
    await page.click('#privacy-form-field');
    await page.click('.form-btn-submit');

    await page.waitForSelector('a[data-partner="Iren"][class="btn btn-results-box btn-calcola-green lead-post-self"]');
    await page.waitFor(100);

    const btn = await page.$('a[data-partner="Iren"][class="btn btn-results-box btn-calcola-green lead-post-self"]');
    if (btn) {
        btn.click();
    }

    await page.waitForSelector('#prodotto_luce_id');
    if(luce){
        await page.select('#prodotto_luce_id', '1');
        await page.waitForSelector('.Form-section--fadeIn #pod_code');
        await page.select('#potential_difference', data.contracts.tensioneRichiesta);
        await page.type('#pod_code', data.contracts.pod);
    }
    if(gas){
        await page.select('#prodotto_gas_id', '2');
        await page.waitForSelector('.Form-section--fadeIn #pdr_code');
        await page.select('#pdr_type', data.contracts.tipologiaPdr);
        await page.type('#pdr_code', data.contracts.pdr);
    }

    await page.waitFor(1000);
    await page.click('#birth_date');
    await page.keyboard.down('Control');
    await page.keyboard.press('KeyA');
    await page.keyboard.up('Control');
    await page.keyboard.press('Backspace');
    await page.type('#birth_date', data.contracts.dataNascita);
    await page.type('#birth_date', data.contracts.dataNascita);
    await page.select('#sex', data.contracts.sesso);
    await page.type('#cf', data.contracts.codiceFiscale);
    await page.type('#birth_place', data.contracts.comuneNascita);
    await page.waitForSelector('#autocomplete_list_birth li:nth-child(1)');
    const comune = await page.$('#autocomplete_list_birth li:nth-child(1)');

    if (comune) await comune.click();
    await page.click('#contract_form_submit');
    await page.waitForSelector('.is-show #activation_address');
    await page.type('#activation_address', data.contracts.attivazioneVia);
    await page.type('#activation_street_number', data.contracts.attivazioneCivico);
    await page.type('#activation_staircase', data.contracts.attivazioneInterno);
    await page.type('#activation_city', data.contracts.attivazioneCitta);
    await page.waitForSelector('#autocomplete_list_activation li:nth-child(1)');
    const comuneAttivazione = await page.$('#autocomplete_list_activation li[data-zip="00144"]');
    if (comuneAttivazione) await comuneAttivazione.click();
    await page.click('#address_form_submit');
    await page.waitForSelector('.Modal-success.is-loading', {timeout: 90000});
    await page.waitForSelector('.Modal-success:not(.is-loading)');
    await page.waitForSelector('.is-show #document_id');
    await page.select('#document_id', data.contracts.documentoTipo);
    await page.type('#document_id_number', data.contracts.documentoNumero);
    await page.type('#document_id_issue_date', data.contracts.documentoData);
    await page.type('#document_id_issue_authority', data.contracts.documentoRilasciato);
    await page.click('#phone');
    await page.keyboard.down('Control');
    await page.keyboard.press('KeyA');
    await page.keyboard.up('Control');
    await page.keyboard.press('Backspace');
    await page.type('#phone', data.contracts.cellulare);
    await page.waitFor(100);
    await page.click('#personal_form_submit');
    await page.waitForSelector('.is-show #payment_method');
    await page.select('#payment_method', data.contracts.paymentMethod);
    await page.click('#payment_form_submit');
    await page.waitForSelector('.is-show #residence_declaration');
    await page.select('#residence_declaration', data.contracts.residenza);
    await page.select('#job_title', data.contracts.occupazione);
    await page.click('#additional_form_submit');
    await page.waitForSelector('.is-show #consenso_1_si');
    await page.click('label[for="consenso_1_si"]');
    await page.click('label[for="consenso_2_si"]');
    await page.click('label[for="consenso_3_si"]');
    await page.click('#privacy_form_submit');
    await page.waitForSelector('.is-show #generate_contract_btn');
    await page.click('#generate_contract_btn');
    await page.waitFor(2000);
    await page.close();
}

describe('Flusso self care test supermoney', () => {

    const browser = createTestBrowser();

    afterAll( () => {
        browser.then(b => b.close());
    });


    it('should fail (luce)', async done => {
        const data = {
            sm:{
                nome: 'yuri',
                cognome: 'passamonti',
                cellulare: '3333333331',
                email: 'yuri.passamonti@blastingnews.com'
            },
            contracts: {
                pod: 'IT123E12345678',
                tensioneRichiesta: '220V',
                dataNascita: '09111981',
                sesso: 'M',
                codiceFiscale: 'PSSYRU81S09H501C',
                comuneNascita: 'roma',
                attivazioneVia: 'via fasulla',
                attivazioneCivico: '123',
                attivazioneInterno: 'A1',
                attivazioneCitta: 'roma'

            }
        };


        const b = await browser;
        const page = await b.newPage();

        await page.goto('https://test.energia.supermoney.eu/energia-elettrica/?secondLanded');
        await page.waitForSelector('.js-nome');
        await page.type('.form-group--desktop .js-nome', data.sm.nome);
        await page.click('label[for="not-active"]');
        await page.type('.form-group--desktop .form-input.js-cognome', data.sm.cognome);
        await page.type('.form-group--desktop .js-cellulare', data.sm.cellulare);
        await page.type('.form-group--desktop .js-email', data.sm.email);
        await page.click('#privacy-form-field');
        await page.click('.form-btn-submit');

        await page.waitForSelector('a[data-partner="Iren"][class="btn btn-results-box btn-calcola-green lead-post-self"]');
        await page.waitFor(100);

        const btn = await page.$('a[data-partner="Iren"][class="btn btn-results-box btn-calcola-green lead-post-self"]');
        if(btn) {btn.click();}
        await page.waitForSelector('#prodotto_luce_id');
        await page.waitForSelector('#pod_code');

        await page.select('#potential_difference', data.contracts.tensioneRichiesta);
        await page.click('#birth_date');
        await page.keyboard.down('Control');
        await page.keyboard.press('KeyA');
        await page.keyboard.up('Control');
        await page.keyboard.press('Backspace');
        await page.type('#birth_date', data.contracts.dataNascita);
        await page.select('#sex', data.contracts.sesso);
        await page.type('#cf', data.contracts.codiceFiscale);
        await page.type('#birth_place', data.contracts.comuneNascita);
        await page.waitForSelector('#autocomplete_list_birth li:nth-child(1)');
        const comune = await page.$('#autocomplete_list_birth li:nth-child(1)');
        await page.type('#pod_code', data.contracts.pod);
        if(comune) await comune.click();
        await page.click('#contract_form_submit');
        await page.waitForSelector('.is-show #activation_address');
        await page.type('#activation_address', data.contracts.attivazioneVia);
        await page.type('#activation_street_number', data.contracts.attivazioneCivico);
        await page.type('#activation_staircase', data.contracts.attivazioneInterno);
        await page.type('#activation_city', data.contracts.attivazioneCitta);
        await page.waitForSelector('#autocomplete_list_activation li:nth-child(1)');
        const comuneAttivazione = await page.$('#autocomplete_list_activation li[data-zip="00144"]');
        if(comuneAttivazione) await comuneAttivazione.click();
        await page.click('#address_form_submit');
        await page.waitForSelector('.Form-failure');
        const content = await page.content();

        expect(content.includes('Siamo spiacenti! Il prodotto da te selezionato non è attivabile per la tua utenza')).toBeTruthy();
        done();
    }, 120000);



    it('should generate a contract (luce)', async done => {

        const contracts = Object.assign({}, baseData.contracts,
            { pod: 'IT002E0000330A',
            tensioneRichiesta: '220V',})

        const data = Object.assign({}, baseData,{contracts});


        await expect(compileSelfCare(browser, data, true, false)).resolves.toBeUndefined();

        done();


    }, 120000);


    it('should generate a contract (gas)', async done => {

        const contracts = Object.assign({}, baseData.contracts,
            {pdr: '00881103022099',
            tipologiaPdr: 'domestico',});

        const data = Object.assign({}, baseData,{contracts});

        await expect(compileSelfCare(browser, data, false, true)).resolves.toBeUndefined();

        done();


    }, 120000);


    it('should generate a contract (luce e gas)', async done => {
        const contracts = Object.assign({}, baseData.contracts,
            {pdr: '00881103022099',
                tipologiaPdr: 'domestico',
                pod: 'IT002E0000330A',
                tensioneRichiesta: '220V',
            }
        );
        const data = Object.assign({}, baseData,{contracts});

        await expect(compileSelfCare(browser, data, true, true)).resolves.toBeUndefined();

        done();


    }, 120000);

});
