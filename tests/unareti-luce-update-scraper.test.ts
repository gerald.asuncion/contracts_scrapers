import {createTestBrowser, createTestMysql, successResponse} from "./utils";
import scraper from '../src/scrapers/unareti/unareti-luce-update-scraper'
import {logger} from "../src/logger";

describe('unareti scraper update', () => {

    const toItTeam = 'emiliano.tuzia@blastingnews.com';
    const baseDownloadPath = './output';
    const browser = createTestBrowser();

    const buildScraper = () => {
        const mailer = {
            send: jest.fn()
        }

        const mysqlPool = createTestMysql();

        return scraper({browser, mailer, mysqlPool, logger, baseDownloadPath, toItTeam});

    };


    it('should import', async () => {

        const unaretiLuceUpdateScraper = buildScraper()

        const response = await unaretiLuceUpdateScraper.scrape({})
        expect(response).toEqual(successResponse);
    }, 50000);

});
