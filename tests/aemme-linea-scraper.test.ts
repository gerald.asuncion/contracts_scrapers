import {Browser, launch} from "puppeteer";
import scraper from "../src/scrapers/terranova/aemme-linea-scraper";
import {buildScraper, createTestBrowser} from "./utils";

describe('Aemme Linea Scraper', () => {
  let browser: Promise<Browser> = createTestBrowser();

  const checkScraper = buildScraper(browser)(scraper);

  const successResponse = {success: true};
  const failedResponse = {success: false};

  const inventedPdr1 = 'IT012E00210022';
  const inventedPdr2 = 'IT001E89269404';

  const validPod = 'IT001E89269404';

  it('should return a failure for invented pdr 1', async () => checkScraper({pdr:inventedPdr1}, failedResponse), 50000);
  it('should return a failure for invented pdr 2', async () => checkScraper({pdr:inventedPdr2}, failedResponse), 50000);
});
