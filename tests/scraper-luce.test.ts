import {Browser, launch} from "puppeteer";
import {buildScraper, createTestBrowser} from "./utils";

import {scraperLuce} from "../src/scrapers/scraper-luce";

describe('Scraper LUCE', () => {
    let browser: Promise<Browser> = launch();

    beforeAll(() => {

    });

    afterAll(() => {
        browser.then(b => b.close());
    });
    const checkScraper = buildScraper(browser)(scraperLuce);


    const comune = {
        nome: 'Fiumicino',
        codice: '058120',
        zona: {codice: '3', nome: 'Centro'},
        regione: {codice: '12', nome: 'Lazio'},
        provincia: {codice: '058', nome: 'Roma'},
        sigla: 'RM',
        codiceCatastale: 'M297',
        cap: ['00054'],
        popolazione: 67626

    };
    const milano = {
        comune:
            {
                nome: 'Milano',
                codice: '015146',
                zona: {codice: '1', nome: 'Nord-ovest'},
                regione: {codice: '03', nome: 'Lombardia'},
                provincia: {codice: '015', nome: 'Milano'},
                sigla: 'MI',
                codiceCatastale: 'F205',
                cap: []
            }
    }


    it('should return e distribuzione for fiumicino', async () => checkScraper({comune}, {
        success: true,
        results: ['E-DISTRIBUZIONE S.P.A.']
    }), 50000);
    it('should return 1 result for milano', async () => checkScraper(milano, {
        success: true,
        results: ['UNARETI SPA']
    }), 50000);


});
