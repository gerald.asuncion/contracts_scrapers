import {Browser, launch} from "puppeteer";
import {buildScraper, createTestBrowser} from "./utils";
import {ScraperArgs, ScraperInterface} from "../src/scrapers/ScraperInterface";

export default (nome: string, scraper: any) => {


    describe(nome + ' Scraper', () => {
        let browser: Promise<Browser> = createTestBrowser();

        beforeAll(() => {

        });

        afterAll(() => {
            //browser.then(b => b.close());
        });
        const checkScraper = buildScraper(browser)(scraper);

        const successResponse = {success: true};
        const failedResponse = {success: false};

        const inventedPod1 = 'IT012E00210022';
        const inventedPod2 = 'IT001E89269404';

        const validPod = 'IT001E89269404';


        it('should return a failure for invented pod 1', async () => checkScraper({pod: inventedPod1}, failedResponse), 50000);
        it('should return a failure for invented pod 2', async () => checkScraper({pod: inventedPod2}, failedResponse), 50000);


    });
}
