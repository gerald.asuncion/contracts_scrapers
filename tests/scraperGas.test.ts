import {Browser} from "puppeteer";
import {buildScraper, createTestBrowser} from "./utils";

import scraperGas from "../src/scrapers/scraper-gas";

describe('Scraper GAS', () => {
    let browser: Promise<Browser> = createTestBrowser();

    beforeAll(() => {

    });

    afterAll(() => {
        browser.then(b => b.close());
    });
    const checkScraper = buildScraper(browser)(scraperGas);


    const comune = {
        nome: 'Fiumicino',
        codice: '058120',
        zona: {codice: '3', nome: 'Centro'},
        regione: {codice: '12', nome: 'Lazio'},
        provincia: {codice: '058', nome: 'Roma'},
        sigla: 'RM',
        codiceCatastale: 'M297',
        cap: ['00054'],
        popolazione: 67626

    };
    const milano = {
        comune:
            {
                nome: 'Milano',
                codice: '015146',
                zona: {codice: '1', nome: 'Nord-ovest'},
                regione: {codice: '03', nome: 'Lombardia'},
                provincia: {codice: '015', nome: 'Milano'},
                sigla: 'MI',
                codiceCatastale: 'F205',
                cap: []
            }
    }


    it('should return Italgas for fiumicino', async () => checkScraper({comune}, {
        success: true,
        results: ['ITALGAS RETI S.P.A.']
    }), 50000);
    it('should return 3 results for milano', async () => checkScraper(milano, {
        success: true,
        results: ['UNARETI SPA', '2I RETE GAS S.P.A.', 'ITALGAS RETI S.P.A.']
    }), 50000);


});
