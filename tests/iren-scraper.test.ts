import {launch} from "puppeteer";
import irenScraper from "../src/scrapers/iren/iren-scraper";


describe('Iren Scraper', () => {

    const openBrowser = () => launch({headless: false});

    const successResponse = {success: true};


    it('should return a success', async () => {

        const browser = await openBrowser();
        const scraper = irenScraper({browser});

        const result = await scraper.scrape({
            codiceFiscale: 'PSSYRU81S09H501C'
        } );

        expect(result).toEqual(successResponse)
        await browser.close()
    }, 15000);

});
