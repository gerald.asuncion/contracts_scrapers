import amagAlessandriaScraper from "../src/scrapers/amag/amag-alessandria-scraper";
import {buildScraper, createTestBrowser} from "./utils";

describe('Amag Alessandria Scraper', () => {

  const browser = createTestBrowser();


  const checkScraper = buildScraper(browser)(amagAlessandriaScraper);

  beforeAll(() => {

  });

  afterAll(() => {
      browser.then(b => b.close());
  });


  const successResponse = {success: true};
  const failedResponse = {success: false};

  const inventedPdr1 = {pdr:'IT012E00210022'};
  const inventedPdr2 = {pdr:'IT001E89269404'};

  const validPod = 'IT001E89269404';

  it('should return a failure for invented pdr 1', async () => checkScraper(inventedPdr1, failedResponse), 60000);
  it('should return a failure for invented pdr 2', async () => checkScraper(inventedPdr2, failedResponse), 60000);

});
