import {createTestBrowser, failedResponse, successResponse} from "./utils";
import aretiAlertScraper from "../src/scrapers/areti/areti-alert-scraper";

describe('Areti Alert scraper', () => {

    const browser = createTestBrowser();
    const mailer = {
        send:jest.fn()
    };

    beforeAll(() => {

    });

    afterAll(() => {
        browser.then(b => b.close());
    });

    it('should return a success for imported POD file', async () => {
        const scraper = aretiAlertScraper({browser,mailer});
        const result = await scraper.scrape('');
        expect(result).toEqual(successResponse)
    }, 15000);
});
