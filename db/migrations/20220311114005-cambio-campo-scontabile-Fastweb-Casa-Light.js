'use strict';

const SQL = `UPDATE fastweb_offerte SET scontabile = ? WHERE offerta = 'Fastweb Casa Light'`;

module.exports = {
  async up (queryInterface, Sequelize) {
    await queryInterface.sequelize.query(SQL, {
      replacements: [false],
      type: Sequelize.QueryTypes.UPDATE
    });
  },

  async down (queryInterface, Sequelize) {
    await queryInterface.sequelize.query(SQL, {
      replacements: [true],
      type: Sequelize.QueryTypes.UPDATE
    });
  }
};
