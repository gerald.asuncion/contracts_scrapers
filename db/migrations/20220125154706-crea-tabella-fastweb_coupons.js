'use strict';

const TABLE_NAME = 'fastweb_coupons';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable(TABLE_NAME, {
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },
      data_creazione: {
        type: Sequelize.DATE,
        allowNull: false,
        defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
      },
      data_aggiornamento: {
        type: Sequelize.DATE,
        allowNull: false,
        defaultValue: Sequelize.literal('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP')
      },
      codice: {
        type: Sequelize.STRING(100),
        allowNull: false
      },
      sconto_id: {
        type: Sequelize.INTEGER,
        allowNull: false
      },
      abilitato: {
        type: Sequelize.BOOLEAN,
        allowNull: false,
        defaultValue: true
      }
    });

    await queryInterface.sequelize.query(`
      INSERT INTO ${TABLE_NAME} (codice,sconto_id)
      VALUES
      ('CONV5X12MAR18',3);
    `);
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable(TABLE_NAME);
  }
};
