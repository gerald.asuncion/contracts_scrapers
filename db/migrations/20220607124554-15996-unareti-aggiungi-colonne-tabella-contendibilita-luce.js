'use strict';

const TABLE_NAME = 'unareti_contendibilita_luce';

module.exports = {
  async up (queryInterface, Sequelize) {
    await queryInterface.addColumn(TABLE_NAME, 'codice_istat', {
      type: Sequelize.STRING(5),
      allowNull: true
    });

    await queryInterface.addColumn(TABLE_NAME, 'piano', {
      type: Sequelize.STRING(5),
      allowNull: true
    });

    await queryInterface.addColumn(TABLE_NAME, 'matricola_contatore', {
      type: Sequelize.STRING(100),
      allowNull: true
    });

    await queryInterface.addColumn(TABLE_NAME, 'tensione', {
      type: Sequelize.STRING(100),
      allowNull: true
    });

    await queryInterface.addColumn(TABLE_NAME, 'potenza_predisposta_sul_pod', {
      type: Sequelize.STRING(100),
      allowNull: true
    });
  },

  async down (queryInterface, Sequelize) {
    await queryInterface.removeColumn(TABLE_NAME, 'codice_istat');

    await queryInterface.removeColumn(TABLE_NAME, 'piano');

    await queryInterface.removeColumn(TABLE_NAME, 'matricola_contatore');

    await queryInterface.removeColumn(TABLE_NAME, 'tensione');

    await queryInterface.removeColumn(TABLE_NAME, 'potenza_predisposta_sul_pod');
  }
};
