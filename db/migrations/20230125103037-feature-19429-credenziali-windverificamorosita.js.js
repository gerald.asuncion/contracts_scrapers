'use strict';

const { credentialExists, getScraperId, addCredenziale } = require("../helpers/query-credentials-insert");

const credentialsToAdd = [
  {
    "username": "SUPERMONEY11",
    "password": "Supermoney2022!"
  },
  {
    "username": "SUPERMONEY24",
    "password": "Supermoney2022!"
  },
  {
    "username": "SUPERMONEY12",
    "password": "Supermoney2022!"
  },
  {
    "username": "SUPERMONEY13",
    "password": "Supermoney2022!"
  },
  {
    "username": "SUPERMONEY14",
    "password": "Supermoney2022!"
  },
  {
    "username": "SUPERMONEY15",
    "password": "Supermoney2022!"
  },
  {
    "username": "SUPERMONEY27",
    "password": "Supermoney2022!"
  },
  {
    "username": "SUPERMONEY28",
    "password": "Supermoney2022!"
  },
  {
    "username": "SUPERMONEY17",
    "password": "Supermoney2022!"
  },
  {
    "username": "SUPERMONEY18",
    "password": "Supermoney2022!"
  },
  {
    "username": "SUPERMONEY19",
    "password": "Supermoney2022!"
  },
  {
    "username": "SUPERMONEY20",
    "password": "Supermoney2022!"
  },
  {
    "username": "SUPERMONEY21",
    "password": "Supermoney2022!"
  },
  {
    "username": "SUPERMONEY22",
    "password": "Supermoney2022!"
  },
  {
    "username": "SUPERMONEY23",
    "password": "Supermoney2022!"
  },
  {
    "username": "SUPERMONEY26",
    "password": "Supermoney2022!"
  },
  {
    "username": "SUPERMONEY25",
    "password": "Supermoney2022!"
  },
  {
    "username": "SUPERMONEY16",
    "password": "Supermoney2022!"
  },
  {
    "username": "SUPERMONEY29",
    "password": "Supermoney2022!"
  },
  {
    "username": "SUPERMONEY30",
    "password": "Supermoney2022!"
  },
  {
    "username": "SUPERMONEY31",
    "password": "Supermoney2022!"
  },
  {
    "username": "SUPERMONEY32",
    "password": "Supermoney2022!"
  },
  {
    "username": "SUPERMONEY33",
    "password": "Supermoney2022!"
  },
  {
    "username": "SUPERMONEY34",
    "password": "Supermoney2022!"
  },
  {
    "username": "SUPERMONEY35",
    "password": "Supermoney2022!"
  },
  {
    "username": "SUPERMONEY23",
    "password": "Supermoney2022!"
  },
  {
    "username": "SUPERMONEY37",
    "password": "Supermoney2022!"
  },
  {
    "username": "SUPERMONEY38",
    "password": "Supermoney2022!"
  },
  {
    "username": "SUPERMONEY39",
    "password": "4ct$CY6?"
  },
  {
    "username": "SUPERMONEY40",
    "password": "Supermoney2022!"
  },
  {
    "username": "SUPERMONEY41",
    "password": "Supermoney2022!"
  },
  {
    "username": "SUPERMONEY42",
    "password": "Supermoney2022!"
  },
  {
    "username": "SUPERMONEY43",
    "password": "Supermoney2022!"
  },
  {
    "username": "SUPERMONEY44",
    "password": "Supermoney2022!"
  },
  {
    "username": "SUPERMONEY45",
    "password": "Supermoney2022!"
  },
  {
    "username": "SUPERMONEY46",
    "password": "Supermoney2022!"
  },
  {
    "username": "SUPERMONEY47",
    "password": "Supermoney2022!"
  },
  {
    "username": "SUPERMONEY48",
    "password": "Supermoney2022!"
  },
  {
    "username": "SUPERMONEY49",
    "password": "Supermoney2022!"
  }
];
const scraperCode = "wind-verifica-morosita--II-livello";

module.exports = {
  async up(queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */

    const scraperId = await getScraperId(queryInterface, scraperCode);

    console.log("credentialExists", scraperId, scraperCode);

    const credentialsCond = await Promise.all(credentialsToAdd.map(async credential => ({
      ...credential,
      exists: await credentialExists(queryInterface, credential.username, scraperCode)
    })));

    const credentialsCondToAdd = credentialsCond.filter(credential => !credential.exists);
    // const credentialsCondToUpdate = credentialsCond.filter(credential => credential.exists);

    for (const credenziale of credentialsCondToAdd) {
      console.log("adding credenziale", credenziale);

      await addCredenziale(queryInterface, credenziale, scraperId);
    }
  },

  async down(queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
