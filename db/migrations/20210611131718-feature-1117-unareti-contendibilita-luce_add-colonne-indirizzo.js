'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.addColumn('unareti_contendibilita_luce', 'toponimo', {
      type: Sequelize.STRING(100),
      allowNull: true
    });

    await queryInterface.addColumn('unareti_contendibilita_luce', 'nome_strada', {
      type: Sequelize.STRING(100),
      allowNull: true
    });

    await queryInterface.addColumn('unareti_contendibilita_luce', 'civico', {
      type: Sequelize.STRING(30),
      allowNull: true
    });

    await queryInterface.addColumn('unareti_contendibilita_luce', 'estensione_civico', {
      type: Sequelize.STRING(30),
      allowNull: true
    });

    await queryInterface.addColumn('unareti_contendibilita_luce', 'cap', {
      type: Sequelize.STRING(5),
      allowNull: true
    });

    await queryInterface.addColumn('unareti_contendibilita_luce', 'comune', {
      type: Sequelize.STRING(100),
      allowNull: true
    });

    await queryInterface.addColumn('unareti_contendibilita_luce', 'provincia', {
      type: Sequelize.STRING(2),
      allowNull: true
    });

    await queryInterface.addColumn('unareti_contendibilita_luce', 'interno', {
      type: Sequelize.STRING(100),
      allowNull: true
    });

    await queryInterface.addColumn('unareti_contendibilita_luce', 'scala', {
      type: Sequelize.STRING(100),
      allowNull: true
    });

    await queryInterface.addColumn('unareti_contendibilita_luce', 'potenza_contrattuale', {
      type: Sequelize.DECIMAL(10, 2),
      allowNull: true
    });

    await queryInterface.addColumn('unareti_contendibilita_luce', 'valore_di_tensione', {
      type: Sequelize.DECIMAL(10, 2),
      allowNull: true
    });
  }
};
