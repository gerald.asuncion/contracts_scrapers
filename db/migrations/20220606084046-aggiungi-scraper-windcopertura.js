'use strict';

const TABLE_NAME = 'Scrapers';

const SQL = `
INSERT INTO ${TABLE_NAME} (label,codice,partner,tipologia,dataCreazione,dataAggiornamento)
VALUES
('Wind Check Copertura', 'wind-check-copertura', 'wind', 'fattibilita', ?, ?);
`;

function padStartDate(str) {
  return String(str).padStart(2, '0');
}

function formatTimestamp() {
  const date = new Date();
  const first = [
    date.getFullYear(),
    padStartDate(date.getMonth() + 1),
    padStartDate(date.getDate())
  ].join('-');

  const second = [
    padStartDate(date.getHours()),
    padStartDate(date.getMinutes()),
    padStartDate(date.getSeconds())
  ].join(':');

  // YYYY-MM-dd HH:mm:ss
  return `${first} ${second}`;
}

const data = formatTimestamp();

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.sequelize.query(SQL, {
      replacements: [
        data,
        data
      ]
    });
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
  }
};
