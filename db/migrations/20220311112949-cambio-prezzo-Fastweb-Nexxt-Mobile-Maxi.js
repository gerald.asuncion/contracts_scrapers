'use strict';

const SQL = `UPDATE fastweb_offerte SET prezzo = ? WHERE offerta = 'Fastweb Nexxt Mobile Maxi'`;

module.exports = {
  async up (queryInterface, Sequelize) {
    await queryInterface.sequelize.query(SQL, {
      replacements: [11.95],
      type: Sequelize.QueryTypes.UPDATE
    });
  },

  async down (queryInterface, Sequelize) {
    await queryInterface.sequelize.query(SQL, {
      replacements: [10.95],
      type: Sequelize.QueryTypes.UPDATE
    });
  }
};
