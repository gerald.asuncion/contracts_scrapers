'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.addColumn(
        'areti_contendibilita',
        'indirizzo',
        {
          type: Sequelize.STRING,
          allowNull: true,
        },
      ),
      queryInterface.addColumn(
        'areti_contendibilita',
        'interno',
        {
          type: Sequelize.STRING(100),
          allowNull: true,
        },
      ),
      queryInterface.addColumn(
        'areti_contendibilita',
        'scala',
        {
          type: Sequelize.STRING(100),
          allowNull: true,
        },
      ),
      queryInterface.addColumn(
        'areti_contendibilita',
        'potenza_disponibile',
        {
          type: Sequelize.Sequelize.DECIMAL(10, 3),
          allowNull: true,
        },
      ),
      queryInterface.addColumn(
        'areti_contendibilita',
        'tensione_rete',
        {
          type: Sequelize.STRING(100),
          allowNull: true,
        },
      ),
    ]);
  },

  down: async (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.removeColumn('areti_contendibilita', 'indirizzo'),
      queryInterface.removeColumn('areti_contendibilita', 'interno'),
      queryInterface.removeColumn('areti_contendibilita', 'scala'),
      queryInterface.removeColumn('areti_contendibilita', 'potenza_disponibile'),
      queryInterface.removeColumn('areti_contendibilita', 'tensione_rete'),
    ]);
  }
};
