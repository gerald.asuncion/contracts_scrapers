'use strict';

const TABLE_NAME = 'IREN_GAS_REMI';
const COLUMN_NAME = 'VECCHIA_REMI';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.changeColumn(TABLE_NAME, COLUMN_NAME, {
      type: Sequelize.STRING(20)
    });
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.changeColumn(TABLE_NAME, COLUMN_NAME, {
      type: Sequelize.STRING(10)
    });
  }
};
