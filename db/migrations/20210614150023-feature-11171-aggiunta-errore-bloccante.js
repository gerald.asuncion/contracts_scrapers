'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.sequelize.query(`
      INSERT INTO blocco_scraper_errors (scraper,testo,bloccante)
      VALUES
      ('Fastweb', 'Timeout exceeded while waiting for event', 1);
    `);
  }
};
