'use strict';

const SQL = `UPDATE fastweb_offerte SET prezzo = ? WHERE offerta = 'Fastweb Nexxt Casa Plus'`;

module.exports = {
  async up (queryInterface, Sequelize) {
    await queryInterface.sequelize.query(SQL, {
      replacements: [35.95],
      type: Sequelize.QueryTypes.UPDATE
    });
  },

  async down (queryInterface, Sequelize) {
    await queryInterface.sequelize.query(SQL, {
      replacements: [32.95],
      type: Sequelize.QueryTypes.UPDATE
    });
  }
};
