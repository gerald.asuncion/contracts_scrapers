'use strict';

const TABLE_NAME = 'fastweb_offerte';

module.exports = {
  async up (queryInterface, Sequelize) {
    await queryInterface.sequelize.query(`
      INSERT INTO ${TABLE_NAME} (offerta,prezzo,scontabile)
      VALUES
      ('nessuna opzione',0.00, false);
    `);
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
  }
};
