'use strict';

const TABLE_NAME = 'fastweb_offerte';

module.exports = {
  async up(queryInterface, Sequelize) {

    await queryInterface.bulkInsert('fastweb_offerte', [{
      offerta: 'Fastweb Casa Light FWA',
      prezzo: 22.95,
      scontabile: false,
    }, {
      offerta: 'Fastweb Casa FWA',
      prezzo: 26.95,
      scontabile: false,
    }], {});

    /*

    // on test user has no privileges on fastweb_offerte!!!
    await queryInterface.changeColumn(TABLE_NAME, 'offerta', {
      type: Sequelize.STRING(255), // Sequelize.STRING,
      unique: true,
      allowNull: false
    });

    const transaction = await queryInterface.sequelize.transaction();

    try {
      let res;

      // on duplicate works only with indexes or unique constraints
      await queryInterface.sequelize.query(`
        INSERT INTO ${TABLE_NAME} (offerta, prezzo, scontabile) VALUES ('Fastweb Casa Light FWA', 22.95, 0)
        ON CONFLICT ("offerta") DUPLICATE KEY UPDATE prezzo = 22.95;
        INSERT INTO ${TABLE_NAME} (offerta, prezzo, scontabile) VALUES ('Fastweb Casa FWA', 22.95, 0)
        ON CONFLICT ("offerta") DUPLICATE KEY UPDATE prezzo = 26.95;
      `, {transaction});

      // upsert wont work for the same above
      res = await queryInterface.upsert(TABLE_NAME, {
        offerta: 'Fastweb Casa Light FWA',
        prezzo: 22.95,
        scontabile: false,
      }, {
        prezzo: 22.95
      }, {
        offerta: 'Fastweb Casa Light FWA'
      }, {
        transaction,
        logging: console.log
      });

      console.log("upsert 'Fastweb Casa Light FWA'")
      console.log(res)

      res = await queryInterface.upsert(TABLE_NAME, {
        offerta: 'Fastweb Casa FWA',
        prezzo: 26.95,
        scontabile: false,
      }, {
        prezzo: 26.95
      }, {
        offerta: 'Fastweb Casa FWA'
      }, {
        transaction,
        logging: console.log
      });

      console.log("upsert 'Fastweb Casa FWA'")
      console.log(res)

      await transaction.commit();
    } catch (e) {
      console.log("upsert failed")
      console.error(e)

      await transaction.rollback();
    }
    */
  },

  async down(queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */

    /*
    await queryInterface.changeColumn(TABLE_NAME, 'offerta', {
      type: Sequelize.STRING(255), // Sequelize.STRING,
      unique: false,
      allowNull: false
    });
    */
  }
};
