'use strict';

const TABLE_NAME = 'fastweb_coupons';

const SQL = `UPDATE ${TABLE_NAME} AS A, (SELECT id FROM ${TABLE_NAME} WHERE codice = ?) AS B SET A.codice = ? WHERE A.id = B.id;`;

const PREV_COUPON = 'CONV5X12MAR18';
const NEXT_COUPON = 'CONV10FOREVERMAG18';

module.exports = {
  async up (queryInterface, Sequelize) {
    await queryInterface.sequelize.query(
      SQL,
      {
        replacements: [
          PREV_COUPON,
          NEXT_COUPON
        ],
        type: Sequelize.QueryTypes.UPDATE
      }
    );
  },

  async down (queryInterface, Sequelize) {
    await queryInterface.sequelize.query(
      SQL,
      {
        replacements: [
          NEXT_COUPON,
          PREV_COUPON
        ],
        type: Sequelize.QueryTypes.UPDATE
      }
    );
  }
};
