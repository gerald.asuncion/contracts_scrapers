'use strict';

const TABLE_NAME = 'Scrapers';

function padStartDate(str) {
  return String(str).padStart(2, '0');
}

function formatTimestamp() {
  const date = new Date();
  const first = [
    date.getFullYear(),
    padStartDate(date.getMonth() + 1),
    padStartDate(date.getDate())
  ].join('-');

  const second = [
    padStartDate(date.getHours()),
    padStartDate(date.getMinutes()),
    padStartDate(date.getSeconds())
  ].join(':');

  // YYYY-MM-dd HH:mm:ss
  return `${first} ${second}`;
}

const data = formatTimestamp();

module.exports = {
  async up (queryInterface, Sequelize) {
    await queryInterface.sequelize.query(`
      INSERT INTO ${TABLE_NAME} (label,codice,partner,tipologia,dataCreazione,dataAggiornamento)
      VALUES
      ('Wind verifica morosità', 'wind-verifica-morosita', 'wind', 'fattibilita', "${data}", "${data}"),
      ('Wind verifica morosità - II Auth', 'wind-verifica-morosita--II-livello', 'wind', 'fattibilita', "${data}", "${data}")
    `);
  },

  async down (queryInterface, Sequelize) {
  }
};
