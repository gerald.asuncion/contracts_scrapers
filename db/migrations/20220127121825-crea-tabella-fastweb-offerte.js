'use strict';

const TABLE_NAME = 'fastweb_offerte';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable(TABLE_NAME, {
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },
      data_creazione: {
        type: Sequelize.DATE,
        allowNull: false,
        defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
      },
      data_aggiornamento: {
        type: Sequelize.DATE,
        allowNull: false,
        defaultValue: Sequelize.literal('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP')
      },
      offerta: {
        type: Sequelize.STRING(255),
        allowNull: false
      },
      prezzo: {
        type: Sequelize.FLOAT,
        allowNull: false
      },
      scontabile: {
        type: Sequelize.BOOLEAN,
        allowNull: false,
        defaultValue: true
      }
    });

    await queryInterface.sequelize.query(`
      INSERT INTO ${TABLE_NAME} (offerta,prezzo,scontabile)
      VALUES
      ('Fastweb Nexxt Casa',28.95, true),
      ('Fastweb Nexxt Casa Plus',32.95, true),
      ('Fastweb Casa Light',25.95, true),
      ('Fastweb Nexxt Mobile',7.95, false),
      ('Fastweb Nexxt Mobile Maxi',10.95, false);
    `);
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable(TABLE_NAME);
  }
};
