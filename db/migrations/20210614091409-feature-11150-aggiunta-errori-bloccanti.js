'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.changeColumn('blocco_scraper_errors', 'testo', {
      type: Sequelize.STRING(255),
      allowNull: false
    });

    await queryInterface.sequelize.query(`
      INSERT INTO blocco_scraper_errors (scraper,testo,bloccante)
      VALUES
      ('Fastweb', 'Errori post invio firma: j_id_k8: Indicare se il cliente possiede una numerazione da portare; j_id_l9: Indicare se il cliente possiede una numerazione dati da portare', 1);
    `);
  }
};
