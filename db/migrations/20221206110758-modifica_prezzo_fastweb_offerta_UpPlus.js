'use strict';

const TABLE_NAME = 'fastweb_offerte';

module.exports = {
  async up (queryInterface, Sequelize) {
    const transaction = await queryInterface.sequelize.transaction();

    try {
      await queryInterface.sequelize.query(`
        UPDATE ${TABLE_NAME} set prezzo = 3
        WHERE offerta = 'UpPlus'
      `, {transaction});

      await transaction.commit();
    } catch (e) {
      await transaction.rollback();
    }
  },

  async down (queryInterface, Sequelize) {
    const transaction = await queryInterface.sequelize.transaction();

    try {
      await queryInterface.sequelize.query(`
        UPDATE ${TABLE_NAME} set prezzo = 2
        WHERE offerta = 'UpPlus'
      `, {transaction});

      await transaction.commit();
    } catch (e) {
      await transaction.rollback();
    }
  }
};
