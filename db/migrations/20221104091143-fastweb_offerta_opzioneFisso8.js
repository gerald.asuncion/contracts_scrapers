'use strict';

const TABLE_NAME = 'fastweb_offerte';

module.exports = {
  async up (queryInterface, Sequelize) {
    const transaction = await queryInterface.sequelize.transaction();

    try {
      await queryInterface.sequelize.query(`
        INSERT INTO ${TABLE_NAME} (offerta,prezzo,scontabile)
        VALUES ('Sconto Fisso Mobile',-3, false);
      `, {transaction});

      await transaction.commit();
    } catch (e) {
      await transaction.rollback();
    }
  },

  async down (queryInterface, Sequelize) {
    const transaction = await queryInterface.sequelize.transaction();

    try {
      await queryInterface.sequelize.query(`
        DELETE FROM ${TABLE_NAME} WHERE offerta = 'Sconto Fisso Mobile';
      `, {transaction});

      await transaction.commit();
    } catch (e) {
      await transaction.rollback();
    }
  }
};
