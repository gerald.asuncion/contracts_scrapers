'use strict';

const TABLE_NAME = 'fastweb_sconti';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable(TABLE_NAME, {
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },
      data_creazione: {
        type: Sequelize.DATE,
        allowNull: false,
        defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
      },
      data_aggiornamento: {
        type: Sequelize.DATE,
        allowNull: false,
        defaultValue: Sequelize.literal('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP')
      },
      label: {
        type: Sequelize.STRING(255),
        allowNull: false
      },
      valore: {
        type: Sequelize.FLOAT,
        allowNull: false
      }
    });

    await queryInterface.sequelize.query(`
      INSERT INTO ${TABLE_NAME} (label,valore)
      VALUES
      ('Nessuno Sconto',0),
      ('Sconto 5€ x 12 mesi',5),
      ('Sconto 6€ x 12 mesi',6);
    `);
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable(TABLE_NAME);
  }
};
