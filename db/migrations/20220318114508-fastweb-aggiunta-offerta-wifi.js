'use strict';

const TABLE_NAME = 'fastweb_offerte';

module.exports = {
  async up (queryInterface, Sequelize) {
    await queryInterface.sequelize.query(`
      INSERT INTO ${TABLE_NAME} (offerta,prezzo,scontabile)
      VALUES
      ('Servizio potenziamento Wi Fi',0.00, false),
      ('Servizio di estensione del segnale Wi Fi',4.00, false);
    `);
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
  }
};
