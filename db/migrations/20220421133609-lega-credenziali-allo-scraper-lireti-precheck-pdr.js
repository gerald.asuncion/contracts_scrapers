'use strict';

const TABLE_NAME = 'RelCredenziali';
const USERNAME = 'GIUSEPPE.MESSINA';
const PASSWORD = 'marzo2022';

const SQL_SCRAPER_ID = 'SELECT id FROM Scrapers WHERE codice = "lereti-precheck-pdr";';
const SQL_CREDENZIALI_ID = 'SELECT id FROM Credenziali WHERE username = ? AND password = ?;';
const SQL_INSERT_REL = `
INSERT INTO ${TABLE_NAME} (scraperId,credenzialiId,dataCreazione,dataAggiornamento)
VALUES
(?, ?, ?, ?);
`;

function padStartDate(str) {
  return String(str).padStart(2, '0');
}

function formatTimestamp() {
  const date = new Date();
  const first = [
    date.getFullYear(),
    padStartDate(date.getMonth() + 1),
    padStartDate(date.getDate())
  ].join('-');

  const second = [
    padStartDate(date.getHours()),
    padStartDate(date.getMinutes()),
    padStartDate(date.getSeconds())
  ].join(':');

  // YYYY-MM-dd HH:mm:ss
  return `${first} ${second}`;
}

const data = formatTimestamp();

module.exports = {
  async up (queryInterface, Sequelize) {
    const transaction = await queryInterface.sequelize.transaction();
    try {
      const [ scrapers ] = await queryInterface.sequelize.query(SQL_SCRAPER_ID, {
        returning: true,
        transaction
      });

      const [ credentials ] = await queryInterface.sequelize.query(SQL_CREDENZIALI_ID, {
        returning: true,
        replacements: [
          USERNAME,
          PASSWORD
        ],
        transaction
      });

      await queryInterface.sequelize.query(SQL_INSERT_REL, {
        replacements: [
          scrapers[0].id,
          credentials[0].id,
          data,
          data
        ],
      });

      await transaction.commit();
    } catch (ex) {
      console.error('Rollbackking transaction');
      console.error(ex);

      transaction && (await transaction.rollback());

      throw ex;
    }
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
  }
};
