'use strict';

const TABLE_NAME = 'fastweb_offerte';

module.exports = {
  async up (queryInterface, Sequelize) {
    const transaction = await queryInterface.sequelize.transaction();

    try {
      await queryInterface.sequelize.query(`
        UPDATE ${TABLE_NAME}
        set prezzo = 29.95
        WHERE offerta = 'Fastweb Nexxt Casa';
      `, {transaction});

      await queryInterface.sequelize.query(`
        UPDATE ${TABLE_NAME}
        set prezzo = 26.95
        WHERE offerta = 'Fastweb Casa Light';
      `, {transaction});

      await transaction.commit();
    } catch (e) {
      await transaction.rollback();
    }
  },

  async down (queryInterface, Sequelize) {
    const transaction = await queryInterface.sequelize.transaction();

    try {
      await queryInterface.sequelize.query(`
        UPDATE ${TABLE_NAME}
        set prezzo = 28.95
        WHERE offerta = 'Fastweb Nexxt Casa';
      `, {transaction});

      await queryInterface.sequelize.query(`
        UPDATE ${TABLE_NAME}
        set prezzo = 25.95
        WHERE offerta = 'Fastweb Casa Light';
      `, {transaction});

      await transaction.commit();
    } catch (e) {
      await transaction.rollback();
    }
  }
};
