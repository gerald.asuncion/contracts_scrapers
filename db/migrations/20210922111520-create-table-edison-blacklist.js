'use strict';

const TABLE_NAME = 'edison_blacklist';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable(TABLE_NAME, {
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },
      data_creazione: {
        type: Sequelize.DATE,
        allowNull: false,
        defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
      },
      data_aggiornamento: {
        type: Sequelize.DATE,
        allowNull: false,
        defaultValue: Sequelize.literal('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP')
      },
      cap: {
        type: Sequelize.STRING(5),
        allowNull: false
      },
      comune: {
        type: Sequelize.STRING(100),
        allowNull: false
      }
    });

    await queryInterface.sequelize.query(`
      INSERT INTO ${TABLE_NAME} (comune,cap)
      VALUES
      ('ACERRA','80011'),
      ('AFRAGOLA','80021'),
      ('CASORIA','80026'),
      ('GIUGLIANO IN CAMPANIA','80014'),
      ('NAPOLI','80126'),
      ('NAPOLI','80145'),
      ('NAPOLI','80147'),
      ('TORRE ANNUNZIATA','80058');
    `);
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable(TABLE_NAME);
  }
};
