'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.changeColumn('queue', 'response', {
      type: Sequelize.TEXT,
      allowNull: true
    });
  }
};
