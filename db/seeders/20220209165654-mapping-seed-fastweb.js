'use strict';

const { up, down } = require('../helpers/seeders-mapping')
const scrapersEnsureExists = require('../helpers/query-scrapers-ensureexists')

let tables = {
  Scrapers: { tableName: 'Scrapers' },
  CampiEditabili: { tableName: 'CampiEditabili' },
  Mapping: { tableName: 'Mappings' }
};

const data = {
  scraper: 'fastweb',
  campi: [
    {
      "label": "Comune Nascita",
      "campo": "comuneNascita",
      "tipo": "geo"
    },
    {
      "label": "Residenza Comune",
      "campo": "residenzaComune",
      "tipo": "geo"
    },
    {
      "label": "Comune Nascita (EE)",
      "campo": "comuneNascita (EE)",
      "tipo": "geo"
    },
    {
      "label": "Residenza Comune (EE)",
      "campo": "residenzaComune (EE)",
      "tipo": "geo"
    },
    {
      "label": "Attivazione Comune",
      "campo": "attivazioneComune",
      "tipo": "geo"
    },
    {
      "label": "Attivazione Comune (EE)",
      "campo": "attivazioneComune (EE)",
      "tipo": "geo"
    },
    {
      "label": "Recapito Comune",
      "campo": "recapitoComune",
      "tipo": "geo"
    },
    {
      "label": "Recapito Comune (EE)",
      "campo": "recapitoComune (EE)",
      "tipo": "geo"
    }
  ],
  mapping: {
    "comuneNascita": {
      "bolzano": "bolzano/bozen",
      "reggio calabria": "reggio di calabria",
      // "seveso (mi)": "seveso (mb)",
      // "andria (ba)": "andria (bt)",
      // "bosa (nu)": "bosa (or)",
      // "monza (mi)": "monza (mb)",
      "merano": "merano/meran",
      // "merano (bz)": "merano/meran (bz)",
      // "fermo (ap)": "fermo (fm)"
    },
    "residenzaComune": {
      // "bolzano": "bolzano/bozen",
      // "reggio calabria": "reggio di calabria",
      // "seveso (mi)": "seveso (mb)",
      // "andria (ba)": "andria (bt)",
      // "bosa (nu)": "bosa (or)",
      // "monza (mi)": "monza (mb)",
      "monza (mb)": "monza (mi)",
      "merano": "merano/meran",
      // "merano (bz)": "merano/meran (bz)",
      // "fermo (ap)": "fermo (fm)",
      // "reggio di calabria (rc)": "reggio calabria (rc)",
      // "bolzano/bozen (bz)": "bolzano (bz)"
    },
    "comuneNascita (EE)": {
      "dominicana repubblica": "repubblica dominicana",
      "iugoslavia": "jugoslavia",
      "centrafricana repubblica": "impero centroafricano",
      "centroafricano impero": "impero centroafricano",
      "azerbaigian": "azerbaidjan",
      "russia (federazione russa)": "russia - federazione russa",
      "irlanda (eire)": "irlanda",
      "moldova": "moldavia",
      "slovacchia": "repubblica slovacca",
      "cina": "cina repubblica popolare",
      "calatafimi-segesta": "calatafimi segesta",
      "macedonia del nord": "macedonia",
      "repubblica socialista federale di jugoslavia": "jugoslavia",
      "regno unito": "gran bretagna",
      "repubblica democratica del congo": "congo repubblica democratica",
      "unione sovietica": "urss",
      "bosnia-erzegovina": "bosnia ed erzegovina",
      "sudafrica": "repubblica sudafricana",
      "federazione russa": "russia - federazione russa"
    },
    "residenzaComune (EE)": {
      "dominicana repubblica": "repubblica dominicana",
      "iugoslavia": "jugoslavia",
      "centrafricana repubblica": "impero centroafricano",
      "centroafricano impero": "impero centroafricano",
      "azerbaigian": "azerbaidjan",
      "russia (federazione russa)": "russia - federazione russa",
      "irlanda (eire)": "irlanda",
      "moldova": "moldavia",
      "slovacchia": "repubblica slovacca",
      "cina": "cina repubblica popolare",
      "calatafimi-segesta": "calatafimi segesta",
      "macedonia del nord": "macedonia",
      "repubblica socialista federale di jugoslavia": "jugoslavia",
      "regno unito": "gran bretagna",
      "repubblica democratica del congo": "congo repubblica democratica",
      "unione sovietica": "urss",
      "bosnia-erzegovina": "bosnia ed erzegovina",
      "sudafrica": "repubblica sudafricana",
      "federazione russa": "russia - federazione russa"
    },
    "attivazioneComune": {
      // "bolzano": "bolzano/bozen",
      // "reggio calabria": "reggio di calabria",
      // "seveso (mi)": "seveso (mb)",
      // "andria (ba)": "andria (bt)",
      // "bosa (nu)": "bosa (or)",
      // "monza (mi)": "monza (mb)",
      "monza (mb)": "monza (mi)",
      "merano": "merano/meran",
      // "merano (bz)": "merano/meran (bz)",
      // "fermo (ap)": "fermo (fm)",
      // "reggio di calabria (rc)": "reggio calabria (rc)",
      // "bolzano/bozen (bz)": "bolzano (bz)"
    },
    "attivazioneComune (EE)": {
      "dominicana repubblica": "repubblica dominicana",
      "iugoslavia": "jugoslavia",
      "centrafricana repubblica": "impero centroafricano",
      "centroafricano impero": "impero centroafricano",
      "azerbaigian": "azerbaidjan",
      "russia (federazione russa)": "russia - federazione russa",
      "irlanda (eire)": "irlanda",
      "moldova": "moldavia",
      "slovacchia": "repubblica slovacca",
      "cina": "cina repubblica popolare",
      "calatafimi-segesta": "calatafimi segesta",
      "macedonia del nord": "macedonia",
      "repubblica socialista federale di jugoslavia": "jugoslavia",
      "regno unito": "gran bretagna",
      "repubblica democratica del congo": "congo repubblica democratica",
      "unione sovietica": "urss",
      "bosnia-erzegovina": "bosnia ed erzegovina",
      "sudafrica": "repubblica sudafricana",
      "federazione russa": "russia - federazione russa"
    },
    "recapitoComune": {
      "bolzano": "bolzano/bozen",
      "reggio calabria": "reggio di calabria",
      "seveso (mi)": "seveso (mb)",
      "andria (ba)": "andria (bt)",
      "bosa (nu)": "bosa (or)",
      // "monza (mi)": "monza (mb)",
      "merano": "merano/meran",
      "merano (bz)": "merano/meran (bz)",
      "fermo (ap)": "fermo (fm)",
      "reggio di calabria (rc)": "reggio calabria (rc)",
      "bolzano/bozen (bz)": "bolzano (bz)"
    },
    "recapitoComune (EE)": {
      "dominicana repubblica": "repubblica dominicana",
      "iugoslavia": "jugoslavia",
      "centrafricana repubblica": "impero centroafricano",
      "centroafricano impero": "impero centroafricano",
      "azerbaigian": "azerbaidjan",
      "russia (federazione russa)": "russia - federazione russa",
      "irlanda (eire)": "irlanda",
      "moldova": "moldavia",
      "slovacchia": "repubblica slovacca",
      "cina": "cina repubblica popolare",
      "calatafimi-segesta": "calatafimi segesta",
      "macedonia del nord": "macedonia",
      "repubblica socialista federale di jugoslavia": "jugoslavia",
      "regno unito": "gran bretagna",
      "repubblica democratica del congo": "congo repubblica democratica",
      "unione sovietica": "urss",
      "bosnia-erzegovina": "bosnia ed erzegovina",
      "sudafrica": "repubblica sudafricana",
      "federazione russa": "russia - federazione russa"
    }
  }
}

module.exports = {
  up: async (queryInterface, Sequelize) => {
    const audit = {
      dataCreazione: new Date(),
      dataAggiornamento: new Date()
    };

    await scrapersEnsureExists(
      queryInterface,
      'fastweb',
      'Fastweb',
      'fastweb',
      'inserimento'
    );

    await up(tables, data)(queryInterface, Sequelize);
  },
  down: down(tables)
};
