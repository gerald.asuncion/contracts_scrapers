'use strict';

let tables = {
  // CampiEditabili: { schema: 'app', tableName: 'CampiEditabili' },
  Scrapers: { tableName: 'Scrapers' },
  CampiEditabili: { tableName: 'CampiEditabili' },
  Mapping: { tableName: 'Mappings' }
};

const data = {
  scraper: 'eolo',
  campi: [{
    label: 'Comune di attivazione',
    campo: 'comuneAttivazione',
    tipo: 'geo'
  }, {
    label: 'Comune di nascita dell\'intestatario della linea telefonica',
    campo: 'comuneDiNascitaIntestatarioLineaTelefonica',
    tipo: 'geo'
  }, {
    label: 'Comune di rilascio del documento',
    campo: 'comuneRilascioDocumento',
    tipo: 'geo'
  }, {
    label: 'Comune di fatturazione',
    campo: 'comuneFatturazione',
    tipo: 'geo'
  }, {
    label: 'Comune di nascita di fatturazione',
    campo: 'comuneDiNascitaFatturazione',
    tipo: 'geo'
  }, {
    label: 'Comune di nascita di intestatario',
    campo: 'comuneDiNascitaIntestatario',
    tipo: 'geo'
  }],
  mapping: {
    "comuneAttivazione": {
      "magliano de' marsi": "Magliano de' Marsi",
      "cassina de' pecchi": "Cassina de' Pecchi",
      "bellaria-igea-marina": "Bellaria-Igea Marina",
      "gadesco-pieve-delmona": "Gadesco-Pieve Delmona",
      "pieve di bono prezzo": "Pieve di Bono-Prezzo",
      "pieve-di-bono-prezzo": "Pieve di Bono-Prezzo",
      "trentola-ducenta": "Trentola Ducenta",
      "reggio emilia": "Reggio nell'Emilia",
      "bielorussia (russia bianca)": "bielorussia",
      "russia (federazione russa)": "russia",
      "burkina faso": "burkina",
      "regno unito": "gran bretagna",
      "repubblica socialista federale di jugoslavia": "iugoslavia",
      "cassina de' pecchi (mi)": "Cassina de' Pecchi",
      "repubblica dominicana": "Dominicana Repubblica",
      "repubblica democratica del congo": "Congo Repubblica Popolare",
      "macedonia del nord": "Macedonia",
      "federazione russa": "russia",
      "kosovo": "kossovo",
      "moldova": "moldavia",
      "pre'-saint-didier": "saint-didier"
    },
    "comuneDiNascitaIntestatarioLineaTelefonica": {
      "magliano de' marsi": "Magliano de' Marsi",
      "cassina de' pecchi": "Cassina de' Pecchi",
      "bellaria-igea-marina": "Bellaria-Igea Marina",
      "gadesco-pieve-delmona": "Gadesco-Pieve Delmona",
      "pieve di bono prezzo": "Pieve di Bono-Prezzo",
      "pieve-di-bono-prezzo": "Pieve di Bono-Prezzo",
      "trentola-ducenta": "Trentola Ducenta",
      "reggio emilia": "Reggio nell'Emilia",
      "bielorussia (russia bianca)": "bielorussia",
      "russia (federazione russa)": "russia",
      "burkina faso": "burkina",
      "regno unito": "gran bretagna",
      "repubblica socialista federale di jugoslavia": "iugoslavia",
      "cassina de' pecchi (mi)": "Cassina de' Pecchi",
      "repubblica dominicana": "Dominicana Repubblica",
      "repubblica democratica del congo": "Congo Repubblica Popolare",
      "macedonia del nord": "Macedonia",
      "federazione russa": "russia",
      "kosovo": "kossovo",
      "moldova": "moldavia",
      "pre'-saint-didier": "saint-didier"
    },
    "comuneRilascioDocumento": {
      "magliano de' marsi": "Magliano de' Marsi",
      "cassina de' pecchi": "Cassina de' Pecchi",
      "bellaria-igea-marina": "Bellaria-Igea Marina",
      "gadesco-pieve-delmona": "Gadesco-Pieve Delmona",
      "pieve di bono prezzo": "Pieve di Bono-Prezzo",
      "pieve-di-bono-prezzo": "Pieve di Bono-Prezzo",
      "trentola-ducenta": "Trentola Ducenta",
      "reggio emilia": "Reggio nell'Emilia",
      "bielorussia (russia bianca)": "bielorussia",
      "russia (federazione russa)": "russia",
      "burkina faso": "burkina",
      "regno unito": "gran bretagna",
      "repubblica socialista federale di jugoslavia": "iugoslavia",
      "cassina de' pecchi (mi)": "Cassina de' Pecchi",
      "repubblica dominicana": "Dominicana Repubblica",
      "repubblica democratica del congo": "Congo Repubblica Popolare",
      "macedonia del nord": "Macedonia",
      "federazione russa": "russia",
      "kosovo": "kossovo",
      "moldova": "moldavia",
      "pre'-saint-didier": "saint-didier"
    },
    "comuneFatturazione": {
      "magliano de' marsi": "Magliano de' Marsi",
      "cassina de' pecchi": "Cassina de' Pecchi",
      "bellaria-igea-marina": "Bellaria-Igea Marina",
      "gadesco-pieve-delmona": "Gadesco-Pieve Delmona",
      "pieve di bono prezzo": "Pieve di Bono-Prezzo",
      "pieve-di-bono-prezzo": "Pieve di Bono-Prezzo",
      "trentola-ducenta": "Trentola Ducenta",
      "reggio emilia": "Reggio nell'Emilia",
      "bielorussia (russia bianca)": "bielorussia",
      "russia (federazione russa)": "russia",
      "burkina faso": "burkina",
      "regno unito": "gran bretagna",
      "repubblica socialista federale di jugoslavia": "iugoslavia",
      "cassina de' pecchi (mi)": "Cassina de' Pecchi",
      "repubblica dominicana": "Dominicana Repubblica",
      "repubblica democratica del congo": "Congo Repubblica Popolare",
      "macedonia del nord": "Macedonia",
      "federazione russa": "russia",
      "kosovo": "kossovo",
      "moldova": "moldavia",
      "pre'-saint-didier": "saint-didier"
    },
    "comuneDiNascitaFatturazione": {
      "magliano de' marsi": "Magliano de' Marsi",
      "cassina de' pecchi": "Cassina de' Pecchi",
      "bellaria-igea-marina": "Bellaria-Igea Marina",
      "gadesco-pieve-delmona": "Gadesco-Pieve Delmona",
      "pieve di bono prezzo": "Pieve di Bono-Prezzo",
      "pieve-di-bono-prezzo": "Pieve di Bono-Prezzo",
      "trentola-ducenta": "Trentola-Ducenta",
      "reggio emilia": "Reggio nell'Emilia",
      "bielorussia (russia bianca)": "bielorussia",
      "russia (federazione russa)": "russia",
      "burkina faso": "burkina",
      "regno unito": "gran bretagna",
      "repubblica socialista federale di jugoslavia": "iugoslavia",
      "cassina de' pecchi (mi)": "Cassina de' Pecchi",
      "repubblica dominicana": "Dominicana Repubblica",
      "repubblica democratica del congo": "Congo Repubblica Popolare",
      "macedonia del nord": "Macedonia",
      "federazione russa": "russia",
      "kosovo": "kossovo",
      "moldova": "moldavia",
      "pre'-saint-didier": "saint-didier"
    },
    "comuneDiNascitaIntestatario": {
      "magliano de' marsi": "Magliano de' Marsi",
      "cassina de' pecchi": "Cassina de' Pecchi",
      "bellaria-igea-marina": "Bellaria-Igea Marina",
      "gadesco-pieve-delmona": "Gadesco-Pieve Delmona",
      "pieve di bono prezzo": "Pieve di Bono-Prezzo",
      "pieve-di-bono-prezzo": "Pieve di Bono-Prezzo",
      "trentola-ducenta": "Trentola-Ducenta",
      "reggio emilia": "Reggio nell'Emilia",
      "bielorussia (russia bianca)": "bielorussia",
      "russia (federazione russa)": "russia",
      "burkina faso": "burkina",
      "regno unito": "gran bretagna",
      "repubblica socialista federale di jugoslavia": "iugoslavia",
      "cassina de' pecchi (mi)": "Cassina de' Pecchi",
      "repubblica dominicana": "Dominicana Repubblica",
      "repubblica democratica del congo": "Congo Repubblica Popolare",
      "macedonia del nord": "Macedonia",
      "federazione russa": "russia",
      "kosovo": "kossovo",
      "moldova": "moldavia",
      "pre'-saint-didier": "saint-didier"
    }
  }
}

module.exports = {
  up: async (queryInterface, Sequelize) => {
    const audit = {
      dataCreazione: new Date(),
      dataAggiornamento: new Date()
    };

    const transaction = await queryInterface.sequelize.transaction();

    try {
      const [[{ id: scraperId }]] = await queryInterface.sequelize.query(`
        SELECT id
        FROM ${tables.Scrapers.tableName} t0
        WHERE t0.codice = '${data.scraper}'
      `, {
        returning: true,
        transaction
      });

      console.log('Scraper:', data.scraper, 'id:', scraperId);

      const [ listaCampiDb ] = await queryInterface.sequelize.query(`
        SELECT id, campo
        FROM ${tables.CampiEditabili.tableName} t0
      `, {
        returning: true,
        transaction
      });

      const listaCampiToAdd = data.campi
      .filter(({ campo }) => !listaCampiDb.includes(({ campo: campoDb }) => campoDb === campo))
      .map(data => ({
        ...audit,
        ...data
      }));

      // id sono interi sequenziali, ma a causa dei possibili sdoppioni ora sono sballati
      const campiFirstInsertedId = await queryInterface.bulkInsert(tables.CampiEditabili, listaCampiToAdd, {
        returning: true,
        transaction
      });

      const [ listaCampiDbNew ] = await queryInterface.sequelize.query(`
        SELECT id, campo
        FROM ${tables.CampiEditabili.tableName} t0
      `, {
        returning: true,
        transaction
      });

      console.log('Campi aggiunti:', listaCampiDbNew.length - listaCampiDb.length);

      await queryInterface.bulkInsert(tables.Mapping, listaCampiToAdd.map(({ campo }, idx) => ({
        ...audit,
        scraperId,
        // campoId: listaCampiDb campiFirstInsertedId + idx,
        campoId: listaCampiDbNew.find(({ campo: campoDb }) => campoDb === campo).id,
        mapping: JSON.stringify(data.mapping[campo])
      })), {
        transaction
      });

      await transaction.commit();
    } catch (ex) {
      console.error('Rollbackking transaction');
      console.error(ex);

      transaction && await transaction.rollback();

      throw ex;
    }
  },

  down: async (queryInterface, Sequelize) => {
    // questo è il primo seeding, quindi se arriviamo fin qui cancelliamoli tutti
    await queryInterface.bulkDelete(tables.Mapping, null, {});
    await queryInterface.bulkDelete(tables.CampiEditabili, null, {});
  }
};
