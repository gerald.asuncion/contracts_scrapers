'use strict';

const { up, down } = require('../helpers/seeders-mapping')

let tables = {
  Scrapers: { tableName: 'Scrapers' },
  CampiEditabili: { tableName: 'CampiEditabili' },
  Mapping: { tableName: 'Mappings' }
};

const data = {
  scraper: 'fastweb-verifica-documento',
  campi: [
    {
      "label": "Nascita Stato",
      "campo": "nascitaStato",
      "tipo": "geo"
    }
  ],
  mapping: {
    "nascitaStato": {
      "unione sovietica": "unione repubbliche socialiste sovietiche",
      "federazione russa": "russia"
    }
  }
}

module.exports = {
  up: up(tables, data),
  down: down(tables)
};
