'use strict';

let tables = {
  // CampiEditabili: { schema: 'app', tableName: 'CampiEditabili' },
  Scrapers: { tableName: 'Scrapers' },
  CampiEditabili: { tableName: 'CampiEditabili' },
  Mapping: { tableName: 'Mappings' }
};

const data = {
  scraper: 'eni',
  campi: [
    { label: "Comune Indirizzo Attivazione", campo: "comuneIndirizzoAttivazione", tipo: "geo" },
    { label: "Provincia Sede Legale", campo: "provinciaSedeLegale", tipo: "geo" },
    { label: "Provincia Indirizzo Attivazione", campo: "provinciaIndirizzoAttivazione", tipo: "geo" },
    { label: "Provincia Recapito Fattura", campo: "provinciaRecapitoFattura", tipo: "geo" },
    { label: "Comune Nascita Legale Rappresentante", campo: "comuneNascitaLegaleRappresentante", tipo: "geo" },
    { label: "Comune Rilascio Documento Legale Rappresentante", campo: "comuneRilascioDocumentoLegaleRappresentante", tipo: "geo" },
    { label: "Provincia Nascita Legale Rappresentante", campo: "provinciaNascitaLegaleRappresentante", tipo: "geo" },
    { label: "Comune Sede Legale", campo: "comuneSedeLegale", tipo: "geo" },
    { label: "Comune Recapito Fattura", campo: "comuneRecapitoFattura", tipo: "geo" }
  ],
  mapping: {
    "comuneIndirizzoAttivazione": {
      "gran bretagna e irlanda del nord": "regno unito",
      "germania repubblica democratica": "germania",
      "reggio di calabria": "reggio calabria",
      "iran": "iran, repubblica islamica del"
    },
    "provinciaSedeLegale": {
      "monza e della brianza": "monza brianza",
      "reggio calabria": "reggio di calabria",
      "reggio emilia": "reggio nell'emilia",
      "forlì": "forlì-cesena",
      "forli'": "forlì-cesena",
      "pesaro-urbino": "pesaro urbino",
      "verbania": "verbano-cusio-ossola",
      "ee": "estero",
      "verbano cusio ossola": "verbano-cusio-ossola",
      "pesaro e urbino": "pesaro urbino",
      "barletta-andria-trani": "barletta andria trani",
      "massa e carrara": "massa-carrara"
    },
    "provinciaIndirizzoAttivazione": {
      "monza e della brianza": "monza brianza",
      "reggio calabria": "reggio di calabria",
      "reggio emilia": "reggio nell'emilia",
      "forlì": "forlì-cesena",
      "forli'": "forlì-cesena",
      "pesaro-urbino": "pesaro urbino",
      "verbania": "verbano-cusio-ossola",
      "ee": "estero",
      "verbano cusio ossola": "verbano-cusio-ossola",
      "pesaro e urbino": "pesaro urbino",
      "barletta-andria-trani": "barletta andria trani",
      "massa e carrara": "massa-carrara"
    },
    "provinciaRecapitoFattura": {
      "monza e della brianza": "monza brianza",
      "reggio calabria": "reggio di calabria",
      "reggio emilia": "reggio nell'emilia",
      "forlì": "forlì-cesena",
      "forli'": "forlì-cesena",
      "pesaro-urbino": "pesaro urbino",
      "verbania": "verbano-cusio-ossola",
      "ee": "estero",
      "verbano cusio ossola": "verbano-cusio-ossola",
      "pesaro e urbino": "pesaro urbino",
      "barletta-andria-trani": "barletta andria trani",
      "massa e carrara": "massa-carrara"
    },
    "comuneNascitaLegaleRappresentante": {
      "gran bretagna e irlanda del nord": "regno unito",
      "germania repubblica democratica": "germania",
      "reggio di calabria": "reggio calabria",
      "iran": "iran, repubblica islamica del"
    },
    "comuneRilascioDocumentoLegaleRappresentante": {
      "gran bretagna e irlanda del nord": "regno unito",
      "germania repubblica democratica": "germania",
      "reggio di calabria": "reggio calabria",
      "iran": "iran, repubblica islamica del"
    },
    "provinciaNascitaLegaleRappresentante": {
      "monza e della brianza": "monza brianza",
      "reggio calabria": "reggio di calabria",
      "reggio emilia": "reggio nell'emilia",
      "forlì": "forlì-cesena",
      "forli'": "forlì-cesena",
      "pesaro-urbino": "pesaro urbino",
      "verbania": "verbano-cusio-ossola",
      "ee": "estero",
      "verbano cusio ossola": "verbano-cusio-ossola",
      "pesaro e urbino": "pesaro urbino",
      "barletta-andria-trani": "barletta andria trani",
      "massa e carrara": "massa-carrara"
    },
    "comuneSedeLegale": {
      "gran bretagna e irlanda del nord": "regno unito",
      "germania repubblica democratica": "germania",
      "reggio di calabria": "reggio calabria",
      "iran": "iran, repubblica islamica del"
    },
    "comuneRecapitoFattura": {
      "gran bretagna e irlanda del nord": "regno unito",
      "germania repubblica democratica": "germania",
      "reggio di calabria": "reggio calabria",
      "iran": "iran, repubblica islamica del"
    }
  }
};

module.exports = {
  up: async (queryInterface, Sequelize) => {
    const audit = {
      dataCreazione: new Date(),
      dataAggiornamento: new Date()
    };

    const transaction = await queryInterface.sequelize.transaction();

    try {
      const [[{ id: scraperId }]] = await queryInterface.sequelize.query(`
        SELECT id
        FROM ${tables.Scrapers.tableName} t0
        WHERE t0.codice = '${data.scraper}'
      `, {
        returning: true,
        transaction
      });

      console.log('Scraper:', data.scraper, 'id:', scraperId);

      const [ listaCampiDb ] = await queryInterface.sequelize.query(`
        SELECT id, campo
        FROM ${tables.CampiEditabili.tableName} t0
      `, {
        returning: true,
        transaction
      });

      const listaCampiToAdd = data.campi
      .filter(({ campo }) => !listaCampiDb.includes(({ campo: campoDb }) => campoDb === campo))
      .map(data => ({
        ...audit,
        ...data
      }));

      // id sono interi sequenziali, ma a causa dei possibili sdoppioni ora sono sballati
      const campiFirstInsertedId = await queryInterface.bulkInsert(tables.CampiEditabili, listaCampiToAdd, {
        returning: true,
        transaction
      });

      const [ listaCampiDbNew ] = await queryInterface.sequelize.query(`
        SELECT id, campo
        FROM ${tables.CampiEditabili.tableName} t0
      `, {
        returning: true,
        transaction
      });

      console.log('Campi aggiunti:', listaCampiDbNew.length - listaCampiDb.length);

      await queryInterface.bulkInsert(tables.Mapping, listaCampiToAdd.map(({ campo }, idx) => ({
        ...audit,
        scraperId,
        // campoId: listaCampiDb campiFirstInsertedId + idx,
        campoId: listaCampiDbNew.find(({ campo: campoDb }) => campoDb === campo).id,
        mapping: JSON.stringify(data.mapping[campo])
      })), {
        transaction
      });

      await transaction.commit();
    } catch (ex) {
      console.error('Rollbackking transaction');
      console.error(ex);

      transaction && await transaction.rollback();

      throw ex;
    }
  },

  down: async (queryInterface, Sequelize) => {
    // filter by scraperId
    // await queryInterface.bulkDelete(tables.Mapping, null, {});
    // await queryInterface.bulkDelete(tables.CampiEditabili, null, {});
  }
};
