'use strict';

const scrapersEnsureExists = require('../helpers/query-scrapers-ensureexists')

let tables = {
  Scrapers: { tableName: 'Scrapers' },
  CampiEditabili: { tableName: 'CampiEditabili' },
  Mapping: { tableName: 'Mappings' }
};

const data = {
  scraper: 'arera',
  scraperLabel: 'Arera',
  campi: [
    { label: "Provincia", campo: "provincia", tipo: "geo" },
    { label: "Regione", campo: "regione", tipo: "geo" }
  ],
  mapping: {
    provincia: {
      'reggio calabria': 'reggio di calabria',
      'reggio emilia': "reggio nell'emilia",
      'forli-cesena': 'forlì-cesena',
      'bolzano': 'bolzano/bozen',
      'verbano cusio ossola': 'verbano-cusio-ossola',
      'pesaro-urbino': 'pesaro e urbino',
      'verbania': 'verbano-cusio-ossola'
    },
    regione: {
      'piemonte': '01',
      "valle d'aosta": '02',
      "lombardia": '03',
      "trentino-alto adige": '04',
      'veneto': '05',
      'friuli-venezia giulia': '06',
      'liguria': '07',
      'emilia-romagna': '08',
      'toscana': '09',
      'umbria': '10',
      'marche': '11',
      'lazio': '12',
      'abruzzo': '13',
      'molise': '14',
      'campania': '15',
      'puglia': '16',
      'basilicata': '17',
      'calabria': '18',
      'sicilia': '19',
      'sardegna': '20'
    }
  }
};

module.exports = {
  up: async (queryInterface, Sequelize) => {
    const audit = {
      dataCreazione: new Date(),
      dataAggiornamento: new Date()
    };

    await scrapersEnsureExists(
      queryInterface,
      data.scraper,
      data.scraperLabel,
      data.scraper,
      'inserimento'
    );

    const transaction = await queryInterface.sequelize.transaction();

    try {
      const [[{ id: scraperId }]] = await queryInterface.sequelize.query(`
        SELECT id
        FROM ${tables.Scrapers.tableName} t0
        WHERE t0.codice = '${data.scraper}'
      `, {
        returning: true,
        transaction
      });

      console.log('Scraper:', data.scraper, 'id:', scraperId);

      const [ listaCampiDb ] = await queryInterface.sequelize.query(`
        SELECT id, campo
        FROM ${tables.CampiEditabili.tableName} t0
      `, {
        returning: true,
        transaction
      });

      const listaCampiToAdd = data.campi
      .filter(({ campo }) => !listaCampiDb.includes(({ campo: campoDb }) => campoDb === campo))
      .map(data => ({
        ...audit,
        ...data
      }));

      // id sono interi sequenziali, ma a causa dei possibili sdoppioni ora sono sballati
      const campiFirstInsertedId = await queryInterface.bulkInsert(tables.CampiEditabili, listaCampiToAdd, {
        returning: true,
        transaction
      });

      const [ listaCampiDbNew ] = await queryInterface.sequelize.query(`
        SELECT id, campo
        FROM ${tables.CampiEditabili.tableName} t0
      `, {
        returning: true,
        transaction
      });

      console.log('Campi aggiunti:', listaCampiDbNew.length - listaCampiDb.length);

      await queryInterface.bulkInsert(tables.Mapping, listaCampiToAdd.map(({ campo }, idx) => ({
        ...audit,
        scraperId,
        // campoId: listaCampiDb campiFirstInsertedId + idx,
        campoId: listaCampiDbNew.find(({ campo: campoDb }) => campoDb === campo).id,
        mapping: JSON.stringify(data.mapping[campo])
      })), {
        transaction
      });

      await transaction.commit();
    } catch (ex) {
      console.error('Rollbackking transaction');
      console.error(ex);

      transaction && await transaction.rollback();

      throw ex;
    }
  },

  down: async (queryInterface, Sequelize) => {
    // nulla da fare
  }
};
