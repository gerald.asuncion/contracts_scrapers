'use strict';

// let tables = {
//   // CampiEditabili: { schema: 'app', tableName: 'CampiEditabili' },
//   Scrapers: { tableName: 'Scrapers' },
//   CampiEditabili: { tableName: 'CampiEditabili' },
//   Mapping: { tableName: 'Mappings' }
// };

// const data = {
//   scraper: '...',
//   campi: [],
//   mapping: {
//   }
// }

module.exports = {
  up: (tables, data) => async (queryInterface, Sequelize) => {
    const audit = {
      dataCreazione: new Date(),
      dataAggiornamento: new Date()
    };

    const transaction = await queryInterface.sequelize.transaction();

    try {
      const [[{ id: scraperId }]] = await queryInterface.sequelize.query(`
        SELECT id
        FROM ${tables.Scrapers.tableName} t0
        WHERE t0.codice = '${data.scraper}'
      `, {
        returning: true,
        transaction
      });

      console.log('Scraper:', data.scraper, 'id:', scraperId);

      const [ listaCampiDb ] = await queryInterface.sequelize.query(`
        SELECT id, campo
        FROM ${tables.CampiEditabili.tableName} t0
      `, {
        returning: true,
        transaction
      });

      const listaCampiToAdd = data.campi
      .filter(({ campo }) => !listaCampiDb.includes(({ campo: campoDb }) => campoDb === campo))
      .map(data => ({
        ...audit,
        ...data
      }));

      // id sono interi sequenziali, ma a causa dei possibili sdoppioni ora sono sballati
      const campiFirstInsertedId = await queryInterface.bulkInsert(tables.CampiEditabili, listaCampiToAdd, {
        returning: true,
        transaction
      });

      const [ listaCampiDbNew ] = await queryInterface.sequelize.query(`
        SELECT id, campo
        FROM ${tables.CampiEditabili.tableName} t0
      `, {
        returning: true,
        transaction
      });

      console.log('Campi aggiunti:', listaCampiDbNew.length - listaCampiDb.length);

      await queryInterface.bulkInsert(tables.Mapping, listaCampiToAdd.map(({ campo }, idx) => ({
        ...audit,
        scraperId,
        // campoId: listaCampiDb campiFirstInsertedId + idx,
        campoId: listaCampiDbNew.find(({ campo: campoDb }) => campoDb === campo).id,
        mapping: JSON.stringify(data.mapping[campo])
      })), {
        transaction
      });

      await transaction.commit();
    } catch (ex) {
      console.error('Rollbackking transaction');
      console.error(ex);

      transaction && await transaction.rollback();

      throw ex;
    }
  },

  down: (tables) => async (queryInterface, Sequelize) => {
    // questo è il primo seeding, quindi se arriviamo fin qui cancelliamoli tutti
    await queryInterface.bulkDelete(tables.Mapping, null, {});
    await queryInterface.bulkDelete(tables.CampiEditabili, null, {});
  }
};
