module.exports = async function(
  queryInterface,
  codice, label, partner, tipologia,
  tableName = 'Scrapers'
) {
  console.log(`checking if ${label} scraper exists.`)

  const {
    ScraperExists
  } = await queryInterface.sequelize.query(
    `SELECT EXISTS(SELECT * FROM ${tableName} WHERE codice = ?) as ScraperExists`,
    {
      replacements: [
        codice
      ],
      plain: true,
      raw: false,
      type: queryInterface.sequelize.QueryTypes.SELECT
    }
  );

  if (ScraperExists == 0) {
    console.log(`inserting ${label} scraper.`);

    const audit = {
      dataCreazione: new Date(),
      dataAggiornamento: new Date()
    };

    await queryInterface.bulkInsert({ tableName }, [{
      ...audit,
      label,
      codice,
      partner,
      tipologia
    }]);
  }
}
