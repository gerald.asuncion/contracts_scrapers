const tables = {
  // CampiEditabili: { schema: 'app', tableName: 'CampiEditabili' },
  Scrapers: { tableName: 'Scrapers' },
  CampiEditabili: { tableName: 'CampiEditabili' },
  Mapping: { tableName: 'Mappings' },
  Credenziali: { tableName: 'Credenziali' },
  RelCredenziali: { tableName: 'RelCredenziali' }
};

async function getScraperId(queryInterface, scraperCodice, tableName = tables.Scrapers.tableName) {
  // console.log(`checking if scraper "${scraperCodice}" exists.`)

  const {
    id
  } = await queryInterface.sequelize.query(
    `SELECT scr.id as id FROM ${tableName} scr WHERE codice = ?`,
    {
      replacements: [
        scraperCodice
      ],
      plain: true,
      // raw: true,
      type: queryInterface.sequelize.QueryTypes.SELECT
    }
  );

  return id;
}

async function credentialExists(queryInterface, username, scraperIdOrCode) {
  // console.log(`checking if credential "${username}" exists for scraper "${scraperIdOrCode}".`)

  const query = `
    SELECT EXISTS (
      select * from ${tables.Credenziali.tableName} c
      left join ${tables.RelCredenziali.tableName} rc on rc.credenzialiId = c.id
      left join ${tables.Scrapers.tableName} s on rc.scraperId = s.id
      where
        c.username = ? AND
        ${typeof scraperIdOrCode === 'string' ? "s.codice" : "s.id"} = ?
    ) as CredentialExists
  `;

  const { CredentialExists } = await queryInterface.sequelize.query(query, {
    replacements: [
      username,
      scraperIdOrCode
    ],
    plain: true,
    raw: false,
    type: queryInterface.sequelize.QueryTypes.SELECT
  });

  return !!CredentialExists;
}

async function addCredenziale(queryInterface, { username, password, durataPassword = 100, email = '' }, scraperId) {
  await queryInterface.sequelize.transaction(async(transaction) => {
    await queryInterface.sequelize.query(`INSERT INTO ${tables.Credenziali.tableName} (username, password, durataPassword, email, dataCreazione, dataAggiornamento) VALUES(?, ?, ?, ?, now(), now());`, {
      replacements: [
        username,
        password,
        durataPassword,
        email
      ],
      transaction
    });

    await queryInterface.sequelize.query(`INSERT INTO ${tables.RelCredenziali.tableName} (scraperId, credenzialiId, dataCreazione, dataAggiornamento) VALUES(?, LAST_INSERT_ID(), now(), now());`, {
      replacements: [
        scraperId
      ],
      transaction
    });
  });
}

module.exports = {
  getScraperId,
  credentialExists,
  addCredenziale
}
