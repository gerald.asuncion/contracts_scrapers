module.exports = {
  apps : [
    {
      name: 'api',
      script: './build/api/index.js',
      watch: ['api.version.txt'],
      autorestart: true,
      error_file: '/var/log/web/scrapers.supermoney.lan/pm2.log',
      out_file: '/var/log/web/scrapers.supermoney.lan/pm2.log',
      wait_ready: true,
      treekill: false,
      kill_timeout: 1200000, // 20 minuti
      // exec_mode: 'cluster',
      // instances: 4,
      time: true,
      env: {
        NODE_ENV: 'prod',
        NODE_APP_INSTANCE: 'prod'
      },
      env_dev: {
        NODE_ENV: 'development',
        NODE_APP_INSTANCE: 'dev'
      },
      env_test: {
        NODE_ENV: 'test',
        NODE_APP_INSTANCE: 'test'
      }
    },
    {
      name: 'taskrunner',
      script: './build/taskrunner/index.taskrunner.js',
      instances: 8,
      exec_mode: "fork",
      watch: ['taskrunner.version.txt'],
      autorestart: true,
      error_file: '/var/log/web/scrapers.supermoney.lan/pm2.log',
      out_file: '/var/log/web/scrapers.supermoney.lan/pm2.log',
      wait_ready: true,
      treekill: false,
      kill_timeout: 1200000, // 20 minuti
      time: true,
      env: {
        NODE_ENV: 'prod',
        NODE_APP_INSTANCE: 'prod'
      },
      env_dev: {
        NODE_ENV: 'development',
        NODE_APP_INSTANCE: 'dev'
      },
      env_test: {
        NODE_ENV: 'test',
        NODE_APP_INSTANCE: 'test'
      }
    }
  ],

  // deploy : {
  //   production : {
  //     user : 'node',
  //     host : '212.83.163.1',
  //     ref  : 'origin/master',
  //     repo : 'git@github.com:repo.git',
  //     path : '/var/www/production',
  //     'post-deploy' : 'npm install && pm2 reload ecosystem.config.js --env production'
  //   }
  // }
};
